<?php

namespace App\Components\Rowcomprasnfe;

class Rowcomprasnfe
{
//Função que monta a linha de resultado da busca na tabela
// $config é composto por:
// "dado" => Dados que serão utilizados para a montagem da linha,
// "request" => Request original,
// 'buscar_por' => Buscar por (utilizado para montar a row de acordo com o padrão dynamic row),
// 'busca_parametro' => Parametro de busca (utilizado para montar a row de acordo com o padrão dynamic row),
// 'reset' => Identifica se o request é do tipo RESET, onde é recarregado o estado original da página
   public function generaterow(object $config)
   {
      $result = '';
      foreach($config->dado as $key => $dado) {
         $result .= '<tr>
                        <td>
                            <svg onclick="toggleModal('.$key.')" id="card_plus'.$key.'"
                            xmlns="http://www.w3.org/2000/svg" class="mx-4 h-5 w-5 text-green-600 cursor-pointer"
                            viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd"
                                d="M10 18a8 8 0 100-16 8 8 0 000 16zm1-11a1 1 0 10-2 0v2H7a1 1 0 100 2h2v2a1 1 0 102 0v-2h2a1 1 0 100-2h-2V7z"
                                clip-rule="evenodd" />
                            </svg>
                            <svg id="card_minus'.$key.'" xmlns="http://www.w3.org/2000/svg"
                            class="hidden mx-4 h-5 w-5 text-red-600" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd"
                                d="M10 18a8 8 0 100-16 8 8 0 000 16zM7 9a1 1 0 000 2h6a1 1 0 100-2H7z"
                                clip-rule="evenodd" />
                            </svg>
                        </td>
                        <td class="px-6 py-4 whitespace-normal">'.$dado->razaosocialdestinatario.'</td>
                        <td class="px-6 py-4 whitespace-normal">'.$dado->cnpjdestinatario.'</td>
                        <td class="px-6 py-4 whitespace-normal">'.$dado->numnfe.'</td>
                        <td class="px-6 py-4 whitespace-normal">'.date('d/m/Y' , strtotime($dado->dtemissao)).'</td>
                        <td class="px-6 py-4 whitespace-normal">'.$dado->chavenfe.'</td>
                        <td class="px-6 py-4 whitespace-normal">'.$dado->razaosocialemitente.'</td>
                        <td>
                            <!--Modal-->
                            <div id="modal'.$key.'"
                                class="modal opacity-0 pointer-events-none fixed w-full h-full top-0 left-0 flex items-center justify-center">
                                <div class="modal-overlay absolute w-full h-full bg-gray-900 opacity-50"></div>
        
                                <div
                                    class="modal-container bg-white w-11/12 md:max-w-2xl mx-auto rounded shadow-lg z-50 overflow-y-auto">
                                    <div
                                    class="modal-close absolute top-0 right-0 cursor-pointer flex flex-col items-center mt-4 mr-4 text-white text-sm z-50"
                                    name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                    <svg onclick="toggleModal('.$key.')" class="fill-current text-white"
                                        xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                                        <path
                                            d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z">
                                        </path>
                                    </svg>
                                    <span class="text-sm" name="texto"
                                        style="font-size: 0.875rem; line-height: 1.25rem;">(Esc)</span>
                                    </div>
        
                                    <!-- Add margin if you want to see some of the overlay behind the modal-->
                                    <div class="modal-content py-2 pb-6 text-left px-6">
        
                                    <!--botão X de fechar-->
                                    <div class="flex justify-end items-center">
                                        <div class="modal-close cursor-pointer z-50">
                                            <svg onclick="toggleModal('.$key.')" class="fill-current text-red-500"
                                                xmlns="http://www.w3.org/2000/svg" width="22" height="22" font-weight="bold"
                                                viewBox="0 0 18 18">
                                                <path
                                                d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z">
                                                </path>
                                            </svg>
                                        </div>
                                    </div>
        
                                    <!--Body-->
                                    <div id='.$key.'>
                                        <div class="modal-content text-left px-6">
                                            <div class="flex justify-between items-center pb-3">
                                                <p class="text-sm font-bold text-lightBlue-500" name="texto"
                                                style="font-size: 0.875rem; line-height: 1.25rem;">Mais informações</p>
                                            </div>
                                            <div>
                                                <p class="text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                                <b class="mr-2 text-gray-700">CNPJ
                                                    Emitente:</b>'.$dado->cnpjemitente.'
                                                </p>
                                                <p class="text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                                <b class="mr-2 text-gray-700">Município
                                                    Origem:</b>'.$dado->municipioorigem.'
                                                </p>
                                                <p class="text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                                <b class="mr-2 text-gray-700">UF Origem:</b>'.$dado->uforigem.'
                                                </p>
                                                <p class="text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                                <b class="mr-2 text-gray-700">Município
                                                    Destino:</b>'.$dado->municipiodestino.'
                                                </p>
                                                <p class="text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                                <b class="mr-2 text-gray-700">UF
                                                    Destino:</b>'.$dado->ufdestino.'
                                                </p>
                                                <p class="text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                                <b class="mr-2 text-gray-700">Valor Total
                                                    Operação:</b>'.'R$ '.number_format($dado->vlroperacao, 2, ',', '.').'
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>';
        }

      return $result;
   }
}
