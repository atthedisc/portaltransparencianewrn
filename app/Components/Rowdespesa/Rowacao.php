<?php

namespace App\Components\Rowdespesa;

use Illuminate\Support\Facades\Crypt;

class Rowacao
{
//Função que monta a linha de resultado da busca na tabela
// $config é composto por:
// "dado" => Dados que serão utilizados para a montagem da linha,
// "request" => Request original,
// 'buscar_por' => Buscar por (utilizado para montar a row de acordo com o padrão dynamic row),
// 'busca_parametro' => Parametro de busca (utilizado para montar a row de acordo com o padrão dynamic row),
// 'reset' => Identifica se o request é do tipo RESET, onde é recarregado o estado original da página
   public function generaterow(object $config)
   {
      //recebe como parametro a query faz a montagem da row, a cada elemento de unidade gestora (pode ser mais de um) e mantem suas subfunçoes
      $result = '';
      foreach ($config->dado->acoes as $acao) {
         $result .= '<tr>
         <td class="pl-4">
            <a href="'.route('gastos-diretos-detalhada',
            ['request' => Crypt::encrypt([
               'codgrupodespesa' => 'codgrupodespesa',
               'codelementodespesa' => 'codelementodespesa',
               'codunidadegestora' => 'codunidadegestora',
               'codgestao' => 'codgestao',
               'codcredor' => 'codcredor',
               'codacao' => $acao->codacao,
               'codfonterecurso' => 'codfonterecurso',
               'txtdescricaofuncao' => 'txtdescricaofuncao',
               'codsubfuncao' => 'codsubfuncao',
               'fase' => $config->request->fase,
               'mes' => $config->request->mes,
               'ano' => $config->request->ano,
               'classificacao' => $config->request->classificacao,
               'posicao' => $config->request->posicao,
               'txtdescricaoacao' => $acao->txtdescricaoacao
            ])]).'">
               <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 text-lightBlue-500"
                  viewBox="0 0 20 20" fill="currentColor">
                  <path
                     d="M11 3a1 1 0 100 2h2.586l-6.293 6.293a1 1 0 101.414 1.414L15 6.414V9a1 1 0 102 0V4a1 1 0 00-1-1h-5z" />
                  <path
                     d="M5 5a2 2 0 00-2 2v8a2 2 0 002 2h8a2 2 0 002-2v-3a1 1 0 10-2 0v3H5V7h3a1 1 0 000-2H5z" />
               </svg>
            </a>
         </td>
         <td class="px-6 py-4">'.$acao->txtdescricaoacao.'</td>';
         if($config->request->fase != 'vportal_notapagamento_gastosdiretos'){
           $result .= '<td class="px-6 py-4 text-right">
            <span
               class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800"
               name="texto" style="font-size: 0.75rem; line-height: 1rem;">
               R$ '.number_format($acao->vlrpagamento, 2, ',', '.').'
            </span>
         </td>';
        } else {
            $result .= '<td class="px-6 py-4 whitespace-nowrap">
            <span
               class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-yellow-100 text-yellow-800"
               name="texto" style="font-size: 0.75rem; line-height: 1rem;">
               R$ '.number_format($acao->vlrpagamento, 2, ',', '.').'
            </span>
         </td>
         <td class="px-6 py-4 whitespace-nowrap text-right">
            <span
               class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800"
               name="texto" style="font-size: 0.75rem; line-height: 1rem;">
               R$ '.number_format($acao->vlrpagamentorp, 2, ',', '.').'
            </span>
         </td>';
        }
      $result .= '</tr>';
      }

      return $result;
   }
}
