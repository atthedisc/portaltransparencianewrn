<?php

namespace App\Components\Rowdespesa;

use Illuminate\Support\Facades\Crypt;

class Rowunidade
{
//Função que monta a linha de resultado da busca na tabela
// $config é composto por:
// "dado" => Dados que serão utilizados para a montagem da linha,
// "request" => Request original,
// 'buscar_por' => Buscar por (utilizado para montar a row de acordo com o padrão dynamic row),
// 'busca_parametro' => Parametro de busca (utilizado para montar a row de acordo com o padrão dynamic row),
// 'reset' => Identifica se o request é do tipo RESET, onde é recarregado o estado original da página
   public function generaterow(object $config)
   {
      //recebe como parametro a query faz a montagem da row, a cada elemento de unidade gestora (pode ser mais de um) e mantem suas subfunçoes
      $result = '';
      if ($config->request->fase == 'vportal_notapagamento_gastosdiretos') {
        foreach ($config->dado->unidadegestora as $key => $unidade){
            if($config->request->dynamic != 'true') {
                $result .= '<tr data-key="'.$key.'" data-fetch="false" data-codigo='."'".'{"codunidadegestora": '.$unidade->codunidadegestora.''.($config->reset?"":',"'.$config->buscar_por.'": "'.$config->busca_parametro.'"').'}'."'".' class="treegrid-'.($config->request->key ? $config->request->key : $key).'">
                                <td class="px-6 py-4 whitespace-nowrap"> '.$unidade->txtdescricaounidade.'</td>
                                <td class="px-6 py-4 whitespace-nowrap"></td>
                                <td class="px-6 py-4 whitespace-nowrap">
                                <span
                                    class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-yellow-100 text-yellow-800"
                                    name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                    R$ '.number_format($unidade->vlrpagamento, 2, ',', '.').'
                                </span>
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap text-right">
                                <span
                                    class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800"
                                    name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                    R$ '.number_format($unidade->vlrpagamentorp, 2, ',', '.').'
                                </span>
                                </td>
                            </tr>
                            <tr class="treegrid-parent-'.($config->request->key ? $config->request->key : $key).' bg-gray-100">
                                <th class="px-6 py-3 text-left text-sm font-semibold text-gray-800 uppercase tracking-wider"
                                name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                Gestão
                                </th>
                                <th class="px-6 py-3 text-left text-sm font-semibold text-gray-800 uppercase tracking-wider"
                                name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                </th>
                                <th class="px-6 py-3 text-left text-sm font-semibold text-gray-800 uppercase tracking-wider"
                                name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">Exercício Corrente
                                </th>
                                <th class="px-6 py-3 text-right text-sm font-semibold text-gray-800 uppercase tracking-wider"
                                name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">Restos a Pagar
                                </th>
                            </tr>';
            }
            else { // caso seja da dynamic page, entrega somente o conteudo (sem a primeira row)
                foreach ($config->dado->gestao->where('codunidadegestora',$unidade->codunidadegestora) as $g) {
                    $result .= '<tr class="treegrid-'.($config->request->key ? $config->request->key : $key).'-'.$g->codgestao.' treegrid-parent-'.($config->request->key ? $config->request->key : $key).'">
                                    <td class="px-6 py-4"> '.$g->txtdescricaogestao.'</td>
                                    <td class="px-6 py-4"></td>
                                    <td class="px-6 py-4">
                                    <span
                                        class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-yellow-100 text-yellow-800"
                                        name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                        R$ '.number_format($g->vlrpagamento, 2, ',', '.').'
                                    </span>
                                    </td>
                                    <td class="px-6 py-4 text-right">
                                    <span
                                        class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800"
                                        name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                        R$ '.number_format($g->vlrpagamentorp, 2, ',', '.').'
                                    </span>
                                    </td>
                                </tr>
                                <tr class="treegrid-parent-'.($config->request->key ? $config->request->key : $key).'-'.$g->codgestao.' bg-gray-100">
                                    <th class="px-6 py-3 text-left text-sm font-semibold text-gray-800 uppercase tracking-wider"
                                    name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                    CNPJ/CPF/IG
                                    </th>
                                    <th class="px-6 py-3 text-left text-sm font-semibold text-gray-800 uppercase tracking-wider"
                                    name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">Favorecido
                                    </th>
                                    <th class="px-6 py-3 text-left text-sm font-semibold text-gray-800 uppercase tracking-wider"
                                    name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">Exercício Corrente
                                    </th>
                                    <th class="px-6 py-3 text-right text-sm font-semibold text-gray-800 uppercase tracking-wider"
                                    name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">Restos a Pagar
                                    </th>
                                </tr>';
                                foreach ($config->dado->favorecidos->where('codunidadegestora',$unidade->codunidadegestora)->where('codgestao',$g->codgestao) as $favorecido){
                                    $result .= '<tr class="treegrid-'.($config->request->key ? $config->request->key : $key).'-'.$favorecido->codgestao.'-'.$favorecido->codcredor.' treegrid-parent-'.($config->request->key ? $config->request->key : $key).'-'.$favorecido->codgestao.'">
                                                    <td class="py-4">
                                                    <a class="inline-block" href="'.route('gastos-diretos-detalhada',
                                                    ['request' => Crypt::encrypt([
                                                        'codgrupodespesa' => 'codgrupodespesa',
                                                        'codelementodespesa' => 'codelementodespesa',
                                                        'codunidadegestora' => $favorecido->codunidadegestora,
                                                        'codgestao' => $favorecido->codgestao,
                                                        'codcredor' => $favorecido->codcredor,
                                                        'codacao' => 'codacao',
                                                        'codfonterecurso' => 'codfonterecurso',
                                                        'txtdescricaofuncao' => 'txtdescricaofuncao',
                                                        'codsubfuncao' => 'codsubfuncao',
                                                        'fase' => $config->request->fase,
                                                        'mes' => $config->request->mes,
                                                        'ano' => $config->request->ano,
                                                        'classificacao' => $config->request->classificacao,
                                                        'posicao' => $config->request->posicao,
                                                        'txtdescricaoacao' => 'txtdescricaoacao'
                                                    ])]).'">
                                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 text-lightBlue-500"
                                                            viewBox="0 0 20 20" fill="currentColor">
                                                            <path
                                                                d="M11 3a1 1 0 100 2h2.586l-6.293 6.293a1 1 0 101.414 1.414L15 6.414V9a1 1 0 102 0V4a1 1 0 00-1-1h-5z" />
                                                            <path
                                                                d="M5 5a2 2 0 00-2 2v8a2 2 0 002 2h8a2 2 0 002-2v-3a1 1 0 10-2 0v3H5V7h3a1 1 0 000-2H5z" />
                                                        </svg>
                                                    </a>';
                                                    if (strlen($favorecido->codcredor) == 11)
                                                    {
                                                        $result .= "###.". substr($favorecido->codcredor, 3, 3) . "." . substr($favorecido->codcredor, 6, 3) ."-##";
                                                    }
                                                    else
                                                    {
                                                        $result .= $favorecido->codcredor;
                                                    }
                                                    
                                        $result .= '</td>
                                                    </td>
                                                    <td class="px-6 py-4">'.$favorecido->txtnomecredor.'</td>
                                                    <td class="px-6 py-4">
                                                    <span
                                                        class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-yellow-100 text-yellow-800">
                                                        R$ '.number_format($favorecido->vlrpagamento, 2, ',', '.').'
                                                    </span>
                                                    </td>
                                                    <td class="px-6 py-4 text-right">
                                                    <span
                                                        class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800"
                                                        name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                                        R$ '.number_format($favorecido->vlrpagamentorp, 2, ',', '.').'
                                                    </span>
                                                    </td>
                                                </tr>';
                                }
                }
            }
        }
    } else {
        foreach ($config->dado->unidadegestora as $key => $unidade){
            if($config->request->dynamic != 'true') {
            $result.= '<tr data-key="'.$key.'" data-fetch="false" data-codigo='."'".'{"codunidadegestora": '.$unidade->codunidadegestora.''.($config->reset?"":',"'.$config->buscar_por.'": "'.$config->busca_parametro.'"').'}'."'".' class="treegrid-'.($config->request->key ? $config->request->key : $key).'">
                            <td class="px-6 py-4 whitespace-nowrap"> '.$unidade->txtdescricaounidade.'</td>
                            <td class="px-6 py-4 whitespace-nowrap"></td>
                            <td class="px-6 py-4 whitespace-nowrap text-right">
                            <span
                                class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800"
                                name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                R$ '.number_format($unidade->vlrpagamento, 2, ',', '.').'
                            </span>
                            </td>
                            <tr class="treegrid-parent-'.($config->request->key ? $config->request->key : $key).' bg-gray-100">
                                <th class="px-6 py-3 text-left text-sm font-semibold text-gray-800 uppercase tracking-wider"
                                name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                GESTÃO
                                </th>
                                <th class="px-6 py-3 text-left text-sm font-semibold text-gray-800 uppercase tracking-wider"
                                name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                </th>
                                <th class="px-6 py-3 text-right text-sm font-semibold text-gray-800 uppercase tracking-wider"
                                name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">Valor (R$)
                                </th>
                            </tr>
                        </tr>';
            }
            else { // caso seja da dynamic page, entrega somente o conteudo (sem a primeira row)
                foreach ($config->dado->gestao->where('codunidadegestora',$unidade->codunidadegestora) as $g) {
                    $result .= '<tr class="treegrid-'.($config->request->key ? $config->request->key : $key).'-'.$g->codgestao.' treegrid-parent-'.($config->request->key ? $config->request->key : $key).'">
                                    <td class="px-6 py-4"> '.$g->txtdescricaogestao.'
                                    </td>
                                    <td class="px-6 py-4"></td>
                                    <td class="px-6 py-4 text-right">
                                    <span
                                        class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800"
                                        name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                        R$ '.number_format($g->vlrpagamento, 2, ',', '.').'
                                    </span>
                                    </td>
                                    <tr class="treegrid-parent-'.($config->request->key ? $config->request->key : $key).'-'.$g->codgestao.' bg-gray-100">
                                        <th class="px-6 py-3 text-left text-sm font-semibold text-gray-800 uppercase tracking-wider"
                                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                        CNPJ/CPF/IG
                                        </th>
                                        <th class="px-6 py-3 text-left text-sm font-semibold text-gray-800 uppercase tracking-wider"
                                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">Favorecido
                                        </th>
                                        <th class="px-6 py-3 text-right text-sm font-semibold text-gray-800 uppercase tracking-wider"
                                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">Valor (R$)
                                        </th>
                                    </tr>
                                </tr>';
                                foreach ($config->dado->favorecidos->where('codunidadegestora',$unidade->codunidadegestora)->where('codgestao',$g->codgestao) as $favorecido){
                                    $result .= '<tr class="treegrid-'.($config->request->key ? $config->request->key : $key).'-'.$favorecido->codgestao.'-'.$favorecido->codcredor.' treegrid-parent-'.($config->request->key ? $config->request->key : $key).'-'.$favorecido->codgestao.'">
                                                    <td class="py-4">
                                                    <a class="inline-block" href="'.route('gastos-diretos-detalhada',
                                                    ['request' => Crypt::encrypt([
                                                        'codgrupodespesa' => 'codgrupodespesa',
                                                        'codelementodespesa' => 'codelementodespesa',
                                                        'codunidadegestora' => $favorecido->codunidadegestora,
                                                        'codgestao' => $favorecido->codgestao,
                                                        'codcredor' => $favorecido->codcredor,
                                                        'codacao' => 'codacao',
                                                        'codfonterecurso' => 'codfonterecurso',
                                                        'txtdescricaofuncao' => 'txtdescricaofuncao',
                                                        'codsubfuncao' => 'codsubfuncao',
                                                        'fase' => $config->request->fase,
                                                        'mes' => $config->request->mes,
                                                        'ano' => $config->request->ano,
                                                        'classificacao' => $config->request->classificacao,
                                                        'posicao' => $config->request->posicao,
                                                        'txtdescricaoacao' => 'txtdescricaoacao'
                                                    ])]).'">
                                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 text-lightBlue-500"
                                                            viewBox="0 0 20 20" fill="currentColor">
                                                            <path
                                                                d="M11 3a1 1 0 100 2h2.586l-6.293 6.293a1 1 0 101.414 1.414L15 6.414V9a1 1 0 102 0V4a1 1 0 00-1-1h-5z" />
                                                            <path
                                                                d="M5 5a2 2 0 00-2 2v8a2 2 0 002 2h8a2 2 0 002-2v-3a1 1 0 10-2 0v3H5V7h3a1 1 0 000-2H5z" />
                                                        </svg>
                                                    </a>';
                                                    if (strlen($favorecido->codcredor) == 11)
                                                    {
                                                        $result .= "###.". substr($favorecido->codcredor, 3, 3) . "." . substr($favorecido->codcredor, 6, 3) ."-##";
                                                    }
                                                    else
                                                    {
                                                        $result .= $favorecido->codcredor;
                                                    }
                                        $result .= '</td>
                                                    <td class="px-6 py-4">'.$favorecido->txtnomecredor.'</td>
                                                    <td class="px-6 py-4 text-right">
                                                    <span
                                                        class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800"
                                                        name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                                        R$ '.number_format($favorecido->vlrpagamento, 2, ',', '.').'
                                                    </span>
                                                    </td>
                                                </tr>';
                                }
                }
            }
        }
    }
      return $result;
   }
}
