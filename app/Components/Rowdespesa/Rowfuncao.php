<?php

namespace App\Components\Rowdespesa;

use Illuminate\Support\Facades\Crypt;

class Rowfuncao
{
//Função que monta a linha de resultado da busca na tabela
// $config é composto por:
// "dado" => Dados que serão utilizados para a montagem da linha,
// "request" => Request original,
// 'buscar_por' => Buscar por (utilizado para montar a row de acordo com o padrão dynamic row),
// 'busca_parametro' => Parametro de busca (utilizado para montar a row de acordo com o padrão dynamic row),
// 'reset' => Identifica se o request é do tipo RESET, onde é recarregado o estado original da página
   public function generaterow(object $config)
   {
      //recebe como parametro a query faz a montagem da row, a cada elemento de unidade gestora (pode ser mais de um) e mantem suas subfunçoes
      $result = '';
      if ($config->request->fase == 'vportal_notapagamento_gastosdiretos') {
        foreach ($config->dado->funcao as $key => $f){
         if($config->request->dynamic != 'true') {
            $result .='<tr data-key="'.($config->request->key ? $config->request->key : $key).'" data-fetch="false" data-codigo='."'".'{"txtdescricaofuncao": "'.$f->txtdescricaofuncao.'"'.($config->reset?"":',"'.$config->buscar_por.'": "'.$config->busca_parametro.'"').'}'."'".' class="treegrid-'.($config->request->key ? $config->request->key : $key).'">
                            <td class="px-6 py-4 whitespace-nowrap"> '.$f->txtdescricaofuncao.'</td>
                            <td class="px-6 py-4 whitespace-nowrap">
                            <span
                                class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-yellow-100 text-yellow-800"
                                name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                R$ '.number_format($f->vlrpagamento, 2, ',', '.').'
                            </span>
                            </td>
                            <td class="px-6 py-4 whitespace-nowrap text-right">
                            <span
                                class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800"
                                name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                R$ '.number_format($f->vlrpagamentorp, 2, ',', '.').'
                            </span>
                            </td>
                        </tr>
                        <tr class="treegrid-parent-'.($config->request->key ? $config->request->key : $key).' bg-gray-100">
                            <th class="px-6 py-3 text-left text-sm font-semibold text-gray-800 uppercase tracking-wider"
                            name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                            Subfunção</th>
                            <th class="px-6 py-3 text-left text-sm font-semibold text-gray-800 uppercase tracking-wider"
                            name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                            Exercício Corrente</th>
                            <th class="px-6 py-3 text-right text-sm font-semibold text-gray-800 uppercase tracking-wider"
                            name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">Restos
                            a Pagar</th>
                        </tr>';
         } else {
            foreach ($config->dado->subfuncao->where('txtdescricaofuncao', $f->txtdescricaofuncao) as $sub) {
               $result .= '<tr class="treegrid-'.($config->request->key ? $config->request->key : $key).'-'.$sub->codsubfuncao.' treegrid-parent-'.($config->request->key ? $config->request->key : $key).'">
                                <td class="px-6 py-4"> '.$sub->txtdescricaosubfuncao.'</td>
                                <td class="px-6 py-4">
                                    <span
                                        class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-yellow-100 text-yellow-800"
                                        name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                        R$ '.number_format($sub->vlrpagamento, 2, ',', '.').'
                                    </span>
                                </td>
                                <td class="px-6 py-4 text-right">
                                    <span
                                        class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800"
                                        name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                        R$ '.number_format($sub->vlrpagamentorp, 2, ',', '.').'
                                    </span>
                                </td>
                                </tr>
                                <tr class="treegrid-parent-'.($config->request->key ? $config->request->key : $key).'-'.$sub->codsubfuncao.' bg-gray-100">
                                <th class="px-6 py-3 text-left text-sm font-semibold text-gray-800 uppercase tracking-wider"
                                    name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Ação</th>
                                <th class="px-6 py-3 text-left text-sm font-semibold text-gray-800 uppercase tracking-wider"
                                    name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Exercício Corrente</th>
                                <th class="px-6 py-3 text-right text-sm font-semibold text-gray-800 uppercase tracking-wider"
                                    name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">Restos
                                    a Pagar</th>
                            </tr>';
           foreach ($config->dado->acao->where('txtdescricaofuncao', $f->txtdescricaofuncao)->where('codsubfuncao', $sub->codsubfuncao) as $a){
               $result .= '<tr class="treegrid-'.($config->request->key ? $config->request->key : $key).'-'.$a->codsubfuncao.'-'.$a->codacao.' treegrid-parent-'.($config->request->key ? $config->request->key : $key).'-'.$a->codsubfuncao.'">
                                <td class="flex flex-wrap py-4">
                                    <a class="inline-block" href="'.route('gastos-diretos-detalhada',
                                    ['request' => Crypt::encrypt([
                                        'codgrupodespesa' => 'codgrupodespesa',
                                        'codelementodespesa' => 'codelementodespesa',
                                        'codunidadegestora' => 'codunidadegestora',
                                        'codgestao' => 'codgestao',
                                        'codcredor' => 'codcredor',
                                        'codacao' => $a->codacao,
                                        'codfonterecurso' => 'codfonterecurso',
                                        'txtdescricaofuncao' => $a->txtdescricaofuncao,
                                        'codsubfuncao' => $a->codsubfuncao,
                                        'fase' => $config->request->fase,
                                        'mes' => $config->request->mes,
                                        'ano' => $config->request->ano,
                                        'classificacao' => $config->request->classificacao,
                                        'posicao' => $config->request->posicao,
                                        'txtdescricaoacao' => $a->txtdescricaoacao
                                    ])]).'">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 text-lightBlue-500"
                                            viewBox="0 0 20 20" fill="currentColor">
                                            <path
                                            d="M11 3a1 1 0 100 2h2.586l-6.293 6.293a1 1 0 101.414 1.414L15 6.414V9a1 1 0 102 0V4a1 1 0 00-1-1h-5z" />
                                            <path
                                            d="M5 5a2 2 0 00-2 2v8a2 2 0 002 2h8a2 2 0 002-2v-3a1 1 0 10-2 0v3H5V7h3a1 1 0 000-2H5z" />
                                        </svg>
                                    </a>'.$a->txtdescricaoacao.'
                                </td>
                                <td class="px-6 py-4">
                                    <span
                                        class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-yellow-100 text-yellow-800"
                                        name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                        R$ '.number_format($a->vlrpagamento, 2, ',', '.').'
                                    </span>
                                </td>
                                <td class="px-6 py-4 text-right">
                                    <span
                                        class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800"
                                        name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                        R$ '.number_format($a->vlrpagamentorp, 2, ',', '.').'
                                    </span>
                                </td>
                            </tr>';
                       }
                 }
            }
        }
    } else {
            foreach ($config->dado->funcao as $key => $f){
            if($config->request->dynamic != 'true') {
                $result .= '<tr data-key="'.($config->request->key ? $config->request->key : $key).'" data-fetch="false" data-codigo='."'".'{"txtdescricaofuncao": "'.$f->txtdescricaofuncao.'"'.($config->reset?"":',"'.$config->buscar_por.'": "'.$config->busca_parametro.'"').'}'."'".' class="treegrid-'.($config->request->key ? $config->request->key : $key).'">
                                <td class="px-6 py-4 whitespace-nowrap"> '.$f->txtdescricaofuncao.'</td>
                                <td class="px-6 py-4 whitespace-nowrap text-right">
                                <span
                                    class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800"
                                    name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                    R$ '.number_format($f->vlrpagamento, 2, ',', '.').'
                                </span>
                                </td>
                                <tr class="treegrid-parent-'.($config->request->key ? $config->request->key : $key).' bg-gray-100">
                                    <th class="px-6 py-3 text-left text-sm font-semibold text-gray-800 uppercase tracking-wider"
                                    name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Subfunção
                                    </th>
                                    <th class="px-6 py-3 text-right text-sm font-semibold text-gray-800 uppercase tracking-wider"
                                    name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">Valor
                                    (R$)
                                    </th>
                                </tr>
                            </tr>';
            } else {
                foreach ($config->dado->subfuncao->where('txtdescricaofuncao', $f->txtdescricaofuncao) as $sub) {
                $result .= '<tr class="treegrid-'.($config->request->key ? $config->request->key : $key).'-'.$sub->codsubfuncao.' treegrid-parent-'.($config->request->key ? $config->request->key : $key).'">
                                    <td class="px-6 py-4"> '.$sub->txtdescricaosubfuncao.'</td>
                                    <td class="px-6 py-4 text-right">
                                        <span
                                            class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800"
                                            name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                            R$ '.number_format($sub->vlrpagamento, 2, ',', '.').'
                                        </span>
                                    </td>
                                        <tr class="treegrid-parent-'.($config->request->key ? $config->request->key : $key).'-'.$sub->codsubfuncao.' bg-gray-100">
                                        <th class="px-6 py-3 text-left text-sm font-semibold text-gray-800 uppercase tracking-wider"
                                            name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                            AÇÃO
                                        </th>
                                        <th class="px-6 py-3 text-right text-sm font-semibold text-gray-800 uppercase tracking-wider"
                                            name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">Valor (R$)
                                        </th>
                                        </tr>
                                    </tr>';
                foreach ($config->dado->acao->where('txtdescricaofuncao', $f->txtdescricaofuncao)->where('codsubfuncao', $sub->codsubfuncao) as $a){
                $result .= '<tr class="treegrid-'.($config->request->key ? $config->request->key : $key).'-'.$a->codsubfuncao.'-'.$a->codacao.' treegrid-parent-'.($config->request->key ? $config->request->key : $key).'-'.$a->codsubfuncao.'">
                                <td class="py-4">
                                    <a class="inline-block" href="'.route('gastos-diretos-detalhada',
                                    ['request' => Crypt::encrypt([
                                    'codgrupodespesa' => 'codgrupodespesa',
                                    'codelementodespesa' => 'codelementodespesa',
                                    'codunidadegestora' => 'codunidadegestora',
                                    'codgestao' => 'codgestao',
                                    'codcredor' => 'codcredor',
                                    'codacao' => $a->codacao,
                                    'codfonterecurso' => 'codfonterecurso',
                                    'txtdescricaofuncao' => $a->txtdescricaofuncao,
                                    'codsubfuncao' => $a->codsubfuncao,
                                    'fase' => $config->request->fase,
                                    'mes' => $config->request->mes,
                                    'ano' => $config->request->ano,
                                    'classificacao' => $config->request->classificacao,
                                    'posicao' => $config->request->posicao,
                                    'txtdescricaoacao' => $a->txtdescricaoacao
                                    ])]).'">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 text-lightBlue-500"
                                        viewBox="0 0 20 20" fill="currentColor">
                                        <path
                                            d="M11 3a1 1 0 100 2h2.586l-6.293 6.293a1 1 0 101.414 1.414L15 6.414V9a1 1 0 102 0V4a1 1 0 00-1-1h-5z" />
                                        <path
                                            d="M5 5a2 2 0 00-2 2v8a2 2 0 002 2h8a2 2 0 002-2v-3a1 1 0 10-2 0v3H5V7h3a1 1 0 000-2H5z" />
                                    </svg>
                                    </a>
                                    '.$a->txtdescricaoacao.'
                                </td>
                                <td class="px-6 py-4 text-right">
                                    <span
                                    class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800"
                                    name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                    R$ '.number_format($a->vlrpagamento, 2, ',', '.').'
                                    </span>
                                </td>
                            </tr>';
                        }
                    }
                }
            }
        }
        return $result;
    }
}