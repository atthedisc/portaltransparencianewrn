<?php

namespace App\Components\Rowreceita;

class Rowunidade
{
//Função que monta a linha de resultado da busca na tabela
// $config é composto por:
// "dado" => Dados que serão utilizados para a montagem da linha,
// "request" => Request original,
// 'buscar_por' => Buscar por (utilizado para montar a row de acordo com o padrão dynamic row),
// 'busca_parametro' => Parametro de busca (utilizado para montar a row de acordo com o padrão dynamic row),
// 'reset' => Identifica se o request é do tipo RESET, onde é recarregado o estado original da página
   public function generaterow(object $config)
   {
      //recebe como parametro a query faz a montagem da row, a cada elemento de unidade gestora (pode ser mais de um) e mantem suas subfunçoes
      $result = '';
      foreach ($config->dado->unidadegestora as $key => $unidade) {
         if($config->request->dynamic != 'true') {
            $result.= '<tr data-key="'.($config->request->key ? $config->request->key : $key).'" data-fetch="false" data-codigo='."'".'{"codunidade": '.$unidade->codunidade.''.($config->reset?"":',"'.$config->buscar_por.'": "'.$config->busca_parametro.'"').'}'."'".' class="treegrid-'.($config->request->key ? $config->request->key : $key).'">
            <td class="px-4 py-4 w-4/6"> ';
    
        $result.= '</td>
                    <td class="pr-12 text-right">
                        <div class="text-blue-600 font-semibold text-xs leading-loose">
                            R$ ' . $unidade->valor_previsto . ' 
                        </div>
                    </td>
                    <td class="py-4 text-center">
                        <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800"
                          name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                        R$ '.number_format($unidade->valorreceita, 2, ',', '.').'
                        </span>
                    </td>
                    <td class="pr-5 py-4 text-right"> 
                        <div class="text-blue-600 font-semibold text-xs leading-loose">
                            '. $unidade->porcentagem_formatada .'%
                        </div>                                 
                    </td>
                </tr>
                <tr class="treegrid-parent-'.($config->request->key ? $config->request->key : $key).'"></tr>';
    } else {
               foreach ($config->dado->gestao->where('codunidade',$unidade->codunidade) as $g) {
               $result.= '<tr class="treegrid-'.($config->request->key ? $config->request->key : $key).'-'.$g->codgestao .' treegrid-parent-'.($config->request->key ? $config->request->key : $key).'">
                           <td class="px-6 py-4"> '.$g->txtdescricaogestao .'</td>
                           <td class="pr-12 text-right">
                              <div class="text-blue-600 font-semibold text-xs leading-loose">
                                    R$ ' . $g->valor_previsto . ' 
                                 </div>
                           </td>
                           <td class="py-4 text-center">
                           <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800"
                           name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                            R$ '.number_format($g->valorreceita, 2, ',', '.').'
                           </td>
                           <td class="pr-5 py-4 text-right"> 
                                 <div class="text-blue-600 font-semibold text-xs leading-loose">
                                    '. $g->porcentagem_formatada .'%
                                 </div>                                 
                           </td>
                        </tr>';
                        foreach ($config->dado->categorias->where('codunidade',$unidade->codunidade)->where('codgestao',$g->codgestao) as $categoria) {
                           $result.= '<tr class="treegrid-'.($config->request->key ? $config->request->key : $key).'-'. $categoria->codgestao.'-'. $categoria->codcategoria.' treegrid-parent-'.($config->request->key ? $config->request->key : $key).'-'. $categoria->codgestao.'">
                                       <td class="px-6 py-4"> '. $categoria->txtdescricaocategoria.'</td>
                                       <td class="pr-12 text-right">
                                          <div class="text-blue-600 font-semibold text-xs leading-loose">
                                             R$ ' . $categoria->valor_previsto . ' 
                                          </div>
                                       </td>
                                       <td class="py-4 text-center">
                                          <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800"
                                          name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                             R$ '.number_format($categoria->valorreceita, 2, ',', '.').'
                                       </td>
                                       <td class="pr-5 py-4 text-right"> 
                                          <div class="text-blue-600 font-semibold text-xs leading-loose">
                                             '. $categoria->porcentagem_formatada .'%
                                          </div>                                 
                                       </td>
                                    </tr>';
                                    foreach ($config->dado->origens->where('codunidade',$unidade->codunidade)->where('codgestao',$g->codgestao)->where('codcategoria',$categoria->codcategoria) as $origem) {
                                       $result.= '<tr class="treegrid-'.($config->request->key ? $config->request->key : $key).'-'.$origem->codgestao.'-'.$origem->codcategoria.'-'.$origem->codorigem.' treegrid-parent-'.($config->request->key ? $config->request->key : $key).'-'.$origem->codgestao.'-'.$origem->codcategoria.'">
                                                   <td class="px-6 py-4"> '.$origem->txtdescricaoorigem.'</td>
                                                   <td class="pr-12 text-right">
                                                      <div class="text-blue-600 font-semibold text-xs leading-loose">
                                                         R$ ' . $origem->valor_previsto . ' 
                                                      </div>
                                                   </td>
                                                   <td class="py-4 text-center">
                                                      <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800"
                                                      name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                                         R$ '.number_format($origem->valorreceita, 2, ',', '.').'
                                                   </td>
                                                   <td class="pr-5 py-4 text-right"> 
                                                      <div class="text-blue-600 font-semibold text-xs leading-loose">
                                                         '. $origem->porcentagem_formatada .'%
                                                      </div>                                 
                                                   </td>
                                                 </tr>';
                                                foreach ($config->dado->especies->where('codunidade',$unidade->codunidade)->where('codgestao',$g->codgestao)->where('codcategoria',$categoria->codcategoria)->where('codorigem',$origem->codorigem) as $especie) {
                                                   $result.= '<tr class="treegrid-'.($config->request->key ? $config->request->key : $key).'-'.$especie->codgestao.'-'.$especie->codcategoria.'-'.$especie->codorigem.'-'.$especie->codespecie.' treegrid-parent-'.($config->request->key ? $config->request->key : $key).'-'.$especie->codgestao.'-'.$especie->codcategoria.'-'.$especie->codorigem.'">
                                                               <td class="px-6 py-4"> '.$especie->txtdescricaoespecie.'</td>
                                                               <td class="pr-12 text-right">
                                                                  <div class="text-blue-600 font-semibold text-xs leading-loose">
                                                                     R$ ' . $especie->valor_previsto . ' 
                                                                  </div>
                                                               </td>
                                                               <td class="py-4 text-center">
                                                                  <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800"
                                                                  name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                                                        R$ '.number_format($especie->valorreceita, 2, ',', '.').'
                                                               </td>
                                                               <td class="pr-5 py-4 text-right"> 
                                                                  <div class="text-blue-600 font-semibold text-xs leading-loose">
                                                                     '. $especie->porcentagem_formatada .'%
                                                                  </div>                                 
                                                               </td>
                                                            </tr>';
                                                            foreach ($config->dado->rubricas->where('codunidade',$unidade->codunidade)->where('codgestao',$g->codgestao)->where('codcategoria',$categoria->codcategoria)->where('codorigem',$origem->codorigem)->where('codespecie',$especie->codespecie) as $rubrica) {
                                                               $result.= '<tr class="treegrid-'.($config->request->key ? $config->request->key : $key).'-'.$rubrica->codgestao.'-'.$rubrica->codcategoria.'-'.$rubrica->codorigem.'-'.$rubrica->codespecie.'-'.$rubrica->codrubrica.' treegrid-parent-'.($config->request->key ? $config->request->key : $key).'-'.$rubrica->codgestao.'-'.$rubrica->codcategoria.'-'.$rubrica->codorigem.'-'.$rubrica->codespecie.'">
                                                                           <td class="px-6 py-4"> '.$rubrica->txtdescricaorubrica.'</td>
                                                                           <td class="pr-12 text-right">
                                                                              <div class="text-blue-600 font-semibold text-xs leading-loose">
                                                                                 R$ ' . $rubrica->valor_previsto . ' 
                                                                              </div>
                                                                           </td>
                                                                           <td class="px-6 py-4 text-center">
                                                                              <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800"
                                                                              name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                                                                    R$ '.number_format($rubrica->valorreceita, 2, ',', '.').'
                                                                           </td>
                                                                           <td class="pr-5 py-4 text-right"> 
                                                                              <div class="text-blue-600 font-semibold text-xs leading-loose">
                                                                                 '. $rubrica->porcentagem_formatada .'%
                                                                              </div>                                 
                                                                           </td>
                                                                        </tr>';
                                                                        foreach ($config->dado->alineas->where('codunidade',$unidade->codunidade)->where('codgestao',$g->codgestao)->where('codcategoria',$categoria->codcategoria)->where('codorigem',$origem->codorigem)->where('codespecie',$especie->codespecie)->where('codrubrica',$rubrica->codrubrica) as $alinea) {
                                                                           $result.= '<tr class="treegrid-'.($config->request->key ? $config->request->key : $key).'-'.$alinea->codgestao.'-'.$alinea->codcategoria.'-'.$alinea->codorigem.'-'.$alinea->codespecie.'-'.$alinea->codrubrica.'-'.$alinea->codalinea.' treegrid-parent-'.($config->request->key ? $config->request->key : $key).'-'.$alinea->codgestao.'-'.$alinea->codcategoria.'-'.$alinea->codorigem.'-'.$alinea->codespecie.'-'.$alinea->codrubrica.'">
                                                                                       <td class="px-6 py-4"> '.$alinea->txtdescricaoalinea.'</td>
                                                                                       <td class="pr-12 text-right">
                                                                                          <div class="text-blue-600 font-semibold text-xs leading-loose">
                                                                                             R$ ' . $alinea->valor_previsto . ' 
                                                                                          </div>
                                                                                       </td>
                                                                                       <td class="px-6 py-4 text-center">
                                                                                          <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800"
                                                                                          name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                                                                                R$ '.number_format($alinea->valorreceita, 2, ',', '.').'
                                                                                       </td>
                                                                                       <td class="pr-5 py-4 text-right"> 
                                                                                          <div class="text-blue-600 font-semibold text-xs leading-loose">
                                                                                             '. $alinea->porcentagem_formatada .'%
                                                                                          </div>                                 
                                                                                       </td>
                                                                                    </tr>';
                                                                                    foreach ($config->dado->detalhamento->where('codunidade',$unidade->codunidade)->where('codgestao',$g->codgestao)->where('codcategoria',$categoria->codcategoria)->where('codorigem',$origem->codorigem)->where('codespecie',$especie->codespecie)->where('codrubrica',$rubrica->codrubrica)->where('codalinea',$alinea->codalinea) as $id => $detalhes) {
                                                                                       $result.= '<tr class="treegrid-'.($config->request->key ? $config->request->key : $key).'-'.$detalhes->codgestao.'-'.$detalhes->codcategoria.'-'.$detalhes->codorigem.'-'.$detalhes->codespecie.'-'.$detalhes->codrubrica.'-'.$detalhes->codalinea.'-detalhes treegrid-parent-'.($config->request->key ? $config->request->key : $key).'-'.$detalhes->codgestao.'-'.$detalhes->codcategoria.'-'.$detalhes->codorigem.'-'.$detalhes->codespecie.'-'.$detalhes->codrubrica.'-'.$detalhes->codalinea.'">
                                                                                                   <td class="px-6 py-4"> '.$detalhes->txtdescricaodetalhada.'</td>
                                                                                                   <td class="pr-12 text-right">
                                                                                                      <div class="text-blue-600 font-semibold text-xs leading-loose">
                                                                                                         R$ ' . $detalhes->valor_previsto . ' 
                                                                                                      </div>
                                                                                                   </td>
                                                                                                   <td class="py-4 text-center">
                                                                                                      <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800"
                                                                                                      name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                                                                                         R$ '.number_format($detalhes->valorreceita, 2, ',', '.').'
                                                                                                   </td>
                                                                                                   <td class="pr-5 py-4 text-right"> 
                                                                                                      <div class="text-blue-600 font-semibold text-xs leading-loose">
                                                                                                         '. $detalhes->porcentagem_formatada .'%
                                                                                                      </div>                                 
                                                                                                   </td>
                                                                                                </tr>';
                                                                                    }
                                                                        }
                                                            }
                                                }
                                    }
                        }
            }
         }            
      }
      return $result;
   }
}
