<?php

namespace App\Components\Despesas;

use App\Models\GastosDireto\LiquidacaoGastosDiretos;
use App\Models\GastosDireto\NotaEmpenhoGastosDiretos;
use App\Models\GastosDireto\NotaPagamentoGastosDiretos;
use Illuminate\Support\Facades\DB;

class Favorecido
{
   //Faz a query de todos os dados necessarios para montar a row de acordo com as seleçoes da busca
    public function query(object $config)
    {
        if ($config->fase == 'vportal_notapagamento_gastosdiretos') {
            $model = NotaPagamentoGastosDiretos::class;
         } else if ($config->fase == 'vportal_notaempenho_gastosdiretos') {
            $model = NotaEmpenhoGastosDiretos::class;
         } else {
            $model = LiquidacaoGastosDiretos::class;
         }

         if ($config->fase == 'vportal_notapagamento_gastosdiretos') {
            $favorecidos = $model::select('codcredor', 'txtnomecredor', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
               ->whereRaw($config->where)
               ->groupBy('codcredor', 'txtnomecredor')
               ->orderBy('txtnomecredor')
               ->paginate(10);
            if(property_exists($config,'request')) {
               if($config->request->dynamic == 'true') {
                  $grupodespesa = $model::select('txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'codgrupodespesa', 'codelementodespesa', 'codcredor', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
                  ->whereRaw($config->where)
                  ->groupBy('txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'codgrupodespesa', 'codelementodespesa', 'codcredor')
                  ->get();
   
                  $unidadegestora = $model::select('txtdescricaounidade', 'codunidadegestora', 'codcredor', 'codgrupodespesa', 'codelementodespesa', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
                     ->whereRaw($config->where)
                     ->groupBy('txtdescricaounidade', 'codunidadegestora', 'codcredor', 'codgrupodespesa', 'codelementodespesa')
                     ->get();
                  return (object) [
                     'favorecidos' => $favorecidos,
                     'grupodespesa' => $grupodespesa,
                     'unidadegestora' => $unidadegestora,
                  ];
               }
            }
         } else {
            // Empenho e Liquidação
            $favorecidos = $model::select('codcredor', 'txtnomecredor', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
               ->whereRaw($config->where)
               ->groupBy('codcredor', 'txtnomecredor')
               ->orderBy('txtnomecredor')
               ->paginate(10);
            if(property_exists($config,'request')) {
               if($config->request->dynamic == 'true') {
                  $grupodespesa = $model::select('txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'codgrupodespesa', 'codelementodespesa', 'codcredor', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
                  ->whereRaw($config->where)
                  ->groupBy('txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'codgrupodespesa', 'codelementodespesa', 'codcredor')
                  ->get();
   
                  $unidadegestora = $model::select('txtdescricaounidade', 'codunidadegestora', 'codcredor', 'codgrupodespesa', 'codelementodespesa', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
                     ->whereRaw($config->where)
                     ->groupBy('txtdescricaounidade', 'codunidadegestora', 'codcredor', 'codgrupodespesa', 'codelementodespesa')
                     ->get();
                  return (object) [
                     'favorecidos' => $favorecidos,
                     'grupodespesa' => $grupodespesa,
                     'unidadegestora' => $unidadegestora,
                  ];
               }
            }
         }
         // Retornar as consultas para a página
         return (object) [
            'favorecidos' => $favorecidos,
         ];
    }
}