<?php

namespace App\Components\Despesas;

use App\Models\GastosDireto\LiquidacaoGastosDiretos;
use App\Models\GastosDireto\NotaEmpenhoGastosDiretos;
use App\Models\GastosDireto\NotaPagamentoGastosDiretos;
use Illuminate\Support\Facades\DB;

class Fonte
{
   //Faz a query de todos os dados necessarios para montar a row de acordo com as seleçoes da busca
    public function query(object $config)
    {
        if ($config->fase == 'vportal_notapagamento_gastosdiretos') {
            $model = NotaPagamentoGastosDiretos::class;
         } else if ($config->fase == 'vportal_notaempenho_gastosdiretos') {
            $model = NotaEmpenhoGastosDiretos::class;
         } else {
            $model = LiquidacaoGastosDiretos::class;
         }

         if ($config->fase == 'vportal_notapagamento_gastosdiretos') {
            $fonte = $model::select('codfonterecurso', 'txtdescricaofonterecurso', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
               ->whereRaw($config->where)
               ->groupBy('codfonterecurso', 'txtdescricaofonterecurso')
               ->orderBy('txtdescricaofonterecurso')
               ->paginate(10);

            if(property_exists($config,'request')) {
               if($config->request->dynamic == 'true') {
                  $unidadegestora = $model::select('txtdescricaounidade', 'codunidadegestora', 'codfonterecurso', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
                  ->whereRaw($config->where)
                  ->groupBy('txtdescricaounidade', 'codunidadegestora', 'codfonterecurso')
                  ->get();
   
                  $gestao = $model::select('txtdescricaogestao', 'codgestao', 'codfonterecurso', 'codunidadegestora', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
                     ->whereRaw($config->where)
                     ->groupBy('txtdescricaogestao', 'codgestao', 'codfonterecurso', 'codunidadegestora')
                     ->get();
                  return (object) [
                     'fontes' => $fonte,
                     'unidadegestora' => $unidadegestora,
                     'gestao' => $gestao,
                  ];
               }
            }
         } else {
            // Empenho e Liquidação
             $fonte = $model::select('codfonterecurso', 'txtdescricaofonterecurso', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
               ->whereRaw($config->where)
               ->groupBy('codfonterecurso', 'txtdescricaofonterecurso')
               ->orderBy('txtdescricaofonterecurso')
               ->paginate(10);

               if(property_exists($config,'request')) {
                  if($config->request->dynamic == 'true') {
                     $unidadegestora = $model::select('txtdescricaounidade', 'codunidadegestora', 'codfonterecurso', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
                     ->whereRaw($config->where)
                     ->groupBy('txtdescricaounidade', 'codunidadegestora', 'codfonterecurso')
                     ->get();
      
                     $gestao = $model::select('txtdescricaogestao', 'codgestao', 'codfonterecurso', 'codunidadegestora', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
                        ->whereRaw($config->where)
                        ->groupBy('txtdescricaogestao', 'codgestao', 'codfonterecurso', 'codunidadegestora')
                        ->get();
                     return (object) [
                        'fontes' => $fonte,
                        'unidadegestora' => $unidadegestora,
                        'gestao' => $gestao,
                     ];
                  }
               }
         }
         // Retornar as consultas para a página
         return (object) [
            'fontes' => $fonte,
         ];
    }
}