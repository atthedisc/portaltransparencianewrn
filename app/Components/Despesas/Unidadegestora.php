<?php

namespace App\Components\Despesas;

use App\Models\GastosDireto\LiquidacaoGastosDiretos;
use App\Models\GastosDireto\NotaEmpenhoGastosDiretos;
use App\Models\GastosDireto\NotaPagamentoGastosDiretos;
use Illuminate\Support\Facades\DB;

class Unidadegestora
{
   //Faz a query de todos os dados necessarios para montar a row de acordo com as seleçoes da busca
    public function query(object $config)
    {
        if ($config->fase == 'vportal_notapagamento_gastosdiretos') {
            $model = NotaPagamentoGastosDiretos::class;
         } else if ($config->fase == 'vportal_notaempenho_gastosdiretos') {
            $model = NotaEmpenhoGastosDiretos::class;
         } else {
            $model = LiquidacaoGastosDiretos::class;
         }

         if ($config->fase == 'vportal_notapagamento_gastosdiretos') {
            $unidadegestora = $model::select('txtdescricaounidade', 'codunidadegestora', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
               ->whereRaw($config->where)
               ->groupBy('txtdescricaounidade', 'codunidadegestora')
               ->orderBy('txtdescricaounidade')
               ->paginate(10);

               if(property_exists($config,'request')) {
                  if($config->request->dynamic == 'true') {
                     $gestao = $model::select('txtdescricaogestao', 'codgestao', 'codunidadegestora', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
                     ->whereRaw($config->where)
                     ->groupBy('txtdescricaogestao', 'codgestao', 'codunidadegestora')
                     ->get();
      
                     $favorecidos = $model::select('codcredor', 'txtnomecredor', 'codunidadegestora', 'codgestao', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
                     ->whereRaw($config->where)
                     ->groupBy('codcredor', 'txtnomecredor', 'codunidadegestora', 'codgestao')
                     ->get();
                     return (object) [
                        'unidadegestora' => $unidadegestora,
                        'gestao' => $gestao,
                        'favorecidos' => $favorecidos,
                     ];
                  }
               }
         } else {
            // Empenho e Liquidação
            $unidadegestora = $model::select('txtdescricaounidade', 'codunidadegestora', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
               ->whereRaw($config->where)
               ->groupBy('txtdescricaounidade', 'codunidadegestora')
               ->orderBy('txtdescricaounidade')
               ->paginate(10);
            if(property_exists($config,'request')) {
               if($config->request->dynamic == 'true') {
                  $gestao = $model::select('txtdescricaogestao', 'codgestao', 'codunidadegestora', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
                  ->whereRaw($config->where)
                  ->groupBy('txtdescricaogestao', 'codgestao', 'codunidadegestora')
                  ->get();

                  $favorecidos = $model::select('codcredor', 'txtnomecredor', 'codunidadegestora', 'codgestao', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
                  ->whereRaw($config->where)
                  ->groupBy('codcredor', 'txtnomecredor', 'codunidadegestora', 'codgestao')
                  ->get();
                  return (object) [
                     'unidadegestora' => $unidadegestora,
                     'gestao' => $gestao,
                     'favorecidos' => $favorecidos,
                  ];
               }
            }
         }
         // Retornar as consultas para a página
         return (object) [
            'unidadegestora' => $unidadegestora,
         ];
    }
}