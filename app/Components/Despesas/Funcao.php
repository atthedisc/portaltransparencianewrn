<?php

namespace App\Components\Despesas;

use App\Models\GastosDireto\LiquidacaoGastosDiretos;
use App\Models\GastosDireto\NotaEmpenhoGastosDiretos;
use App\Models\GastosDireto\NotaPagamentoGastosDiretos;
use Illuminate\Support\Facades\DB;

class Funcao
{
   //Faz a query de todos os dados necessarios para montar a row de acordo com as seleçoes da busca
    public function query(object $config)
    {
        if ($config->fase == 'vportal_notapagamento_gastosdiretos') {
            $model = NotaPagamentoGastosDiretos::class;
         } else if ($config->fase == 'vportal_notaempenho_gastosdiretos') {
            $model = NotaEmpenhoGastosDiretos::class;
         } else {
            $model = LiquidacaoGastosDiretos::class;
         }

         if ($config->fase == 'vportal_notapagamento_gastosdiretos') {
            $funcao = $model::select('txtdescricaofuncao', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
               ->whereRaw($config->where)
               ->groupBy('txtdescricaofuncao')
               ->orderBy('txtdescricaofuncao')
               ->get();

            if(property_exists($config,'request')) {
               if($config->request->dynamic == 'true') {
                    $subfuncao = $model::select('txtdescricaofuncao', 'codsubfuncao', 'txtdescricaosubfuncao', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
                    ->whereRaw($config->where)
                    ->groupBy('txtdescricaofuncao', 'codsubfuncao', 'txtdescricaosubfuncao')
                    ->get();
   
                    $acao = $model::select('codacao', 'txtdescricaofuncao', 'codsubfuncao', 'txtdescricaoacao', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
                        ->whereRaw($config->where)
                        ->groupBy('codacao', 'txtdescricaofuncao', 'codsubfuncao', 'txtdescricaoacao')
                        ->get();
                    return (object) [
                        'funcao' => $funcao,
                        'subfuncao' => $subfuncao,
                        'acao' => $acao,
                    ];
               }
            }
         } else {
            // Empenho e Liquidação
            $funcao = $model::select('txtdescricaofuncao', 'txtdescricaofuncao', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
            ->whereRaw($config->where)
                ->groupBy('txtdescricaofuncao', 'txtdescricaofuncao')
                ->orderBy('txtdescricaofuncao')
                ->get();

               if(property_exists($config,'request')) {
                  if($config->request->dynamic == 'true') {
                    $subfuncao = $model::select('txtdescricaofuncao', 'codsubfuncao', 'txtdescricaosubfuncao', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
                    ->whereRaw($config->where)
                        ->groupBy('txtdescricaofuncao', 'codsubfuncao', 'txtdescricaosubfuncao')
                        ->get();
        
                    $acao = $model::select('codacao', 'txtdescricaofuncao', 'codsubfuncao', 'txtdescricaoacao', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
                        ->whereRaw($config->where)
                        ->groupBy('codacao', 'txtdescricaofuncao', 'codsubfuncao', 'txtdescricaoacao')
                        ->get();
                    return (object) [
                        'funcao' => $funcao,
                        'subfuncao' => $subfuncao,
                        'acao' => $acao,
                    ];
                  }
               }
         }
         // Retornar as consultas para a página
         return (object) [
            'funcao' => $funcao,
         ];
    }
}