<?php

namespace App\Components\Despesas;

use App\Models\GastosDireto\LiquidacaoGastosDiretos;
use App\Models\GastosDireto\NotaEmpenhoGastosDiretos;
use App\Models\GastosDireto\NotaPagamentoGastosDiretos;
use Illuminate\Support\Facades\DB;

class Despesa
{
   //Faz a query de todos os dados necessarios para montar a row de acordo com as seleçoes da busca
    public function query(object $config)
    {
        if ($config->fase == 'vportal_notapagamento_gastosdiretos') {
            $model = NotaPagamentoGastosDiretos::class;
         } else if ($config->fase == 'vportal_notaempenho_gastosdiretos') {
            $model = NotaEmpenhoGastosDiretos::class;
         } else {
            $model = LiquidacaoGastosDiretos::class;
         }

         if ($config->fase == 'vportal_notapagamento_gastosdiretos') {
            $grupodespesa = $model::select('txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'codgrupodespesa', 'codelementodespesa', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
               ->whereRaw($config->where)
               ->groupBy('txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'codgrupodespesa', 'codelementodespesa')
               ->orderBy('txtdescricaogrupodespesa')
               ->paginate(10);
            if(property_exists($config,'request')) {
               if($config->request->dynamic == 'true') {
                  $unidadegestora = $model::select('txtdescricaounidade', 'codunidadegestora', 'codgrupodespesa', 'codelementodespesa', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
                     ->whereRaw($config->where)
                     ->groupBy('txtdescricaounidade', 'codunidadegestora', 'codgrupodespesa', 'codelementodespesa')
                     ->get();

                  $gestao = $model::select('txtdescricaogestao', 'codgestao', 'codgrupodespesa', 'codelementodespesa', 'codunidadegestora', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
                     ->whereRaw($config->where)
                     ->groupBy('txtdescricaogestao', 'codgestao', 'codgrupodespesa', 'codelementodespesa', 'codunidadegestora')
                     ->get();
                  return (object) [
                     'grupodespesa' => $grupodespesa,
                     'unidadegestora' => $unidadegestora,
                     'gestao' => $gestao,
                  ];
               }
            }
         } else {
            // Empenho e Liquidação
            $grupodespesa = $model::select('txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'codgrupodespesa', 'codelementodespesa', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
               ->whereRaw($config->where)
               ->groupBy('txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'codgrupodespesa', 'codelementodespesa')
               ->orderBy('txtdescricaogrupodespesa')
               ->paginate(10);
            if(property_exists($config,'request')) {
               if($config->request->dynamic == 'true') {
                  $unidadegestora = $model::select('txtdescricaounidade', 'codunidadegestora', 'codgrupodespesa', 'codelementodespesa', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
                     ->whereRaw($config->where)
                     ->groupBy('txtdescricaounidade', 'codunidadegestora', 'codgrupodespesa', 'codelementodespesa')
                     ->get();

                  $gestao = $model::select('txtdescricaogestao', 'codgestao', 'codgrupodespesa', 'codelementodespesa', 'codunidadegestora', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
                     ->whereRaw($config->where)
                     ->groupBy('txtdescricaogestao', 'codgestao', 'codgrupodespesa', 'codelementodespesa', 'codunidadegestora')
                     ->get();
                  return (object) [
                     'grupodespesa' => $grupodespesa,
                     'unidadegestora' => $unidadegestora,
                     'gestao' => $gestao,
                  ];
               }
            }
         }
         // Retornar as consultas para a página
         return (object) [
            'grupodespesa' => $grupodespesa,
         ];
    }
}