<?php

namespace App\Components\Despesas;

use App\Models\GastosDireto\LiquidacaoGastosDiretos;
use App\Models\GastosDireto\NotaEmpenhoGastosDiretos;
use App\Models\GastosDireto\NotaPagamentoGastosDiretos;
use Illuminate\Support\Facades\DB;

class Acao
{
   //Faz a query de todos os dados necessarios para montar a row de acordo com as seleçoes da busca
    public function query(object $config)
    {
        if ($config->fase == 'vportal_notapagamento_gastosdiretos') {
            $model = NotaPagamentoGastosDiretos::class;
         } else if ($config->fase == 'vportal_notaempenho_gastosdiretos') {
            $model = NotaEmpenhoGastosDiretos::class;
         } else {
            $model = LiquidacaoGastosDiretos::class;
         }

         if ($config->fase == 'vportal_notapagamento_gastosdiretos') {
            $acao = $model::select('codacao', 'txtdescricaoacao', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
               ->whereRaw($config->where)
               ->groupBy('codacao', 'txtdescricaoacao')
               ->orderBy('txtdescricaoacao')
               ->paginate(10);
         }
         // Empenho
         else if ($config->fase == 'vportal_notaempenho_gastosdiretos'){
            // Unir tabela de Gastos Diretos com Transferencia de Recursos
            $acao = $model::select('codacao', 'txtdescricaoacao', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
               ->whereRaw($config->where)
               ->groupBy('codacao', 'txtdescricaoacao')
               ->orderBy('txtdescricaoacao')
               ->paginate(10);
         }
         // Liquidação
         else {
            $acao = $model::select('codacao', 'txtdescricaoacao', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
               ->whereRaw($config->where)
               ->groupBy('codacao', 'txtdescricaoacao')
               ->orderBy('txtdescricaoacao')
               ->paginate(10);
         }

         // Retornar as consultas para a página
         return (object) [
            'acoes' => $acao,
         ];
    }
}