<?php

namespace App\Components\Popover;

class Popover
{
   //Essa função gera o popover
    public function generatepop(object $config)
    {
        if (($config->request->nulo != 'true')) {
            $id = "'".$config->request->id."'";
            $output = ' <div id="pop'.$config->request->id.'"name="'. $config->request->id .'" class="hidden absolute z-10">
                           <div class="bg-white cursor-pointer shadow rounded-lg px-3 py-2 space-y-2">
                              <table class="divide-y">
                                 <thead class="divide-y">
                                    <tr class="flex items-center">
                                       <td class="text-center px-2">
                                          <!-- Header -->
                                          <div>
                                             <div class="font-semibold text-sm">
                                                Receita Prevista (Bruta)
                                             </div>
                                          </div>
                                       </td>
                                       <td class="text-center px-2">
                                          <b class="font-semibold">Arrecadada</b>
                                       </td>
                                       <td class="text-center px-2">
                                          <button name="pop" onclick="pop(' . $id . ');" class="h-6 w-6 flex justify-center items-center bg-red-200 text-red-500 rounded-full cursor-pointer hover:bg-red-300">
                                             <svg class="w-4 h-4" fill="none" stroke="currentColor" viewBox="0 0 24 24"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                   d="M6 18L18 6M6 6l12 12"></path>
                                             </svg>
                                          </button>
                                       </td>
                                    </tr>
                                 </thead>
                                 <tbody class="divide-y">
                                 <!-- Content -->
                                    <tr class="flex justify-between px-2">
                                       <td class="text-center">
                                          <div class="overflow-ellipsis overflow-hidden text-sm">
                                             <div class="text-blue-600 font-semibold text-xs leading-loose">
                                             ' . $config->request->valor . '
                                             </div>
                                          </div>
                                       </td>
                                       <td class="text-center">
                                          <div class="clamp-3 leading-tight">
                                             <span class="text-blue-600 font-semibold text-xs leading-loose">' . $config->request->porcentagem . '%</span>
                                          </div>
                                       </td>
                                       <td class="text-center">
                                          <span></span>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>';
            return $output;
         }
         // valor nulo
         else {
            $id = "'" . $config->request->id . "'";
            $output = '<div id="pop' . $config->request->id . '"name="' . $config->request->id . '" class="hidden absolute z-10">
               <div class="bg-white cursor-pointer shadow rounded-lg px-3 py-2 w-64 space-y-2">
                  <!-- Header -->
                  <div class="flex justify-between items-center w-full">
                     <div class="font-semibold text-sm">
                        Não há receita prevista.
                     </div>
                     <button name="pop" onclick="pop(' . $id . ');" class="h-6 w-6 flex justify-center items-center bg-red-200 text-red-500 rounded-full cursor-pointer hover:bg-red-300">
                        <svg class="w-4 h-4" fill="none" stroke="currentColor" viewBox="0 0 24 24"
                           xmlns="http://www.w3.org/2000/svg">
                           <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                              d="M6 18L18 6M6 6l12 12"></path>
                        </svg>
                     </button>
                  </div>
               </div>
            </div>';
            return $output;
         }
    }
}