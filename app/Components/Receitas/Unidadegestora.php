<?php

namespace App\Components\Receitas;

use App\Models\Receitas;
use Illuminate\Support\Facades\DB;

class Unidadegestora
{
      //Esta função faz a consulta para um determinado contexto
      //$config é composto por:
      // "mes" => Mes de contexto da busca,
      // "ano" => Ano de contexto da busca,
      //  "posicao" => Posição refere-se a acumulado ou não acumulado,
      //  "fase" => Pode ser usada ou não e refere-se a tabela em que o dado deve ser buscado,
      //  "where" => Condicionais que serão usadas para estabelecer o contexto na consulta
    public function query(object $config)
    {
         $model = Receitas::class;
         $unidadegestora = $model::select('codunidade', 'txtdescricaounidade', DB::raw('SUM(valorreceita) as valorreceita'))
            ->whereRaw($config->where) //where configuravel passado como parametro
            ->groupBy('codunidade', 'txtdescricaounidade')
            ->orderBy('txtdescricaounidade')
            ->paginate(10);

         if(property_exists($config,'request')) {
            if($config->request->dynamic == 'true') {
               $gestoes = $model::select('codgestao', 'txtdescricaogestao', 'codunidade', DB::raw('SUM(valorreceita) as valorreceita'))
               ->whereRaw($config->where)
               ->groupBy('codgestao', 'txtdescricaogestao', 'codunidade')
               ->get();

               foreach ($gestoes as $gestao) {
                  $valorPrevisto = $model::select(DB::raw('SUM(vlrprevisaoatualizado) as valor'))
                     ->where('codunidade', $gestao->codunidade)
                     ->where('mes', '<=', $config->mes)
                     ->where('numexerciciofinanc', $config->ano)
                     ->where('codgestao', $gestao->codgestao)
                     ->first();
      
                     if ($valorPrevisto->valor == 0) {
                        $gestao->porcentagem = 0;
                        $gestao->porcentagem_formatada = '0,0';
                        $gestao->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     } else {
                        $gestao->porcentagem = (($gestao->valorreceita * 100) / number_format($valorPrevisto->valor, 2, '.', ''));
                        $gestao->porcentagem_formatada = number_format($gestao->porcentagem, 2, ',', '.');
                        $gestao->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     }
               }
   
               $categorias = $model::select('codcategoria', 'txtdescricaocategoria', 'codunidade', 'codgestao', DB::raw('SUM(valorreceita) as valorreceita'))
                  ->whereRaw($config->where)
                  ->groupBy('codcategoria', 'txtdescricaocategoria', 'codunidade', 'codgestao')
                  ->get();
               
               foreach ($categorias as $categoria) {
                  $valorPrevisto = $model::select(DB::raw('SUM(vlrprevisaoatualizado) as valor'))
                        ->where('codunidade', $gestao->codunidade)
                        ->where('codcategoria', $categoria->codcategoria)
                        ->where('mes', '<=', $config->mes)
                        ->where('numexerciciofinanc', $config->ano)
                        ->first();
         
                        if ($valorPrevisto->valor == 0) {
                           $categoria->porcentagem = 0;
                           $categoria->porcentagem_formatada = '0,0';
                           $categoria->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                        } else {
                           $categoria->porcentagem = (($categoria->valorreceita * 100) / number_format($valorPrevisto->valor, 2, '.', ''));
                           $categoria->porcentagem_formatada = number_format($categoria->porcentagem, 2, ',', '.');
                           $categoria->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                        }
               }

      
               $origens = $model::select('codorigem', 'txtdescricaoorigem', 'codunidade', 'codgestao', 'codcategoria', DB::raw('SUM(valorreceita) as valorreceita'))
                  ->whereRaw($config->where)
                  ->groupBy('codorigem', 'txtdescricaoorigem', 'codunidade', 'codgestao', 'codcategoria')
                  ->get();

               foreach ($origens as $origem) {
                  $valorPrevisto = $model::select(DB::raw('SUM(vlrprevisaoatualizado) as valor'))
                     ->where('codunidade', $gestao->codunidade)
                     ->where('mes', '<=', $config->mes)
                     ->where('numexerciciofinanc', $config->ano)
                     ->where('codorigem', $origem->codorigem)
                     ->first();
      
                     if ($valorPrevisto->valor == 0) {
                        $origem->porcentagem = 0;
                        $origem->porcentagem_formatada = '0,0';
                        $origem->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     } else {
                        $origem->porcentagem = (($origem->valorreceita * 100) / number_format($valorPrevisto->valor, 2, '.', ''));
                        $origem->porcentagem_formatada = number_format($origem->porcentagem, 2, ',', '.');
                        $origem->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     }
               }
      
               $especies = $model::select('codespecie', 'txtdescricaoespecie', 'codunidade', 'codgestao', 'codcategoria', 'codorigem', DB::raw('SUM(valorreceita) as valorreceita'))
                  ->whereRaw($config->where)
                  ->groupBy('codespecie', 'txtdescricaoespecie', 'codunidade', 'codgestao', 'codcategoria', 'codorigem')
                  ->get();

               foreach ($especies as $especie) {
                  $valorPrevisto = $model::select(DB::raw('SUM(vlrprevisaoatualizado) as valor'))
                     ->where('codunidade', $gestao->codunidade)
                     ->where('mes', '<=', $config->mes)
                     ->where('numexerciciofinanc', $config->ano)
                     ->where('codespecie', $especie->codespecie)
                     ->first();
      
                     if ($valorPrevisto->valor == 0) {
                        $especie->porcentagem = 0;
                        $especie->porcentagem_formatada = '0,0';
                        $especie->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     } else {
                        $especie->porcentagem = (($especie->valorreceita * 100) / number_format($valorPrevisto->valor, 2, '.', ''));
                        $especie->porcentagem_formatada = number_format($especie->porcentagem, 2, ',', '.');
                        $especie->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     }
               }
      
               $rubricas = $model::select('codrubrica', 'txtdescricaorubrica', 'codunidade', 'codgestao', 'codcategoria', 'codorigem', 'codespecie', DB::raw('SUM(valorreceita) as valorreceita'))
                  ->whereRaw($config->where)
                  ->groupBy('codrubrica', 'txtdescricaorubrica', 'codunidade', 'codgestao', 'codcategoria', 'codorigem', 'codespecie')
                  ->get();

               foreach ($rubricas as $rubrica) {
                  $valorPrevisto = $model::select(DB::raw('SUM(vlrprevisaoatualizado) as valor'))
                     ->where('codunidade', $gestao->codunidade)
                     ->where('mes', '<=', $config->mes)
                     ->where('numexerciciofinanc', $config->ano)
                     ->where('codrubrica', $rubrica->codrubrica)
                     ->first();
      
                     if ($valorPrevisto->valor == 0) {
                        $rubrica->porcentagem = 0;
                        $rubrica->porcentagem_formatada = '0,0';
                        $rubrica->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     } else {
                        $rubrica->porcentagem = (($rubrica->valorreceita * 100) / number_format($valorPrevisto->valor, 2, '.', ''));
                        $rubrica->porcentagem_formatada = number_format($rubrica->porcentagem, 2, ',', '.');
                        $rubrica->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     }
               }
      
               $alineas = $model::select('codalinea', 'txtdescricaoalinea', 'codunidade', 'codgestao', 'codcategoria', 'codorigem', 'codespecie', 'codrubrica', DB::raw('SUM(valorreceita) as valorreceita'))
                  ->whereRaw($config->where)
                  ->groupBy('codalinea', 'txtdescricaoalinea', 'codunidade', 'codgestao', 'codcategoria', 'codorigem', 'codespecie', 'codrubrica')
                  ->get();
               
               foreach ($alineas as $alinea) {
                  $valorPrevisto = $model::select(DB::raw('SUM(vlrprevisaoatualizado) as valor'))
                     ->where('codunidade', $gestao->codunidade)
                     ->where('mes', '<=', $config->mes)
                     ->where('numexerciciofinanc', $config->ano)
                     ->where('codalinea', $alinea->codalinea)
                     ->first();
      
                     if ($valorPrevisto->valor == 0) {
                        $alinea->porcentagem = 0;
                        $alinea->porcentagem_formatada = '0,0';
                        $alinea->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     } else {
                        $alinea->porcentagem = (($alinea->valorreceita * 100) / number_format($valorPrevisto->valor, 2, '.', ''));
                        $alinea->porcentagem_formatada = number_format($alinea->porcentagem, 2, ',', '.');
                        $alinea->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     }
               }
      
               $detalhamento = $model::select('txtdescricaodetalhada', 'codunidade', 'codgestao', 'codcategoria', 'codorigem', 'codespecie', 'codrubrica', 'codalinea', DB::raw('SUM(valorreceita) as valorreceita'))
                  ->whereRaw($config->where)
                  ->groupBy('txtdescricaodetalhada', 'codunidade', 'codgestao', 'codcategoria', 'codorigem', 'codespecie', 'codrubrica', 'codalinea')
                  ->get();

               foreach ($detalhamento as $detail) {
                  $valorPrevisto = $model::select(DB::raw('SUM(vlrprevisaoatualizado) as valor'))
                     ->where('codunidade', $gestao->codunidade)
                     ->where('mes', '<=', $config->mes)
                     ->where('numexerciciofinanc', $config->ano)
                     ->where('txtdescricaodetalhada', $detail->txtdescricaodetalhada)
                     ->first();
      
                     if ($valorPrevisto->valor == 0) {
                        $detail->porcentagem = 0;
                        $detail->porcentagem_formatada = '0,0';
                        $detail->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     } else {
                        $detail->porcentagem = (($detail->valorreceita * 100) / number_format($valorPrevisto->valor, 2, '.', ''));
                        $detail->porcentagem_formatada = number_format($detail->porcentagem, 2, ',', '.');
                        $detail->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     }
               }
               return ((object) [
                  'unidadegestora' => $unidadegestora,
                  'gestao' => $gestoes,
                  'categorias' => $categorias,
                  'origens' => $origens,
                  'especies' => $especies,
                  'rubricas' => $rubricas,
                  'alineas' => $alineas,
                  'detalhamento' => $detalhamento,
                  'mes' => $config->mes,
                  'ano' => $config->ano,
                  'posicao' => $config->posicao,
               ]);
            }
         }
         return ((object) [
            'unidadegestora' => $unidadegestora,
         ]);
    }
}