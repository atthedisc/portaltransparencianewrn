<?php

namespace App\Components\Receitas;

use Illuminate\Support\Facades\DB;
use App\Models\Receitas;

class Receita
{
   //Esta função faz a consulta para um determinado contexto
   //$config é composto por:
   // "mes" => Mes de contexto da busca,
   // "ano" => Ano de contexto da busca,
   //  "posicao" => Posição refere-se a acumulado ou não acumulado,
   //  "fase" => Pode ser usada ou não e refere-se a tabela em que o dado deve ser buscado,
   //  "where" => Condicionais que serão usadas para estabelecer o contexto na consulta
    public function query(object $config)
    {
        $model = Receitas::class;
        $categorias = $model::select('codcategoria', 'txtdescricaocategoria', DB::raw('SUM(valorreceita) as valorreceita'))
            ->whereRaw($config->where)
            ->groupBy('codcategoria', 'txtdescricaocategoria')
            ->orderByRaw("CASE WHEN txtdescricaocategoria = 'Receitas Correntes' then 0 WHEN txtdescricaocategoria = 'Deduções da Receita' then 1 WHEN txtdescricaocategoria = 'Receitas de Capital' then 2 Else 3 END")
            ->paginate(10);
            
        if(property_exists($config,'request')) {
            if($config->request->dynamic == 'true') {
               $origens = $model::select('codorigem', 'txtdescricaoorigem', 'codcategoria', DB::raw('SUM(valorreceita) as valorreceita'))
               ->whereRaw($config->where)
               ->groupBy('codorigem', 'txtdescricaoorigem', 'codcategoria')
               ->get();

               foreach ($origens as $origem) {
                  $valorPrevisto = $model::select(DB::raw('SUM(vlrprevisaoatualizado) as valor'))
                     ->where('codcategoria', $origem->codcategoria)
                     ->where('mes', '<=', $config->mes)
                     ->where('numexerciciofinanc', $config->ano)
                     ->where('codorigem', $origem->codorigem)
                     ->first();
      
                     if ($valorPrevisto->valor == 0) {
                        $origem->porcentagem = 0;
                        $origem->porcentagem_formatada = '0,0';
                        $origem->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     } else {
                        $origem->porcentagem = (($origem->valorreceita * 100) / number_format($valorPrevisto->valor, 2, '.', ''));
                        $origem->porcentagem_formatada = number_format($origem->porcentagem, 2, ',', '.');
                        $origem->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     }
               }

               $especies = $model::select('codespecie', 'txtdescricaoespecie', 'codcategoria', 'codorigem', DB::raw('SUM(valorreceita) as valorreceita'))
                  ->whereRaw($config->where)
                  ->groupBy('codespecie', 'txtdescricaoespecie', 'codcategoria', 'codorigem')
                  ->get();

               foreach ($especies as $especie) {
                  $valorPrevisto = $model::select(DB::raw('SUM(vlrprevisaoatualizado) as valor'))
                     ->where('codcategoria', $especie->codcategoria)
                     ->where('mes', '<=', $config->mes)
                     ->where('numexerciciofinanc', $config->ano)
                     ->where('codespecie', $especie->codespecie)
                     ->first();
      
                     if ($valorPrevisto->valor == 0) {
                        $especie->porcentagem = 0;
                        $especie->porcentagem_formatada = '0,0';
                        $especie->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     } else {
                        $especie->porcentagem = (($especie->valorreceita * 100) / number_format($valorPrevisto->valor, 2, '.', ''));
                        $especie->porcentagem_formatada = number_format($especie->porcentagem, 2, ',', '.');
                        $especie->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     }
               }

               $rubricas = $model::select('codrubrica', 'txtdescricaorubrica', 'codcategoria', 'codorigem', 'codespecie', DB::raw('SUM(valorreceita) as valorreceita'))
                  ->whereRaw($config->where)
                  ->groupBy('codrubrica', 'txtdescricaorubrica', 'codcategoria', 'codorigem', 'codespecie')
                  ->get();

               foreach ($rubricas as $rubrica) {
                  $valorPrevisto = $model::select(DB::raw('SUM(vlrprevisaoatualizado) as valor'))
                     ->where('codcategoria', $rubrica->codcategoria)
                     ->where('mes', '<=', $config->mes)
                     ->where('numexerciciofinanc', $config->ano)
                     ->where('codrubrica', $rubrica->codrubrica)
                     ->first();
      
                     if ($valorPrevisto->valor == 0) {
                        $rubrica->porcentagem = 0;
                        $rubrica->porcentagem_formatada = '0,0';
                        $rubrica->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     } else {
                        $rubrica->porcentagem = (($rubrica->valorreceita * 100) / number_format($valorPrevisto->valor, 2, '.', ''));
                        $rubrica->porcentagem_formatada = number_format($rubrica->porcentagem, 2, ',', '.');
                        $rubrica->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     }
               }

               $alineas = $model::select('codalinea', 'txtdescricaoalinea', 'codcategoria', 'codorigem', 'codespecie', 'codrubrica', DB::raw('SUM(valorreceita) as valorreceita'))
                  ->whereRaw($config->where)
                  ->groupBy('codalinea', 'txtdescricaoalinea', 'codcategoria', 'codorigem', 'codespecie', 'codrubrica')
                  ->get();

               foreach ($alineas as $alinea) {
                  $valorPrevisto = $model::select(DB::raw('SUM(vlrprevisaoatualizado) as valor'))
                     ->where('codcategoria', $alinea->codcategoria)
                     ->where('mes', '<=', $config->mes)
                     ->where('numexerciciofinanc', $config->ano)
                     ->where('codalinea', $alinea->codalinea)
                     ->first();
      
                     if ($valorPrevisto->valor == 0) {
                        $alinea->porcentagem = 0;
                        $alinea->porcentagem_formatada = '0,0';
                        $alinea->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     } else {
                        $alinea->porcentagem = (($alinea->valorreceita * 100) / number_format($valorPrevisto->valor, 2, '.', ''));
                        $alinea->porcentagem_formatada = number_format($alinea->porcentagem, 2, ',', '.');
                        $alinea->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     }
               }

               $detalhamento = $model::select('txtdescricaodetalhada', 'codcategoria', 'codorigem', 'codespecie', 'codrubrica', 'codalinea', DB::raw('SUM(valorreceita) as valorreceita'))
                  ->whereRaw($config->where)
                  ->groupBy('txtdescricaodetalhada', 'codcategoria', 'codorigem', 'codespecie', 'codrubrica', 'codalinea')
                  ->get();

               foreach ($detalhamento as $detail) {
                  $valorPrevisto = $model::select(DB::raw('SUM(vlrprevisaoatualizado) as valor'))
                     ->where('codcategoria', $detail->codcategoria)
                     ->where('mes', '<=', $config->mes)
                     ->where('numexerciciofinanc', $config->ano)
                     ->where('txtdescricaodetalhada', $detail->txtdescricaodetalhada)
                     ->first();
      
                     if ($valorPrevisto->valor == 0) {
                        $detail->porcentagem = 0;
                        $detail->porcentagem_formatada = '0,0';
                        $detail->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     } else {
                        $detail->porcentagem = (($detail->valorreceita * 100) / number_format($valorPrevisto->valor, 2, '.', ''));
                        $detail->porcentagem_formatada = number_format($detail->porcentagem, 2, ',', '.');
                        $detail->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     }
               }

               // Detalhamento para Deduções da Receita
               $detalhamentoDR = $model::select('txtdescricaodeducaodetalhada', 'codcategoria', DB::raw('SUM(valorreceita) as valorreceita'))
               ->whereRaw($config->where)
               ->groupBy('coddeducaodetalhada', 'txtdescricaodeducaodetalhada', 'codcategoria')
               ->get();

               foreach ($detalhamentoDR as $edu) {
                  $valorPrevisto = $model::select(DB::raw('SUM(vlrprevisaoatualizado) as valor'))
                     ->where('codcategoria', $edu->codcategoria)
                     ->where('mes', '<=', $config->mes)
                     ->where('numexerciciofinanc', $config->ano)
                     ->where('txtdescricaodeducaodetalhada', $edu->txtdescricaodeducaodetalhada)
                     ->first();
      
                     if ($valorPrevisto->valor == 0) {
                        $edu->porcentagem = 0;
                        $edu->porcentagem_formatada = '0,0';
                        $edu->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     } else {
                        $edu->porcentagem = (($edu->valorreceita * 100) / number_format($valorPrevisto->valor, 2, '.', ''));
                        $edu->porcentagem_formatada = number_format($edu->porcentagem, 2, ',', '.');
                        $edu->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     }
               }

               return((object) [
                  'categorias' => $categorias,
                  'origens' => $origens,
                  'especies' => $especies,
                  'rubricas' => $rubricas,
                  'alineas' => $alineas,
                  'detalhamento' => $detalhamento,
                  'detalhamentoDR' => $detalhamentoDR,
               ]);
            }
        }
            
            return ['categorias' => $categorias];
            
    }
}