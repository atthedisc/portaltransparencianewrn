<?php

namespace App\Components\Receitas;

use Illuminate\Support\Facades\DB;
use App\Models\Receitas;

class Fonte
{
   //Esta função faz a consulta para um determinado contexto
   //$config é composto por:
   // "mes" => Mes de contexto da busca,
   // "ano" => Ano de contexto da busca,
   //  "posicao" => Posição refere-se a acumulado ou não acumulado,
   //  "fase" => Pode ser usada ou não e refere-se a tabela em que o dado deve ser buscado,
   //  "where" => Condicionais que serão usadas para estabelecer o contexto na consulta
    public function query(object $config)
    {
        $model = Receitas::class;
         $fontes = $model::select('codfonterecurso', 'txtdescricaofonterecurso', DB::raw('SUM(valorreceita) as valorreceita'))
            ->whereRaw($config->where)
            ->groupBy('codfonterecurso', 'txtdescricaofonterecurso')
            ->orderBy('txtdescricaofonterecurso')
            ->paginate(10);

         if(property_exists($config,'request')) {
            if($config->request->dynamic == 'true') {
               $categorias = $model::select('codcategoria', 'txtdescricaocategoria', 'codfonterecurso', DB::raw('SUM(valorreceita) as valorreceita'))
               ->whereRaw($config->where)
               ->groupBy('codcategoria', 'txtdescricaocategoria', 'codfonterecurso')
               ->get();

               foreach ($categorias as $item) {
                  $valorPrevisto = $model::select(DB::raw('SUM(vlrprevisaoatualizado) as valor'))
                     ->where('codfonterecurso', $item->codfonterecurso)
                     ->where('codcategoria', $item->codcategoria)
                     ->where('mes', $config->posicao, $config->mes)
                     ->where('numexerciciofinanc', $config->ano)
                     ->first();
      
                     if ($valorPrevisto->valor == 0) {
                        $item->porcentagem = 0;
                        $item->porcentagem_formatada = '0,0';
                        $item->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     } else {
                        $item->porcentagem = (($item->valorreceita * 100) / number_format($valorPrevisto->valor, 2, '.', ''));
                        $item->porcentagem_formatada = number_format($item->porcentagem, 2, ',', '.');
                        $item->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     }
               }
   
               $origens = $model::select('codorigem', 'txtdescricaoorigem', 'codfonterecurso', 'codcategoria', DB::raw('SUM(valorreceita) as valorreceita'))
                  ->whereRaw($config->where)
                  ->groupBy('codorigem', 'txtdescricaoorigem', 'codfonterecurso', 'codcategoria')
                  ->get();

               foreach ($origens as $item) {
                  $valorPrevisto = $model::select(DB::raw('SUM(vlrprevisaoatualizado) as valor'))
                     ->where('codfonterecurso', $item->codfonterecurso)
                     ->where('codorigem', $item->codorigem)
                     ->where('mes', $config->posicao, $config->mes)
                     ->where('numexerciciofinanc', $config->ano)
                     ->first();
      
                     if ($valorPrevisto->valor == 0) {
                        $item->porcentagem = 0;
                        $item->porcentagem_formatada = '0,0';
                        $item->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     } else {
                        $item->porcentagem = (($item->valorreceita * 100) / number_format($valorPrevisto->valor, 2, '.', ''));
                        $item->porcentagem_formatada = number_format($item->porcentagem, 2, ',', '.');
                        $item->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     }
               }
      
               $especies = $model::select('codespecie', 'txtdescricaoespecie', 'codfonterecurso', 'codcategoria', 'codorigem', DB::raw('SUM(valorreceita) as valorreceita'))
                  ->whereRaw($config->where)
                  ->groupBy('codespecie', 'txtdescricaoespecie', 'codfonterecurso', 'codcategoria', 'codorigem')
                  ->get();

               foreach ($especies as $item) {
                  $valorPrevisto = $model::select(DB::raw('SUM(vlrprevisaoatualizado) as valor'))
                     ->where('codfonterecurso', $item->codfonterecurso)
                     ->where('codespecie', $item->codespecie)
                     ->where('mes', $config->posicao, $config->mes)
                     ->where('numexerciciofinanc', $config->ano)
                     ->first();
      
                     if ($valorPrevisto->valor == 0) {
                        $item->porcentagem = 0;
                        $item->porcentagem_formatada = '0,0';
                        $item->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     } else {
                        $item->porcentagem = (($item->valorreceita * 100) / number_format($valorPrevisto->valor, 2, '.', ''));
                        $item->porcentagem_formatada = number_format($item->porcentagem, 2, ',', '.');
                        $item->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     }
               }
      
               $rubricas = $model::select('codrubrica', 'txtdescricaorubrica', 'codfonterecurso', 'codcategoria', 'codorigem', 'codespecie', DB::raw('SUM(valorreceita) as valorreceita'))
                  ->whereRaw($config->where)
                  ->groupBy('codrubrica', 'txtdescricaorubrica', 'codfonterecurso', 'codcategoria', 'codorigem', 'codespecie')
                  ->get();

               foreach ($rubricas as $item) {
                  $valorPrevisto = $model::select(DB::raw('SUM(vlrprevisaoatualizado) as valor'))
                     ->where('codfonterecurso', $item->codfonterecurso)
                     ->where('codrubrica', $item->codrubrica)
                     ->where('mes', $config->posicao, $config->mes)
                     ->where('numexerciciofinanc', $config->ano)
                     ->first();
      
                     if ($valorPrevisto->valor == 0) {
                        $item->porcentagem = 0;
                        $item->porcentagem_formatada = '0,0';
                        $item->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     } else {
                        $item->porcentagem = (($item->valorreceita * 100) / number_format($valorPrevisto->valor, 2, '.', ''));
                        $item->porcentagem_formatada = number_format($item->porcentagem, 2, ',', '.');
                        $item->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     }
               }
      
               $alineas = $model::select('codalinea', 'txtdescricaoalinea', 'codfonterecurso', 'codcategoria', 'codorigem', 'codespecie', 'codrubrica', DB::raw('SUM(valorreceita) as valorreceita'))
                  ->whereRaw($config->where)
                  ->groupBy('codalinea', 'txtdescricaoalinea', 'codfonterecurso', 'codcategoria', 'codorigem', 'codespecie', 'codrubrica')
                  ->get();

               foreach ($alineas as $item) {
                  $valorPrevisto = $model::select(DB::raw('SUM(vlrprevisaoatualizado) as valor'))
                     ->where('codfonterecurso', $item->codfonterecurso)
                     ->where('codalinea', $item->codalinea)
                     ->where('mes', $config->posicao, $config->mes)
                     ->where('numexerciciofinanc', $config->ano)
                     ->first();
      
                     if ($valorPrevisto->valor == 0) {
                        $item->porcentagem = 0;
                        $item->porcentagem_formatada = '0,0';
                        $item->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     } else {
                        $item->porcentagem = (($item->valorreceita * 100) / number_format($valorPrevisto->valor, 2, '.', ''));
                        $item->porcentagem_formatada = number_format($item->porcentagem, 2, ',', '.');
                        $item->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     }
               }
      
               $detalhamento = $model::select('txtdescricaodetalhada', 'codfonterecurso', 'codcategoria', 'codorigem', 'codespecie', 'codrubrica', 'codalinea', DB::raw('SUM(valorreceita) as valorreceita'))
                  ->whereRaw($config->where)
                  ->groupBy('txtdescricaodetalhada', 'codfonterecurso', 'codcategoria', 'codorigem', 'codespecie', 'codrubrica', 'codalinea')
                  ->get();

               foreach ($detalhamento as $item) {
                  $valorPrevisto = $model::select(DB::raw('SUM(vlrprevisaoatualizado) as valor'))
                     ->where('codfonterecurso', $item->codfonterecurso)
                     ->where('txtdescricaodetalhada', $item->txtdescricaodetalhada)
                     ->where('mes', $config->posicao, $config->mes)
                     ->where('numexerciciofinanc', $config->ano)
                     ->first();
      
                     if ($valorPrevisto->valor == 0) {
                        $item->porcentagem = 0;
                        $item->porcentagem_formatada = '0,0';
                        $item->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     } else {
                        $item->porcentagem = (($item->valorreceita * 100) / number_format($valorPrevisto->valor, 2, '.', ''));
                        $item->porcentagem_formatada = number_format($item->porcentagem, 2, ',', '.');
                        $item->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                     }
               }
      
               return ((object) [
                  'fontes' => $fontes,
                  'categorias' => $categorias,
                  'origens' => $origens,
                  'especies' => $especies,
                  'rubricas' => $rubricas,
                  'alineas' => $alineas,
                  'detalhamento' => $detalhamento,
                  'mes' => $config->mes,
                  'ano' => $config->ano,
                  'posicao' => $config->posicao,
               ]);
            }
         }
         return ((object) [
            'fontes' => $fontes,
         ]);
    }
}