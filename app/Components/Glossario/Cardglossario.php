<?php

namespace App\Components\Glossario;

class Cardglossario
{
   //Essa função gera o card do glossário
   public function generatecard(object $config)
   {
$result = '<div class="w-full md:w-1/1 xl:w-1/1 p-3">
               <div class="p-2 bg-white shadow-md hover:shodow-lg rounded-2xl">
                  <div class="font-medium text-left text-lightBlue-500 uppercase pb-2" name="texto"
                     style="font-size: 1rem; line-height: 1.5rem;">
                     '.$config->nome.':
                  </div>
                  <p class="text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                     '.$config->descricao.' <br>
                  </p>
               </div>
            </div>';
   return($result);
   }
}