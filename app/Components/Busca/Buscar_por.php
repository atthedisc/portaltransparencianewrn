<?php

namespace App\Components\Busca;

class Buscar_por
{
   public function generatebuscar_por(array $config)
   {
      $keys = array_keys($config);
      $output = '
      <div class="flex-row my-5">
         <div class="">
            <div id="busca-container" class="bg-white rounded-lg shadow justify-center py-2">
               <div id="div-buscar-por">
                  <div data-state="false">
                     <div class="flex flex-wrap justify-center">
                        <div class="md:w-1/1 xl:w-1/1 p-3">
                           <h1 class="text-base text-center font-semibold text-lightBlue-700 uppercase" name="texto"
                              style="font-size: 1rem; line-height: 1.5rem;">
                              <i class="fas fa-search"></i>
                              Busca Avançada
                           </h1>
                           <span class="flex justify-center text-xs text-gray-500" name= "texto"
                           style="font-size: 0.75rem; line-height: 1rem;">
                              Se desejar, você pode fazer uma busca mais detalhada
                           </span>
                        </div>
                     </div>

                     <div class="flex flex-wrap">
                        <div class="flex justify-center w-full md:w-1/1 xl:w-1/1 p-3">
                           <div class="flex flex-col px-2 text-sm">
                                 <label for="buscar_por" class="text-xs font-semibold text-lightBlue-700 uppercase" name="texto"
                                    style="font-size: 0.75rem; line-height: 1rem;">
                                    Buscar por
                                 </label>
                                 <select id="buscar_por" name="buscar_por"
                                    class="flex justify-center w-80 p-3 text-gray-700 bg-white border border-gray-300 focus:outline-none focus:bg-gray-100 focus:shadow-inner">';
                                    foreach ($keys as $key) {
                                       $output .= '<option value="' . $key . '">' . $config[$key] . '</option>';
                                    }
                                    $output .= '
                                 </select>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div id="div-busca-parametro" class="flex flex-col w-full hidden"> <br>
               </div>
            </div>
         </div>
      </div>';
      return $output;
   }
}
