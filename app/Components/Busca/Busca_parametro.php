<?php

namespace App\Components\Busca;

use Illuminate\Support\Facades\DB;

class Busca_parametro
{
   //Função para Gerar os parametros de busca
   public function generatebusca_parametro(object $config)
   {
      $output = '
      <div id="busca_parametro_wrapper" class="flex flex-wrap">
         <div class="flex justify-center w-full md:w-1/1 xl:w-1/1 p-3">
            <div class="flex flex-col px-2 text-sm">
               <label for="busca_parametro" class="text-xs pb-2 font-semibold text-lightBlue-700 uppercase" name="texto" 
                  style="font-size: 0.75rem; line-height: 1rem;">
               Parâmetro de busca
               </label>
               <select id="busca_parametro" name="busca_parametro"
                  class="flex justify-center w-80 p-3 text-gray-700 bg-white border border-gray-300 focus:outline-none focus:bg-gray-100 focus:shadow-inner">
                  <option value="None"></option>
         ';
      //If feito caso a página nao precise de um contexto para a busca de resultados (Separação por ano ou por mes etc)
      if(is_null(collect($config)->get('where'))) {
         $grupo = DB::connection($config->schema)->table($config->table)
         ->select($config->buscar_por)
         ->orderBy($config->buscar_por)
         ->groupBy($config->buscar_por)
         ->get();
      }
      else {
         $grupo = DB::connection($config->schema)->table($config->table)
         ->select($config->buscar_por)
         ->whereRaw($config->where)
         ->orderBy($config->buscar_por)
         ->groupBy($config->buscar_por)
         ->get();
      }
      //Verifica se obteve parametros de busca
      if (!(int)count($grupo)) {
         return '
         <div class="flex justify-center">
            <p class="py-4 text-sm text-center uppercase" name="texto"
            style="font-size: 0.875rem; line-height: 1.25rem;">
               Desculpe, não foram encontrados dados para esse filtro
            </p>
         </div>
         ';
      } else {
         //Foreach de todos os parametros de busca para a opção selecionada no buscar por
         foreach ($grupo as $elemento) {
            $elem = collect($elemento);
            //Alterações de nomes solicitadas
            if($elem->get($config->buscar_por) == "Transferências aos municípios")
            {
               $nome_parametro = "Transferência constitucional de receitas estaduais a municípios";
            } else
            {
               $nome_parametro = $elem->get($config->buscar_por);
            }
            $output .= '<option value="' . urlencode($elem->get($config->buscar_por)) . '">' . $nome_parametro . '</option>';
         }
         $output .= '
                  </select>
               </div>
            </div>
         </div>';

         $output .= '<div class="flex flex-wrap justify-center">
                        <div class="flex justify-center">
                           <div class="w-full md:w-1/2 xl:w-1/2 p-3">
                              <button class="px-4 py-2 rounded-md text-sm font-medium border shadow focus:outline-none focus:ring transition text-blue-600 bg-blue-50 border-blue-200 hover:bg-blue-100 active:bg-blue-200 focus:ring-blue-300"
                                 onclick="buscar()" name="texto"
                                 style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Buscar
                              </button>
                           </div>
                           <div class="w-full md:w-1/2 xl:w-1/2 p-3">
                              <button id="bot"
                                 class="cursor-pointer w-auto px-4 py-2 rounded-md text-sm font-medium border shadow focus:outline-none focus:ring transition text-gray-600 bg-gray-50 border-gray-200 hover:bg-gray-100 active:bg-gray-200 focus:ring-gray-300"
                                 onclick="reset()" name="texto"
                                 style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Limpar
                              </button>
                           </div>
                        <div>
                     </div>';
         return $output;
      }
   }
}
