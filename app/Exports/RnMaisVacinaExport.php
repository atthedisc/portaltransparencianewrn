<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class RnMaisVacinaExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $tabela = DB::table('vacinacaocompraseservicos')
            ->select('contratante', 'objeto', 'contratado', 'cnpjcpf', 'atocontratual', 'nprocesso', 'valorcontrato', 'parcelaobjeto', 'dataassinatura', 'vigencia', 'localdeexecucao', 'atoautirizacaocontratacao', 'valorempenhado', 'quantidadeentrega', 'valorpago', 'valorapagar')
            ->orderBy('id')
            ->get();
        
        return $tabela;
    }

    public function headings(): array
    {
        return [
            'Contratante',
            'Objeto',
            'Contratado(a)',
            'CNPJ/CPF',
            'Ato Contratual',
            'N. Processo',
            'Valor do Contrato (R$)',
            'Parcelas do Objeto',
            'Data da Assinatura',
            'Vigência',
            'Local de Execução',
            'Ato de Autorização de Contrato',
            'Valor Empenhado (R$)',
            'Quantidade de Entrega',
            'Valor Pago',
            'Valor a Pagar'
        ];
    }
}
