<?php

namespace App\Exports;

use App\Models\ComprasPublicasNFe;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ComprasPublicasNFeExport implements FromQuery, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    use Exportable;

    public function __construct(int $ano, int $mes)
    {
        $this->ano = $ano;
        $this->mes = $mes;
    }

    public function query()
    {
        return ComprasPublicasNFe::query()
            ->select('razaosocialdestinatario', 'cnpjdestinatario', 'numnfe', 'dtemissao', 'chavenfe', 'razaosocialemitente', 'cnpjemitente',  'municipioorigem', 'uforigem', 'municipiodestino', 'ufdestino', 'vlroperacao')
            ->where('mes', $this->mes)
            ->where('ano', $this->ano)
            ->orderBy('razaosocialdestinatario');
    }

    public function headings(): array
    {
        return [
            'RAZÃO SOCIAL DESTINATARIO',
            'CNPJ DESTINATARIO',
            'NÚMERO DA NF-E',
            'DATA EMISSÃO',
            'CHAVE DA NF-E',
            'RAZÃO SOCIAL EMITENTE',
            'CNPJ EMITENTE',
            'MUNICÍPIO ORIGEM',
            'UF ORIGEM',
            'MUNICÍPIO DESTINO',
            'UF DESTINO',
            'VALOR TOTAL OPERAÇÃO'
        ];
    }
}
