<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;

class OrdemCronologicaExport implements FromQuery, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    use Exportable;

    public function __construct($mes, int $ano, string $orgao, string $fonte)
    {
        $this->mes = $mes;
        $this->ano = $ano;
        $this->orgao = $orgao;
        $this->fonte = $fonte;
    }

    public function query()
    {
        if ($this->mes == 'Todos') {
            return DB::connection('pgsql_siaf')->table('vw_vportalordemcronopag')
                ->where('CDFONTE', $this->fonte)
                ->where('cdunidadegestora', $this->orgao)
                ->where('ano', $this->ano)
                ->orderBy('cdunidadegestora');
        } else {
            return DB::connection('pgsql_siaf')->table('vw_vportalordemcronopag')
                ->where('CDFONTE', $this->fonte)
                ->where('cdunidadegestora', $this->orgao)
                ->where('ano', $this->ano)
                ->where('mes', $this->mes)
                ->orderBy('cdunidadegestora');
        }
    }

    public function headings(): array
    {
        return [
            "Cod. Unidade Gestora",
            "Num. Unidade Gestora",
            "Cod. Gestao",
            "Num. Gestao",
            "Num. Nota Empenho",
            "Cod. Identificador Uso",
            "Cod. Fonte",
            "Num. Fonte",
            "Cod. SubElemento",
            "Num. Modalidade",
            "Data Nota Empenho",
            "Num. Processo",
            "Referencia Legal",
            "Num. Nota Lancamento",
            "Data Nota Lancamento",
            "Data Emissao",
            "Tipo Documento Despesa",
            "Num. Documento",
            "Valor Documento",
            "Data Autuacao",
            "Atesto",
            "Data Vencimento",
            "Valor Liquido",
            "Num. Preparacao Pagamento",
            "Data Pagamento",
            "Valor Preparacao Pagamento",
            "Credor",
            "Num. Credor",
            "Justificativa Quebra de Ordem",
            "Ordenador Despesa",
            "Data Quebra Ordem Cronologica",
            "Mes",
            "Ano",

        ];
    }
}
