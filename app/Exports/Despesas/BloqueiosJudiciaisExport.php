<?php

namespace App\Exports\Despesas;

use App\Models\BloqueiosJudiciais;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;

class BloqueiosJudiciaisExport implements FromQuery, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    use Exportable;

    public function __construct(int $mes, int $ano)
    {
        $this->mes = $mes;
        $this->ano = $ano;
    }

    public function query()
    {
        return BloqueiosJudiciais::query()->select('mes', 'valor')
            ->where('mes', '<=', $this->mes)
            ->where('numexerciciofinanc', $this->ano)
            ->orderBy('mes');
    }

    public function headings(): array
    {
        return [
            'Mês',
            'Saldo Mensal bloqueio'
        ];
    }
}
