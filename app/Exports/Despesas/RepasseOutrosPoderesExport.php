<?php

namespace App\Exports\Despesas;

use App\Models\RepasseOutrosPoderes;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;

class RepasseOutrosPoderesExport implements FromQuery, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    use Exportable;

    public function __construct(int $mes, int $ano, string $posicao)
    {
        $this->mes = $mes;
        $this->ano = $ano;
        $this->posicao = $posicao;
    }

    public function query()
    {
        return RepasseOutrosPoderes::query()->select('txtpoder', DB::raw('SUM(total_vlrpagamento) as total_vlrpagamento'))
            ->where('numexerciciofinanc', $this->ano)
            ->where('mes_repasse_outros_poderes', $this->posicao, $this->mes)
            ->groupBy('txtpoder')
            ->orderBy('total_vlrpagamento');
    }

    public function headings(): array
    {
        return [
            'Poder',
            'Valor Total Repassado'
        ];
    }
}
