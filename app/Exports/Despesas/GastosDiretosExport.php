<?php

namespace App\Exports\Despesas;

use App\Models\GastosDireto\LiquidacaoGastosDiretos;
use App\Models\GastosDireto\NotaEmpenhoGastosDiretos;
use App\Models\GastosDireto\NotaPagamentoGastosDiretos;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;

class GastosDiretosExport implements FromQuery, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    use Exportable;

    private $mes;
    private $ano;
    private $posicao;
    private $classificacao;
    private $fase;

    public function __construct(int $mes, int $ano, string $posicao, string $classificacao, string $fase = null)
    {
        $this->mes = $mes;
        $this->ano = $ano;
        $this->posicao = $posicao;
        $this->classificacao = $classificacao;
        $this->fase = $fase;
    }

    public function query()
    {
        // Pagamento
        if ($this->fase == 'vportal_notapagamento_gastosdiretos') {
            // Por Ação
            if ($this->classificacao == 'acao') {
                return NotaPagamentoGastosDiretos::query()
                    ->select('codacao', 'txtdescricaoacao', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
                    ->where('mes', $this->posicao, $this->mes)
                    ->where('numexerciciofinanc', $this->ano)
                    ->groupBy('codacao', 'txtdescricaoacao')
                    ->orderBy('txtdescricaoacao');
            }
            // Por Fonte
            else if ($this->classificacao == 'fonte') {
                return NotaPagamentoGastosDiretos::query()
                    ->select('txtdescricaofonterecurso', 'codunidadegestora', 'txtdescricaounidade', 'codgestao', 'txtdescricaogestao', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
                    ->where('mes', $this->posicao, $this->mes)
                    ->where('numexerciciofinanc', $this->ano)
                    ->groupBy('txtdescricaofonterecurso', 'codunidadegestora', 'txtdescricaounidade', 'codgestao', 'txtdescricaogestao')
                    ->orderBy('txtdescricaofonterecurso');
            }
            // Por Favorecido
            else if ($this->classificacao == 'favorecido') {
                return NotaPagamentoGastosDiretos::query()
                    ->select('codcredor', 'txtnomecredor', 'txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'codunidadegestora', 'txtdescricaounidade', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
                    ->where('mes', $this->posicao, $this->mes)
                    ->where('numexerciciofinanc', $this->ano)
                    ->groupBy('codcredor', 'txtnomecredor', 'txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'codunidadegestora', 'txtdescricaounidade')
                    ->orderBy('txtnomecredor');
            }
            // Por Despesa
            else if ($this->classificacao == 'despesa') {
                return NotaPagamentoGastosDiretos::query()
                    ->select('txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'txtdescricaounidade', 'txtdescricaogestao', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
                    ->where('mes', $this->posicao, $this->mes)
                    ->where('numexerciciofinanc', $this->ano)
                    ->groupBy('txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'txtdescricaounidade', 'txtdescricaogestao')
                    ->orderBy('txtdescricaogrupodespesa');
            }
            // Por Unidade Gestora
            else if ($this->classificacao == 'gestora') {
                return NotaPagamentoGastosDiretos::query()
                    ->select('codunidadegestora', 'txtdescricaounidade', 'codgestao', 'txtdescricaogestao', 'codcredor', 'txtnomecredor', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
                    ->where('mes', $this->posicao, $this->mes)
                    ->where('numexerciciofinanc', $this->ano)
                    ->groupBy('codunidadegestora', 'txtdescricaounidade', 'codgestao', 'txtdescricaogestao', 'codcredor', 'txtnomecredor')
                    ->orderBy('txtdescricaounidade');
            }
            // Por Funcao
            else if ($this->classificacao == 'funcao') {
                return NotaPagamentoGastosDiretos::query()
                    ->select('codfuncao', 'txtdescricaofuncao', 'txtdescricaosubfuncao', 'txtdescricaoacao', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
                    ->where('mes', $this->posicao, $this->mes)
                    ->where('numexerciciofinanc', $this->ano)
                    ->groupBy('codfuncao', 'txtdescricaofuncao', 'txtdescricaosubfuncao', 'txtdescricaoacao')
                    ->orderBy('txtdescricaofuncao');
            }
        }
        // Empenho
        else if ($this->fase == 'vportal_notaempenho_gastosdiretos') {
            // Por Ação
            if ($this->classificacao == 'acao') {
                return NotaEmpenhoGastosDiretos::query()
                    ->select('codacao', 'txtdescricaoacao', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
                    ->where('mes', $this->posicao, $this->mes)
                    ->where('numexerciciofinanc', $this->ano)
                    ->groupBy('codacao', 'txtdescricaoacao')
                    ->orderBy('txtdescricaoacao');
            }
            // Por Fonte
            else if ($this->classificacao == 'fonte') {
                return NotaEmpenhoGastosDiretos::query()
                    ->select('txtdescricaofonterecurso', 'codunidadegestora', 'txtdescricaounidade', 'codgestao', 'txtdescricaogestao', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
                    ->where('mes', $this->posicao, $this->mes)
                    ->where('numexerciciofinanc', $this->ano)
                    ->groupBy('txtdescricaofonterecurso', 'codunidadegestora', 'txtdescricaounidade', 'codgestao', 'txtdescricaogestao')
                    ->orderBy('txtdescricaofonterecurso');
            }
            // Por Favorecido
            else if ($this->classificacao == 'favorecido') {
                return NotaEmpenhoGastosDiretos::query()
                    ->select('codcredor', 'txtnomecredor', 'txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'codunidadegestora', 'txtdescricaounidade', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
                    ->where('mes', $this->posicao, $this->mes)
                    ->where('numexerciciofinanc', $this->ano)
                    ->groupBy('codcredor', 'txtnomecredor', 'txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'codunidadegestora', 'txtdescricaounidade')
                    ->orderBy('txtnomecredor');
            }
            // Por Despesa
            else if ($this->classificacao == 'despesa') {
                return NotaEmpenhoGastosDiretos::query()
                    ->select('txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'txtdescricaounidade', 'txtdescricaogestao', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
                    ->where('mes', $this->posicao, $this->mes)
                    ->where('numexerciciofinanc', $this->ano)
                    ->groupBy('txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'txtdescricaounidade', 'txtdescricaogestao')
                    ->orderBy('txtdescricaogrupodespesa');
            }
            // Por Unidade Gestora
            else if ($this->classificacao == 'gestora') {
                return NotaEmpenhoGastosDiretos::query()
                    ->select('codunidadegestora', 'txtdescricaounidade', 'codgestao', 'txtdescricaogestao', 'codcredor', 'txtnomecredor', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
                    ->where('mes', $this->posicao, $this->mes)
                    ->where('numexerciciofinanc', $this->ano)
                    ->groupBy('codunidadegestora', 'txtdescricaounidade', 'codgestao', 'txtdescricaogestao', 'codcredor', 'txtnomecredor')
                    ->orderBy('txtdescricaounidade');
            }
            // Por Funcao
            else if ($this->classificacao == 'funcao') {
                return NotaEmpenhoGastosDiretos::query()
                    ->select('codfuncao', 'txtdescricaofuncao', 'txtdescricaosubfuncao', 'txtdescricaoacao', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
                    ->where('mes', $this->posicao, $this->mes)
                    ->where('numexerciciofinanc', $this->ano)
                    ->groupBy('codfuncao', 'txtdescricaofuncao', 'txtdescricaosubfuncao', 'txtdescricaoacao')
                    ->orderBy('txtdescricaofuncao');
            }
        }
        // Liquidação
        else if ($this->fase == 'vportal_liquidacaodespesa_gastosdiretos') {
            // Por Ação
            if ($this->classificacao == 'acao') {
                return LiquidacaoGastosDiretos::query()
                    ->select('codacao', 'txtdescricaoacao', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
                    ->where('mes', $this->posicao, $this->mes)
                    ->where('numexerciciofinanc', $this->ano)
                    ->groupBy('codacao', 'txtdescricaoacao')
                    ->orderBy('txtdescricaoacao');
            }
            // Por Fonte
            else if ($this->classificacao == 'fonte') {
                return LiquidacaoGastosDiretos::query()
                    ->select('txtdescricaofonterecurso', 'codunidadegestora', 'txtdescricaounidade', 'codgestao', 'txtdescricaogestao', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
                    ->where('mes', $this->posicao, $this->mes)
                    ->where('numexerciciofinanc', $this->ano)
                    ->groupBy('txtdescricaofonterecurso', 'codunidadegestora', 'txtdescricaounidade', 'codgestao', 'txtdescricaogestao')
                    ->orderBy('txtdescricaofonterecurso');
            }
            // Por Favorecido
            else if ($this->classificacao == 'favorecido') {
                return LiquidacaoGastosDiretos::query()
                    ->select('codcredor', 'txtnomecredor', 'txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'codunidadegestora', 'txtdescricaounidade', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
                    ->where('mes', $this->posicao, $this->mes)
                    ->where('numexerciciofinanc', $this->ano)
                    ->groupBy('codcredor', 'txtnomecredor', 'txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'codunidadegestora', 'txtdescricaounidade')
                    ->orderBy('txtnomecredor');
            }
            // Por Despesa
            else if ($this->classificacao == 'despesa') {
                return LiquidacaoGastosDiretos::query()
                    ->select('txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'txtdescricaounidade', 'txtdescricaogestao', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
                    ->where('mes', $this->posicao, $this->mes)
                    ->where('numexerciciofinanc', $this->ano)
                    ->groupBy('txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'txtdescricaounidade', 'txtdescricaogestao')
                    ->orderBy('txtdescricaogrupodespesa');
            }
            // Por Unidade Gestora
            else if ($this->classificacao == 'gestora') {
                return LiquidacaoGastosDiretos::query()
                    ->select('codunidadegestora', 'txtdescricaounidade', 'codgestao', 'txtdescricaogestao', 'codcredor', 'txtnomecredor', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
                    ->where('mes', $this->posicao, $this->mes)
                    ->where('numexerciciofinanc', $this->ano)
                    ->groupBy('codunidadegestora', 'txtdescricaounidade', 'codgestao', 'txtdescricaogestao', 'codcredor', 'txtnomecredor')
                    ->orderBy('txtdescricaounidade');
            }
            // Por Funcao
            else if ($this->classificacao == 'funcao') {
                return LiquidacaoGastosDiretos::query()
                    ->select('codfuncao', 'txtdescricaofuncao', 'txtdescricaosubfuncao', 'txtdescricaoacao', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
                    ->where('mes', $this->posicao, $this->mes)
                    ->where('numexerciciofinanc', $this->ano)
                    ->groupBy('codfuncao', 'txtdescricaofuncao', 'txtdescricaosubfuncao', 'txtdescricaoacao')
                    ->orderBy('txtdescricaofuncao');
            }
        } else {
            if ($this->classificacao == 'despesa') {
                $subquery = DB::connection('pgsql_siaf')
                ->table('vportal_notaempenho_gastosdiretos')
                ->select('txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'txtdescricaounidade', 'txtdescricaogestao')
                ->selectRaw('SUM(vlrpagamento) AS vlrempenho,
                            COALESCE(null, 0) as vlrliquido, 
                            COALESCE(null, 0) as vlrpagamento, 
                            COALESCE(null, 0) as vlrpagamentorp')
                ->where('numexerciciofinanc', $this->ano)
                ->where('mes', $this->posicao, $this->mes)
                ->groupBy('txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'txtdescricaounidade', 'txtdescricaogestao');

                $subquery2 = DB::connection('pgsql_siaf')
                ->table('vportal_liquidacaodespesa_gastosdiretos')
                ->select('txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'txtdescricaounidade', 'txtdescricaogestao')
                ->selectRaw('COALESCE(null, 0) as vlrempenho, 
                            SUM(vlrpagamento) AS vlrliquido,
                            COALESCE(null, 0) as vlrpagamento, 
                            COALESCE(null, 0) as vlrpagamentorp')
                ->where('numexerciciofinanc', $this->ano)
                ->where('mes', $this->posicao, $this->mes)
                ->groupBy('txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'txtdescricaounidade', 'txtdescricaogestao');

                $subquery3 = DB::connection('pgsql_siaf')
                ->table('vportal_notapagamento_gastosdiretos')
                ->select('txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'txtdescricaounidade', 'txtdescricaogestao')
                ->selectRaw('COALESCE(null, 0) AS vlrempenho,
                            COALESCE(null, 0) AS vlrliquido,
                            SUM(vlrpagamento) AS vlrpagamento,
                            SUM(vlrpagamentorp) AS vlrpagamentorp')
                ->where('numexerciciofinanc', $this->ano)
                ->where('mes', $this->posicao, $this->mes)
                ->groupBy('txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'txtdescricaounidade', 'txtdescricaogestao');

                return DB::connection('pgsql_siaf')
                    ->table(DB::raw("({$subquery->toSql()} UNION ALL {$subquery2->toSql()} UNION ALL {$subquery3->toSql()}) AS resultado"))
                    ->setBindings(array_merge(
                    $subquery->getBindings(),
                    $subquery2->getBindings(),
                    $subquery3->getBindings()
                ))
                    ->select(
                    'resultado.txtdescricaogrupodespesa', 
                    'resultado.txtdescricaoelementodespesa', 
                    'resultado.txtdescricaounidade',
                    DB::raw('COALESCE(SUM(resultado.vlrempenho), 0) AS vlrempenho'), 
                    DB::raw('COALESCE(SUM(resultado.vlrliquido), 0) AS vlrliquido'), 
                    DB::raw('COALESCE(SUM(resultado.vlrpagamento), 0) AS vlrpagamento'), 
                    DB::raw('COALESCE(SUM(resultado.vlrpagamentorp), 0) AS vlrpagamentorp')
                    )
                    ->groupBy(
                    'resultado.txtdescricaogrupodespesa', 
                    'resultado.txtdescricaoelementodespesa', 
                    'resultado.txtdescricaounidade'
                    )
                    ->orderBy('resultado.txtdescricaogrupodespesa', 'ASC')
                    ->orderBy('resultado.txtdescricaoelementodespesa', 'ASC')
                    ->orderBy('resultado.txtdescricaounidade', 'ASC');
            }
            else if ($this->classificacao == 'gestora') {
                
                $subquery = DB::connection('pgsql_siaf')
                ->table('vportal_notaempenho_gastosdiretos')
                ->select('codunidadegestora', 'codgestao', 'codcredor', 'txtdescricaounidade', 'txtdescricaogestao', 'txtnomecredor')
                ->selectRaw("SUM(vlrpagamento) AS vlrempenho,
                             COALESCE(null, 0) as vlrliquido, 
                             COALESCE(null, 0) as vlrpagamento, 
		                     COALESCE(null, 0) as vlrpagamentorp,
                             'vportal_notaempenho_gastosdiretos' as tbvportal")
                ->where('numexerciciofinanc', $this->ano)
                ->groupBy('codunidadegestora', 'codgestao', 'codcredor', 'txtdescricaounidade', 'txtdescricaogestao', 'txtnomecredor');
                if($this->posicao === '='){
                   $subquery->where('mes', '=', $this->mes);
                }else{
                   $subquery->where('mes','<=', $this->mes);
                }
            
                $subquery2 = DB::connection('pgsql_siaf')
                   ->table('vportal_liquidacaodespesa_gastosdiretos')
                   ->select('codunidadegestora', 'codgestao', 'codcredor', 'txtdescricaounidade', 'txtdescricaogestao', 'txtnomecredor')
                   ->selectRaw("COALESCE(null, 0) as vlrempenho, 
                                SUM(vlrpagamento) AS vlrliquido,
                                COALESCE(null, 0) as vlrpagamento, 
		                        COALESCE(null, 0) as vlrpagamentorp,
                                'vportal_liquidacaodespesa_gastosdiretos' as tbvportal")
                   ->where('numexerciciofinanc', $this->ano)
                   ->groupBy('codunidadegestora', 'codgestao', 'codcredor', 'txtdescricaounidade', 'txtdescricaogestao', 'txtnomecredor'); 
            
                if($this->posicao === '='){
                   $subquery2->where('mes', '=', $this->mes);
                }else{
                   $subquery2->where('mes','<=', $this->mes);
                }
            
                $subquery3 = DB::connection('pgsql_siaf')
                   ->table('vportal_notapagamento_gastosdiretos')
                   ->select('codunidadegestora', 'codgestao', 'codcredor', 'txtdescricaounidade', 'txtdescricaogestao', 'txtnomecredor')
                   ->selectRaw("COALESCE(null, 0) AS vlrempenho,
                                COALESCE(null, 0) AS vlrliquido,
                                SUM(vlrpagamento) AS vlrpagamento,
                                SUM(vlrpagamentorp) AS vlrpagamentorp,
                                'vportal_notapagamento_gastosdiretos' as tbvportal")
                   ->where('numexerciciofinanc', $this->ano)
                   ->groupBy('codunidadegestora', 'codgestao', 'codcredor', 'txtdescricaounidade', 'txtdescricaogestao', 'txtnomecredor'); 
            
                   if($this->posicao === '='){
                      $subquery3->where('mes', '=', $this->mes);
                   }else{
                      $subquery3->where('mes','<=', $this->mes);
                   }
               
                return DB::connection('pgsql_siaf')
                   ->table(DB::raw("({$subquery->toSql()} UNION ALL {$subquery2->toSql()} UNION ALL {$subquery3->toSql()}) AS resultado"))
                   ->setBindings(array_merge(
                      $subquery->getBindings(),
                      $subquery2->getBindings(),
                      $subquery3->getBindings()
                  ))
                   ->select(
                      'resultado.txtdescricaounidade', 
                      'resultado.txtdescricaogestao', 
                      'resultado.codcredor',
                      'resultado.txtnomecredor', 
                      DB::raw('COALESCE(SUM(resultado.vlrempenho), 0) AS vlrempenho'), 
                      DB::raw('COALESCE(SUM(resultado.vlrliquido), 0) AS vlrliquido'), 
                      DB::raw('COALESCE(SUM(resultado.vlrpagamento), 0) AS vlrpagamento'), 
                      DB::raw('COALESCE(SUM(resultado.vlrpagamentorp), 0) AS vlrpagamentorp')
                   )
                   ->groupBy(
                      'resultado.txtdescricaounidade', 
                      'resultado.txtdescricaogestao', 
                      'resultado.codcredor',
                      'resultado.txtnomecredor'
                   )
                   ->orderBy('resultado.txtdescricaounidade', 'ASC')
                   ->orderBy('resultado.txtnomecredor', 'ASC');
            }
            else if ($this->classificacao == 'acao') {     
                $subquery = DB::connection('pgsql_siaf')
                    ->table('vportal_notaempenho_gastosdiretos')
                    ->select('codacao')
                    ->selectRaw("INITCAP(txtdescricaoacao) as txtdescricaoacao,
                                 SUM(vlrpagamento) AS vlrempenho, 
                                 COALESCE(null, 0) as vlrliquido,
                                 COALESCE(null, 0) as vlrpagamento, 
                                 COALESCE(null, 0) as vlrpagamentorp,
                                 'vportal_notaempenho_gastosdiretos' as tbvportal,
                                 'notaEmpenho' as tipo")
                    ->where('numexerciciofinanc', $this->ano)
                    ->groupBy('codacao', 'txtdescricaoacao');

                if($this === '='){
                   $subquery->where('mes', '=', $this->mes);
                }else{
                   $subquery->where('mes','<=', $this->mes);
                }

                $subquery2 = DB::connection('pgsql_siaf')
                   ->table('vportal_liquidacaodespesa_gastosdiretos')
                   ->select('codacao')
                   ->selectRaw("INITCAP(txtdescricaoacao) as txtdescricaoacao,
                                COALESCE(null, 0) as vlrempenho, 
                                SUM(vlrpagamento) AS vlrliquido,
                                COALESCE(null, 0) as vlrpagamento, 
		                          COALESCE(null, 0) as vlrpagamentorp,
                                'vportal_liquidacaodespesa_gastosdiretos' as tbvportal,
                                'liquidacao' as tipo")
                   ->where('numexerciciofinanc', $this->ano)
                   ->groupBy('codacao', 'txtdescricaoacao');

                if($this->posicao === '='){
                   $subquery2->where('mes', '=', $this->mes);
                }else{
                   $subquery2->where('mes','<=', $this->mes);
                }

                $subquery3 = DB::connection('pgsql_siaf')
                   ->table('vportal_notapagamento_gastosdiretos')
                   ->select('codacao')
                   ->selectRaw("INITCAP(txtdescricaoacao) as txtdescricaoacao,
                                COALESCE(null, 0) AS vlrempenho,
                                COALESCE(null, 0) AS vlrliquido,
                                SUM(vlrpagamento) AS vlrpagamento,
                                SUM(vlrpagamentorp) AS vlrpagamentorp,
                                'vportal_notapagamento_gastosdiretos' as tbvportal,
                                case when SUM(vlrpagamento) > 0 then 'notaPagamento'
                	                     when SUM(vlrpagamentorp)  > 0 then 'valorPagar'
                                end as tipo")
                   ->where('numexerciciofinanc', $this->ano)
                   ->groupBy('codacao', 'txtdescricaoacao');

                   if($this->posicao === '='){
                      $subquery3->where('mes', '=', $this->mes);
                   }else{
                      $subquery3->where('mes','<=', $this->mes);
                   }

                return DB::connection('pgsql_siaf')
                   ->table(DB::raw("({$subquery->toSql()} UNION ALL {$subquery2->toSql()} UNION ALL {$subquery3->toSql()}) AS resultado"))
                   ->setBindings(array_merge(
                      $subquery->getBindings(),
                      $subquery2->getBindings(),
                      $subquery3->getBindings()
                  ))
                   ->select(
                      'resultado.txtdescricaoacao',
                      DB::raw('COALESCE(SUM(resultado.vlrempenho), 0) AS vlrempenho'), 
                      DB::raw('COALESCE(SUM(resultado.vlrliquido), 0) AS vlrliquido'), 
                      DB::raw('COALESCE(SUM(resultado.vlrpagamento), 0) AS vlrpagamento'), 
                      DB::raw('COALESCE(SUM(resultado.vlrpagamentorp), 0) AS vlrpagamentorp')
                   )
                   ->groupBy(
                       'resultado.txtdescricaoacao'
                   )
                   ->orderBy('resultado.txtdescricaoacao', 'ASC');
            }
            else if($this->classificacao == 'fonte'){
                $subquery = DB::connection('pgsql_siaf')
                    ->table('vportal_notaempenho_gastosdiretos')
                    ->select('codfonterecurso',
                             'codunidadegestora',
                             'codgestao',
                             'txtdescricaofonterecurso',
                             'txtdescricaounidade',
                             'txtdescricaogestao')
                    ->selectRaw("SUM(vlrpagamento) AS vlrempenho,
                                 COALESCE(null, 0) as vlrliquido, 
                                 COALESCE(null, 0) as vlrpagamento, 
		                           COALESCE(null, 0) as vlrpagamentorp,
                                 'vportal_notaempenho_gastosdiretos' as tbvportal,
                                 'notaEmpenho' as tipo")
                    ->where('numexerciciofinanc', $this->ano)
                    ->groupBy('codfonterecurso', 'codunidadegestora', 'codgestao', 'txtdescricaofonterecurso', 'txtdescricaounidade', 'txtdescricaogestao');
                if($this === '='){
                   $subquery->where('mes', '=', $this->mes);
                }else{
                   $subquery->where('mes','<=', $this->mes);
                }

                $subquery2 = DB::connection('pgsql_siaf')
                   ->table('vportal_liquidacaodespesa_gastosdiretos')
                   ->select('codfonterecurso',
                            'codunidadegestora',
                            'codgestao',
                            'txtdescricaofonterecurso',
                            'txtdescricaounidade',
                            'txtdescricaogestao')
                   ->selectRaw("COALESCE(null, 0) as vlrempenho, 
                                SUM(vlrpagamento) AS vlrliquido,
                                COALESCE(null, 0) as vlrpagamento, 
		                          COALESCE(null, 0) as vlrpagamentorp,
                                'vportal_liquidacaodespesa_gastosdiretos' as tbvportal,
                                'liquidacao' as tipo")
                   ->where('numexerciciofinanc', $this->ano)
                   ->groupBy('codfonterecurso', 'codunidadegestora', 'codgestao', 'txtdescricaofonterecurso', 'txtdescricaounidade', 'txtdescricaogestao');

                if($this === '='){
                   $subquery2->where('mes', '=', $this->mes);
                }else{
                   $subquery2->where('mes','<=', $this->mes);
                }
            
                $subquery3 = DB::connection('pgsql_siaf')
                   ->table('vportal_notapagamento_gastosdiretos')
                   ->select('codfonterecurso',
                            'codunidadegestora',
                            'codgestao',
                            'txtdescricaofonterecurso',
                            'txtdescricaounidade',
                            'txtdescricaogestao')
                   ->selectRaw("COALESCE(null, 0) AS vlrempenho,
                                COALESCE(null, 0) AS vlrliquido,
                                SUM(vlrpagamento) AS vlrpagamento,
                                SUM(vlrpagamentorp) AS vlrpagamentorp,
                                'vportal_notapagamento_gastosdiretos' as tbvportal,
                                case when SUM(vlrpagamento) > 0 then 'notaPagamento'
                                     when SUM(vlrpagamentorp)  > 0 then 'valorPagar'
                                end as tipo")
                   ->where('numexerciciofinanc', $this->ano)
                   ->groupBy('codfonterecurso', 'codunidadegestora', 'codgestao', 'txtdescricaofonterecurso', 'txtdescricaounidade', 'txtdescricaogestao');
            
                   if($this === '='){
                      $subquery3->where('mes', '=', $this->mes);
                   }else{
                      $subquery3->where('mes','<=', $this->mes);
                   }
               
                return DB::connection('pgsql_siaf')
                   ->table(DB::raw("({$subquery->toSql()} UNION ALL {$subquery2->toSql()} UNION ALL {$subquery3->toSql()}) AS resultado"))
                   ->setBindings(array_merge(
                      $subquery->getBindings(),
                      $subquery2->getBindings(),
                      $subquery3->getBindings()
                  ))
                   ->select(
                      'resultado.txtdescricaofonterecurso',
                      'resultado.codunidadegestora',
                      'resultado.txtdescricaounidade',
                      'resultado.codgestao',
                      'resultado.txtdescricaogestao',
                      DB::raw('COALESCE(SUM(resultado.vlrempenho), 0) AS vlrempenho'), 
                      DB::raw('COALESCE(SUM(resultado.vlrliquido), 0) AS vlrliquido'), 
                      DB::raw('COALESCE(SUM(resultado.vlrpagamento), 0) AS vlrpagamento'), 
                      DB::raw('COALESCE(SUM(resultado.vlrpagamentorp), 0) AS vlrpagamentorp')
                   )
                   ->groupBy(
                      'resultado.codunidadegestora',
                      'resultado.codgestao',
                      'resultado.txtdescricaofonterecurso',
                      'resultado.txtdescricaounidade',
                      'resultado.txtdescricaogestao'
                   )
                   ->orderBy('resultado.txtdescricaofonterecurso', 'ASC')
                   ->orderBy('resultado.txtdescricaounidade', 'ASC')
                   ->orderBy('resultado.txtdescricaogestao', 'ASC');
            }
            else if ($this->classificacao == 'favorecido'){
                $subquery = DB::connection('pgsql_siaf')
                    ->table('vportal_notaempenho_gastosdiretos')
                    ->select('txtnomecredor',
                             'txtdescricaounidade',
                             'txtdescricaogrupodespesa',
                             'txtdescricaoelementodespesa')
                    ->selectRaw("codcredor::varchar as codcredor,
                                 codgrupodespesa::varchar as codgrupodespesa,
                                 codelementodespesa::varchar as codelementodespesa,
                                 codunidadegestora::varchar as codunidadegestora")
                    ->selectRaw("SUM(vlrpagamento) AS vlrempenho,
                                 COALESCE(null, 0) as vlrliquido, 
                                 COALESCE(null, 0) as vlrpagamento, 
                                 COALESCE(null, 0) as vlrpagamentorp,
                                 'vportal_notaempenho_gastosdiretos' as tbvportal,
                                 'notaEmpenho' as tipo")
                    ->where('numexerciciofinanc', $this->ano)
                    ->groupBy('codcredor', 'codgrupodespesa', 'codelementodespesa', 'codunidadegestora', 'txtnomecredor', 'txtdescricaounidade', 'txtdescricaogrupodespesa', 'txtdescricaoelementodespesa');

                    if($this === '='){
                       $subquery->where('mes', '=', $this->mes);
                    }else{
                       $subquery->where('mes','<=', $this->mes);
                    }

                    $subquery2 = DB::connection('pgsql_siaf')
                       ->table('vportal_liquidacaodespesa_gastosdiretos')
                       ->select('txtnomecredor',
                                'txtdescricaounidade',
                                'txtdescricaogrupodespesa',
                                'txtdescricaoelementodespesa')
                       ->selectRaw("codcredor::varchar as codcredor,
                                    codgrupodespesa::varchar as codgrupodespesa,
                                    codelementodespesa::varchar as codelementodespesa,
                                    codunidadegestora::varchar as codunidadegestora")
                       ->selectRaw("COALESCE(null, 0) as vlrempenho, 
                                    SUM(vlrpagamento) AS vlrliquido,
                                    COALESCE(null, 0) as vlrpagamento, 
                                    COALESCE(null, 0) as vlrpagamentorp,
                                    'vportal_liquidacaodespesa_gastosdiretos' as tbvportal,
                                    'liquidacao' as tipo")
                       ->where('numexerciciofinanc', $this->ano)
                       ->groupBy('codcredor', 'codgrupodespesa', 'codelementodespesa', 'codunidadegestora', 'txtnomecredor', 'txtdescricaounidade', 'txtdescricaogrupodespesa', 'txtdescricaoelementodespesa');


                    if($this === '='){
                       $subquery2->where('mes', '=', $this->mes);
                    }else{
                       $subquery2->where('mes','<=', $this->mes);
                    }
                
                    $subquery3 = DB::connection('pgsql_siaf')
                       ->table('vportal_notapagamento_gastosdiretos')
                       ->select('txtnomecredor',
                                'txtdescricaounidade',
                                'txtdescricaogrupodespesa',
                                'txtdescricaoelementodespesa')
                       ->selectRaw("codcredor::varchar as codcredor,
                                    codgrupodespesa::varchar as codgrupodespesa,
                                    codelementodespesa::varchar as codelementodespesa,
                                    codunidadegestora::varchar as codunidadegestora")
                       ->selectRaw("COALESCE(null, 0) AS vlrempenho,
                                    COALESCE(null, 0) AS vlrliquido,
                                    SUM(vlrpagamento) AS vlrpagamento,
                                    SUM(vlrpagamentorp) AS vlrpagamentorp,
                                    'vportal_notapagamento_gastosdiretos' as tbvportal,
                                    case when SUM(vlrpagamento) > 0 then 'notaPagamento'
                                         when SUM(vlrpagamentorp)  > 0 then 'valorPagar'
                                    end as tipo")
                       ->where('numexerciciofinanc', $this->ano)
                       ->groupBy('codcredor', 'codgrupodespesa', 'codelementodespesa', 'codunidadegestora', 'txtnomecredor', 'txtdescricaounidade', 'txtdescricaogrupodespesa', 'txtdescricaoelementodespesa');
                
                       if($this === '='){
                          $subquery3->where('mes', '=', $this->mes);
                       }else{
                          $subquery3->where('mes','<=', $this->mes);
                       }
                   
                    return DB::connection('pgsql_siaf')
                       ->table(DB::raw("({$subquery->toSql()} UNION ALL {$subquery2->toSql()} UNION ALL {$subquery3->toSql()}) AS resultado"))
                       ->setBindings(array_merge(
                          $subquery->getBindings(),
                          $subquery2->getBindings(),
                          $subquery3->getBindings()
                      ))
                       ->select(
                          'resultado.codcredor',
                          'resultado.txtnomecredor',
                          'resultado.txtdescricaogrupodespesa',
                          'resultado.txtdescricaoelementodespesa',
                          DB::raw('COALESCE(SUM(resultado.vlrempenho), 0) AS vlrempenho'), 
                          DB::raw('COALESCE(SUM(resultado.vlrliquido), 0) AS vlrliquido'), 
                          DB::raw('COALESCE(SUM(resultado.vlrpagamento), 0) AS vlrpagamento'), 
                          DB::raw('COALESCE(SUM(resultado.vlrpagamentorp), 0) AS vlrpagamentorp')
                       )
                       ->groupBy(
                          'resultado.codcredor',
                          'resultado.txtnomecredor',
                          'resultado.txtdescricaogrupodespesa',
                          'resultado.txtdescricaoelementodespesa'
                       )
                       ->orderBy('resultado.txtnomecredor', 'ASC')
                       ->orderBy('resultado.txtdescricaogrupodespesa', 'ASC')
                       ->orderBy('resultado.txtdescricaoelementodespesa', 'ASC');
            }
            else if ($this->classificacao == 'funcao'){
                $subquery = DB::connection('pgsql_siaf')
                    ->table('vportal_notaempenho_gastosdiretos')
                    ->select('txtdescricaofuncao',
                             'txtdescricaosubfuncao',
                             'txtdescricaoacao')
                    ->selectRaw("codfuncao::varchar as codfuncao,
                                 codacao::varchar as codacao,
                                 codsubfuncao::varchar as codsubfuncao")
                    ->selectRaw("SUM(vlrpagamento) AS vlrempenho,
                                 COALESCE(null, 0) as vlrliquido, 
                                 COALESCE(null, 0) as vlrpagamento, 
                                 COALESCE(null, 0) as vlrpagamentorp,
                                 'vportal_notaempenho_gastosdiretos' as tbvportal,
                                 'notaEmpenho' as tipo")
                    ->where('numexerciciofinanc', $this->ano)
                    ->groupBy('codacao', 'codsubfuncao', 'codfuncao', 'txtdescricaofuncao', 'txtdescricaosubfuncao', 'txtdescricaoacao');

                    if($this->posicao === '='){
                       $subquery->where('mes', '=', $this->mes);
                    }else{
                       $subquery->where('mes','<=', $this->mes);
                    }

                $subquery2 = DB::connection('pgsql_siaf')
                   ->table('vportal_liquidacaodespesa_gastosdiretos')
                   ->select('txtdescricaofuncao',
                            'txtdescricaosubfuncao',
                            'txtdescricaoacao')
                   ->selectRaw("codfuncao::varchar as codfuncao,
                                codacao::varchar as codacao,
                                codsubfuncao::varchar as codsubfuncao")
                   ->selectRaw("COALESCE(null, 0) as vlrempenho, 
                                SUM(vlrpagamento) AS vlrliquido,
                                COALESCE(null, 0) as vlrpagamento, 
                                COALESCE(null, 0) as vlrpagamentorp,
                                'vportal_liquidacaodespesa_gastosdiretos' as tbvportal,
                                'liquidacao' as tipo")
                   ->where('numexerciciofinanc', $this->ano)
                   ->groupBy('codacao', 'codsubfuncao', 'codfuncao', 'txtdescricaofuncao', 'txtdescricaosubfuncao', 'txtdescricaoacao');


                if($this->posicao === '='){
                   $subquery2->where('mes', '=', $this->mes);
                }else{
                   $subquery2->where('mes','<=', $this->mes);
                }
            
                $subquery3 = DB::connection('pgsql_siaf')
                   ->table('vportal_notapagamento_gastosdiretos')
                   ->select('txtdescricaofuncao',
                            'txtdescricaosubfuncao',
                            'txtdescricaoacao')
                   ->selectRaw("codfuncao::varchar as codfuncao,
                                codacao::varchar as codacao,
                                codsubfuncao::varchar as codsubfuncao")
                   ->selectRaw("COALESCE(null, 0) AS vlrempenho,
                                COALESCE(null, 0) AS vlrliquido,
                                SUM(vlrpagamento) AS vlrpagamento,
                                SUM(vlrpagamentorp) AS vlrpagamentorp,
                                'vportal_notapagamento_gastosdiretos' as tbvportal,
                                case when SUM(vlrpagamento) > 0 then 'notaPagamento'
                                     when SUM(vlrpagamentorp)  > 0 then 'valorPagar'
                                end as tipo")
                   ->where('numexerciciofinanc', $this->ano)
                   ->groupBy('codacao', 'codsubfuncao','codfuncao', 'txtdescricaofuncao', 'txtdescricaosubfuncao', 'txtdescricaoacao');
            
                   if($this->posicao === '='){
                      $subquery3->where('mes', '=', $this->mes);
                   }else{
                      $subquery3->where('mes','<=', $this->mes);
                   }
               
                return DB::connection('pgsql_siaf')
                   ->table(DB::raw("({$subquery->toSql()} UNION ALL {$subquery2->toSql()} UNION ALL {$subquery3->toSql()}) AS resultado"))
                   ->setBindings(array_merge(
                      $subquery->getBindings(),
                      $subquery2->getBindings(),
                      $subquery3->getBindings()
                  ))
                   ->select(
                      'resultado.txtdescricaofuncao',
                      'resultado.txtdescricaosubfuncao',
                      'resultado.txtdescricaoacao',
                      DB::raw('COALESCE(SUM(resultado.vlrempenho), 0) AS vlrempenho'), 
                      DB::raw('COALESCE(SUM(resultado.vlrliquido), 0) AS vlrliquido'), 
                      DB::raw('COALESCE(SUM(resultado.vlrpagamento), 0) AS vlrpagamento'), 
                      DB::raw('COALESCE(SUM(resultado.vlrpagamentorp), 0) AS vlrpagamentorp')
                   )
                   ->groupBy(
                      'resultado.txtdescricaofuncao',
                      'resultado.txtdescricaosubfuncao',
                      'resultado.txtdescricaoacao'
                   )
                   ->orderBy('resultado.txtdescricaofuncao', 'ASC')
                   ->orderBy('resultado.txtdescricaosubfuncao', 'ASC')
                   ->orderBy('resultado.txtdescricaoacao', 'ASC');
            }
        }
    }

    public function headings(): array
    {
        // Pagamento
        if ($this->fase == 'vportal_notapagamento_gastosdiretos') {
            // Por Ação
            if ($this->classificacao == 'acao') {
                return [
                    'Cod. Ação',
                    'Ação',
                    'Exercício Corrente',
                    'Restos a Pagar'
                ];
            }
            // Por Fonte
            else if ($this->classificacao == 'fonte') {
                return [
                    'Fonte de Recurso',
                    'Cod. Unidade Gestora',
                    'Unidade Gestora',
                    'Cod. Gestão',
                    'Gestão',
                    'Exercício Corrente',
                    'Restos a Pagar'
                ];
            }
            // Por Favorecido
            else if ($this->classificacao == 'favorecido') {
                return [
                    'CPF/CNPJ/IG',
                    'Favorecido',
                    'Grupo de Despesa',
                    'Elemento de Despesa',
                    'Cod. Unidade Gestora',
                    'Unidade Gestora',
                    'Exercício Corrente',
                    'Restos a Pagar'
                ];
            }
            // Por Despesa
            else if ($this->classificacao == 'despesa') {
                return [
                    'Grupo de Despesa',
                    'Elemento de Despesa',
                    'Unidade Gestora',
                    'Gestão',
                    'Exercício Corrente',
                    'Restos a Pagar'
                ];
            }
            // Por Unidade Gestora
            else if ($this->classificacao == 'gestora') {
                return [
                    'Cod. Unidade Gestora',
                    'Unidade Gestora',
                    'Cod. Gestão',
                    'Gestão',
                    'CNPJ/CPF/IG',
                    'Favorecido',
                    'Exercício Corrente',
                    'Restos a Pagar'
                ];
            }
            // Por Funcao
            else if ($this->classificacao == 'funcao') {
                return [
                    'Cod. Função',
                    'Função',
                    'SubFunção',
                    'Ação',
                    'Exercício Corrente',
                    'Restos a Pagar'
                ];
            }
        }
        // Empenho e Liquidação
        else {
            // Por Ação
            if ($this->classificacao == 'acao') {
                return [
                    'Ação',
                    'Valor Empenhado',
                    'Valor Liquidação',
                    'Valor Pagamento',
                    'Restos a Pagar'
                ];
            }
            // Por Fonte
            else if ($this->classificacao == 'fonte') {
                return [
                    'Fonte de Recurso',
                    'Cod. Unidade Gestora',
                    'Unidade Gestora',
                    'Cod. Gestão',
                    'Gestão',
                    'Valor Empenhado',
                    'Valor Liquidação',
                    'Valor Pagamento',
                    'Restos a Pagar'
                ];
            }
            // Por Favorecido
            else if ($this->classificacao == 'favorecido') {
                return [
                    'CPF/CNPJ/IG',
                    'Favorecido',
                    'Grupo de Despesa',
                    'Elemento de Despesa',
                    'Valor Empenhado',
                    'Valor Liquidação',
                    'Valor Pagamento',
                    'Restos a Pagar'
                ];
            }
            // Por Despesa
            else if ($this->classificacao == 'despesa') {
                return [
                    'Grupo de Despesa',
                    'Elemento de Despesa',
                    'Descrição da Unidade',
                    'Valor Empenhado',
                    'Valor Liquidação',
                    'Valor Pagamento',
                    'Restos a Pagar'
                ];
            }
            // Por Unidade Gestora
            else if ($this->classificacao == 'gestora') {
                return [
                    'Unidade Gestora',
                    'Gestão',
                    'CNPJ/CPF/IG',
                    'Favorecido',
                    'Valor Empenhado',
                    'Valor Liquidação',
                    'Valor Pagamento',
                    'Restos a Pagar'
                ];
            }
            // Por Funcao
            else if ($this->classificacao == 'funcao') {
                return [
                    'Função',
                    'SubFunção',
                    'Ação',
                    'Valor Empenhado',
                    'Valor Liquidação',
                    'Valor Pagamento',
                    'Restos a Pagar'
                ];
            }
        }
    }
}
