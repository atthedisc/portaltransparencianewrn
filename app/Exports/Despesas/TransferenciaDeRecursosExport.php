<?php

namespace App\Exports\Despesas;

use App\Models\TransferenciaDeRecursos\TransferenciaLiquidacao;
use App\Models\TransferenciaDeRecursos\TransferenciaNotaEmpenho;
use App\Models\TransferenciaDeRecursos\TransferenciaNotaPagamento;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;

class TransferenciaDeRecursosExport implements FromQuery, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    use Exportable;

    public function __construct(int $mes, int $ano, string $posicao, string $classificacao, string $fase)
    {
        $this->mes = $mes;
        $this->ano = $ano;
        $this->posicao = $posicao;
        $this->classificacao = $classificacao;
        $this->fase = $fase;
    }

    public function query()
    {
        // Pagamento
        if ($this->fase == 'vw_notapagamento_transferenciarecursos') {
            // Por Ação
            if ($this->classificacao == 'acao') {
                return TransferenciaNotaPagamento::query()
                    ->select('txtdescricaoacao', 'txtdescricaofuncao', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
                    ->where('mes', $this->posicao, $this->mes)
                    ->where('numexerciciofinanc', $this->ano)
                    ->groupBy('txtdescricaoacao', 'txtdescricaofuncao')
                    ->orderBy('txtdescricaoacao');
            }
            // Por Fonte
            else if ($this->classificacao == 'fonte') {
                return TransferenciaNotaPagamento::query()
                    ->select('txtdescricaofonterecurso', 'txtdescricaounidade', 'codgestao', 'txtdescricaogestao', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
                    ->where('mes', $this->posicao, $this->mes)
                    ->where('numexerciciofinanc', $this->ano)
                    ->groupBy('txtdescricaofonterecurso', 'txtdescricaounidade', 'codgestao', 'txtdescricaogestao')
                    ->orderBy('txtdescricaofonterecurso');
            }
            // Por Favorecido
            else if ($this->classificacao == 'favorecido') {
                return TransferenciaNotaPagamento::query()
                    ->select('codcredor', 'txtnomecredor', 'txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'codunidade', 'txtdescricaounidade', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
                    ->where('mes', $this->posicao, $this->mes)
                    ->where('numexerciciofinanc', $this->ano)
                    ->groupBy('codcredor', 'txtnomecredor', 'txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'codunidade', 'txtdescricaounidade')
                    ->orderBy('txtnomecredor');
            }
        }
        // Empenho
        else if ($this->fase == 'vw_notaempenho_transferenciarecursos') {
            // Por Ação
            if ($this->classificacao == 'acao') {
                return TransferenciaNotaEmpenho::query()
                    ->select('txtdescricaoacao', 'txtdescricaofuncao', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
                    ->where('mes', $this->posicao, $this->mes)
                    ->where('numexerciciofinanc', $this->ano)
                    ->groupBy('txtdescricaoacao', 'txtdescricaofuncao')
                    ->orderBy('txtdescricaoacao');
            }
            // Por Fonte
            else if ($this->classificacao == 'fonte') {
                return TransferenciaNotaEmpenho::query()
                    ->select('txtdescricaofonterecurso', 'txtdescricaounidade', 'codgestao', 'txtdescricaogestao', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
                    ->where('mes', $this->posicao, $this->mes)
                    ->where('numexerciciofinanc', $this->ano)
                    ->groupBy('txtdescricaofonterecurso', 'txtdescricaounidade', 'codgestao', 'txtdescricaogestao')
                    ->orderBy('txtdescricaofonterecurso');
            }
            // Por Favorecido
            else if ($this->classificacao == 'favorecido') {
                return TransferenciaNotaEmpenho::query()
                    ->select('codcredor', 'txtnomecredor', 'txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'codunidade', 'txtdescricaounidade', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
                    ->where('mes', $this->posicao, $this->mes)
                    ->where('numexerciciofinanc', $this->ano)
                    ->groupBy('codcredor', 'txtnomecredor', 'txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'codunidade', 'txtdescricaounidade')
                    ->orderBy('txtnomecredor');
            }
        }
        // Liquidação
        else if ($this->fase == 'vw_liquidacaodespesa_transferenciarecursos') {
            // Por Ação
            if ($this->classificacao == 'acao') {
                return TransferenciaLiquidacao::query()
                    ->select('txtdescricaoacao', 'txtdescricaofuncao', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
                    ->where('mes', $this->posicao, $this->mes)
                    ->where('numexerciciofinanc', $this->ano)
                    ->groupBy('txtdescricaoacao', 'txtdescricaofuncao')
                    ->orderBy('txtdescricaoacao');
            }
            // Por Fonte
            else if ($this->classificacao == 'fonte') {
                return TransferenciaLiquidacao::query()
                    ->select('txtdescricaofonterecurso', 'txtdescricaounidade', 'codgestao', 'txtdescricaogestao', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
                    ->where('mes', $this->posicao, $this->mes)
                    ->where('numexerciciofinanc', $this->ano)
                    ->groupBy('txtdescricaofonterecurso', 'txtdescricaounidade', 'codgestao', 'txtdescricaogestao')
                    ->orderBy('txtdescricaofonterecurso');
            }
            // Por Favorecido
            else if ($this->classificacao == 'favorecido') {
                return TransferenciaLiquidacao::query()
                    ->select('codcredor', 'txtnomecredor', 'txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'codunidade', 'txtdescricaounidade', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
                    ->where('mes', $this->posicao, $this->mes)
                    ->where('numexerciciofinanc', $this->ano)
                    ->groupBy('codcredor', 'txtnomecredor', 'txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'codunidade', 'txtdescricaounidade')
                    ->orderBy('txtnomecredor');
            }
        }
    }

    public function headings(): array
    {
        // Pagamento
        if ($this->fase == 'vw_notapagamento_transferenciarecursos') {
            // Por Ação
            if ($this->classificacao == 'acao') {
                return [
                    'Ação',
                    'Função',
                    'Exercício Corrente (R$)',
                    'Restos a Pagar (R$)'
                ];
            }
            // Por Fonte
            else if ($this->classificacao == 'fonte') {
                return [
                    'Fonte de Recurso',
                    'Unidade Gestora',
                    'Cod. Gestão',
                    'Gestão',
                    'Exercício Corrente (R$)',
                    'Restos a Pagar (R$)'
                ];
            }
            // Por Favorecido
            else if ($this->classificacao == 'favorecido') {
                return [
                    'CPF/CNPJ/IG',
                    'Favorecido',
                    'Grupo de Despesa',
                    'Elemento de Despesa',
                    'Cod. Unidade Gestora',
                    'Unidade Gestora',
                    'Exercício Corrente (R$)',
                    'Restos a Pagar (R$)'
                ];
            }
        }
        // Empenho e Liquidação
        else {
            // Por Ação
            if ($this->classificacao == 'acao') {
                return [
                    'Ação',
                    'Função',
                    'Valor (R$)'
                ];
            }
            // Por Fonte
            else if ($this->classificacao == 'fonte') {
                return [
                    'Fonte de Recurso',
                    'Unidade Gestora',
                    'Cod. Gestão',
                    'Gestão',
                    'Valor (R$)'
                ];
            }
            // Por Favorecido
            else if ($this->classificacao == 'favorecido') {
                return [
                    'CPF/CNPJ/IG',
                    'Favorecido',
                    'Grupo de Despesa',
                    'Elemento de Despesa',
                    'Cod. Unidade Gestora',
                    'Unidade Gestora',
                    'Valor (R$)'
                ];
            }
        }
    }
}
