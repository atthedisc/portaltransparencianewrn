<?php

namespace App\Exports\Receita;

use App\Models\Receitas;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReceitasExport implements FromQuery, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    use Exportable;

    public function __construct(int $mes, int $ano, string $posicao, string $classificacao)
    {
        $this->mes = $mes;
        $this->ano = $ano;
        $this->posicao = $posicao;
        $this->classificacao = $classificacao;
    }

    public function query()
    {
        // Por Receita
        if ($this->classificacao == 'receita') {
            return Receitas::query()
                ->select('codcategoria','txtdescricaocategoria', 'txtdescricaoorigem', 'txtdescricaoespecie', 'txtdescricaorubrica', 'txtdescricaoalinea', 'txtdescricaodetalhada', DB::raw('SUM(valorreceita) as valorreceita'), DB::raw('SUM(vlrprevisaoatualizado) as vlrprevisaoatualizado'))
                ->where('mes', $this->posicao, $this->mes)
                ->where('numexerciciofinanc', $this->ano)
                ->groupBy('codcategoria','txtdescricaocategoria', 'txtdescricaoorigem', 'txtdescricaoespecie', 'txtdescricaorubrica', 'txtdescricaoalinea', 'txtdescricaodetalhada')
                ->orderBy('txtdescricaocategoria');
        }
        // Por Fonte de Recurso
        else if ($this->classificacao == 'fonterecurso') {
            return Receitas::query()
                ->select('codfonterecurso','txtdescricaofonterecurso', 'txtdescricaocategoria', 'txtdescricaoorigem', 'txtdescricaoespecie', 'txtdescricaorubrica', 'txtdescricaoalinea', 'txtdescricaodetalhada', DB::raw('SUM(valorreceita) as valorreceita'), DB::raw('SUM(vlrprevisaoatualizado) as vlrprevisaoatualizado'))
                ->where('mes', $this->posicao, $this->mes)
                ->where('numexerciciofinanc', $this->ano)
                ->groupBy('codfonterecurso','txtdescricaofonterecurso', 'txtdescricaocategoria', 'txtdescricaoorigem', 'txtdescricaoespecie', 'txtdescricaorubrica', 'txtdescricaoalinea', 'txtdescricaodetalhada')
                ->orderBy('txtdescricaofonterecurso');
        }
        // Por Unidade Gestora
        else if ($this->classificacao == 'gestora') {
            return Receitas::query()
                ->select('txtdescricaounidade', 'txtdescricaogestao', 'txtdescricaocategoria', 'txtdescricaoorigem', 'txtdescricaoespecie', 'txtdescricaorubrica', 'txtdescricaoalinea', 'txtdescricaodetalhada', DB::raw('SUM(valorreceita) as valorreceita'), DB::raw('SUM(vlrprevisaoatualizado) as vlrprevisaoatualizado'))
                ->where('mes', $this->posicao, $this->mes)
                ->where('numexerciciofinanc', $this->ano)
                ->groupBy('txtdescricaounidade', 'txtdescricaogestao', 'txtdescricaocategoria', 'txtdescricaoorigem', 'txtdescricaoespecie', 'txtdescricaorubrica', 'txtdescricaoalinea', 'txtdescricaodetalhada')
                ->orderBy('txtdescricaounidade');
        }
    }

    public function headings(): array
    {
        // Por Receita
        if ($this->classificacao == 'receita') {
            return [
                'Código Receita',
                'Categoria',
                'Origem',
                'Espécie',
                'Rúbrica',
                'Alínea',
                'Detalhamento',
                'Receita Prevista (Bruta)',
                'Receita Arrecadada (Bruta)'
            ];
        }
        // Por Fonte de Recurso
        else if ($this->classificacao == 'fonterecurso') {
            return [
                'Código Fonte de Recurso',
                'Fonte de Recurso',
                'Categoria',
                'Origem',
                'Espécie',
                'Rúbrica',
                'Alínea',
                'Detalhamento',
                'Receita Prevista (Bruta)',
                'Receita Arrecadada (Bruta)'
            ];
        }
        // Por Unidade Gestora
        else if ($this->classificacao == 'gestora') {
            return [
                'Unidade Gestora',
                'Gestão',
                'Categoria',
                'Origem',
                'Espécie',
                'Rúbrica',
                'Alínea',
                'Detalhamento',
                'Receita Prevista (Bruta)',
                'Receita Arrecadada (Bruta)'
            ];
        }
    }
}
