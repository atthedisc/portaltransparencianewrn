<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CompraseservicosExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $tabela = DB::table('covid19compraseservicos')
            ->select('contratante', 'ncontrato', 'nprocesso', 'valordocontrato', 'valorpagors', 'anulacaors', 'dataassinatura', 'contratado', 'cnpjcpf', 'objeto', 'modalidadedespesa', 'fundamentolegal', 'fontedorecurso', 'vigencia', 'localdeexecucao', 'situacao')
            ->get();
        
        return $tabela;
    }

    public function headings(): array
    {
        return [
            'Orgão',
            'N. Contrato',
            'N. Processo',
            'Valor do Contrato (R$)',
            'Valor Pago (R$)',
            'Valor anulado (R$)',
            'Data Assinatura',
            'Contratado',
            'CPF/CNPJ/RG',
            'Objeto',
            'Modalidade',
            'Fundamento Legal',
            'Fonte do Recurso',
            'Vigência',
            'Local de execução',
            'Entrega do bem/serviço - Situação'
        ];
    }
}
