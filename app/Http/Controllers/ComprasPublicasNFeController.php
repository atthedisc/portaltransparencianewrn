<?php

namespace App\Http\Controllers;

use App\Components\Busca\Buscar_por;
use App\Exports\ComprasPublicasNFeExport;
use App\Models\ComprasPublicasNFe;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ComprasPublicasNFeController extends Controller
{
   public function index(Request $request)
   {
      $ano = collect(['',2024,2023, 2022, 2021, 2020, 2019]);
      $mes = collect(['', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]);
      $model = ComprasPublicasNFe::class;

      $busca = new Buscar_por;
      $busca = $busca->generatebuscar_por([
         "None" => "",
         "razaosocialdestinatario" => "Razão Social Destinatario",
         "cnpjdestinatario" => "CNPJ Destinatario",
         "numnfe" => "Número da NF-e",
         "dtemissao" => "Data Emissão",
         "chavenfe" => "Chave Da NF-e",
         "razaosocialemitente" => "Razão Social Emitente",
      ]);

      //TODOS OS DADOS DA VIEW
      $dados = $model::where('ano', $request->ano)
         ->where('mes', $request->mes)
         ->paginate(15);

      if ($request->mes == NULL and $request->ano == NULL) {
         $hidden = true;
      } else {
         $hidden = false;
      }

      //RETORNANDO VARIAVEIS PARA INDEX
      return view('compras-publicas-nfe.index', [
         'dados' => $dados,
         'anos' => $ano,
         'meses' => $mes,
         'ano_selected' => $request->ano,
         'mes_selected' => $request->mes,
         'hidden' => $hidden,
         'busca' => $busca
      ]);
   }

   public function exportxls($ano, $mes)
   {
      return Excel::download(new ComprasPublicasNFeExport($ano, $mes), 'compras-publicasnfe.xlsx');
   }
   public function exportpdf($ano, $mes)
   {
      return Excel::download(new ComprasPublicasNFeExport($ano, $mes), 'compras-publicasnfe.pdf');
   }
}
