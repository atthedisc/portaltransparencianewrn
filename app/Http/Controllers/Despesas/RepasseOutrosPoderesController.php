<?php

namespace App\Http\Controllers\Despesas;

use App\Exports\Despesas\RepasseOutrosPoderesExport;
use App\Http\Controllers\Controller;
use App\Models\RepasseOutrosPoderes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class RepasseOutrosPoderesController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->posicao == 'Acumulado') {
            $posicao = '<=';
        } else {
            $posicao = '=';
        }

        $meses = collect([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]);
        $anos = collect([2024, 2023, 2022, 2021, 2020, 2019]);
        $posicoes = collect(['No mês', 'Acumulado']);
        $model = RepasseOutrosPoderes::class;

        $dados = $model::select('idpoder', 'txtpoder', DB::raw('SUM(total_vlrpagamento) as total_vlrpagamento'))
            ->where('numexerciciofinanc', $request->ano)
            ->where('mes_repasse_outros_poderes', $posicao, $request->mes)
            ->groupBy('idpoder', 'txtpoder')
            ->orderBy('total_vlrpagamento')
            ->get();

        return view('despesas/repasse-outros-poderes.index', [
            'dados' => $dados,
            'meses' => $meses,
            'anos' => $anos,
            'posicoes' => $posicoes,
            'ano_selected' => $request->ano,
            'mes_selected' => $request->mes,
            'posicao_selected' => $request->posicao,
            'posicao' => $posicao
        ]);
    }

    public function exportxls($mes, $ano, $posicao)
    {
        return Excel::download(new RepasseOutrosPoderesExport($mes, $ano, $posicao), 'repasse-outros-poderes.xlsx');
    }

    public function exportpdf($mes, $ano, $posicao)
    {
        return Excel::download(new RepasseOutrosPoderesExport($mes, $ano, $posicao), 'repasse-outros-poderes.pdf');
    }
}
