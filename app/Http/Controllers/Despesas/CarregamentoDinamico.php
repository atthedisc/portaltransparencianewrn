<?php

namespace App\Http\Controllers\Despesas;

use App\Components\Despesas\Despesa;
use App\Components\Despesas\Favorecido;
use App\Components\Despesas\Fonte;
use App\Components\Despesas\Funcao;
use App\Components\Despesas\Unidadegestora;
use App\Components\Receitas\Fonte as ReceitasFonte;
use App\Components\Receitas\Receita;
use App\Components\Receitas\Unidadegestora as ReceitasUnidadegestora;
use App\Components\Rowdespesa\Rowdespesa;
use App\Components\Rowdespesa\Rowfavorecido;
use App\Components\Rowdespesa\Rowfonte;
use App\Components\Rowdespesa\Rowfuncao;
use App\Components\Rowdespesa\Rowunidade;
use App\Components\Rowreceita\Rowfonte as RowreceitaRowfonte;
use App\Components\Rowreceita\Rowreceita;
use App\Components\Rowreceita\Rowunidade as RowreceitaRowunidade;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CarregamentoDinamico extends Controller
{
    public function index(Request $request)
    {
        $query = null;
        $where = "mes {$request->posicao} {$request->mes} and numexerciciofinanc = '{$request->ano}'";
        $increment = '';
        // Monta o where de acordo com a quantidade de condicionais para achar os dados
        foreach ($request->dados as $key => $codigo) {
            $increment .= $increment . " and {$key} = '{$codigo}'";
        }
        $where = $where . $increment;
        //Despesa

        if($request->source == 'despesa'){
            //faz as queries para pagamento dependendo da classificação
            if ($request->classificacao == 'despesa') {
                $query = new Despesa;
                $row = new Rowdespesa;
            } else if ($request->classificacao == 'gestora') {
                $query = new Unidadegestora;
                $row = new Rowunidade;
            } else if ($request->classificacao == 'fonte') {
                $query = new Fonte;
                $row = new Rowfonte;
            } else if ($request->classificacao == 'favorecido') {
                $query = new Favorecido;
                $row = new Rowfavorecido;
            }
            else if ($request->classificacao == 'funcao') {
                $query = new Funcao;
                $row = new Rowfuncao;
            }
        }
        else if($request->source == 'receita'){
                //faz as queries para empenho e liquidação dependendo da classificação
                if ($request->page == 'receita') {
                    $query = new Receita;
                    $row = new Rowreceita;
                } else if ($request->page == 'unidade') {
                    $query = new ReceitasUnidadegestora;
                    $row = new RowreceitaRowunidade;
                } else if ($request->page == 'fonte') {
                    $query = new ReceitasFonte;
                    $row = new RowreceitaRowfonte;
                }
        }
        $query = $query->query((object) [
            "mes" => $request->mes,
            "ano" => $request->ano,
            "posicao" => $request->posicao,
            "fase" => $request->fase,
            "where" => $where,
            "request" => $request
        ]);

        //monta a linha com os dados obtidos
        $dados = $row->generaterow((object) [
            "dado" => (object) $query,
            "request" => $request
        ]);

        if ($dados != "") {
            return $dados;
        }
        return '<tr><td><p class="p-2"> Não foram encontrados dados para essa pesquisa</p></td></tr>';
    }
}