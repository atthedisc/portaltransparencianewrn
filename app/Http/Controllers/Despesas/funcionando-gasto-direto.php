<?php

namespace App\Http\Controllers\Despesas;

use App\Components\Busca\Buscar_por;
use App\Exports\Despesas\GastosDiretosExport;
use App\Http\Controllers\Controller;
use App\Models\GastosDireto\LiquidacaoGastosDiretos;
use App\Models\GastosDireto\NotaEmpenhoGastosDiretos;
use App\Models\GastosDireto\NotaPagamentoGastosDiretos;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Crypt as FacadesCrypt;

class GastosDiretosController extends Controller
{

   public function index(Request $request){
       
       if (!$request->query() && !$request->input()) {
           return redirect()->route('despesas');
       }
     
       // Determina a posição a ser consultada
       $posicao = ($request->posicao == 'Acumulado') ? '<=' : '=';
     

       // Calcula o valor total dos gastos
       $sumtotal = LiquidacaoGastosDiretos::where('mes', $posicao, $request->mes)->where('numexerciciofinanc', $request->ano)->sum('vlrpagamento');
     
       if ($request->classificacao == 'despesa') {
           
         // Gera os parâmetros de busca
         $busca = (new Buscar_por())->generatebuscar_por([
             "None" => "",
             "txtdescricaogrupodespesa" => "Grupo de despesa",
             "txtdescricaoelementodespesa" => "Elemento de despesa"
         ]);
     
         // Função para criar subconsultas
         $createSubquery = function($table, $select) use ($request, $posicao) {
             
             if($table == 'vportal_notaempenho_gastosdiretos'){
                $modelo = NotaEmpenhoGastosDiretos::class;
             } elseif ($table == 'vportal_notapagamento_gastosdiretos'){
                $modelo = NotaPagamentoGastosDiretos::class;
             } else {
                $modelo = LiquidacaoGastosDiretos::class;
             }
             
             $query = $modelo::where('numexerciciofinanc', $request->ano)
                 ->select( 'txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'txtdescricaounidade', 'txtdescricaogestao', 'codelementodespesa', 'codunidadegestora', 'codgestao' )
                 ->selectRaw('CAST(codgrupodespesa AS varchar) AS codgrupodespesa')
                 ->selectRaw($select)
                 ->groupBy( 'txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'txtdescricaounidade', 'txtdescricaogestao', 'codgrupodespesa', 'codelementodespesa', 'codunidadegestora', 'codgestao' );
     
             if ($posicao === '=') {
                 $query->where('mes', '=', $request->mes);
             } else {
                 $query->where('mes', '<=', $request->mes);
             }
     
             return $query;
         };
     
         // Define as subconsultas
         $subquery1 = $createSubquery(
             'vportal_notaempenho_gastosdiretos',
             "SUM(vlrpagamento) AS vlrempenho, 0 AS vlrliquido, 0 AS vlrpagamento, 0 AS vlrpagamentorp, 'vportal_notaempenho_gastosdiretos' as tbvportal"
         );
         $subquery2 = $createSubquery(
             'vportal_liquidacaodespesa_gastosdiretos',
             "0 AS vlrempenho, SUM(vlrpagamento) AS vlrliquido, 0 AS vlrpagamento, 0 AS vlrpagamentorp, 'vportal_liquidacaodespesa_gastosdiretos' as tbvportal"
         );
         $subquery3 = $createSubquery(
             'vportal_notapagamento_gastosdiretos',
             "0 AS vlrempenho, 0 AS vlrliquido, SUM(vlrpagamento) AS vlrpagamento, SUM(vlrpagamentorp) AS vlrpagamentorp, 'vportal_notapagamento_gastosdiretos' as tbvportal"
         );
         
         // Junta as subconsultas
         $grupodespesa = DB::connection('pgsql_siaf')
             ->table(DB::raw("({$subquery1->toSql()} UNION ALL {$subquery2->toSql()} UNION ALL {$subquery3->toSql()}) AS resultado"))
             ->setBindings(array_merge(
                 $subquery1->getBindings(),
                 $subquery2->getBindings(),
                 $subquery3->getBindings()
             ))
             ->select(
                 'resultado.txtdescricaogrupodespesa',
                 'resultado.txtdescricaoelementodespesa',
                 'resultado.txtdescricaounidade',
                 'resultado.txtdescricaogestao',
                 DB::raw("STRING_AGG(CONCAT(resultado.codgrupodespesa, '/', resultado.tbvportal), ',') AS result"),
                 'resultado.codelementodespesa',
                 'resultado.codunidadegestora',
                 'resultado.codgestao',
                 DB::raw('SUM(resultado.vlrempenho) AS vlrempenho'),
                 DB::raw('SUM(resultado.vlrliquido) AS vlrliquido'),
                 DB::raw('SUM(resultado.vlrpagamento) AS vlrpagamento'),
                 DB::raw('SUM(resultado.vlrpagamentorp) AS vlrpagamentorp')
             )
             ->groupBy(
                 'resultado.txtdescricaogrupodespesa',
                 'resultado.txtdescricaoelementodespesa',
                 'resultado.txtdescricaounidade',
                 'resultado.txtdescricaogestao',
                 'resultado.codelementodespesa',
                 'resultado.codunidadegestora',
                 'resultado.codgestao'
             )
             ->orderBy('resultado.txtdescricaogrupodespesa', 'ASC')
             ->orderBy('resultado.txtdescricaoelementodespesa', 'ASC')
             ->orderBy('resultado.txtdescricaounidade', 'ASC')
             ->paginate(10);
     
         // Gráficos
         $grupodespesacharts = LiquidacaoGastosDiretos::select('txtdescricaogrupodespesa', 'codgrupodespesa', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
             ->where('mes', $posicao, $request->mes)
             ->where('numexerciciofinanc', $request->ano)
             ->groupBy('txtdescricaogrupodespesa', 'codgrupodespesa')
             ->orderBy('txtdescricaogrupodespesa')
             ->get();
     
         // Retornar as consultas para a página
         return view('despesas/gastos-diretos/tipo-despesa.index', [
             'grupodespesa' => $grupodespesa,
             'fase' => $request->fase,
             'mes' => $request->mes,
             'ano' => $request->ano,
             'classificacao' => $request->classificacao,
             'posicao' => $posicao,
             'nomefase' => null,
             'sumtotal' => $sumtotal,
             'grupodespesacharts' => $grupodespesacharts,
             'elementodespesacharts' => $grupodespesacharts,
             'busca' => $busca
         ]);
     }

       
       
       
       
      // Filtro Por Unidade Gestora
      else if ($request->classificacao == 'gestora') {
         $busca = new Buscar_por;
         $busca = $busca->generatebuscar_por([
            "None" => "",
            "txtdescricaounidade" => "Unidade Gestora",
         ]);

         $subquery = DB::connection('pgsql_siaf')
            ->table('vportal_notaempenho_gastosdiretos')
            ->select('codunidadegestora', 'codgestao', 'codcredor', 'txtdescricaounidade', 'txtdescricaogestao', 'txtnomecredor')
            ->selectRaw("SUM(vlrpagamento) AS vlrempenho,
                         COALESCE(null, 0) as vlrliquido, 
                         COALESCE(null, 0) as vlrpagamento, 
		                 COALESCE(null, 0) as vlrpagamentorp,
                         'vportal_notaempenho_gastosdiretos' as tbvportal")
            ->where('numexerciciofinanc', $request->ano)
            ->groupBy('codunidadegestora', 'codgestao', 'codcredor', 'txtdescricaounidade', 'txtdescricaogestao', 'txtnomecredor');
         if($posicao === '='){
            $subquery->where('mes', '=', $request->mes);
         }else{
            $subquery->where('mes','<=', $request->mes);
         }

         $subquery2 = DB::connection('pgsql_siaf')
            ->table('vportal_liquidacaodespesa_gastosdiretos')
            ->select('codunidadegestora', 'codgestao', 'codcredor', 'txtdescricaounidade', 'txtdescricaogestao', 'txtnomecredor')
            ->selectRaw("COALESCE(null, 0) as vlrempenho, 
                         SUM(vlrpagamento) AS vlrliquido,
                         COALESCE(null, 0) as vlrpagamento, 
		                 COALESCE(null, 0) as vlrpagamentorp,
                         'vportal_liquidacaodespesa_gastosdiretos' as tbvportal")
            ->where('numexerciciofinanc', $request->ano)
            ->groupBy('codunidadegestora', 'codgestao', 'codcredor', 'txtdescricaounidade', 'txtdescricaogestao', 'txtnomecredor'); 

         if($posicao === '='){
            $subquery2->where('mes', '=', $request->mes);
         }else{
            $subquery2->where('mes','<=', $request->mes);
         }

         $subquery3 = DB::connection('pgsql_siaf')
            ->table('vportal_notapagamento_gastosdiretos')
            ->select('codunidadegestora', 'codgestao', 'codcredor', 'txtdescricaounidade', 'txtdescricaogestao', 'txtnomecredor')
            ->selectRaw("COALESCE(null, 0) AS vlrempenho,
                         COALESCE(null, 0) AS vlrliquido,
                         SUM(vlrpagamento) AS vlrpagamento,
                         SUM(vlrpagamentorp) AS vlrpagamentorp,
                         'vportal_notapagamento_gastosdiretos' as tbvportal")
            ->where('numexerciciofinanc', $request->ano)
            ->groupBy('codunidadegestora', 'codgestao', 'codcredor', 'txtdescricaounidade', 'txtdescricaogestao', 'txtnomecredor'); 

            if($posicao === '='){
               $subquery3->where('mes', '=', $request->mes);
            }else{
               $subquery3->where('mes','<=', $request->mes);
            }

         $unidadegestora = DB::connection('pgsql_siaf')
            ->table(DB::raw("({$subquery->toSql()} UNION ALL {$subquery2->toSql()} UNION ALL {$subquery3->toSql()}) AS resultado"))
            ->setBindings(array_merge(
               $subquery->getBindings(),
               $subquery2->getBindings(),
               $subquery3->getBindings()
           ))
            ->select(
               'resultado.codunidadegestora', 
               'resultado.codgestao', 
               'resultado.codcredor',
               'resultado.txtdescricaounidade',
               'resultado.txtdescricaogestao',
               'resultado.txtnomecredor',
               DB::raw("STRING_AGG(resultado.tbvportal, ',') AS result"), 
               DB::raw('COALESCE(SUM(resultado.vlrempenho), 0) AS vlrempenho'), 
               DB::raw('COALESCE(SUM(resultado.vlrliquido), 0) AS vlrliquido'), 
               DB::raw('COALESCE(SUM(resultado.vlrpagamento), 0) AS vlrpagamento'), 
               DB::raw('COALESCE(SUM(resultado.vlrpagamentorp), 0) AS vlrpagamentorp')
            )
            ->groupBy(
               'resultado.codunidadegestora', 
               'resultado.codgestao', 
               'resultado.codcredor',
               'resultado.txtdescricaounidade',
               'resultado.txtdescricaogestao',
               'resultado.txtnomecredor'
            )
            ->orderBy('resultado.txtdescricaounidade', 'ASC')
            ->orderBy('resultado.txtdescricaogestao', 'ASC')
            ->orderBy('resultado.txtnomecredor', 'ASC')
            ->paginate(10);

        // Gráficos
        $grupodespesacharts = $model::select('txtdescricaounidade', 'codunidadegestora', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
        ->where('mes', $posicao, $request->mes)
        ->where('numexerciciofinanc', $request->ano)
        ->groupBy('txtdescricaounidade', 'codunidadegestora')
        ->orderBy('txtdescricaounidade')
        ->get();

        $elementodespesacharts = $model::select('txtdescricaounidade', 'codunidadegestora', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
        ->where('mes', $posicao, $request->mes)
        ->where('numexerciciofinanc', $request->ano)
        ->groupBy('txtdescricaounidade', 'codunidadegestora')
        ->orderBy('txtdescricaounidade')
        ->get();

         // Retornar as consultas para a página
         return view('despesas/gastos-diretos/unidade-gestora.index', [
            'unidadegestora' => $unidadegestora,
            'fase' => $request->fase,
            'mes' => $request->mes,
            'ano' => $request->ano,
            'classificacao' => $request->classificacao,
            'posicao' => $posicao,
            'nomefase' => $nomefase,
            'sumtotal' => $sumtotal,
            'grupodespesacharts' => $grupodespesacharts,
            'elementodespesacharts' => $elementodespesacharts,
            "busca" => $busca
         ]);
      }
      // Filtro Por Ação
      else if ($request->classificacao == 'acao') {
         $busca = new Buscar_por;
         $busca = $busca->generatebuscar_por([
            "None" => "",
            "txtdescricaoacao" => "Ação",
         ]);
         
         $sumtr = $model::where('mes', $posicao, $request->mes)
            ->where('numexerciciofinanc', $request->ano)
            ->sum('vlrpagamento');


            $subquery = DB::connection('pgsql_siaf')
            ->table('vportal_notaempenho_gastosdiretos')
            ->select('codacao')
            ->selectRaw("INITCAP(txtdescricaoacao) as txtdescricaoacao,
                         SUM(vlrpagamento) AS vlrempenho, 
                         COALESCE(null, 0) as vlrliquido,
                         COALESCE(null, 0) as vlrpagamento, 
                         COALESCE(null, 0) as vlrpagamentorp,
                         'vportal_notaempenho_gastosdiretos' as tbvportal,
                         'notaEmpenho' as tipo")
            ->where('numexerciciofinanc', $request->ano)
            ->groupBy('codacao', 'txtdescricaoacao');
         if($posicao === '='){
            $subquery->where('mes', '=', $request->mes);
         }else{
            $subquery->where('mes','<=', $request->mes);
         }

         $subquery2 = DB::connection('pgsql_siaf')
            ->table('vportal_liquidacaodespesa_gastosdiretos')
            ->select('codacao')
            ->selectRaw("INITCAP(txtdescricaoacao) as txtdescricaoacao,
                         COALESCE(null, 0) as vlrempenho, 
                         SUM(vlrpagamento) AS vlrliquido,
                         COALESCE(null, 0) as vlrpagamento, 
		                   COALESCE(null, 0) as vlrpagamentorp,
                         'vportal_liquidacaodespesa_gastosdiretos' as tbvportal,
                         'liquidacao' as tipo")
            ->where('numexerciciofinanc', $request->ano)
            ->groupBy('codacao', 'txtdescricaoacao');

         if($posicao === '='){
            $subquery2->where('mes', '=', $request->mes);
         }else{
            $subquery2->where('mes','<=', $request->mes);
         }

         $subquery3 = DB::connection('pgsql_siaf')
            ->table('vportal_notapagamento_gastosdiretos')
            ->select('codacao')
            ->selectRaw("INITCAP(txtdescricaoacao) as txtdescricaoacao,
                         COALESCE(null, 0) AS vlrempenho,
                         COALESCE(null, 0) AS vlrliquido,
                         SUM(vlrpagamento) AS vlrpagamento,
                         SUM(vlrpagamentorp) AS vlrpagamentorp,
                         'vportal_notapagamento_gastosdiretos' as tbvportal,
                         case when SUM(vlrpagamento) > 0 then 'notaPagamento'
        	                     when SUM(vlrpagamentorp)  > 0 then 'valorPagar'
                         end as tipo")
            ->where('numexerciciofinanc', $request->ano)
            ->groupBy('codacao', 'txtdescricaoacao');

            if($posicao === '='){
               $subquery3->where('mes', '=', $request->mes);
            }else{
               $subquery3->where('mes','<=', $request->mes);
            }

         $acao = DB::connection('pgsql_siaf')
            ->table(DB::raw("({$subquery->toSql()} UNION ALL {$subquery2->toSql()} UNION ALL {$subquery3->toSql()}) AS resultado"))
            ->setBindings(array_merge(
               $subquery->getBindings(),
               $subquery2->getBindings(),
               $subquery3->getBindings()
           ))
            ->select( 
               'resultado.txtdescricaoacao',
               DB::raw("STRING_AGG(CONCAT(CONCAT(resultado.codacao, '/', resultado.tbvportal),'/', resultado.tipo), ',') as result"), 
               DB::raw('COALESCE(SUM(resultado.vlrempenho), 0) AS vlrempenho'), 
               DB::raw('COALESCE(SUM(resultado.vlrliquido), 0) AS vlrliquido'), 
               DB::raw('COALESCE(SUM(resultado.vlrpagamento), 0) AS vlrpagamento'), 
               DB::raw('COALESCE(SUM(resultado.vlrpagamentorp), 0) AS vlrpagamentorp')
            )
            ->groupBy(
                'resultado.txtdescricaoacao'
            )
            ->orderBy('resultado.txtdescricaoacao', 'ASC')
            ->paginate(10);

        // Gráficos
        $grupodespesacharts = $model::select('txtdescricaoacao', 'codacao', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
        ->where('mes', $posicao, $request->mes)
        ->where('numexerciciofinanc', $request->ano)
        ->groupBy('txtdescricaoacao', 'codacao')
        ->orderBy('txtdescricaoacao')
        ->get();

        $elementodespesacharts = $model::select('txtdescricaoacao', 'codacao', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
        ->where('mes', $posicao, $request->mes)
        ->where('numexerciciofinanc', $request->ano)
        ->groupBy('txtdescricaoacao', 'codacao')
        ->orderBy('txtdescricaoacao')
        ->get();

         // Retornar as consultas para a página
         return view('despesas/gastos-diretos/acao.index', [
            'acoes' => $acao,
            'fase' => $request->fase,
            'mes' => $request->mes,
            'ano' => $request->ano,
            'classificacao' => $request->classificacao,
            'posicao' => $posicao,
            'nomefase' => $nomefase,
            'sumtotal' => $sumtotal,
            'grupodespesacharts' => $grupodespesacharts,
            'elementodespesacharts' => $elementodespesacharts,
            'sumtr' => $sumtr,
            'busca' => $busca
         ]);
      }
      // Filtro Por Fonte
      else if ($request->classificacao == 'fonte') {
         $busca = new Buscar_por;
         $busca = $busca->generatebuscar_por([
            "None" => "",
            "txtdescricaofonterecurso" => "Fonte de recurso",
         ]);

         $subquery = DB::connection('pgsql_siaf')
            ->table('vportal_notaempenho_gastosdiretos')
            ->select('codfonterecurso',
                     'codunidadegestora',
                     'codgestao',
                     'txtdescricaofonterecurso',
                     'txtdescricaounidade',
                     'txtdescricaogestao')
            ->selectRaw("SUM(vlrpagamento) AS vlrempenho,
                         COALESCE(null, 0) as vlrliquido, 
                         COALESCE(null, 0) as vlrpagamento, 
		                   COALESCE(null, 0) as vlrpagamentorp,
                         'vportal_notaempenho_gastosdiretos' as tbvportal,
                         'notaEmpenho' as tipo")
            ->where('numexerciciofinanc', $request->ano)
            ->groupBy('codfonterecurso', 'codunidadegestora', 'codgestao', 'txtdescricaofonterecurso', 'txtdescricaounidade', 'txtdescricaogestao');
         if($posicao === '='){
            $subquery->where('mes', '=', $request->mes);
         }else{
            $subquery->where('mes','<=', $request->mes);
         }

         $subquery2 = DB::connection('pgsql_siaf')
            ->table('vportal_liquidacaodespesa_gastosdiretos')
            ->select('codfonterecurso',
                     'codunidadegestora',
                     'codgestao',
                     'txtdescricaofonterecurso',
                     'txtdescricaounidade',
                     'txtdescricaogestao')
            ->selectRaw("COALESCE(null, 0) as vlrempenho, 
                         SUM(vlrpagamento) AS vlrliquido,
                         COALESCE(null, 0) as vlrpagamento, 
		                   COALESCE(null, 0) as vlrpagamentorp,
                         'vportal_liquidacaodespesa_gastosdiretos' as tbvportal,
                         'liquidacao' as tipo")
            ->where('numexerciciofinanc', $request->ano)
            ->groupBy('codfonterecurso', 'codunidadegestora', 'codgestao', 'txtdescricaofonterecurso', 'txtdescricaounidade', 'txtdescricaogestao');

         if($posicao === '='){
            $subquery2->where('mes', '=', $request->mes);
         }else{
            $subquery2->where('mes','<=', $request->mes);
         }

         $subquery3 = DB::connection('pgsql_siaf')
            ->table('vportal_notapagamento_gastosdiretos')
            ->select('codfonterecurso',
                     'codunidadegestora',
                     'codgestao',
                     'txtdescricaofonterecurso',
                     'txtdescricaounidade',
                     'txtdescricaogestao')
            ->selectRaw("COALESCE(null, 0) AS vlrempenho,
                         COALESCE(null, 0) AS vlrliquido,
                         SUM(vlrpagamento) AS vlrpagamento,
                         SUM(vlrpagamentorp) AS vlrpagamentorp,
                         'vportal_notapagamento_gastosdiretos' as tbvportal,
                         case when SUM(vlrpagamento) > 0 then 'notaPagamento'
                              when SUM(vlrpagamentorp)  > 0 then 'valorPagar'
                         end as tipo")
            ->where('numexerciciofinanc', $request->ano)
            ->groupBy('codfonterecurso', 'codunidadegestora', 'codgestao', 'txtdescricaofonterecurso', 'txtdescricaounidade', 'txtdescricaogestao');

            if($posicao === '='){
               $subquery3->where('mes', '=', $request->mes);
            }else{
               $subquery3->where('mes','<=', $request->mes);
            }

         $fonte = DB::connection('pgsql_siaf')
            ->table(DB::raw("({$subquery->toSql()} UNION ALL {$subquery2->toSql()} UNION ALL {$subquery3->toSql()}) AS resultado"))
            ->setBindings(array_merge(
               $subquery->getBindings(),
               $subquery2->getBindings(),
               $subquery3->getBindings()
           ))
            ->select(
               'resultado.codunidadegestora',
               'resultado.codgestao',
               'resultado.txtdescricaofonterecurso',
               'resultado.txtdescricaounidade',
               'resultado.txtdescricaogestao',
               DB::raw("STRING_AGG(CONCAT(CONCAT(resultado.codfonterecurso, '/', resultado.tbvportal), '/', resultado.tipo), ',') AS result"), 
               DB::raw('COALESCE(SUM(resultado.vlrempenho), 0) AS vlrempenho'), 
               DB::raw('COALESCE(SUM(resultado.vlrliquido), 0) AS vlrliquido'), 
               DB::raw('COALESCE(SUM(resultado.vlrpagamento), 0) AS vlrpagamento'), 
               DB::raw('COALESCE(SUM(resultado.vlrpagamentorp), 0) AS vlrpagamentorp')
            )
            ->groupBy(
               'resultado.codunidadegestora',
               'resultado.codgestao',
               'resultado.txtdescricaofonterecurso',
               'resultado.txtdescricaounidade',
               'resultado.txtdescricaogestao'
            )
            ->orderBy('resultado.txtdescricaofonterecurso', 'ASC')
            ->orderBy('resultado.txtdescricaounidade', 'ASC')
            ->orderBy('resultado.txtdescricaogestao', 'ASC')
            ->paginate(10);

        // Gráficos
        $grupodespesacharts = $model::select('txtdescricaofonterecurso', 'codfonterecurso', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
        ->where('mes', $posicao, $request->mes)
        ->where('numexerciciofinanc', $request->ano)
        ->groupBy('txtdescricaofonterecurso', 'codfonterecurso')
        ->orderBy('txtdescricaofonterecurso')
        ->get();

        $elementodespesacharts = $model::select('txtdescricaofonterecurso', 'codfonterecurso', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
        ->where('mes', $posicao, $request->mes)
        ->where('numexerciciofinanc', $request->ano)
        ->groupBy('txtdescricaofonterecurso', 'codfonterecurso')
        ->orderBy('txtdescricaofonterecurso')
        ->get();

         // Retornar as consultas para a página
         return view('despesas/gastos-diretos/fonte.index', [
            'fontes' => $fonte,
            'fase' => $request->fase,
            'mes' => $request->mes,
            'ano' => $request->ano,
            'classificacao' => $request->classificacao,
            'posicao' => $posicao,
            'nomefase' => $nomefase,
            'sumtotal' => $sumtotal,
            'grupodespesacharts' => $grupodespesacharts,
            'elementodespesacharts' => $elementodespesacharts,
            'busca' => $busca
         ]);
      }
      // Filtro Por Favorecido
      else if ($request->classificacao == 'favorecido') {
         $busca = new Buscar_por;
         $busca = $busca->generatebuscar_por([
            "None" => "",
            "txtnomecredor" => "Favorecido",
         ]);

         $subquery = DB::connection('pgsql_siaf')
         ->table('vportal_notaempenho_gastosdiretos')
         ->select('txtnomecredor',
                  'txtdescricaounidade',
                  'txtdescricaogrupodespesa',
                  'txtdescricaoelementodespesa')
         ->selectRaw("codcredor::varchar as codcredor,
                      codgrupodespesa::varchar as codgrupodespesa,
                      codelementodespesa::varchar as codelementodespesa,
                      codunidadegestora::varchar as codunidadegestora")
         ->selectRaw("SUM(vlrpagamento) AS vlrempenho,
                      COALESCE(null, 0) as vlrliquido, 
                      COALESCE(null, 0) as vlrpagamento, 
                      COALESCE(null, 0) as vlrpagamentorp,
                      'vportal_notaempenho_gastosdiretos' as tbvportal,
                      'notaEmpenho' as tipo")
         ->where('numexerciciofinanc', $request->ano)
         ->groupBy('codcredor', 'codgrupodespesa', 'codelementodespesa', 'codunidadegestora', 'txtnomecredor', 'txtdescricaounidade', 'txtdescricaogrupodespesa', 'txtdescricaoelementodespesa');

         if($posicao === '='){
            $subquery->where('mes', '=', $request->mes);
         }else{
            $subquery->where('mes','<=', $request->mes);
         }

         $subquery2 = DB::connection('pgsql_siaf')
            ->table('vportal_liquidacaodespesa_gastosdiretos')
            ->select('txtnomecredor',
                     'txtdescricaounidade',
                     'txtdescricaogrupodespesa',
                     'txtdescricaoelementodespesa')
            ->selectRaw("codcredor::varchar as codcredor,
                         codgrupodespesa::varchar as codgrupodespesa,
                         codelementodespesa::varchar as codelementodespesa,
                         codunidadegestora::varchar as codunidadegestora")
            ->selectRaw("COALESCE(null, 0) as vlrempenho, 
                         SUM(vlrpagamento) AS vlrliquido,
                         COALESCE(null, 0) as vlrpagamento, 
                         COALESCE(null, 0) as vlrpagamentorp,
                         'vportal_liquidacaodespesa_gastosdiretos' as tbvportal,
                         'liquidacao' as tipo")
            ->where('numexerciciofinanc', $request->ano)
            ->groupBy('codcredor', 'codgrupodespesa', 'codelementodespesa', 'codunidadegestora', 'txtnomecredor', 'txtdescricaounidade', 'txtdescricaogrupodespesa', 'txtdescricaoelementodespesa');


         if($posicao === '='){
            $subquery2->where('mes', '=', $request->mes);
         }else{
            $subquery2->where('mes','<=', $request->mes);
         }

         $subquery3 = DB::connection('pgsql_siaf')
            ->table('vportal_notapagamento_gastosdiretos')
            ->select('txtnomecredor',
                     'txtdescricaounidade',
                     'txtdescricaogrupodespesa',
                     'txtdescricaoelementodespesa')
            ->selectRaw("codcredor::varchar as codcredor,
                         codgrupodespesa::varchar as codgrupodespesa,
                         codelementodespesa::varchar as codelementodespesa,
                         codunidadegestora::varchar as codunidadegestora")
            ->selectRaw("COALESCE(null, 0) AS vlrempenho,
                         COALESCE(null, 0) AS vlrliquido,
                         SUM(vlrpagamento) AS vlrpagamento,
                         SUM(vlrpagamentorp) AS vlrpagamentorp,
                         'vportal_notapagamento_gastosdiretos' as tbvportal,
                         case when SUM(vlrpagamento) > 0 then 'notaPagamento'
                              when SUM(vlrpagamentorp)  > 0 then 'valorPagar'
                         end as tipo")
            ->where('numexerciciofinanc', $request->ano)
            ->groupBy('codcredor', 'codgrupodespesa', 'codelementodespesa', 'codunidadegestora', 'txtnomecredor', 'txtdescricaounidade', 'txtdescricaogrupodespesa', 'txtdescricaoelementodespesa');

            if($posicao === '='){
               $subquery3->where('mes', '=', $request->mes);
            }else{
               $subquery3->where('mes','<=', $request->mes);
            }

         $favorecidos = DB::connection('pgsql_siaf')
            ->table(DB::raw("({$subquery->toSql()} UNION ALL {$subquery2->toSql()} UNION ALL {$subquery3->toSql()}) AS resultado"))
            ->setBindings(array_merge(
               $subquery->getBindings(),
               $subquery2->getBindings(),
               $subquery3->getBindings()
           ))
            ->select(
               'resultado.codcredor',
               'resultado.codunidadegestora',
               'resultado.codelementodespesa',
               'resultado.txtnomecredor',
               'resultado.txtdescricaounidade',
               'resultado.txtdescricaogrupodespesa',
               'resultado.txtdescricaoelementodespesa',
               DB::raw("STRING_AGG(CONCAT(CONCAT(resultado.codgrupodespesa, '/', resultado.tbvportal), '/', resultado.tipo), ',')  AS result"), 
               DB::raw('COALESCE(SUM(resultado.vlrempenho), 0) AS vlrempenho'), 
               DB::raw('COALESCE(SUM(resultado.vlrliquido), 0) AS vlrliquido'), 
               DB::raw('COALESCE(SUM(resultado.vlrpagamento), 0) AS vlrpagamento'), 
               DB::raw('COALESCE(SUM(resultado.vlrpagamentorp), 0) AS vlrpagamentorp')
            )
            ->groupBy(
               'resultado.codcredor',
               'resultado.codunidadegestora',
               'resultado.codelementodespesa',
               'resultado.txtnomecredor',
               'resultado.txtdescricaounidade',
               'resultado.txtdescricaogrupodespesa',
               'resultado.txtdescricaoelementodespesa'
            )
            ->orderBy('resultado.txtnomecredor', 'ASC')
            ->orderBy('resultado.txtdescricaounidade', 'ASC')
            ->orderBy('resultado.txtdescricaogrupodespesa', 'ASC')
            ->orderBy('resultado.txtdescricaoelementodespesa', 'ASC')
            ->paginate(10);

           // Gráficos
           $grupodespesacharts = $model::select('txtnomecredor', 'codcredor', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
           ->where('mes', $posicao, $request->mes)
           ->where('numexerciciofinanc', $request->ano)
           ->groupBy('txtnomecredor', 'codcredor')
           ->orderBy('txtnomecredor')
           ->limit(10)
           ->get();

           $elementodespesacharts = $model::select('txtnomecredor', 'codcredor', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
           ->where('mes', $posicao, $request->mes)
           ->where('numexerciciofinanc', $request->ano)
           ->groupBy('txtnomecredor', 'codcredor')
           ->orderBy('txtnomecredor')
           ->limit(10)
           ->get();

            // Retornar as consultas para a página
            return view('despesas/gastos-diretos/favorecido.index', [
               'favorecidos' => $favorecidos,
               'fase' => $request->fase,
               'mes' => $request->mes,
               'ano' => $request->ano,
               'classificacao' => $request->classificacao,
               'posicao' => $posicao,
               'nomefase' => $nomefase,
               'sumtotal' => $sumtotal,
               'grupodespesacharts' => $grupodespesacharts,
               'elementodespesacharts' => $elementodespesacharts,
               'busca' => $busca
            ]);
      }
      // Filtro Por Função
      else if ($request->classificacao == 'funcao') {
         $busca = new Buscar_por;
         $busca = $busca->generatebuscar_por([
            "None" => "",
            "txtdescricaofuncao" => "Função",
            "txtdescricaosubfuncao" => "Subfunção"
         ]);

         // Condição para qual tabela a ser consultada -> tabela Pagamentos terá diferenças em relação a Empenho e Liquidação
         $subquery = DB::connection('pgsql_siaf')
         ->table('vportal_notaempenho_gastosdiretos')
         ->select('txtdescricaofuncao',
                  'txtdescricaosubfuncao',
                  'txtdescricaoacao')
         ->selectRaw("codacao::varchar as codacao,
                      codsubfuncao::varchar as codsubfuncao")
         ->selectRaw("SUM(vlrpagamento) AS vlrempenho,
                      COALESCE(null, 0) as vlrliquido, 
                      COALESCE(null, 0) as vlrpagamento, 
                      COALESCE(null, 0) as vlrpagamentorp,
                      'vportal_notaempenho_gastosdiretos' as tbvportal,
                      'notaEmpenho' as tipo")
         ->where('numexerciciofinanc', $request->ano)
         ->groupBy('codacao', 'codsubfuncao', 'txtdescricaofuncao', 'txtdescricaosubfuncao', 'txtdescricaoacao');

         if($posicao === '='){
            $subquery->where('mes', '=', $request->mes);
         }else{
            $subquery->where('mes','<=', $request->mes);
         }

         $subquery2 = DB::connection('pgsql_siaf')
            ->table('vportal_liquidacaodespesa_gastosdiretos')
            ->select('txtdescricaofuncao',
                     'txtdescricaosubfuncao',
                     'txtdescricaoacao')
            ->selectRaw("codacao::varchar as codacao,
                         codsubfuncao::varchar as codsubfuncao")
            ->selectRaw("COALESCE(null, 0) as vlrempenho, 
                         SUM(vlrpagamento) AS vlrliquido,
                         COALESCE(null, 0) as vlrpagamento, 
                         COALESCE(null, 0) as vlrpagamentorp,
                         'vportal_liquidacaodespesa_gastosdiretos' as tbvportal,
                         'liquidacao' as tipo")
            ->where('numexerciciofinanc', $request->ano)
            ->groupBy('codacao', 'codsubfuncao', 'txtdescricaofuncao', 'txtdescricaosubfuncao', 'txtdescricaoacao');


         if($posicao === '='){
            $subquery2->where('mes', '=', $request->mes);
         }else{
            $subquery2->where('mes','<=', $request->mes);
         }

         $subquery3 = DB::connection('pgsql_siaf')
            ->table('vportal_notapagamento_gastosdiretos')
            ->select('txtdescricaofuncao',
                     'txtdescricaosubfuncao',
                     'txtdescricaoacao')
            ->selectRaw("codacao::varchar as codacao,
                         codsubfuncao::varchar as codsubfuncao")
            ->selectRaw("COALESCE(null, 0) AS vlrempenho,
                         COALESCE(null, 0) AS vlrliquido,
                         SUM(vlrpagamento) AS vlrpagamento,
                         SUM(vlrpagamentorp) AS vlrpagamentorp,
                         'vportal_notapagamento_gastosdiretos' as tbvportal,
                         case when SUM(vlrpagamento) > 0 then 'notaPagamento'
                              when SUM(vlrpagamentorp)  > 0 then 'valorPagar'
                         end as tipo")
            ->where('numexerciciofinanc', $request->ano)
            ->groupBy('codacao', 'codsubfuncao', 'txtdescricaofuncao', 'txtdescricaosubfuncao', 'txtdescricaoacao');

            if($posicao === '='){
               $subquery3->where('mes', '=', $request->mes);
            }else{
               $subquery3->where('mes','<=', $request->mes);
            }

         $funcao = DB::connection('pgsql_siaf')
            ->table(DB::raw("({$subquery->toSql()} UNION ALL {$subquery2->toSql()} UNION ALL {$subquery3->toSql()}) AS resultado"))
            ->setBindings(array_merge(
               $subquery->getBindings(),
               $subquery2->getBindings(),
               $subquery3->getBindings()
           ))
            ->select(
               'resultado.codacao',
               'resultado.codsubfuncao',
               'resultado.txtdescricaofuncao',
               'resultado.txtdescricaosubfuncao',
               'resultado.txtdescricaoacao',
               DB::raw("STRING_AGG(CONCAT(resultado.tbvportal, '/', resultado.tipo), ',')  AS result"), 
               DB::raw('COALESCE(SUM(resultado.vlrempenho), 0) AS vlrempenho'), 
               DB::raw('COALESCE(SUM(resultado.vlrliquido), 0) AS vlrliquido'), 
               DB::raw('COALESCE(SUM(resultado.vlrpagamento), 0) AS vlrpagamento'), 
               DB::raw('COALESCE(SUM(resultado.vlrpagamentorp), 0) AS vlrpagamentorp')
            )
            ->groupBy(
               'resultado.codacao',
               'resultado.codsubfuncao',
               'resultado.txtdescricaofuncao',
               'resultado.txtdescricaosubfuncao',
               'resultado.txtdescricaoacao'
            )
            ->orderBy('resultado.txtdescricaofuncao', 'ASC')
            ->orderBy('resultado.txtdescricaosubfuncao', 'ASC')
            ->orderBy('resultado.txtdescricaoacao', 'ASC')
            ->paginate(10);

           // Gráficos
           $grupofuncaocharts = $model::select('txtdescricaofuncao', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
           ->where('mes', $posicao, $request->mes)
           ->where('numexerciciofinanc', $request->ano)
           ->groupBy('txtdescricaofuncao')
           ->orderBy('txtdescricaofuncao')
           ->get();


         $elementofuncaocharts = $model::select('txtdescricaosubfuncao', 'txtdescricaofuncao', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
            ->where('mes', $posicao, $request->mes)
            ->where('numexerciciofinanc', $request->ano)
            ->groupBy('txtdescricaosubfuncao', 'txtdescricaofuncao')
            ->orderBy('txtdescricaosubfuncao')
            ->get();

         // Retornar as consultas para a página
         return view('despesas/gastos-diretos/funcao.index', [
            'funcoes' => $funcao,
            'fase' => $request->fase,
            'mes' => $request->mes,
            'ano' => $request->ano,
            'classificacao' => $request->classificacao,
            'posicao' => $posicao,
            'nomefase' => $nomefase,
            'sumtotal' => $sumtotal,
            'grupofuncaocharts' => $grupofuncaocharts,
            'elementofuncaocharts' => $elementofuncaocharts,
            'busca' => $busca
         ]);
      }
   }
   // Função para as consultas de detalhamento dos Documentos da página gastos-diretos-detalhada
   public function detalhamento($request)
   {
      $infos = (object) FacadesCrypt::decrypt($request);
      // Nome de qual fase de gastos a ser consultada
      if ($infos->fase == 'vportal_notaempenho_gastosdiretos') {
         $nomefase = 'Empenho';
         $model = NotaEmpenhoGastosDiretos::class;
      } else if ($infos->fase == 'vportal_liquidacaodespesa_gastosdiretos') {
         $nomefase = 'Liquidação';
         $model = LiquidacaoGastosDiretos::class;
      } else {
         // $fase == 'vportal_notapagamento_gastosdiretos'
         $nomefase = 'Pagamento';
         $model = NotaPagamentoGastosDiretos::class;
      }

      // Valor total dos gastos
      $sumtotal = $model::where('mes', $infos->posicao, $infos->mes)
         ->where('numexerciciofinanc', $infos->ano)
         ->sum('vlrpagamento');

      // Filtro Tipo de Despesa
      if ($infos->classificacao == 'despesa') {
         // Níveis de Navegação em Tipo de Despesa
         $nomedespesa = $model::select('txtdescricaoelementodespesa')
            ->where('codelementodespesa', $infos->codelementodespesa)
            ->limit(1)
            ->get();
         $sumdespesa = $model::where('codelementodespesa', $infos->codelementodespesa)
            ->where('mes', $infos->posicao, $infos->mes)
            ->where('numexerciciofinanc', $infos->ano)
            ->sum('vlrpagamento');

         $nomegestora = $model::select('txtdescricaounidade')
            ->where('codunidadegestora', $infos->codunidadegestora)
            ->limit(1)
            ->get();
         $sumgestora = $model::where('codunidadegestora', $infos->codunidadegestora)
            ->where('codelementodespesa', $infos->codelementodespesa)
            ->where('mes', $infos->posicao, $infos->mes)
            ->where('numexerciciofinanc', $infos->ano)
            ->sum('vlrpagamento');

         $nomegestao = $model::select('txtdescricaogestao')
            ->where('codgestao', $infos->codgestao)
            ->limit(1)
            ->get();
         $sumgestao = $model::where('codgestao', $infos->codgestao)
            ->where('codunidadegestora', $infos->codunidadegestora)
            ->where('codgrupodespesa', $infos->codgrupodespesa)
            ->where('codelementodespesa', $infos->codelementodespesa)
            ->where('mes', $infos->posicao, $infos->mes)
            ->where('numexerciciofinanc', $infos->ano)
            ->sum('vlrpagamento');

         // Condição para qual tabela a ser consultada -> tabela Pagamentos terá diferenças em relação a Empenho e Liquidação
         if ($infos->fase == 'vportal_notapagamento_gastosdiretos') {
            $favorecidos = $model::select('codcredor', 'txtnomecredor', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
               ->where('codgrupodespesa', $infos->codgrupodespesa)
               ->where('codelementodespesa', $infos->codelementodespesa)
               ->where('codunidadegestora', $infos->codunidadegestora)
               ->where('codgestao', $infos->codgestao)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('codcredor', 'txtnomecredor')
               ->get();

            $documentos = $model::select('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
               ->where('codgrupodespesa', $infos->codgrupodespesa)
               ->where('codelementodespesa', $infos->codelementodespesa)
               ->where('codunidadegestora', $infos->codunidadegestora)
               ->where('codgestao', $infos->codgestao)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora')
               ->get();
         } else {
            // Empenho e Liquidação
            $favorecidos = $model::select('codcredor', 'txtnomecredor', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
               ->where('codgrupodespesa', $infos->codgrupodespesa)
               ->where('codelementodespesa', $infos->codelementodespesa)
               ->where('codunidadegestora', $infos->codunidadegestora)
               ->where('codgestao', $infos->codgestao)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('codcredor', 'txtnomecredor')
               ->get();

            $documentos = $model::select('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
               ->where('codgrupodespesa', $infos->codgrupodespesa)
               ->where('codelementodespesa', $infos->codelementodespesa)
               ->where('codunidadegestora', $infos->codunidadegestora)
               ->where('codgestao', $infos->codgestao)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora')
               ->get();
         }
         // Retornar as consultas para a página
         return view('despesas/gastos-diretos/tipo-despesa.gastos-diretos-detalhada', [
            'favorecidos' => $favorecidos,
            'documentos' => $documentos,
            'fase' => $infos->fase,
            'nomefase' => $nomefase,
            'posicao' => $infos->posicao,
            'mes' => $infos->mes,
            'ano' => $infos->ano,
            'sumtotal' => $sumtotal,
            'nomedespesa' => $nomedespesa[0],
            'sumdespesa' => $sumdespesa,
            'nomegestora' => $nomegestora[0],
            'sumgestora' => $sumgestora,
            'nomegestao' => $nomegestao[0],
            'sumgestao' => $sumgestao
         ]);
      }
      // Filtro Por Unidade Gestora
      else if ($infos->classificacao ==  'gestora') {
         // Níveis de Navegação em Unidade Gestora
         $nomegestora = $model::select('txtdescricaounidade')
            ->where('codunidadegestora', $infos->codunidadegestora)
            ->limit(1)
            ->get();
         $sumgestora = $model::where('codunidadegestora', $infos->codunidadegestora)
            ->where('mes', $infos->posicao, $infos->mes)
            ->where('numexerciciofinanc', $infos->ano)
            ->sum('vlrpagamento');

         $nomegestao = $model::select('txtdescricaogestao')
            ->where('codgestao', $infos->codgestao)
            ->limit(1)
            ->get();
         $sumgestao = $model::where('codgestao', $infos->codgestao)
            ->where('codunidadegestora', $infos->codunidadegestora)
            ->where('mes', $infos->posicao, $infos->mes)
            ->where('numexerciciofinanc', $infos->ano)
            ->sum('vlrpagamento');

         $nomefavorecido = $model::select('txtnomecredor')
            ->where('codcredor', $infos->codcredor)
            ->limit(1)
            ->get();
         $sumfavorecido = $model::where('codcredor', $infos->codcredor)
            ->where('codunidadegestora', $infos->codunidadegestora)
            ->where('codgestao', $infos->codgestao)
            ->where('mes', $infos->posicao, $infos->mes)
            ->where('numexerciciofinanc', $infos->ano)
            ->sum('vlrpagamento');

         // Condição para qual tabela a ser consultada -> tabela Pagamentos terá diferenças em relação a Empenho e Liquidação
         if ($infos->fase == 'vportal_notapagamento_gastosdiretos') {
            $grupodespesa = $model::select('txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'codgrupodespesa', 'codelementodespesa', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
               ->where('codunidadegestora', $infos->codunidadegestora)
               ->where('codgestao', $infos->codgestao)
               ->where('codcredor', $infos->codcredor)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'codgrupodespesa', 'codelementodespesa')
               ->get();

            $documentos = $model::select('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', 'codgrupodespesa', 'codelementodespesa', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
               ->where('codunidadegestora', $infos->codunidadegestora)
               ->where('codgestao', $infos->codgestao)
               ->where('codcredor', $infos->codcredor)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', 'codgrupodespesa', 'codelementodespesa')
               ->get();
         } else {
            // Empenho e Liquidação
            $grupodespesa = $model::select('txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'codgrupodespesa', 'codelementodespesa', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
               ->where('codunidadegestora', $infos->codunidadegestora)
               ->where('codgestao', $infos->codgestao)
               ->where('codcredor', $infos->codcredor)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'codgrupodespesa', 'codelementodespesa')
               ->get();

            $documentos = $model::select('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', 'codgrupodespesa', 'codelementodespesa', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
               ->where('codunidadegestora', $infos->codunidadegestora)
               ->where('codgestao', $infos->codgestao)
               ->where('codcredor', $infos->codcredor)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', 'codgrupodespesa', 'codelementodespesa')
               ->get();
         }
         // Retornar as consultas para a página
         return view('despesas/gastos-diretos/unidade-gestora.gastos-diretos-detalhada', [
            'grupodespesa' => $grupodespesa,
            'documentos' => $documentos,
            'fase' => $infos->fase,
            'nomefase' => $nomefase,
            'posicao' => $infos->posicao,
            'mes' => $infos->mes,
            'ano' => $infos->ano,
            'sumtotal' => $sumtotal,
            'nomegestora' => $nomegestora[0],
            'sumgestora' => $sumgestora,
            'nomegestao' => $nomegestao[0],
            'sumgestao' => $sumgestao,
            'nomefavorecido' => $nomefavorecido[0],
            'sumfavorecido' => $sumfavorecido,
         ]);
      }

      // Filtro Por Ação
      else if ($infos->classificacao == 'acao') {
         // Níveis de Navegação em Ação
         $nomeacao = $model::select('txtdescricaoacao')
            ->where('codacao', $infos->codacao)
            ->limit(1)
            ->get();
         $sumacao = $model::where('codacao', $infos->codacao)
            ->where('mes', $infos->posicao, $infos->mes)
            ->where('numexerciciofinanc', $infos->ano)
            ->sum('vlrpagamento');                       

         // SUM de Transferencia de Recursos
         $sumtr = $model::where('mes', $infos->posicao, $infos->mes)
            ->where('numexerciciofinanc', $infos->ano)
            ->sum('vlrpagamento');

         // Condição para qual tabela a ser consultada -> tabela Pagamentos terá diferenças em relação a Empenho e Liquidação
         if ($infos->fase == 'vportal_notapagamento_gastosdiretos') {
            $favorecidos = $model::select('codcredor', 'txtnomecredor', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
               ->where('codacao', $infos->codacao)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('codcredor', 'txtnomecredor')
               ->get();

            $documentos = $model::select('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', 'codgrupodespesa', 'codelementodespesa', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
               ->where('codacao', $infos->codacao)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', 'codgrupodespesa', 'codelementodespesa')
               ->get();
         } else {
            // Empenho e Liquidação
            $favorecidos = $model::select('codcredor', 'txtnomecredor', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
               ->where('codacao', $infos->codacao)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('codcredor', 'txtnomecredor')
               ->get();

            $documentos = $model::select('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', 'codgrupodespesa', 'codelementodespesa', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
               ->where('codacao', $infos->codacao)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', 'codgrupodespesa', 'codelementodespesa')
               ->get();
         }
         // Retornar as consultas para a página
         return view('despesas/gastos-diretos/acao.gastos-diretos-detalhada', [
            'favorecidos' => $favorecidos,
            'documentos' => $documentos,
            'fase' => $infos->fase,
            'nomefase' => $nomefase,
            'posicao' => $infos->posicao,
            'mes' => $infos->mes,
            'ano' => $infos->ano,
            'sumtotal' => $sumtotal,
            'nomeacao' => $nomeacao[0],
            'sumacao' => $sumacao,
            'sumtr' => $sumtr
         ]);
      }

      // Filtro Por Fonte
      else if ($infos->classificacao == 'fonte') {
         // Níveis de Navegação por Fonte
         $nomefonte = $model::select('txtdescricaofonterecurso')
            ->where('codfonterecurso', $infos->codfonterecurso)
            ->limit(1)
            ->get();
         $sumfonte = $model::where('codfonterecurso', $infos->codfonterecurso)
            ->where('mes', $infos->posicao, $infos->mes)
            ->where('numexerciciofinanc', $infos->ano)
            ->sum('vlrpagamento');

         $nomegestora = $model::select('txtdescricaounidade')
            ->where('codunidadegestora', $infos->codunidadegestora)
            ->limit(1)
            ->get();
         $sumgestora = $model::where('codunidadegestora', $infos->codunidadegestora)
            ->where('codfonterecurso', $infos->codfonterecurso)
            ->where('mes', $infos->posicao, $infos->mes)
            ->where('numexerciciofinanc', $infos->ano)
            ->sum('vlrpagamento');

         $nomegestao = $model::select('txtdescricaogestao')
            ->where('codgestao', $infos->codgestao)
            ->limit(1)
            ->get();
         $sumgestao = $model::where('codgestao', $infos->codgestao)
            ->where('codunidadegestora', $infos->codunidadegestora)
            ->where('codfonterecurso', $infos->codfonterecurso)
            ->where('mes', $infos->posicao, $infos->mes)
            ->where('numexerciciofinanc', $infos->ano)
            ->sum('vlrpagamento');

         // Condição para qual tabela a ser consultada -> tabela Pagamentos terá diferenças em relação a Empenho e Liquidação
         if ($infos->fase == 'vportal_notapagamento_gastosdiretos') {
            $favorecidos = $model::select('codcredor', 'txtnomecredor', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
               ->where('codfonterecurso', $infos->codfonterecurso)
               ->where('codunidadegestora', $infos->codunidadegestora)
               ->where('codgestao', $infos->codgestao)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('codcredor', 'txtnomecredor')
               ->get();

            $documentos = $model::select('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
               ->where('codfonterecurso', $infos->codfonterecurso)
               ->where('codunidadegestora', $infos->codunidadegestora)
               ->where('codgestao', $infos->codgestao)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora')
               ->get();
         } else {
            // Empenho e Liquidação
            $favorecidos = $model::select('codcredor', 'txtnomecredor', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
               ->where('codfonterecurso', $infos->codfonterecurso)
               ->where('codunidadegestora', $infos->codunidadegestora)
               ->where('codgestao', $infos->codgestao)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('codcredor', 'txtnomecredor')
               ->get();

            $documentos = $model::select('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
               ->where('codfonterecurso', $infos->codfonterecurso)
               ->where('codunidadegestora', $infos->codunidadegestora)
               ->where('codgestao', $infos->codgestao)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora')
               ->get();
         }
         // Retornar as consultas para a página
         return view('despesas/gastos-diretos/fonte.gastos-diretos-detalhada', [
            'favorecidos' => $favorecidos,
            'documentos' => $documentos,
            'fase' => $infos->fase,
            'nomefase' => $nomefase,
            'posicao' => $infos->posicao,
            'mes' => $infos->mes,
            'ano' => $infos->ano,
            'sumtotal' => $sumtotal,
            'nomefonte' => $nomefonte[0],
            'sumfonte' => $sumfonte,
            'nomegestora' => $nomegestora[0],
            'sumgestora' => $sumgestora,
            'nomegestao' => $nomegestao[0],
            'sumgestao' => $sumgestao
         ]);
      }

      // Filtro Por Favorecido
      else if ($infos->classificacao == 'favorecido') {
         // Níveis de Navegação por Favorecido
         $nomefavorecido = $model::select('txtnomecredor')
            ->where('codcredor', $infos->codcredor)
            ->limit(1)
            ->get();
         $sumfavorecido = $model::where('codcredor', $infos->codcredor)
            ->where('mes', $infos->posicao, $infos->mes)
            ->where('numexerciciofinanc', $infos->ano)
            ->sum('vlrpagamento');

         $nomedespesa = $model::select('txtdescricaoelementodespesa')
            ->where('codelementodespesa', $infos->codelementodespesa)
            ->limit(1)
            ->get();
         $sumdespesa = $model::where('codelementodespesa', $infos->codelementodespesa)
            ->where('codcredor', $infos->codcredor)
            ->where('mes', $infos->posicao, $infos->mes)
            ->where('numexerciciofinanc', $infos->ano)
            ->sum('vlrpagamento');

         $nomegestora = $model::select('txtdescricaounidade')
            ->where('codunidadegestora', $infos->codunidadegestora)
            ->limit(1)
            ->get();
         $sumgestora = $model::where('codunidadegestora', $infos->codunidadegestora)
            ->where('codcredor', $infos->codcredor)
            ->where('codelementodespesa', $infos->codelementodespesa)
            ->where('mes', $infos->posicao, $infos->mes)
            ->where('numexerciciofinanc', $infos->ano)
            ->sum('vlrpagamento');

         // Condição para qual tabela a ser consultada -> tabela Pagamentos terá diferenças em relação a Empenho e Liquidação
         if ($infos->fase == 'vportal_notapagamento_gastosdiretos') {
            $gestao = $model::select('codgestao', 'txtdescricaogestao', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
               ->where('codgrupodespesa', $infos->codgrupodespesa)
               ->where('codelementodespesa', $infos->codelementodespesa)
               ->where('codunidadegestora', $infos->codunidadegestora)
               ->where('codcredor', $infos->codcredor)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('codgestao', 'txtdescricaogestao')
               ->get();

            $documentos = $model::select('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', 'codgestao', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
               ->where('codgrupodespesa', $infos->codgrupodespesa)
               ->where('codelementodespesa', $infos->codelementodespesa)
               ->where('codunidadegestora', $infos->codunidadegestora)
               ->where('codcredor', $infos->codcredor)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', 'codgestao')
               ->get();
         } else {
            // Empenho e Liquidação
            $gestao = $model::select('codgestao', 'txtdescricaogestao', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
               ->where('codgrupodespesa', $infos->codgrupodespesa)
               ->where('codelementodespesa', $infos->codelementodespesa)
               ->where('codunidadegestora', $infos->codunidadegestora)
               ->where('codcredor', $infos->codcredor)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('codgestao', 'txtdescricaogestao')
               ->get();

            $documentos = $model::select('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', 'codgestao', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
               ->where('codgrupodespesa', $infos->codgrupodespesa)
               ->where('codelementodespesa', $infos->codelementodespesa)
               ->where('codunidadegestora', $infos->codunidadegestora)
               ->where('codcredor', $infos->codcredor)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', 'codgestao')
               ->get();
         }
         // Retornar as consultas para a página
         return view('despesas/gastos-diretos/favorecido.gastos-diretos-detalhada', [
            'gestao' => $gestao,
            'documentos' => $documentos,
            'fase' => $infos->fase,
            'nomefase' => $nomefase,
            'posicao' => $infos->posicao,
            'mes' => $infos->mes,
            'ano' => $infos->ano,
            'sumtotal' => $sumtotal,
            'nomefavorecido' => $nomefavorecido[0],
            'sumfavorecido' => $sumfavorecido,
            'nomedespesa' => $nomedespesa[0],
            'sumdespesa' => $sumdespesa,
            'nomegestora' => $nomegestora[0],
            'sumgestora' => $sumgestora
         ]);
      }

      // Filtro Por Função
      else if ($infos->classificacao == 'funcao') {
         // Níveis de Navegação por Função
         $nomefuncao = $model::select('txtdescricaofuncao')
            ->where('txtdescricaofuncao', $infos->txtdescricaofuncao)
            ->limit(1)
            ->get();
         $sumfuncao = $model::where('txtdescricaofuncao', $infos->txtdescricaofuncao)
            ->where('mes', $infos->posicao, $infos->mes)
            ->where('numexerciciofinanc', $infos->ano)
            ->sum('vlrpagamento');

         $nomesubfuncao = $model::select('txtdescricaosubfuncao')
            ->where('codsubfuncao', $infos->codsubfuncao)
            ->limit(1)
            ->get();
         $sumsubfuncao = $model::where('codsubfuncao', $infos->codsubfuncao)
            ->where('txtdescricaofuncao', $infos->txtdescricaofuncao)
            ->where('mes', $infos->posicao, $infos->mes)
            ->where('numexerciciofinanc', $infos->ano)
            ->sum('vlrpagamento');

         $nomeacao = $model::select('txtdescricaoacao')
            ->where('txtdescricaoacao', $infos->txtdescricaoacao)
            ->limit(1)
            ->get();
         $sumacao = $model::where('codacao', $infos->codacao)
            ->where('txtdescricaofuncao', $infos->txtdescricaofuncao)
            ->where('codsubfuncao', $infos->codsubfuncao)
            ->where('mes', $infos->posicao, $infos->mes)
            ->where('numexerciciofinanc', $infos->ano)
            ->sum('vlrpagamento');

         if ($infos->fase == 'vportal_notapagamento_gastosdiretos') {
            // Condição para qual tabela a ser consultada -> tabela Pagamentos terá diferenças em relação a Empenho e Liquidação
            $favorecidos = $model::select('codcredor', 'txtnomecredor', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
               ->where('txtdescricaofuncao', $infos->txtdescricaofuncao)
               ->where('codsubfuncao', $infos->codsubfuncao)
               ->where('codacao', $infos->codacao)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('codcredor', 'txtnomecredor')
               ->get();

            $documentos = $model::select('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
               ->where('txtdescricaofuncao', $infos->txtdescricaofuncao)
               ->where('codsubfuncao', $infos->codsubfuncao)
               ->where('codacao', $infos->codacao)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora')
               ->get();
         } else {
            // Empenho e Liquidação
            $favorecidos = $model::select('codcredor', 'txtnomecredor', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
               ->where('txtdescricaofuncao', $infos->txtdescricaofuncao)
               ->where('codsubfuncao', $infos->codsubfuncao)
               ->where('codacao', $infos->codacao)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('codcredor', 'txtnomecredor')
               ->get();

            $documentos = $model::select('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
               ->where('txtdescricaofuncao', $infos->txtdescricaofuncao)
               ->where('codsubfuncao', $infos->codsubfuncao)
               ->where('codacao', $infos->codacao)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora')
               ->get();
         }
         // Retornar as consultas para a página
         return view('despesas/gastos-diretos/funcao.gastos-diretos-detalhada', [
            'favorecidos' => $favorecidos,
            'documentos' => $documentos,
            'fase' => $infos->fase,
            'nomefase' => $nomefase,
            'posicao' => $infos->posicao,
            'mes' => $infos->mes,
            'ano' => $infos->ano,
            'sumtotal' => $sumtotal,
            'nomefuncao' => $nomefuncao[0],
            'sumfuncao' => $sumfuncao,
            'nomesubfuncao' => $nomesubfuncao[0],
            'sumsubfuncao' => $sumsubfuncao,
            'nomeacao' => $nomeacao[0],
            'sumacao' => $sumacao
         ]);
      }
   }
   // Função para a ultima página de detalhamento
   public function detalhes($request)
   {
      // Condição para qual tabela a ser consultada -> tabela Pagamentos terá diferenças em relação a Empenho
      $infos = (object) FacadesCrypt::decrypt($request);
      if ($infos->fase == 'vportal_notaempenho_gastosdiretos') {
         $table = 'vw_vportalnotaempenho';
         $documento = 'numempenho';
         $nomefase = 'Empenho';

         $detalhesdocumento = DB::connection('pgsql_siaf')->table($table)
            ->where($documento, $infos->numdocumento)
            ->where('codcredor', $infos->codcredor)
            ->where('txtdescricaoprograma', $infos->txtdescricaoprograma)
            ->where('txtdescricaoacao', $infos->txtdescricaoacao)
            ->where('codunidadegestora', $infos->codunidadegestora)
            ->get();

         // Retornar as consultas para a página
         return view('despesas/gastos-diretos.detalhes-documento', [
            'detalhesdocumento' => $detalhesdocumento,
            'fase' => $infos->fase,
            'nomefase' => $nomefase
         ]);
      }
      // Liquidação
      else if ($infos->fase == 'vportal_liquidacaodespesa_gastosdiretos') {
         $table = 'vw_portalnotalancamento';
         $documento = 'numnotalancamento';
         $nomefase = 'Liquidação';

         $detalhesdocumento = DB::connection('pgsql_siaf')->table($table)
            ->where($documento, $infos->numdocumento)
            ->where('codcredor', $infos->codcredor)
            ->where('txtdescricaoprograma', $infos->txtdescricaoprograma)
            ->where('txtdescricaoacao', $infos->txtdescricaoacao)
            ->where('codunidadegestora', $infos->codunidadegestora)
            ->get();
         
         // Retornar as consultas para a página
         return view('despesas/gastos-diretos.detalhes-documento', [
            'detalhesdocumento' => $detalhesdocumento,
            'fase' => $infos->fase,
            'nomefase' => $nomefase
         ]);
      }
      // Pagamentos -> $fase == 'vportal_notapagamento_gastosdiretos'
      else {
         $table = 'vportalordembancaria';
         $documento = 'preparacaopagamento';
         $nomefase = 'Pagamento';

         // restos a pagar será a tabela vportalordembancariarp
         $detalhesrestoapagar = DB::connection('pgsql_siaf')->table($table)
            ->where($documento, $infos->numdocumento)
            ->where('codcredor', $infos->codcredor)
            ->where('txtdescricaoprograma', $infos->txtdescricaoprograma)
            ->where('txtdescricaoacao', $infos->txtdescricaoacao)
            ->where('codunidadegestora', $infos->codunidadegestora)
            ->get();

         $detalhesdocumento = DB::connection('pgsql_siaf')->table($table)
            ->where($documento, $infos->numdocumento)
            ->where('codcredor', $infos->codcredor)
            ->where('txtdescricaoprograma', $infos->txtdescricaoprograma)
            ->where('txtdescricaoacao', $infos->txtdescricaoacao)
            ->where('codunidadegestora', $infos->codunidadegestora)
            ->get();

         // Retornar as consultas para a página
         return view('despesas/gastos-diretos.detalhes-documento', [
            'detalhesdocumento' => $detalhesdocumento,
            'detalhesrestoapagar' => $detalhesrestoapagar,
            'fase' => $infos->fase,
            'nomefase' => $nomefase
         ]);
      }
   }
   // Função para Fazer Download das consultas para planilha excel
   public function exportxls($mes, $ano, $posicao, $classificacao, $fase = null)
   {
      return Excel::download(new GastosDiretosExport($mes, $ano, $posicao, $classificacao, $fase), 'gastos-diretos.xlsx');
   }
   // Função para Fazer Download das consultas para PDF
   public function exportpdf($mes, $ano, $posicao, $classificacao, $fase = null)
   {
      return Excel::download(new GastosDiretosExport($mes, $ano, $posicao, $classificacao, $fase), 'gastos-diretos.pdf');
   }
}