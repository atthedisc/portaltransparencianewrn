<?php

namespace App\Http\Controllers\Despesas;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\GastosDireto\NotaEmpenhoGastosDiretos;

class DespesasController extends Controller
{
   public function __invoke(Request $request)
   {
      $data = Carbon::now();
      $mescorrente = $data->month;

       $teste = NotaEmpenhoGastosDiretos::all();
       
      $meses = collect([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]);

      return view('despesas.index', [ 'meses' => $meses, 'mescorrente' => $mescorrente, 'teste' => $teste ]);
   } 
}
