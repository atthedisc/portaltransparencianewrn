<?php

namespace App\Http\Controllers\Despesas;

use App\Components\Busca\Buscar_por;
use App\Exports\Despesas\GastosDiretosExport;
use App\Http\Controllers\Controller;

use App\Models\GastosDireto\LiquidacaoGastosDiretos;
use App\Models\GastosDireto\NotaEmpenhoGastosDiretos;
use App\Models\GastosDireto\NotaPagamentoGastosDiretos;

use App\Models\GastosDireto\VPoralGastosDiretos;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Crypt as FacadesCrypt;

class GastosDiretosController extends Controller
{
    
   public function index(Request $request){

        if (!$request->query() && !$request->input()) {
           return redirect()->route('despesas');
        }
     
        $posicao = ($request->posicao == 'Acumulado') ? '<=' : '=';

        $sumtotal = LiquidacaoGastosDiretos::where('mes', $posicao, $request->mes)->where('numexerciciofinanc', $request->ano)->sum('vlrpagamento');

       // #Filtro por Despesas
      if ($request->classificacao == 'despesa') {

         $busca = (new Buscar_por())->generatebuscar_por([
             "None" => "",
             "txtdescricaogrupodespesa" => "Grupo de despesa",
             "txtdescricaoelementodespesa" => "Elemento de despesa"
         ]);

         $createSubquery = function($table, $select) use ($request, $posicao) {

             if($table == 'vportal_notaempenho_gastosdiretos'){
                $modelo = NotaEmpenhoGastosDiretos::class;
             } elseif ($table == 'vportal_notapagamento_gastosdiretos'){
                $modelo = NotaPagamentoGastosDiretos::class;
             } else {
                $modelo = LiquidacaoGastosDiretos::class;
             }

             $query = $modelo::where('numexerciciofinanc', $request->ano)
                 ->select('txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'txtdescricaounidade', 'txtdescricaogestao', 'codelementodespesa', 'codunidadegestora', 'codgestao' )
                 ->selectRaw('CAST(codgrupodespesa AS varchar) AS codgrupodespesa')
                 ->selectRaw($select)
                 ->groupBy( 'txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'txtdescricaounidade', 'txtdescricaogestao', 'codgrupodespesa', 'codelementodespesa', 'codunidadegestora', 'codgestao' );

             if ($posicao === '=') {
                 $query->where('mes', '=', $request->mes);
             } else {
                 $query->where('mes', '<=', $request->mes);
             }

             return $query;
         };

         $subquery1 = $createSubquery(
             'vportal_notaempenho_gastosdiretos',
             "SUM(vlrpagamento) AS vlrempenho, 0 AS vlrliquido, 0 AS vlrpagamento, 0 AS vlrpagamentorp, 'vportal_notaempenho_gastosdiretos' as tbvportal"
         );
         $subquery2 = $createSubquery(
             'vportal_liquidacaodespesa_gastosdiretos',
             "0 AS vlrempenho, SUM(vlrpagamento) AS vlrliquido, 0 AS vlrpagamento, 0 AS vlrpagamentorp, 'vportal_liquidacaodespesa_gastosdiretos' as tbvportal"
         );
         $subquery3 = $createSubquery(
             'vportal_notapagamento_gastosdiretos',
             "0 AS vlrempenho, 0 AS vlrliquido, SUM(vlrpagamento) AS vlrpagamento, SUM(vlrpagamentorp) AS vlrpagamentorp, 'vportal_notapagamento_gastosdiretos' as tbvportal"
         );

         // Junta as subconsultas
         $grupodespesa = DB::connection('pgsql_siaf')
             ->table(DB::raw("({$subquery1->toSql()} UNION ALL {$subquery2->toSql()} UNION ALL {$subquery3->toSql()}) AS resultado"))
             ->setBindings(array_merge(
                 $subquery1->getBindings(),
                 $subquery2->getBindings(),
                 $subquery3->getBindings()
             ))
             ->select(
                 'resultado.txtdescricaogrupodespesa',
                 'resultado.txtdescricaoelementodespesa',
                 'resultado.txtdescricaounidade',
                 'resultado.txtdescricaogestao',
                 DB::raw("STRING_AGG(CONCAT(resultado.codgrupodespesa, '/', resultado.tbvportal), ',') AS result"),
                 'resultado.codelementodespesa',
                 'resultado.codunidadegestora',
                 'resultado.codgestao',
                 DB::raw('SUM(resultado.vlrempenho) AS vlrempenho'),
                 DB::raw('SUM(resultado.vlrliquido) AS vlrliquido'),
                 DB::raw('SUM(resultado.vlrpagamento) AS vlrpagamento'),
                 DB::raw('SUM(resultado.vlrpagamentorp) AS vlrpagamentorp')
             )
             ->groupBy(
                 'resultado.txtdescricaogrupodespesa',
                 'resultado.txtdescricaoelementodespesa',
                 'resultado.txtdescricaounidade',
                 'resultado.txtdescricaogestao',
                 'resultado.codelementodespesa',
                 'resultado.codunidadegestora',
                 'resultado.codgestao'
             )
             ->orderBy('resultado.txtdescricaogrupodespesa', 'ASC')
             ->orderBy('resultado.txtdescricaoelementodespesa', 'ASC')
             ->orderBy('resultado.txtdescricaounidade', 'ASC')
             ->paginate(10);

        // Construção do Gráfico
        $grupodespesacharts = LiquidacaoGastosDiretos::select('txtdescricaogrupodespesa', 'codgrupodespesa', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
        ->where('mes', $posicao, $request->mes)
        ->where('numexerciciofinanc', $request->ano)
        ->groupBy('txtdescricaogrupodespesa', 'codgrupodespesa')
        ->orderBy('txtdescricaogrupodespesa')
        ->get();
           
         // Retornar as consultas para a página
         return view('despesas/gastos-diretos/tipo-despesa.index', [
             'grupodespesa' => $grupodespesa,
             'fase' => $request->fase,
             'mes' => $request->mes,
             'ano' => $request->ano,
             'classificacao' => $request->classificacao,
             'posicao' => $posicao,
             'nomefase' => null,
             'sumtotal' => $sumtotal,
             'grupodespesacharts' => $grupodespesacharts,
             'elementodespesacharts' => $grupodespesacharts,
             'busca' => $busca
         ]);
          
      }
       
       // #Filtro Por Unidade Gestora
       else if ($request->classificacao == 'gestora') {
          
         $busca = (new Buscar_por)->generatebuscar_por([
            "None" => "",
            "txtdescricaounidade" => "Unidade Gestora",
        ]);
          
        $queries = [
            'vportal_notaempenho_gastosdiretos' => [
                'vlrempenho' => 'SUM(vlrpagamento)', 
                'vlrliquido' => 'COALESCE(NULL, 0)', 
                'vlrpagamento' => 'COALESCE(NULL, 0)', 
                'vlrpagamentorp' => 'COALESCE(NULL, 0)'
            ],
            'vportal_liquidacaodespesa_gastosdiretos' => [
                'vlrempenho' => 'COALESCE(NULL, 0)', 
                'vlrliquido' => 'SUM(vlrpagamento)', 
                'vlrpagamento' => 'COALESCE(NULL, 0)', 
                'vlrpagamentorp' => 'COALESCE(NULL, 0)'
            ],
            'vportal_notapagamento_gastosdiretos' => [
                'vlrempenho' => 'COALESCE(NULL, 0)', 
                'vlrliquido' => 'COALESCE(NULL, 0)', 
                'vlrpagamento' => 'SUM(vlrpagamento)', 
                'vlrpagamentorp' => 'SUM(vlrpagamentorp)'
            ]
        ];

        // Função para criar subconsultas
         $createSubquery = function($table, $columns) use ($request, $posicao) 
         {
            if (!is_array($columns)) {
               throw new \Exception("Esperado um array para columns, recebido " . gettype($columns));
            }

            $subquery = DB::connection('pgsql_siaf')->table($table)
               ->select('codunidadegestora', 'codgestao', 'codcredor', 'txtdescricaounidade', 'txtdescricaogestao', 'txtnomecredor')
               ->selectRaw("{$columns['vlrempenho']} AS vlrempenho, {$columns['vlrliquido']} AS vlrliquido, {$columns['vlrpagamento']} AS vlrpagamento, {$columns['vlrpagamentorp']} AS vlrpagamentorp, '$table' as tbvportal")
               ->where('numexerciciofinanc', $request->ano)
               ->groupBy('codunidadegestora', 'codgestao', 'codcredor', 'txtdescricaounidade', 'txtdescricaogestao', 'txtnomecredor');

            if ($posicao === '=') {
               $subquery->where('mes', '=', $request->mes);
            } else {
               $subquery->where('mes', '<=', $request->mes);
            }

            return $subquery;
         };

         // Criar as subconsultas
         $subqueries = array_map(function($table, $columns) use ($createSubquery) {
            if (!is_array($columns)) {
               throw new \Exception("Esperado um array para columns, recebido " . gettype($columns));
            }
            return $createSubquery($table, $columns);
         }, array_keys($queries), $queries);

         // Unir as subconsultas
         $unionQuery = $subqueries[0];
         for ($i = 1; $i < count($subqueries); $i++) {
            $unionQuery->unionAll($subqueries[$i]);
         }

         $unidadegestora = DB::connection('pgsql_siaf')
            ->table(DB::raw("({$unionQuery->toSql()}) AS resultado"))
            ->setBindings($unionQuery->getBindings())
            ->select(
               'resultado.codunidadegestora', 
               'resultado.codgestao', 
               'resultado.codcredor',
               'resultado.txtdescricaounidade',
               'resultado.txtdescricaogestao',
               'resultado.txtnomecredor',
               DB::raw("STRING_AGG(resultado.tbvportal, ',') AS result"), 
               DB::raw('COALESCE(SUM(resultado.vlrempenho), 0) AS vlrempenho'), 
               DB::raw('COALESCE(SUM(resultado.vlrliquido), 0) AS vlrliquido'), 
               DB::raw('COALESCE(SUM(resultado.vlrpagamento), 0) AS vlrpagamento'), 
               DB::raw('COALESCE(SUM(resultado.vlrpagamentorp), 0) AS vlrpagamentorp')
            )
            ->groupBy(
               'resultado.codunidadegestora', 
               'resultado.codgestao', 
               'resultado.codcredor',
               'resultado.txtdescricaounidade',
               'resultado.txtdescricaogestao',
               'resultado.txtnomecredor'
            )
            ->orderBy('resultado.txtdescricaounidade', 'ASC')
            ->orderBy('resultado.txtdescricaogestao', 'ASC')
            ->orderBy('resultado.txtnomecredor', 'ASC')
            ->paginate(10);

         // Construção do Gráfico
         $grupodespesacharts = LiquidacaoGastosDiretos::select('txtdescricaogestao', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
         ->where('mes', $posicao, $request->mes)
         ->where('numexerciciofinanc', $request->ano)
         ->groupBy('txtdescricaogestao')
         ->orderBy('txtdescricaogestao')
         ->get();

         // Retornar as consultas para a página
         return view('despesas/gastos-diretos/unidade-gestora.index', [
            'unidadegestora' => $unidadegestora,
            'fase' => $request->fase,
            'mes' => $request->mes,
            'ano' => $request->ano,
            'classificacao' => $request->classificacao,
            'posicao' => $posicao,
            'nomefase' => null,
            'sumtotal' => $sumtotal,
            'grupodespesacharts' => $grupodespesacharts,
            'elementodespesacharts' => $grupodespesacharts,
            "busca" => $busca
         ]);
      }
       
      // #Filtro Por Ação
      else if ($request->classificacao == 'acao') {
          
        $busca = (new Buscar_por)->generatebuscar_por([
            "None" => "",
            "txtdescricaoacao" => "Ação",
        ]);

        $baseQuery = function($table, $columns) use ($request, $posicao) {
            
            $query = DB::connection('pgsql_siaf')->table($table)->select('codacao')->selectRaw("INITCAP(txtdescricaoacao) as txtdescricaoacao, $columns")->where('numexerciciofinanc', $request->ano)->groupBy('codacao', 'txtdescricaoacao');

            if ($posicao === '=') {
                $query->where('mes', '=', $request->mes);
            } else {
                $query->where('mes', '<=', $request->mes);
            }

            return $query;
        };

        $subquery1 = $baseQuery('vportal_notaempenho_gastosdiretos', "
            SUM(vlrpagamento) AS vlrempenho, 
            COALESCE(null, 0) as vlrliquido,
            COALESCE(null, 0) as vlrpagamento, 
            COALESCE(null, 0) as vlrpagamentorp,
            'vportal_notaempenho_gastosdiretos' as tbvportal,
            'notaEmpenho' as tipo
        ", 'vlrempenho', 'notaEmpenho');

        $subquery2 = $baseQuery('vportal_liquidacaodespesa_gastosdiretos', "
            COALESCE(null, 0) as vlrempenho, 
            SUM(vlrpagamento) AS vlrliquido,
            COALESCE(null, 0) as vlrpagamento, 
            COALESCE(null, 0) as vlrpagamentorp,
            'vportal_liquidacaodespesa_gastosdiretos' as tbvportal,
            'liquidacao' as tipo
        ", 'vlrliquido', 'liquidacao');

        $subquery3 = $baseQuery('vportal_notapagamento_gastosdiretos', "
            COALESCE(null, 0) AS vlrempenho,
            COALESCE(null, 0) AS vlrliquido,
            SUM(vlrpagamento) AS vlrpagamento,
            SUM(vlrpagamentorp) AS vlrpagamentorp,
            'vportal_notapagamento_gastosdiretos' as tbvportal,
            CASE 
                WHEN SUM(vlrpagamento) > 0 THEN 'notaPagamento'
                WHEN SUM(vlrpagamentorp) > 0 THEN 'valorPagar'
            END as tipo
        ", 'vlrpagamento', 'notaPagamento');

        $acao = DB::connection('pgsql_siaf')
            ->table(DB::raw("({$subquery1->toSql()} UNION ALL {$subquery2->toSql()} UNION ALL {$subquery3->toSql()}) AS resultado"))
            ->mergeBindings($subquery1)
            ->mergeBindings($subquery2)
            ->mergeBindings($subquery3)
            ->select(
                'resultado.txtdescricaoacao',
                DB::raw("STRING_AGG(CONCAT(CONCAT(resultado.codacao, '/', resultado.tbvportal), '/', resultado.tipo), ',') as result"), 
                DB::raw('COALESCE(SUM(resultado.vlrempenho), 0) AS vlrempenho'), 
                DB::raw('COALESCE(SUM(resultado.vlrliquido), 0) AS vlrliquido'), 
                DB::raw('COALESCE(SUM(resultado.vlrpagamento), 0) AS vlrpagamento'), 
                DB::raw('COALESCE(SUM(resultado.vlrpagamentorp), 0) AS vlrpagamentorp')
            )
            ->groupBy('resultado.txtdescricaoacao')
            ->orderBy('resultado.txtdescricaoacao', 'ASC')
            ->paginate(10);

        // Construção do Gráfico
        $grupodespesacharts = LiquidacaoGastosDiretos::select('txtdescricaoacao', 'codgrupodespesa', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
        ->where('mes', $posicao, $request->mes)
        ->where('numexerciciofinanc', $request->ano)
        ->groupBy('txtdescricaoacao', 'codgrupodespesa')
        ->orderBy('txtdescricaoacao')
        ->get();

         // Retornar as consultas para a página
         return view('despesas/gastos-diretos/acao.index', [
            'acoes' => $acao,
            'fase' => $request->fase,
            'mes' => $request->mes,
            'ano' => $request->ano,
            'classificacao' => $request->classificacao,
            'posicao' => $posicao,
            'nomefase' => null,
            'sumtotal' => $sumtotal,
            'grupodespesacharts' => $grupodespesacharts,
            'elementodespesacharts' => $grupodespesacharts,
            'sumtotal' => $sumtotal,
            'busca' => $busca
         ]);      
      }
       
      // ## TESTA: Filtro Por Fonte
      else if ($request->classificacao == 'fonte') {

            // Configuração inicial
            $busca = new Buscar_por;
            $busca = $busca->generatebuscar_por([
                "None" => "",
                "txtdescricaofonterecurso" => "Fonte de recurso",
            ]);

            // Função para gerar subqueries
            $basequery = function($table, $selectRaw) use ($request, $posicao){

               if($table == 'vportal_notaempenho_gastosdiretos'){
                  $modelo = NotaEmpenhoGastosDiretos::class;
               } elseif ($table == 'vportal_notapagamento_gastosdiretos'){
                  $modelo = NotaPagamentoGastosDiretos::class;
               } else {
                  $modelo = LiquidacaoGastosDiretos::class;
               }

                $subquery = $modelo::where('numexerciciofinanc', $request->ano)
                    ->select('codfonterecurso', 'codunidadegestora', 'codgestao', 'txtdescricaofonterecurso', 'txtdescricaounidade', 'txtdescricaogestao')
                    ->selectRaw($selectRaw)
                    ->where('numexerciciofinanc', $request->ano)
                    ->groupBy('codfonterecurso', 'codunidadegestora', 'codgestao', 'txtdescricaofonterecurso', 'txtdescricaounidade', 'txtdescricaogestao');

                  if ($posicao === '=') {
                     $subquery->where('mes', '=', $request->mes);
                 } else {
                     $subquery->where('mes', '<=', $request->mes);
                 }

                return $subquery;
            };

            // Subqueries
            $subquery1 = $basequery(
                'vportal_notaempenho_gastosdiretos',
                "SUM(vlrpagamento) AS vlrempenho, 
                COALESCE(NULL, 0) as vlrliquido, 
                COALESCE(NULL, 0) as vlrpagamento, 
                COALESCE(NULL, 0) as vlrpagamentorp, 
                'vportal_notaempenho_gastosdiretos' as tbvportal, 
                'notaEmpenho' as tipo"
            );

            $subquery2 = $basequery(
                'vportal_liquidacaodespesa_gastosdiretos',
                "COALESCE(NULL, 0) as vlrempenho, SUM(vlrpagamento) AS vlrliquido, COALESCE(NULL, 0) as vlrpagamento, COALESCE(NULL, 0) as vlrpagamentorp, 'vportal_liquidacaodespesa_gastosdiretos' as tbvportal, 'liquidacao' as tipo"
            );

            $subquery3 = $basequery(
                'vportal_notapagamento_gastosdiretos',
                "COALESCE(NULL, 0) AS vlrempenho, COALESCE(NULL, 0) AS vlrliquido, SUM(vlrpagamento) AS vlrpagamento, SUM(vlrpagamentorp) AS vlrpagamentorp, 'vportal_notapagamento_gastosdiretos' as tbvportal, CASE WHEN SUM(vlrpagamento) > 0 THEN 'notaPagamento' WHEN SUM(vlrpagamentorp) > 0 THEN 'valorPagar' END as tipo"
            );

            // União das subqueries
            $fontes = DB::connection('pgsql_siaf')
                ->table(DB::raw("({$subquery1->toSql()} UNION ALL {$subquery2->toSql()} UNION ALL {$subquery3->toSql()}) AS resultado"))
                ->mergeBindings($subquery1->getQuery())
                ->mergeBindings($subquery2->getQuery())
                ->mergeBindings($subquery3->getQuery())
                ->select(
                    'resultado.codunidadegestora', 'resultado.codgestao', 'resultado.txtdescricaofonterecurso', 'resultado.txtdescricaounidade', 'resultado.txtdescricaogestao',
                    DB::raw("STRING_AGG(CONCAT(CONCAT(resultado.codfonterecurso, '/', resultado.tbvportal), '/', resultado.tipo), ',') AS result"),
                    DB::raw('COALESCE(SUM(resultado.vlrempenho), 0) AS vlrempenho'),
                    DB::raw('COALESCE(SUM(resultado.vlrliquido), 0) AS vlrliquido'),
                    DB::raw('COALESCE(SUM(resultado.vlrpagamento), 0) AS vlrpagamento'),
                    DB::raw('COALESCE(SUM(resultado.vlrpagamentorp), 0) AS vlrpagamentorp')
                )
                ->groupBy('resultado.codunidadegestora', 'resultado.codgestao', 'resultado.txtdescricaofonterecurso', 'resultado.txtdescricaounidade', 'resultado.txtdescricaogestao')
                ->orderBy('resultado.txtdescricaofonterecurso', 'ASC')
                ->orderBy('resultado.txtdescricaounidade', 'ASC')
                ->orderBy('resultado.txtdescricaogestao', 'ASC')
                ->paginate(10);

            // Construção do Gráfico
            $grupodespesacharts = LiquidacaoGastosDiretos::select('txtdescricaogestao', 'codgrupodespesa', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
            ->where('mes', $posicao, $request->mes)
            ->where('numexerciciofinanc', $request->ano)
            ->groupBy('txtdescricaogestao', 'codgrupodespesa')
            ->orderBy('txtdescricaogestao')
            ->get();

            // Retornar as consultas para a página
            return view('despesas/gastos-diretos/fonte.index', [
                'fontes' => $fontes,
                'mes' => $request->mes,
                'fase' => null,
                'ano' => $request->ano,
                'classificacao' => $request->classificacao,
                'posicao' => $posicao,
                'sumtotal' => $sumtotal,
                'grupodespesacharts' => $grupodespesacharts,
                'elementodespesacharts' => $grupodespesacharts,
                'busca' => $busca
            ]);
           
      }
       
      // ## TESTA: Filtro Por Favorecido
      else if ($request->classificacao == 'favorecido') {
         
            // Configuração inicial
            $busca = new Buscar_por;
            $busca = $busca->generatebuscar_por([
                "None" => "",
                "txtnomecredor" => "Favorecido",
            ]);

            // Função para criar subconsultas
            $basequery = function($table, $selectRaw) use ($request, $posicao){

               if($table == 'vportal_notaempenho_gastosdiretos'){
                  $modelo = NotaEmpenhoGastosDiretos::class;
               } elseif ($table == 'vportal_notapagamento_gastosdiretos'){
                  $modelo = NotaPagamentoGastosDiretos::class;
               } else {
                  $modelo = LiquidacaoGastosDiretos::class;
               }

                $subquery = $modelo::where('numexerciciofinanc', $request->ano)
                    ->select('txtnomecredor', 'txtdescricaounidade', 'txtdescricaogrupodespesa', 'txtdescricaoelementodespesa')
                    ->selectRaw($selectRaw)
                    ->groupBy('codcredor', 'codgrupodespesa', 'codelementodespesa', 'codunidadegestora', 'txtnomecredor', 'txtdescricaounidade', 'txtdescricaogrupodespesa', 'txtdescricaoelementodespesa');

                  if ($posicao === '=') {
                     $subquery->where('mes', '=', $request->mes);
                  } else {
                     $subquery->where('mes', '<=', $request->mes);
                  }

                return $subquery;
            };

            // Subqueries
            $subquery1 = $basequery(
                'vportal_notaempenho_gastosdiretos',
                "codcredor::varchar as codcredor, codgrupodespesa::varchar as codgrupodespesa, codelementodespesa::varchar as codelementodespesa, codunidadegestora::varchar as codunidadegestora, SUM(vlrpagamento) AS vlrempenho, COALESCE(null, 0) as vlrliquido, COALESCE(null, 0) as vlrpagamento, COALESCE(null, 0) as vlrpagamentorp, 'vportal_notaempenho_gastosdiretos' as tbvportal, 'notaEmpenho' as tipo"
            );

            $subquery2 = $basequery(
                'vportal_liquidacaodespesa_gastosdiretos',
                "codcredor::varchar as codcredor, codgrupodespesa::varchar as codgrupodespesa, codelementodespesa::varchar as codelementodespesa, codunidadegestora::varchar as codunidadegestora, COALESCE(null, 0) as vlrempenho, SUM(vlrpagamento) AS vlrliquido, COALESCE(null, 0) as vlrpagamento, COALESCE(null, 0) as vlrpagamentorp, 'vportal_liquidacaodespesa_gastosdiretos' as tbvportal, 'liquidacao' as tipo"
            );

            $subquery3 = $basequery(
                'vportal_notapagamento_gastosdiretos',
                "codcredor::varchar as codcredor, codgrupodespesa::varchar as codgrupodespesa, codelementodespesa::varchar as codelementodespesa, codunidadegestora::varchar as codunidadegestora, COALESCE(null, 0) AS vlrempenho, COALESCE(null, 0) AS vlrliquido, SUM(vlrpagamento) AS vlrpagamento, SUM(vlrpagamentorp) AS vlrpagamentorp, 'vportal_notapagamento_gastosdiretos' as tbvportal, CASE WHEN SUM(vlrpagamento) > 0 THEN 'notaPagamento' WHEN SUM(vlrpagamentorp) > 0 THEN 'valorPagar' END as tipo"
            );

            // União das subqueries
            $favorecidos = DB::connection('pgsql_siaf')
                ->table(DB::raw("({$subquery1->toSql()} UNION ALL {$subquery2->toSql()} UNION ALL {$subquery3->toSql()}) AS resultado"))
                ->setBindings(array_merge(
                    $subquery1->getBindings(),
                    $subquery2->getBindings(),
                    $subquery3->getBindings()
                ))
                ->select(
                    'resultado.codcredor', 'resultado.codunidadegestora', 'resultado.codelementodespesa', 'resultado.txtnomecredor', 'resultado.txtdescricaounidade', 'resultado.txtdescricaogrupodespesa', 'resultado.txtdescricaoelementodespesa',
                    DB::raw("STRING_AGG(CONCAT(CONCAT(resultado.codgrupodespesa, '/', resultado.tbvportal), '/', resultado.tipo), ',')  AS result"),
                    DB::raw('COALESCE(SUM(resultado.vlrempenho), 0) AS vlrempenho'),
                    DB::raw('COALESCE(SUM(resultado.vlrliquido), 0) AS vlrliquido'),
                    DB::raw('COALESCE(SUM(resultado.vlrpagamento), 0) AS vlrpagamento'),
                    DB::raw('COALESCE(SUM(resultado.vlrpagamentorp), 0) AS vlrpagamentorp')
                )
                ->groupBy('resultado.codcredor', 'resultado.codunidadegestora', 'resultado.codelementodespesa', 'resultado.txtnomecredor', 'resultado.txtdescricaounidade', 'resultado.txtdescricaogrupodespesa', 'resultado.txtdescricaoelementodespesa')
                ->orderBy('resultado.txtnomecredor', 'ASC')
                ->orderBy('resultado.txtdescricaounidade', 'ASC')
                ->orderBy('resultado.txtdescricaogrupodespesa', 'ASC')
                ->orderBy('resultado.txtdescricaoelementodespesa', 'ASC')
                ->paginate(10);

            // Construção do Gráfico
            $grupodespesacharts = LiquidacaoGastosDiretos::select('txtdescricaogrupodespesa', 'codgrupodespesa', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
            ->where('mes', $posicao, $request->mes)
            ->where('numexerciciofinanc', $request->ano)
            ->groupBy('txtdescricaogrupodespesa', 'codgrupodespesa')
            ->orderBy('txtdescricaogrupodespesa')
            ->get();

            // Retornar as consultas para a página
            return view('despesas/gastos-diretos/favorecido.index', [
                'favorecidos' => $favorecidos,
                'mes' => $request->mes,
                'ano' => $request->ano,
                'fase' => null,
                'classificacao' => $request->classificacao,
                'posicao' => $posicao,
                'sumtotal' => $sumtotal,
                'grupodespesacharts' => $grupodespesacharts,
                'elementodespesacharts' => $grupodespesacharts,
                'busca' => $busca
            ]);
          
      }
       
      // ## fz Filtro Por Função
      else if ($request->classificacao == 'funcao') {
         $busca = new Buscar_por;
         $busca = $busca->generatebuscar_por([
            "None" => "",
            "txtdescricaofuncao" => "Função",
            "txtdescricaosubfuncao" => "Subfunção"
         ]);

         // Função para criar subconsultas
         $basequery = function($table, $selectRaw) use ($request, $posicao){

            if($table == 'vportal_notaempenho_gastosdiretos'){
               $modelo = NotaEmpenhoGastosDiretos::class;
            } elseif ($table == 'vportal_notapagamento_gastosdiretos'){
               $modelo = NotaPagamentoGastosDiretos::class;
            } else {
               $modelo = LiquidacaoGastosDiretos::class;
            }

             $subquery = $modelo::where('numexerciciofinanc', $request->ano)
                 ->select('txtdescricaofuncao', 'txtdescricaosubfuncao', 'txtdescricaoacao')
                 ->selectRaw("codacao::varchar as codacao, codsubfuncao::varchar as codsubfuncao")
                 ->selectRaw($selectRaw)
                 ->groupBy('codacao', 'codsubfuncao', 'txtdescricaofuncao', 'txtdescricaosubfuncao', 'txtdescricaoacao');

               if ($posicao === '=') {
                  $subquery->where('mes', '=', $request->mes);
               } else {
                  $subquery->where('mes', '<=', $request->mes);
               }

             return $subquery;
            };

            $subquery = $basequery(
               'vportal_notaempenho_gastosdiretos',
               "SUM(vlrpagamento) AS vlrempenho, COALESCE(null, 0) as vlrliquido, COALESCE(null, 0) as vlrpagamento, COALESCE(null, 0) as vlrpagamentorp, 'vportal_notaempenho_gastosdiretos' as tbvportal, 'notaEmpenho' as tipo"
            );

            $subquery2 = $basequery(
               'vportal_liquidacaodespesa_gastosdiretos',
               "COALESCE(null, 0) as vlrempenho, SUM(vlrpagamento) AS vlrliquido, COALESCE(null, 0) as vlrpagamento, COALESCE(null, 0) as vlrpagamentorp, 'vportal_liquidacaodespesa_gastosdiretos' as tbvportal, 'liquidacao' as tipo"
            );

            $subquery3 = $basequery(
               'vportal_notapagamento_gastosdiretos',
               "COALESCE(null, 0) AS vlrempenho, COALESCE(null, 0) AS vlrliquido, SUM(vlrpagamento) AS vlrpagamento, SUM(vlrpagamentorp) AS vlrpagamentorp, 'vportal_notapagamento_gastosdiretos' as tbvportal, case when SUM(vlrpagamento) > 0 then 'notaPagamento' when SUM(vlrpagamentorp)  > 0 then 'valorPagar' end as tipo"
            );

         $funcao = DB::connection('pgsql_siaf')
            ->table(DB::raw("({$subquery->toSql()} UNION ALL {$subquery2->toSql()} UNION ALL {$subquery3->toSql()}) AS resultado"))
            ->setBindings(array_merge(
               $subquery->getBindings(),
               $subquery2->getBindings(),
               $subquery3->getBindings()
           ))
            ->select(
               'resultado.codacao',
               'resultado.codsubfuncao',
               'resultado.txtdescricaofuncao',
               'resultado.txtdescricaosubfuncao',
               'resultado.txtdescricaoacao',
               DB::raw("STRING_AGG(CONCAT(resultado.tbvportal, '/', resultado.tipo), ',')  AS result"), 
               DB::raw('COALESCE(SUM(resultado.vlrempenho), 0) AS vlrempenho'), 
               DB::raw('COALESCE(SUM(resultado.vlrliquido), 0) AS vlrliquido'), 
               DB::raw('COALESCE(SUM(resultado.vlrpagamento), 0) AS vlrpagamento'), 
               DB::raw('COALESCE(SUM(resultado.vlrpagamentorp), 0) AS vlrpagamentorp')
            )
            ->groupBy(
               'resultado.codacao',
               'resultado.codsubfuncao',
               'resultado.txtdescricaofuncao',
               'resultado.txtdescricaosubfuncao',
               'resultado.txtdescricaoacao'
            )
            ->orderBy('resultado.txtdescricaofuncao', 'ASC')
            ->orderBy('resultado.txtdescricaosubfuncao', 'ASC')
            ->orderBy('resultado.txtdescricaoacao', 'ASC')
            ->paginate(10);

          // Construção do Gráfico
         $grupofuncaocharts = LiquidacaoGastosDiretos::select('txtdescricaofuncao', 'codgrupodespesa', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
         ->where('mes', $posicao, $request->mes)
         ->where('numexerciciofinanc', $request->ano)
         ->groupBy('txtdescricaofuncao', 'codgrupodespesa')
         ->orderBy('txtdescricaofuncao')
         ->get();

         // Retornar as consultas para a página
         return view('despesas/gastos-diretos/funcao.index', [
            'funcoes' => $funcao,
            'fase' => null,
            'mes' => $request->mes,
            'ano' => $request->ano,
            'classificacao' => $request->classificacao,
            'posicao' => $posicao,
            'sumtotal' => $sumtotal,
            'grupofuncaocharts' => $grupofuncaocharts,
            'elementofuncaocharts' => $grupofuncaocharts,
            'busca' => $busca
         ]);
      }
       
   }
    
   // Função para as consultas de detalhamento dos Documentos da página gastos-diretos-detalhada
   public function detalhamento($request){
       
      $infos = (object) FacadesCrypt::decrypt($request);
       
      // Nome de qual fase de gastos a ser consultada
      if ($infos->fase == 'vportal_notaempenho_gastosdiretos') {
         $nomefase = 'Empenho';
         $model = NotaEmpenhoGastosDiretos::class;
      } else if ($infos->fase == 'vportal_liquidacaodespesa_gastosdiretos') {
         $nomefase = 'Liquidação';
         $model = LiquidacaoGastosDiretos::class;
      } else {
         // $fase == 'vportal_notapagamento_gastosdiretos'
         $nomefase = 'Pagamento';
         $model = NotaPagamentoGastosDiretos::class;
      }

      // Valor total dos gastos
      $sumtotal = $model::where('mes', $infos->posicao, $infos->mes)->where('numexerciciofinanc', $infos->ano)->sum('vlrpagamento');

      // Filtro Tipo de Despesa
      if ($infos->classificacao == 'despesa') {
          
         // Níveis de Navegação em Tipo de Despesa
         $nomedespesa = $model::select('txtdescricaoelementodespesa')->where('codelementodespesa', $infos->codelementodespesa)->limit(1)->get();
         $sumdespesa = $model::where('codelementodespesa', $infos->codelementodespesa)->where('mes', $infos->posicao, $infos->mes)->where('numexerciciofinanc', $infos->ano)->sum('vlrpagamento');

         $nomegestora = $model::select('txtdescricaounidade')->where('codunidadegestora', $infos->codunidadegestora)->limit(1)->get();
         $sumgestora = $model::where('codunidadegestora', $infos->codunidadegestora)->where('codelementodespesa', $infos->codelementodespesa)->where('mes', $infos->posicao, $infos->mes)->where('numexerciciofinanc', $infos->ano)->sum('vlrpagamento');

         $nomegestao = $model::select('txtdescricaogestao')->where('codgestao', $infos->codgestao)->limit(1)->get();
         $sumgestao = $model::where('codgestao', $infos->codgestao)->where('codunidadegestora', $infos->codunidadegestora)->where('codgrupodespesa', $infos->codgrupodespesa)->where('codelementodespesa', $infos->codelementodespesa)->where('mes', $infos->posicao, $infos->mes)->where('numexerciciofinanc', $infos->ano)->sum('vlrpagamento');

         // Condição para qual tabela a ser consultada -> tabela Pagamentos terá diferenças em relação a Empenho e Liquidação
         if ($infos->fase == 'vportal_notapagamento_gastosdiretos') {
             
            $favorecidos = $model::select('codcredor', 'txtnomecredor', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
               ->where('codgrupodespesa', $infos->codgrupodespesa)
               ->where('codelementodespesa', $infos->codelementodespesa)
               ->where('codunidadegestora', $infos->codunidadegestora)
               ->where('codgestao', $infos->codgestao)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('codcredor', 'txtnomecredor')
               ->get();

            $documentos = $model::select('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
               ->where('codgrupodespesa', $infos->codgrupodespesa)
               ->where('codelementodespesa', $infos->codelementodespesa)
               ->where('codunidadegestora', $infos->codunidadegestora)
               ->where('codgestao', $infos->codgestao)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora')
               ->get();
         } else {
            // Empenho e Liquidação
            $favorecidos = $model::select('codcredor', 'txtnomecredor', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
               ->where('codgrupodespesa', $infos->codgrupodespesa)
               ->where('codelementodespesa', $infos->codelementodespesa)
               ->where('codunidadegestora', $infos->codunidadegestora)
               ->where('codgestao', $infos->codgestao)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('codcredor', 'txtnomecredor')
               ->get();

            $documentos = $model::select('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
               ->where('codgrupodespesa', $infos->codgrupodespesa)
               ->where('codelementodespesa', $infos->codelementodespesa)
               ->where('codunidadegestora', $infos->codunidadegestora)
               ->where('codgestao', $infos->codgestao)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora')
               ->get();
         }
         // Retornar as consultas para a página
         return view('despesas/gastos-diretos/tipo-despesa.gastos-diretos-detalhada', [
            'favorecidos' => $favorecidos,
            'documentos' => $documentos,
            'fase' => $infos->fase,
            'nomefase' => $nomefase,
            'posicao' => $infos->posicao,
            'mes' => $infos->mes,
            'ano' => $infos->ano,
            'sumtotal' => $sumtotal,
            'nomedespesa' => $nomedespesa[0],
            'sumdespesa' => $sumdespesa,
            'nomegestora' => $nomegestora[0],
            'sumgestora' => $sumgestora,
            'nomegestao' => $nomegestao[0],
            'sumgestao' => $sumgestao
         ]);
      }
       
      // Filtro Por Unidade Gestora
      else if ($infos->classificacao ==  'gestora') {
         // Níveis de Navegação em Unidade Gestora
         $nomegestora = $model::select('txtdescricaounidade')
            ->where('codunidadegestora', $infos->codunidadegestora)
            ->limit(1)
            ->get();
         $sumgestora = $model::where('codunidadegestora', $infos->codunidadegestora)
            ->where('mes', $infos->posicao, $infos->mes)
            ->where('numexerciciofinanc', $infos->ano)
            ->sum('vlrpagamento');

         $nomegestao = $model::select('txtdescricaogestao')
            ->where('codgestao', $infos->codgestao)
            ->limit(1)
            ->get();
         $sumgestao = $model::where('codgestao', $infos->codgestao)
            ->where('codunidadegestora', $infos->codunidadegestora)
            ->where('mes', $infos->posicao, $infos->mes)
            ->where('numexerciciofinanc', $infos->ano)
            ->sum('vlrpagamento');

         $nomefavorecido = $model::select('txtnomecredor')
            ->where('codcredor', $infos->codcredor)
            ->limit(1)
            ->get();
         $sumfavorecido = $model::where('codcredor', $infos->codcredor)
            ->where('codunidadegestora', $infos->codunidadegestora)
            ->where('codgestao', $infos->codgestao)
            ->where('mes', $infos->posicao, $infos->mes)
            ->where('numexerciciofinanc', $infos->ano)
            ->sum('vlrpagamento');

         // Condição para qual tabela a ser consultada -> tabela Pagamentos terá diferenças em relação a Empenho e Liquidação
         if ($infos->fase == 'vportal_notapagamento_gastosdiretos') {
            $grupodespesa = $model::select('txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'codgrupodespesa', 'codelementodespesa', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
               ->where('codunidadegestora', $infos->codunidadegestora)
               ->where('codgestao', $infos->codgestao)
               ->where('codcredor', $infos->codcredor)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'codgrupodespesa', 'codelementodespesa')
               ->get();

            $documentos = $model::select('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', 'codgrupodespesa', 'codelementodespesa', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
               ->where('codunidadegestora', $infos->codunidadegestora)
               ->where('codgestao', $infos->codgestao)
               ->where('codcredor', $infos->codcredor)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', 'codgrupodespesa', 'codelementodespesa')
               ->get();
         } else {
            // Empenho e Liquidação
            $grupodespesa = $model::select('txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'codgrupodespesa', 'codelementodespesa', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
               ->where('codunidadegestora', $infos->codunidadegestora)
               ->where('codgestao', $infos->codgestao)
               ->where('codcredor', $infos->codcredor)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('txtdescricaogrupodespesa', 'txtdescricaoelementodespesa', 'codgrupodespesa', 'codelementodespesa')
               ->get();

            $documentos = $model::select('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', 'codgrupodespesa', 'codelementodespesa', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
               ->where('codunidadegestora', $infos->codunidadegestora)
               ->where('codgestao', $infos->codgestao)
               ->where('codcredor', $infos->codcredor)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', 'codgrupodespesa', 'codelementodespesa')
               ->get();
         }
         // Retornar as consultas para a página
         return view('despesas/gastos-diretos/unidade-gestora.gastos-diretos-detalhada', [
            'grupodespesa' => $grupodespesa,
            'documentos' => $documentos,
            'fase' => $infos->fase,
            'nomefase' => $nomefase,
            'posicao' => $infos->posicao,
            'mes' => $infos->mes,
            'ano' => $infos->ano,
            'sumtotal' => $sumtotal,
            'nomegestora' => $nomegestora[0],
            'sumgestora' => $sumgestora,
            'nomegestao' => $nomegestao[0],
            'sumgestao' => $sumgestao,
            'nomefavorecido' => $nomefavorecido[0],
            'sumfavorecido' => $sumfavorecido,
         ]);
      }

      // Filtro Por Ação
      else if ($infos->classificacao == 'acao') {
         // Níveis de Navegação em Ação
         $nomeacao = $model::select('txtdescricaoacao')
            ->where('codacao', $infos->codacao)
            ->limit(1)
            ->get();
         $sumacao = $model::where('codacao', $infos->codacao)
            ->where('mes', $infos->posicao, $infos->mes)
            ->where('numexerciciofinanc', $infos->ano)
            ->sum('vlrpagamento');                       

         // SUM de Transferencia de Recursos
         $sumtr = $model::where('mes', $infos->posicao, $infos->mes)
            ->where('numexerciciofinanc', $infos->ano)
            ->sum('vlrpagamento');

         // Condição para qual tabela a ser consultada -> tabela Pagamentos terá diferenças em relação a Empenho e Liquidação
         if ($infos->fase == 'vportal_notapagamento_gastosdiretos') {
            $favorecidos = $model::select('codcredor', 'txtnomecredor', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
               ->where('codacao', $infos->codacao)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('codcredor', 'txtnomecredor')
               ->get();

            $documentos = $model::select('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', 'codgrupodespesa', 'codelementodespesa', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
               ->where('codacao', $infos->codacao)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', 'codgrupodespesa', 'codelementodespesa')
               ->get();
         } else {
            // Empenho e Liquidação
            $favorecidos = $model::select('codcredor', 'txtnomecredor', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
               ->where('codacao', $infos->codacao)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('codcredor', 'txtnomecredor')
               ->get();

            $documentos = $model::select('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', 'codgrupodespesa', 'codelementodespesa', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
               ->where('codacao', $infos->codacao)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', 'codgrupodespesa', 'codelementodespesa')
               ->get();
         }
         // Retornar as consultas para a página
         return view('despesas/gastos-diretos/acao.gastos-diretos-detalhada', [
            'favorecidos' => $favorecidos,
            'documentos' => $documentos,
            'fase' => $infos->fase,
            'nomefase' => $nomefase,
            'posicao' => $infos->posicao,
            'mes' => $infos->mes,
            'ano' => $infos->ano,
            'sumtotal' => $sumtotal,
            'nomeacao' => $nomeacao[0],
            'sumacao' => $sumacao,
            'sumtr' => $sumtr
         ]);
      }

      // Filtro Por Fonte
      else if ($infos->classificacao == 'fonte') {
         // Níveis de Navegação por Fonte
         $nomefonte = $model::select('txtdescricaofonterecurso')
            ->where('codfonterecurso', $infos->codfonterecurso)
            ->limit(1)
            ->get();
         $sumfonte = $model::where('codfonterecurso', $infos->codfonterecurso)
            ->where('mes', $infos->posicao, $infos->mes)
            ->where('numexerciciofinanc', $infos->ano)
            ->sum('vlrpagamento');

         $nomegestora = $model::select('txtdescricaounidade')
            ->where('codunidadegestora', $infos->codunidadegestora)
            ->limit(1)
            ->get();
         $sumgestora = $model::where('codunidadegestora', $infos->codunidadegestora)
            ->where('codfonterecurso', $infos->codfonterecurso)
            ->where('mes', $infos->posicao, $infos->mes)
            ->where('numexerciciofinanc', $infos->ano)
            ->sum('vlrpagamento');

         $nomegestao = $model::select('txtdescricaogestao')
            ->where('codgestao', $infos->codgestao)
            ->limit(1)
            ->get();
         $sumgestao = $model::where('codgestao', $infos->codgestao)
            ->where('codunidadegestora', $infos->codunidadegestora)
            ->where('codfonterecurso', $infos->codfonterecurso)
            ->where('mes', $infos->posicao, $infos->mes)
            ->where('numexerciciofinanc', $infos->ano)
            ->sum('vlrpagamento');

         // Condição para qual tabela a ser consultada -> tabela Pagamentos terá diferenças em relação a Empenho e Liquidação
         if ($infos->fase == 'vportal_notapagamento_gastosdiretos') {
            $favorecidos = $model::select('codcredor', 'txtnomecredor', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
               ->where('codfonterecurso', $infos->codfonterecurso)
               ->where('codunidadegestora', $infos->codunidadegestora)
               ->where('codgestao', $infos->codgestao)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('codcredor', 'txtnomecredor')
               ->get();

            $documentos = $model::select('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
               ->where('codfonterecurso', $infos->codfonterecurso)
               ->where('codunidadegestora', $infos->codunidadegestora)
               ->where('codgestao', $infos->codgestao)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora')
               ->get();
         } else {
            // Empenho e Liquidação
            $favorecidos = $model::select('codcredor', 'txtnomecredor', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
               ->where('codfonterecurso', $infos->codfonterecurso)
               ->where('codunidadegestora', $infos->codunidadegestora)
               ->where('codgestao', $infos->codgestao)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('codcredor', 'txtnomecredor')
               ->get();

            $documentos = $model::select('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
               ->where('codfonterecurso', $infos->codfonterecurso)
               ->where('codunidadegestora', $infos->codunidadegestora)
               ->where('codgestao', $infos->codgestao)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora')
               ->get();
         }
         // Retornar as consultas para a página
         return view('despesas/gastos-diretos/fonte.gastos-diretos-detalhada', [
            'favorecidos' => $favorecidos,
            'documentos' => $documentos,
            'fase' => $infos->fase,
            'nomefase' => $nomefase,
            'posicao' => $infos->posicao,
            'mes' => $infos->mes,
            'ano' => $infos->ano,
            'sumtotal' => $sumtotal,
            'nomefonte' => $nomefonte[0],
            'sumfonte' => $sumfonte,
            'nomegestora' => $nomegestora[0],
            'sumgestora' => $sumgestora,
            'nomegestao' => $nomegestao[0],
            'sumgestao' => $sumgestao
         ]);
      }

      // Filtro Por Favorecido
      else if ($infos->classificacao == 'favorecido') {
         // Níveis de Navegação por Favorecido
         $nomefavorecido = $model::select('txtnomecredor')
            ->where('codcredor', $infos->codcredor)
            ->limit(1)
            ->get();
         $sumfavorecido = $model::where('codcredor', $infos->codcredor)
            ->where('mes', $infos->posicao, $infos->mes)
            ->where('numexerciciofinanc', $infos->ano)
            ->sum('vlrpagamento');

         $nomedespesa = $model::select('txtdescricaoelementodespesa')
            ->where('codelementodespesa', $infos->codelementodespesa)
            ->limit(1)
            ->get();
         $sumdespesa = $model::where('codelementodespesa', $infos->codelementodespesa)
            ->where('codcredor', $infos->codcredor)
            ->where('mes', $infos->posicao, $infos->mes)
            ->where('numexerciciofinanc', $infos->ano)
            ->sum('vlrpagamento');

         $nomegestora = $model::select('txtdescricaounidade')
            ->where('codunidadegestora', $infos->codunidadegestora)
            ->limit(1)
            ->get();
         $sumgestora = $model::where('codunidadegestora', $infos->codunidadegestora)
            ->where('codcredor', $infos->codcredor)
            ->where('codelementodespesa', $infos->codelementodespesa)
            ->where('mes', $infos->posicao, $infos->mes)
            ->where('numexerciciofinanc', $infos->ano)
            ->sum('vlrpagamento');

         // Condição para qual tabela a ser consultada -> tabela Pagamentos terá diferenças em relação a Empenho e Liquidação
         if ($infos->fase == 'vportal_notapagamento_gastosdiretos') {
            $gestao = $model::select('codgestao', 'txtdescricaogestao', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
               ->where('codgrupodespesa', $infos->codgrupodespesa)
               ->where('codelementodespesa', $infos->codelementodespesa)
               ->where('codunidadegestora', $infos->codunidadegestora)
               ->where('codcredor', $infos->codcredor)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('codgestao', 'txtdescricaogestao')
               ->get();

            $documentos = $model::select('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', 'codgestao', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
               ->where('codgrupodespesa', $infos->codgrupodespesa)
               ->where('codelementodespesa', $infos->codelementodespesa)
               ->where('codunidadegestora', $infos->codunidadegestora)
               ->where('codcredor', $infos->codcredor)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', 'codgestao')
               ->get();
         } else {
            // Empenho e Liquidação
            $gestao = $model::select('codgestao', 'txtdescricaogestao', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
               ->where('codgrupodespesa', $infos->codgrupodespesa)
               ->where('codelementodespesa', $infos->codelementodespesa)
               ->where('codunidadegestora', $infos->codunidadegestora)
               ->where('codcredor', $infos->codcredor)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('codgestao', 'txtdescricaogestao')
               ->get();

            $documentos = $model::select('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', 'codgestao', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
               ->where('codgrupodespesa', $infos->codgrupodespesa)
               ->where('codelementodespesa', $infos->codelementodespesa)
               ->where('codunidadegestora', $infos->codunidadegestora)
               ->where('codcredor', $infos->codcredor)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', 'codgestao')
               ->get();
         }
         // Retornar as consultas para a página
         return view('despesas/gastos-diretos/favorecido.gastos-diretos-detalhada', [
            'gestao' => $gestao,
            'documentos' => $documentos,
            'fase' => $infos->fase,
            'nomefase' => $nomefase,
            'posicao' => $infos->posicao,
            'mes' => $infos->mes,
            'ano' => $infos->ano,
            'sumtotal' => $sumtotal,
            'nomefavorecido' => $nomefavorecido[0],
            'sumfavorecido' => $sumfavorecido,
            'nomedespesa' => $nomedespesa[0],
            'sumdespesa' => $sumdespesa,
            'nomegestora' => $nomegestora[0],
            'sumgestora' => $sumgestora
         ]);
      }

      // Filtro Por Função
      else if ($infos->classificacao == 'funcao') {
         // Níveis de Navegação por Função
         $nomefuncao = $model::select('txtdescricaofuncao')
            ->where('txtdescricaofuncao', $infos->txtdescricaofuncao)
            ->limit(1)
            ->get();
         $sumfuncao = $model::where('txtdescricaofuncao', $infos->txtdescricaofuncao)
            ->where('mes', $infos->posicao, $infos->mes)
            ->where('numexerciciofinanc', $infos->ano)
            ->sum('vlrpagamento');

         $nomesubfuncao = $model::select('txtdescricaosubfuncao')
            ->where('codsubfuncao', $infos->codsubfuncao)
            ->limit(1)
            ->get();
         $sumsubfuncao = $model::where('codsubfuncao', $infos->codsubfuncao)
            ->where('txtdescricaofuncao', $infos->txtdescricaofuncao)
            ->where('mes', $infos->posicao, $infos->mes)
            ->where('numexerciciofinanc', $infos->ano)
            ->sum('vlrpagamento');

         $nomeacao = $model::select('txtdescricaoacao')
            ->where('txtdescricaoacao', $infos->txtdescricaoacao)
            ->limit(1)
            ->get();
         $sumacao = $model::where('codacao', $infos->codacao)
            ->where('txtdescricaofuncao', $infos->txtdescricaofuncao)
            ->where('codsubfuncao', $infos->codsubfuncao)
            ->where('mes', $infos->posicao, $infos->mes)
            ->where('numexerciciofinanc', $infos->ano)
            ->sum('vlrpagamento');

         if ($infos->fase == 'vportal_notapagamento_gastosdiretos') {
            // Condição para qual tabela a ser consultada -> tabela Pagamentos terá diferenças em relação a Empenho e Liquidação
            $favorecidos = $model::select('codcredor', 'txtnomecredor', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
               ->where('txtdescricaofuncao', $infos->txtdescricaofuncao)
               ->where('codsubfuncao', $infos->codsubfuncao)
               ->where('codacao', $infos->codacao)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('codcredor', 'txtnomecredor')
               ->get();

            $documentos = $model::select('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
               ->where('txtdescricaofuncao', $infos->txtdescricaofuncao)
               ->where('codsubfuncao', $infos->codsubfuncao)
               ->where('codacao', $infos->codacao)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora')
               ->get();
         } else {
            // Empenho e Liquidação
            $favorecidos = $model::select('codcredor', 'txtnomecredor', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
               ->where('txtdescricaofuncao', $infos->txtdescricaofuncao)
               ->where('codsubfuncao', $infos->codsubfuncao)
               ->where('codacao', $infos->codacao)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('codcredor', 'txtnomecredor')
               ->get();

            $documentos = $model::select('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
               ->where('txtdescricaofuncao', $infos->txtdescricaofuncao)
               ->where('codsubfuncao', $infos->codsubfuncao)
               ->where('codacao', $infos->codacao)
               ->where('mes', $infos->posicao, $infos->mes)
               ->where('numexerciciofinanc', $infos->ano)
               ->groupBy('numdocumento', 'dtemissao', 'txtdescricaoprograma', 'txtdescricaoacao', 'codcredor', 'codunidadegestora')
               ->get();
         }
         // Retornar as consultas para a página
         return view('despesas/gastos-diretos/funcao.gastos-diretos-detalhada', [
            'favorecidos' => $favorecidos,
            'documentos' => $documentos,
            'fase' => $infos->fase,
            'nomefase' => $nomefase,
            'posicao' => $infos->posicao,
            'mes' => $infos->mes,
            'ano' => $infos->ano,
            'sumtotal' => $sumtotal,
            'nomefuncao' => $nomefuncao[0],
            'sumfuncao' => $sumfuncao,
            'nomesubfuncao' => $nomesubfuncao[0],
            'sumsubfuncao' => $sumsubfuncao,
            'nomeacao' => $nomeacao[0],
            'sumacao' => $sumacao
         ]);
      }
   }
    
   // Função para a ultima página de detalhamento
   public function detalhes($request)
   {
      // Condição para qual tabela a ser consultada -> tabela Pagamentos terá diferenças em relação a Empenho
      $infos = (object) FacadesCrypt::decrypt($request);
       
      if ($infos->fase == 'vportal_notaempenho_gastosdiretos') {
          
         $table = 'vw_vportalnotaempenho';
         $documento = 'numempenho';
         $nomefase = 'Empenho';

         $detalhesdocumento = DB::connection('pgsql_siaf')->table($table)
            ->where($documento, $infos->numdocumento)
            ->where('codcredor', $infos->codcredor)
            ->where('txtdescricaoprograma', $infos->txtdescricaoprograma)
            ->where('txtdescricaoacao', $infos->txtdescricaoacao)
            ->where('codunidadegestora', $infos->codunidadegestora)
            ->get();

         // Retornar as consultas para a página
         return view('despesas/gastos-diretos.detalhes-documento', [ 'detalhesdocumento' => $detalhesdocumento, 'fase' => $infos->fase, 'nomefase' => $nomefase ]);
      }
      // Liquidação
      else if ($infos->fase == 'vportal_liquidacaodespesa_gastosdiretos') {
         $table = 'vw_portalnotalancamento';
         $documento = 'numnotalancamento';
         $nomefase = 'Liquidação';

         $detalhesdocumento = DB::connection('pgsql_siaf')->table($table)
            ->where($documento, $infos->numdocumento)
            ->where('codcredor', $infos->codcredor)
            ->where('txtdescricaoprograma', $infos->txtdescricaoprograma)
            ->where('txtdescricaoacao', $infos->txtdescricaoacao)
            ->where('codunidadegestora', $infos->codunidadegestora)
            ->get();
         
         // Retornar as consultas para a página
         return view('despesas/gastos-diretos.detalhes-documento', [
            'detalhesdocumento' => $detalhesdocumento,
            'fase' => $infos->fase,
            'nomefase' => $nomefase
         ]);
      }
      // Pagamentos -> $fase == 'vportal_notapagamento_gastosdiretos'
      else {
         $table = 'vportalordembancaria';
         $documento = 'preparacaopagamento';
         $nomefase = 'Pagamento';

         // restos a pagar será a tabela vportalordembancariarp
         $detalhesrestoapagar = DB::connection('pgsql_siaf')->table($table)
            ->where($documento, $infos->numdocumento)
            ->where('codcredor', $infos->codcredor)
            ->where('txtdescricaoprograma', $infos->txtdescricaoprograma)
            ->where('txtdescricaoacao', $infos->txtdescricaoacao)
            ->where('codunidadegestora', $infos->codunidadegestora)
            ->get();

         $detalhesdocumento = DB::connection('pgsql_siaf')->table($table)
            ->where($documento, $infos->numdocumento)
            ->where('codcredor', $infos->codcredor)
            ->where('txtdescricaoprograma', $infos->txtdescricaoprograma)
            ->where('txtdescricaoacao', $infos->txtdescricaoacao)
            ->where('codunidadegestora', $infos->codunidadegestora)
            ->get();

         // Retornar as consultas para a página
         return view('despesas/gastos-diretos.detalhes-documento', [
            'detalhesdocumento' => $detalhesdocumento,
            'detalhesrestoapagar' => $detalhesrestoapagar,
            'fase' => $infos->fase,
            'nomefase' => $nomefase
         ]);
      }
   }
    
   // Função para Fazer Download das consultas para planilha excel
   public function exportxls($mes, $ano, $posicao, $classificacao, $fase = null)
   {
      return Excel::download(new GastosDiretosExport($mes, $ano, $posicao, $classificacao, $fase), 'gastos-diretos.xlsx');
   }
    
   // Função para Fazer Download das consultas para PDF
   public function exportpdf($mes, $ano, $posicao, $classificacao, $fase = null)
   {
      return Excel::download(new GastosDiretosExport($mes, $ano, $posicao, $classificacao, $fase), 'gastos-diretos.pdf');
   }
    
    
}