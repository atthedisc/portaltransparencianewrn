<?php

namespace App\Http\Controllers\Despesas;

use App\Exports\Despesas\BloqueiosJudiciaisExport;
use App\Http\Controllers\Controller;
use App\Models\BloqueiosJudiciais;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class BloqueiosJudiciaisController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $meses = collect([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]);
        $anos = collect([2024, 2023, 2022, 2021, 2020, 2019]);
        $model = BloqueiosJudiciais::class;

        $dados = $model::where('numexerciciofinanc', $request->ano)
            ->where('mes', '<=', $request->mes)
            ->orderBy('mes')
            ->get();

        return view('despesas/bloqueios-judiciais.index', [
            'dados' => $dados,
            'meses' => $meses,
            'anos' => $anos,
            'ano_selected' => $request->ano,
            'mes_selected' => $request->mes,
        ]);
    }

    public function exportxls($mes, $ano)
    {
        return Excel::download(new BloqueiosJudiciaisExport($mes, $ano), 'bloqueios-judiciais.xlsx');
    }
    public function exportpdf($mes, $ano)
    {
        return Excel::download(new BloqueiosJudiciaisExport($mes, $ano), 'bloqueios-judiciais.pdf');
    }
}
