<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class WelcomeController extends Controller
{
   public function index(Request $request)
   {
      $ano = Carbon::now()->format('Y');
      //$ano = 2023;

      $educacaogastos = Cache::remember('educacaogastos', 3600, function () use ($ano) {
         return DB::connection('pgsql_siaf')->table('vportal_notapagamento_gastosdiretos')
            ->select('vlrpagamento')
            ->where('numexerciciofinanc', $ano)
            ->where('txtdescricaofuncao', 'Educação')
            ->get();
      });

      $saudegastos = Cache::remember('saudegastos', 3600, function () use ($ano)  {
         return DB::connection('pgsql_siaf')->table('vportal_notapagamento_gastosdiretos')
            ->select('vlrpagamento')
            ->where('numexerciciofinanc', $ano)
            ->where('txtdescricaofuncao', 'Saúde')
            ->get();
      });

      $segurancagastos = Cache::remember('segurancagastos', 3600, function () use ($ano) {
         return DB::connection('pgsql_siaf')->table('vportal_notapagamento_gastosdiretos')
            ->select('vlrpagamento')
            ->where('numexerciciofinanc', $ano)
            ->where('txtdescricaofuncao', 'Segurança Pública')
            ->get();
      });

      $transportegastos = Cache::remember('transportegastos', 3600, function () use ($ano) {
         return DB::connection('pgsql_siaf')->table('vportal_notapagamento_gastosdiretos')
            ->select('vlrpagamento')
            ->where('numexerciciofinanc', $ano)
            ->where('txtdescricaofuncao', 'Transporte')
            ->get();
      });

      return view('welcome', [
         'educacaogastos' => $educacaogastos,
         'saudegastos' => $saudegastos,
         'segurancagastos' => $segurancagastos,
         'transportegastos' => $transportegastos,
         'ano' => $ano

      ]);
   }

   public function gastos(Request $request)
   {
      $href = "gastos-diretos/" . $request->nome . "/" . $request->ano . "";

      $output = '<div id="cartao" class="hidden relative bg-white border border-' . $request->color . '-500 py-6 px-6 rounded-xl w-full my-4 shadow-xl">
       <div name="cardname" class="text-' . $request->color . '-500 flex items-center justify-center zoomttg">
          ' . $request->icon . '<span class="text-center text-base"> ' . $request->nome . '</span> </i>
       </div>

       <div>

          <div class="text-gray-400 text-sm my-3 text-center" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
             <i class="far fa-clock"> <span>' . $request->ano . '</span> </i>
          </div>

          <div class="border-t-2 "></div>

          <div class="my-4">
             <p class="text-xl text-center" name="texto" style="font-size: 1.25rem; line-height: 1.75rem;">R$ <span id="' . $request->nome . '"></span></p>
          </div>

          <div class="flex space-x-4">
             <a name="card" href="' . $href . '"
                   class="w-full px-4 py-1.5 text-center text-white bg-' . $request->color . '-500 rounded-lg hover:bg-' . $request->color . '-700 cursor-pointer" name="texto" style="font-size: 1rem; line-height: 1.5rem;">Detalhar</a>
          </div>
          </div>
       </div>';
      return $output;
   }

   public function buscargasto(Request $request)
   {
      $output = Null;
      $dado = DB::connection('pgsql_siaf')->table('vportal_notapagamento_gastosdiretos')
         ->select('vlrpagamento')
         ->where('numexerciciofinanc', $request->ano)
         ->where('txtdescricaofuncao', $request->nome)
         ->get();


      $valor =  $dado->sum('vlrpagamento');

      if ($request->nome == 'Administração') {
         $output = [$valor, '<i class="fas fa-chart-pie fa-2x text-center">', 'indigo'];
      } elseif ($request->nome == 'Agricultura') {
         $output = [$valor, '<i class="fab fa-pagelines fa-2x text-center">', 'lime'];
      } elseif ($request->nome == 'Assistência Social') {
         $output = [$valor, '<i class="fas fa-user fa-2x text-center">', 'teal'];
      } elseif ($request->nome == 'Ciência e Tecnologia') {
         $output = [$valor, '<i class="fas fa-bong fa-2x text-center">', 'yellow'];
      } elseif ($request->nome == 'Comércio e Serviços') {
         $output = [$valor, '<i class="fas fa-shopping-cart fa-2x text-center">', 'green'];
      } elseif ($request->nome == 'Direitos da Cidadania') {
         $output = [$valor, '<i class="fas fa-fist-raised fa-2x text-center">', 'red'];
      } elseif ($request->nome == 'Educação') {
         $output = [$valor, '<i class="fa fa-book fa-2x text-center">', 'blue'];
      } elseif ($request->nome == 'Encargos Especiais') {
         $output = [$valor, '<i class="fas fa-user-tie fa-2x text-center">', 'yellow'];
      } elseif ($request->nome == 'Energia') {
         $output = [$valor, '<i class="fas fa-lightbulb fa-2x text-center">', 'pink'];
      } elseif ($request->nome == 'Gestão Ambiental') {
         $output = [$valor, '<i class="fas fa-leaf fa-2x text-center">', 'emerald'];
      } elseif ($request->nome == 'Indústria') {
         $output = [$valor, '<i class="fas fa-industry fa-2x text-center">', 'warmGray'];
      } elseif ($request->nome == 'Judiciária') {
         $output = [$valor, '<i class="fas fa-balance-scale fa-2x text-center">', 'indigo'];
      } elseif ($request->nome == 'Habitação') {
         $output = [$valor, '<i class="fas fa-home fa-2x text-center">', 'orange'];
      } elseif ($request->nome == 'Saúde') {
         $output = [$valor, '<i class="fas fa-heartbeat fa-2x text-center">', 'red'];
      } elseif ($request->nome == 'Segurança Pública') {
         $output = [$valor, '<i class="fa fa-lock fa-2x text-center">', 'blueGray'];
      } elseif ($request->nome == 'Organização Agrária') {
         $output = [$valor, '<i class="fas fa-seedling fa-2x text-center">', 'lime'];
      } elseif ($request->nome == 'Transporte') {
         $output = [$valor, '<i class="fas fa-bus fa-2x text-center">', 'amber'];
      }
      return $output;
   }
}
