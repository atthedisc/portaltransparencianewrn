<?php

namespace App\Http\Controllers;

use App\Components\Busca\Buscar_por;
use App\Components\Popover\Popover;
use App\Exports\Receita\ReceitasExport;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Receitas;

class ReceitasController extends Controller
{
   public function index()
   {

      $data = Carbon::now();
      $mescorrente = $data->month;

      $meses = collect([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]);

      return view('receitas.index', [
         'meses' => $meses,
         'mescorrente' => $mescorrente
      ]);
   }

   public function receitaPrevista(Request $request)
   {

      if(!$request->query() && !$request->input()) {
         return redirect()->route('receita');
      }
      if ($request->posicao == 'Acumulado') {
         $posicao = '<=';
      } else {
         $posicao = '=';
      }

      $model = Receitas::class;

      $previstoano = $model::select('vlrprevisaoatualizado')
         ->where('codcategoria', '!=', '7000000000')
         ->where('numexerciciofinanc', $request->ano)
         ->get();

      $brutorealizado = $model::select('vlrarrecadacao')
         ->where('mes', $posicao, $request->mes)
         ->where('codcategoria', '!=', '7000000000')
         ->where('numexerciciofinanc', $request->ano)
         ->get();

      // Por Receita
      if ($request->classificacao == 'receita') {
         $vlrliquido = $model::select('codcategoria', 'txtdescricaocategoria', DB::raw('SUM(valorreceita) as valorreceita'))
            ->where('mes', $posicao, $request->mes)
            ->where('codcategoria', '!=', '7000000000')
            ->where('numexerciciofinanc', $request->ano)
            ->groupBy('codcategoria', 'txtdescricaocategoria')
            ->get();

         $categorias =$model::select('codcategoria', 'txtdescricaocategoria', DB::raw('SUM(valorreceita) as valorreceita'))
            ->where('mes', $posicao, $request->mes)
            ->where('numexerciciofinanc', $request->ano)
            ->groupBy('codcategoria', 'txtdescricaocategoria')
            ->orderByRaw("CASE WHEN txtdescricaocategoria = 'Receitas Correntes' then 0 WHEN txtdescricaocategoria = 'Deduções da Receita' then 1 WHEN txtdescricaocategoria = 'Receitas de Capital' then 2 Else 3 END")
            ->paginate(10);

         // Detalhamento para Deduções da Receita
         $detalhamentoDR = $model::select('txtdescricaodeducaodetalhada', 'codcategoria', DB::raw('SUM(valorreceita) as valorreceita'))
         ->where('mes', $posicao, $request->mes)
         ->where('numexerciciofinanc', $request->ano)
         ->groupBy('txtdescricaodeducaodetalhada', 'codcategoria')
         ->get();

         $categoriaschart = $model::select('codcategoria', 'txtdescricaocategoria', DB::raw('SUM(valorreceita) as valorreceita'))
            ->where('mes', $posicao, $request->mes)
            ->where('numexerciciofinanc', $request->ano)
            ->groupBy('codcategoria', 'txtdescricaocategoria')
            ->orderByRaw("CASE WHEN txtdescricaocategoria = 'Receitas Correntes' then 0 WHEN txtdescricaocategoria = 'Deduções da Receita' then 1 WHEN txtdescricaocategoria = 'Receitas de Capital' then 2 Else 3 END")
            ->get();

         $origenschart = $model::select('codorigem', 'txtdescricaoorigem', 'codcategoria', DB::raw('SUM(valorreceita) as valorreceita'))
            ->where('mes', $posicao, $request->mes)
            ->where('numexerciciofinanc', $request->ano)
            ->groupBy('codorigem', 'txtdescricaoorigem', 'codcategoria')
            ->get();

         $busca = new Buscar_por;
         $busca = $busca->generatebuscar_por([
            "None" => "",
            "txtdescricaoorigem" => 'Origem',
            "txtdescricaoespecie" => 'Espécie',
            "txtdescricaorubrica" => 'Rubricas',
            "txtdescricaoalinea" => 'Alinea',
            "txtdescricaodetalhada" => 'Detalhamento',
            "txtdescricaodeducaodetalhada" => 'Deduções da Receita Corrente',

         ]);

         foreach ($categorias as $categoria) {
            $valorPrevisto = $model::select(DB::raw('SUM(vlrprevisaoatualizado) as valor'))
               ->where('codcategoria', $categoria->codcategoria)
               ->where('mes', '<=', $request->mes)
               ->where('numexerciciofinanc', $request->ano)
               ->first();

               if ($valorPrevisto->valor == 0) {
                  $categoria->porcentagem = 0;
                  $categoria->porcentagem_formatada = '0,0';
                  $categoria->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
               } else {
                  $categoria->porcentagem = (($categoria->valorreceita * 100) / number_format($valorPrevisto->valor, 2, '.', ''));
                  $categoria->porcentagem_formatada = number_format($categoria->porcentagem, 2, ',', '.');
                  $categoria->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
               }
         }


         return view('receitas/receita.index', [
            'vlrliquido' => $vlrliquido,
            'categorias' => $categorias,
            'detalhamentoDR' => $detalhamentoDR,
            'mes' => $request->mes,
            'ano' => $request->ano,
            'posicao' => $posicao,
            'classificacao' => $request->classificacao,
            'previstoano' => $previstoano,
            'brutorealizado' => $brutorealizado,
            'categoriaschart' => $categoriaschart,
            'origenschart' => $origenschart,
            'busca' => $busca
         ]);
      }
      // Por Unidade Gestora
      else if ($request->classificacao == 'gestora') {
         $vlrliquido = $model::select('codunidade', 'txtdescricaounidade', DB::raw('SUM(valorreceita) as valorreceita'))
            ->where('mes', $posicao, $request->mes)
            ->where('codcategoria', '!=', '7000000000')
            ->where('numexerciciofinanc', $request->ano)
            ->groupBy('codunidade', 'txtdescricaounidade')
            ->get();

         $unidadegestora = $model::select('codunidade', 'txtdescricaounidade', DB::raw('SUM(valorreceita) as valorreceita'))
            ->where('mes', $posicao, $request->mes)
            ->where('numexerciciofinanc', $request->ano)
            ->groupBy('codunidade', 'txtdescricaounidade')
            ->orderBy('txtdescricaounidade')
            ->paginate(10);

         $gestao = $model::select('codgestao', 'txtdescricaogestao', 'codunidade', DB::raw('SUM(valorreceita) as valorreceita'))
            ->where('mes', $posicao, $request->mes)
            ->where('numexerciciofinanc', $request->ano)
            ->groupBy('codgestao', 'txtdescricaogestao', 'codunidade')
            ->get();

         $categorias = $model::select('codcategoria', 'txtdescricaocategoria', 'codunidade', 'codgestao', DB::raw('SUM(valorreceita) as valorreceita'))
            ->where('mes', $posicao, $request->mes)
            ->where('numexerciciofinanc', $request->ano)
            ->groupBy('codcategoria', 'txtdescricaocategoria', 'codunidade', 'codgestao')
            ->get();

            $busca = new Buscar_por;
            $busca = $busca->generatebuscar_por([
               "None" => "",
               "txtdescricaounidade" => "Unidade gestora",
               "txtdescricaoorigem" => 'Origem'
            ]);

            foreach ($unidadegestora as $unidade) {
               $valorPrevisto = $model::select(DB::raw('SUM(vlrprevisaoatualizado) as valor'))
                  ->where('codunidade', $unidade->codunidade)
                  ->where('mes', $posicao, $request->mes)
                  ->where('numexerciciofinanc', $request->ano)
                  ->first();
   
                  if ($valorPrevisto->valor == 0) {
                     $unidade->porcentagem = 0;
                     $unidade->porcentagem_formatada = '0,0';
                     $unidade->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                  } else {
                     $unidade->porcentagem = (($unidade->valorreceita * 100) / number_format($valorPrevisto->valor, 2, '.', ''));
                     $unidade->porcentagem_formatada = number_format($unidade->porcentagem, 2, ',', '.');
                     $unidade->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                  }
            }
    
             return view('receitas/unidade-gestora.index', [
                'vlrliquido' => $vlrliquido,
                'unidadegestora' => $unidadegestora,
                'gestao' => $gestao,
                'categorias' => $categorias,
                'mes' => $request->mes,
                'ano' => $request->ano,
                'posicao' => $posicao,
                'classificacao' => $request->classificacao,
                'previstoano' => $previstoano,
                'brutorealizado' => $brutorealizado,
                'busca' => $busca
             ]);
      }
      // Por Fonte
      else if ($request->classificacao == 'fonterecurso') {
         $vlrliquido = $model::select('codfonterecurso', 'txtdescricaofonterecurso', DB::raw('SUM(valorreceita) as valorreceita'))
            ->where('mes', $posicao, $request->mes)
            ->where('codcategoria', '!=', '7000000000')
            ->where('numexerciciofinanc', $request->ano)
            ->groupBy('codfonterecurso', 'txtdescricaofonterecurso')
            ->get();

         $fontes = $model::select('codfonterecurso', 'txtdescricaofonterecurso', DB::raw('SUM(valorreceita) as valorreceita'))
            ->where('mes', $posicao, $request->mes)
            ->where('numexerciciofinanc', $request->ano)
            ->groupBy('codfonterecurso', 'txtdescricaofonterecurso')
            ->orderBy('txtdescricaofonterecurso')
            ->paginate(10);

         $categorias = $model::select('codcategoria', 'txtdescricaocategoria', 'codfonterecurso', DB::raw('SUM(valorreceita) as valorreceita'))
            ->where('mes', $posicao, $request->mes)
            ->where('numexerciciofinanc', $request->ano)
            ->groupBy('codcategoria', 'txtdescricaocategoria', 'codfonterecurso')
            ->get();

            $busca = new Buscar_por;
            $busca = $busca->generatebuscar_por([
               "None" => "",
               "txtdescricaofonterecurso" => "Fonte",
               "txtdescricaoorigem" => 'Origem'
            ]);   

            foreach ($fontes as $item) {
               $valorPrevisto = $model::select(DB::raw('SUM(vlrprevisaoatualizado) as valor'))
                  ->where('codfonterecurso', $item->codfonterecurso)
                  ->where('mes', $posicao, $request->mes)
                  ->where('numexerciciofinanc', $request->ano)
                  ->first();
   
                  if ($valorPrevisto->valor == 0) {
                     $item->porcentagem = 0;
                     $item->porcentagem_formatada = '0,0';
                     $item->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                  } else {
                     $item->porcentagem = (($item->valorreceita * 100) / number_format($valorPrevisto->valor, 2, '.', ''));
                     $item->porcentagem_formatada = number_format($item->porcentagem, 2, ',', '.');
                     $item->valor_previsto = number_format($valorPrevisto->valor, 2, ',', '.');
                  }
            }
   
            return view('receitas/fonte.index', [
               'vlrliquido' => $vlrliquido,
               'fontes' => $fontes,
               'categorias' => $categorias,
               'mes' => $request->mes,
               'ano' => $request->ano,
               'posicao' => $posicao,
               'classificacao' => $request->classificacao,
               'previstoano' => $previstoano,
               'brutorealizado' => $brutorealizado,
               'busca' => $busca
            ]);
      }
   }

   public function vlrprev(Request $request)
   {
      $model = Receitas::class;
      $where = '';
      foreach($request->info as $key => $cod)
      {
         $where.= '"'.$key.'" = '."'".urldecode($cod)."'".' and ';
      }
      $vlrprev = $model::select(DB::raw('SUM(vlrprevisaoatualizado) as valor'))
         ->whereRaw(substr($where, 0, -5))
         ->where('mes', '<=', $request->mes)
         ->where('numexerciciofinanc', $request->ano)
         ->get();
      return $vlrprev[0]->valor;
   }

   public function popOver(Request $request)
   {
     $popover = new Popover;
     $popover = $popover->generatepop((object) [
        "request" => $request
     ]);
     return $popover;
   }

   public function exportxls($mes, $ano, $posicao, $classificacao)
   {
      return Excel::download(new ReceitasExport($mes, $ano, $posicao, $classificacao), 'Receitas-' . $classificacao . '.xls');
   }
   public function exportpdf($mes, $ano, $posicao, $classificacao)
   {
      return Excel::download(new ReceitasExport($mes, $ano, $posicao, $classificacao), 'Receitas-' . $classificacao . '.pdf');
   }
}
