<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Components\Glossario\Cardglossario;

class GlossarioController extends Controller
{
  public function index(Request $request)
    {
        $tabela = DB::table('novoglossario')->orderBy('nome', 'asc')->paginate(50);
        if($request->ajax()){
         $output = '';
         $card = new Cardglossario;
            foreach ($tabela as $elemento){
               $result = $card->generatecard((object) [
                  "nome" => $elemento->nome,
                  "descricao" => $elemento->descricao,
               ]);
               $output .= $result;
            }
         return $output;
        } else
        {
         return view('glossario.index', [
            'tables' => $tabela
        ]);
        }
    }
   
  public function card(Request $request)
    {
       $output = '';
        $tabela = DB::table('novoglossario')
        ->where('letra',$request->letra)
        ->orderBy('id', 'asc')
        ->get();
        if(!$tabela->isEmpty()){
            $card = new Cardglossario;
            foreach ($tabela as $elemento){
               $result = $card->generatecard((object) [
                  "nome" => $elemento->nome,
                  "descricao" => $elemento->descricao,
               ]);
               $output .= $result;
            }
        }else
        {
           $output = '
            <p class="text-sm text-center uppercase text-gray-600 pb-5">
               OPS!<br>
               Desculpe, não foram encontrados dados cadastrados para essa letra.
            </p>
           ';
        }
        
        return($output);
    }
}