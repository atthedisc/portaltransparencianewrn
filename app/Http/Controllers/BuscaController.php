<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Components\Rowordemcrono\Rowordemcrono;
use App\Components\Busca\Busca_parametro;
use App\Components\Receitas\Unidadegestora;
use App\Components\Receitas\Fonte as ReceitasFonte;
use App\Components\Despesas\Acao;
use App\Components\Despesas\Despesa;
use App\Components\Despesas\Favorecido;
use App\Components\Despesas\Fonte;
use App\Components\Despesas\Funcao;
use App\Components\Despesas\Unidadegestora as DespesasUnidadegestora;
use App\Components\Receitas\Receita;
use App\Components\Rowcomprasnfe\Rowcomprasnfe;
use App\Components\Rowcomprasservicos\Rowcomprasservicos;
use App\Components\Rowdespesa\Rowacao;
use App\Components\Rowdespesa\Rowdespesa;
use App\Components\Rowdespesa\Rowfavorecido;
use App\Components\Rowdespesa\Rowfonte;
use App\Components\Rowdespesa\Rowfuncao;
use App\Components\Rowdespesa\Rowunidade as RowdespesaRowunidade;
use App\Components\Rowreceita\Rowfonte as RowreceitaRowfonte;
use App\Components\Rowreceita\Rowreceita;
use App\Components\Rowreceita\Rowunidade;
use App\Components\Rowrnmaisvacina\Rowrnmaisvacina;
use App\Models\ComprasPublicasNFe;
use App\Models\Glossario;
use App\Models\SearchLinks;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Collection as SupportCollection;

class BuscaController extends Controller
{
   /**
    * Handle the incoming request.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */

    //Função que retorna as options de busca
   public function option_busca(Request $request)
   {
      $parametros = [];
      $busca = new Busca_parametro;
      //Para Ordem Cronológica
      if($request->source == 'ordem_cronologica') {
         if ($request->mes == 'Todos') {
            $posicao = '<=';
            $mes = 12;
         } else {
            $posicao = '=';
            $mes = $request->mes;
         }
         $parametros = [
            "table" => "vw_vportalordemcronopag",
            "schema" => 'pgsql_siaf',
            "buscar_por" => $request->buscar_por,
            "where" => "cdunidadegestora = '{$request->orgao}' and ".'"CDFONTE"'." = '{$request->fonte}' and ano = '{$request->ano}' and mes {$posicao} {$mes}"
         ];
      }
      //Para Receita
      else if($request->source == 'receita') {
         //serve para unidade, receita e fonte
         $parametros = [
            "table" => "vw_receita",
            "schema" => 'pgsql_siaf',
            "buscar_por" => $request->buscar_por,
            "where" => "numexerciciofinanc = '{$request->ano}' and mes {$request->posicao} {$request->mes}"
         ];
      }
   
      //Para Despesa
      else if($request->source == 'despesa') {
         //serve para fonte, favorecido, ação (resto a implementar)
         $parametros = [
            "table" => $request->fase,
            "schema" => 'pgsql_siaf',
            "buscar_por" => $request->buscar_por,
            "where" => "numexerciciofinanc = '{$request->ano}' and mes {$request->posicao} {$request->mes}"
         ];
      }

      //Para Compras publicas NFE
      else if($request->source == 'compras_nfe') {
         //serve para fonte, favorecido, ação (resto a implementar)
         $parametros = [
            "table" => 'vw_compraspublicasnfe',
            "schema" => 'pgsql',
            "buscar_por" => $request->buscar_por,
            "where" => "ano = '{$request->ano}' and mes = {$request->mes}"
         ];
      }

      //Para Compras e Servico
      else if($request->source == 'compras_servicos') {
         //serve para fonte, favorecido, ação (resto a implementar)
         $parametros = [
            "table" => 'covid19compraseservicos',
            "schema" => 'pgsql',
            "buscar_por" => $request->buscar_por,
         ];
      }

      //Para Rn mais vacina
      else if($request->source == 'rn_mais_vacina') {
         //serve para fonte, favorecido, ação (resto a implementar)
         $parametros = [
            "table" => 'vacinacaocompraseservicos',
            "schema" => 'pgsql',
            "buscar_por" => $request->buscar_por,
         ];
      }

      $result = $busca->generatebusca_parametro((object) $parametros);
      return $result;
   }


 // Gerar resultado da busca

   public function buscar(Request $request)
   {
      $dados = '';
      $row = null;
      $query = null;
      $parametro_busca = urldecode($request->busca_parametro);

      //Para Ordem Cronológica
      if($request->source == 'ordem_cronologica'){
         //setando datas para ordem cronologica
         if ($request->mes == 'Todos') {
            $posicao = '<=';
            $mes = 12;
         } else {
            $posicao = '=';
            $mes = $request->mes;
         }
         
         if ($request->input('reset')) {
            $query = DB::connection('pgsql_siaf')->table('vw_vportalordemcronopag')
               ->whereRaw('"CDFONTE"'." = '{$request->fonte}' and cdunidadegestora = '{$request->orgao}' and ano = '{$request->ano}' and mes {$posicao} {$mes}")
               ->paginate(10);
         } else {
            $query = DB::connection('pgsql_siaf')->table('vw_vportalordemcronopag')
               ->whereRaw('"CDFONTE"'." = '{$request->fonte}' and cdunidadegestora = '{$request->orgao}' and ano = '{$request->ano}' and mes {$posicao} {$mes} and ".'"'.$request->buscar_por.'"'." = '{$parametro_busca}'")
               ->get();
         }
         $row = new Rowordemcrono;
      }
      //para Compra publicas NFE
      else if($request->source == 'compras_nfe'){
         
         if ($request->input('reset')) {
            $query = ComprasPublicasNFe::where('ano', $request->ano)
            ->where('mes', $request->mes)
            ->paginate(15);
         } else {
            $query = ComprasPublicasNFe::where('ano', $request->ano)
            ->where('mes', $request->mes)
            ->where($request->buscar_por,$parametro_busca)
            ->get();
         }
         $row = new Rowcomprasnfe;
      }
      //Para Compras e Servico
      else if($request->source == 'compras_servicos'){
         
         if ($request->input('reset')) {
            $query = DB::table('covid19compraseservicos')->orderBy('id')->paginate(10);
         } else {
            $query = DB::table('covid19compraseservicos')
            ->where($request->buscar_por,$parametro_busca)
            ->orderBy('id')
            ->get();
         }
         $row = new Rowcomprasservicos;
      }
      //Para Rn mais vacina
      else if($request->source == 'rn_mais_vacina'){
         
         if ($request->input('reset')) {
            $query = DB::table('vacinacaocompraseservicos')->orderBy('id')->paginate(10);
         } else {
            $query = DB::table('vacinacaocompraseservicos')
            ->where($request->buscar_por,$parametro_busca)
            ->orderBy('id')
            ->get();
         }
         $row = new Rowrnmaisvacina;
      }
      //Para Receita
      else if($request->source == 'receita'){
         if($request->page == 'fonte') {
            $query = new ReceitasFonte;
            $row = new RowreceitaRowfonte;
         }
         else if($request->page == 'unidade') {
            $query = new Unidadegestora;
            $row = new Rowunidade;
         }
         else if($request->page == 'receita') {
            $query = new Receita;
            $row = new Rowreceita;
         }
         
         if ($request->input('reset')) {
            $query = $query->query((object) [
               "mes" => $request->mes,
               "ano" => $request->ano,
               "posicao" => $request->posicao,
               "where" => "mes {$request->posicao} {$request->mes} and numexerciciofinanc = {$request->ano}"
            ]);
         } else {
            if($request->page == 'receita'){
               if($request->buscar_por == 'txtdescricaodetalhada'){
                  $where = "mes {$request->posicao} {$request->mes} and codcategoria != '8000000000' and {$request->buscar_por} = '{$parametro_busca}' and numexerciciofinanc = '{$request->ano}'";
               }
               elseif($request->buscar_por == 'txtdescricaodeducaodetalhada'){
                  $where = "mes {$request->posicao} {$request->mes} and codcategoria = '8000000000' and {$request->buscar_por} = '{$parametro_busca}' and numexerciciofinanc = '{$request->ano}'";
               }
               else{
                  $where = "mes {$request->posicao} {$request->mes} and {$request->buscar_por} = '{$parametro_busca}' and numexerciciofinanc = '{$request->ano}'";
               }
            }
            else{
               $where = "mes {$request->posicao} {$request->mes} and {$request->buscar_por} = '{$parametro_busca}' and numexerciciofinanc = '{$request->ano}'";
            }
            $query = $query->query((object) [
               "mes" => $request->mes,
               "ano" => $request->ano,
               "posicao" => $request->posicao,
               "where" => $where
            ]);

         }
      }
      //Para Despesa
      else if($request->source == 'despesa'){
         //selecionar qual row e query usar
         if($request->page == 'fonte') {
            $query = new Fonte;
            $row = new Rowfonte;
         }
         elseif($request->page == 'acao') {
            $query = new Acao;
            $row = new Rowacao;
         }
         elseif($request->page == 'favorecido') {
            $query = new Favorecido;
            $row = new Rowfavorecido;
         }
         elseif($request->page == 'despesa') {
            $query = new Despesa;
            $row = new Rowdespesa;
         }
         elseif($request->page == 'unidade') {
            $query = new DespesasUnidadegestora;
            $row = new RowdespesaRowunidade;
         }
         elseif($request->page == 'funcao') {
            $query = new Funcao;
            $row = new Rowfuncao;
         }
         if ($request->input('reset')) {
            $query = $query->query((object) [
               "mes" => $request->mes,
               "ano" => $request->ano,
               "posicao" => $request->posicao,
               "fase" => $request->fase,
               "where" => "mes {$request->posicao} {$request->mes} and numexerciciofinanc = {$request->ano}"
            ]);
         } else {
            $query = $query->query((object) [
               "mes" => $request->mes,
               "ano" => $request->ano,
               "posicao" => $request->posicao,
               "fase" => $request->fase,
               "where" => "mes {$request->posicao} {$request->mes} and {$request->buscar_por} = '{$parametro_busca}' and numexerciciofinanc = '{$request->ano}'"
            ]);
         }
      }
      //Montagem da Row (serve para todos)
         $dados = $row->generaterow((object) [
            "dado" => (object) $query,
            "request" => $request,
            'buscar_por' => $request->buscar_por,
            'busca_parametro' => $parametro_busca,
            'reset' => $request->input('reset')
         ]);
         if($dados != ""){
            return $dados;
         }
         return '<tr><td><p class="p-2"> Não foram encontrados dados para essa pesquisa</p></td></tr>';
   }

   public function search (Request $request) {
      $parameter = $request->input()['q'];

      $pages = SearchLinks::whereRaw("unaccent(".'title'.") ilike unaccent('%".$parameter."%')")
      ->orWhereRaw("unaccent(".'name'.") ilike unaccent('%".$parameter."%')")->get()->toArray();

      $glossario = Glossario::whereRaw("unaccent(".'nome'.") ilike unaccent('%".$parameter."%')")
      ->orWhereRaw("unaccent(".'descricao'.") ilike unaccent('%".$parameter."%')")->get();

      $pagesSerialized = [];

      foreach ($pages as $page) {
         array_push($pagesSerialized, (object) ['title' => $page['title'], 'name' => $page['name'], 'url' => env('APP_URL') . $page['url'], 'path' => $page['url']]) ;
      }

      return view('search.results', ['pages' => collect($pagesSerialized), 'glossario' => $glossario]);
   }
}
