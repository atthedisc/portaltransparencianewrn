<?php

namespace App\Http\Controllers;

use App\Components\Busca\Buscar_por;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class TodosTiposGastosController extends Controller
{
   /**
    * Handle the incoming request.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function index(Request $request)
   {
      $anos = collect([2024, 2023, 2022, 2021, 2020, 2019]);

      if ($request->ano == NULL) {
         $request->ano = 2024;
      }

      $administracaogastos = Cache::remember('administracaogastos' . $request->ano, 86400, function () use ($request) {
         return DB::connection('pgsql_siaf')->table('vportal_notapagamento_gastosdiretos')
            ->select('vlrpagamento')
            ->where('numexerciciofinanc', $request->ano)
            ->where('txtdescricaofuncao', 'Administração')
            ->get();
      });

      $agriculturagastos = Cache::remember('agriculturagastos' . $request->ano, 86400, function () use ($request) {
         return DB::connection('pgsql_siaf')->table('vportal_notapagamento_gastosdiretos')
            ->select('vlrpagamento')
            ->where('numexerciciofinanc', $request->ano)
            ->where('txtdescricaofuncao', 'Agricultura')
            ->get();
      });

      $assistenciagastos = Cache::remember('assistenciagastos' . $request->ano, 86400, function () use ($request) {
         return DB::connection('pgsql_siaf')->table('vportal_notapagamento_gastosdiretos')
            ->select('vlrpagamento')
            ->where('numexerciciofinanc', $request->ano)
            ->where('txtdescricaofuncao', 'Assistência Social')
            ->get();
      });

      $cetgastos = Cache::remember('cetgastos' . $request->ano, 86400, function () use ($request) {
         return DB::connection('pgsql_siaf')->table('vportal_notapagamento_gastosdiretos')
            ->select('vlrpagamento')
            ->where('numexerciciofinanc', $request->ano)
            ->where('txtdescricaofuncao', 'Ciência e Tecnologia')
            ->get();
      });

      $comunicacoesgastos = Cache::remember('comunicacaogastos' . $request->ano, 86400, function () use ($request) {
         return DB::connection('pgsql_siaf')->table('vportal_notapagamento_gastosdiretos')
            ->select('vlrpagamento')
            ->where('numexerciciofinanc', $request->ano)
            ->where('txtdescricaofuncao', 'Comunicações')
            ->get();
      });

      $comerciogastos = Cache::remember('comerciogastos' . $request->ano, 86400, function () use ($request) {
         return DB::connection('pgsql_siaf')->table('vportal_notapagamento_gastosdiretos')
            ->select('vlrpagamento')
            ->where('numexerciciofinanc', $request->ano)
            ->where('txtdescricaofuncao', 'Comércio e Serviços')
            ->get();
      });

      $culturagastos = Cache::remember('culturagastos' . $request->ano, 86400, function () use ($request) {
         return DB::connection('pgsql_siaf')->table('vportal_notapagamento_gastosdiretos')
            ->select('vlrpagamento')
            ->where('numexerciciofinanc', $request->ano)
            ->where('txtdescricaofuncao', 'Cultura')
            ->get();
      });

      $direitosgastos = Cache::remember('direitosgastos' . $request->ano, 86400, function () use ($request) {
         return DB::connection('pgsql_siaf')->table('vportal_notapagamento_gastosdiretos')
            ->select('vlrpagamento')
            ->where('numexerciciofinanc', $request->ano)
            ->where('txtdescricaofuncao', 'Direitos da Cidadania')
            ->get();
      });

      $educacaogastos = Cache::remember('educacaogastos' . $request->ano, 86400, function () use ($request) {
         return DB::connection('pgsql_siaf')->table('vportal_notapagamento_gastosdiretos')
            ->select('vlrpagamento')
            ->where('numexerciciofinanc', $request->ano)
            ->where('txtdescricaofuncao', 'Educação')
            ->get();
      });

      $encargosgastos = Cache::remember('encargosgastos' . $request->ano, 86400, function () use ($request) {
         return DB::connection('pgsql_siaf')->table('vportal_notapagamento_gastosdiretos')
            ->select('vlrpagamento')
            ->where('numexerciciofinanc', $request->ano)
            ->where('txtdescricaofuncao', 'Encargos Especiais')
            ->get();
      });

      $energiagastos = Cache::remember('energiagastos' . $request->ano, 86400, function () use ($request) {
         return DB::connection('pgsql_siaf')->table('vportal_notapagamento_gastosdiretos')
            ->select('vlrpagamento')
            ->where('numexerciciofinanc', $request->ano)
            ->where('txtdescricaofuncao', 'Energia')
            ->get();
      });

      $gestaogastos = Cache::remember('gestaogastos' . $request->ano, 86400, function () use ($request) {
         return DB::connection('pgsql_siaf')->table('vportal_notapagamento_gastosdiretos')
            ->select('vlrpagamento')
            ->where('numexerciciofinanc', $request->ano)
            ->where('txtdescricaofuncao', 'Gestão Ambiental')
            ->get();
      });

      $industriagastos = Cache::remember('industriagastos' . $request->ano, 86400, function () use ($request) {
         return DB::connection('pgsql_siaf')->table('vportal_notapagamento_gastosdiretos')
            ->select('vlrpagamento')
            ->where('numexerciciofinanc', $request->ano)
            ->where('txtdescricaofuncao', 'Indústria')
            ->get();
      });

      $judiciariagastos = Cache::remember('judiciariogastos' . $request->ano, 86400, function () use ($request) {
         return DB::connection('pgsql_siaf')->table('vportal_notapagamento_gastosdiretos')
            ->select('vlrpagamento')
            ->where('numexerciciofinanc', $request->ano)
            ->where('txtdescricaofuncao', 'Judiciária')
            ->get();
      });

      $habitacaogastos = Cache::remember('habitacaogastos' . $request->ano, 86400, function () use ($request) {
         return DB::connection('pgsql_siaf')->table('vportal_notapagamento_gastosdiretos')
            ->select('vlrpagamento')
            ->where('numexerciciofinanc', $request->ano)
            ->where('txtdescricaofuncao', 'Habitação')
            ->get();
      });

      $legislativagastos = Cache::remember('legislativagastos' . $request->ano, 86400, function () use ($request) {
         return DB::connection('pgsql_siaf')->table('vportal_notapagamento_gastosdiretos')
            ->select('vlrpagamento')
            ->where('numexerciciofinanc', $request->ano)
            ->where('txtdescricaofuncao', 'Legislativa')
            ->get();
      });

      $previdenciagastos = Cache::remember('previdenciagastos' . $request->ano, 86400, function () use ($request) {
         return DB::connection('pgsql_siaf')->table('vportal_notapagamento_gastosdiretos')
            ->select('vlrpagamento')
            ->where('numexerciciofinanc', $request->ano)
            ->where('txtdescricaofuncao', 'Previdência Social')
            ->get();
      });

      $saudegastos = Cache::remember('saudegastos' . $request->ano, 86400, function () use ($request) {
         return DB::connection('pgsql_siaf')->table('vportal_notapagamento_gastosdiretos')
            ->select('vlrpagamento')
            ->where('numexerciciofinanc', $request->ano)
            ->where('txtdescricaofuncao', 'Saúde')
            ->get();
      });

      $segurancagastos = Cache::remember('segurancagastos' . $request->ano, 86400, function () use ($request) {
         return DB::connection('pgsql_siaf')->table('vportal_notapagamento_gastosdiretos')
            ->select('vlrpagamento')
            ->where('numexerciciofinanc', $request->ano)
            ->where('txtdescricaofuncao', 'Segurança Pública')
            ->get();
      });

      $organizacaoagrariagastos = Cache::remember('organizacaoagrariagastos' . $request->ano, 86400, function () use ($request) {
         return DB::connection('pgsql_siaf')->table('vportal_notapagamento_gastosdiretos')
            ->select('vlrpagamento')
            ->where('numexerciciofinanc', $request->ano)
            ->where('txtdescricaofuncao', 'Organização Agrária')
            ->get();
      });

      $transportegastos = Cache::remember('transportegastos' . $request->ano, 86400, function () use ($request) {
         return DB::connection('pgsql_siaf')->table('vportal_notapagamento_gastosdiretos')
            ->select('vlrpagamento')
            ->where('numexerciciofinanc', $request->ano)
            ->where('txtdescricaofuncao', 'Transporte')
            ->get();
      });

      return view('todos-tipos-gastos.index', [
         'administracaogastos' => $administracaogastos,
         'agriculturagastos' => $agriculturagastos,
         'assistenciagastos' => $assistenciagastos,
         'cetgastos' => $cetgastos,
         'comunicacoesgastos' => $comunicacoesgastos,
         'comerciogastos' => $comerciogastos,
         'culturagastos' => $culturagastos,
         'direitosgastos' => $direitosgastos,
         'educacaogastos' => $educacaogastos,
         'encargosgastos' => $encargosgastos,
         'energiagastos' => $energiagastos,
         'gestaogastos' => $gestaogastos,
         'industriagastos' => $industriagastos,
         'judiciariagastos' => $judiciariagastos,
         'habitacaogastos' => $habitacaogastos,
         'legislativagastos' => $legislativagastos,
         'previdenciagastos' => $previdenciagastos,
         'saudegastos' => $saudegastos,
         'segurancagastos' => $segurancagastos,
         'organizacaoagrariagastos' => $organizacaoagrariagastos,
         'transportegastos' => $transportegastos,
         'ano_selected' => $request->ano,
         'anos' => $anos
      ]);
   }

   public function gastosporfuncao($funcaore, $ano)
   {
      $busca = new Buscar_por;
      $busca = $busca->generatebuscar_por([
         "None" => "",
         "txtdescricaofuncao" => "Função",
         "txtdescricaosubfuncao" => "Subfunção"
      ]);
      // valor total da despesa
      $sumtotal = DB::connection('pgsql_siaf')->table('vportal_notapagamento_gastosdiretos')
         ->where('mes', '<=', '12')
         ->where('txtdescricaofuncao', $funcaore)
         ->where('numexerciciofinanc', $ano)
         ->sum('vlrpagamento');

      $funcao = DB::connection('pgsql_siaf')->table('vportal_notapagamento_gastosdiretos')
         ->select('txtdescricaofuncao', 'txtdescricaofuncao', DB::raw('SUM(vlrpagamento) as vlrpagamento'), DB::raw('SUM(vlrpagamentorp) as vlrpagamentorp'))
         ->where('mes', '<=', '12')
         ->where('txtdescricaofuncao', $funcaore)
         ->where('numexerciciofinanc', $ano)
         ->groupBy('txtdescricaofuncao', 'txtdescricaofuncao')
         ->get();

      // Gráficos
      $funcaochart = DB::connection('pgsql_siaf')->table('vportal_notapagamento_gastosdiretos')
         ->select('txtdescricaofuncao', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
         ->where('mes', '<=', '12')
         ->where('numexerciciofinanc', $ano)
         ->where('txtdescricaofuncao', $funcaore)
         ->groupBy('txtdescricaofuncao')
         ->orderBy('txtdescricaofuncao')
         ->get();

      $elementofuncaocharts = DB::connection('pgsql_siaf')->table('vportal_notapagamento_gastosdiretos')
         ->select('txtdescricaosubfuncao', 'txtdescricaofuncao', DB::raw('SUM(vlrpagamento) as vlrpagamento'))
         ->where('mes', '<=', '12')
         ->where('numexerciciofinanc', $ano)
         ->where('txtdescricaofuncao', $funcaore)
         ->groupBy('txtdescricaosubfuncao', 'txtdescricaofuncao')
         ->orderBy('txtdescricaosubfuncao')
         ->get();

      return view('despesas/gastos-diretos/funcao.index', [
         'funcao' => $funcao,
         'fase' => 'vportal_notapagamento_gastosdiretos',
         'mes' => '12',
         'ano' => $ano,
         'classificacao' => 'funcao',
         'posicao' => '<=',
         'nomefase' => 'Pagamento',
         'sumtotal' => $sumtotal,
         'funcaochart' => $funcaochart,
         'elementofuncaocharts' => $elementofuncaocharts,
         'busca' => $busca
      ]);
   }
}
