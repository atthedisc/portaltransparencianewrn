<?php

namespace App\Http\Controllers\Covid;

use App\Components\Busca\Buscar_por;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\CompraseservicosExport;

class CompraseServicosController extends Controller
{
   public function index(Request $request)
   {
      $quadro = DB::table('covid19compraseservicosquadro')->orderBy('id')->get();
      $tabela = DB::table('covid19compraseservicos')->orderBy('id')->paginate(10);

      $busca = new Buscar_por;
      $busca = $busca->generatebuscar_por([
         "None" => "",
         "contratante" => 'Orgão',
         "ncontrato" => 'N. contrato',
         "nprocesso" => 'N. processo',
         "dataassinatura" => 'Data assinatura',
         "contratado" => "Contratado(a)",
         "objeto" => "Objeto",
      ]);

      return view('covid/compras-e-servicos.index', [
         'dados' => $quadro,
         'tables' => $tabela,
         'busca' => $busca
      ]);
   }

   public function exportxls()
   {
      return Excel::download(new CompraseservicosExport, 'compras-e-servicos.xlsx');
   }

   public function exportpdf()
   {
      return Excel::download(new CompraseservicosExport, 'compras-e-servicos.pdf');
   }
}
