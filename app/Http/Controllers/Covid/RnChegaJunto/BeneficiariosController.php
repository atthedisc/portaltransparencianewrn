<?php

namespace App\Http\Controllers\Covid\RnChegaJunto;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BeneficiariosController extends Controller
{
   public function index()
   {
      $tabela = DB::table('rnchegajunto')
         ->select('beneficiarios', 'beneficiarioslink')
         ->whereNotNull('beneficiarios')
         ->orderBy('id', 'desc')
         ->paginate(10);
      return view('covid/rn-chega-junto.beneficiarios', [
         'tables' => $tabela
      ]);
   }
}
