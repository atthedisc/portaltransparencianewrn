<?php

namespace App\Http\Controllers\Covid\RnChegaJunto;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DoacoesRecebidasController extends Controller
{
   public function index()
   {
      $tabela = DB::table('rnchegajunto')
         ->select('doacoesrecebidas', 'doacoesrecebidaslink')
         ->whereNotNull('doacoesrecebidas')
         ->orderBy('id', 'desc')
         ->paginate(10);
      return view('covid/rn-chega-junto.doacoes-recebidas', [
         'tables' => $tabela
      ]);
   }
}
