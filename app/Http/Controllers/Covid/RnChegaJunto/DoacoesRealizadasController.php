<?php

namespace App\Http\Controllers\Covid\RnChegaJunto;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DoacoesRealizadasController extends Controller
{
   public function index()
   {
      $tabela = DB::table('rnchegajunto')->orderBy('id', 'desc')->paginate(10);
      return view('covid/rn-chega-junto.doacoes-realizadas', [
         'tables' => $tabela
      ]);
   }
}
