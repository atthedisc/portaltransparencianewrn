<?php

namespace App\Http\Controllers\Covid\RnMaisUnido;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BeneficiariosController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $tables = DB::table('rnmaisunido_beneficiarios')->orderBy('id')->get();
        return view('covid\rn-mais-unido.beneficiarios',['tables'=> $tables]);
    }
}
