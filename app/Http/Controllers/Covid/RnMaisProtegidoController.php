<?php

namespace App\Http\Controllers\Covid;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RnMaisProtegidoController extends Controller
{
   public function index()
   {
      $tabela = DB::table('rnmaisprotegido')->orderBy('id', 'desc')->paginate(10);
      return view('covid/rn-mais-protegido.doacoesrecebidas', [
         'tables' => $tabela
      ]);
   }
}
