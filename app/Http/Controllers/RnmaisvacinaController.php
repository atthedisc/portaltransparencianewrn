<?php

namespace App\Http\Controllers;

use App\Components\Busca\Buscar_por;
use App\Exports\RnMaisVacinaExport;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class RnmaisvacinaController extends Controller
{
   public function index($status)
   {
      if ($status == 'covid') {
         $layouts = "layouts/covid.app";
      } else if ($status == 'transparencia') {
         $layouts = "layouts.app";
         $status = '';
      } else {
         return view('errors.404');
      }

      $busca = new Buscar_por;
      $busca = $busca->generatebuscar_por([
         "None" => "",
         "contratante" => 'Contratante',
         "objeto" => 'Objeto',
         "contratado" => 'Contratado(a)',
         "cnpjcpf" => 'CNPJ/CPF',
         "atocontratual" => 'Ato contratual',
         "nprocesso" => 'N. processo',
      ]);

      $tabela = DB::table('vacinacaocompraseservicos')->orderBy('id')->paginate(10);

      return view('rn-mais-vacina.index', [
         'tables' => $tabela,
         'layouts' => $layouts,
         'status' => $status,
         'busca' => $busca
      ]);
   }

   public function exportxls()
   {
      return Excel::download(new RnMaisVacinaExport, 'rn-mais-vacina.xlsx');
   }
   public function exportpdf()
   {
      return Excel::download(new RnMaisVacinaExport, 'rn-mais-vacina.pdf');
   }
}