<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class CookieController extends Controller
{
    /**
     * Get a cookie.
     *
     * @param  Request  $request
     * @return string
     */
    public function getCookie(Request $request)
    {
        $value = $request->cookie('name');
        return $value;
    }

    public function listCookies()
    {
        $cookies = Cookie::get();
        return $cookies;
    }

    public function deleteAllCookies()
    {
        $cookies = Cookie::get();
        try {
            foreach ($cookies as $name => $value) {
                if ($name != "XSRF-TOKEN" && $name != "laravel_session") {
                    Cookie::queue(Cookie::forget($name));
                }
            }
        } catch (\Throwable $th) {
            throw $th;
            return response('error', 500);
        }

        //fazer retornar algum sucesso?
        return response('ok', 200);
    }
}
