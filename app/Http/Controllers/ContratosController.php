<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContratosController extends Controller
{
    public function __invoke(Request $request)
    {
        //dd($request);
        $jurisdicionado = DB::table('tcecontratos')->groupBy('jurisdicionado')->orderBy('jurisdicionado')->get('jurisdicionado');
        $ano = DB::table('tcecontratos')->groupBy('ano')->orderBy('ano', 'desc')->get('ano');

        $dados = DB::table('tcecontratos')
            ->where('jurisdicionado', $request->jurisdicionado)
            ->where('ano', $request->ano)
            ->paginate(10);

        return view('contratos.index', [
            'jurisdicionado' => $jurisdicionado,
            'anos' => $ano,
            'dados' => $dados,
            'ano_selected' => $request->ano,
            'jurisdicionado_selected' => $request->jurisdicionado,
        ]);
    }
}
