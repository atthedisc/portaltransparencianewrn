<?php

namespace App\Http\Controllers\RnChegaJuntoNoCombateAFome;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DoadoresController extends Controller
{
  public function index()
    {
        $tabela = DB::table('rnchegajuntonocombateafome')
        ->select('doadores', 'doadoreslink')
        ->whereNotNull('doadores')
        ->orderBy('id', 'desc')
        ->paginate(7);
        return view('rn-chega-junto-no-combate-a-fome/doacoes-da-sociedade.doadores', [
            'tables' => $tabela
        ]);
    }
}
