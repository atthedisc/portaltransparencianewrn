<?php

namespace App\Http\Controllers\RnChegaJuntoNoCombateAFome;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DoacoesDoGovernoController extends Controller
{
  public function index()
    {
        $tabela = DB::table('rnchegajuntonocombateafome')->orderBy('id', 'desc')->paginate(7);
        return view('rn-chega-junto-no-combate-a-fome.doacoes-do-governo', [
            'tables' => $tabela
        ]);
    }
}
