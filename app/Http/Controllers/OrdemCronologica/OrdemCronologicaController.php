<?php

namespace App\Http\Controllers\OrdemCronologica;

use App\Components\Busca\Buscar_por;
use App\Exports\OrdemCronologicaExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class OrdemCronologicaController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $orgao = DB::table('orgao_interno')->orderBy('nomeorgao')->get();
        $ano = collect(['', 2024, 2023, 2022, 2021, 2020, 2019]);
        $mes = collect(['','Todos', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]);

        $fonte = DB::connection('pgsql_siaf')->table('vw_vportalordemcronopag')
            ->select('NMFONTE', 'CDFONTE')
            ->where('cdunidadegestora', $request->orgao)
            ->where('NMFONTE', '!=', 'TODAS')
            ->groupBy('NMFONTE', 'CDFONTE')
            ->get();

        if ($request->mes == 'Todos') {
            $dados = DB::connection('pgsql_siaf')->table('vw_vportalordemcronopag')
                ->where('CDFONTE', $request->fonte)
                ->where('cdunidadegestora', $request->orgao)
                ->where('ano', $request->ano)
                ->paginate(10);
        } else {
            $dados = DB::connection('pgsql_siaf')->table('vw_vportalordemcronopag')
                ->where('CDFONTE', $request->fonte)
                ->where('cdunidadegestora', $request->orgao)
                ->where('ano', $request->ano)
                ->where('mes', $request->mes)
                ->paginate(10);
        }
        
        $busca = new Buscar_por;
        $busca = $busca->generatebuscar_por([
           "None" => "",
           "NMCREDOR" => "Credor",
           "NUPROCESSO" => "Numero Processo",
           "ATESTO" => "Data Atesto",
           "DTAUTUACAO" => "Data da Atuação"
        ]);
        
        return view('ordem-cronologica.index', [
            'orgaos' => $orgao,
            'fontes' => $fonte,
            'anos' => $ano,
            'meses' => $mes,
            'dados' => $dados,
            'orgao_selected' => $request->orgao,
            'fonte_selected' => $request->fonte,
            'ano_selected' => $request->ano,
            'mes_selected' => $request->mes,
            'busca' => $busca
        ]);
    }

    public function exportxls($mes, $ano, $orgao, $fonte)
    {
       return Excel::download(new OrdemCronologicaExport($mes, $ano, $orgao, $fonte), 'OrdemCronologica-' . $fonte . '.xls');
    }
    public function exportpdf($mes, $ano, $orgao, $fonte)
    {
       return Excel::download(new OrdemCronologicaExport($mes, $ano, $orgao, $fonte), 'OrdemCronologica-' . $fonte . '.pdf');
    }
}
