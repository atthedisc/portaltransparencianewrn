<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CampanhasController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        return view('campanhas.index', [
            'id' => $id
        ]);
    }
}
