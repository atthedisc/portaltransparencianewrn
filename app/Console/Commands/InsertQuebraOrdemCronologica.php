<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Output\ConsoleOutput;

class InsertQuebraOrdemCronologica extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insert:quebraordem';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando para Inserir as Justificativas de quebra de ordem cronológica';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $output = new ConsoleOutput();
        
        $output->writeln("<info>my message</info>");

        $justificativaQuebraOrdem = DB::connection('pgsql_siaf')->table('quebraordemcronopag')->get(); // tabela com as justificativas corretas

        // tabela de ordem cronologica
        foreach ($justificativaQuebraOrdem as $key => $quebraOrdem) {
            $update = DB::connection('pgsql_siaf')->table('vportalordemcronopag')
                ->where('CDUNIDADEGESTORA', $quebraOrdem->cdunidadegestora)
                ->where('NUPREPARACAOPAGAMENTO', $quebraOrdem->nupreparacaopagamento)
                ->update(['JUSTIFICATIVAQUEBRAORDEM' => $quebraOrdem->justificativaquebraordem]);

            if ($update) {
                $output->writeln("<info>" . $key . " elemento " . $quebraOrdem->cdunidadegestora . " - " . $quebraOrdem->nupreparacaopagamento . "</info>");
            } else {
                $output->writeln("<error>" . $key . " não encontrado" . "</error>");
            }
        }
        return Command::SUCCESS;
    }
}
