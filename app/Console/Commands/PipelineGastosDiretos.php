<?php

namespace App\Console\Commands;

use App\Models\GastosDireto\LiquidacaoGastosDiretos;
use App\Models\GastosDireto\NotaEmpenhoGastosDiretos;
use App\Models\GastosDireto\NotaPagamentoGastosDiretos;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class PipelineGastosDiretos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pipeline:gastosdiretos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando para inserir Dados das views de Despesas Gastos Diretos na tabela de referência exibida no Portal';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        NotaPagamentoGastosDiretos::query()->delete();
        DB::connection('pgsql_siaf')->statement('INSERT INTO vportal_notapagamento_gastosdiretos SELECT * FROM vw_notapagamento_gastosdiretos');

        NotaEmpenhoGastosDiretos::query()->delete();
        DB::connection('pgsql_siaf')->statement('INSERT INTO vportal_notaempenho_gastosdiretos SELECT * FROM vw_notaempenho_gastosdiretos');

        LiquidacaoGastosDiretos::query()->delete();
        DB::connection('pgsql_siaf')->statement('INSERT INTO vportal_liquidacaodespesa_gastosdiretos SELECT * FROM vw_liquidacaodespesa_gastosdiretos');

        return Command::SUCCESS;
    }
}
