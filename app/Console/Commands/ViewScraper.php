<?php

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;
use App\Models\SearchLinks;

class ViewScraper extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'view:scraper';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command is intended to scrape specific data from all of the GET routes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    private function isInvalidPageTitle(string $title, string $routeName)
    {
        return (str_contains($title, 'Page not found') || str_contains($title, 'Error 500') || !$routeName || str_contains($title, '🧨'));
    }


    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $filtered_routes = ['debugbar', 'api'];
        $route_collection = Route::getRoutes();
        $routes = [];

        foreach ($route_collection as $value) {
            $string_contains = false;
            foreach ($filtered_routes as $route) {
                $string_contains = $string_contains ? True : str_contains($value->getName(), $route) || str_contains($value->uri, $route);
            }

            if (in_array('GET', $value->methods) && !$string_contains) {
                array_push($routes, array('name' => $value->getName(), 'uri' => $value->uri));
            }
        }

        $length = strval(count($routes));

        foreach ($routes as $key => $route) {
            dump(strval($key + 1) . ' of ' . $length);

            $tag = [];
            try {
                $url = env('APP_URL') . (substr($route['uri'], 0, 1) == "/" ? $route['uri'] : ("/" . $route['uri']));
                $response = Http::get($url);
                preg_match("/(?<=\<title\>)(\s*?)(.*?)(\s*?)(?=\<\/title\>)/", $response->body(), $tag);
                $title = '';
                if (str_contains($tag[2], 'Portal da Transparência do Rio Grande do Norte')) {
                    $route['title'] = $tag[2];
                    #$routes[$key] = $route;
                    $title = str_replace(' - ', '', str_replace('Portal da Transparência do Rio Grande do Norte', '', $route['title']));
                } else {
                    $title = $tag[2];
                }

                if ($this->isInvalidPageTitle($title, $route['name'])) {
                    throw new Exception('Invalid Route');
                }

                SearchLinks::firstOrCreate([
                    'title' =>  $title,
                    'name' => $route['name'],
                    'url' => (substr($route['uri'], 0, 1) == "/" ? $route['uri'] : ("/" . $route['uri'])),
                ]);
            } catch (Exception $e) {
                dump("Tag",$tag);
                dump("Rota",$route);
                dump($e);
                unset($routes[$key]);
            }
        }
        dd($routes);
        return 0;
    }
}
