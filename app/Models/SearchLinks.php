<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SearchLinks extends Model
{
    protected $connection = 'pgsql';
    
    protected $table = 'search_links';
    
    protected $fillable = ['title', 'url', 'name'];
}
