<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Receitas extends Model
{
    protected $connection = 'pgsql_siaf';
    
    protected $table = 'vw_receita';
}
