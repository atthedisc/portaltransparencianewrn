<?php

namespace App\Models\TransferenciaDeRecursos;

use Illuminate\Database\Eloquent\Model;

class TransferenciaNotaPagamento extends Model
{
    protected $connection = 'pgsql_siaf';

    protected $table = 'vw_notapagamento_transferenciarecursos';
}
