<?php

namespace App\Models\TransferenciaDeRecursos;

use Illuminate\Database\Eloquent\Model;

class TransferenciaLiquidacao extends Model
{
    protected $connection = 'pgsql_siaf';

    protected $table = 'vw_liquidacaodespesa_transferenciarecursos';
}
