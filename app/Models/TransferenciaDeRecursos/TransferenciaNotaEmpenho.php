<?php

namespace App\Models\TransferenciaDeRecursos;

use Illuminate\Database\Eloquent\Model;

class TransferenciaNotaEmpenho extends Model
{
    protected $connection = 'pgsql_siaf';

    protected $table = 'vw_notaempenho_transferenciarecursos';
}
