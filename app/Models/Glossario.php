<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Glossario extends Model
{
    protected $connection = 'pgsql';
    
    protected $table = 'novoglossario';

}
