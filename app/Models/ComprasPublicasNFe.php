<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ComprasPublicasNFe extends Model
{
    protected $table = 'vw_compraspublicasnfe';
}
