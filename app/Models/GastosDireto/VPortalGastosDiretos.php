<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VPortalGastosDiretos extends Model
{
    protected $connection = 'pgsql_siaf';
    protected $table = 'vportal_gastosdiretos';
}
