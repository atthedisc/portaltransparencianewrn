<?php

namespace App\Models\GastosDireto;

use Illuminate\Database\Eloquent\Model;

class LiquidacaoGastosDiretos extends Model
{
    protected $connection = 'pgsql_siaf';

    protected $table = 'vportal_liquidacaodespesa_gastosdiretos';
}
