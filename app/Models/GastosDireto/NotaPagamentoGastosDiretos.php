<?php

namespace App\Models\GastosDireto;

use Illuminate\Database\Eloquent\Model;

class NotaPagamentoGastosDiretos extends Model
{
    protected $connection = 'pgsql_siaf';

    protected $table = 'vportal_notapagamento_gastosdiretos';
}
