<?php

namespace App\Models\GastosDireto;

use Illuminate\Database\Eloquent\Model;

class NotaEmpenhoGastosDiretos extends Model
{
    protected $connection = 'pgsql_siaf';

    protected $table = 'vportal_notaempenho_gastosdiretos';
}
