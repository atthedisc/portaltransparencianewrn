<?php

namespace App\Models\GastosDireto;

use Illuminate\Database\Eloquent\Model;

class VGastosDiretos extends Model
{
    protected $connection = 'pgsql_siaf';

    protected $table = 'vportal_gastosdiretos';
}
