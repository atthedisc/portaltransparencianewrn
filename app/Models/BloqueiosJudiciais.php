<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BloqueiosJudiciais extends Model
{
    protected $connection = 'pgsql_siaf';

    protected $table = 'bloqueiosjudiciais';
}
