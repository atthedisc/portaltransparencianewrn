<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RepasseOutrosPoderes extends Model
{
    protected $connection = 'pgsql_siaf';

    protected $table = 'vw_repasseoutrospoderesmensal';
}
