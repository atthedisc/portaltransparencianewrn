let info = document.getElementById('info');
let source = info.getAttribute('data-source');

//Após coletar os dados necessarios, envia um request via ajax para que sejam resgatados os dados de acordo com o selecioando 
function buscar() {
    let _token = document.querySelector('input[name="_token"]').value
    const busca_parametro = $('#busca_parametro').val();
    if (busca_parametro != "None") {
        document.querySelector('#load').classList.remove('hidden');
        // Composição do request com as condições e váriaveis que serão utilizadas no processamento da requisição
        data = { buscar_por: $('#buscar_por').val(),busca_parametro, _token, ...get_consts() };
        $.ajax({
            type: 'POST',
            url: '/buscar',
            data,
            success: function (response) {
                document.querySelector('#table').innerHTML = '';
                try{
                    document.querySelector('#pag').classList.add('hidden'); //remove tab de paginacao
                } catch (err) {}
                document.querySelector('#table').innerHTML = response;
                document.querySelector('#table').setAttribute('data-resetable', "true"); //define busca como resetavel, logo podendo ser utilizado o reset
                document.querySelector('#load').classList.add('hidden'); //remove load
                if(data['reloadJs'] == 'true') {
                    reload_js("/js/jquery.treegrid.min.js");
                }
            },
            error: function (response) {
                document.querySelector('#load').classList.add('hidden');
                $('#table').html('<tr><td class="text-lg p-3 w-full"> Ocorreu um erro ao fazer a pesquisa </td><td></td></tr>');
            }
        });
    }
}

//Esta função serve para trazer os dados iniciais após uma consulta
function reset() {
    let _token = document.querySelector('input[name="_token"]').value
    if (document.querySelector('#table').getAttribute('data-resetable') == "true") { //evita que a funcao reset seja chamada infinitamente ou sem ter sido realizada uma busca
        document.querySelector('#load').classList.remove('hidden'); //tela de load 
        data = { reset:'reset', _token,...get_consts() }
        $.ajax({
            type: 'POST',
            url: '/buscar',
            data,
            success: function (response) {
                document.querySelector('#table').innerHTML = '';
                document.querySelector('#table').innerHTML = response;
                try{
                    document.querySelector('#pag').classList.remove('hidden');
                } catch (err) {
                    console.log("nao possui paginacao")
                }
                document.querySelector('#busca_parametro').value = 'None';//reseta valores seleção
                document.querySelector('#select2-busca_parametro-container').title = '';//reseta valores seleção
                document.querySelector('#select2-busca_parametro-container').innerHTML = '';//reseta valores seleção
                document.querySelector('#load').classList.add('hidden');
                document.querySelector('#table').setAttribute('data-resetable', "false"); //define como nao resetavel novamente
                if(data['reloadJs'] == 'true') {
                    reload_js("/js/jquery.treegrid.min.js");
                }
            },
            error: function (response) {
                document.querySelector('#load').classList.add('hidden');
            }
        });
    }
}

//Esta função serve para capturar o evento de clique no campo de busca, tras os parametros de busca
$("#div-buscar-por").on('change', '#buscar_por', function () {
    const buscar_por = this.value;
    let _token = document.querySelector('input[name="_token"]').value
    if (buscar_por != 'None') {
        data = { buscar_por, _token, ...get_consts() };
        $.ajax({
            type: 'POST',
            url: '/option-busca',
            data,
            success: function (response) {
                document.getElementById("div-busca-parametro").classList.remove("hidden")
                document.getElementById("div-busca-parametro").innerHTML = response;
                $('#busca_parametro').select2({ dropdownParent: $('#busca_parametro_wrapper') });
            },
            error: function (response) {
            }
        });
    }
    else {
        document.getElementById("div-busca-parametro").classList.add("hidden")
        document.getElementById("div-busca-parametro").innerHTML = '';
    }
});

//esta função busca as váriaveis de acordo com a página e a contexto da consulta.
function get_consts() {
    const source = info.getAttribute('data-source');
    const page = info.getAttribute('data-page');
    let result = null;
    if (source == 'receita') {
        result =  {
            ano: info.getAttribute('data-ano'),
            mes: info.getAttribute('data-mes'),
            posicao: info.getAttribute('data-posicao'),
            reloadJs: info.getAttribute('data-reloadJs'),
            source,
            page,
        }
    }
    else if (source == 'despesa') {
        result =  {
            fase: info.getAttribute('fase'),
            ano: info.getAttribute('data-ano'),
            mes: info.getAttribute('data-mes'),
            posicao: info.getAttribute('data-posicao'),
            classificacao: info.getAttribute('data-classificacao'),
            reloadJs: info.getAttribute('data-reloadJs'),
            source,
            page,
        }
    }
    else if (source == 'ordem_cronologica') {
        result =  {
            orgao: document.querySelector('#orgao').value,
            fonte: document.querySelector('#fonte').value,
            mes: document.querySelector('#mes').value,
            ano: document.querySelector('#ano').value,
            reloadJs: info.getAttribute('data-reloadJs'),
            source,
        }
    }

    else if (source == 'compras_nfe') {
        result =  {
            mes: document.querySelector('#mes').value,
            ano: document.querySelector('#ano').value,
            reloadJs: info.getAttribute('data-reloadJs'),
            source,
        }
    }

    else if (source == 'compras_servicos') {
        result =  {
            reloadJs: info.getAttribute('data-reloadJs'),
            source,
        }
    }

    else if (source == 'rn_mais_vacina') {
        result =  {
            reloadJs: info.getAttribute('data-reloadJs'),
            source,
        }
    }
    return result;
}
// transformar caixa de busca em select2
$(document).ready(() => {
    $('#buscar_por').val('None');
    $('#buscar_por').select2({ dropdownParent: $('#div-buscar-por') });
})