
//Esta função observa o evento de click, e caso o click nao seja dentro de um popover ele fecha o popover que estiver aberto (clickaway)
$(window).click(function (event) {
  var ultimo = event.target.closest('[id]').id
  if ($('*[data-pop="aberto"]').length != 0) {
    if (event.target.closest('*[data-pop="aberto"]')) {
      for (var i = 0; i < $('*[data-pop="aberto"]').length; i++) {
        var id = $('*[data-pop="aberto"]')[i].id
        if (id != ultimo) {
          if (!ultimo.includes(id)) {
            pop(id)
          }
        }
      }
    }
    else {
      for (var i = 0; i < $('*[data-pop="aberto"]').length; i++) {
        var id = $('*[data-pop="aberto"]')[i].id
        if (id != ultimo)
          pop(id)
      }
    }
  }
});

//criar popover e controla o popover existente
// id = id do elemento linha da tabela
// receita = Valor da receita arrecadada
// arrayinfo = objeto que contem a "identidade" da linha.
// olho = elemento html do olho
function popOver(id, receita, arrayinfo, olho) {
  if (sessionStorage.getItem("pop") != "1") {
    sessionStorage.setItem("pop", 1);
    olho[0].classList.add("animate-spin");
    var nulo = true;
    var mes = document.getElementById("info").getAttribute("data-mes");
    var ano = document.getElementById("info").getAttribute("data-ano");
    var posicao = document.getElementById("info").getAttribute("data-pos");
    var formatter = new Intl.NumberFormat('pt-BR', {
      style: 'currency',
      currency: 'BRL',
    });
    var _token = $('input[name="_token"]').val();
    var porcentagem = 0;
    var prev = 0;
    for(var value in arrayinfo) {
      arrayinfo[value] = encodeURIComponent(arrayinfo[value]);
    }

    if (document.getElementById(id).getAttribute("data-pop") === "fechado") {
      $.ajax({
        type: 'POST',
        url: "/popprev",
        data: { receita: receita, info: arrayinfo, ano: ano, mes: mes, posicao: posicao, _token: _token },
        success: function (numero) {
          prev = numero;
      
          //caso retorne 0 ou algo diferente (string nula por exemplo)
          if (prev == '0' || prev == 0) {
            nulo = true;
          }
          else {
            nulo = false;
          }
          if (numero != 0) {
            porcentagem = ((parseFloat(receita) * 100) / parseFloat(numero)).toFixed(2)
          }
          $.ajax({
            type: 'POST',
            url: "/pop",
            data: { valor: formatter.format(prev), nulo: nulo, porcentagem: porcentagem, id: id, _token: _token },
            success: function (data) {
              olho[0].classList.remove("animate-spin");
              document.getElementById(id).innerHTML += data;
              document.getElementById(id).setAttribute("data-pop", "aberto")
              document.getElementById("pop" + id).classList.remove("hidden")
              document.getElementById("pop" + id).classList.add("active")
              sessionStorage.setItem("pop", 0);
            },
            error: function (data) {
              console.log(data);
            }
          });
        },
        error: function (numero) {
          console.log(numero);
        }
      });

    }
    else {
      olho[0].classList.remove("animate-spin")
      sessionStorage.setItem("pop", 0);
      pop(id);
    }
  }
}
//fechar e abrir popover
// id = id do elemento linha da tabela
function pop(id) {
  if (document.getElementById(id).getAttribute("data-pop") === "escondido") {
    document.getElementById("pop" + id).classList.remove("hidden");
    document.getElementById("pop" + id).classList.add("active");
    document.getElementById(id).setAttribute("data-pop", "aberto");
  }
  else if (document.getElementById(id).getAttribute("data-pop") === "aberto") {
    document.getElementById("pop" + id).classList.add("hidden");
    document.getElementById("pop" + id).classList.remove("active");
    document.getElementById(id).setAttribute("data-pop", "escondido");
  }
}