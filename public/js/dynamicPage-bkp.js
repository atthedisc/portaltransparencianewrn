//Esta função busca as váriaveis de acordo com a página e a contexto da consulta.
function get_consts() {
    const source = info.getAttribute('data-source');
    const page = info.getAttribute('data-page');
    let result = null;
    if (source == 'receita') {
        result =  {
            ano: info.getAttribute('data-ano'),
            mes: info.getAttribute('data-mes'),
            posicao: info.getAttribute('data-posicao'),
            reloadJs: info.getAttribute('data-reloadJs'),
            source,
            page,
        }
    }
    else if (source == 'despesa') {
        result =  {
            fase: info.getAttribute('fase'),
            ano: info.getAttribute('data-ano'),
            mes: info.getAttribute('data-mes'),
            posicao: info.getAttribute('data-posicao'),
            classificacao: info.getAttribute('data-classificacao'),
            reloadJs: info.getAttribute('data-reloadJs'),
            source,
            page,
        }
    }
    return result;
}

//esta função gerencia os eventos de click que sejam efetuados na tag responsável pela abertura do treegrid.
$('#table').on('click','span.treegrid-expander',function (event) { // Executa caso o clique tenha sido no icone de expandir
    const first = event.target.parentNode;
    const row = first.parentNode;
    const header = row.nextElementSibling;
    const fetch = row.getAttribute('data-fetch');
    const key = row.getAttribute('data-key');
    const codigo = row.getAttribute('data-codigo');
    let result = null;
    if(fetch == 'false') { //Caso a linha nao tenha sido preenchida ainda o bloco de codigo pode ser executado (Atributo data-fetch da row)
        result = get_consts()
       // Codigo é o atributo data-codigo que tem em cada elemento primario da Row
       if(codigo) {
          $("#load").removeClass('hidden');
          const dados = JSON.parse(codigo);
          const _token = document.querySelector('input[name="_token"]').value;
          console.log(dados);
          //Request para trazer os dados
          $.ajax({
                type: 'POST',
                url: '/dynamic-page',
                data: { dados, key, dynamic:'true', _token, ...result },
                success: function (response) {
                   $(response).insertAfter( header )
                   row.setAttribute('data-fetch','true')
                   reload_js("/js/jquery.treegrid.min.js")
                   $(row).treegrid("toggle");
                   $("#load").addClass('hidden');
                },
                error: function (response) {
                   $("#load").addClass('hidden');
                }
          });
       }
    }
 });