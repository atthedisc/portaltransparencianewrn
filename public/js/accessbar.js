/* FOCO */
$('a[id^="/"]').on('click', function (event) {
  event.preventDefault();
  $('html, body').animate({
    scrollTop: $($.attr(this, 'href')).offset().top
  }, 500);
  setTimeout(100);
  document.getElementById(this.id.substring(1, this.id.length)).focus();
});

/* CONTRASTE */
link = document.getElementById("linkcontraste");
imagem = document.getElementById("logoimg");
rnunido = document.getElementById("rnunido");
rnunidocamp = document.getElementById("img2");
var ligado = sessionStorage.getItem("contraste");
var cookie = document.getElementById("cookies");

//Para ver inicializações consulte "Onload.js"

function contraste() {
  var url = document.URL.replace("http://", "");
  ligado = sessionStorage.getItem("contraste");
  if (ligado === "false") {
    link.href = "/css/contraste.css";
    if (imagem.name == "transparencia") {
      imagem.src = "/images/logo/Transparencia-contraste.png";
    }

    else if (imagem.name == "covid") {
      imagem.src = "/images/logo/Covid-contraste.png";
      if (url.substring(url.indexOf("/"), url.length) == "/covid") {
        rnunido.src = "/images/logo/rnmaisunido_contraste.png";
      }
      if (url.substring(url.indexOf("/"), url.length).includes("/campanhas")) {
        rnunidocamp.src = "/images/logo/rnmaisunido_contraste.png";
      }

    }

    sessionStorage.setItem("contraste", true);
  }
  else {
    link.href = "";
    if (imagem.name == "transparencia") {
      imagem.src = "/images/logo/logo-portal-transparencia-60x60.png";
    }
    else if (imagem.name == "covid") {
      imagem.src = "/images/logo/virusnovo1-ajustada.png";
      if (url.substring(url.indexOf("/"), url.length) == "/covid") {
        rnunido.src = "/images/logo/rnmaisunido_colorido.png";
      }
      if (url.substring(url.indexOf("/"), url.length).includes("/campanhas")) {
        rnunidocamp.src = "/images/logo/rnmaisunido_colorido.png";
      }
    }
    sessionStorage.setItem("contraste", false);
  }
}

/* FONTE */
const aum = 0.0625;
var at = sessionStorage.getItem("aumento");
const textos = document.getElementsByName("texto");
function fonte(event) {
  //Quando o botão apertado for o A+ e o aumento for menor que 0.375, as fontes aumentam
  if ((event == "A+") && (parseFloat(sessionStorage.getItem("aumento")) <= 0.375)) {
    textos.forEach(texto => {
      tamanho = parseFloat(texto.style.fontSize) + aum;
      //parsefloat remove a unidade de escala (rem, px...)
      texto.style.fontSize = (tamanho) + "rem";
    });
    sessionStorage.setItem("aumento", parseFloat(sessionStorage.getItem("aumento")) + aum);
  }

  //Quando o botão apertado for o A- e o aumento for maior que 0, as fontes diminuem
  else if ((event == "A-") && (parseFloat(sessionStorage.getItem("aumento")) > 0)) {
    textos.forEach(texto => {
      tamanho = parseFloat(texto.style.fontSize) - aum;
      //parsefloat remove a unidade de escala (rem, px...)
      texto.style.fontSize = (tamanho) + "rem";
    });
    sessionStorage.setItem("aumento", parseFloat(sessionStorage.getItem("aumento")) - aum);
  }
  //Quando o botão apertado for o A e o aumento for maior que 0, as fontes resetam
  else if ((event == "A") && (parseFloat(sessionStorage.getItem("aumento")) > 0)) {
    textos.forEach(texto => {
      tamanho = parseFloat(texto.style.fontSize) - parseFloat(sessionStorage.getItem("aumento"));
      //parsefloat remove a unidade de escala (rem, px...)
      texto.style.fontSize = (tamanho) + "rem";
    });
    sessionStorage.setItem("aumento", 0);
  }
}

function fontereload() {
  //Quando o botão apertado for o A+ e o aumento for menor que 0.375, as fontes aumentam
  textos.forEach(texto => {
    tamanho = parseFloat(texto.style.fontSize) + parseFloat(sessionStorage.getItem("aumento"));
    //parsefloat remove a unidade de escala (rem, px...)
    texto.style.fontSize = (tamanho) + "rem";
  });
}