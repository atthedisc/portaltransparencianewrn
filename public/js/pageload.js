var page = document.getElementById('pagewrapper')
var load = document.getElementById('load')
var isChromium = window.chrome;
var winNav = window.navigator;
var vendorName = winNav.vendor;
var isOpera = typeof window.opr !== "undefined";
var isIEedge = winNav.userAgent.indexOf("Edg") > -1;
var isIOSChrome = winNav.userAgent.match("CriOS");

if (isIOSChrome) {
   // is Google Chrome on IOS
} else if(
  isChromium !== null &&
  typeof isChromium !== "undefined" &&
  vendorName === "Google Inc." &&
  isOpera === false &&
  isIEedge === false
) {
   // is Google Chrome
} else { 
   // not Google Chrome 
   $(window).on('beforeunload', function(){
    load.classList.remove("hidden");
    page.classList.add("hidden");
    window.scrollTo(0, 0);
});
}

  //desativa
  // Array.prototype.forEach.call(document.getElementById('export'), function(anchor) {
  //   anchor.addEventListener("click", unhookBeforeUnload);
  // });