//Ao carregar a pagina checa algumas coisas
$(window).on('load', function(){
  //ativa tela de loading
  let stateCheck = setInterval(() => {
    if (document.readyState === 'complete') {
      clearInterval(stateCheck);
      $(load).addClass("hidden");
      $(page).removeClass("hidden");
    }
  }, 100);
  //Conteudo do if roda somente no primeiro loading da pagina
  if (sessionStorage.getItem("load") === null) {
    sessionStorage.setItem("contraste", false);
    sessionStorage.setItem("aumento", 0); // FONTE
    sessionStorage.setItem("load", true);
  }
  //cookie
  if (localStorage.getItem("cookie") === null) {
    localStorage.setItem("cookie", true);
    cookie.classList.remove("hidden");
  } else if (localStorage.getItem("cookie") === "true") {
    cookie.classList.remove("hidden");
  } else {
    concordo();
  }
  //cookie

  //roda sempre que a pagina carrega
  if (sessionStorage.getItem("contraste") === "true") {
    sessionStorage.setItem("contraste", false);
    contraste();
  }
  //roda sempre que a pagina carrega FONTE
  if (parseFloat(sessionStorage.getItem("aumento")) > 0) {
    fontereload();
  }

  //pop receitas
  if (window.location.pathname === "/receita-prevista") {
    sessionStorage.setItem("pop", 0);
  }

  console.log("local", localStorage);
  console.log("session", sessionStorage);
});