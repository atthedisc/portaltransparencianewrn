function toggle(btn, id) {
   const theRows = document.querySelectorAll(id);
   const theButton = document.getElementById(btn); // Obter botão que acionou a function

   const plus = document.getElementById('plus' + btn); // obter icones dos botões
   const minus = document.getElementById('minus' + btn); // obter icones dos botões

   // se o botão não estiver expandido...
   if (theButton.getAttribute("aria-expanded") == "false") {
      for (var i = 0; i < theRows.length; i++) {
         theRows[i].classList.add("shown");
         theRows[i].classList.remove("hidden");
      }
      theButton.setAttribute("aria-expanded", "true"); // definir o botão para expandido 
      plus.classList.add("hidden"); // aparcer icone de "-" e sumir o de "+"
      minus.classList.remove("hidden"); // aparcer icone de "-" e sumir o de "+"

      // Caso contrário o botão não é expandido...
   } else {
      // Percorrer as linhas e ocultá-las
      for (var i = 0; i < theRows.length; i++) {
         theRows[i].classList.add("hidden");
         theRows[i].classList.remove("shown");
      }

      theButton.setAttribute("aria-expanded", "false"); // definir o botão para recolhido

      plus.classList.remove("hidden"); // aparecer icone de "+" e sumir o de "-"
      minus.classList.add("hidden"); // aparecer icone de "+" e sumir o de "-"

      // condição para pegar o pai de todos, para fazer a logica de fechar todos ao fechar o pai
      // if (!theButton.id.includes('btnchildren') && !theButton.id.includes('btngrandchildren')) {
      //    alert('pegar apenas o pai de todos')
      // }
   }
}