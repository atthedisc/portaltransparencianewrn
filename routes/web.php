<?php

use App\Http\Controllers\BuscaController;
// use App\Http\Controllers\CampanhasController;
use App\Http\Controllers\ComprasPublicasNFeController;
use App\Http\Controllers\Covid\CompraseServicosController;
use App\Http\Controllers\Covid\RnChegaJunto\BeneficiariosController;
use App\Http\Controllers\Covid\RnChegaJunto\DoacoesRealizadasController;
use App\Http\Controllers\Covid\RnChegaJunto\DoacoesRecebidasController;
use App\Http\Controllers\Covid\RnMaisProtegidoController;
use App\Http\Controllers\Despesas\BloqueiosJudiciaisController;
use App\Http\Controllers\Despesas\CarregamentoDinamico;
use App\Http\Controllers\Despesas\GastosDiretosController;
use App\Http\Controllers\Despesas\RepasseOutrosPoderesController;
use App\Http\Controllers\GlossarioController;
use App\Http\Controllers\ReceitasController;
use App\Http\Controllers\RnChegaJuntoNoCombateAFome\BeneficiadosController;
use App\Http\Controllers\RnChegaJuntoNoCombateAFome\DoacoesDoGovernoController;
use App\Http\Controllers\RnChegaJuntoNoCombateAFome\DoadoresController;
use App\Http\Controllers\RnmaisvacinaController;
use App\Http\Controllers\TodosTiposGastosController;
use App\Http\Controllers\WelcomeController;
use App\Http\Controllers\CookieController;
use App\Http\Controllers\OrdemCronologica\OrdemCronologicaController;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
|                          Portal da Transparência
|--------------------------------------------------------------------------
*/

Route::get('/', [WelcomeController::class, 'index'])->name('Início');
/*
|--------------------------------------------------------------------------
| Contas Públicas
|--------------------------------------------------------------------------
*/
Route::get('/notas-tecnicas', function () {
   return view('contas-publicas/normatizacao.notas-tecnicas');
})->name('notas-tecnicas');

Route::get('/orientacoes-tecnicas', function () {
   return view('contas-publicas/normatizacao.orientacoes-tecnicas');
})->name('orientacoes-tecnicas');

/*
|--------------------------------------------------------------------------
| Contabilidade Facil
|--------------------------------------------------------------------------
*/
Route::get('/oque-e-contabilidade-facil', function () {
   return view('contabilidade-facil.oque-e-contabilidade-facil');
})->name('oque-e-contabilidade-facil');

Route::get('/contas-bimestrais', function () {
   return view('contabilidade-facil/rn-contabil.contas-bimestrais');
})->name('contas-bimestrais');

Route::get('/contas-quadrimestrais', function () {
   return view('contabilidade-facil/rn-contabil.contas-quadrimestrais');
})->name('contas-quadrimestrais');

Route::get('/rn-em-foco', function () {
   return view('contabilidade-facil.rn-em-foco');
})->name('rn-em-foco');

Route::get('/dicas-execucao-orcamentaria', function () {
   return view('contabilidade-facil.dicas-execucao-orcamentaria');
})->name('dicas-execucao-orcamentaria');

/*
|--------------------------------------------------------------------------
| RN Mais Vacina
|--------------------------------------------------------------------------
*/
Route::get('/rn-mais-vacina/{status}', [RnmaisvacinaController::class, 'index'])->name('rn-mais-vacina');
Route::post('/rn-mais-vacina/exportxls', [RnmaisvacinaController::class, 'exportxls'])->name('export-xls-rn-mais-vacina');
Route::post('/rn-mais-vacina/exportpdf', [RnmaisvacinaController::class, 'exportpdf'])->name('export-pdf-rn-mais-vacina');

/*
|--------------------------------------------------------------------------
| RN Chega Junto no Combate à fome
|--------------------------------------------------------------------------
*/
Route::any('/rn-chega-junto-no-combate-a-fome-doacoes', [DoacoesDoGovernoController::class, 'index'])->name('doacoes');

Route::any('/rn-chega-junto-no-combate-a-fome-beneficiados', [BeneficiadosController::class, 'index'])->name('beneficiados');

Route::any('/rn-chega-junto-no-combate-a-fome-doadores', [DoadoresController::class, 'index'])->name('doadores');

Route::get('/rn-chega-junto-no-combate-a-fome-registros', function () {
   return view('rn-chega-junto-no-combate-a-fome.registros-de-entregas');
})->name('registros');

/*
|--------------------------------------------------------------------------
| Ordem Cronologica
|--------------------------------------------------------------------------
*/
Route::any('/ordem-cronologica', [OrdemCronologicaController::class, 'index'])->name('ordem-cronologica');
Route::post('/ordem-cronologica/exportxls/{mes}/{ano}/{orgao}/{fonte}', [OrdemCronologicaController::class, 'exportxls'])->name('export-xls-ordem-cronologica');
Route::post('/ordem-cronologica/exportpdf/{mes}/{ano}/{orgao}/{fonte}', [OrdemCronologicaController::class, 'exportpdf'])->name('export-pdf-ordem-cronologica');

/*
|----------------------------------------------------------"----------------
| Contratos
|--------------------------------------------------------------------------
*/
Route::any('/contratos', ContratosController::class)->name('contratos');

/*
|--------------------------------------------------------------------------
| Orgãos do Governo
|--------------------------------------------------------------------------
*/
Route::get('/orgaos-do-governo', function () {
   return view('orgaos-do-governo.index');
})->name('orgaos-do-governo');

Route::get('/cartasdeservico-set', function () {
   return view('orgaos-do-governo.cartasdeservico-set');
})->name('cartasdeservico-set');

/*
|--------------------------------------------------------------------------
| Compras Públicas - NFe
|--------------------------------------------------------------------------
*/
Route::any('/compras-publicas-nfe', [ComprasPublicasNFeController::class, 'index'])->name('compras-publicas-nfe');
Route::post('/compras-publicas-nfe/exportxls/{ano}/{mes}', [ComprasPublicasNFeController::class, 'exportxls'])->name('export-xls-compras-publicas-nfe');
Route::post('/compras-publicas-nfe/exportpdf/{ano}/{mes}', [ComprasPublicasNFeController::class, 'exportpdf'])->name('export-pdf-compras-publicas-nfe');

/*
|--------------------------------------------------------------------------
| Todos tipos de gastos
|--------------------------------------------------------------------------
*/
Route::any('/todos-tipos-gastos', [TodosTiposGastosController::class, 'index'])->name('todos-tipos-gastos');
Route::any('/gastos-diretos/{funcao}/{ano}', [TodosTiposGastosController::class, 'gastosporfuncao'])->name('gastos-diretos-tipo-funcao');

/*
|--------------------------------------------------------------------------
| Despesas
|--------------------------------------------------------------------------
*/
Route::get('/despesas', Despesas\DespesasController::class)->name('despesas');

/*
|--------------------------------------------------------------------------
| Despesas -> Gastos Diretos
|--------------------------------------------------------------------------
*/

Route::any('/gastos-diretos', [GastosDiretosController::class, 'index'])->name('gastos-diretos');
Route::get('/gastos-diretos-detalhada/{request}', [GastosDiretosController::class, 'detalhamento'])->name('gastos-diretos-detalhada');
Route::get('/detalhes-documento/{request}', [GastosDiretosController::class, 'detalhes'])->name('detalhes-documento');
Route::post('/gastos-diretos/exportxls/{mes}/{ano}/{posicao}/{classificacao}/{fase?}', [GastosDiretosController::class, 'exportxls'])->name('export-xls-gastos-diretos');
Route::post('/gastos-diretos/exportpdf/{mes}/{ano}/{posicao}/{classificacao}/{fase?}', [GastosDiretosController::class, 'exportpdf'])->name('export-pdf-gastos-diretos');

/*
|--------------------------------------------------------------------------
| Despesas -> Repasse Outros Poderes
|--------------------------------------------------------------------------
*/
Route::any('/repasse-outros-poderes', [RepasseOutrosPoderesController::class, 'index'])->name('repasse-outros-poderes');
Route::post('/repasse-outros-poderes/exportxls/{mes}/{ano}/{posicao}', [RepasseOutrosPoderesController::class, 'exportxls'])->name('export-xls-repasse-outros-poderes');
Route::post('/repasse-outros-poderes/exportpdf/{mes}/{ano}/{posicao}', [RepasseOutrosPoderesController::class, 'exportpdf'])->name('export-pdf-repasse-outros-poderes');

/*
|--------------------------------------------------------------------------
| Despesas -> Bloqueios Judiciais
|--------------------------------------------------------------------------
*/
Route::any('/bloqueios-judiciais', [BloqueiosJudiciaisController::class, 'index'])->name('bloqueios-judiciais');
Route::post('/bloqueios-judiciais/exportxls/{mes}/{ano}', [BloqueiosJudiciaisController::class, 'exportxls'])->name('export-xls-bloqueios-judiciais');
Route::post('/bloqueios-judiciais/exportpdf/{mes}/{ano}', [BloqueiosJudiciaisController::class, 'exportpdf'])->name('export-pdf-bloqueios-judiciais');

/*
|--------------------------------------------------------------------------
| Receitas
|--------------------------------------------------------------------------
*/
Route::any('/receita', [ReceitasController::class, 'index'])->name('receita');
Route::any('/receita-prevista', [ReceitasController::class, 'receitaPrevista'])->name('receita-prevista');
Route::post('/receita-prevista/exportxls/{mes}/{ano}/{posicao}/{classificacao}', [ReceitasController::class, 'exportxls'])->name('export-xls-receita-prevista');
Route::post('/receita-prevista/exportpdf/{mes}/{ano}/{posicao}/{classificacao}', [ReceitasController::class, 'exportpdf'])->name('export-pdf-receita-prevista');

/*
|--------------------------------------------------------------------------
| Popover
|--------------------------------------------------------------------------
*/
Route::post('/pop', [ReceitasController::class, 'popover'])->name('pop');
Route::post('/popprev', [ReceitasController::class, 'vlrprev'])->name('popprev');

/*
|--------------------------------------------------------------------------
| Perguntas Frequentes
|--------------------------------------------------------------------------
*/
Route::get('/perguntas-frequentes', function () {
   return view('perguntas-frequentes.index');
})->name('perguntas-frequentes');

/*
|--------------------------------------------------------------------------
| Sobre o Portal
|--------------------------------------------------------------------------
*/
Route::get('/sobre-o-portal', function () {
   return view('sobre-o-portal.index');
})->name('sobre-o-portal');

/*
|--------------------------------------------------------------------------
| Glossário
|--------------------------------------------------------------------------
*/
Route::any('/glossario', [GlossarioController::class, 'index'])->name('glossario');
Route::post('/cardglossario', [GlossarioController::class, 'card'])->name('cardglossario');

/*
|--------------------------------------------------------------------------
| Campanhas
|--------------------------------------------------------------------------
*/
// Route::get('/campanhas/{id}', [CampanhasController::class, 'index'])->name('campanhas');

/*
|--------------------------------------------------------------------------
| Empresas Inidôneas
|--------------------------------------------------------------------------
*/
Route::get('/empresas-inidoneas', function () {
   return view('empresas-inidoneas.index');
})->name('empresas-inidoneas');

/*
|--------------------------------------------------------------------------
| Plano Estrategico
|--------------------------------------------------------------------------
*/
Route::get('/plano-estrategico', function () {
   return view('plano-estrategico.index');
})->name('plano-estrategico');

/*
|--------------------------------------------------------------------------
|Obras e Execução
|--------------------------------------------------------------------------
*/
Route::get('/obras-e-execucao', function () {
   return view('obras-e-execucao.index');
})->name('obras-e-execucao');

/*
|--------------------------------------------------------------------------
| Terceirizados
|--------------------------------------------------------------------------
*/
Route::get('/terceirizados', function () {
   return view('terceirizados.index');
})->name('terceirizados');

/*
|--------------------------------------------------------------------------
| Defensores Dativos
|--------------------------------------------------------------------------
*/
Route::get('/defensores-dativos', function () {
   return view('despesas/defensores-dativos.index');
})->name('defensores-dativos');

/*
|--------------------------------------------------------------------------
| Conselhos Estaduais
|--------------------------------------------------------------------------
*/
Route::get('/conselhos-estaduais', function () {
   return view('conselhos-estaduais.index');
})->name('conselhos-estaduais');

/*
|--------------------------------------------------------------------------
| Ouvidoria
|--------------------------------------------------------------------------
*/
Route::get('/ouvidoria', function () {
   return view('ouvidoria.index');
})->name('ouvidoria');

/*
|--------------------------------------------------------------------------
| Renúncia de Receita
|--------------------------------------------------------------------------
*/
Route::get('/renuncia-receita', function () {
   return view('renuncia-receita.index');
})->name('renuncia-receita');

/*
|--------------------------------------------------------------------------
| Emendas Parlamentares
|--------------------------------------------------------------------------
*/
Route::get('/emendas-parlamentares', function () {
   return view('emendas-parlamentares.index');
})->name('emendas-parlamentares');


/*
|--------------------------------------------------------------------------
| Cadastro Autoridades
|--------------------------------------------------------------------------
*/
Route::get('/cadastro-autoridades', function () {
   return view('cadastro-autoridades.index');
})->name('cadastro-autoridades');

/*
|--------------------------------------------------------------------------
| Notas de Esclarecimento
|--------------------------------------------------------------------------
*/
Route::get('/notas-esclarecimento', function () {
   return view('notas-esclarecimento.index');
})->name('notas-esclarecimento');

Route::get('/nota-09-01-24', function () {
   return view('notas-esclarecimento.nota-09-01-24');
})->name('nota-09-01-24');


/*
|--------------------------------------------------------------------------
| Saúde
|--------------------------------------------------------------------------
*/
Route::get('/saude', function () {
   return view('saude.index');
})->name('saude');

/*
|--------------------------------------------------------------------------
| Contas Governo
|--------------------------------------------------------------------------
*/

Route::get('/contas-governo', function () {
   return view('contas-governo.index');
})->name('contas-governo');

/*
|--------------------------------------------------------------------------
| Fale conosco
|--------------------------------------------------------------------------
*/
Route::get('/faleconosco', function () {
   return view('fale-conosco.index');
})->name('faleconosco');

/*
|--------------------------------------------------------------------------
| Mapa do Site
|--------------------------------------------------------------------------
*/
Route::get('/mapa-do-site', function () {
   return view('mapa-do-site.index');
})->name('mapadosite');

/*
|--------------------------------------------------------------------------
| Serviços Control
|--------------------------------------------------------------------------
*/
Route::get('/control', function () {
   return view('control.index');
})->name('control');

/*
|--------------------------------------------------------------------------
|                             Portal COVID-19
|--------------------------------------------------------------------------
*/
Route::get('/covid', function () {
   return view('covid/welcome');
})->name('Covid');

/*
|--------------------------------------------------------------------------
| Despesas - Compras e Serviços
|--------------------------------------------------------------------------
*/
Route::get('/compras-e-servicos', [CompraseServicosController::class, 'index'])->name('compras-e-servicos');
Route::post('/compras-e-servicos/exportxls', [CompraseservicosController::class, 'exportxls'])->name('export-xls-compras-e-servicos');
Route::post('/compras-e-servicos/exportpdf', [CompraseservicosController::class, 'exportpdf'])->name('export-pdf-compras-e-servicos');

/*
|--------------------------------------------------------------------------
| Legislação
|--------------------------------------------------------------------------
*/
Route::get('/instrucao-normativa', function () {
   return view('covid/legislacao/instrucao-normativa');
})->name('instrucao-normativa');

/*
|--------------------------------------------------------------------------
| RN Mais Protegido
|--------------------------------------------------------------------------
*/
Route::any('/rn-mais-protegido-doacoes-recebidas', [RnMaisProtegidoController::class, 'index'])->name('rn-mais-protegido-doacoes-recebidas');

/*
|--------------------------------------------------------------------------
| RN Mais Unido
|--------------------------------------------------------------------------
*/
Route::get('/rn-mais-unido-doacoes-recebidas', Covid\RnMaisUnido\DoacoesRecebidasController::class)->name('doacoesrecebidas');

Route::get('/rn-mais-unido-beneficiarios', Covid\RnMaisUnido\BeneficiariosController::class)->name('beneficiarios');

/*
|--------------------------------------------------------------------------
| RN Chega Junto
|--------------------------------------------------------------------------
*/

Route::any('/rn-chega-junto-doacoes-realizadas', [DoacoesRealizadasController::class, 'index'])->name('rnchegajunto-doacoesrealizadas');

Route::any('/rn-chega-junto-doacoes-recebidas', [DoacoesRecebidasController::class, 'index'])->name('rnchegajunto-doacoesrecebidas');

Route::any('/rn-chega-junto-beneficiarios', [BeneficiariosController::class, 'index'])->name('rnchegajunto-beneficiarios');

/*
|--------------------------------------------------------------------------
| Busca
|--------------------------------------------------------------------------
*/
// Busca Geral
Route::post('/option-busca', [BuscaController::class, 'option_busca'])->name('option-busca');
Route::post('/buscar', [BuscaController::class, 'buscar'])->name('buscar');

Route::post('/dynamic-page', [CarregamentoDinamico::class, 'index'])->name('dynamic-page');

/*
|--------------------------------------------------------------------------
| Redirect docs Transparência Covid-19
|--------------------------------------------------------------------------
*/

Route::prefix('Documentos')->group(function () {

   // Pedidos de Informação (e-SIC Pedidos)
   Route::prefix('e-SIC')->group(function () {
      Route::get('/2022', function () {
         return Redirect::to('docs/transparencia/pedidosdeinformacao/2022/e-SIC_-_Pedidos_COVID-19.pdf');
      })->name('e-SIC-2022');
      Route::get('/2021', function () {
         return Redirect::to('docs/transparencia/pedidosdeinformacao/2021/e-SIC_-_Pedidos_COVID-_2021_completo.pdf');
      })->name('e-SIC-2021');
      Route::get('/2020', function () {
         return Redirect::to('docs/transparencia/pedidosdeinformacao/2020/e-SIC - Pedidos COVID-19 - 2020.pdf');
      })->name('e-SIC-2020');
   });

   // Relatorio Estatístico da LAI
   Route::prefix('Relatorio Lai')->group(function () {
      Route::get('/2022', function () {
         return Redirect::to('docs/transparencia/relatorioestatisticoLAI/2022/Relatório_e-Sic_-_Covid19_-_2022.pdf');
      })->name('Relatorio Lai-2022');
      Route::get('/2021', function () {
         return Redirect::to('docs/transparencia/relatorioestatisticoLAI/2021/Relatório_e-Sic_-_Covid19_-_Novembro_2021.pdf');
      })->name('Relatorio Lai-2021');
      Route::get('/2020', function () {
         return Redirect::to('docs/transparencia/relatorioestatisticoLAI/2020/Relatório e-Sic - Covid19 - Dezembro 2020.pdf');
      })->name('Relatorio Lai-2020');
   });

   // Relatorio de Manifestações da Ouvidoria
   Route::prefix('Relatorio Ouv')->group(function () {
      Route::get('/2022', function () {
         return Redirect::to('docs/transparencia/relatoriomanifestacoesouvidoria/2022/Relatório_e-Ouv_-_Covid19_-_2022.pdf');
      })->name('Relatorio Ouv-2022');
      Route::get('/2021', function () {
         return Redirect::to('docs/transparencia/relatoriomanifestacoesouvidoria/2021/Relatório e-Ouv - Covid19 - 2021.pdf');
      })->name('Relatorio Ouv-2021');
      Route::get('/2020', function () {
         return Redirect::to('docs/transparencia/relatoriomanifestacoesouvidoria/2020/Relatório e-Ouv - Covid19 - Dezembro 2020.pdf');
      })->name('Relatorio Ouv-2020');
   });

   
});

/*
|--------------------------------------------------------------------------
| Redirect pages Transparência Covid-19
|--------------------------------------------------------------------------
*/

//E-sic
Route::get('/e-sic', function () {
   return Redirect::to('http://www.sic.rn.gov.br/');
})->name('solicitacao-informacoes');

//
Route::get('/servicos-ao-cidadao', function () {
   return Redirect::to('http://www.cidadao.rn.gov.br/');
})->name('servicos-ao-cidadao');

//canal-de-denuncia
Route::get('/canal-de-denuncia', function () {
   return Redirect::to('https://falabr.cgu.gov.br/Login/Identificacao/Identificacao.aspx?idFormulario=4&tipo=1&ReturnUrl=%2fpublico%2fManifestacao%2fRegistrarManifestacao.aspx%3fidFormulario%3d4%26tipo%3d1%26origem%3didp%26modo%3d');
})->name('canal-de-denuncia');

//contratos
Route::get('/contratos-publicos', function () {
   return Redirect::to('https://sipac.rn.gov.br/public/ContratosPublic.do?aba=p-contratos&acao=156');
})->name('contratos-publicos');

//convenios-concedidos
Route::get('/convenios-concedidos', function () {
   return Redirect::to('http://201.76.150.19:8080/conveniorn/conveniorelsite.aspx');
})->name('convenios-concedidos');

//portal-de-compras
Route::get('/portal-de-compras', function () {
   return Redirect::to('http://compras.rn.gov.br/');
})->name('portal-de-compras');

//transferencia-para-municipios
Route::get('/transferencia-para-municipios', function () {
   return Redirect::to('https://www.seplan.rn.gov.br/documentos/Contas%20P%c3%bablicas/');
})->name('transferencia-para-municipios');

//transparencia-ambiental
Route::get('/transparencia-ambiental', function () {
   return Redirect::to('https://sistemas.idema.rn.gov.br/servicos/');
})->name('transparencia-ambiental');


//patrimonio-imobiliario
Route::get('/patrimonio-imobiliario', function () {
   return Redirect::to('http://sipat.rn.gov.br/');
})->name('patrimonio-imobiliario');

//transferencias-recebidas
Route::get('/transferencias-recebidas', function () {
   return Redirect::to('https://portaldatransparencia.gov.br/convenios/consulta?paginacaoSimples=true&tamanhoPagina=&offset=&direcaoOrdenacao=asc&tipoConvenente=8&uf=RN&colunasSelecionadas=linkDetalhamento%2CnumeroConvenio%2Cuf%2CmunicipioConvenente%2Csituacao%2CtipoTransferencia%2Cobjetivo%2CorgaoSuperior%2Corgao%2Cconcedente%2Cconvenente%2CdataInicioVigencia%2CdataFimVigencia%2CvalorLiberado%2CvalorCelebrado');
})->name('transferencias-recebidas');

//transparencia-publica
Route::get('/transparencia-publica', function () {
   return Redirect::to('https://radardatransparencia.atricon.org.br/radar-da-transparencia-publica.html');
})->name('transparencia-publica');

//divida-ativa
Route::get('/divida-ativa', function () {
   return Redirect::to('http://nucleo.pge.rn.gov.br/');
})->name('divida-ativa');

//educacao
Route::get('/educacao', function () {
   return Redirect::to('http://www.educacao.rn.gov.br/Conteudo.asp?TRAN=ITEM&TARG=172320&ACT=&PAGE=0&PARM=&LBL=Documentos');
})->name('educacao');


//consulta publica
Route::get('/consulta-publica', function () {
   return Redirect::to('https://docs.google.com/forms/d/e/1FAIpQLSd6Lj0wezYe3IhHMfl_u4nkJWxuVVb5a8m28EmNCH9TgxbvoA/viewform');
})->name('consulta-publica');

// Rota para os cookies para o LGPD

Route::get('/lgpd', function () {
   return view('lgpd.cookie');
})->name('lgpd');
Route::get('/cookie/get/{name}', [CookieController::class, 'getCookie'])->name('getSpecificCookie');
Route::get('/cookie/list', [CookieController::class, 'listCookies'])->name('listCookies');
Route::delete('/cookie/deleteall', [CookieController::class, 'deleteAllCookies'])->name('rejeitarCookies');

// Search

Route::get('/search', [BuscaController::class, 'search'])->name('search');
