<p align="center"><a href="#"><img src="public/images/logo/logo-portal-transparencia.png" width="400"></a></p>

## Portal da Transparência do Rio Grande do Norte

Repositório dedicado ao armazenamento do código do novo Portal da Transparência do Rio Grande do Norte, para mais informações sobre as melhorias no Portal, Acesse: [Controladoria inicia melhorias no Portal da Transparência do Governo](http://control.rn.gov.br/Conteudo.asp?TRAN=ITEM&TARG=258761&ACT=&PAGE=&PARM=&LBL=NOT%CDCIA).

## Equipe de Desenvolvimento

- [Alefe Oliveira de Macedo Teixeira](https://github.com/AlefeOliveira5)
- [Ana Paula Oliveira Da Silva](https://github.com/anapaulaods)
- [Matheus Vinícius da Silva](https://github.com/matusilva)
- [João Rock Britto Santos](https://github.com/Jrockbritto)
- [Mariana Medeiros Pires](https://github.com/marianamp)

### Coordenadores
- Maria Olivia
- [Paulo Câmara de Oliveira Shioga](https://github.com/PauloControl)


### Contribuições
...

## Sobre o Portal da Transparência do RN

O Portal da Transparência do Estado do RN é coordenado pela Controladoria Geral do Estado e possibilita o acompanhamento das informações orçamentárias e financeiras do executivo estadual, permitindo ao cidadão a fiscalização, de forma eficiente e transparente da utilização dos recursos públicos, para assegurar a sua correta aplicação. O Portal também disponibiliza informações sobre os instrumentos de Planejamento, Relatórios Fiscais, Licitações, obras realizadas no Estado e outras informações de relevância para os cidadãos norteriograndenses.


## Tecnologia Utilizada

<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.


## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
