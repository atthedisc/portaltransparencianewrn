<div id="altu" class="flex justify-center fixed z-10 w-full text-gray-700 bg-white dark-mode:text-gray-200 dark-mode:bg-gray-800 shadow">
   <div id='content' class="container-fluid-covid container">
      <div x-data="{ open: false }"
         class="flex flex-col mx-auto xl:items-center justify-between xl:justify-between xl:flex-row xl:px-6 2xl:px-2">

         <div class="p-4 flex flex-row items-center justify-between">
            <a href="<?php echo e(url('/covid')); ?>">
               <img id="logoimg" class="w-2/3 md:w-full" name="covid" src="<?php echo e(asset('images/logo/virusnovo1-ajustada.png')); ?>" alt="">

               <span id="logotext"
                  class="hidden font-semibold tracking-widest text-gray-900 uppercase rounded-lg dark-mode:text-white focus:outline-none focus:shadow-outline"
                  name="texto" style="font-size:1.125rem;">
                  Transparência COVID-19
               </span>
            </a>

            <button class="xl:hidden rounded-lg focus:outline-none focus:shadow-outline" @click="open = !open">
               <svg fill="currentColor" viewBox="0 0 20 20" class="w-6 h-6">
                  <path x-show="!open" fill-rule="evenodd"
                     d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                     clip-rule="evenodd"></path>
                  <path x-show="open" fill-rule="evenodd"
                     d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                     clip-rule="evenodd"></path>
               </svg>
            </button>
         </div>

         <div class="flex-column">

            <div id="ouvi" class="flex justify-end">
               <div class="flex relative place-content-center inline-block">
                  <div class="links hidden xl:flex items-center">
                     <a aria-label="Ouvidoria"
                        href="<?php echo e(asset('https://sistema.ouvidorias.gov.br/publico/RN/manifestacao/RegistrarManifestacao')); ?>"
                        class="text-gray-500 hover:text-black px-2" target="_blank" name="texto"
                        style="font-size: 0.75rem;">
                        <img src="<?php echo e(asset('images/ouvidoria.png')); ?>" alt="">
                     </a>
                     <a aria-label="Acesso à Informação" href="<?php echo e(asset('http://www.sic.rn.gov.br/')); ?>"
                        class="text-gray-500 hover:text-black px-2" target="_blank" name="texto"
                        style="font-size: 0.75rem;">
                        <img src="<?php echo e(asset('images/acesso_a_informacao_logo.png')); ?>" alt="">
                     </a>
                  </div>
               </div>
            </div>

            <nav :class="{'flex': open, 'hidden': !open}"
               class="flex-col flex-grow justify-start pb-4 xl:items-center pl-6 xl:pb-0 hidden xl:flex xl:flex-row">

               
               <div @click.away="open = false" class="relative" x-data="{ open: false }">

                  <button @click="open = !open"
                     class="flex items-center px-2 py-2 mt-2 font-semibold md:mt-0 rounded-lg focus:shadow-outline transition duration-150 ease-in-out transform hover:-translate-y-1 hover:scale-110 hover:text-green-400"
                     name="texto" style="font-size:0.875rem;">
                     <img class="mr-1" src="<?php echo e(asset('images/icons/favicon-covid-16x16.png')); ?>" alt="">
                     Transparência

                     <svg fill="currentColor" viewBox="0 0 20 20" :class="{'rotate-180': open, 'rotate-0': !open}"
                        class="inline w-4 h-4 mt-1 transition-transform duration-200 transform md:-mt-1">
                        <path fill-rule="evenodd"
                           d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                           clip-rule="evenodd">
                        </path>
                     </svg>

                  </button>
                  <div x-show="open" x-transition:enter="transition ease-out duration-300"
                     x-transition:enter-start="transform opacity-0 scale-95"
                     x-transition:enter-end="transform opacity-100 scale-100"
                     x-transition:leave="transition ease-in duration-75"
                     x-transition:leave-start="transform opacity-100 scale-100"
                     x-transition:leave-end="transform opacity-0 scale-95"
                     class="absolute z-10 w-full mt-2 origin-top rounded-lg shadow-lg md:w-96"
                     style="display:none; box-shadow: 2px 2px white;">
                     <div class="py-2 bg-white rounded-md shadow">

                        <a class="block px-4 py-2 bg-transparent hover:text-green-900 hover:bg-green-100 focus:shadow-outline"
                           name="texto" style="font-size:0.875rem;" href="<?php echo e(route('compras-e-servicos')); ?>">
                           Despesas - Compras e Serviços
                        </a>

                        <div @click.away="open = false" class="relative" x-data="{ open: false }">
                           <div class="flex items-center">
                              <button @click="open = !open"
                                 class="block px-4 py-2 w-full flex items-start bg-transparent hover:bg-green-100 hover:text-green-900 focus:shadow-outline"
                                 name="texto" style="font-size:0.875rem;">
                                 Pedidos de Informação Covid-19
                                 <svg fill="currentColor" viewBox="0 0 20 20"
                                    :class="{'rotate-180': open, 'rotate-0': !open}"
                                    class="inline w-4 h-4 mt-1 transition-transform duration-200 transform md:mt-0">
                                    <path fill-rule="evenodd"
                                       d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                       clip-rule="evenodd">
                                    </path>
                                 </svg>
                              </button>
                           </div>
                           <div x-show="open" x-transition:enter="transition ease-out duration-300"
                              x-transition:enter-start="transform opacity-0 scale-95"
                              x-transition:enter-end="transform opacity-100 scale-100"
                              x-transition:leave="transition ease-in duration-75"
                              x-transition:leave-start="transform opacity-100 scale-100"
                              x-transition:leave-end="transform opacity-0 scale-95"
                              class="absolute w-full mt-2 origin-top rounded-md shadow-lg md:w-48 z-10"
                              style="display:none; box-shadow: 2px 2px white;">
                              <div class="py-2 bg-white rounded-md shadow">
                                 <a class="block px-4 py-2 bg-transparent hover:text-green-900 hover:bg-green-100 focus:shadow-outline"
                                    name="texto" style="font-size:0.875rem;"
                                    href="<?php echo e(route('e-SIC-2022')); ?>"
                                    target="_blank">
                                    Pedidos 2022
                                 </a>
                                 <a class="block px-4 py-2 bg-transparent hover:text-green-900 hover:bg-green-100 focus:shadow-outline"
                                    name="texto" style="font-size:0.875rem;"
                                    href="<?php echo e(route('e-SIC-2021')); ?>"
                                    target="_blank">
                                    Pedidos 2021
                                 </a>
                                 <a class="block px-4 py-2 bg-transparent hover:text-green-900 hover:bg-green-100 focus:shadow-outline"
                                    name="texto" style="font-size:0.875rem;"
                                    href="<?php echo e(route('e-SIC-2020')); ?>"
                                    target="_blank">
                                    Pedidos 2020
                                 </a>
                              </div>
                           </div>
                        </div>

                        <div @click.away="open = false" class="relative" x-data="{ open: false }">
                           <div class="flex items-center">
                              <button @click="open = !open"
                                 class="block px-4 py-2 w-full flex items-start bg-transparent hover:bg-green-100 hover:text-green-900 focus:shadow-outline"
                                 name="texto" style="font-size:0.875rem;">
                                 Relatório Estatístico da LAI
                                 <svg fill="currentColor" viewBox="0 0 20 20"
                                    :class="{'rotate-180': open, 'rotate-0': !open}"
                                    class="inline w-4 h-4 mt-1 transition-transform duration-200 transform md:mt-0">
                                    <path fill-rule="evenodd"
                                       d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                       clip-rule="evenodd">
                                    </path>
                                 </svg>
                              </button>
                           </div>
                           <div x-show="open" x-transition:enter="transition ease-out duration-300"
                              x-transition:enter-start="transform opacity-0 scale-95"
                              x-transition:enter-end="transform opacity-100 scale-100"
                              x-transition:leave="transition ease-in duration-75"
                              x-transition:leave-start="transform opacity-100 scale-100"
                              x-transition:leave-end="transform opacity-0 scale-95"
                              class="absolute w-full mt-2 origin-top rounded-md shadow-lg md:w-48 z-10"
                              style="display:none; box-shadow: 2px 2px white;">
                              <div class="py-2 bg-white rounded-md shadow">
                                 <a class="block px-4 py-2 bg-transparent hover:text-green-900 hover:bg-green-100 focus:shadow-outline"
                                    name="texto" style="font-size:0.875rem;"
                                    href="<?php echo e(route('Relatorio Lai-2022')); ?>"
                                    target="_blank">
                                    Relatório - 2022
                                 </a>
                                 <a class="block px-4 py-2 bg-transparent hover:text-green-900 hover:bg-green-100 focus:shadow-outline"
                                    name="texto" style="font-size:0.875rem;"
                                    href="<?php echo e(route('Relatorio Lai-2021')); ?>"
                                    target="_blank">
                                    Relatório - 2021
                                 </a>
                                 <a class="block px-4 py-2 bg-transparent hover:text-green-900 hover:bg-green-100 focus:shadow-outline"
                                    name="texto" style="font-size:0.875rem;"
                                    href="<?php echo e(route('Relatorio Lai-2020')); ?>"
                                    target="_blank">
                                    Relatório - 2020
                                 </a>
                              </div>
                           </div>
                        </div>

                        <div @click.away="open = false" class="relative" x-data="{ open: false }">
                           <div class="flex items-center">
                              <button @click="open = !open"
                                 class="block px-4 py-2 w-full flex items-start bg-transparent hover:bg-green-100 hover:text-green-900 focus:shadow-outline"
                                 name="texto" style="font-size:0.875rem;">
                                 Relatório de Manifestações da Ouvidoria
                                 <svg fill="currentColor" viewBox="0 0 20 20"
                                    :class="{'rotate-180': open, 'rotate-0': !open}"
                                    class="inline w-4 h-4 mt-1 transition-transform duration-200 transform md:mt-0">
                                    <path fill-rule="evenodd"
                                       d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                       clip-rule="evenodd">
                                    </path>
                                 </svg>
                              </button>
                           </div>
                           <div x-show="open" x-transition:enter="transition ease-out duration-300"
                              x-transition:enter-start="transform opacity-0 scale-95"
                              x-transition:enter-end="transform opacity-100 scale-100"
                              x-transition:leave="transition ease-in duration-75"
                              x-transition:leave-start="transform opacity-100 scale-100"
                              x-transition:leave-end="transform opacity-0 scale-95"
                              class="absolute w-full mt-2 origin-top rounded-md shadow-lg md:w-auto z-10"
                              style="display:none; box-shadow: 2px 2px white;">
                              <div class="py-2 bg-white rounded-md shadow">
                                 <a class="block px-4 py-2 bg-transparent hover:text-green-900 hover:bg-green-100 focus:shadow-outline"
                                    name="texto" style="font-size:0.875rem;"
                                    href="<?php echo e(route('Relatorio Ouv-2022')); ?>"
                                    target="_blank">
                                    Relatório Ouvidoria - 2022
                                 </a>
                                 <a class="block px-4 py-2 bg-transparent hover:text-green-900 hover:bg-green-100 focus:shadow-outline"
                                    name="texto" style="font-size:0.875rem;"
                                    href="<?php echo e(route('Relatorio Ouv-2021')); ?>"
                                    target="_blank">
                                    Relatório Ouvidoria - 2021
                                 </a>
                                 <a class="block px-4 py-2 bg-transparent hover:text-green-900 hover:bg-green-100 focus:shadow-outline"
                                    name="texto" style="font-size:0.875rem;"
                                    href="<?php echo e(route('Relatorio Ouv-2020')); ?>"
                                    target="_blank">
                                    Relatório Ouvidoria - 2020
                                 </a>
                              </div>
                           </div>
                        </div>

                        <div @click.away="open = false" class="relative" x-data="{ open: false }">
                           <div class="flex items-center">
                              <button @click="open = !open"
                                 class="block px-4 py-2 w-full flex items-start bg-transparent hover:bg-green-100 hover:text-green-900 focus:shadow-outline"
                                 name="texto" style="font-size:0.875rem;">
                                 Estímulos a Economia
                                 <svg fill="currentColor" viewBox="0 0 20 20"
                                    :class="{'rotate-180': open, 'rotate-0': !open}"
                                    class="inline w-4 h-4 mt-1 transition-transform duration-200 transform md:mt-0">
                                    <path fill-rule="evenodd"
                                       d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                       clip-rule="evenodd">
                                    </path>
                                 </svg>
                              </button>
                           </div>
                           <div x-show="open" x-transition:enter="transition ease-out duration-300"
                              x-transition:enter-start="transform opacity-0 scale-95"
                              x-transition:enter-end="transform opacity-100 scale-100"
                              x-transition:leave="transition ease-in duration-75"
                              x-transition:leave-start="transform opacity-100 scale-100"
                              x-transition:leave-end="transform opacity-0 scale-95"
                              class="absolute w-full mt-2 origin-top rounded-md shadow-lg md:w-48 z-10"
                              style="display:none; box-shadow: 2px 2px white;">
                              <div class="py-2 bg-white rounded-md shadow">
                                 <a class="block px-4 py-2 bg-transparent hover:text-green-900 hover:bg-green-100 focus:shadow-outline"
                                    name="texto" style="font-size:0.875rem;"
                                    href="<?php echo e(url('http://www.set.rn.gov.br/contentProducao/aplicacao/set_v2/principal/arquivos/apresentacao-ilustrada-medidas-SET-enfrentamento-COVID-19.pdf')); ?>"
                                    target="_blank">
                                    Medidas
                                 </a>
                                 <a class="block px-4 py-2 bg-transparent hover:text-green-900 hover:bg-green-100 focus:shadow-outline"
                                    name="texto" style="font-size:0.875rem;"
                                    href="<?php echo e(url('http://www.set.rn.gov.br/contentProducao/aplicacao/set_v2/principal/arquivos/consolidacao-medidas-SET.pdf')); ?>"
                                    target="_blank">
                                    Normativos
                                 </a>
                              </div>
                           </div>
                        </div>

                        <a class="block px-4 py-2 bg-transparent hover:text-green-900 hover:bg-green-100 focus:shadow-outline"
                           name="texto" style="font-size:0.875rem;"
                           href="<?php echo e(url('https://docs.google.com/document/d/1HXUeuU1Nd_R1-oQL5UZqYLa8p6FlC2mJl3LYvbuMO08/edit#')); ?>"
                           target="_blank">
                           Proteção Social
                        </a>
                        <a class="block px-4 py-2 bg-transparent hover:text-green-900 hover:bg-green-100 focus:shadow-outline"
                           name="texto" style="font-size:0.875rem;"
                           href="<?php echo e(asset('docs/divida_publica/LC 173 2020 - Aplicação da Dívida Pública.pdf')); ?>"
                           target="_blank">
                           Aplicação Dívida Pública (LC 173/2020)
                        </a>
                     </div>
                  </div>
               </div>

               
               <div @click.away="open = false" class="relative" x-data="{ open: false }">

                  <button @click="open = !open"
                     class="flex items-center px-2 py-2 mt-2 font-semibold md:mt-0 focus:shadow-outline rounded-lg transition duration-150 ease-in-out transform hover:-translate-y-1 hover:scale-110 hover:text-indigo-400"
                     name="texto" style="font-size:0.875rem;">
                     <i class="fas fa-balance-scale fa-fw mr-1 text-indigo-400"></i>
                     Legislação

                     <svg fill="currentColor" viewBox="0 0 20 20" :class="{'rotate-180': open, 'rotate-0': !open}"
                        class="inline w-4 h-4 mt-1 transition-transform duration-200 transform md:-mt-1">
                        <path fill-rule="evenodd"
                           d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                           clip-rule="evenodd">
                        </path>
                     </svg>

                  </button>
                  <div x-show="open" x-transition:enter="transition ease-out duration-300"
                     x-transition:enter-start="transform opacity-0 scale-95"
                     x-transition:enter-end="transform opacity-100 scale-100"
                     x-transition:leave="transition ease-in duration-75"
                     x-transition:leave-start="transform opacity-100 scale-100"
                     x-transition:leave-end="transform opacity-0 scale-95"
                     class="absolute z-10 w-full mt-2 origin-top rounded-md shadow-lg md:w-60"
                     style="display:none; box-shadow: 2px 2px white;">
                     <div class="py-2 bg-white rounded-md shadow">
                        <a class="block px-4 py-2 bg-transparent hover:text-indigo-900 hover:bg-indigo-100 focus:shadow-outline"
                           name="texto" style="font-size:0.875rem;"
                           href="<?php echo e(url('http://www.adcon.rn.gov.br/ACERVO/control/DOC/DOC000000000238551.PDF')); ?>"
                           target="_blank">
                           Decretos por Temas
                        </a>
                        <a class="block px-4 py-2 bg-transparent hover:text-indigo-900 hover:bg-indigo-100 focus:shadow-outline"
                           name="texto" style="font-size:0.875rem;" href="<?php echo e(route('instrucao-normativa')); ?>">
                           Instrução Normativa
                        </a>
                     </div>
                  </div>
               </div>

               
               

               
               <a class="flex items-center px-2 py-2 mt-2 font-semibold md:mt-0 focus:shadow-outline transition rounded-lg duration-150 ease-in-out transform hover:-translate-y-1 hover:scale-110 hover:text-teal-500"
                  name="texto" style="font-size: 0.875rem;" href="<?php echo e(route('rn-mais-vacina', [
                     'status' => 'covid'
                  ])); ?>">
                  <img class="mr-1" src="<?php echo e(asset('images/icons/rn_mais_vacina-icon.png')); ?>" alt="">
                  RN + Vacina
               </a>

               
               <a class="flex items-center px-2 py-2 mt-2 font-semibold md:mt-0 focus:shadow-outline transition rounded-lg duration-150 ease-in-out transform hover:-translate-y-1 hover:scale-110 hover:text-purple-400"
                  name="texto" style="font-size: 0.875rem;" href="<?php echo e(url('https://portalcovid19.saude.rn.gov.br/')); ?>"
                  target="_blank">
                  <i class="fas fa-heartbeat fa-fw mr-1 text-purple-400"></i>
                  Portal Saúde
               </a>

               
               <a class="flex items-center px-2 py-2 mt-2 font-semibold md:mt-0 rounded-lg focus:shadow-outline transition duration-150 ease-in-out transform hover:-translate-y-1 hover:scale-110 hover:text-lightBlue-600"
                  name="texto" style="font-size: 0.875rem;"
                  href="<?php echo e(url('/control')); ?>">
                  <img class="mr-1" src="<?php echo e(asset('images/icons/logo-rn-redi.png')); ?>" alt="">
                  CONTROL
               </a>
            </nav>
         </div>
      </div>
   </div>
</div>

<script src="<?php echo e(asset('js/alpine.min.js')); ?>"></script>

<script>
   if (screen.width>1024){
       // When the user scrolls down 80px from the top of the document, resize the navbar's padding and the logo's font size
       window.onscroll = function() {scrollFunction()};
       const logoimg = document.getElementById('logoimg');
       const logotext = document.getElementById('logotext');
       const ouvi = document.getElementById('ouvi');

       function scrollFunction() {
         if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
             logoimg.classList.add("hidden");
             logotext.classList.remove("hidden");
             logotext.classList.add("active");
             ouvi.classList.add("hidden");

         } else {
             logoimg.classList.remove("hidden");
             logoimg.classList.add("active");
             logotext.classList.remove('active');
             logotext.classList.add('hidden');
             ouvi.classList.remove("hidden");
         }
       }
   }
</script>
<?php /**PATH /var/www/html/portaltransparencianewrn/resources/views/layouts/covid/nav.blade.php ENDPATH**/ ?>