<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">

<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <link rel="icon" href="<?php echo e(asset('images/icons/favicon-covid-16x16.png')); ?>" type="image/ico">
   <title>Portal da Transparência do Rio Grande do Norte <?php echo $__env->yieldContent('title'); ?></title>

   
   <link rel="stylesheet" href="<?php echo e(asset('css/app.css')); ?>">

   
   <link rel="stylesheet" href="<?php echo e(asset('css/main.css')); ?>">
   <link rel="stylesheet" href="<?php echo e(asset('css/nav.css')); ?>">
   <link rel="stylesheet" href="<?php echo e(asset('css/accessbar.css')); ?>">
   <link id="linkcontraste" rel="stylesheet" href="">

   <?php if(env("ENVIRONMENT") == 'testing'): ?>

   
   <script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo e(env(" G_TAG")); ?>"></script>
   <script>
      window.dataLayer = window.dataLayer || [];
         function gtag(){dataLayer.push(arguments);}
         gtag('js', new Date());

         gtag('config', '<?php echo e(env("G_TAG")); ?>');
   </script>
   <?php elseif(env("ENVIRONMENT") == 'production'): ?>

   
   <script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo e(env(" G_TAG_PRODUCTION")); ?>"></script>
   <script>
      window.dataLayer = window.dataLayer || [];
         function gtag(){dataLayer.push(arguments);}
         gtag('js', new Date());

         gtag('config', '<?php echo e(env("G_TAG_PRODUCTION")); ?>');
   </script>

   <?php endif; ?>
   <style>
      .select2-results__options {
         font-size: 0.8em;
      }
   </style>
   <script src="<?php echo e(asset('js/jquery-latest.min.js')); ?>"></script>
   
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

   <?php echo $__env->yieldContent('css'); ?>
   <?php echo $__env->yieldContent('js'); ?>
</head>

<noscript>
   <h1 class="text-black text-xl pl-6 pt-6">
      Esta página precisa que o javascript esteja ativado para funcionar.
   </h1>

   <style>
      #pagewrapper {
         display: none;
      }
   </style>
</noscript>

<body id="corpo" class="bg-warmGray-50 font-sans leading-normal tracking-normal">

   <div id="pagewrapper">
      <!-- Cookies -->
      <div id="cookies" class="flex fixed bottom-0 hidden z-50">
         <?php echo $__env->make('layouts.cookie', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
      </div>

      <!-- Accessbar -->
      <div id="accessbar">
         <?php echo $__env->make('layouts.accessbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
      </div>

      <!-- Nav -->
      <div id="nav">
         <?php echo $__env->make('layouts/covid.nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
      </div>

      <!-- Spacing -->
      <div id='space'></div>

      <!-- Vlibras -->
      <div id="vlibras">
         <?php echo $__env->make('layouts.vlibras', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
      </div>

      <!-- Content -->
      <div id='content' class="container-fluid-covid container w-full mx-auto py-5">
         <?php echo $__env->yieldContent('content'); ?>
      </div>

      <!-- Footer -->
      <div id="footer">
         <?php echo $__env->make('layouts.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
      </div>
   </div>

   <!-- Loading -->
   <div id="load" class="hidden">
      <?php echo $__env->make('layouts.loading', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
   </div>
</body>

<!-- Script -->
<script src="<?php echo e(asset('js/contpos.js')); ?>"></script>
<script src="<?php echo e(asset('js/pageload.js')); ?>"></script>
<script src="<?php echo e(asset('js/cookie.js')); ?>"></script>
<script src="<?php echo e(asset('js/accessbar.js')); ?>"></script>
<script src="https://vlibras.gov.br/app/vlibras-plugin.js"></script>
<script>
   new window.VLibras.Widget('https://vlibras.gov.br/app');
</script>

</html><?php /**PATH /var/www/html/portaltransparencianewrn/resources/views/layouts/covid/app.blade.php ENDPATH**/ ?>