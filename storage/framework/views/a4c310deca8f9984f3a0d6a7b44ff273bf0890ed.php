<?php $__env->startSection('title'); ?>
- Despesas
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <section class="text-gray-600 body-font">
        <div class="container px-5 mx-auto flex flex-wrap flex-col">
            
            <ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                <li class="inline-flex items-center">
                    <a href="<?php echo e(url('/')); ?>">Inicio</a>
                    <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                        <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd"></path>
                    </svg>
                </li>
                <li class="inline-flex items-center">
                    <a class="text-teal-400" href="#">Despesas</a>
                </li>
            </ul>
            
            <hr class="border-b-2 border-lightBlue-300 mb-12 mx-4">
            
            <div id="tabs" class="grid grid-cols-3 mx-auto mb-10 items-end" name="texto" style="font-size: 1rem; line-height: 1.5rem;">
                <span class="flex flex-wrap">
                    <a id="default-tab" href="#first" class="sm:px-6 py-3 text-center w-full border-b title-font font-medium inline-flex justify-center items-center leading-none hover:text-red-600 tracking-wider rounded-t">
                        Despesa
                    </a>
                </span>
                <span class="flex flex-wrap">
                    <a href="#second" class="sm:px-6 py-3 text-center w-full border-b title-font font-medium inline-flex justify-center items-center leading-none hover:text-red-600 tracking-wider">
                        Repasse Outros Poderes
                    </a>
                </span>
                <span class="flex flex-wrap">
                    <a href="#third" class="sm:px-6 py-3 text-center w-full border-b title-font font-medium inline-flex justify-center items-center leading-none hover:text-red-600 tracking-wider">
                        Bloqueios Judiciais
                    </a>
                </span>
            </div>
            
            <section id="tab-contents" class="text-gray-600 body-font relative ">
                
                
                <div id="first" class="lg:w-2/3 container px-5 mx-auto bg-white p-12 rounded-md shadow-lg">
                    <div class="flex flex-col text-center w-full mb-12">
                        <h1 class="text-xl font-semibold mb-4 text-red-600 uppercase" name="texto" style="font-size: 1.25rem; line-height: 1.75rem;">Despesa</h1>
                        <p class="mx-auto leading-relaxed text-base" name="texto" style="font-size: 1rem; line-height: 1.5rem;"> Nessa área você pode conferir todos gastos diretos referente ao Estado do RN, selecionando as opções abaixo de acordo com sua necessidade de consulta.</p>
                    </div>
                    <div class="mx-auto">
                        <form action="<?php echo e(route('gastos-diretos')); ?>" method="POST">
                            <?php echo csrf_field(); ?>
                            <div class="grid grid-cols-1 md:grid-cols-3 gap-3">
                                <span>
                                 <label for="posicao" class="block text-xs font-semibold text-red-600 uppercase" name="texto" style="font-size: 0.75rem; line-height: 1rem;">Posição a ser Consultada
                                 </label>
                                    <select id="posicao" name="posicao" class="block w-full p-3 mt-2 text-gray-700 bg-white border border-gray-300 focus:outline-none focus:bg-gray-100 focus:shadow-inner">
                                        <option value="Acumulado">Acumulado</option>
                                        <option value="No mês">No mês</option>
                                    </select>
                                </span>
                                <span>
                                 <label for="mes" class="block text-xs font-semibold text-red-600 uppercase" name="texto" style="font-size: 0.75rem; line-height: 1rem;">Mês de Referência
                                 </label>
                                    <select id="mes" name="mes" class="block w-full p-3 mt-2 text-gray-700 bg-white border border-gray-300 focus:outline-none focus:bg-gray-100 focus:shadow-inner">
                                        <?php $__currentLoopData = $meses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($mes); ?>"><?php echo app('translator')->get('meses.'.$mes); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </span>
                                <span>
                                 <label for="ano" class="block text-xs font-semibold text-red-600 uppercase" name="texto" style="font-size: 0.75rem; line-height: 1rem;">Ano de Referência
                                 </label>
                                    <select id="ano" name="ano" class="block w-full p-3 mt-2 text-gray-700 bg-white border border-gray-300 focus:outline-none focus:bg-gray-100 focus:shadow-inner">
                                        <option value="2024">2024</option>
                                        <option value="2023">2023</option>
                                        <option value="2022">2022</option>
                                        <option value="2021">2021</option>
                                        <option value="2020">2020</option>
                                        <option value="2019">2019</option>
                                    </select>
                                </span>
                            </div>
                            <div class="grid grid-cols-1 mt-2 md:grid-cols-2 gap-3">
                                <span>
                                    <label for="classificacao" class="block text-xs font-semibold text-red-600 uppercase" name="texto" style="font-size: 0.75rem; line-height: 1rem;">Classificação</label>
                                    <select id="classificacao" name="classificacao" class="block w-full p-3 mt-2 text-gray-700 bg-white border border-gray-300 focus:outline-none focus:bg-gray-100 focus:shadow-inner">
                                        <option value="despesa">Por Tipo de Despesa</option>
                                        <option value="gestora">Por Unidade Gestora</option>
                                        <option value="acao">Por Ação</option>
                                        <option value="fonte">Por Fonte</option>
                                        <option value="favorecido">Por Favorecido</option>
                                        <option value="funcao">Por Função</option>
                                    </select>
                                </span>
                            </div>
                            <div class="pt-6">
                                <input type="submit" class="cursor-pointer flex mx-auto text-white bg-red-700 border-0 py-2 px-8 focus:outline-none hover:bg-red-800 rounded text-lg" value="Consultar" />
                            </div>
                        </form>
                    </div>
                </div>

                
                <div id="second" class="hidden lg:w-2/3 container px-5 mx-auto bg-white p-12 rounded-md shadow-lg">
                    
                    <div class="flex flex-col text-center w-full mb-12">
                        <h1 class="text-xl font-semibold mb-4 text-red-600 uppercase" name="texto" style="font-size: 1.25rem; line-height: 1.75rem;">Repasse Outros Poderes</h1>
                        <p class="mx-auto leading-relaxed text-base" name="texto" style="font-size: 1rem; line-height: 1.5rem;">Apresenta os valores repassados aos poderes Legislativo e Judiciário, como também ao Ministério Público e Tribunal de Contas da esfera estadual. "Selecionando as opções abaixo de acordo com sua necessidade de consulta."</p>
                    </div>
                    
                    <div class="mx-auto">
                        <form action="<?php echo e(route('repasse-outros-poderes')); ?>" method="POST" class="mt-6">
                            <?php echo csrf_field(); ?>
                            <div class="grid grid-cols-1 md:grid-cols-3 gap-3">
                                <span>
                                    <label for="posicao" class="block text-xs font-semibold text-red-600 uppercase" name="texto" style="font-size: 0.75rem; line-height: 1rem;">Posição a ser Consultada</label>
                                    <select id="posicao" name="posicao" class="block w-full p-3 mt-2 text-gray-700 bg-white border border-gray-300 focus:outline-none focus:bg-gray-100 focus:shadow-inner">
                                        <option value="Acumulado">Acumulado</option>
                                        <option value="No mês">No mês</option>
                                    </select>
                                </span>
                                <span>
                                    <label for="mes" class="block text-xs font-semibold text-red-600 uppercase" name="texto" style="font-size: 0.75rem; line-height: 1rem;">Mês de Referência</label>
                                    <select id="mes" name="mes" class="block w-full p-3 mt-2 text-gray-700 bg-white border border-gray-300 focus:outline-none focus:bg-gray-100 focus:shadow-inner">
                                        <?php $__currentLoopData = $meses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($mes); ?>"><?php echo app('translator')->get('meses.'.$mes); ?>
                                            </option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </span>
                                <span>
                                    <label for="ano" class="block text-xs font-semibold text-red-600 uppercase" name="texto" style="font-size: 0.75rem; line-height: 1rem;">Ano de Referência</label>
                                    <select id="ano" name="ano" class="block w-full p-3 mt-2 text-gray-700 bg-white border border-gray-300 focus:outline-none focus:bg-gray-100 focus:shadow-inner">
                                        <option value="2024">2024</option>
                                        <option value="2023">2023</option>
                                        <option value="2022">2022</option>
                                        <option value="2021">2021</option>
                                        <option value="2020">2020</option>
                                        <option value="2019">2019</option>
                                    </select>
                                </span>
                            </div>
                            <div class="pt-6 w-full">
                                <input type="submit" class="cursor-pointer flex mx-auto text-white bg-red-700 border-0 py-2 px-8 focus:outline-none hover:bg-red-800 rounded text-lg" value="Consultar" name="texto" style="font-size: 1.125rem; line-height: 1.75rem;" />
                            </div>
                        </form>
                    </div>
                </div>

                
                <div id="third" class="hidden lg:w-2/3 container px-5 mx-auto bg-white p-12 rounded-md shadow-lg">
                    <div class="flex flex-col text-center w-full mb-12">
                        <h1 class="text-xl font-semibold mb-4 text-red-600 uppercase" name="texto" style="font-size: 1.25rem; line-height: 1.75rem;">Bloqueios Judiciais</h1>
                        <p class="mx-auto leading-relaxed text-base" name="texto" style="font-size: 1rem; line-height: 1.5rem;">Medida cautelar utilizada para resguardar a entrega de um bem determinado, objeto de disputa judicial. Selecionando as opções abaixo de acordo com sua necessidade de consulta.</p>
                    </div>
                    <div class="mx-auto">
                        <form action="<?php echo e(route('bloqueios-judiciais')); ?>" method="POST" class="mt-6">
                            <?php echo csrf_field(); ?>
                            <div class="grid grid-cols-2 gap-3">
                                <label for="mes" class="block text-xs font-semibold text-red-600 uppercase" name="texto" style="font-size: 0.75rem; line-height: 1rem;">Mês de
                                    Referência
                                </label>
                                <label for="ano" class="block text-xs font-semibold text-red-600 uppercase" name="texto" style="font-size: 0.75rem; line-height: 1rem;">Ano de
                                    Referência
                                </label>
                            </div>
                            <div class="grid grid-cols-2 gap-3">
                                <span>
                                    <select id="mes" name="mes" class="block w-full p-3 mt-2 text-gray-700 bg-white border border-gray-300 focus:outline-none focus:bg-gray-100 focus:shadow-inner">
                                        <?php $__currentLoopData = $meses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($mes); ?>"><?php echo app('translator')->get('meses.'.$mes); ?>
                                            </option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </span>
                                <span>
                                    <select id="ano" name="ano" class="block w-full p-3 mt-2 text-gray-700 bg-white border border-gray-300 focus:outline-none focus:bg-gray-100 focus:shadow-inner">
                                        <option value="2024">2024</option>
                                        <option value="2023">2023</option>
                                        <option value="2022">2022</option>
                                        <option value="2021">2021</option>
                                        <option value="2020">2020</option>
                                        <option value="2019">2019</option>
                                    </select>
                                </span>
                            </div>
                            <div class="pt-6 w-full">
                                <input type="submit" class="cursor-pointer flex mx-auto text-white bg-red-700 border-0 py-2 px-8 focus:outline-none hover:bg-red-800 rounded text-lg" value="Consultar" name="texto" style="font-size: 1.125rem; line-height: 1.75rem;" />
                            </div>
                        </form>
                    </div>
                </div>

            </section>
            
        </div>
    </section>

    <script>
        let tabsContainer = document.querySelector("#tabs");

        let tabTogglers = tabsContainer.querySelectorAll("a");

        tabTogglers.forEach(function(toggler) {
            toggler.addEventListener("click", function(e) {
                e.preventDefault();

                let tabName = this.getAttribute("href");
                let tabContents = document.querySelector("#tab-contents");

                for (let i = 0; i < tabContents.children.length; i++) {
                    tabTogglers[i].parentElement.classList.remove("border-red-500", "border-b-4",
                        "text-red-500");
                    tabContents.children[i].classList.remove("hidden");
                    if ("#" + tabContents.children[i].id === tabName) {
                        continue;
                    }
                    tabContents.children[i].classList.add("hidden");
                }
                e.target.parentElement.classList.add("border-red-500", "border-b-4", "text-red-500");
            });
        });
        document.getElementById("default-tab").click();
    </script>

    <script>
        document.getElementById('mes').value = <?php echo e($mescorrente); ?>;
    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/portaltransparencianewrn/resources/views/despesas/index.blade.php ENDPATH**/ ?>