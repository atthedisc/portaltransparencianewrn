<?php $__env->startSection('title'); ?>
- Empresas inidôneas
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<section class="text-gray-600 body-font">
   <div class="container px-5 mx-auto">
      <div class="flex flex-col text-center w-full">

         <h1 class="text-lg text-gray-900 tracking-widest font-medium title-font uppercase pb-2" name="texto"
            style="font-size: 1.125rem; line-height: 2rem;">
            <i class="fas fa-building"></i>
            Empresas inidôneas
         </h1>

         <ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto"
            style="font-size: 0.875rem; line-height: 1.25rem;">
            <li class="inline-flex items-center">
               <a href="<?php echo e(url('/')); ?>">Início</a>
               <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                  <path fill-rule="evenodd"
                     d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                     clip-rule="evenodd"></path>
               </svg>
            </li>
            <li class="inline-flex items-center">
               <a href="#" class="text-teal-400">
                  Empresas inidôneas
               </a>
            </li>
         </ul>

         <hr class="border-b-2 border-lightBlue-300 mb-6 mx-4">

         <table
            class='mx-auto mb-24 max-w-sm md:max-w-full rounded-lg bg-white divide-y divide-lightBlue-200 overflow-hidden text-center'>

            <tr class="text-coolGray-400 text-center">
               <th class="font-normal text-sm tracking-wider px-6 py-4 text-center" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">
                  Clique no link desejado para acessar
               </th>
            </tr>

            <tbody class="divide-y divide-lightBlue-200">
               <tr>
                  <td class="px-6 py-4 text-left text-sm">
                     <i class="fa fa-link mr-1"></i>
                     <a href="<?php echo e(url('https://contas.tcu.gov.br/ords/f?p=1660:2:::NO:2')); ?>"
                        class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        TCU: Inidôneos - Licitantes Inidôneos
                     </a>
                  </td>
               </tr>
               <tr>
                  <td class="px-6 py-4 text-left text-sm">
                     <i class="fa fa-link mr-1"></i>
                     <a href="<?php echo e(url('https://www.cnj.jus.br/improbidade_adm/consultar_requerido.php')); ?>"
                        class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        CNJ: CNIA - Cadastro Nacional de Condenações Cíveis por Ato de Improbidade Administrativa e
                        Inelegibilidade
                     </a>
                  </td>
               </tr>
               <tr>
                  <td class="px-6 py-4 text-left text-sm">
                     <i class="fa fa-link mr-1"></i>
                     <a href="<?php echo e(url('https://portaldatransparencia.gov.br/pagina-interna/603245-ceis')); ?>"
                        class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        CEIS - Cadastro Nacional de Empresas Inidôneas e Suspensas
                     </a>
                  </td>
               </tr>
               <tr>
                  <td class="px-6 py-4 text-left text-sm">
                     <i class="fa fa-link mr-1"></i>
                     <a href="<?php echo e(url('https://portaldatransparencia.gov.br/pagina-interna/603244-cnep')); ?>"
                        class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        CNEP - Cadastro Nacional de Empresas Punidas
                     </a>
                  </td>
               </tr>
               <tr>
                  <td class="px-6 py-4 text-left text-sm">
                     <i class="fa fa-link mr-1"></i>
                     <a href="<?php echo e(url('http://201.76.150.19:8080/SCEISRNPortal/')); ?>"
                        class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        Consulta Pública - Cadastro de Empresas Inidôneas e Suspensas do RN
                     </a>
                  </td>
               </tr>
            </tbody>
         </table>

      </div>
   </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/portaltransparencianewrn/resources/views/empresas-inidoneas/index.blade.php ENDPATH**/ ?>