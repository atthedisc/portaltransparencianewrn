<?php $__env->startSection('title'); ?>
- Mapa do site
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<section class="text-gray-600 body-font">
   <div class="container px-5 mx-auto">
      <div class="flex flex-col text-center w-full">

         <h1 class="text-lg text-gray-900 tracking-widest font-medium title-font pb-2" name="texto"
            style="font-size: 1.125rem; line-height: 2rem;">
            <i class="fas fa-map-marked-alt"></i>
            MAPA DO SITE
         </h1>

         <p class="lg:w-3/4 mx-auto leading-relaxed text-sm text-center text-gray-600" name="texto"
            style="font-size: 0.875rem; line-height: 1.25rem;">
            O Mapa do site exibe, de forma categorizada, uma visão geral de todo o conteúdo disponível no Portal da
            Transparência do Rio Grande do Norte.
         </p>

         <ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto"
            style="font-size: 0.875rem; line-height: 1.25rem;">
            <li class="inline-flex items-center">
               <a href="<?php echo e(url('/')); ?>">Início</a>
               <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                  <path fill-rule="evenodd"
                     d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                     clip-rule="evenodd"></path>
               </svg>
            </li>
            <li class="inline-flex items-center">
               <a href="#" class="text-teal-400">
                  Mapa do site
               </a>
            </li>
         </ul>

         <hr class="border-b-2 border-lightBlue-300 mb-6 mx-4">

         <div class="text-center">
            <h1 class="font-bold text-lg text-gray-900" name="texto" style="font-size: 1.125rem; line-height: 1.75rem;">
               <a class="hover:underline hover:text-lightBlue-700" href="<?php echo e(url('/')); ?>">
                  PORTAL DA TRANSPARÊNCIA
               </a>
            </h1>
         </div>

         <div class="flex flex-wrap pt-5">

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
               <div class="bg-white border border-lightBlue-600 rounded-md shadow h-full p-2">
                  <div class="flex flex-row items-center">
                     <div class="flex-1 text-left">
                        <h3 class="font-semibold uppercase text-gray-800 pb-2" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           Receita
                        </h3>
                        <ol class="text-sm text-lightBlue-500" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700" href="<?php echo e(route('receita')); ?>">
                                 Receitas
                              </a>
                           </li>
                        </ol>
                     </div>
                  </div>
               </div>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
               <div class="bg-white border border-lightBlue-600 rounded-md shadow h-full p-2">
                  <div class="flex flex-row items-center">
                     <div class="flex-1 text-left">
                        <h3 class="font-semibold uppercase text-gray-800 pb-2" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           Despesa
                        </h3>
                        <ol class="text-sm text-lightBlue-500" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700" href="<?php echo e(asset('despesas')); ?>">
                                 Despesa
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700" href="<?php echo e(asset('despesas')); ?>">
                                 Repasse Outros Poderes
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700" href="<?php echo e(asset('despesas')); ?>">
                                 Bloqueios Judiciais
                              </a>
                           </li>
                        </ol>
                     </div>
                  </div>
               </div>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
               <div class="bg-white border border-lightBlue-600 rounded-md shadow h-full p-2">
                  <div class="flex flex-row items-center">
                     <div class="flex-1 text-left">
                        <h3 class="font-semibold uppercase text-gray-800 pb-2" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           Pessoal
                        </h3>
                        <ol class="text-sm text-lightBlue-500" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(url('http://servicos.searh.rn.gov.br/searh/Remuneracao')); ?>" target="_blank">
                                 Remuneração
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(url('http://servicos.searh.rn.gov.br/searh/Diaria')); ?>" target="_blank">
                                 Diárias
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(route('cadastro-autoridades')); ?>"
                                 target="_blank">
                                 Cadastro de autoridades
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(url('docs/estagiarios/20230713-Estagiarios.xlsx')); ?>"
                                 target="_blank">
                                 Relação de estagiários
                              </a>
                           </li>
                        </ol>
                     </div>
                  </div>
               </div>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
               <div class="bg-white h-full border border-lightBlue-600 rounded-md shadow p-2">
                  <div class="flex flex-row items-center">
                     <div class="flex-1 text-left">
                        <h3 class="font-semibold uppercase text-gray-800 pb-2" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           Contas Públicas
                        </h3>
                        <ol class="text-sm text-lightBlue-500" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(url('http://www.seplan.rn.gov.br/Conteudo.asp?TRAN=PASTAC&TARG=3334&ACT=&PAGE=&PARM=&LBL=')); ?>"
                                 target="_blank">
                                 Plano Plural - PPA
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(url('http://www.seplan.rn.gov.br/Conteudo.asp?TRAN=PASTAC&TARG=2476&ACT=&PAGE=0&PARM=&LBL=Legisla%E7%E3o')); ?>"
                                 target="_blank">
                                 Lei das Diretrizes Orçamentárias - LDO
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(url('http://www.seplan.rn.gov.br/Conteudo.asp?TRAN=PASTAC&TARG=3225&ACT=&PAGE=&PARM=&LBL=')); ?>"
                                 target="_blank">
                                 Orçamentos
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(url('http://control.rn.gov.br/Conteudo.asp?TRAN=CATALG&TARG=178&ACT=&PAGE=0&PARM=&LBL=Demonstrativos+Fiscais')); ?>"
                                 target="_blank">
                                 Demonstrativo Fiscal
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(url('http://www.control.rn.gov.br/Conteudo.asp?TRAN=PASTAC&TARG=896&ACT=&PAGE=&PARM=&LBL=Balan%E7os')); ?>"
                                 target="_blank">
                                 Balanço
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              Normatização
                           </li>
                           <li>
                              <a class="pl-4 hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(route('notas-tecnicas')); ?>">
                                 Notas Técnicas
                              </a>
                           </li>
                           <li>
                              <a class="pl-4 hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(route('orientacoes-tecnicas')); ?>">
                                 Orientações Técnicas
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(url('contas-governo')); ?>"
                                 target="_blank">
                                 Apreciação das Contas do Governo
                              </a>
                           </li>
                        </ol>
                     </div>
                  </div>
               </div>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
               <div class="bg-white h-full border border-lightBlue-600 rounded-md shadow p-2">
                  <div class="flex flex-row items-center">
                     <div class="flex-1 text-left">
                        <h3 class="font-semibold uppercase text-gray-800 pb-2" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           Contabilidade Fácil
                        </h3>
                        <ol class="text-sm text-lightBlue-500" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(route('oque-e-contabilidade-facil')); ?>">
                                 O que é Contabilidade Fácil
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              RN Contábil
                           </li>
                           <li>
                              <a class="pl-4 hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(route('contas-bimestrais')); ?>">
                                 Contas Bimestrais
                              </a>
                           </li>
                           <li>
                              <a class="pl-4 hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(route('contas-quadrimestrais')); ?>">
                                 Contas Quadrimestrais
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700" href="<?php echo e(route('rn-em-foco')); ?>">
                                 RN em foco
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              RN + fácil
                           </li>
                           <li>
                              <a class="pl-4 hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(asset('docs/contabilidadefacil/rnmaisfacil/exercicio2021.pdf')); ?>"
                                 target="_blank">
                                 Exercício 2021
                              </a>
                           </li>
                           <li>
                              <a class="pl-4 hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(asset('docs/contabilidadefacil/rnmaisfacil/exercicio2020.pdf')); ?>"
                                 target="_blank">
                                 Exercício 2020
                              </a>
                           </li>
                           <li>
                              <a class="pl-4 hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(asset('docs/contabilidadefacil/rnmaisfacil/exercicio2019.pdf')); ?>"
                                 target="_blank">
                                 Exercício 2019
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(route('dicas-execucao-orcamentaria')); ?>">
                                 Dicas de Execução Orçamentária
                              </a>
                           </li>
                        </ol>
                     </div>
                  </div>
               </div>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
               <div class="bg-white h-full border border-lightBlue-600 rounded-md shadow p-2">
                  <div class="flex flex-row items-center">
                     <div class="flex-1 text-left">
                        <h3 class="font-semibold uppercase text-gray-800 pb-2" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           RN Mais Vacina
                        </h3>
                        <ol class="text-sm text-lightBlue-500" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(route('rn-mais-vacina', ['status' => 'transparencia'])); ?>">
                                 RN + Vacina
                              </a>
                           </li>
                        </ol>
                     </div>
                  </div>
               </div>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
               <div class="bg-white h-full border border-lightBlue-600 rounded-md shadow p-2">
                  <div class="flex flex-row items-center">
                     <div class="flex-1 text-left">
                        <h3 class="font-semibold uppercase text-gray-800 pb-2" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           CONTROL
                        </h3>
                        <ol class="text-sm text-lightBlue-500" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(url('/control')); ?>">
                                 CONTROL
                              </a>
                           </li>
                        </ol>
                     </div>
                  </div>
               </div>
            </div>

            

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
               <div class="bg-white border border-lightBlue-600 rounded-md shadow h-full p-2">
                  <div class="flex flex-row items-center">
                     <div class="flex-1 text-left">
                        <h3 class="font-semibold uppercase text-gray-800 pb-2" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           Navegação
                        </h3>
                        <ol class="text-sm text-lightBlue-500" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(route('todos-tipos-gastos')); ?>">
                                 Tipos-Gastos
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(asset('ordem-cronologica')); ?>">
                                 Ordem Cronológica
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(route('orgaos-do-governo')); ?>">
                                 Órgãos do Governo (Cartas de Serviços)
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(route('compras-publicas-nfe')); ?>">
                                 Compras Públicas - NFE
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(route('empresas-inidoneas')); ?>">
                                 Empresas Inidôneas
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(route('conselhos-estaduais')); ?>">
                                 Conselhos Estaduais
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(asset('http://www.sic.rn.gov.br/')); ?>" target="_blank">
                                 Solicitações de Informações (E-Sic)
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(asset('http://www.cidadao.rn.gov.br/')); ?>" target="_blank">
                                 Serviços ao Cidadão
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(asset('https://sistema.ouvidorias.gov.br/publico/RN/manifestacao/RegistrarManifestacao')); ?>"
                                 target="_blank">
                                 Ouvidoria
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(asset('https://falabr.cgu.gov.br/Login/Identificacao/Identificacao.aspx?idFormulario=4&tipo=1&ReturnUrl=%2fpublico%2fManifestacao%2fRegistrarManifestacao.aspx%3fidFormulario%3d4%26tipo%3d1%26origem%3didp%26modo%3d')); ?>"
                                 target="_blank">
                                 Denúncia de Corrupção (Órgão gestor CONTROLADORIA RN)
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(asset('https://sipac.rn.gov.br/public/ContratosPublic.do?aba=p-contratos&acao=156')); ?>"
                                 target="_blank">
                                 Contratos
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(asset('http://201.76.150.19:8080/conveniorn/conveniorelsite.aspx')); ?>"
                                 target="_blank">
                                 Convênios
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(asset('http://servicos.searh.rn.gov.br/searh/Licitacao')); ?>" target="_blank">
                                 Portal de Compras
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(asset('http://www.seplan.rn.gov.br/Conteudo.asp?TRAN=PASTAC&TARG=2474&ACT=&PAGE=0&PARM=&LBL=Transpar%EAncia')); ?>"
                                 target="_blank">
                                 Transferência para Municípios
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(asset('https://sistemas.idema.rn.gov.br/servicos/')); ?>" target="_blank">
                                 Idema – Transparência Ambiental
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(asset('http://www.set.rn.gov.br/contentProducao/aplicacao/set_v2/legislacao/enviados/index_incentivos_fiscais-atos_declaratorios.asp')); ?>"
                                 target="_blank">
                                 Renúncia de Receita
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(asset('https://app.powerbi.com/view?r=eyJrIjoiZThjNjQxMDUtYTI3Ni00NjY3LTgyZDktMjBkYmJjMzZkNTA4IiwidCI6IjZiZGM1YTAyLWI4YTEtNDFhZS1iMmQ4LTUxNmQwYTU1NmRiMyJ9')); ?>"
                                 target="_blank">
                                 Obras - Governo Cidadão
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(asset('http://sipat.rn.gov.br/')); ?>"
                                 target="_blank">
                                 Patrimômio Imobiliário
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(asset('emendas-parlamentares')); ?>"
                                 target="_blank">
                                 Emendas Parlamentares
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(asset('https://portaldatransparencia.gov.br/convenios/consulta?paginacaoSimples=true&tamanhoPagina=&offset=&direcaoOrdenacao=asc&tipoConvenente=8&uf=RN&colunasSelecionadas=linkDetalhamento%2CnumeroConvenio%2Cuf%2CmunicipioConvenente%2Csituacao%2CtipoTransferencia%2Cobjetivo%2CorgaoSuperior%2Corgao%2Cconcedente%2Cconvenente%2CdataInicioVigencia%2CdataFimVigencia%2CvalorLiberado%2CvalorCelebrado&ordenarPor=orgao&direcao=desc')); ?>"
                                 target="_blank">
                                 Transferências Recebidas
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(asset('https://radar.tce.mt.gov.br/extensions/radar-da-transparencia-publica/radar-da-transparencia-publica.html')); ?>"
                                 target="_blank">
                                 Radar da Transparência Pública
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(asset('defensores-dativos')); ?>"
                                 target="_blank">
                                 Despesas com Defensores Dativos
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(asset('http://nucleo.pge.rn.gov.br/')); ?>"
                                 target="_blank">
                                 Dívida Ativa
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(asset('http://www.educacao.rn.gov.br/Conteudo.asp?TRAN=ITEM&TARG=172320&ACT=&PAGE=0&PARM=&LBL=Documentos')); ?>"
                                 target="_blank">
                                 Educação
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(asset('saude')); ?>"
                                 target="_blank">
                                 Saúde
                              </a>
                           </li>
                        </ol>
                     </div>
                  </div>
               </div>
            </div>

         </div>

         <div class="text-center">
            <h1 class="font-bold text-lg text-gray-900 pt-8" name="texto"
               style="font-size: 1.125rem; line-height: 1.75rem;">
               <a class="hover:underline hover:text-lightBlue-700" href="<?php echo e(url('/covid')); ?>">
                  TRANSPARÊNCIA COVID-19
               </a>
            </h1>
         </div>

         <div class="flex flex-wrap pt-5">

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
               <div class="bg-white h-full border border-green-600 rounded-md shadow p-2">
                  <div class="flex flex-row items-center">
                     <div class="flex-1 text-left">
                        <h3 class="font-semibold uppercase text-gray-800 pb-2" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           Transparência
                        </h3>
                        <ol class="text-sm text-lightBlue-500" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(route('compras-e-servicos')); ?>">
                                 Despesas – Compras e Serviços
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              Pedidos da Informação Covid-19
                           </li>
                           <li>
                           <a class="pl-4 hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(route('e-SIC-2022')); ?>"
                                 target="_blank">
                                 Pedidos 2022
                              </a>
                           </li>   
                           <li>
                              <a class="pl-4 hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(route('e-SIC-2021')); ?>"
                                 target="_blank">
                                 Pedidos 2021
                              </a>
                           </li>
                           <li>
                              <a class="pl-4 hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(route('e-SIC-2020')); ?>"
                                 target="_blank">
                                 Pedidos 2020
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              Relatório Estatístico da LAI
                           </li>
                           <li>
                              <a class="pl-4 hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(route('Relatorio Lai-2022')); ?>"
                                 target="_blank">
                                 Relatório – 2022
                              </a>
                           </li>
                           <li>
                              <a class="pl-4 hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(route('Relatorio Lai-2021')); ?>"
                                 target="_blank">
                                 Relatório – 2021
                              </a>
                           </li>
                           <li>
                              <a class="pl-4 hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(route('Relatorio Lai-2020')); ?>"
                                 target="_blank">
                                 Relatório – 2020
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              Relatório de Manifestações da Ouvidoria
                           </li>
                           <li>
                              <a class="pl-4 hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(route('Relatorio Ouv-2022')); ?>"
                                 target="_blank">
                                 Relatório Ouvidoria – 2022
                              </a>
                           </li>
                           <li>
                              <a class="pl-4 hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(route('Relatorio Ouv-2021')); ?>"
                                 target="_blank">
                                 Relatório Ouvidoria – 2021
                              </a>
                           </li>
                           <li>
                              <a class="pl-4 hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(route('Relatorio Ouv-2020')); ?>"
                                 target="_blank">
                                 Relatório Ouvidoria – 2020
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              Estímulos a Economia
                           </li>
                           <li>
                              <a class="pl-4 hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(url('http://www.set.rn.gov.br/contentProducao/aplicacao/set_v2/principal/arquivos/apresentacao-ilustrada-medidas-SET-enfrentamento-COVID-19.pdf')); ?>"
                                 target="_blank">
                                 Medidas
                              </a>
                           </li>
                           <li class="pl-4">
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(url('http://www.set.rn.gov.br/contentProducao/aplicacao/set_v2/principal/arquivos/consolidacao-medidas-SET.pdf')); ?>"
                                 target="_blank">
                                 Normativos
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(url('https://docs.google.com/document/d/1HXUeuU1Nd_R1-oQL5UZqYLa8p6FlC2mJl3LYvbuMO08/edit#')); ?>"
                                 target="_blank">
                                 Proteção Social
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(asset('docs/divida_publica/LC 173 2020 - Aplicação da Dívida Pública.pdf')); ?>"
                                 target="_blank">
                                 Aplicações Dívida Pública (LC 173/2020)
                              </a>
                           </li>
                        </ol>
                     </div>
                  </div>
               </div>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
               <div class="bg-white h-full border border-green-600 rounded-md shadow p-2">
                  <div class="flex flex-row items-center">
                     <div class="flex-1 text-left">
                        <h3 class="font-semibold uppercase text-gray-800 pb-2" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           Legislação
                        </h3>
                        <ol class="text-sm text-lightBlue-500" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(url('http://www.adcon.rn.gov.br/ACERVO/control/DOC/DOC000000000238551.PDF')); ?>"
                                 target="_blank">
                                 Decretos por Temas
                              </a>
                           </li>
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(route('instrucao-normativa')); ?>">
                                 Instrução Normativa
                              </a>
                           </li>
                        </ol>
                     </div>
                  </div>
               </div>
            </div>

            

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
               <div class="bg-white h-full border border-green-600 rounded-md shadow p-2">
                  <div class="flex flex-row items-center">
                     <div class="flex-1 text-left">
                        <h3 class="font-semibold uppercase text-gray-800 pb-2" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           RN Mais Vacina
                        </h3>
                        <ol class="text-sm text-lightBlue-500" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(route('rn-mais-vacina', ['status' => 'transparencia'])); ?>">
                                 RN + Vacina
                              </a>
                           </li>
                        </ol>
                     </div>
                  </div>
               </div>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
               <div class="bg-white h-full border border-green-600 rounded-md shadow p-2">
                  <div class="flex flex-row items-center">
                     <div class="flex-1 text-left">
                        <h3 class="font-semibold uppercase text-gray-800 pb-2" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           Portal Saúde
                        </h3>
                        <ol class="text-sm text-lightBlue-500" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(url('https://portalcovid19.saude.rn.gov.br/')); ?>" target="_blank">
                                 Portal Saúde
                              </a>
                           </li>
                        </ol>
                     </div>
                  </div>
               </div>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
               <div class="bg-white h-full border border-green-600 rounded-md shadow p-2">
                  <div class="flex flex-row items-center">
                     <div class="flex-1 text-left">
                        <h3 class="font-semibold uppercase text-gray-800 pb-2" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           CONTROL
                        </h3>
                        <ol class="text-sm text-lightBlue-500" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(url('/control')); ?>" target="_blank">
                                 CONTROL
                              </a>
                           </li>
                        </ol>
                     </div>
                  </div>
               </div>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
               <div class="bg-white h-full border border-green-600 rounded-md shadow p-2">
                  <div class="flex flex-row items-center">
                     <div class="flex-1 text-left">
                        <h3 class="font-semibold uppercase text-gray-800 pb-2" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           Navegação
                        </h3>
                        <ol class="text-sm text-lightBlue-500" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           <li>
                              <i class="fas fa-angle-right"></i>
                              <a class="hover:underline hover:text-lightBlue-700"
                                 href="<?php echo e(asset('docs/leialdirblanc/ALDIR BLANC RN - LISTA GERAL DE CONTEMPLADOS.xlsx')); ?>"
                                 target="_blank">
                                 Lei Aldir Blanc
                              </a>
                           </li>
                        </ol>
                     </div>
                  </div>
               </div>
            </div>

         </div>

      </div>
   </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/portaltransparencianewrn/resources/views/mapa-do-site/index.blade.php ENDPATH**/ ?>