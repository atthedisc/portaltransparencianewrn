<?php $__env->startSection('css'); ?>
<link rel="stylesheet" href="<?php echo e(asset('css/exportfiles.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('css/jquery.treegrid.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<style>.new-collapse{display:none;}</style>

    
    <h1 class="text-lg text-gray-900 tracking-widest font-medium title-font uppercase text-center pb-2" name="texto" style="font-size: 1.125rem; line-height: 2rem;">
       <i class="fas fa-search-dollar"></i>
       Despesa
    </h1>
    <ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
       <li class="inline-flex items-center">
          <a href="<?php echo e(url('/')); ?>">Inicio</a>
          <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
             <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd"></path>
          </svg>
       </li>
       <li class="inline-flex items-center">
          <a href="<?php echo e(route('despesas')); ?>">Despesas</a>
          <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
             <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd"></path>
          </svg>
       </li>
       <li class="inline-flex items-center">
          <a href="#" class="text-teal-400">Despesa</a>
       </li>
    </ul>
    <hr class="border-b-2 border-lightBlue-300 mb-12 mx-4">

    
    <div class="relative flex flex-col-reverse md:flex-row gap-3 w-full mb-4">
       <div class="flex px-4 pb-4 bg-white h-auto w-full border-b-4 border-lightBlue-300 rounded-md flex-col">
          <div id="container" class="h-full w-full"></div>
       </div>

       <div class="flex px-4 py-4 justify-center bg-white border-b-4 border-lightBlue-300 rounded-md flex-col">
          <div class="px-5 py-4 bg-white border border-gray-200 rounded-md">
             <div class="card rounded-xl flex flex-col">
                <div class="grid grid-row-4 md:grid-row-4 gap-3">
                   <div class="py-3 pl-5 bg-gray-50 text-sm rounded-md shadow" name="texto"
                      style="font-size: 0.875rem; color: #333333; line-height: 2.25rem;">
                      <ul class="flex flex-wrap">
                         <li><span class="font-semibold text-red-600">Despesa total:</span></li>
                         <li class="w-full">R$ <?php echo e(number_format($sumtotal, 2, ',', '.')); ?></li>
                      </ul>
                   </div>
                   <div class="py-3 pl-5 bg-gray-50 text-sm rounded-md shadow" name="texto"
                      style="font-size: 0.875rem; color: #333333; line-height: 2.25rem;">
                      <ul class="flex flex-wrap">
                         <li class="font-semibold mr-2 text-gray-600">Classificação: </li>
                         <li class="w-full">Fonte de Recurso</li>
                      </ul>
                   </div>
                   <div class="py-3 pl-5 bg-gray-50 text-sm rounded-md shadow" name="texto"
                      style="font-size: 0.875rem; color: #333333; line-height: 2.25rem;">
                      <ul class="flex flex-wrap">
                         <?php if($posicao == '=' or $mes == 1): ?>
                         <li class="font-semibold mr-2 text-gray-600">Exercício: </li>
                         <li class="w-full"><?php echo app('translator')->get('meses.'.$mes); ?></li>
                         <?php else: ?>
                         <li class="font-semibold mr-2 text-gray-600">Periodo: </li>
                         <li class="w-full">Janeiro a
                            <?php echo e(strtolower(__('meses.'.$mes))); ?></li>
                         <?php endif; ?>
                      </ul>
                   </div>
                   <div class="py-3 pl-5 bg-gray-50 text-sm rounded-md shadow" name="texto"
                      style="font-size: 0.875rem; color: #333333; line-height: 2.25rem;">
                      <ul class="flex flex-wrap">
                         <li class="font-semibold mr-2 text-gray-600">Ano: </li>
                         <li class="w-full"><?php echo e($ano); ?></li>
                      </ul>
                   </div>
                </div>
             </div>
          </div>
       </div>
    </div>

    
    <div class="w-full max-w-xl mx-auto">
       <div class="px-7 bg-white shadow-lg rounded-2xl mb-5">
          <div class="flex">
             <!-- Ajustar a exportação do arquivo xls e pdf -->
             <div class="flex-auto hover:w-full group">
                <form target="_blank" action="<?php echo e(route('export-xls-gastos-diretos', [
                   'mes' => $mes,
                   'ano' => $ano,
                   'posicao' => $posicao,
                   'classificacao' => $classificacao
                ])); ?>" method="POST">
                   <?php echo csrf_field(); ?>
                   <button type="submit"
                      class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-green-500">
                      <span
                         class="block px-1 py-1 border border-transparent group-hover:border-green-500 rounded-full group-hover:flex-grow">
                         <i class="far fa-file-excel text-2xl pt-1"></i><span
                            class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Baixar arquivo XLS</span>
                      </span>
                   </button>
                </form>
             </div>
             <div class="flex-auto hover:w-full group">
                <form target="_blank" action="<?php echo e(route('export-pdf-gastos-diretos', [
                   'mes' => $mes,
                   'ano' => $ano,
                   'posicao' => $posicao,
                   'classificacao' => $classificacao
                ])); ?>" method="POST">
                   <?php echo csrf_field(); ?>
                   <button type="submit"
                      class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-red-500">
                      <span
                         class="block px-1 py-1 border border-transparent group-hover:border-red-500 rounded-full group-hover:flex-grow">
                         <i class="far fa-file-pdf text-2xl pt-1"></i><span
                            class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Baixar arquivo PDF</span>
                      </span>
                   </button>
                </form>
             </div>
             <div class="flex-auto hover:w-full group">
                <a href="#" onclick="window.print()"
                   class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-blueGray-500">
                   <span
                      class="block px-1 py-1 border border-transparent group-hover:border-blueGray-500 rounded-full group-hover:flex-grow">
                      <i class="fas fa-print text-2xl pt-1"></i><span
                         class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Imprimir arquivo</span>
                   </span>
                </a>
             </div>
             <div class="flex-auto hover:w-full group">
                <a href="<?php echo e(asset('docs/dicionario-de-dados/Dicionario de Dados Abertos-RN - Despesa - Gastos direto.xlsx')); ?>"
                   target="_blank"
                   class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-indigo-500">
                   <span
                      class="block px-1 py-1 border border-transparent group-hover:border-indigo-500 rounded-full group-hover:flex-grow">
                      <i class="far fa-file-alt text-2xl pt-1"></i><span
                         class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Dicionario de dados</span>
                   </span>
                </a>
             </div>
             <div class="flex-auto hover:w-full group">
                <a href="https://creativecommons.org/licenses/by/4.0/?ref=chooser-v1" target="_blank"
                   class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-orange-500">
                   <span
                      class="block px-1 py-1 border border-transparent group-hover:border-orange-500 rounded-full group-hover:flex-grow">
                      <i class="far fa-closed-captioning text-2xl pt-1"></i>
                      <span class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Licença</span>
                   </span>
                </a>
             </div>
          </div>
       </div>
    </div>

    <?php echo $busca; ?>

    <div class="flex flex-col pb-4">
       <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
          <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
             <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                 
                <table id="tree" class="min-w-full divide-y divide-gray-200">
                   <thead class="bg-lightBlue-100">
                      <tr>
                         <th class="px-6 py-3 text-left text-sm font-semibold text-gray-800 uppercase tracking-wider">
                            Função
                         </th>
                         <th class="px-6 py-3 text-left text-sm font-semibold text-gray-800 uppercase tracking-wider">
                            Subfunção
                         </th>
                         <th class="px-6 py-3 text-left text-sm font-semibold text-gray-800 uppercase tracking-wider">
                            Ação
                         </th>
                         <th class="px-6 py-3 text-left text-sm font-semibold text-gray-800 uppercase tracking-wider">
                            Valor Empenhado
                         </th>
                         <th class="px-6 py-3 text-left text-sm font-semibold text-gray-800 uppercase tracking-wider">
                            Valor Liquidação
                         </th>
                         <th class="px-6 py-3 text-left text-sm font-semibold text-gray-800 uppercase tracking-wider">
                            Valor Pagamento
                         </th>
                         <th class="px-6 py-3 text-right text-sm font-semibold text-gray-800 uppercase tracking-wider">
                            Restos a Pagar
                         </th>
                      </tr>
                   </thead>
                   <tbody id="table" data-resetable="false" class="bg-white divide-y divide-gray-200 text-sm">
                       <?php $__currentLoopData = $funcoes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $funcao): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           <tr data-key="<?php echo e($key); ?>" data-fetch="false">
                              <td class="px-6 py-4 whitespace-nowrap">
                                 <i onclick="showDetail(<?php echo e($key); ?>)" id="icon-<?php echo e($key); ?>" class="fa fa-plus-circle" aria-hidden="true" style="cursor:pointer; font-size:16px; color:#77c600;"></i>
                                 <?php echo e($funcao->txtdescricaofuncao); ?>

                              </td>
                              <td class="px-6 py-4 whitespace-nowrap"><?php echo e($funcao->txtdescricaosubfuncao); ?></td>
                              <td class="px-6 py-4 whitespace-nowrap"><?php echo e($funcao->txtdescricaoacao); ?></td>
                              <td class="px-6 py-4 whitespace-nowrap">
                                 <div class="flex items-center justify-end gap-1">
                                    <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-yellow-100 text-yellow-800">
                                       R$ <?php echo e(number_format($funcao->vlrempenho, 2, ',', '.')); ?>

                                    </span>
                                       
                                 </div>
                              </td>

                              <td class="px-6 py-4 whitespace-nowrap">
                                 <div class="flex items-center justify-end gap-1">
                                    <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-yellow-100 text-yellow-800">
                                          R$ <?php echo e(number_format($funcao->vlrliquido, 2, ',', '.')); ?>

                                    </span>
                                    
                                 </div>
                              </td>

                              <td class="px-6 py-4 whitespace-nowrap">
                                 <div class="flex items-center justify-end gap-1">
                                    <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-yellow-100 text-yellow-800">
                                          R$ <?php echo e(number_format($funcao->vlrpagamento, 2, ',', '.')); ?>

                                    </span>
                                    
                                 </div>
                              </td>

                              <td class="px-6 py-4 whitespace-nowrap text-right">
                                 <div class="flex items-center gap-1">
                                 <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800">
                                       R$ <?php echo e(number_format($funcao->vlrpagamentorp, 2, ',', '.')); ?>

                                 </span>
                                 <?php $__currentLoopData = explode(',', $funcao->result); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dados_notPag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                       <?php list($fase_notPag, $tipo_notPag) = explode('/', $dados_notPag); ?>
                                       <?php if("vportal_notapagamento_gastosdiretos" == $fase_notPag && "valorPagar"  == $tipo_notPag): ?>
                                          <a class="inline-block" href="<?php echo e(route('gastos-diretos-detalhada', [ 'request' => Crypt::encrypt([ 'codgrupodespesa' => 'codgrupodespesa', 'codelementodespesa' => 'codelementodespesa', 'codunidadegestora' => 'codunidadegestora', 'codgestao' => 'codgestao', 'codcredor' => 'codcredor', 'codacao' => $funcao->codacao, 'codfonterecurso' => 'codfonterecurso', 'txtdescricaofuncao' => $funcao->txtdescricaofuncao, 'codsubfuncao' => $funcao->codsubfuncao, 'fase' => $fase_notPag, 'mes' => $mes, 'ano' => $ano, 'classificacao' => $classificacao, 'posicao' => $posicao, 'txtdescricaoacao' => $funcao->txtdescricaoacao ]) ])); ?>">
                                              <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 text-lightBlue-500" viewBox="0 0 20 20" fill="currentColor">
                                                 <path d="M11 3a1 1 0 100 2h2.586l-6.293 6.293a1 1 0 101.414 1.414L15 6.414V9a1 1 0 102 0V4a1 1 0 00-1-1h-5z" />
                                                 <path d="M5 5a2 2 0 00-2 2v8a2 2 0 002 2h8a2 2 0 002-2v-3a1 1 0 10-2 0v3H5V7h3a1 1 0 000-2H5z" />
                                              </svg>
                                           </a>
                                       <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                 </div>
                              </td>
                           </tr>

                           
                           <tr id="cabecalho-<?php echo e($key); ?>" class="new-collapse treegrid-parent-5 bg-gray-100">
                              <th class="px-6 py-3 text-left text-sm font-semibold text-gray-800 uppercase tracking-wider" name="texto" style="font-size: 0.675rem; line-height: 1.25rem;">
                                 AÇÃO
                              </th>
                           </tr>
                            <tr id="<?php echo e($key); ?>" class="new-collapse" style="height:70px !important;">
                                <th class="px-6 py-3 text-center text-sm font-semibold text-gray-800 uppercase tracking-wider" name="texto" style="font-size: 0.775rem; line-height: 1.25rem;">
                                   <?php echo e($funcao->txtdescricaoacao); ?>:
                                </th>
                                <?php $__currentLoopData = explode(',', $funcao->result); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dados_emp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php list($fase_emp, $tipo_emp) = explode('/', $dados_emp); ?>
                                       <?php if("vportal_notaempenho_gastosdiretos" == $fase_emp && "notaEmpenho" == $tipo_emp): ?>
                                       <td>
                                             <a class="inline-block" href="<?php echo e(route('gastos-diretos-detalhada', [ 'request' => Crypt::encrypt([ 'codgrupodespesa' => 'codgrupodespesa', 'codelementodespesa' => 'codelementodespesa', 'codunidadegestora' => 'codunidadegestora', 'codgestao' => 'codgestao', 'codcredor' => 'codcredor', 'codacao' => $funcao->codacao, 'codfonterecurso' => 'codfonterecurso', 'txtdescricaofuncao' => $funcao->txtdescricaofuncao, 'codsubfuncao' => $funcao->codsubfuncao, 'fase' => $fase_emp, 'mes' => $mes, 'ano' => $ano, 'classificacao' => $classificacao, 'posicao' => $posicao, 'txtdescricaoacao' => $funcao->txtdescricaoacao ]) ])); ?>">
                                             <span style="color:#0d98d9;">Empenho <i class="fas fa-external-link-alt"></i></span>
                                             </a>
                                          </td>
                                          <?php else: ?>
                                          
                                       <?php endif; ?>
                                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                 <?php $__currentLoopData = explode(',', $funcao->result); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dados_liq): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                       <?php list( $fase_liq, $tipo_liq) = explode('/', $dados_liq); ?>
                                       <?php if("vportal_liquidacaodespesa_gastosdiretos" == $fase_liq && "liquidacao" == $tipo_liq): ?>
                                       <td>
                                          <a class="inline-block" href="<?php echo e(route('gastos-diretos-detalhada', [ 'request' => Crypt::encrypt([ 'codgrupodespesa' => 'codgrupodespesa', 'codelementodespesa' => 'codelementodespesa', 'codunidadegestora' => 'codunidadegestora', 'codgestao' => 'codgestao', 'codcredor' => 'codcredor', 'codacao' => $funcao->codacao, 'codfonterecurso' => 'codfonterecurso', 'txtdescricaofuncao' => $funcao->txtdescricaofuncao, 'codsubfuncao' => $funcao->codsubfuncao, 'fase' => $fase_liq, 'mes' => $mes, 'ano' => $ano, 'classificacao' => $classificacao, 'posicao' => $posicao, 'txtdescricaoacao' => $funcao->txtdescricaoacao ]) ])); ?>">
                                             <span style="color:#0d98d9;">Liquidação <i class="fas fa-external-link-alt"></i></span>
                                          </a>
                                       </td>
                                       <?php else: ?>
                                       
                                       <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    <?php $__currentLoopData = explode(',', $funcao->result); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dados_notPag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php list($fase_notPag, $tipo_notPag) = explode('/', $dados_notPag); ?>
                                    <?php if("vportal_notapagamento_gastosdiretos" == $fase_notPag && "notaPagamento"  == $tipo_notPag): ?>
                                    <td>
                                       <a class="inline-block" href="<?php echo e(route('gastos-diretos-detalhada', [ 'request' => Crypt::encrypt([ 'codgrupodespesa' => 'codgrupodespesa', 'codelementodespesa' => 'codelementodespesa', 'codunidadegestora' => 'codunidadegestora', 'codgestao' => 'codgestao', 'codcredor' => 'codcredor', 'codacao' => $funcao->codacao, 'codfonterecurso' => 'codfonterecurso', 'txtdescricaofuncao' => $funcao->txtdescricaofuncao, 'codsubfuncao' => $funcao->codsubfuncao, 'fase' => $fase_notPag, 'mes' => $mes, 'ano' => $ano, 'classificacao' => $classificacao, 'posicao' => $posicao, 'txtdescricaoacao' => $funcao->txtdescricaoacao ]) ])); ?>">
                                          <span style="color:#0d98d9;">Pagamento <i class="fas fa-external-link-alt"></i></span>
                                        </a>
                                       </td>
                                       <?php else: ?>
                                       
                                    <?php endif; ?>
                                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <td colspan="4"></td>
                            </tr>

                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
                </table>
                 
                <div id="pag"><?php echo e($funcoes->appends(Request::all())->links()); ?></div>
             </div>
          </div>
       </div>
    </div>

    <div class="alert flex flex-row items-center bg-blue-200 p-1 rounded">
       <div
          class="alert-icon flex items-center bg-blue-100 border-2 border-blue-500 justify-center flex-shrink-0 rounded-full">
          <span class="text-blue-500">
             <svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
                <path fill-rule="evenodd"
                   d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z"
                   clip-rule="evenodd"></path>
             </svg>
          </span>
       </div>
       <div class="alert-content ml-4">
          <div class="alert-description text-xs text-blue-600">
             <span class="font-semibold">Dados atualizados em:</span> <?php echo e(date("d/m/Y")); ?> | <span class="font-semibold">Fonte:</span>
             SIGEF - SISTEMA INTEGRADO DE PLANEJAMENTO E GESTÃO FISCAL (Poder Executivo).
          </div>
       </div>
    </div>

<meta id="info" data-source="despesa" data-page="despesa" data-reloadjs="true" data-classificacao="<?php echo e($classificacao); ?>" data-mes="<?php echo e($mes); ?>" fase="<?php echo e($fase); ?>" data-ano="<?php echo e($ano); ?>" data-posicao="<?php echo e($posicao); ?>">

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="<?php echo e(asset('js/select2.full.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery.treegrid.min.js')); ?>"></script>

<script> //Função que recarrega o treegrid
   function reload_js(src) {
      $('script[src="' + src + '"]').remove();
      $('<script>').attr('src', src).appendTo('#content');
      $("#tree").treegrid({ initialState: 'collapsed' });
   }
</script>

<script>
   $("#tree").treegrid({ initialState: 'collapsed' });
</script>

<script>
   function showDetail(param) {

       let collapse = document.getElementById(param);
       let cabecalho = document.getElementById('cabecalho-'+param);
       let icon = document.getElementById('icon-'+param);

           if (collapse.classList.contains('new-collapse')) {
               collapse.classList.remove('new-collapse');
           } else {
               collapse.classList.add('new-collapse');
           }

           if (cabecalho.classList.contains('new-collapse')) {
               cabecalho.classList.remove('new-collapse');
           } else {
               cabecalho.classList.add('new-collapse');
           }

           if (icon.classList.contains('fa-plus-circle')) {
               icon.classList.remove('fa-plus-circle');
               icon.classList.add('fa-minus-circle');
               icon.style.color='red';
           } else {
             icon.classList.remove('fa-minus-circle');
               icon.classList.add('fa-plus-circle');
               icon.style.color='#77c600';
           }
   }
</script>


<script src="<?php echo e(asset('js/search.js')); ?>"></script>

<script src="<?php echo e(asset('js/dynamicPage.js')); ?>"></script>

<script>
   const formatNumber = new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' })

   <?php if($fase == 'vportal_notapagamento_gastosdiretos'): ?>
      const title = 'Total da Despesa paga'
   <?php elseif($fase == 'vportal_notaempenho_gastosdiretos'): ?>
      const title = 'Total da Despesa empenhada'
   <?php else: ?>
      const title = 'Total da Despesa liquidada'
   <?php endif; ?>

   <?php if($posicao == '=' or $mes == 1): ?>
      const subtitle = `<?php echo e(strtolower(__('meses.'.$mes))); ?> de <?php echo e($ano); ?>`
   <?php else: ?>
      const subtitle = `janeiro a <?php echo e(strtolower(__('meses.'.$mes))); ?> de <?php echo e($ano); ?>`
   <?php endif; ?>

// Create the chart
Highcharts.setOptions({
      lang: {
         drillUpText: `◁ Voltar`
      }
});

Highcharts.chart('container', {
    chart: {
         type: 'column',
         spacingBottom: 30
    },

    credits : {
      style: {
         color: '#333'
      }
    },

    title: {
        text: 'Total das Despesas'
    },

    subtitle: {
        text: subtitle
    },

    accessibility: {
        announceNewData: {
            enabled: true
        }
    },

    xAxis: {
        type: 'category'
    },

    yAxis: {
        title: {
            text: 'Despesas em R$'
        },
        labels : {
           enabled: true,
           formatter: function() {
              return formatNumber.format(this.value ** 3)
           }
        }
    },

    legend: {
        enabled: false
    },

    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                formatter: function() {
                   return formatNumber.format(this.point.y ** 3)
                }
            }
        }
    },

    tooltip: {
        formatter: function() {
           return '<span style="font-size:11px">' + this.series.name + '</span><br>' +
           '<span style="color:' + this.point.color + '">' + this.point.name + '</span>: <b>' +
           formatNumber.format(this.point.y ** 3) + '</b> de despesa<br/>'
         }
    },

    series: [
           {
               name: "Função",
               colorByPoint: true,
               data: [
                  <?php $__currentLoopData = $grupofuncaocharts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $funcao): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                   {
                        name: `<?php echo e($funcao->txtdescricaofuncao); ?>`,
                        <?php if($funcao->vlrpagamento > 0): ?>
                           y: Math.pow(<?php echo e($funcao->vlrpagamento); ?>, 1 / 3),
                        <?php endif; ?>
                   },
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
               ]
           }
       ],
    drilldown: {

        activeAxisLabelStyle: {
            textDecoration: 'none',
            color: '#666',
            fontWeight: 'normal'
        },

        activeDataLabelStyle: {
            textDecoration: 'none',
            color: '#000'
        },

        series: [
           <?php $__currentLoopData = $grupofuncaocharts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $funcao): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            {
                name: `Subfunção`,
                data: [
                  
               ]
            },
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
         ]
    }
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/portaltransparencianewrn/resources/views/despesas/gastos-diretos/funcao/index.blade.php ENDPATH**/ ?>