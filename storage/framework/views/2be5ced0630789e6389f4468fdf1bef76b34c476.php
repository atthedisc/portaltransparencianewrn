<?php $__env->startSection('css'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('css/carousel.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/welcome.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/welcomegasto.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/loading.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('js/carousel.js')); ?>"></script>
    <script src="<?php echo e(asset('js/countUp.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('title'); ?>
- Início
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <?php echo csrf_field(); ?>
    <div class="w-full px-4 text-gray-800 leading-normal">
        <div class="flex flex-wrap">
            <div class="w-full md:w-1/2 xl:w-1/2 p-3">
                <a id="conteudo" href="<?php echo e(route('receita')); ?>">
                    <div
                        class="bg-white border border-green-500 rounded-md shadow p-2 hover:border-green-500 hover:shadow-2xl transition duration-500 transform hover:scale-105">
                        <div class="flex flex-row items-center">
                            <div class="flex-shrink pr-4">
                                <div class="rounded-lg p-3 bg-green-500"><i
                                        class="fa fa-search-dollar fa-2x fa-fw fa-inverse"></i>
                                </div>
                            </div>
                            <div class="flex-1 text-right md:text-center">
                                <h3 class="font-semibold uppercase text-gray-600" name="texto"
                                    style="font-size: 1.125rem; line-height: 1.75rem;">
                                    Receita
                                    <span class="text-green-500">
                                        <i class="fas fa-caret-up"></i>
                                    </span>
                                </h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="w-full md:w-1/2 xl:w-1/2 p-3">
                <a href="<?php echo e(route('despesas')); ?>">
                    <div
                        class="bg-white border border-red-500 rounded-md shadow p-2 hover:border-red-500 hover:shadow-2xl transition duration-500 transform hover:scale-105">
                        <div class="flex flex-row items-center">
                            <div class="flex-shrink pr-4">
                                <div class="rounded p-3 bg-red-600"><i
                                        class="fas fa-search-dollar fa-2x fa-fw fa-inverse"></i>
                                </div>
                            </div>
                            <div class="flex-1 text-right md:text-center">
                                <h3 class="font-semibold uppercase text-gray-600" name="texto"
                                    style="font-size: 1.125rem; line-height: 1.75rem;">
                                    Despesa
                                    <span class="text-red-500">
                                        <i class="fas fa-caret-down"></i>
                                    </span>
                                </h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <div class="flex items-center justify-between px-4 py-4 border-b lg:py-2 lg:pt-10 dark:border-gray-darker">
            <h1 class="text-base md:text-xl" name="texto" style="font-size: 1rem; line-height: 1.5rem;">
                Despesas por função
            </h1>
            <a class="px-4 py-2 text-sm text-white rounded-md bg-lightBlue-500 hover:bg-lightBlue-800" id="vertodos"
                name="texto" href="<?php echo e(route('todos-tipos-gastos')); ?>" style="font-size: 0.875rem; line-height: 1.25rem;">
                Ver todos <i class="fas fa-arrow-right"></i>
            </a>
        </div>

        <div class="mt-2">

            <div class="border-b">
                <div class="grid lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 grid-cols-1 gap-4 px-4 pt-2 pb-4">

                    <a
                        href="<?php echo e(route('gastos-diretos-tipo-funcao', [
                            'funcao' => 'Educação',
                            'ano' => $ano,
                        ])); ?>">
                        <div
                            class="bg-blue-300 bg-opacity-95 p-2 rounded-3xl border border-blue-500 flex justify-around cursor-pointer hover:bg-blue-400 transition-colors duration-500">
                            <div class="rounded-full p-3 bg-blue-500"><i class="fa fa-book fa-2x fa-fw fa-inverse"></i>
                            </div>
                            <div class="flex flex-col justify-center">
                                <p class="text-gray-900 dark:text-gray-300 text-sm font-semibold" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Educação
                                </p>
                                <p class="text-black dark:text-gray-100 text-justify" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    R$ <span id="educacao"></span>
                                </p>
                            </div>
                        </div>
                    </a>

                    <a
                        href="<?php echo e(route('gastos-diretos-tipo-funcao', [
                            'funcao' => 'Saúde',
                            'ano' => $ano,
                        ])); ?>">
                        <div
                            class="bg-red-300 bg-opacity-95 p-2 rounded-3xl border border-red-500 flex justify-around cursor-pointer hover:bg-red-400 transition-colors duration-500">
                            <div class="rounded-full p-3 bg-red-500"><i class="fas fa-heartbeat fa-2x fa-fw fa-inverse"></i>
                            </div>
                            <div class="flex flex-col justify-center">
                                <p class="text-gray-900 dark:text-gray-300 text-sm font-semibold" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Saúde
                                </p>
                                <p class="text-black dark:text-gray-100 text-justify" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    R$ <span id="saude"></span>
                                </p>
                            </div>
                        </div>
                    </a>

                    <a
                        href="<?php echo e(route('gastos-diretos-tipo-funcao', [
                            'funcao' => 'Segurança Pública',
                            'ano' => $ano,
                        ])); ?>">
                        <div
                            class="bg-blueGray-300 bg-opacity-95 p-2 rounded-3xl border border-blueGray-500 flex justify-around cursor-pointer hover:bg-blueGray-400 transition-colors duration-500">
                            <div class="rounded-full p-3 bg-blueGray-800"><i class="fa fa-lock fa-2x fa-fw fa-inverse"></i>
                            </div>
                            <div class="flex flex-col justify-center">
                                <p class="text-gray-900 dark:text-gray-300 text-sm font-semibold" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Segurança
                                </p>
                                <p class="text-black dark:text-gray-100 text-justify" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    R$ <span id="seguranca"></span>
                                </p>
                            </div>
                        </div>
                    </a>

                    <a
                        href="<?php echo e(route('gastos-diretos-tipo-funcao', [
                            'funcao' => 'Transporte',
                            'ano' => $ano,
                        ])); ?>">
                        <div
                            class="bg-amber-300 bg-opacity-95 p-2 rounded-3xl border border-amber-500 flex justify-around cursor-pointer hover:bg-amber-400 transition-colors duration-500">
                            <div class="rounded-full p-3 bg-amber-500"><i class="fas fa-bus fa-2x fa-fw fa-inverse"></i>
                            </div>
                            <div class="flex flex-col justify-center">
                                <p class="text-gray-900 dark:text-gray-300 text-sm font-semibold" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Transporte
                                </p>
                                <p class="text-black dark:text-gray-100 text-justify" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    R$ <span id="transporte"></span>
                                </p>
                            </div>
                        </div>
                    </a>

                </div>
            </div>

            <!--Carousel-->
            
            <!-- /Carousel -->

        </div>

        <!--Console Content-->
        <div class="flex flex-wrap pt-5">

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
                <a
                    href="<?php echo e(route('consulta-publica')); ?>" target="_blank">
                    <div
                        class="bg-white border border-lightBlue-600 rounded-md shadow p-2 hover:bg-lightBlue-300 hover:border-transparent | transition-colors duration-500">
                        <div class="flex flex-row items-center">
                            <div class="flex-shrink pr-4">
                                <div class="rounded p-2 bg-lightBlue-600"><i
                                        class="fas fa-message fa-2x fa-fw fa-inverse"></i>
                                </div>
                            </div>
                            <div class="flex-1 text-right md:text-center">
                                <h3 class="font-semibold uppercase text-grey-800" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Consulta Pública
                                </h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
                <a href="<?php echo e(route('ordem-cronologica')); ?>">
                    <div
                        class="bg-white border border-lightBlue-600 rounded-md shadow p-2 hover:bg-lightBlue-300 hover:border-transparent | transition-colors duration-500">
                        <div class="flex flex-row items-center">
                            <div class="">
                                <div class="rounded p-2 bg-lightBlue-600"><i
                                        class="fas fa-coins fa-2x fa-fw fa-inverse"></i></div>
                            </div>
                            <div class="flex-1 text-right md:text-center">
                                <h3 class="font-semibold uppercase text-gray-800" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Ordem Cronológica
                                </h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
                <a href="<?php echo e(route('orgaos-do-governo')); ?>">
                    <div
                        class="bg-white border border-lightBlue-600 rounded-md shadow p-2 hover:bg-lightBlue-300 hover:border-transparent | transition-colors duration-500">
                        <div class="flex flex-row items-center">
                            <div class="flex-shrink pr-4">
                                <div class="rounded p-2 bg-lightBlue-600">
                                    <i class="fas fa-hotel fa-2x fa-fw fa-inverse"></i>
                                </div>
                            </div>
                            <div class="flex-1 text-right md:text-center">
                                <h3 class="font-semibold uppercase text-gray-800" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Órgãos do Governo
                                    <span class="text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                        (Cartas de Serviços)
                                    </span>
                                </h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
                <a href="<?php echo e(route('compras-publicas-nfe')); ?>">
                    <div
                        class="bg-white border border-lightBlue-600 rounded-md shadow p-2 hover:bg-lightBlue-300 hover:border-transparent | transition-colors duration-500">
                        <div class="flex flex-row items-center">
                            <div class="flex-shrink pr-4">
                                <div class="rounded p-2 bg-lightBlue-600"><i
                                        class="fas fa-file-invoice-dollar fa-2x fa-fw fa-inverse"></i>
                                </div>
                            </div>
                            <div class="flex-1 text-right md:text-center">
                                <h3 class="font-semibold uppercase text-gray-800" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Compras Públicas - NFe
                                </h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
                <a href="<?php echo e(route('empresas-inidoneas')); ?>">
                    <div
                        class="bg-white border border-lightBlue-600 rounded-md shadow p-2 hover:bg-lightBlue-300 hover:border-transparent | transition-colors duration-500">
                        <div class="flex flex-row items-center">
                            <div class="flex-shrink pr-4">
                                <div class="rounded p-2 bg-lightBlue-600"><i
                                        class="far fa-building fa-2x fa-fw fa-inverse"></i>
                                </div>
                            </div>
                            <div class="flex-1 text-right md:text-center">
                                <h3 class="font-semibold uppercase text-gray-800" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Empresas Inidôneas
                                </h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
                <a href="<?php echo e(route('conselhos-estaduais')); ?>">
                    <div
                        class="bg-white border border-lightBlue-600 rounded-md shadow p-2 hover:bg-lightBlue-300 hover:border-transparent | transition-colors duration-500">
                        <div class="flex flex-row items-center">
                            <div class="flex-shrink pr-4">
                                <div class="rounded p-2 bg-lightBlue-600">
                                    <i class="fas fa-users-cog fa-2x fa-fw fa-inverse"></i>
                                </div>
                            </div>
                            <div class="flex-1 text-right md:text-center">
                                <h3 class="font-semibold uppercase text-gray-800" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Conselhos Estaduais
                                </h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
                <a href="<?php echo e(route('solicitacao-informacoes')); ?>" target="_blank">
                    <div
                        class="bg-white border border-lightBlue-600 rounded-md shadow p-2 hover:bg-lightBlue-300 hover:border-transparent | transition-colors duration-500">
                        <div class="flex flex-row items-center">
                            <div class="flex-shrink pr-4">
                                <div class="rounded p-2 bg-lightBlue-600">
                                    <i class="fas fa-info-circle fa-2x fa-fw fa-inverse"></i>
                                </div>
                            </div>
                            <div class="flex-1 text-right md:text-center">
                                <h3 class="font-semibold uppercase text-gray-800" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Solicitações de informs.
                                    <span class="text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                        (e-Sic)
                                    </span>
                                </h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
                <a href="<?php echo e(route('servicos-ao-cidadao')); ?>" target="_blank">
                    <div
                        class="bg-white border border-lightBlue-600 rounded-md shadow p-2 hover:bg-lightBlue-300 hover:border-transparent | transition-colors duration-500">
                        <div class="flex flex-row items-center">
                            <div class="flex-shrink pr-4">
                                <div class="rounded p-2 bg-lightBlue-600"><i
                                        class="fas fa-user-shield fa-2x fa-fw fa-inverse"></i>
                                </div>
                            </div>
                            <div class="flex-1 text-right md:text-center">
                                <h3 class="font-semibold uppercase text-gray-800" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Serviços ao Cidadão
                                </h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
                <a href="<?php echo e(route('ouvidoria')); ?>">
                    <div
                        class="bg-white border border-lightBlue-600 rounded-md shadow p-2 hover:bg-lightBlue-300 hover:border-transparent | transition-colors duration-500">
                        <div class="flex flex-row items-center">
                            <div class="flex-shrink pr-4">
                                <div class="rounded p-2 bg-lightBlue-600"><i
                                        class="fas fa-comments fa-2x fa-fw fa-inverse"></i>
                                </div>
                            </div>
                            <div class="flex-1 text-right md:text-center">
                                <h3 class="font-semibold uppercase text-gray-800" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Ouvidoria
                                </h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
                <a href="<?php echo e(route('canal-de-denuncia')); ?>"
                    target="_blank">
                    <div
                        class="bg-white border border-lightBlue-600 rounded-md shadow p-2 hover:bg-lightBlue-300 hover:border-transparent | transition-colors duration-500">
                        <div class="flex flex-row items-center">
                            <div class="flex-shrink pr-4">
                                <div class="rounded p-2 bg-lightBlue-600"><i
                                        class="fas fa-bullhorn fa-2x fa-fw fa-inverse"></i>
                                </div>
                            </div>
                            <div class="flex-1 text-right md:text-center">
                                <h3 class="font-semibold uppercase text-gray-800" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Canal de Denúncia
                                    <br>
                                    <span class="" name="texto" style="font-size: 0.6rem; line-height: 1rem;">
                                        (Órgão gestor CONTROLADORIA RN)
                                    </span>
                                </h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
                <a href="<?php echo e(route('contratos-publicos')); ?>"
                    target="_blank">
                    <div
                        class="bg-white border border-lightBlue-600 rounded-md shadow p-2 hover:bg-lightBlue-300 hover:border-transparent | transition-colors duration-500">
                        <div class="flex flex-row items-center">
                            <div class="flex-shrink pr-4">
                                <div class="rounded p-2 bg-lightBlue-600"><i
                                        class="fas fa-file-contract fa-2x fa-fw fa-inverse"></i>
                                </div>
                            </div>
                            <div class="flex-1 text-right md:text-center">
                                <h3 class="font-semibold uppercase text-gray-800" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Contratos
                                </h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
                <a href="<?php echo e(route('convenios-concedidos')); ?>" target="_blank">
                    <div
                        class="bg-white border border-lightBlue-600 rounded-md shadow p-2 hover:bg-lightBlue-300 hover:border-transparent | transition-colors duration-500">
                        <div class="flex flex-row items-center">
                            <div class="flex-shrink pr-4">
                                <div class="rounded p-2 bg-lightBlue-600"><i
                                        class="fas fa-handshake fa-2x fa-fw fa-inverse"></i>
                                </div>
                            </div>
                            <div class="flex-1 text-right md:text-center">
                                <h3 class="font-semibold uppercase text-gray-800" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Convênios Concedidos
                                </h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
                <a href="<?php echo e(route('portal-de-compras')); ?>" target="_blank">
                    <div
                        class="bg-white border border-lightBlue-600 rounded-md shadow p-2 hover:bg-lightBlue-300 hover:border-transparent | transition-colors duration-500">
                        <div class="flex flex-row items-center">
                            <div class="flex-shrink pr-4">
                                <div class="rounded p-2 bg-lightBlue-600"><i
                                        class="fas fa-luggage-cart fa-2x fa-fw fa-inverse"></i>
                                </div>
                            </div>
                            <div class="flex-1 text-right md:text-center">
                                <h3 class="font-semibold uppercase text-gray-800" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Portal de Compras
                                </h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
                <a href="<?php echo e(route('transferencia-para-municipios')); ?>"
                    target="_blank">
                    <div
                        class="bg-white border border-lightBlue-600 rounded-md shadow p-2 hover:bg-lightBlue-300 hover:border-transparent | transition-colors duration-500">
                        <div class="flex flex-row items-center">
                            <div class="flex-shrink pr-4">
                                <div class="rounded p-2 bg-lightBlue-600"><i
                                        class="fas fa-sync-alt fa-2x fa-fw fa-inverse"></i>
                                </div>
                            </div>
                            <div class="flex-1 text-right md:text-center">
                                <h3 class="font-semibold uppercase text-gray-800" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Transferência para Municípios</h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
                <a href="<?php echo e(route('transparencia-ambiental')); ?>" target="_blank">
                    <div
                        class="bg-white border border-lightBlue-600 rounded-md shadow p-2 hover:bg-lightBlue-300 hover:border-transparent | transition-colors duration-500">
                        <div class="flex flex-row items-center">
                            <div class="flex-shrink pr-4">
                                <div class="rounded p-2 bg-lightBlue-600">
                                    <i class="fab fa-pagelines fa-2x fa-fw fa-inverse"></i>
                                </div>
                            </div>
                            <div class="flex-1 text-right md:text-center">
                                <h3 class="font-semibold uppercase text-gray-800" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    <div class="flex md:justify-center justify-end">
                                        <img src="images/logo/idema/idema-verde.svg" width="100" height="100">
                                    </div>
                                    <span class="text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                        Transparência Ambiental
                                    </span>
                                </h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
                <a href="<?php echo e(route('renuncia-receita')); ?>">
                    <div
                        class="bg-white border border-lightBlue-600 rounded-md shadow p-2 hover:bg-lightBlue-300 hover:border-transparent | transition-colors duration-500">
                        <div class="flex flex-row items-center">
                            <div class="flex-shrink pr-4">
                                <div class="rounded p-2 bg-lightBlue-600"><i
                                        class="fas fa-external-link-alt fa-2x fa-fw fa-inverse"></i>
                                </div>
                            </div>
                            <div class="flex-1 text-right md:text-center">
                                <h3 class="font-semibold uppercase text-gray-800" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Renúncia de Receita</h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
                <a href="<?php echo e(route('obras-e-execucao')); ?>"
                    target="_blank">
                    <div
                        class="bg-white border border-lightBlue-600 rounded-md shadow p-2 hover:bg-lightBlue-300 hover:border-transparent | transition-colors duration-500">
                        <div class="flex flex-row items-center">
                            <div class="flex-shrink pr-4">
                                <div class="rounded p-2 bg-lightBlue-600">
                                    <i class="fas fa-hammer fa-2x fa-fw fa-inverse"></i>
                                </div>
                            </div>
                            <div class="flex-1 text-right md:text-center">
                                <h3 class="font-semibold uppercase text-gray-800" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Obras e Execução</h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
                <a href="<?php echo e(route('patrimonio-imobiliario')); ?>" target="_blank">
                    <div
                        class="bg-white border border-lightBlue-600 rounded-md shadow p-2 hover:bg-lightBlue-300 hover:border-transparent | transition-colors duration-500">
                        <div class="flex flex-row items-center">
                            <div class="flex-shrink pr-4">
                                <div class="rounded p-2 bg-lightBlue-600">
                                    <i class="fas fa-home fa-2x fa-fw fa-inverse"></i>
                                </div>
                            </div>
                            <div class="flex-1 text-right md:text-center">
                                <h3 class="font-semibold uppercase text-gray-800" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Patrimônio Imobiliário</h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
                <a href="<?php echo e(route('emendas-parlamentares')); ?>">
                    <div
                        class="bg-white border border-lightBlue-600 rounded-md shadow p-2 hover:bg-lightBlue-300 hover:border-transparent | transition-colors duration-500">
                        <div class="flex flex-row items-center">
                            <div class="flex-shrink pr-4">
                                <div class="rounded p-2 bg-lightBlue-600"><i
                                        class="fas fa-money-check-dollar fa-2x fa-fw fa-inverse"></i>
                                </div>
                            </div>
                            <div class="flex-1 text-right md:text-center">
                                <h3 class="font-semibold uppercase text-gray-800" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Emendas Parlamentares
                                </h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
                <a
                    href="<?php echo e(route('transferencias-recebidas')); ?>">
                    <div
                        class="bg-white border border-lightBlue-600 rounded-md shadow p-2 hover:bg-lightBlue-300 hover:border-transparent | transition-colors duration-500">
                        <div class="flex flex-row items-center">
                            <div class="flex-shrink pr-4">
                                <div class="rounded p-2 bg-lightBlue-600"><i
                                        class="fas fa-money-bill-transfer fa-2x fa-fw fa-inverse"></i>
                                </div>
                            </div>
                            <div class="flex-1 text-right md:text-center">
                                <h3 class="font-semibold uppercase text-gray-800" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Transferências Recebidas
                                </h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
                <a href="<?php echo e(route('transparencia-publica')); ?>"
                    target="_blank">
                    <div
                        class="bg-white border border-lightBlue-600 rounded-md shadow p-2 hover:bg-lightBlue-300 hover:border-transparent | transition-colors duration-500">
                        <div class="flex flex-row items-center">
                            <div class="flex-shrink pr-4">
                                <div class="rounded p-2 bg-lightBlue-600">
                                    <i class="fas fa-satellite-dish fa-2x fa-fw fa-inverse"></i>
                                </div>
                            </div>
                            <div class="flex-1 text-right md:text-center">
                                <h3 class="font-semibold uppercase text-gray-800" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    <div class="flex md:justify-center justify-end">
                                        <img src="images/logo/radar/radar.png" width="100" height="100">
                                    </div>
                                    <span class="text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                        Transparência Pública
                                    </span>
                                </h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
                <a href="<?php echo e(url('/covid')); ?>" target="_blank">
                    <div
                        class="bg-white border border-lightBlue-600 rounded-md shadow p-2 hover:bg-lightBlue-300 hover:border-transparent | transition-colors duration-500">
                        <div class="flex flex-row items-center">
                            <div class="flex-shrink pr-4">
                                <div class="rounded p-2 bg-lightBlue-600">
                                    <i class="fa-solid fa-virus-covid fa-2x fa-fw fa-inverse"></i>
                                </div>
                            </div>
                            <div class="flex-1 text-right md:text-center">
                                <h3 class="font-semibold uppercase text-gray-800" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">

                                    <span class="text-xs" name="texto"
                                        style="font-size: 0.875rem; line-height: 1.25rem;">
                                        Transparência COVID-19
                                    </span>
                                </h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
                <a href="<?php echo e(url('/defensores-dativos')); ?>" target="_blank">
                    <div
                        class="bg-white border border-lightBlue-600 rounded-md shadow p-2 hover:bg-lightBlue-300 hover:border-transparent | transition-colors duration-500">
                        <div class="flex flex-row items-center">
                            <div class="flex-shrink pr-4">
                                <div class="rounded p-2 bg-lightBlue-600">
                                    <i class="fa-solid fa-gavel fa-2x fa-fw fa-inverse"></i>
                                </div>
                            </div>
                            <div class="flex-1 text-right md:text-center">
                                <h3 class="font-semibold uppercase text-gray-800" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">

                                    <span class="text-xs" name="texto"
                                        style="font-size: 0.875rem; line-height: 1.25rem;">
                                        Despesas com Defensores Dativos
                                    </span>
                                </h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
                <a
                    href="<?php echo e(route('divida-ativa')); ?>" target="_blank">
                    <div
                        class="bg-white border border-lightBlue-600 rounded-md shadow p-2 hover:bg-lightBlue-300 hover:border-transparent | transition-colors duration-500">
                        <div class="flex flex-row items-center">
                            <div class="flex-shrink pr-4">
                                <div class="rounded p-2 bg-lightBlue-600"><i
                                        class="fas fa-receipt fa-2x fa-fw fa-inverse"></i>
                                </div>
                            </div>
                            <div class="flex-1 text-right md:text-center">
                                <h3 class="font-semibold uppercase text-gray-800" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Dívida Ativa
                                </h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
                <a
                    href="<?php echo e(route('educacao')); ?>" target="_blank">
                    <div
                        class="bg-white border border-lightBlue-600 rounded-md shadow p-2 hover:bg-lightBlue-300 hover:border-transparent | transition-colors duration-500">
                        <div class="flex flex-row items-center">
                            <div class="flex-shrink pr-4">
                                <div class="rounded p-2 bg-lightBlue-600"><i
                                        class="fas fa-graduation-cap fa-2x fa-fw fa-inverse"></i>
                                </div>
                            </div>
                            <div class="flex-1 text-right md:text-center">
                                <h3 class="font-semibold uppercase text-gray-800" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Educação
                                </h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
                <a href="<?php echo e(url('/saude')); ?>" target="_blank">
                    <div
                        class="bg-white border border-lightBlue-600 rounded-md shadow p-2 hover:bg-lightBlue-300 hover:border-transparent | transition-colors duration-500">
                        <div class="flex flex-row items-center">
                            <div class="flex-shrink pr-4">
                                <div class="rounded p-2 bg-lightBlue-600">
                                    <i class="fa-solid fa-medkit fa-2x fa-fw fa-inverse"></i>
                                </div>
                            </div>
                            <div class="flex-1 text-right md:text-center">
                                <h3 class="font-semibold uppercase text-gray-800" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">

                                    <span class="text-xs" name="texto"
                                        style="font-size: 0.875rem; line-height: 1.25rem;">
                                        Saúde
                                    </span>
                                </h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/4 p-3">
                <a href="<?php echo e(route('plano-estrategico')); ?>"
                    target="_blank">
                    <div
                        class="bg-white border border-lightBlue-600 rounded-md shadow p-2 hover:bg-lightBlue-300 hover:border-transparent | transition-colors duration-500">
                        <div class="flex flex-row items-center">
                            <div class="flex-shrink pr-4">
                                <div class="rounded p-2 bg-lightBlue-600">
                                    <i class="fas fa-clipboard-list fa-2x fa-fw fa-inverse"></i>
                                </div>
                            </div>
                            <div class="flex-1 text-right md:text-center">
                                <h3 class="font-semibold uppercase text-gray-800" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Plano Estratégico</h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>




        </div>
        <!--/ Console Content-->

        <!--Divider-->
        <hr class="border-b-2 border-coolGray-300 my-8">
        <!-- /Divider -->

        <!-- Section -->
        <section class="text-gray-600 mt-16 body-font">
            <div class="container mx-auto">
                <div class="flex flex-col w-full">
                    <h1 class="text-3xl azulclaro font-semibold tracking-widest font-medium title-font mb-8">
                        PORTAL DA TRANSPARÊNCIA
                    </h1>
                    <p class="lg:w-full mx-auto leading-relaxed text-base text-justify" name="texto"
                        style="font-size: 1rem; line-height: 1.5rem;">
                        O Portal da Transparência do Estado do RN possibilita
                        o acompanhamento das informações orçamentárias e financeiras do executivo estadual, permitindo ao
                        cidadão a fiscalização, de forma eficiente e transparente da utilização dos recursos
                        públicos, para assegurar a sua correta aplicação. O Portal também disponibiliza informações sobre os
                        instrumentos de Planejamento, Relatórios Fiscais, Licitações, obras realizadas no Estado e outras
                        informações de relevância para os
                        cidadãos norteriograndenses.
                        <a class="text-blue-400 hover:text-blue-600" href="<?php echo e(route('sobre-o-portal')); ?>" name="texto"
                            style="font-size: 1rem; line-height: 1.5rem;">Clique aqui</a> para mais informações.
                    </p>
                </div>

                <div class="container md:px-5 md:py-10 mx-auto">

                    <div class="flex items-center lg:w-full mx-auto border-b pb-10 mb-10 border-gray-200 sm:flex-row flex-col"
                        data-anime="left">
                        <div
                            class="sm:w-20 sm:h-20 h-12 w-12 sm:mr-10 inline-flex items-center justify-center rounded-full bg-lightBlue-100 azulclaro flex-shrink-0">
                            <i class="fas fa-lock fa-2x"></i>
                        </div>
                        <div class="flex-grow mt-6 sm:mt-0">
                            <h2 class="text-gray-900 title-font font-medium mb-2 md:text-left sm:text-center xs:text-center"
                                name="texto" style="font-size: 1.125rem; line-height: 1.75rem;">
                                Confiabilidade
                            </h2>
                            <p class="leading-relaxed text-justify" name="texto"
                                style="font-size: 0.875rem; line-height: 1.25rem;">
                                O Portal é atualizado diariamente com os dados do Sistema
                                Integrado de Planejamento e Gestão Fiscal (SIGEF), de onde são extraídas diretamente as
                                informações da execução financeiro-orçamentária do Estado.
                            </p>
                        </div>
                    </div>

                    <div class="flex items-center lg:w-full mx-auto border-b pb-10 mb-10 border-gray-200 sm:flex-row flex-col"
                        data-anime="right">
                        <div class="titulo flex-grow order-first mt-6 sm:mt-0">
                            <h2 class="text-gray-900 title-font font-medium mb-2 md:text-right sm:text-center xs:text-center"
                                name="texto" style="font-size: 1.125rem; line-height: 1.75rem;">
                                Consultas
                            </h2>
                            <p class="leading-relaxed text-justify" name="texto"
                                style="font-size: 0.875rem; line-height: 1.25rem;">
                                Ao acessar as opções Receita e Despesa, o
                                cidadão poderá montar a sua própria consulta, a partir de dados e critérios selecionados,
                                obtendo informações que atendam aos seus interesses. Essas consultas trazem
                                recursos gráficos que garantirão uma análise mais rápida e visual.
                            </p>
                        </div>
                        <div
                            class="logo sm:w-20 order-last sm:h-20 h-12 w-12 sm:ml-10 inline-flex items-center justify-center rounded-full bg-red-100 text-red-500 flex-shrink-0">
                            <i class="fas fa-chart-bar fa-2x"></i>
                        </div>
                    </div>

                    <div class="flex items-center lg:w-full mx-auto border-b pb-10 mb-10 border-gray-200 sm:flex-row flex-col"
                        data-anime="left">
                        <div
                            class="sm:w-20 sm:h-20 h-12 w-12 sm:mr-10 inline-flex items-center justify-center rounded-full bg-yellow-100 text-yellow-500 flex-shrink-0">
                            <i class="fas fa-database fa-2x"></i>
                        </div>
                        <div class="flex-grow mt-6 sm:mt-0">
                            <h2 class="text-gray-900 title-font font-medium mb-2 md:text-left sm:text-center xs:text-center"
                                name="texto" style="font-size: 1.125rem; line-height: 1.75rem;">
                                Dados Abertos
                            </h2>
                            <p class="leading-relaxed text-justify" name="texto"
                                style="font-size: 0.875rem; line-height: 1.25rem;">
                                O Portal da Transparência do RN propicia o acesso a dados no
                                formato aberto, processáveis por máquina e disponibilizados em formato CSV. Dessa forma, o
                                cidadão poderá acessar os dados livremente, fazendo cruzamentos e criando aplicativos
                                que demonstrem novas formas de utilização e visualização desses dados em benefício da
                                sociedade.
                            </p>
                        </div>
                    </div>

                    <div class="flex items-center lg:w-full mx-auto border-b pb-10 mb-10 border-gray-200 sm:flex-row flex-col"
                        data-anime="right">
                        <div class="titulo flex-grow order-first mt-6 sm:mt-0">
                            <h2 class="text-gray-900 title-font font-medium mb-2 md:text-right sm:text-center xs:text-center"
                                name="texto" style="font-size: 1.125rem; line-height: 1.75rem;">
                                Fale Conosco
                            </h2>
                            <p class="leading-relaxed text-justify" name="texto"
                                style="font-size: 0.875rem; line-height: 1.25rem;">
                                Se não encontrou o que procurava, acesse os serviços da Ouvidoria
                                Geral do Estado. Caso deseje ter acesso a dados ou informações públicas adicionais, utilize
                                o
                                Sistema de Informação ao Cidadão – e-SIC RN ou consulte a área de ajuda,
                                que disponibiliza glossário, perguntas frequentes e outras formas de auxílio.
                            </p>
                        </div>
                        <div
                            class="logo sm:w-20 order-last sm:h-20 h-12 w-12 sm:ml-10 inline-flex items-center justify-center rounded-full bg-green-100 text-green-500 flex-shrink-0">
                            <i class="far fa-comments fa-2x"></i>
                        </div>
                    </div>
                    
                    <div class="flex items-center lg:w-full mx-auto border-b pb-10 mb-10 border-gray-200 sm:flex-row flex-col"
                    data-anime="left">
                    <div
                        class="sm:w-20 sm:h-20 h-12 w-12 sm:mr-10 inline-flex items-center justify-center rounded-full bg-orange-100 text-orange-500 flex-shrink-0">
                        <i class="fas fa-circle-exclamation fa-2x"></i>
                    </div>
                    <div class="flex-grow mt-6 sm:mt-0">
                        <h2 class="text-gray-900 title-font font-medium mb-2 md:text-left sm:text-center xs:text-center"
                            name="texto" style="font-size: 1.125rem; line-height: 1.75rem;"><a href = "<?php echo e(route('notas-esclarecimento')); ?>" target="_blank">
                            Notas de Esclarecimento
                            </a>
                        </h2>
                        <p class="leading-relaxed text-justify" name="texto"
                            style="font-size: 0.875rem; line-height: 1.25rem;">
                            Aqui você encontrará notas de esclarecimento sobre fatos relevantes que
                            impactaram na disponibilidade de dados do Portal da Transparência do RN.
                            
                        </p>
                    </div>
                </div>

                </div>
            </div>
        </section>
        
        <div id="temp" class="hidden">
            <div wire:loading
                class="relative w-auto h-auto my-8 z-10 bg-warmgray-100 overflow-hidden flex flex-col items-center justify-center">
                <div class="loader ease-linear rounded-full border-4 border-t-4 border-gray-200 h-12 w-12 mb-4"></div>
                <h2 class="text-center text-xl font-semibold">Carregando...</h2>
                <p class="w-1/2 text-center text-sm">Isso pode levar alguns segundos.</p>
            </div>
        </div>
    </div>

    <!-- alterar a ordem na lista dos logos/texto quando a tela fica pequena -->
    <script>
        function updateLogoOrder() {
            $('.logo').each(function() {
                var logo = $(this);
                var titulo = logo.siblings('.titulo');
                if ($(window).width() < 640) {
                    logo.removeClass('order-last').addClass('order-first');
                    titulo.removeClass('order-first').addClass('order-last');
                } else {
                    logo.removeClass('order-first').addClass('order-last');
                    titulo.removeClass('order-last').addClass('order-first');
                }
            });
        }

        $(document).ready(updateLogoOrder);
        $(window).on('resize', updateLogoOrder);
    </script>

    <!-- Animate scroll -->
    <script>
        const debounce = function(func, wait, immediate) {
            let timeout;
            return function(...args) {
                const context = this;
                const later = function() {
                    timeout = null;
                    if (!immediate) func.apply(context, args);
                };

                const callNow = immediate && !timeout;
                clearTimeout(timeout);
                timeout = setTimeout(later, wait);
                if (callNow) func.apply(context, args);
            };
        };

        const target = document.querySelectorAll('[data-anime]');
        const animationClass = 'animate';

        function animeScroll() {
            const windowTop = window.pageYOffset + (window.innerHeight * 0.98);
            target.forEach(function(element) {
                if ((windowTop) > element.offsetTop) {
                    element.classList.add(animationClass);
                }
            })
        }

        animeScroll();

        if (target.length) {
            window.addEventListener('scroll', debounce(function() {
                animeScroll();
            }, 2));
        }
    </script>

    <!-- Animate count numbers -->
    <script>
        const options = {
            separator: '.',
            decimal: ',',
        };
        var educacao = new CountUp("educacao", 0, <?php echo e($educacaogastos->sum('vlrpagamento')); ?>, 2, 1, options);
        var saude = new CountUp("saude", 0, <?php echo e($saudegastos->sum('vlrpagamento')); ?>, 2, 1, options);
        var seguranca = new CountUp("seguranca", 0, <?php echo e($segurancagastos->sum('vlrpagamento')); ?>, 2, 1, options);
        var transporte = new CountUp("transporte", 0, <?php echo e($transportegastos->sum('vlrpagamento')); ?>, 2, 1, options);

        educacao.start();
        saude.start();
        seguranca.start();
        transporte.start();
    </script>

    <style>
        .azulclaro {
            color: #0185C2;
        }

        .bg-azulclaro {
            background-color: #0185C2;
        }
    </style>
    <!-- /Section -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/portaltransparencianewrn/resources/views/welcome.blade.php ENDPATH**/ ?>