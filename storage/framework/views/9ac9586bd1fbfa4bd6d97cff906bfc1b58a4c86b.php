<?php $__env->startSection('title'); ?>
- Todos tipos gastos
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<link rel="stylesheet" href="<?php echo e(asset('css/todostiposgastos.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script src="<?php echo e(asset('js/countUp.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<h1 class="text-lg text-gray-900 tracking-widest font-medium title-font text-center pb-2" name="texto"
   style="font-size: 1.125rem; line-height: 2rem;">
   <i class="fas fa-sort-alpha-up"></i>
   TIPOS-GASTOS
</h1>

<p class="lg:w-3/4 mx-auto leading-relaxed text-sm text-center text-gray-600" name="texto"
   style="font-size: 0.875rem; line-height: 1.25rem;">
   Selecione um ano para visualizar gastos do ano referente.
</p>

<ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto"
   style="font-size: 0.875rem; line-height: 1.25rem;">
   <li class="inline-flex items-center">
      <a href="<?php echo e(url('/')); ?>">Início</a>
      <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
         <path fill-rule="evenodd"
            d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
            clip-rule="evenodd"></path>
      </svg>
   </li>
   <li class="inline-flex items-center">
      <a href="#" class="text-teal-400">
         Tipos-Gastos
      </a>
   </li>
</ul>

<hr class="border-b-2 border-lightBlue-300 mb-6 mx-4">

<form action="<?php echo e(route('todos-tipos-gastos')); ?>" method="POST">
   <?php echo csrf_field(); ?>
   <div class="flex flex-wrap justify-center -mx-3 mb-10">
      <div class="md:w-1/4 px-3 mb-6 md:mb-0">
         <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-state" name="texto"
            style="font-size: 1rem; line-height: 1.5rem;">
            Selecione um ano
         </label>
         <div id="select_ano" class="relative">
            <select
               class="block appearance-none w-full bg-white border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:shadow-inner focus:border-gray-500"
               id="ano" name="ano">
               <?php $__currentLoopData = $anos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ano): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
               <?php if($ano == $ano_selected): ?>
               <option value="<?php echo e($ano_selected); ?>" selected><?php echo e($ano); ?></option>
               <?php else: ?>
               <option value="<?php echo e($ano); ?>"><?php echo e($ano); ?></option>
               <?php endif; ?>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </select>
            <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
               <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                  <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
               </svg>
            </div>
         </div>
      </div>
      <div class="md:w-1/4 px-3 mb-6 md:mb-0">
         <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-state" name="texto"
            style="font-size: 1rem; line-height: 1.5rem;">
            Despesas
         </label>
         <input
            class="cursor-pointer appearance-none block w-full rounded py-3 px-4 leading-tight bg-lightBlue-500 hover:bg-lightBlue-700 text-white"
            type="submit" value="Consultar">
      </div>
   </div>
</form>

<div class="flex items-center justify-center">
   <div class="grid grid-cols-1 gap-6 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-4 xl:grid-cols-4">

      <!-- 1 card -->
      <div class="relative bg-white border border-indigo-700 py-6 px-6 rounded-xl w-64 my-4 shadow-xl">
         <div class="text-indigo-700 flex justify-center zoomttg">
            <i class="fas fa-chart-pie fa-2x text-center"></i>
         </div>

         <div class="mt-8">
            <p class="text-base text-coolGray-700 text-center font-semibold my-2" name="texto"
               style="font-size: 1rem; line-height: 1.5rem;">ADMINISTRAÇÃO</p>

            <div class=" text-gray-400 text-sm my-3 text-center" name="texto"
               style="font-size: 0.875rem; line-height: 1.25rem;">
               <i class="far fa-clock"> <span class=""><?php echo e($ano_selected); ?></span> </i>
            </div>

            <div class="border-t-2"></div>

            <div class="my-8">
               <p class="text-xl text-center" name="texto" style="font-size: 1.25rem; line-height: 1.75rem;">R$ <span
                     id="administracao"></span></p>
            </div>

            <div class="flex space-x-4">
               <a name="card" href="<?php echo e(route('gastos-diretos-tipo-funcao', [
                        'funcao' => 'Administração',
                        'ano' => $ano_selected
                    ])); ?>"
                  class="w-full px-4 py-1.5 text-center text-white bg-lightBlue-500 rounded-lg hover:bg-lightBlue-700"
                  name="texto" style="font-size: 1rem; line-height: 1.5rem;">Detalhar</a>
            </div>
         </div>
      </div>

      <!-- 2 card -->
      <div class="relative bg-white border border-red-500 py-6 px-6 rounded-xl w-64 my-4 shadow-xl">

         <div class="text-red-500 flex justify-center zoomttg">
            <i class="fab fa-pagelines fa-2x text-center"></i>
         </div>

         <div class="mt-8">
            <p class="text-base text-coolGray-700 text-center font-semibold my-2" name="texto"
               style="font-size: 1rem; line-height: 1.5rem;">AGRICULTURA</p>

            <div class=" text-gray-400 text-sm my-3 text-center" name="texto"
               style="font-size: 0.875rem; line-height: 1.25rem;">
               <i class="far fa-clock"> <span class=""><?php echo e($ano_selected); ?></span> </i>
            </div>

            <div class="border-t-2"></div>

            <div class="my-8">
               <p class="text-xl text-center" name="texto" style="font-size: 1.25rem; line-height: 1.75rem;">R$ <span
                     id="agricultura"></span></p>
            </div>

            <div class="flex space-x-4">
               <a name="card" href="<?php echo e(route('gastos-diretos-tipo-funcao', [
                        'funcao' => 'Agricultura',
                        'ano' => $ano_selected
                    ])); ?>"
                  class="w-full px-4 py-1.5 text-center text-white bg-lightBlue-500 rounded-lg hover:bg-lightBlue-700"
                  name="texto" style="font-size: 1rem; line-height: 1.5rem;">Detalhar</a>
            </div>
         </div>
      </div>

      <!-- 3 card -->
      <div class="relative bg-white border border-teal-500 py-6 px-6 rounded-xl w-64 my-4 shadow-xl">
         <div class="text-teal-500 flex justify-center zoomttg">
            <i class="fas fa-user fa-2x text-center"></i>
         </div>

         <div class="mt-8">
            <p class="text-base text-coolGray-700 text-center font-semibold my-2" name="texto"
               style="font-size: 1rem; line-height: 1.5rem;">ASSISTÊNCIA SOCIAL</p>

            <div class="text-gray-400 text-sm my-3 text-center" name="texto"
               style="font-size: 0.875rem; line-height: 1.25rem;">
               <i class="far fa-clock"> <span class=""><?php echo e($ano_selected); ?></span> </i>
            </div>

            <div class="border-t-2 "></div>

            <div class="my-8">
               <p class="text-xl text-center" name="texto" style="font-size: 1.25rem; line-height: 1.75rem;">R$ <span
                     id="assistencia"></span></p>
            </div>

            <div class="flex space-x-4">
               <a name="card" href="<?php echo e(route('gastos-diretos-tipo-funcao', [
                        'funcao' => 'Assistência Social',
                        'ano' => $ano_selected
                    ])); ?>"
                  class="w-full px-4 py-1.5 text-center text-white bg-lightBlue-500 rounded-lg hover:bg-lightBlue-700"
                  name="texto" style="font-size: 1rem; line-height: 1.5rem;">Detalhar</a>
            </div>
         </div>
      </div>

      <!-- 4 card -->
      <div class="relative bg-white border border-yellow-900 py-6 px-6 rounded-xl w-64 my-4 shadow-xl">
         <div class="text-yellow-900 flex justify-center zoomttg">
            <i class="fas fa-bong fa-2x text-center"></i>
         </div>

         <div class="mt-8">
            <p class="text-base text-coolGray-700 text-center font-semibold my-2" name="texto"
               style="font-size: 1rem; line-height: 1.5rem;">CIÊNCIA E TECNOLOGIA</p>

            <div class="text-gray-400 text-sm my-3 text-center" name="texto"
               style="font-size: 0.875rem; line-height: 1.25rem;">
               <i class="far fa-clock"> <span class=""><?php echo e($ano_selected); ?></span> </i>
            </div>

            <div class="border-t-2 "></div>

            <div class="my-8">
               <p class="text-xl text-center" name="texto" style="font-size: 1.25rem; line-height: 1.75rem;">R$ <span
                     id="cet"></span></p>
            </div>

            <div class="flex space-x-4">
               <a name="card" href="<?php echo e(route('gastos-diretos-tipo-funcao', [
                        'funcao' => 'Ciência e Tecnologia',
                        'ano' => $ano_selected
                    ])); ?>"
                  class="w-full px-4 py-1.5 text-center text-white bg-lightBlue-500 rounded-lg hover:bg-lightBlue-700"
                  name="texto" style="font-size: 1rem; line-height: 1.5rem;">Detalhar</a>
            </div>
         </div>
      </div>

   </div>
</div>



<div class="flex items-center justify-center pt-4">
   <div class="grid grid-cols-1 gap-6 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-4 xl:grid-cols-4">

      <!-- 5 card -->
      <div class="relative bg-white border border-green-700 py-6 px-6 rounded-xl w-64 my-4 shadow-xl">
         <div class="text-green-700 flex justify-center zoomttg">
            <i class="fas fa-shopping-cart fa-2x text-center"></i>
         </div>

         <div class="mt-8">
            <p class="text-base text-coolGray-700 text-center font-semibold my-2" name="texto"
               style="font-size: 1rem; line-height: 1.5rem;">COMÉRCIO E SERVIÇOS</p>

            <div class=" text-gray-400 text-sm my-3 text-center" name="texto"
               style="font-size: 0.875rem; line-height: 1.25rem;">
               <i class="far fa-clock"> <span class=""><?php echo e($ano_selected); ?></span> </i>
            </div>

            <div class="border-t-2"></div>

            <div class="my-8">
               <p class="text-xl text-center" name="texto" style="font-size: 1.25rem; line-height: 1.75rem;">R$ <span
                     id="comercio"></span></p>
            </div>

            <div class="flex space-x-4">
               <a name="card" href="<?php echo e(route('gastos-diretos-tipo-funcao', [
                        'funcao' => 'Comércio e Serviços',
                        'ano' => $ano_selected
                    ])); ?>"
                  class="w-full px-4 py-1.5 text-center text-white bg-lightBlue-500 rounded-lg hover:bg-lightBlue-700"
                  name="texto" style="font-size: 1rem; line-height: 1.5rem;">Detalhar</a>
            </div>
         </div>
      </div>

      <!-- 6 card -->
      <div class="relative bg-white border border-lightBlue-700 py-6 px-6 rounded-xl w-64 my-4 shadow-xl">
         <div class=" text-lightBlue-700 flex justify-center zoomttg">
            <i class="fas fa-paint-brush fa-2x text-center"></i>
         </div>

         <div class="mt-8">
            <p class="text-base text-coolGray-700 text-center font-semibold my-2" name="texto"
               style="font-size: 1rem; line-height: 1.5rem;">CULTURA</p>

            <div class=" text-gray-400 text-sm my-3 text-center" name="texto"
               style="font-size: 0.875rem; line-height: 1.25rem;">
               <i class="far fa-clock"> <span class=""><?php echo e($ano_selected); ?></span> </i>
            </div>

            <div class="border-t-2"></div>

            <div class="my-8">
               <p class="text-xl text-center" name="texto" style="font-size: 1.25rem; line-height: 1.75rem;">R$ <span
                     id="cultura"></span></p>
            </div>

            <div class="flex space-x-4">
               <a name="card" href="<?php echo e(route('gastos-diretos-tipo-funcao', [
                        'funcao' => 'Cultura',
                        'ano' => $ano_selected
                    ])); ?>"
                  class="w-full px-4 py-1.5 text-center text-white bg-lightBlue-500 rounded-lg hover:bg-lightBlue-700"
                  name="texto" style="font-size: 1rem; line-height: 1.5rem;">Detalhar</a>
            </div>
         </div>
      </div>

      <!-- 7 card -->
      <div class="relative bg-white border border-red-900 py-6 px-6 rounded-xl w-64 my-4 shadow-xl">
         <div class="text-red-900 flex justify-center zoomttg">
            <i class="fas fa-fist-raised fa-2x text-center"></i>
         </div>

         <div class="mt-8">
            <p class="text-base text-coolGray-700 text-center font-semibold my-2" name="texto"
               style="font-size: 1rem; line-height: 1.5rem;">DIREITOS DA CIDADANIA</p>

            <div class="text-gray-400 text-sm my-3 text-center" name="texto"
               style="font-size: 0.875rem; line-height: 1.25rem;">
               <i class="far fa-clock"> <span class=""><?php echo e($ano_selected); ?></span> </i>
            </div>

            <div class="border-t-2 "></div>

            <div class="my-8">
               <p class="text-xl text-center" name="texto" style="font-size: 1.25rem; line-height: 1.75rem;">R$ <span
                     id="direitos"></span></p>
            </div>

            <div class="flex space-x-4">
               <a name="card" href="<?php echo e(route('gastos-diretos-tipo-funcao', [
                        'funcao' => 'Direitos da Cidadania',
                        'ano' => $ano_selected
                    ])); ?>"
                  class="w-full px-4 py-1.5 text-center text-white bg-lightBlue-500 rounded-lg hover:bg-lightBlue-700"
                  name="texto" style="font-size: 1rem; line-height: 1.5rem;">Detalhar</a>
            </div>
         </div>
      </div>

      <!-- 8 card -->
      <div class="relative bg-white border border-yellow-600 py-6 px-6 rounded-xl w-64 my-4 shadow-xl">
         <div class="text-yellow-600 flex justify-center zoomttg">
            <i class="fas fa-user-tie fa-2x text-center"></i>
         </div>

         <div class="mt-8">
            <p class="text-base text-coolGray-700 text-center font-semibold my-2" name="texto"
               style="font-size: 1rem; line-height: 1.5rem;">ENCARGOS ESPECIAIS</p>

            <div class="text-gray-400 text-sm my-3 text-center" name="texto"
               style="font-size: 0.875rem; line-height: 1.25rem;">
               <i class="far fa-clock"> <span class=""><?php echo e($ano_selected); ?></span> </i>
            </div>

            <div class="border-t-2 "></div>

            <div class="my-8">
               <p class="text-xl text-center" name="texto" style="font-size: 1.25rem; line-height: 1.75rem;">R$ <span
                     id="encargos"></span></p>
            </div>

            <div class="flex space-x-4">
               <a name="card" href="<?php echo e(route('gastos-diretos-tipo-funcao', [
                        'funcao' => 'Encargos Especiais',
                        'ano' => $ano_selected
                    ])); ?>"
                  class="w-full px-4 py-1.5 text-center text-white bg-lightBlue-500 rounded-lg hover:bg-lightBlue-700"
                  name="texto" style="font-size: 1rem; line-height: 1.5rem;">Detalhar</a>
            </div>
         </div>
      </div>

   </div>
</div>



<div class="flex items-center justify-center pt-4">
   <div class="grid grid-cols-1 gap-6 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-4 xl:grid-cols-4">

      <!-- 9 card -->
      <div class="relative bg-white border border-pink-500 py-6 px-6 rounded-xl w-64 my-4 shadow-xl">
         <div class="text-pink-500 flex justify-center zoomttg">
            <i class="fas fa-lightbulb fa-2x text-center"></i>
         </div>

         <div class="mt-8">
            <p class="text-base text-coolGray-700 text-center font-semibold my-2" name="texto"
               style="font-size: 1rem; line-height: 1.5rem;">ENERGIA</p>

            <div class=" text-gray-400 text-sm my-3 text-center" name="texto"
               style="font-size: 0.875rem; line-height: 1.25rem;">
               <i class="far fa-clock"> <span class=""><?php echo e($ano_selected); ?></span> </i>
            </div>

            <div class="border-t-2"></div>

            <div class="my-8">
               <p class="text-xl text-center" name="texto" style="font-size: 1.25rem; line-height: 1.75rem;">R$ <span
                     id="energia"></span></p>
            </div>

            <div class="flex space-x-4">
               <a name="card" href="<?php echo e(route('gastos-diretos-tipo-funcao', [
                        'funcao' => 'Energia',
                        'ano' => $ano_selected
                    ])); ?>"
                  class="w-full px-4 py-1.5 text-center text-white bg-lightBlue-500 rounded-lg hover:bg-lightBlue-700"
                  name="texto" style="font-size: 1rem; line-height: 1.5rem;">Detalhar</a>
            </div>
         </div>
      </div>

      <!-- 10 card -->
      <div class="relative bg-white border border-blue-500 py-6 px-6 rounded-xl w-64 my-4 shadow-xl">
         <div class="text-blue-500 flex justify-center zoomttg">
            <i class="fa fa-book fa-2x text-center"></i>
         </div>

         <div class="mt-8">
            <p class="text-base text-coolGray-700 text-center font-semibold my-2" name="texto"
               style="font-size: 1rem; line-height: 1.5rem;">EDUCAÇÃO</p>

            <div class=" text-gray-400 text-sm my-3 text-center" name="texto"
               style="font-size: 0.875rem; line-height: 1.25rem;">
               <i class="far fa-clock"> <span class=""><?php echo e($ano_selected); ?></span> </i>
            </div>

            <div class="border-t-2"></div>

            <div class="my-8">
               <p class="text-xl text-center" name="texto" style="font-size: 1.25rem; line-height: 1.75rem;">R$ <span
                     id="educacao"></span></p>
            </div>

            <div class="flex space-x-4">
               <a name="card" href="<?php echo e(route('gastos-diretos-tipo-funcao', [
                        'funcao' => 'Educação',
                        'ano' => $ano_selected
                    ])); ?>"
                  class="w-full px-4 py-1.5 text-center text-white bg-lightBlue-500 rounded-lg hover:bg-lightBlue-700"
                  name="texto" style="font-size: 1rem; line-height: 1.5rem;">Detalhar</a>
            </div>
         </div>
      </div>

      <!-- 11 card -->
      <div class="relative bg-white border border-emerald-400 py-6 px-6 rounded-xl w-64 my-4 shadow-xl">
         <div class="text-emerald-400 flex justify-center zoomttg">
            <i class="fas fa-leaf fa-2x text-center"></i>
         </div>

         <div class="mt-8">
            <p class="text-base text-coolGray-700 text-center font-semibold my-2" name="texto"
               style="font-size: 1rem; line-height: 1.5rem;">GESTÃO AMBIENTAL</p>

            <div class="text-gray-400 text-sm my-3 text-center" name="texto"
               style="font-size: 0.875rem; line-height: 1.25rem;">
               <i class="far fa-clock"> <span class=""><?php echo e($ano_selected); ?></span> </i>
            </div>

            <div class="border-t-2 "></div>

            <div class="my-8">
               <p class="text-xl text-center" name="texto" style="font-size: 1.25rem; line-height: 1.75rem;">R$ <span
                     id="gestao"></span></p>
            </div>

            <div class="flex space-x-4">
               <a name="card" href="<?php echo e(route('gastos-diretos-tipo-funcao', [
                        'funcao' => 'Gestão Ambiental',
                        'ano' => $ano_selected
                    ])); ?>"
                  class="w-full px-4 py-1.5 text-center text-white bg-lightBlue-500 rounded-lg hover:bg-lightBlue-700"
                  name="texto" style="font-size: 1rem; line-height: 1.5rem;">Detalhar</a>
            </div>
         </div>
      </div>

      <!-- 12 card -->
      <div class="relative bg-white border border-orange-500 py-6 px-6 rounded-xl w-64 my-4 shadow-xl">
         <div class="text-orange-500 flex justify-center zoomttg">
            <i class="fas fa-home fa-2x text-center"></i>
         </div>

         <div class="mt-8">
            <p class="text-base text-coolGray-700 text-center font-semibold my-2" name="texto"
               style="font-size: 1rem; line-height: 1.5rem;">HABITAÇÃO</p>

            <div class="text-gray-400 text-sm my-3 text-center" name="texto"
               style="font-size: 0.875rem; line-height: 1.25rem;">
               <i class="far fa-clock"> <span class=""><?php echo e($ano_selected); ?></span> </i>
            </div>

            <div class="border-t-2 "></div>

            <div class="my-8">
               <p class="text-xl text-center" name="texto" style="font-size: 1.25rem; line-height: 1.75rem;">R$ <span
                     id="habitacao"></span></p>
            </div>

            <div class="flex space-x-4">
               <a name="card" href="<?php echo e(route('gastos-diretos-tipo-funcao', [
                        'funcao' => 'Habitação',
                        'ano' => $ano_selected
                    ])); ?>"
                  class="w-full px-4 py-1.5 text-center text-white bg-lightBlue-500 rounded-lg hover:bg-lightBlue-700"
                  name="texto" style="font-size: 1rem; line-height: 1.5rem;">Detalhar</a>
            </div>
         </div>
      </div>

   </div>
</div>



<div class="flex items-center justify-center pt-4">
   <div class="grid grid-cols-1 gap-6 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-4 xl:grid-cols-4">

      <!-- 13 card -->
      <div class="relative bg-white border border-warmGray-500 py-6 px-6 rounded-xl w-64 my-4 shadow-xl">
         <div class="text-warmGray-500 flex justify-center zoomttg">
            <i class="fas fa-industry fa-2x text-center"></i>
         </div>

         <div class="mt-8">
            <p class="text-base text-coolGray-700 text-center font-semibold my-2" name="texto"
               style="font-size: 1rem; line-height: 1.5rem;">INDUSTRIA</p>

            <div class=" text-gray-400 text-sm my-3 text-center" name="texto"
               style="font-size: 0.875rem; line-height: 1.25rem;">
               <i class="far fa-clock"> <span class=""><?php echo e($ano_selected); ?></span> </i>
            </div>

            <div class="border-t-2"></div>

            <div class="my-8">
               <p class="text-xl text-center" name="texto" style="font-size: 1.25rem; line-height: 1.75rem;">R$ <span
                     id="industria"></span></p>
            </div>

            <div class="flex space-x-4">
               <a name="card" href="<?php echo e(route('gastos-diretos-tipo-funcao', [
                        'funcao' => 'Indústria',
                        'ano' => $ano_selected
                    ])); ?>"
                  class="w-full px-4 py-1.5 text-center text-white bg-lightBlue-500 rounded-lg hover:bg-lightBlue-700"
                  name="texto" style="font-size: 1rem; line-height: 1.5rem;">Detalhar</a>
            </div>
         </div>
      </div>

      <!-- 14 card -->
      <div class="relative bg-white border border-indigo-900 py-6 px-6 rounded-xl w-64 my-4 shadow-xl">
         <div class="text-indigo-900 flex justify-center zoomttg">
            <i class="fas fa-balance-scale fa-2x text-center"></i>
         </div>

         <div class="mt-8">
            <p class="text-base text-coolGray-700 text-center font-semibold my-2" name="texto"
               style="font-size: 1rem; line-height: 1.5rem;">JUDICIÁRIA</p>

            <div class=" text-gray-400 text-sm my-3 text-center" name="texto"
               style="font-size: 0.875rem; line-height: 1.25rem;">
               <i class="far fa-clock"> <span class=""><?php echo e($ano_selected); ?></span> </i>
            </div>

            <div class="border-t-2"></div>

            <div class="my-8">
               <p class="text-xl text-center" name="texto" style="font-size: 1.25rem; line-height: 1.75rem;">R$ <span
                     id="judiciaria"></span></p>
            </div>

            <div class="flex space-x-4">
               <a name="card" href="<?php echo e(route('gastos-diretos-tipo-funcao', [
                        'funcao' => 'Judiciária',
                        'ano' => $ano_selected
                    ])); ?>"
                  class="w-full px-4 py-1.5 text-center text-white bg-lightBlue-500 rounded-lg hover:bg-lightBlue-700"
                  name="texto" style="font-size: 1rem; line-height: 1.5rem;">Detalhar</a>
            </div>
         </div>
      </div>

      <!-- 15 card -->
      <div class="relative bg-white border border-lime-800 py-6 px-6 rounded-xl w-64 my-4 shadow-xl">
         <div class="text-lime-800 flex justify-center zoomttg">
            <i class="fas fa-seedling fa-2x text-center"></i>
         </div>

         <div class="mt-8">
            <p class="text-base text-coolGray-700 text-center font-semibold my-2" name="texto"
               style="font-size: 1rem; line-height: 1.5rem;">ORGANIZAÇÃO AGRÁRIA</p>

            <div class="text-gray-400 text-sm my-3 text-center" name="texto"
               style="font-size: 0.875rem; line-height: 1.25rem;">
               <i class="far fa-clock"> <span class=""><?php echo e($ano_selected); ?></span> </i>
            </div>

            <div class="border-t-2 "></div>

            <div class="my-8">
               <p class="text-xl text-center" name="texto" style="font-size: 1.25rem; line-height: 1.75rem;">R$ <span
                     id="organizacaoagraria"></span></p>
            </div>

            <div class="flex space-x-4">
               <a name="card" name="card" ref="<?php echo e(route('gastos-diretos-tipo-funcao', [
                        'funcao' => 'Organização Agrária',
                        'ano' => $ano_selected
                    ])); ?>"
                  class="w-full px-4 py-1.5 text-center text-white bg-lightBlue-500 rounded-lg hover:bg-lightBlue-700"
                  name="texto" style="font-size: 1rem; line-height: 1.5rem;">Detalhar</a>
            </div>
         </div>
      </div>

      <!-- 16 card -->
      <div class="relative bg-white border border-red-500 py-6 px-6 rounded-xl w-64 my-4 shadow-xl">
         <div class="text-red-500 flex justify-center zoomttg">
            <i class="fas fa-heartbeat fa-2x text-center"></i>
         </div>

         <div class="mt-8">
            <p class="text-base text-coolGray-700 text-center font-semibold my-2" name="texto"
               style="font-size: 1rem; line-height: 1.5rem;">SAÚDE</p>

            <div class="text-gray-400 text-sm my-3 text-center" name="texto"
               style="font-size: 0.875rem; line-height: 1.25rem;">
               <i class="far fa-clock"> <span class=""><?php echo e($ano_selected); ?></span> </i>
            </div>

            <div class="border-t-2 "></div>

            <div class="my-8">
               <p class="text-xl text-center" name="texto" style="font-size: 1.25rem; line-height: 1.75rem;">R$ <span
                     id="saude"></span></p>
            </div>

            <div class="flex space-x-4">
               <a name="card" href="<?php echo e(route('gastos-diretos-tipo-funcao', [
                        'funcao' => 'Saúde',
                        'ano' => $ano_selected
                    ])); ?>"
                  class="w-full px-4 py-1.5 text-center text-white bg-lightBlue-500 rounded-lg hover:bg-lightBlue-700"
                  name="texto" style="font-size: 1rem; line-height: 1.5rem;">Detalhar</a>
            </div>
         </div>
      </div>

   </div>
</div>



<div class="flex items-center justify-center pt-4">
   <div class="grid grid-cols-1 gap-6 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-4 xl:grid-cols-4">
      <!-- 18 card -->
      <div class="relative bg-white border border-blueGray-500 py-6 px-6 rounded-xl w-64 my-4 shadow-xl">
         <div class="text-blueGray-500 flex justify-center zoomttg">
            <i class="fa fa-lock fa-2x text-center"></i>
         </div>

         <div class="mt-8">
            <p class="text-base text-coolGray-700 text-center font-semibold my-2" name="texto"
               style="font-size: 1rem; line-height: 1.5rem;">SEGURANÇA PÚBLICA</p>

            <div class=" text-gray-400 text-sm my-3 text-center" name="texto"
               style="font-size: 0.875rem; line-height: 1.25rem;">
               <i class="far fa-clock"> <span class=""><?php echo e($ano_selected); ?></span> </i>
            </div>

            <div class="border-t-2"></div>

            <div class="my-8">
               <p class="text-xl text-center" name="texto" style="font-size: 1.25rem; line-height: 1.75rem;">R$ <span
                     id="seguranca"></span></p>
            </div>

            <div class="flex space-x-4">
               <a name="card" href="<?php echo e(route('gastos-diretos-tipo-funcao', [
                        'funcao' => 'Segurança Pública',
                        'ano' => $ano_selected
                    ])); ?>"
                  class="w-full px-4 py-1.5 text-center text-white bg-lightBlue-500 rounded-lg hover:bg-lightBlue-700"
                  name="texto" style="font-size: 1rem; line-height: 1.5rem;">Detalhar</a>
            </div>
         </div>
      </div>

      <!-- 19 card -->
      <div class="relative bg-white border border-amber-500 py-6 px-6 rounded-xl w-64 my-4 shadow-xl">
         <div class="text-amber-500 flex justify-center zoomttg">
            <i class="fas fa-bus fa-2x text-center"></i>
         </div>

         <div class="mt-8">
            <p class="text-base text-coolGray-700 text-center font-semibold my-2" name="texto"
               style="font-size: 1rem; line-height: 1.5rem;">TRANSPORTE</p>

            <div class="text-gray-400 text-sm my-3 text-center" name="texto"
               style="font-size: 0.875rem; line-height: 1.25rem;">
               <i class="far fa-clock"> <span class=""><?php echo e($ano_selected); ?></span> </i>
            </div>

            <div class="border-t-2 "></div>

            <div class="my-8">
               <p class="text-xl text-center" name="texto" style="font-size: 1.25rem; line-height: 1.75rem;">R$ <span
                     id="transporte"></span></p>
            </div>

            <div class="flex space-x-4">
               <a name="card" href="<?php echo e(route('gastos-diretos-tipo-funcao', [
                        'funcao' => 'Transporte',
                        'ano' => $ano_selected
                    ])); ?>"
                  class="w-full px-4 py-1.5 text-center text-white bg-lightBlue-500 rounded-lg hover:bg-lightBlue-700"
                  name="texto" style="font-size: 1rem; line-height: 1.5rem;">Detalhar</a>
            </div>
         </div>
      </div>

   </div>
</div>

<!-- Animate count numbers -->
<script>
   const options = {
       separator: '.',
       decimal: ',',
    };

    var agricultura = new CountUp("agricultura", 0, <?php echo e($agriculturagastos->sum('vlrpagamento')); ?>, 2, 1, options);
    var administracao = new CountUp("administracao", 0, <?php echo e($administracaogastos->sum('vlrpagamento')); ?>, 2, 1, options);
    var assistencia = new CountUp("assistencia", 0, <?php echo e($assistenciagastos->sum('vlrpagamento')); ?>, 2, 1, options);
    var cet = new CountUp("cet", 0, <?php echo e($cetgastos->sum('vlrpagamento')); ?>, 2, 1, options);
    var comercio = new CountUp("comercio", 0, <?php echo e($comerciogastos->sum('vlrpagamento')); ?>, 2, 1, options);
    var cultura = new CountUp("cultura", 0, <?php echo e($culturagastos->sum('vlrpagamento')); ?>, 2, 1, options);
    var direitos = new CountUp("direitos", 0, <?php echo e($direitosgastos->sum('vlrpagamento')); ?>, 2, 1, options);
    var encargos = new CountUp("encargos", 0, <?php echo e($encargosgastos->sum('vlrpagamento')); ?>, 2, 1, options);
    var energia = new CountUp("energia", 0, <?php echo e($energiagastos->sum('vlrpagamento')); ?>, 2, 1, options);
    var educacao = new CountUp("educacao", 0, <?php echo e($educacaogastos->sum('vlrpagamento')); ?>, 2, 1, options);
    var gestao = new CountUp("gestao", 0, <?php echo e($gestaogastos->sum('vlrpagamento')); ?>, 2, 1, options);
    var habitacao = new CountUp("habitacao", 0, <?php echo e($habitacaogastos->sum('vlrpagamento')); ?>, 2, 1, options);
    var industria = new CountUp("industria", 0, <?php echo e($industriagastos->sum('vlrpagamento')); ?>, 2, 1, options);
    var judiciaria = new CountUp("judiciaria", 0, <?php echo e($judiciariagastos->sum('vlrpagamento')); ?>, 2, 1, options);
    var saude = new CountUp("saude", 0, <?php echo e($saudegastos->sum('vlrpagamento')); ?>, 2, 1, options);
    var seguranca = new CountUp("seguranca", 0, <?php echo e($segurancagastos->sum('vlrpagamento')); ?>, 2, 1, options);
    var organizacaoagraria = new CountUp("organizacaoagraria", 0, <?php echo e($organizacaoagrariagastos->sum('vlrpagamento')); ?>, 2, 1, options);
    var transporte = new CountUp("transporte", 0, <?php echo e($transportegastos->sum('vlrpagamento')); ?>, 2, 1, options);

    agricultura.start();
    administracao.start();
    assistencia.start();
    cet.start();
    comercio.start();
    cultura.start();
    direitos.start();
    encargos.start();
    energia.start();
    educacao.start();
    gestao.start();
    habitacao.start();
    industria.start();
    judiciaria.start();
    saude.start();
    seguranca.start();
    organizacaoagraria.start();
    transporte.start();
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/portaltransparencianewrn/resources/views/todos-tipos-gastos/index.blade.php ENDPATH**/ ?>