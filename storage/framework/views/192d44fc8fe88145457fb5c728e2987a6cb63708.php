<?php $__env->startSection('css'); ?>
<link rel="stylesheet" href="<?php echo e(asset('css/exportfiles.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('css/jquery.treegrid.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<h1 class="text-lg text-gray-900 tracking-widest font-medium title-font uppercase text-center pb-2" name="texto"
   style="font-size: 1.125rem; line-height: 2rem;">
   <i class="fas fa-search-dollar"></i>
   Despesa
</h1>
<ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto"
   style="font-size: 0.875rem; line-height: 1.25rem;">
   <li class="inline-flex items-center">
      <a href="<?php echo e(url('/')); ?>">Inicio</a>
      <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
         <path fill-rule="evenodd"
            d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
            clip-rule="evenodd"></path>
      </svg>
   </li>
   <li class="inline-flex items-center">
      <a href="<?php echo e(route('despesas')); ?>">Despesas</a>
      <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
         <path fill-rule="evenodd"
            d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
            clip-rule="evenodd"></path>
      </svg>
   </li>
   <li class="inline-flex items-center">
      <a href="#" class="text-teal-400">Despesa</a>
   </li>
</ul>

<hr class="border-b-2 border-lightBlue-300 mb-12 mx-4">

<div class="relative flex flex-col-reverse md:flex-row gap-3 w-full mb-4">
   <div class="flex px-4 pb-4 bg-white h-auto w-full border-b-4 border-lightBlue-300 rounded-md flex-col">
      <div id="container" class="h-full w-full"></div>
   </div>

   <div class="flex px-4 py-4 justify-center bg-white border-b-4 border-lightBlue-300 rounded-md flex-col">
      <div class="px-5 py-4 bg-white border border-gray-200 rounded-md">
         <div class="card rounded-xl flex flex-col">
            <div class="grid grid-row-4 md:grid-row-4 gap-3">
               <div class="py-3 pl-5 bg-gray-50 text-sm rounded-md shadow" name="texto"
                  style="font-size: 0.875rem; color: #333333; line-height: 1.25rem;">
                  <ul class="flex flex-wrap">
                     <li><span class="font-semibold text-red-600">Despesa total:</span></li>
                     <li class="w-full">R$ <?php echo e(number_format($sumtotal, 2, ',', '.')); ?></li>
                  </ul>
               </div>
               <div class="py-3 pl-5 bg-gray-50 text-sm rounded-md shadow" name="texto"
                  style="font-size: 0.875rem; color: #333333; line-height: 1.25rem;">
                  <ul class="flex flex-wrap">
                     <li class="font-semibold mr-2 text-gray-600">Fase: </li>
                     <li class="w-full"><?php echo e($nomefase); ?></li>
                  </ul>
               </div>
               <div class="py-3 pl-5 bg-gray-50 text-sm rounded-md shadow" name="texto"
                  style="font-size: 0.875rem; color: #333333; line-height: 1.25rem;">
                  <ul class="flex flex-wrap">
                     <li class="font-semibold mr-2 text-gray-600">Classificação: </li>
                     <li class="w-full">Favorecido</li>
                  </ul>
               </div>
               <div class="py-3 pl-5 bg-gray-50 text-sm rounded-md shadow" name="texto"
                  style="font-size: 0.875rem; color: #333333; line-height: 1.25rem;">
                  <ul class="flex flex-wrap">
                     <?php if($posicao == '=' or $mes == 1): ?>
                     <li class="font-semibold mr-2 text-gray-600">Exercício: </li>
                     <li class="w-full"><?php echo e(__('meses.'.$mes)); ?></li>
                     <?php else: ?>
                     <li class="font-semibold mr-2 text-gray-600">Periodo: </li>
                     <li class="w-full">Janeiro a
                        <?php echo e(strtolower(__('meses.'.$mes))); ?></li>
                     <?php endif; ?>
                  </ul>
               </div>
               <div class="py-3 pl-5 bg-gray-50 text-sm rounded-md shadow" name="texto"
                  style="font-size: 0.875rem; color: #333333; line-height: 1.25rem;">
                  <ul class="flex flex-wrap">
                     <li class="font-semibold mr-2 text-gray-600">Ano: </li>
                     <li class="w-full"><?php echo e($ano); ?></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="w-full max-w-xl mx-auto">
   <div class="px-7 bg-white shadow-lg rounded-2xl mb-5">
      <div class="flex">
         <div class="flex-auto hover:w-full group">
            <form target="_blank" action="<?php echo e(route('export-xls-gastos-diretos', [
               'mes' => $mes,
               'ano' => $ano,
               'posicao' => $posicao,
               'classificacao' => $classificacao,
               'fase' => $fase
            ])); ?>" method="POST" enctype="multipart/form-data">
               <?php echo csrf_field(); ?>
               <button type="submit"
                  class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-green-500">
                  <span
                     class="block px-1 py-1 border border-transparent group-hover:border-green-500 rounded-full group-hover:flex-grow">
                     <i class="far fa-file-excel text-2xl pt-1"></i><span
                        class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Baixar arquivo XLS</span>
                  </span>
               </button>
            </form>
         </div>
         <div class="flex-auto hover:w-full group">
            <form target="_blank" action="<?php echo e(route('export-pdf-gastos-diretos', [
               'mes' => $mes,
               'ano' => $ano,
               'posicao' => $posicao,
               'classificacao' => $classificacao,
               'fase' => $fase
            ])); ?>" method="POST" enctype="multipart/form-data">
               <?php echo csrf_field(); ?>
               <button type="submit"
                  class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-red-500">
                  <span
                     class="block px-1 py-1 border border-transparent group-hover:border-red-500 rounded-full group-hover:flex-grow">
                     <i class="far fa-file-pdf text-2xl pt-1"></i><span
                        class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Baixar arquivo PDF</span>
                  </span>
               </button>
            </form>
         </div>
         <div class="flex-auto hover:w-full group">
            <a href="#" onclick="window.print()"
               class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-blueGray-500">
               <span
                  class="block px-1 py-1 border border-transparent group-hover:border-blueGray-500 rounded-full group-hover:flex-grow">
                  <i class="fas fa-print text-2xl pt-1"></i><span
                     class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Imprimir arquivo</span>
               </span>
            </a>
         </div>
         <div class="flex-auto hover:w-full group">
            <a href="<?php echo e(asset('docs/dicionario-de-dados/Dicionario de Dados Abertos-RN - Despesa - Gastos direto.xlsx')); ?>"
               target="_blank"
               class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-indigo-500">
               <span
                  class="block px-1 py-1 border border-transparent group-hover:border-indigo-500 rounded-full group-hover:flex-grow">
                  <i class="far fa-file-alt text-2xl pt-1"></i><span
                     class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Dicionario de dados</span>
               </span>
            </a>
         </div>
         <div class="flex-auto hover:w-full group">
            <a href="https://creativecommons.org/licenses/by/4.0/?ref=chooser-v1" target="_blank"
               class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-orange-500">
               <span
                  class="block px-1 py-1 border border-transparent group-hover:border-orange-500 rounded-full group-hover:flex-grow">
                  <i class="far fa-closed-captioning text-2xl pt-1"></i>
                  <span class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Licença</span>
               </span>
            </a>
         </div>
      </div>
   </div>
</div>

<?php if($fase == 'vportal_notapagamento_gastosdiretos'): ?>

<?php echo $busca; ?>


<div class="flex flex-col pb-4">
   <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
      <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
         <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
            <table id="tree" class="min-w-full divide-y divide-gray-200">
               <thead class="bg-lightBlue-100">
                  <tr>
                     <th class="px-6 py-3 text-left text-sm font-semibold text-gray-800 uppercase tracking-wider"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        CPF/CNPJ/IG</th>
                     <th class="px-6 py-3 text-left text-sm font-semibold text-gray-800 uppercase tracking-wider"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        Favorecido</th>
                     <th class="px-6 py-3 text-left text-sm font-semibold text-gray-800 uppercase tracking-wider"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        Exercício Corrente</th>
                     <th class="px-6 py-3 text-right text-sm font-semibold text-gray-800 uppercase tracking-wider"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">Restos
                        a Pagar</th>
                  </tr>
               </thead>
               <tbody id="table" data-resetable="false" class="bg-white divide-y divide-gray-200 text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">
                  <?php $__currentLoopData = $favorecidos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $favorecido): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <tr data-key="<?php echo e($key); ?>" data-fetch="false" data-codigo='{"codcredor": "<?php echo e($favorecido->codcredor); ?>","txtnomecredor": "<?php echo e($favorecido->txtnomecredor); ?>"}' class="treegrid-<?php echo e($key); ?>">
                     <td class="px-6 py-4 whitespace-nowrap">
                        <?php if(strlen($favorecido->codcredor) == 11): ?>
                        <?php echo e("###.". substr($favorecido->codcredor, 3, 3) . "." . substr($favorecido->codcredor, 6, 3) .
                        "-##"); ?>

                        <?php else: ?>
                        <?php echo e($favorecido->codcredor); ?>

                        <?php endif; ?>
                     </td>
                     <td class="px-6 py-4 whitespace-nowrap"><?php echo e($favorecido->txtnomecredor); ?></td>
                     <td class="px-6 py-4 whitespace-nowrap">
                        <span
                           class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-yellow-100 text-yellow-800"
                           name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                           R$ <?php echo e(number_format($favorecido->vlrpagamento, 2, ',', '.')); ?>

                        </span>
                     </td>
                     <td class="px-6 py-4 whitespace-nowrap text-right">
                        <span
                           class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800"
                           name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                           R$ <?php echo e(number_format($favorecido->vlrpagamentorp, 2, ',', '.')); ?>

                        </span>
                     </td>
                  </tr>
                  <tr class="treegrid-parent-<?php echo e($key); ?> bg-gray-100">
                     <th class="px-6 py-3 text-left text-sm font-semibold text-gray-800 uppercase tracking-wider"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        Grupo de Despesa
                     </th>
                     <th class="px-6 py-3 text-left text-sm font-semibold text-gray-800 uppercase tracking-wider"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">Elemento de Despesa
                     </th>
                     <th class="px-6 py-3 text-left text-sm font-semibold text-gray-800 uppercase tracking-wider"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">Exercício Corrente
                     </th>
                     <th class="px-6 py-3 text-left text-sm font-semibold text-gray-800 uppercase tracking-wider"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">Restos a Pagar
                     </th>
                  </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
               </tbody>
            </table>
            <div id="pag"><?php echo e($favorecidos->appends(Request::all())->links()); ?></div>
         </div>
      </div>
   </div>
</div>
<?php else: ?>

<?php echo $busca; ?>


<div class="flex flex-col pb-4">
   <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
      <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
         <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
            <table id="tree" class="min-w-full divide-y divide-gray-200">
               <thead class="bg-lightBlue-100">
                  <tr>
                     <th class="px-6 py-3 text-left text-sm font-semibold text-gray-800 uppercase tracking-wider"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        CPF/CNPJ/IG</th>
                     <th class="px-6 py-3 text-left text-sm font-semibold text-gray-800 uppercase tracking-wider"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        Favorecido</th>
                     <th class="px-6 py-3 text-right text-sm font-semibold text-gray-800 uppercase tracking-wider"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">Valor
                        (R$)</th>
                  </tr>
               </thead>
               <tbody id="table" data-resetable="false" class="bg-white divide-y divide-gray-200 text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">
                  <?php $__currentLoopData = $favorecidos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $favorecido): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <tr data-key="<?php echo e($key); ?>" data-fetch="false" data-codigo='{"codcredor": "<?php echo e($favorecido->codcredor); ?>","txtnomecredor": "<?php echo e($favorecido->txtnomecredor); ?>"}' class="treegrid-<?php echo e($key); ?>">
                     <td class="px-6 py-4 whitespace-nowrap">
                        <?php if(strlen($favorecido->codcredor) == 11): ?>
                        <?php echo e("###.". substr($favorecido->codcredor, 3, 3) . "." . substr($favorecido->codcredor, 6, 3) .
                        "-##"); ?>

                        <?php else: ?>
                        <?php echo e($favorecido->codcredor); ?>

                        <?php endif; ?>
                     </td>
                     <td class="px-6 py-4 whitespace-nowrap"><?php echo e($favorecido->txtnomecredor); ?></td>
                     <td class="px-6 py-4 whitespace-nowrap text-right">
                        <span
                           class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800"
                           name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                           R$ <?php echo e(number_format($favorecido->vlrpagamento, 2, ',', '.')); ?>

                        </span>
                     </td>
                     <tr class="treegrid-parent-<?php echo e($key); ?> bg-gray-100">
                        <th class="px-6 py-3 text-left text-sm font-semibold text-gray-800 uppercase tracking-wider"
                           name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                           Grupo de Despesa
                        </th>
                        <th class="px-6 py-3 text-left text-sm font-semibold text-gray-800 uppercase tracking-wider"
                           name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">ELEMENTO DE DESPESA
                        </th>
                        <th class="px-6 py-3 text-right text-sm font-semibold text-gray-800 uppercase tracking-wider"
                           name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">Valor (R$)
                        </th>
                     </tr>
                  </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
               </tbody>
            </table>
            <div id="pag"><?php echo e($favorecidos->appends(Request::all())->links()); ?></div>
         </div>
      </div>
   </div>
</div>
<?php endif; ?>
<div class="alert flex flex-row items-center bg-blue-200 p-1 rounded">
   <div
      class="alert-icon flex items-center bg-blue-100 border-2 border-blue-500 justify-center flex-shrink-0 rounded-full">
      <span class="text-blue-500">
         <svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
            <path fill-rule="evenodd"
               d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z"
               clip-rule="evenodd"></path>
         </svg>
      </span>
   </div>
   <div class="alert-content ml-4">
      <div class="alert-description text-xs text-blue-600" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
         <span class="font-semibold">Dados atualizados em:</span> <?php echo e(date("d/m/Y")); ?> | <span class="font-semibold">Fonte:</span>
         SIGEF - SISTEMA INTEGRADO DE PLANEJAMENTO E GESTÃO FISCAL
      </div>
   </div>
</div>

<meta id="info" data-source="despesa" data-page="favorecido" data-reloadjs="true" data-classificacao="<?php echo e($classificacao); ?>" data-mes="<?php echo e($mes); ?>" fase="<?php echo e($fase); ?>" data-ano="<?php echo e($ano); ?>" data-posicao="<?php echo e($posicao); ?>">

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="<?php echo e(asset('js/select2.full.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery.treegrid.min.js')); ?>"></script>

<script>
   function reload_js(src) {
      $('script[src="' + src + '"]').remove();
      $('<script>').attr('src', src).appendTo('#content');
      $("#tree").treegrid({ initialState: 'collapsed' });
   }
</script>

<script>
   $("#tree").treegrid({ initialState: 'collapsed' });
</script>


<script src="<?php echo e(asset('js/search.js')); ?>"></script>

<script src="<?php echo e(asset('js/dynamicPage.js')); ?>"></script>

<script>
   const formatNumber = new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' })

   <?php if($fase == 'vportal_notapagamento_gastosdiretos'): ?>
      const title = 'Total da Despesa paga'
   <?php elseif($fase == 'vportal_notaempenho_gastosdiretos'): ?>
      const title = 'Total da Despesa empenhada'
   <?php else: ?>
      const title = 'Total da Despesa liquidada'
   <?php endif; ?>

   <?php if($posicao == '=' or $mes == 1): ?>
      const subtitle = `<?php echo e(strtolower(__('meses.'.$mes))); ?> de <?php echo e($ano); ?>`
   <?php else: ?>
      const subtitle = `janeiro a <?php echo e(strtolower(__('meses.'.$mes))); ?> de <?php echo e($ano); ?>`
   <?php endif; ?>

   // Create the chart
   Highcharts.setOptions({
         lang: {
            drillUpText: `◁ Voltar`
         }
   });

   Highcharts.chart('container', {
       chart: {
           type: 'column',
           spacingBottom: 30
       },

       credits : {
          style: {
            color: '#333'
          }
       },

       title: {
           text: title
       },

       subtitle: {
           text: subtitle
       },

       accessibility: {
           announceNewData: {
               enabled: true
           }
       },

       xAxis: {
           type: 'category'
       },

       yAxis: {
           title: {
               text: 'Despesas em R$'
           },
           labels : {
              enabled: true,
              formatter: function() {
                 return formatNumber.format(this.value ** 3)
              }
           }
       },

       legend: {
           enabled: false
       },

       plotOptions: {
           series: {
               borderWidth: 0,
               dataLabels: {
                   enabled: true,
                   formatter: function() {
                      return formatNumber.format(this.point.y ** 3)
                   }
               }
           }
       },

       tooltip: {
           formatter: function() {
              return '<span style="font-size:11px">' + this.series.name + '</span><br>' +
              '<span style="color:' + this.point.color + '">' + this.point.name + '</span>: <b>' +
              formatNumber.format(this.point.y ** 3) + '</b> de despesa<br/>'
            }
       },

       series: [
           {
               name: "Favorecidos",
               colorByPoint: true,
               data: [
                  <?php $__currentLoopData = $favorecidos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $favorecido): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                   {
                        name: `<?php echo e($favorecido->txtnomecredor); ?>`,
                        <?php if($favorecido->vlrpagamento >= 0): ?>
                           y: Math.pow(<?php echo e($favorecido->vlrpagamento); ?>, 1 / 3),
                        <?php else: ?>
                           y: (Math.pow((<?php echo e($favorecido->vlrpagamento); ?> * (-1)), 1 / 3)) * (-1),
                        <?php endif; ?>
                        drilldown: `<?php echo e($favorecido->codcredor); ?>`
                   },
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
               ]
           }
       ],
       drilldown: {

        activeAxisLabelStyle: {
            textDecoration: 'none',
            color: '#666',
            fontWeight: 'normal'
        },

        activeDataLabelStyle: {
            textDecoration: 'none',
            color: '#000'
        },

           series: [
              <?php $__currentLoopData = $favorecidos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $favorecido): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
               {
                   name: `Grupos de Despesas`,
                   id: `<?php echo e($favorecido->codcredor); ?>`,
                   data: [
                     <?php $__currentLoopData = $grupodespesacharts->where('codcredor', $favorecido->codcredor); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $grupo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                       [
                           "<?php echo e($grupo->txtdescricaogrupodespesa); ?>",
                           <?php if($grupo->vlrpagamento >= 0): ?>
                              Math.pow(<?php echo e($grupo->vlrpagamento); ?>, 1 / 3),
                           <?php else: ?>
                              (Math.pow((<?php echo e($grupo->vlrpagamento); ?> * (-1)), 1 / 3)) * (-1),
                           <?php endif; ?>
                       ],
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  ]
               },
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            ]
       }
   });
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/portaltransparencianewrn/resources/views/despesas/gastos-diretos/favorecido/index.blade.php ENDPATH**/ ?>