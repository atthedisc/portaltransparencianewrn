<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="<?php echo e(asset('images/icons/favicon.ico')); ?>" type="image/ico">
    <title>Portal da Transparência do Rio Grande do Norte <?php echo $__env->yieldContent('title'); ?></title>

    
    <link rel="stylesheet" href="<?php echo e(asset('css/app.css')); ?>">

    
    <link rel="stylesheet" href="<?php echo e(asset('css/main.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/nav.css')); ?>">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <?php echo $__env->yieldContent('css'); ?>
    <?php echo $__env->yieldContent('js'); ?>

</head>

<body class="bg-warmGray-50 font-sans leading-normal tracking-normal">

    <div class="container-fluid container w-full mx-auto lg:pt-14 md:pt-14 pb-10">
       <?php echo $__env->yieldContent('content'); ?>
    </div>

 </body>

</html>
<?php /**PATH /var/www/html/portaltransparencianewrn/resources/views/layouts/errors/app.blade.php ENDPATH**/ ?>