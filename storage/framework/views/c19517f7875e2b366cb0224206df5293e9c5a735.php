<?php $__env->startSection('title'); ?>
- RN Mais Vacina
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<link rel="stylesheet" href="<?php echo e(asset('css/exportfiles.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<h1 class="text-lg text-gray-900 text-center tracking-widest font-medium title-font pb-2 uppercase" name="texto"
    style="font-size: 1.125rem; line-height: 2rem;">
    <i class="fas fa-medkit"></i>
    Vacinação Covid-19: MP 1026/2021 - Compras e Serviços
</h1>

<ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto"
    style="font-size: 0.875rem; line-height: 1.25rem;">
    <li class="inline-flex items-center">
        <a href="<?php echo e(url('/'. $status)); ?>">Inicio</a>
        <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
            <path fill-rule="evenodd"
                d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                clip-rule="evenodd"></path>
        </svg>
    </li>
    <li class="inline-flex items-center">
        <a href="#" class="text-teal-400">Vacinação COVID-19: MP 1026/2021 - Compras e Serviços</a>
    </li>
</ul>

<hr class="border-b-2 border-lightBlue-300 mb-12 mx-4">

<div class="w-full max-w-xl mx-auto">
    <div class="px-7 bg-white shadow-lg rounded-2xl mb-5">
        <div class="flex">
            <div class="flex-auto hover:w-full group">
                <form target="_blank" action="<?php echo e(route('export-xls-rn-mais-vacina')); ?>" method="POST"
                    enctype="multipart/form-data">
                    <?php echo csrf_field(); ?>
                    <button type="submit"
                        class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-green-500">
                        <span
                            class="block px-1 py-1 border border-transparent group-hover:border-green-500 rounded-full group-hover:flex-grow">
                            <i class="far fa-file-excel text-2xl pt-1"></i><span
                                class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Baixar arquivo XLS</span>
                        </span>
                    </button>
                </form>
            </div>
            <div class="flex-auto hover:w-full group">
                <form target="_blank" action="<?php echo e(route('export-pdf-rn-mais-vacina')); ?>" method="POST"
                    enctype="multipart/form-data">
                    <?php echo csrf_field(); ?>
                    <button type="submit"
                        class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-red-500">
                        <span
                            class="block px-1 py-1 border border-transparent group-hover:border-red-500 rounded-full group-hover:flex-grow">
                            <i class="far fa-file-pdf text-2xl pt-1"></i><span
                                class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Baixar arquivo PDF</span>
                        </span>
                    </button>
                </form>
            </div>
            <div class="flex-auto hover:w-full group">
                <a href="#" onclick="window.print()"
                    class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-blueGray-500">
                    <span
                        class="block px-1 py-1 border border-transparent group-hover:border-blueGray-500 rounded-full group-hover:flex-grow">
                        <i class="fas fa-print text-2xl pt-1"></i><span
                            class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Imprimir arquivo</span>
                    </span>
                </a>
            </div>
            <div class="flex-auto hover:w-full group">
                <a href="<?php echo e(asset('docs/dicionario-de-dados/Dicionario de Dados Abertos-RN - RN Mais Vacina (04.02.2021).xlsx')); ?>"
                    target="_blank"
                    class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-indigo-500">
                    <span
                        class="block px-1 py-1 border border-transparent group-hover:border-indigo-500 rounded-full group-hover:flex-grow">
                        <i class="far fa-file-alt text-2xl pt-1"></i><span
                            class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Dicionario de dados</span>
                    </span>
                </a>
            </div>
            <div class="flex-auto hover:w-full group">
                <a href="https://creativecommons.org/licenses/by/4.0/?ref=chooser-v1" target="_blank"
                    class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-orange-500">
                    <span
                        class="block px-1 py-1 border border-transparent group-hover:border-orange-500 rounded-full group-hover:flex-grow">
                        <i class="far fa-closed-captioning text-2xl pt-1"></i>
                        <span class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Licença</span>
                    </span>
                </a>
            </div>
        </div>
    </div>
</div>

<?php echo $busca; ?>


<div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
    <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
        <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
            <table class="min-w-full divide-y divide-lightBlue-200">
                <thead class="bg-lightBlue-50">
                    <tr>
                        <th></th>
                        <th scope="col"
                            class="px-6 py-3 text-left text-xs font-medium text-lightBlue-700 uppercase tracking-wider"
                            name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                            Contratante
                        </th>
                        <th scope="col"
                            class="px-6 py-3 text-left text-xs font-medium text-lightBlue-700 uppercase tracking-wider"
                            name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                            Objeto
                        </th>
                        <th scope="col"
                            class="px-6 py-3 text-left text-xs font-medium text-lightBlue-700 uppercase tracking-wider"
                            name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                            Contratado(a)
                        </th>
                        <th scope="col"
                            class="px-6 py-3 text-left text-xs font-medium text-lightBlue-700 uppercase tracking-wider"
                            name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                            CNPJ/CPF
                        </th>
                        <th scope="col"
                            class="px-6 py-3 text-left text-xs font-medium text-lightBlue-700 uppercase tracking-wider"
                            name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                            Ato Contratual
                        </th>
                        <th scope="col"
                            class="px-6 py-3 text-left text-xs font-medium text-lightBlue-700 uppercase tracking-wider"
                            name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                            N. Processo
                        </th>
                        <th scope="col"
                            class="px-6 py-3 text-left text-xs font-medium text-lightBlue-700 uppercase tracking-wider"
                            name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                            Valor do Contrato (R$)
                        </th>
                        <th scope="col"
                            class="px-6 py-3 text-left text-xs font-medium text-lightBlue-700 uppercase tracking-wider"
                            name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                            Parcelas do Objeto
                        </th>
                    </tr>
                </thead>
                <tbody id="table" data-resetable="false" class="bg-white divide-y divide-lightBlue-200 text-sm" name="texto"
                    style="font-size: 0.875rem; line-height: 1.25rem;">
                    <?php $__currentLoopData = $tables; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tabela): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td>
                            <div>
                                <svg onclick="toggleModal(<?php echo e($tabela->id); ?>)" id="card_plus<?php echo e($tabela->id); ?>"
                                    xmlns="http://www.w3.org/2000/svg"
                                    class="mx-4 h-5 w-5 text-green-600 cursor-pointer" viewBox="0 0 20 20"
                                    fill="currentColor">
                                    <path fill-rule="evenodd"
                                        d="M10 18a8 8 0 100-16 8 8 0 000 16zm1-11a1 1 0 10-2 0v2H7a1 1 0 100 2h2v2a1 1 0 102 0v-2h2a1 1 0 100-2h-2V7z"
                                        clip-rule="evenodd" />
                                </svg>
                            </div>
                            <svg id="card_minus<?php echo e($tabela->id); ?>" xmlns="http://www.w3.org/2000/svg"
                                class="hidden mx-4 h-5 w-5 text-red-600" viewBox="0 0 20 20" fill="currentColor">
                                <path fill-rule="evenodd"
                                    d="M10 18a8 8 0 100-16 8 8 0 000 16zM7 9a1 1 0 000 2h6a1 1 0 100-2H7z"
                                    clip-rule="evenodd" />
                            </svg>
                        </td>

                        <td class="px-10 py-4 whitespace-nowrap ">
                            <?php echo e($tabela->contratante); ?>

                        </td>
                        <td class="px-4 py-4 whitespace-normal ">
                            <?php echo e($tabela->objeto); ?>

                        </td>
                        <td class="px-4 py-4 whitespace-normal">
                            <?php echo e($tabela->contratado); ?>

                        </td>
                        <td class="px-4 py-4 whitespace-nowrap">
                            <?php echo e($tabela->cnpjcpf); ?>

                        </td>
                        <td class="px-4 py-4 whitespace-normal">
                            <?php echo e($tabela->atocontratual); ?>

                        </td>
                        <td class="px-4 py-4 whitespace-nowrap">
                            <?php echo e($tabela->nprocesso); ?>

                        </td>
                        <td class="px-4 py-4 whitespace-nowrap">
                            R$ <?php echo e(number_format($tabela->valorcontrato, 2, ',', '.')); ?>

                        </td>
                        <td class="px-4 py-4 text-center whitespace-nowrap">
                            <?php echo e($tabela->parcelaobjeto); ?>

                        </td>
                    </tr>

                    <!--Modal-->
                    <div id="modal<?php echo e($tabela->id); ?>"
                        class="modal opacity-0 pointer-events-none fixed w-full h-full top-0 left-0 flex items-center justify-center">
                        <div class="modal-overlay absolute w-full h-full bg-gray-900 opacity-50"></div>

                        <div
                            class="modal-container bg-white w-11/12 md:max-w-2xl mx-auto rounded shadow-lg z-50 overflow-y-auto">
                            <div class="modal-close absolute top-0 right-0 cursor-pointer flex flex-col items-center mt-4 mr-4 text-white text-sm z-50"
                                name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                <svg onclick="toggleModal(<?php echo e($tabela->id); ?>)" class="fill-current text-white"
                                    xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                                    <path
                                        d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z">
                                    </path>
                                </svg>
                                <span class="text-sm" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">(Esc)</span>
                            </div>

                            <!-- Add margin if you want to see some of the overlay behind the modal-->
                            <div class="modal-content py-2 pb-6 text-left px-6">

                                <!--botão X de fechar-->
                                <div class="flex justify-end items-center">
                                    <div class="modal-close cursor-pointer z-50">
                                        <svg onclick="toggleModal(<?php echo e($tabela->id); ?>)" class="fill-current text-red-500"
                                            xmlns="http://www.w3.org/2000/svg" width="22" height="22" font-weight="bold"
                                            viewBox="0 0 18 18">
                                            <path
                                                d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z">
                                            </path>
                                        </svg>
                                    </div>
                                </div>

                                <!--Body-->
                                <div id=<?php echo e($tabela->id); ?>>
                                    <div class="modal-content text-left px-6">
                                        <div class="flex justify-between items-center pb-3">
                                            <p class="text-sm font-bold text-lightBlue-500" name="texto"
                                                style="font-size: 0.875rem; line-height: 1.25rem;">Mais informações</p>
                                        </div>
                                        <div>
                                            <p class="text-sm" name="texto"
                                                style="font-size: 0.875rem; line-height: 1.25rem;"><b
                                                    class="mr-2 text-gray-700">Data Assinatura:</b>
                                                <?php echo e($tabela->dataassinatura); ?></p>
                                            <p class="text-sm" name="texto"
                                                style="font-size: 0.875rem; line-height: 1.25rem;"><b
                                                    class="mr-2 text-gray-700">Vigência:</b>
                                                <?php echo e($tabela->vigencia); ?></p>
                                            <p class="text-sm" name="texto"
                                                style="font-size: 0.875rem; line-height: 1.25rem;"><b
                                                    class="mr-2 text-gray-700">Local de
                                                    execução:</b><?php echo e($tabela->localdeexecucao); ?></p>
                                            <p class="text-sm" name="texto"
                                                style="font-size: 0.875rem; line-height: 1.25rem;"><b
                                                    class="mr-2 text-gray-700">Ato de Autorização de
                                                    Contrato:</b>
                                                <?php echo e($tabela->atoautirizacaocontratacao); ?>

                                            </p>
                                            <p class="text-sm" name="texto"
                                                style="font-size: 0.875rem; line-height: 1.25rem;"><b
                                                    class="mr-2 text-gray-700">Valor Empenhado:</b>R$
                                                <?php echo e(number_format($tabela->valorempenhado, 2, ',', '.')); ?></p>
                                            <p class="text-sm" name="texto"
                                                style="font-size: 0.875rem; line-height: 1.25rem;"><b
                                                    class="mr-2 text-gray-700">Quantidade de Entrega:</b>
                                                <?php echo e($tabela->quantidadeentrega); ?></p>
                                            <p class="text-sm" name="texto"
                                                style="font-size: 0.875rem; line-height: 1.25rem;"><b
                                                    class="mr-2 text-gray-700">Valor Pago: </b>R$
                                                <?php echo e(number_format($tabela->valorpago, 2, ',', '.')); ?></p>
                                            <p class="text-sm" name="texto"
                                                style="font-size: 0.875rem; line-height: 1.25rem;"><b
                                                    class="mr-2 text-gray-700">Valor a Pagar: </b>R$
                                                <?php echo e(number_format($tabela->valorapagar, 2, ',', '.')); ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
            <div id="pag"><?php echo e($tables->links()); ?></div>
        </div>
    </div>
</div>


<div class="pt-4">
    <div class="p-2 text-blueGray-800 uppercase tracking-wider text-xs text-right" name="texto"
        style="font-size: 0.75rem; line-height: 1rem;">
        <i class="fas fa-arrow-up"></i>
        Dados atualizados em: 30/08/2023
    </div>
</div>

<meta id="info" data-source="rn_mais_vacina" data-page="rn_mais_vacina" data-reloadJs="false">

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="<?php echo e(asset('js/select2.full.min.js')); ?>"></script>


<script src="<?php echo e(asset('js/search.js')); ?>"></script>

<style>
    .modal {
        transition: opacity 0.25s ease;
        z-index: 200;
    }

    body.modal-active {
        overflow-x: hidden;
        overflow-y: visible !important;
    }
</style>

<script>
    function toggleModal (id) {
        const plus = document.querySelector('#card_plus' + id)
        const minus = document.querySelector('#card_minus' + id)
        
        if (plus.classList.contains('hidden')) {
            // Show button
            plus.classList.remove('hidden')
            plus.classList.add('block')
         
            // Delete button Minus
            minus.classList.add('hidden')
            minus.classList.remove('block')
        } else {
            // Delete button Plus
            plus.classList.add('hidden')
            plus.classList.remove('block')

            // Show button Minus
            minus.classList.add('block')
            minus.classList.remove('hidden')
        }
        
        const body = document.querySelector('body')
        const modal = document.querySelector('#modal' + id)
        modal.classList.toggle('opacity-0')
        modal.classList.toggle('pointer-events-none')
        body.classList.toggle('modal-active')

        document.onkeydown = function(evt) {
            evt = evt || window.event
            var isEscape = false

            if ("key" in evt) {
                isEscape = (evt.key === "Escape" || evt.key === "Esc")
            } else {
                isEscape = (evt.keyCode === 27)
            }

            if (isEscape && document.body.classList.contains('modal-active')) {
                toggleModal(id)
            }
        };
    }
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make($layouts, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/portaltransparencianewrn/resources/views/rn-mais-vacina/index.blade.php ENDPATH**/ ?>