<?php $__env->startSection('css'); ?>
<link rel="stylesheet" href="<?php echo e(asset('css/glossario.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('title'); ?>
- Glossário
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<?php echo csrf_field(); ?>
<div class="flex flex-grow flex-col overflow-hidden">

    <h1 class="text-lg text-gray-900 tracking-widest font-medium title-font uppercase text-center pb-2" name="texto"
        style="font-size: 1.125rem; line-height: 2rem;">
        <i class="fas fa-book-open"></i>
        Glossário
    </h1>

    <p class="lg:w-3/4 mx-auto leading-relaxed text-sm text-center text-gray-600" name="texto"
        style="font-size: 0.875rem; line-height: 1.25rem;">
        Lista de palavras e termos utilizados no Portal da Transparência RN
    </p>

    <ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto"
        style="font-size: 0.875rem; line-height: 1.25rem;">
        <li class="inline-flex items-center">
            <a href="<?php echo e(url('/')); ?>">Início</a>
            <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                <path fill-rule="evenodd"
                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                    clip-rule="evenodd"></path>
            </svg>
        </li>
        <li class="inline-flex items-center">
            <a href="#" class="text-teal-400">
                Glossário
            </a>
        </li>
    </ul>

    <hr class="border-b-2 border-lightBlue-300 mb-6 mx-4">

    <div class="flex px-2 -mb-px justify-center">
        <div class="flex flex-wrap pb-6">
            <div class="w-auto mx-3">
                <div class="px-2 py-2 text-center uppercase" name="texto" style="font-size: 1rem; line-height: 1.5rem;">
                    <i class="fas fa-filter"></i>
                    Filtrar por letra:
                </div>
                <table id="table" data-display="" class="w-full" name="texto"
                    style="font-size: 1rem; line-height: 1.5rem;">
                    <tr>
                        <td class="hover:text-white text-center cursor-pointer" name="texto"
                            style="font-size: 0.875rem; line-height: 1.25rem;">TODAS</td>
                    </tr>
                    <tr>
                        <td class="py-3 px-4 hover:text-white text-center cursor-pointer">A</td>
                        <td class="py-3 px-4 hover:text-white text-center cursor-pointer">B</td>
                        <td class="py-3 px-4 hover:text-white text-center cursor-pointer">C</td>
                        <td class="py-3 px-4 hover:text-white text-center cursor-pointer">D</td>
                        <td class="py-3 px-4 hover:text-white text-center cursor-pointer">E</td>
                        <td class="py-3 px-4 hover:text-white text-center cursor-pointer">F</td>
                        <td class="py-3 px-4 hover:text-white text-center cursor-pointer">G</td>
                    </tr>
                    <tr>
                        <td class="py-3 px-4 hover:text-white text-center cursor-pointer">H</td>
                        <td class="py-3 px-4 hover:text-white text-center cursor-pointer">I</td>
                        <td class="py-3 px-4 hover:text-white text-center cursor-pointer">J</td>
                        <td class="py-3 px-4 hover:text-white text-center cursor-pointer">K</td>
                        <td class="py-3 px-4 hover:text-white text-center cursor-pointer">L</td>
                        <td class="py-3 px-4 hover:text-white text-center cursor-pointer">M</td>
                        <td class="py-3 px-4 hover:text-white text-center cursor-pointer">N</td>
                    </tr>
                    <tr>
                        <td class="py-3 px-4 hover:text-white text-center cursor-pointer">O</td>
                        <td class="py-3 px-4 hover:text-white text-center cursor-pointer">P</td>
                        <td class="py-3 px-4 hover:text-white text-center cursor-pointer">Q</td>
                        <td class="py-3 px-4 hover:text-white text-center cursor-pointer">R</td>
                        <td class="py-3 px-4 hover:text-white text-center cursor-pointer">S</td>
                        <td class="py-3 px-4 hover:text-white text-center cursor-pointer">T</td>
                        <td class="py-3 px-4 hover:text-white text-center cursor-pointer">U</td>
                    </tr>
                    <tr>
                        <td class="py-3 px-4 hover:text-white text-center cursor-pointer">V</td>
                        <td class="py-3 px-4 hover:text-white text-center cursor-pointer">W</td>
                        <td class="py-3 px-4 hover:text-white text-center cursor-pointer">X</td>
                        <td class="py-3 px-4 hover:text-white text-center cursor-pointer">Y</td>
                        <td class="py-3 px-4 hover:text-white text-center cursor-pointer">Z</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div class="flex flex-wrap justify-center pb-4">
        <div class="w-36">
            <div class="border-b-2 border-lightBlue-500">
                <div id="letra" class="tracking-widest text-lg text-center text-lightBlue-500 uppercase" name="texto"
                    style="font-size: 1.125rem; line-height: 1.75rem;">
                    Todas
                </div>
            </div>
        </div>
    </div>

    <div id="container" class="flex flex-wrap justify-center">
        <?php $__currentLoopData = $tables; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tabela): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="w-full md:w-1/1 xl:w-1/1 p-3">
            <div class="p-2 bg-white shadow-md hover:shodow-lg rounded-2xl">
                <div class="font-medium text-left text-lightBlue-500 uppercase pb-2" name="texto"
                    style="font-size: 1rem; line-height: 1.5rem;">
                    <?php echo e($tabela->nome); ?>:
                </div>
                <p class="text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                    <?php echo e($tabela->descricao); ?> <br>
                </p>
            </div>
        </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
    <div id="pag">
        <?php echo e($tables->links()); ?>

    </div>
</div>

<script src="<?php echo e(asset('js/jquery-latest.min.js')); ?>"></script>

<script>
    var _token = $('input[name="_token"]').val();
$('tr').on("click", "td", function() {
   var letra = $(this).text();
   $('#letra').html(letra);
   if($('#table').attr('data-display') != letra) {
      if(letra != 'TODAS') {
         $('#load').removeClass('hidden');
         $('#pag').addClass('hidden');
            $.ajax({
               type: 'POST', 
               url: '<?php echo e(route("cardglossario")); ?>',
               data: {letra:letra, _token:_token},
               
               success: function (data) {
                  $('#table').attr('data-display', letra);
                  $('#container').html('');
                  $('#container').html(data);
                  $('#load').addClass('hidden');
               },
               error: function(data) { 
               }
         });
      } else {
         $('#load').removeClass('hidden');
         $.ajax({
               type: 'POST', 
               url: '<?php echo e(route("glossario")); ?>',
               data: {_token:_token},
               
               success: function (data) {
                  $('#table').attr('data-display', letra);
                  $('#container').html('');
                  $('#container').html(data);
                  $('#pag').removeClass('hidden');
                  $('#load').addClass('hidden');
               },
               error: function(data) { 
               }
         });
      }
   }
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/portaltransparencianewrn/resources/views/glossario/index.blade.php ENDPATH**/ ?>