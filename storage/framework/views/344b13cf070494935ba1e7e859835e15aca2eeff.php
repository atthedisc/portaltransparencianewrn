<?php $__env->startSection('title'); ?>
- Fale conosco
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<section class="text-gray-600 body-font">
   <div class="container px-5 mx-auto">
      <div class="flex flex-col text-center w-full">

         <h1 class="text-lg text-gray-900 tracking-widest font-medium title-font pb-2" name="texto"
            style="font-size: 1.125rem; line-height: 2rem;">
            <i class="fas fa-comment-dots"></i>
            FALE CONOSCO
         </h1>

         <ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto"
            style="font-size: 0.875rem; line-height: 1.25rem;">
            <li class="inline-flex items-center">
               <a href="<?php echo e(url('/')); ?>">Início</a>
               <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                  <path fill-rule="evenodd"
                     d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                     clip-rule="evenodd"></path>
               </svg>
            </li>
            <li class="inline-flex items-center">
               <a href="#" class="text-teal-400">
                  Fale conosco
               </a>
            </li>
         </ul>

         <hr class="border-b-2 border-lightBlue-300 mb-6 mx-4">

         <div class="flex flex-wrap">
            <div class="w-full md:w-1/3 xl:w-1/3 p-3">
               <div class="rounded-lg p-3 bg-white">
                  <div class="text-center m-6">
                     <h1 class="text-xl font-semibold text-gray-700 hover:underline" name="texto"
                        style="font-size: 1.25rem; line-height: 1.75rem;">
                        <a href="<?php echo e(url('/')); ?>">
                           Portal da Transparência
                        </a>
                     </h1>
                     <div class="flex flex-row justify-center gap-2 my-5">
                        <a href="<?php echo e(asset('https://forms.gle/HVNH4cCxo333e1fm8')); ?>" target="_blank">
                           <button
                              class="lg:mt-2 xl:mt-0 text-white text-xs bg-blue-500 border-0 py-2 px-6 focus:outline-none hover:bg-blue-600 rounded">
                              Dê sua opinião
                           </button>
                        </a>
                     </div>
                     <span class="text-sm text-gray-500 pt-2" name="texto"
                        style="font-size: 0.875rem; line-height: 1.25rem;">
                        Dê sua opinião sobre o Portal da Transparência, ao final, você pode também deixar um comentário
                        ou sugestão!
                        Sua opinião é muito importante.
                     </span>
                  </div>
                  <div class="flex flex-row justify-center mb-8">
                     <span class="absolute text-lg bg-white font-semibold px-4 text-gray-700" name="texto"
                        style="font-size: 1.125rem; line-height: 1.75rem;">Endereço</span>
                     <div class="w-full bg-gray-200 mt-3 h-px"></div>
                  </div>
                  <span class="text-sm text-gray-500" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                     Centro Administrativo do Estado <br>
                     CONTROL - Controladoria Geral do Estado <br>
                     Av. Sen. Salgado Filho, 1 - Lagoa Nova, Natal - RN, 59064-901
                  </span>
                  <div class="flex flex-row justify-center mb-8 mt-6">
                     <span class="absolute text-lg bg-white font-semibold px-4 text-gray-700" name="texto"
                        style="font-size: 1.125rem; line-height: 1.75rem;">Contato</span>
                     <div class="w-full bg-gray-200 mt-3 h-px"></div>
                  </div>
                  <span class="text-sm text-gray-500" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                     Se preferir você pode entrar em contato com a equipe do Portal por e-mail. <br>
                     <span class="font-semibold">E-mail:</span> transparenciacontrolrn@gmail.com
                  </span>
               </div>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/3 p-3">
               <div class="rounded-lg p-3 bg-white">
                  <div class="text-center m-6">
                     <h1 class="text-xl font-semibold text-gray-700 hover:underline" name="texto"
                        style="font-size: 1.25rem; line-height: 1.75rem;">
                        <a href="<?php echo e(asset('http://www.sic.rn.gov.br/')); ?>" target="_blank">
                           Acesso à informação
                        </a>
                     </h1>
                     <div class="flex flex-row justify-center gap-2 my-5">
                        <a href="<?php echo e(asset('http://www.sic.rn.gov.br/')); ?>" class="text-gray-500 hover:text-black px-2"
                           target="_blank" name="texto" style="font-size: 0.75rem;">
                           <img src="<?php echo e(asset('images/acesso_a_informacao_logo.png')); ?>" alt="">
                        </a>
                     </div>
                     <span class="text-sm text-gray-500 pt-2" name="texto"
                        style="font-size: 0.875rem; line-height: 1.25rem;">
                        O acesso a informação foi instituído pela
                        <a href="<?php echo e(asset('http://www.planalto.gov.br/ccivil_03/_ato2011-2014/2011/lei/l12527.htm')); ?>"
                           class="hover:underline text-lightBlue-500 hover:text-lightBlue-600" target="_blank">Lei
                           Federal nº
                           12.527</a>,
                        <a href="<?php echo e(asset('http://adcon.rn.gov.br/ACERVO/gac/DOC/DOC000000000083706.PDF')); ?>"
                           class="hover:underline text-lightBlue-500 hover:text-lightBlue-600" target="_blank">Lei
                           Estadual nº
                           9963</a>
                        e o
                        <a href="<?php echo e(asset('http://adcon.rn.gov.br/ACERVO/gac/DOC/DOC000000000084356.PDF')); ?>"
                           class="hover:underline text-lightBlue-500 hover:text-lightBlue-600" target="_blank">
                           decreto que a regulamenta, nº 25399, de 31 de Julho de 2015.
                        </a>
                        No Estado do Rio Grande do Norte a
                        lei de acesso a informação foi implementada pelo serviço de informações ao cidadão e-Sic no qual
                        é possível solicitar documentos e dados da administração pública do Rio Grande do Norte.
                     </span>
                  </div>
                  <div class="flex flex-row justify-center mb-8">
                     <span class="absolute text-lg bg-white font-semibold px-4 text-gray-700" name="texto"
                        style="font-size: 1.125rem; line-height: 1.75rem;">Endereço</span>
                     <div class="w-full bg-gray-200 mt-3 h-px"></div>
                  </div>
                  <span class="text-sm text-gray-500" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                     Atendimento presencial <span class="font-semibold">suspenso temporariamente</span> de acordo com
                     Portaria N. 042/2020 - GC/CONTROL
                  </span>
                  <div class="flex flex-row justify-center mb-8 mt-6">
                     <span class="absolute text-lg bg-white font-semibold px-4 text-gray-700" name="texto"
                        style="font-size: 1.125rem; line-height: 1.75rem;">Contato</span>
                     <div class="w-full bg-gray-200 mt-3 h-px"></div>
                  </div>
                  <span class="text-sm text-gray-500" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                     Para pedidos de informação por telefone, ligar para a Controladoria Geral do Estado – CONTROL,
                     informando que se trata de um pedido de informação ao SIC,
                     pelo número: <span class="text-emerald-700">(84) 3232-2010</span>
                  </span>
                  <span class="text-sm text-gray-500" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                     <span class="font-semibold">Horário de atendimento:</span> Segunda à Sexta-feira das 08h às 12h
                  </span>
               </div>
            </div>

            <div class="w-full md:w-1/3 xl:w-1/3 p-3">
               <div class="rounded-lg  p-3 bg-white">
                  <div class="text-center m-6">
                     <h1 class="text-xl font-semibold text-gray-700 hover:underline" name="texto"
                        style="font-size: 1.25rem; line-height: 1.75rem;">
                        <a href="<?php echo e(asset('https://sistema.ouvidorias.gov.br/publico/RN/manifestacao/RegistrarManifestacao')); ?>"
                           target="_blank">
                           Ouvidoria Geral do Estado
                        </a>
                     </h1>
                     <div class="flex flex-row justify-center gap-2 my-5">
                        <div class="flex relative place-content-center inline-block">
                           <a href="<?php echo e(asset('https://sistema.ouvidorias.gov.br/publico/RN/manifestacao/RegistrarManifestacao')); ?>"
                              class="text-gray-500 hover:text-black px-2" target="_blank" name="texto"
                              style="font-size: 0.75rem;">
                              <img src="<?php echo e(asset('images/ouvidoria.png')); ?>" alt="">
                           </a>
                        </div>
                     </div>
                     <span class="text-sm text-left text-gray-500 pt-2" name="texto"
                        style="font-size: 0.875rem; line-height: 1.25rem;">
                        <span class="font-semibold">Denúncia:</span> Comunique um ato ilícito praticado contra a
                        administração pública <br>
                        <span class="font-semibold">Elogio:</span> Expresse se você está satisfeito com um atendimento
                        público <br>
                        <span class="font-semibold">Reclamação:</span> Manifeste sua insatisfação com um serviço público
                        <br>
                        <span class="font-semibold">Simplifique:</span> Sugira alguma ideia para desburocratizar o
                        serviço público <br>
                        <span class="font-semibold">Solicitação:</span> Envie uma ideia ou proposta de melhoria dos
                        serviços públicos <br>
                        <span class="font-semibold">Sugestão:</span> Envie uma ideia ou proposta de melhoria dos
                        serviços públicos
                     </span>
                  </div>
               </div>
            </div>
         </div>

      </div>
   </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/portaltransparencianewrn/resources/views/fale-conosco/index.blade.php ENDPATH**/ ?>