<section id="cookiebg">
    <?php echo csrf_field(); ?>
    <div id="cookie" class="w-full w-screen px-12 py-3 lg:px-20 top-0 bg-gray-500 md:flex items-center -mx-3">
        <div class="row md:flex-8 px-3 mb-5 md:mb-0">
            <p class="text-center md:text-left text-white text-xs leading-tight md:pr-12">
                Estamos realizando adequações em razão da LGPD - Lei Geral de Proteção de Dados Pessoais, lei
                13.709/2018. Utilizamos cookies que armazenam informações sobre a forma como você usa esse site.
                <br />
                Clique
                no botão: Eu aceito todos os cookies para consentir com a utilização dos cookies. Gerenciar cookies para
                configurar.
                <br />
                <br />
                Para mais informações sobre como utilizamos os cookies, veja nossa <a style="text-decoration: underline;"
                    href="<?php echo e(route('lgpd')); ?>" target="_blank" rel="noopener noreferrer">política de cookies</a>.
            </p>
        </div>
        <div class="row md:flex-4 sm:flex-4 px-3 flex-sm-column  flex-md-column items-center text-center align-center">
            <div class="col-md-12">
                <button style="min-width: 250px" id="openModal" onclick="toggleModal('cookies');"
                    class="mb-2 py-2 px-4 bg-blue-500 hover:bg-blue-600 text-white rounded font-semibold text-sm shadow-xl">Gerenciar
                    cookies</button>
            </div>
            <div class="col-md-12">
                <button style="min-width: 250px" id="btncookie" onclick="concordo();"
                    class="mb-2 py-2 px-4 bg-green-500 hover:bg-green-600 text-white rounded font-semibold text-sm shadow-xl">Aceitar
                    todos os cookies</button>
            </div>
            <div class="col-md-12">
                <button style="min-width: 250px" id="btncookie" onclick="rejeitar();"
                    class="py-2 px-4 bg-red-500 hover:bg-red-600 text-white rounded font-semibold text-sm shadow-xl">Rejeitar
                    todos os cookies</button>
            </div>
        </div>
    </div>

    <!-- Preparando modal -->
    <div id="modalcookies"
        class="modal opacity-0 pointer-events-none fixed w-full h-full top-0 left-0 flex items-center justify-center">
        <div class="modal-overlay absolute w-full h-full bg-gray-900 opacity-50"></div>

        <div class="modal-container bg-white w-11/12 md:max-w-2xl mx-auto rounded shadow-lg z-50 overflow-y-auto">
            <div class="modal-close absolute top-0 right-0 cursor-pointer flex flex-col items-center mt-4 mr-4 text-white text-sm z-50"
                name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                <svg onclick="toggleModal('cookies')" class="fill-current text-white" xmlns="http://www.w3.org/2000/svg"
                    width="18" height="18" viewBox="0 0 18 18">
                    <path
                        d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z">
                    </path>
                </svg>
                <span class="text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">(Esc)</span>
            </div>

            <!-- Add margin if you want to see some of the overlay behind the modal-->
            <div class="modal-content text-left">
                <!--Body-->
                <div id="cookies">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="flex justify-between p-3">
                                <div>
                                    <h4 class="modal-title" id="myModalLabel"><strong>Configurações avançadas de
                                            cookies</strong>
                                        <br />
                                        <p style="font-size: 0.66rem">Para mais informações sobre como utilizamos os
                                            cookies, veja nossa
                                            <a style="text-decoration: underline;" href="<?php echo e(route('lgpd')); ?>"
                                                target="_blank" rel="noopener noreferrer">política de cookies</a>.
                                        </p>
                                    </h4>
                                </div>
                                <!--botão X de fechar-->
                                <div class="flex justify-end items-center">
                                    <div class="modal-close cursor-pointer z-50">
                                        <svg onclick="toggleModal('cookies')" class="fill-current text-red-500"
                                            xmlns="http://www.w3.org/2000/svg" width="22" height="22"
                                            font-weight="bold" viewBox="0 0 18 18">
                                            <path
                                                d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z">
                                            </path>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-body p-4 border-t border-b border-gray-300">
                                <div class="flex items-center gap-2">
                                    <label for="cookie-obrigatorio" class="switch">
                                        <input id="cookie-obrigatorio" type="checkbox" role="switch" checked disabled>
                                        <span class="slider round"></span>
                                    </label>
                                    <p>Cookies obrigatórios para o funcionamento do site.</p>
                                </div>
                                <div class="flex items-center gap-2">
                                    <label for="cookie-personalizacao" class="switch">
                                        <input id="cookie-personalizacao" type="checkbox" role="switch" checked>
                                        <span class="slider round"></span>
                                    </label>
                                    <p>Cookies de personalização.</p>
                                </div>
                                <div class="flex items-center gap-2">
                                    <label for="cookie-terceiros" class="switch">
                                        <input id="cookie-terceiros" type="checkbox" role="switch" checked>
                                        <span class="slider round"></span>
                                    </label>
                                    <p>Cookies de terceiros.</p>
                                </div>
                            </div>
                            <div class="flex justify-center gap-3 py-3">
                                <div class="col-md-6">
                                    <button id="btncookie" style="min-width: 150px" onclick="toggleModal('cookies');"
                                        class="py-2 px-8 text-sm shadow-xl border rounded border-red-600 text-red-600 hover:bg-red-600 hover:text-white">Cancelar</button>
                                </div>
                                <div class="col-md-6">
                                    <button id="btncookie" onclick="salvarCookies();" style="min-width: 150px"
                                        class="py-2 px-8 text-sm shadow-xl border rounded border-green-600 text-green-600 hover:bg-green-600 hover:text-white">Salvar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 40px;
            height: 20px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .2s;
            transition: .2s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 16px;
            width: 16px;
            left: 4px;
            bottom: 2px;
            background-color: white;
            -webkit-transition: .2s;
            transition: .2s;
        }

        input:checked+.slider {
            background-color: #2196F3;
        }

        input:disabled+.slider {
            opacity: 50%;
        }

        input:focus+.slider {
            box-shadow: 0 0 0 0.25rem rgba(13,110,253,.25);
        }

        input:checked+.slider:before {
            -webkit-transform: translateX(16px);
            -ms-transform: translateX(16px);
            transform: translateX(16px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 20px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>

    <style>
        .modal {
            transition: opacity 0.25s ease;
            z-index: 200;
        }

        body.modal-active {
            overflow-x: hidden;
            overflow-y: visible !important;
        }
    </style>

    <!-- Script do concordo e rejeitar-->
    <script>
        function salvarCookies() {
            concordo();
            $("#cookie").show();
        }

        var element = document.getElementById("cookies");

        function concordo() {
            element.classList.add("hidden");
            localStorage.setItem("cookie", false);

            $.get("<?php echo e(route('listCookies')); ?>", function(data) {
                console.log(data);
            });
        }

        function rejeitar() {
            //localStorage.setItem("cookie", false);
            const _token = document.querySelector('input[name="_token"]').value;

            $.ajax({
                url: '<?php echo e(route('rejeitarCookies')); ?>',
                method: 'DELETE',
                data: {
                    _token
                },
                success: function(result) {
                    console.log(result);
                },
                error: function(request, msg, error) {
                    // handle failure
                }
            });
        }
    </script>
    <script>
        function toggleModal(id) {
            const body = document.querySelector('body')
            const modal = document.querySelector('#modal' + id)
            modal.classList.toggle('opacity-0')
            modal.classList.toggle('pointer-events-none')
            body.classList.toggle('modal-active')

            document.onkeydown = function(evt) {
                evt = evt || window.event
                var isEscape = false

                if ("key" in evt) {
                    isEscape = (evt.key === "Escape" || evt.key === "Esc")
                } else {
                    isEscape = (evt.keyCode === 27)
                }

                if (isEscape && document.body.classList.contains('modal-active')) {
                    toggleModal(id)
                }
            };
        }
    </script>
</section>
<?php /**PATH /var/www/html/portaltransparencianewrn/resources/views/layouts/cookie.blade.php ENDPATH**/ ?>