<?php $__env->startSection('content'); ?>
    <div class="flex-1 text-right md:text-left">
        <h1 class="text-lg text-gray-900 tracking-widest font-medium title-font uppercase text-center pb-2" name="texto"
            style="font-size: 1.125rem; line-height: 2rem;">
            <i class="fa-sharp fa-solid fa-magnifying-glass"></i>
            Resultados de Pesquisa:
        </h1>

        <ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto"
            style="font-size: 0.875rem; line-height: 1.25rem;">
            <li class="inline-flex items-center">
                <a href="<?php echo e(url('/')); ?>">Início</a>
                <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                    <path fill-rule="evenodd"
                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                        clip-rule="evenodd"></path>
                </svg>
            </li>
            <li class="inline-flex items-center">
                <a href="#" class="text-teal-400">
                    Pesquisa
                </a>
            </li>
        </ul>
        <hr class="border-b-2 border-lightBlue-300 mb-6 mx-4">
    </div>
    <h1 class="text-lg text-gray-900 tracking-widest font-medium title-font uppercase text-left pb-2" name="texto"
        style="font-size: 1.125rem; line-height: 2rem;">

        Links encontrados dentro do portal:
    </h1>

    <?php if(!$pages->isEmpty()): ?>
        <!-- tinha um [0] mas retirei -->
        <?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $page): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div>
                <li class="w-full flex justify-between border-b-2 border-neutral-100 border-opacity-100 py-4 text-lightBlue-700 dark:border-opacity-50 ">
                    <a href="<?php echo e($page->url); ?>" style="font-weight:bold "><?php echo e($page->title); ?></a>
                    <p class="text-sm"><?php echo e($page->path); ?></p>
                </li>

            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php else: ?>
        <p class="text-center py-20">-Resultado não encontrado-</p>
    <?php endif; ?>
    <br>
    <h1 class="text-lg text-gray-900 tracking-widest font-medium title-font uppercase text-left pb-2" name="texto"
        style="font-size: 1.125rem; line-height: 2rem;">

        Ocorrências no glossário:
    </h1>
    <?php if(!$glossario->isEmpty()): ?>
        <!-- tinha um [0] mas retirei -->
        <?php $__currentLoopData = $glossario; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="w-full md:w-1/1 xl:w-1/1 p-3">
                <div class="p-2 bg-white shadow-md hover:shodow-lg rounded-2xl">
                    <div class="font-medium text-left text-lightBlue-700 uppercase pb-2" name="texto"
                        style="font-size: 1rem; line-height: 1.5rem;">
                        <?php echo e($item->nome); ?>:
                    </div>
                    <p class="text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        <?php echo e($item->descricao); ?> <br>
                    </p>
                </div>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php else: ?>
        <p class="text-center py-20">-Resultado não encontrado-</p>
    <?php endif; ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/portaltransparencianewrn/resources/views/search/results.blade.php ENDPATH**/ ?>