<?php $__env->startSection('title'); ?>
- Bloqueios Judiciais
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<link rel="stylesheet" href="<?php echo e(asset('css/exportfiles.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<h1 class="text-lg text-gray-900 tracking-widest font-medium title-font pb-2 text-center uppercase" name="texto"
   style="font-size: 1.125rem; line-height: 2rem;">
   <i class="fas fa-search-dollar"></i>
   Bloqueios Judiciais
</h1>
<ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto"
   style="font-size: 0.875rem; line-height: 1.25rem;">
   <li class="inline-flex items-center">
      <a href="<?php echo e(url('/')); ?>">Inicio</a>
      <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
         <path fill-rule="evenodd"
            d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
            clip-rule="evenodd"></path>
      </svg>
   </li>
   <li class="inline-flex items-center">
      <a href="<?php echo e(route('despesas')); ?>">Despesas</a>
      <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
         <path fill-rule="evenodd"
            d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
            clip-rule="evenodd"></path>
      </svg>
   </li>
   <li class="inline-flex items-center">
      <a href="#" class="text-teal-400">Bloqueios Judiciais</a>
   </li>
</ul>

<hr class="border-b-2 border-lightBlue-300 mb-12 mx-4">

<div>
   <form action="<?php echo e(route('bloqueios-judiciais')); ?>" method="POST" class="mt-6">
      <?php echo csrf_field(); ?>
      <div class="flex justify-between gap-3">
         <span class="w-1/2">
            <label for="mes" class="block text-xs font-semibold text-gray-600 uppercase" name="texto"
               style="font-size: 0.75rem; line-height: 1rem;">Mês de
               Referência</label>
            <select id="mes" name="mes"
               class="block w-full p-3 mt-2 text-gray-700 bg-white border border-gray-300 focus:outline-none focus:bg-gray-100 focus:shadow-inner">
               <?php $__currentLoopData = $meses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

               <?php if($mes == $mes_selected): ?>
               <option value="<?php echo e($mes_selected); ?>" selected>
                  <?php echo app('translator')->get('meses.'.$mes); ?></option>
               <?php else: ?>
               <option value="<?php echo e($mes); ?>"><?php echo app('translator')->get('meses.'.$mes); ?></option>
               <?php endif; ?>

               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </select>
         </span>
         <span class="w-1/2">
            <label for="ano" class="block text-xs font-semibold text-gray-600 uppercase" name="texto"
               style="font-size: 0.75rem; line-height: 1rem;">Ano de
               Referência</label>
            <select id="ano" name="ano"
               class="block w-full p-3 mt-2 text-gray-700 bg-white border border-gray-300 focus:outline-none focus:bg-gray-100 focus:shadow-inner">
               <?php $__currentLoopData = $anos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ano): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

               <?php if($ano == $ano_selected): ?>
               <option value="<?php echo e($ano_selected); ?>" selected><?php echo e($ano); ?></option>
               <?php else: ?>
               <option value="<?php echo e($ano); ?>"><?php echo e($ano); ?></option>
               <?php endif; ?>

               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </select>
         </span>
      </div>
      <div class="p-2 w-full">
         <input type="submit"
            class="cursor-pointer flex mx-auto text-white bg-lightBlue-500 border-0 py-2 px-8 focus:outline-none hover:bg-lightBlue-600 rounded text-lg"
            value="Consultar" />
      </div>
   </form>
</div>

<div>
   <h2 class="text-2xl font-semibold leading-tight my-4" name="texto" style="font-size: 1.5rem; line-height: 2rem;">
      Gráfico do total de Bloqueios Judiciais até
      <?php echo app('translator')->get('meses.'.$mes_selected); ?> do ano <?php echo e($ano_selected); ?></h2>
   <h4 class="font-semibold leading-tight my-4" name="texto" style="font-size: 1rem; line-height: 1.5rem;">Saque
      acumulado bloqueio: R$
      <?php echo e(number_format($dados->sum('valor'), 2, ',', '.')); ?> </h4>
</div>

<div class="w-full max-w-xl mx-auto">
   <div class="px-7 bg-white shadow-lg rounded-2xl mb-5">
      <div class="flex">
         <div class="flex-auto hover:w-full group">
            <form target="_blank" action="<?php echo e(route('export-xls-bloqueios-judiciais', [
               'mes' => $mes_selected,
               'ano' => $ano_selected
            ])); ?>" method="POST" enctype="multipart/form-data">
               <?php echo csrf_field(); ?>
               <button type="submit"
                  class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-green-500">
                  <span
                     class="block px-1 py-1 border border-transparent group-hover:border-green-500 rounded-full group-hover:flex-grow">
                     <i class="far fa-file-excel text-2xl pt-1"></i><span
                        class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Baixar arquivo XLS</span>
                  </span>
               </button>
            </form>
         </div>
         <div class="flex-auto hover:w-full group">
            <form target="_blank" action="<?php echo e(route('export-pdf-bloqueios-judiciais', [
               'mes' => $mes_selected,
               'ano' => $ano_selected
            ])); ?>" method="POST" enctype="multipart/form-data">
               <?php echo csrf_field(); ?>
               <button type="submit"
                  class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-red-500">
                  <span
                     class="block px-1 py-1 border border-transparent group-hover:border-red-500 rounded-full group-hover:flex-grow">
                     <i class="far fa-file-pdf text-2xl pt-1"></i><span
                        class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Baixar arquivo PDF</span>
                  </span>
               </button>
            </form>
         </div>
         <div class="flex-auto hover:w-full group">
            <a href="#" onclick="window.print()"
               class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-blueGray-500">
               <span
                  class="block px-1 py-1 border border-transparent group-hover:border-blueGray-500 rounded-full group-hover:flex-grow">
                  <i class="fas fa-print text-2xl pt-1"></i><span
                     class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Imprimir arquivo</span>
               </span>
            </a>
         </div>
         <div class="flex-auto hover:w-full group">
            <a href="<?php echo e(asset('docs/dicionario-de-dados/Dicionario de Dados Abertos-RN - Despesa - Bloqueios Judiciais.xlsx')); ?>"
               target="_blank"
               class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-indigo-500">
               <span
                  class="block px-1 py-1 border border-transparent group-hover:border-indigo-500 rounded-full group-hover:flex-grow">
                  <i class="far fa-file-alt text-2xl pt-1"></i><span
                     class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Dicionario de dados</span>
               </span>
            </a>
         </div>
         <div class="flex-auto hover:w-full group">
            <a href="https://creativecommons.org/licenses/by/4.0/?ref=chooser-v1" target="_blank"
               class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-orange-500">
               <span
                  class="block px-1 py-1 border border-transparent group-hover:border-orange-500 rounded-full group-hover:flex-grow">
                  <i class="far fa-closed-captioning text-2xl pt-1"></i>
                  <span class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Licença</span>
               </span>
            </a>
         </div>
      </div>
   </div>
</div>

<div class="flex flex-col pt-8">
   <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
      <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
         <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
            <table class="min-w-full divide-y divide-gray-200">
               <thead class="bg-lightBlue-50 text-center">
                  <tr>
                     <th scope="col" class="px-6 py-4 text-xs font-semibold text-gray-600 uppercase tracking-wider"
                        name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                        Mês
                     </th>
                     <th scope="col" class="px-6 py-4 text-xs font-semibold text-gray-600 uppercase tracking-wider"
                        name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                        Saldo Mensal Bloqueio
                     </th>
                  </tr>
               </thead>
               <tbody class="bg-white divide-y divide-gray-200 text-sm text-center" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">
                  <?php $__currentLoopData = $dados; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dado): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <tr>
                     <td class="px-5 py-5 border-b border-gray-200 bg-white">
                        <?php echo app('translator')->get('meses.'.$dado->mes); ?>
                     </td>
                     <td class="px-5 py-5 border-b border-gray-200 bg-white">
                        <span
                           class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800"
                           name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                           R$ <?php echo e(number_format($dado->valor, 2, ',', '.')); ?>

                        </span>
                     </td>
                  </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
               </tbody>
            </table>
            <?php if($dados->isEmpty()): ?>
            <p class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" name="texto"
               style="font-size: 0.75rem; line-height: 1rem;">Não há dados
               cadastrados para essa busca...</p>
            <?php endif; ?>
         </div>
      </div>
   </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/portaltransparencianewrn/resources/views/despesas/bloqueios-judiciais/index.blade.php ENDPATH**/ ?>