<div id="altu"
    class="flex justify-center fixed z-20 w-full text-gray-700 bg-white dark-mode:text-gray-200 dark-mode:bg-gray-800 shadow">
    <div id='content' class="">
        <div x-data="{ open: false }"
            class="flex flex-col mx-auto xl:items-center justify-center xl:justify-between xl:flex-row xl:px-6 2xl:px-2">

            <div class="p-4 flex flex-row items-center justify-between">
                <a aria-label="Página Inicial" href="<?php echo e(url('/')); ?>">
                    <img id="logoimg" class="w-2/3 md:w-full" name="transparencia"
                        src="<?php echo e(asset('images/logo/logo-portal-transparencia-60x60.png')); ?>" alt="">

                    <span id="logotext" name="texto"
                        class="hidden font-semibold tracking-widest text-gray-900 uppercase rounded-lg dark-mode:text-white focus:outline-none focus:shadow-outline"
                        style="font-size: 1.125rem;">
                        Portal da Transparência
                    </span>
                </a>

                <button class="xl:hidden rounded-lg focus:outline-none focus:shadow-outline" aria-label="Navbar"
                    @click="open = !open">
                    <svg fill="currentColor" viewBox="0 0 20 20" class="w-6 h-6">
                        <path x-show="!open" fill-rule="evenodd"
                            d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                            clip-rule="evenodd"></path>
                        <path x-show="open" fill-rule="evenodd"
                            d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                            clip-rule="evenodd"></path>
                    </svg>
                </button>
            </div>

            <div id="navbar" class="flex-column">
                <div id="ouvi" class="flex justify-end py-2">

                    <div class="flex relative place-content-center inline-block">
                        <div class="links hidden xl:flex items-center">
                            <a aria-label="Ouvidoria" href="<?php echo e(route('ouvidoria')); ?>"
                                class="text-gray-500 hover:text-black px-2" name="texto" style="font-size: 0.75rem;">
                                <img src="<?php echo e(asset('images/ouvidoria.png')); ?>" alt="">
                            </a>
                            <a aria-label="Acesso à Informação" href="<?php echo e(asset('http://www.sic.rn.gov.br/')); ?>"
                                class="text-gray-500 hover:text-black px-2" target="_blank" name="texto"
                                style="font-size: 0.75rem;">
                                <img src="<?php echo e(asset('images/acesso_a_informacao_logo.png')); ?>" alt="">
                            </a>

                        </div>
                    </div>
                </div>

                <nav :class="{ 'flex': open, 'hidden': !open }"
                    class="flex-col flex-grow xl:items-center pb-4 pl-6 xl:pb-0 hidden xl:flex xl:flex-row">

                    
                    <div @click.away="open = false" class="relative" x-data="{ open: false }">
                        <button @click="open = !open"
                            class="flex items-center px-2 py-2 mt-2 rounded-lg font-semibold md:mt-0 focus:shadow-outline transition duration-150 ease-in-out transform hover:-translate-y-1 hover:scale-110 hover:text-yellow-500"
                            name="texto" style="font-size: 0.875rem;">
                            <i class="fa fa-funnel-dollar fa-fw mr-1 text-yellow-500"></i>
                            Pessoal

                            <svg fill="currentColor" viewBox="0 0 20 20"
                                :class="{ 'rotate-180': open, 'rotate-0': !open }"
                                class="inline w-4 h-4 mt-1 transition-transform duration-200 transform md:mt-0">
                                <path fill-rule="evenodd"
                                    d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                    clip-rule="evenodd">
                                </path>
                            </svg>

                        </button>
                        <div x-show="open" x-transition:enter="transition ease-out duration-300"
                            x-transition:enter-start="transform opacity-0 scale-95"
                            x-transition:enter-end="transform opacity-100 scale-100"
                            x-transition:leave="transition ease-in duration-75"
                            x-transition:leave-start="transform opacity-100 scale-100"
                            x-transition:leave-end="transform opacity-0 scale-95"
                            class="absolute z-10 w-full mt-2 origin-top rounded-lg shadow-lg md:w-60"
                            style="display:none; box-shadow: 2px 2px white;">
                            <div class="py-2 bg-white rounded-md shadow">
                                <a class="block px-4 py-2 bg-transparent hover:text-yellow-900 hover:bg-yellow-100 focus:shadow-outline"
                                    name="texto" style="font-size: 0.875rem;"
                                    href="<?php echo e(url('http://servicos.searh.rn.gov.br/searh/Remuneracao')); ?>"
                                    target="_blank">Remuneração
                                </a>
                                <a class="block px-4 py-2 bg-transparent hover:text-yellow-900 hover:bg-yellow-100 focus:shadow-outline"
                                    name="texto" style="font-size: 0.875rem;"
                                    href="<?php echo e(url('http://servicos.searh.rn.gov.br/searh/Diaria')); ?>"
                                    target="_blank">Diárias
                                </a>
                                <a class="block px-4 py-2 bg-transparent hover:text-yellow-900 hover:bg-yellow-100 focus:shadow-outline"
                                    name="texto" style="font-size: 0.875rem;"
                                    href="<?php echo e(route('cadastro-autoridades')); ?>"
                                    target="_blank">
                                    Cadastro de autoridades
                                </a>
                                <a class="block px-4 py-2 bg-transparent hover:text-yellow-900 hover:bg-yellow-100 focus:shadow-outline"
                                    name="texto" style="font-size: 0.875rem;"
                                    href="<?php echo e(url('http://servicos.searh.rn.gov.br/searh/estagiario')); ?>"
                                    target="_blank">
                                    Relação de Estagiários
                                </a>
                                <a class="block px-4 py-2 bg-transparent hover:text-yellow-900 hover:bg-yellow-100 focus:shadow-outline"
                                    name="texto" style="font-size: 0.875rem;"
                                    href="<?php echo e(route('terceirizados')); ?>"
                                    target="_blank">
                                    Terceirizados
                                </a>
                                <a class="block px-4 py-2 bg-transparent hover:text-yellow-900 hover:bg-yellow-100 focus:shadow-outline"
                                    name="texto" style="font-size: 0.875rem;"
                                    href="<?php echo e(url('https://cloud.diariooficial.rn.gov.br/s/3ZM7wp9Cd2dBQRR?path=%2F')); ?>"
                                    target="_blank">
                                    Concursos e Processos
                                </a>
                            </div>
                        </div>
                    </div>

                    
                    <div @click.away="open = false" class="relative" x-data="{ open: false }">

                        <button @click="open = !open"
                            class="flex items-center px-2 py-2 mt-2 rounded-lg font-semibold md:mt-0 focus:shadow-outline transition duration-150 ease-in-out transform hover:-translate-y-1 hover:scale-110 hover:text-indigo-400"
                            name="texto" style="font-size: 0.875rem;">
                            <i class="fas fa-file-invoice-dollar fa-fw mr-1 text-indigo-400"></i>
                            Contas Públicas

                            <svg fill="currentColor" viewBox="0 0 20 20"
                                :class="{ 'rotate-180': open, 'rotate-0': !open }"
                                class="inline w-4 h-4 mt-1 transition-transform duration-200 transform md:-mt-1">
                                <path fill-rule="evenodd"
                                    d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                    clip-rule="evenodd">
                                </path>
                            </svg>

                        </button>
                        <div x-show="open" x-transition:enter="transition ease-out duration-300"
                            x-transition:enter-start="transform opacity-0 scale-95"
                            x-transition:enter-end="transform opacity-100 scale-100"
                            x-transition:leave="transition ease-in duration-75"
                            x-transition:leave-start="transform opacity-100 scale-100"
                            x-transition:leave-end="transform opacity-0 scale-95"
                            class="absolute z-10 w-full mt-2 origin-top rounded-md shadow-lg md:w-80"
                            style="display:none; box-shadow: 2px 2px white;">
                            <div class="py-2 bg-white rounded-md shadow">
                                <a class="block px-4 py-2 bg-transparent hover:text-indigo-900 hover:bg-indigo-100 focus:shadow-outline"
                                    name="texto" style="font-size: 0.875rem;"
                                    href="<?php echo e(url('https://www.seplan.rn.gov.br/documentos/Plano%20Plurianual/')); ?>"
                                    target="_blank">
                                    Plano Plurianual - PPA
                                </a>
                                <a class="block px-4 py-2 bg-transparent hover:text-indigo-900 hover:bg-indigo-100 focus:shadow-outline"
                                    name="texto" style="font-size: 0.875rem;"
                                    href="<?php echo e(url('https://www.seplan.rn.gov.br/documentos/Lei%20de%20Diretrizes%20Or%c3%a7ament%c3%a1rias/')); ?>"
                                    target="_blank">
                                    Lei de Diretrizes Orçamentárias - LDO
                                </a>
                                <a class="block px-4 py-2 bg-transparent hover:text-indigo-900 hover:bg-indigo-100 focus:shadow-outline"
                                    name="texto" style="font-size: 0.875rem;"
                                    href="<?php echo e(url('https://www.seplan.rn.gov.br/documentos/Lei%20Or%c3%a7ament%c3%a1ria%20Anual/')); ?>"
                                    target="_blank">
                                    Orçamentos
                                </a>
                                <a class="block px-4 py-2 bg-transparent hover:text-indigo-900 hover:bg-indigo-100 focus:shadow-outline"
                                    name="texto" style="font-size: 0.875rem;"
                                    href="<?php echo e(url('http://control.rn.gov.br/Conteudo.asp?TRAN=CATALG&TARG=178&ACT=&PAGE=0&PARM=&LBL=Demonstrativos+Fiscais')); ?>"
                                    target="_blank">
                                    Demonstrativo Fiscal
                                </a>
                                <a class="block px-4 py-2 bg-transparent hover:text-indigo-900 hover:bg-indigo-100 focus:shadow-outline"
                                    name="texto" style="font-size: 0.875rem;"
                                    href="<?php echo e(url('http://www.control.rn.gov.br/Conteudo.asp?TRAN=PASTAC&TARG=896&ACT=&PAGE=&PARM=&LBL=Balan%E7os')); ?>"
                                    target="_blank">
                                    Balanço
                                </a>

                                <div @click.away="open = false" class="relative" x-data="{ open: false }">
                                    <div class="flex items-center">
                                        <button @click="open = !open"
                                            class="block px-4 py-2 w-full flex items-start bg-transparent hover:bg-indigo-100 hover:text-indigo-900 focus:shadow-outline"
                                            name="texto" style="font-size: 0.875rem;">
                                            Normatização
                                            <svg fill="currentColor" viewBox="0 0 20 20"
                                                :class="{ 'rotate-180': open, 'rotate-0': !open }"
                                                class="inline w-4 h-4 mt-1 transition-transform duration-200 transform md:mt-0">
                                                <path fill-rule="evenodd"
                                                    d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                                    clip-rule="evenodd">
                                                </path>
                                            </svg>
                                        </button>
                                    </div>
                                    <div x-show="open" x-transition:enter="transition ease-out duration-300"
                                        x-transition:enter-start="transform opacity-0 scale-95"
                                        x-transition:enter-end="transform opacity-100 scale-100"
                                        x-transition:leave="transition ease-in duration-75"
                                        x-transition:leave-start="transform opacity-100 scale-100"
                                        x-transition:leave-end="transform opacity-0 scale-95"
                                        class="absolute w-full mt-2 origin-top rounded-md shadow-lg md:w-auto z-10"
                                        style="display:none; box-shadow: 2px 2px white;">
                                        <div class="py-2 bg-white rounded-md shadow">
                                            <a class="block px-4 py-2 bg-transparent hover:text-indigo-900 hover:bg-indigo-100 focus:shadow-outline"
                                                name="texto" style="font-size: 0.875rem;"
                                                href="<?php echo e(route('notas-tecnicas')); ?>">
                                                Notas Técnicas
                                            </a>
                                            <a class="block px-4 py-2 bg-transparent hover:text-indigo-900 hover:bg-indigo-100 focus:shadow-outline"
                                                name="texto" style="font-size: 0.875rem;"
                                                href="<?php echo e(route('orientacoes-tecnicas')); ?>">
                                                Orientações Técnicas
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <a class="block px-4 py-2 bg-transparent hover:text-indigo-900 hover:bg-indigo-100 focus:shadow-outline"
                                    name="texto" style="font-size: 0.875rem;"
                                    href="<?php echo e(route('contas-governo')); ?>"
                                    target="_blank">
                                    Apreciação das Contas do Governo
                                </a>

                            </div>
                        </div>
                    </div>

                    
                    <div @click.away="open = false" class="relative" x-data="{ open: false }">
                        <button @click="open = !open"
                            class="flex items-center px-2 py-2 mt-2 rounded-lg font-semibold md:mt-0 focus:shadow-outline transition duration-150 ease-in-out transform hover:-translate-y-1 hover:scale-110 hover:text-blue-300"
                            name="texto" style="font-size: 0.875rem;">
                            <img class="mr-1" src="<?php echo e(asset('images/icons/contabilidadefacil-icon.png')); ?>"
                                alt="">
                            Contabilidade Fácil

                            <svg fill="currentColor" viewBox="0 0 20 20"
                                :class="{ 'rotate-180': open, 'rotate-0': !open }"
                                class="inline w-4 h-4 mt-1 transition-transform duration-200 transform md:-mt-1">
                                <path fill-rule="evenodd"
                                    d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                    clip-rule="evenodd">
                                </path>
                            </svg>

                        </button>
                        <div x-show="open" x-transition:enter="transition ease-out duration-300"
                            x-transition:enter-start="transform opacity-0 scale-95"
                            x-transition:enter-end="transform opacity-100 scale-100"
                            x-transition:leave="transition ease-in duration-75"
                            x-transition:leave-start="transform opacity-100 scale-100"
                            x-transition:leave-end="transform opacity-0 scale-95"
                            class="absolute z-10 w-full mt-2 origin-top rounded-md shadow-lg md:w-72"
                            style="display:none; box-shadow: 2px 2px white;">
                            <div class="py-2 bg-white rounded-md shadow">
                                <a class="block px-4 py-2 text-sm bg-transparent hover:text-blue-900 hover:bg-blue-100 focus:shadow-outline"
                                    name="texto" style="font-size: 0.875rem;"
                                    href="<?php echo e(route('oque-e-contabilidade-facil')); ?>">O
                                    que
                                    é Contabilidade Fácil
                                </a>

                                <div @click.away="open = false" class="relative" x-data="{ open: false }">
                                    <div class="flex items-center">
                                        <button @click="open = !open"
                                            class="block px-4 py-2 w-full flex items-start bg-transparent hover:bg-blue-100 hover:text-blue-900 focus:shadow-outline"
                                            name="texto" style="font-size: 0.875rem;">
                                            RN Contábil

                                            <svg fill="currentColor" viewBox="0 0 20 20"
                                                :class="{ 'rotate-180': open, 'rotate-0': !open }"
                                                class="inline w-4 h-4 mt-1 transition-transform duration-200 transform md:mt-0">
                                                <path fill-rule="evenodd"
                                                    d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                                    clip-rule="evenodd">
                                                </path>
                                            </svg>

                                        </button>
                                    </div>
                                    <div x-show="open" x-transition:enter="transition ease-out duration-300"
                                        x-transition:enter-start="transform opacity-0 scale-95"
                                        x-transition:enter-end="transform opacity-100 scale-100"
                                        x-transition:leave="transition ease-in duration-75"
                                        x-transition:leave-start="transform opacity-100 scale-100"
                                        x-transition:leave-end="transform opacity-0 scale-95"
                                        class="absolute w-full z-10 mt-2 origin-top rounded-md shadow-lg md:w-auto"
                                        style="display:none; box-shadow: 2px 2px white;">
                                        <div class="py-2 bg-white rounded-md shadow">
                                            <a class="block px-4 py-2 bg-transparent hover:text-blue-900 hover:bg-blue-100 focus:shadow-outline"
                                                name="texto" style="font-size: 0.875rem;"
                                                href="<?php echo e(route('contas-bimestrais')); ?>">Contas Bimestrais
                                            </a>
                                            <a class="block px-4 py-2 bg-transparent hover:text-blue-900 hover:bg-blue-100 focus:shadow-outline"
                                                name="texto" style="font-size: 0.875rem;"
                                                href="<?php echo e(route('contas-quadrimestrais')); ?>">Contas Quadrimestrais
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <a class="block px-4 py-2 bg-transparent hover:text-blue-900 hover:bg-blue-100 focus:shadow-outline"
                                    name="texto" style="font-size: 0.875rem;" href="<?php echo e(route('rn-em-foco')); ?>">
                                    RN em Foco
                                </a>

                                <div @click.away="open = false" class="relative" x-data="{ open: false }">
                                    <div class="flex items-center">
                                        <button @click="open = !open"
                                            class="block px-4 py-2 w-full flex rounded-lg items-start bg-transparent hover:bg-blue-100 hover:text-blue-900 focus:shadow-outline"
                                            name="texto" style="font-size: 0.875rem;">
                                            RN + Fácil

                                            <svg fill="currentColor" viewBox="0 0 20 20"
                                                :class="{ 'rotate-180': open, 'rotate-0': !open }"
                                                class="inline w-4 h-4 mt-1 transition-transform duration-200 transform md:mt-0">
                                                <path fill-rule="evenodd"
                                                    d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                                    clip-rule="evenodd">
                                                </path>
                                            </svg>

                                        </button>
                                    </div>

                                    <div x-show="open" x-transition:enter="transition ease-out duration-300"
                                        x-transition:enter-start="transform opacity-0 scale-95"
                                        x-transition:enter-end="transform opacity-100 scale-100"
                                        x-transition:leave="transition ease-in duration-75"
                                        x-transition:leave-start="transform opacity-100 scale-100"
                                        x-transition:leave-end="transform opacity-0 scale-95"
                                        class="absolute w-full z-10 mt-2 origin-top rounded-md shadow-lg md:w-auto"
                                        style="display:none; box-shadow: 2px 2px white;">
                                        <div class="py-2 bg-white rounded-md shadow">
                                            <a class="block px-4 py-2 bg-transparent hover:text-blue-900 hover:bg-blue-100 focus:shadow-outline"
                                                name="texto" style="font-size: 0.875rem;"
                                                href="<?php echo e(asset('docs/contabilidadefacil/rnmaisfacil/exercicio2022.pdf')); ?>"
                                                target="_blank">
                                                Exercício 2022
                                            </a>
                                            <a class="block px-4 py-2 bg-transparent hover:text-blue-900 hover:bg-blue-100 focus:shadow-outline"
                                                name="texto" style="font-size: 0.875rem;"
                                                href="<?php echo e(asset('docs/contabilidadefacil/rnmaisfacil/exercicio2021.pdf')); ?>"
                                                target="_blank">
                                                Exercício 2021
                                            </a>
                                            <a class="block px-4 py-2 bg-transparent hover:text-blue-900 hover:bg-blue-100 focus:shadow-outline"
                                                name="texto" style="font-size: 0.875rem;"
                                                href="<?php echo e(asset('docs/contabilidadefacil/rnmaisfacil/exercicio2020.pdf')); ?>"
                                                target="_blank">
                                                Exercício 2020
                                            </a>
                                            <a class="block px-4 py-2 bg-transparent hover:text-blue-900 hover:bg-blue-100 focus:shadow-outline"
                                                name="texto" style="font-size: 0.875rem;"
                                                href="<?php echo e(asset('docs/contabilidadefacil/rnmaisfacil/exercicio2019.pdf')); ?>"
                                                target="_blank">
                                                Exercício 2019
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a class="block px-4 py-2 bg-transparent hover:text-blue-900 hover:bg-blue-100 focus:shadow-outline"
                                    name="texto" style="font-size: 0.875rem;"
                                    href="<?php echo e(route('dicas-execucao-orcamentaria')); ?>">
                                    Dicas de Execução Orçamentária
                                </a>
                            </div>
                        </div>
                    </div>

                    
                    <a class="flex items-center px-2 py-2 mt-2 font-semibold md:mt-0 rounded-lg focus:shadow-outline transition duration-150 ease-in-out transform hover:-translate-y-1 hover:scale-110 hover:text-teal-500"
                        name="texto" style="font-size: 0.875rem;"
                        href="<?php echo e(route('rn-mais-vacina', ['status' => 'transparencia'])); ?>">
                        <img class="mr-1" src="<?php echo e(asset('images/icons/rn_mais_vacina-icon.png')); ?>"
                            alt="">
                        RN + Vacina
                    </a>

                    
                    

                    
                    <a class="flex items-center px-2 py-2 mt-2 font-semibold md:mt-0 rounded-lg focus:shadow-outline transition duration-150 ease-in-out transform hover:-translate-y-1 hover:scale-110 hover:text-lightBlue-600"
                        name="texto" style="font-size: 0.875rem;" href="<?php echo e(url('/control')); ?>">
                        <img class="mr-1" src="<?php echo e(asset('images/icons/logo-rn-redi.png')); ?>" alt="">
                        CONTROL
                    </a>

                    <form method="GET" action="<?php echo e(route('search')); ?>">
                        <div class="relative flex w-full items-stretch">
                            <input type="text" name="q" id="q"
                                class="relative m-0 -mr-0.5 block w-[1px] min-w-0 flex-auto rounded-l border border-solid border-neutral-300 bg-clip-padding px-3 py-[0.25rem] text-base font-normal leading-[1.6] text-neutral-700 outline-none transition duration-200 ease-in-out focus:z-[3] focus:border-primary focus:text-neutral-700 focus:shadow-[inset_0_0_0_1px_rgb(59,113,202)] focus:outline-none dark:border-neutral-600 dark:text-neutral-200 dark:placeholder:text-neutral-200 dark:focus:border-primary"
                                placeholder="Pesquisar" aria-label="Search" aria-describedby="button-addon3"
                                required />

                            <!--Search button-->
                            <button type="submit"
                                class=" px-4 py-1 text-sm text-white rounded-md bg bg-lightBlue-500 text-primary transition duration-150 ease-in-out hover:bg-lightBlue-600
                                focus:outline-none focus:ring-0"
                                type="button" id="button-addon3" data-te-ripple-init>
                                <i class="fa-solid fa-magnifying-glass fa-md"></i>
                            </button>
                        </div>
                    </form>

                </nav>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo e(asset('js/alpine.min.js')); ?>"></script>

<script>
    if (screen.width > 1024) {
        window.onscroll = function() {
            scrollFunction()
        };
        const logoimg = document.getElementById('logoimg');
        const logotext = document.getElementById('logotext');
        const ouvi = document.getElementById('ouvi');

        function scrollFunction() {
            if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
                logoimg.classList.add("hidden");
                logotext.classList.remove("hidden");
                logotext.classList.add("active");
                ouvi.classList.add("hidden");

            } else {
                logoimg.classList.remove("hidden");
                logoimg.classList.add("active");
                logotext.classList.remove('active');
                logotext.classList.add('hidden');
                ouvi.classList.remove("hidden");
            }
        }
    }
</script>
<?php /**PATH /var/www/html/portaltransparencianewrn/resources/views/layouts/nav.blade.php ENDPATH**/ ?>