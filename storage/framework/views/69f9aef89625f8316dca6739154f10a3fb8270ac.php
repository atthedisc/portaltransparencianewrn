<?php $__env->startSection('title'); ?>
    - Receitas
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('css/exportfiles.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/jquery.treegrid.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/drilldown.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <h1 class="text-lg text-gray-900 tracking-widest font-medium title-font uppercase text-center pb-2" name="texto"
        style="font-size: 1.125rem; line-height: 2rem;">
        <i class="fas fa-search-dollar"></i>
        Receita
    </h1>
    <ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto"
        style="font-size: 0.875rem; line-height: 1.25rem;">
        <li class="inline-flex items-center">
            <a href="<?php echo e(url('/')); ?>">Inicio</a>
            <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                <path fill-rule="evenodd"
                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                    clip-rule="evenodd"></path>
            </svg>
        </li>
        <li class="inline-flex items-center">
            <a href="<?php echo e(route('receita')); ?>">Receitas</a>
            <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                <path fill-rule="evenodd"
                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                    clip-rule="evenodd"></path>
            </svg>
        </li>
        <li class="inline-flex items-center">
            <a href="#" class="text-teal-400">Receita</a>
        </li>
    </ul>

    <hr class="border-b-2 border-lightBlue-300 mb-12 mx-4">

    <div class="relative flex flex-col-reverse md:flex-row gap-3 w-full mb-4">
        <div class="flex px-4 pb-4 bg-white h-auto w-full border-b-4 border-lightBlue-300 rounded-md flex-col">
            <div id="container" class="h-full w-full"></div>
        </div>

        <div class="flex px-4 py-4 justify-center bg-white border-b-4 border-lightBlue-300 rounded-md flex-col">
            <div class="px-5 py-4 bg-white border border-gray-200 rounded-md">
                <div class="card rounded-xl flex flex-col">
                    <div class="grid grid-row-4 md:grid-row-4 gap-3">
                        <div class="py-3 pl-5 bg-gray-50 text-sm rounded-md shadow" name="texto"
                            style="font-size: 0.875rem; color: #333333; line-height: 1.25rem;">
                            <ul class="flex flex-wrap">
                                <li class="font-semibold text-green-600">Previsão Bruta: </li>
                                <li class="w-full">R$
                                    <?php echo e(number_format($previstoano->sum('vlrprevisaoatualizado'), 2, ',', '.')); ?>

                                </li>
                            </ul>
                        </div>
                        <div class="py-3 pl-5 bg-gray-50 text-sm rounded-md shadow" name="texto"
                            style="font-size: 0.875rem; color: #333333; line-height: 1.25rem;">
                            <ul class="flex flex-wrap">
                                <li class="font-semibold mr-2 text-gray-600">Arrecadação Bruta: </li>
                                <li class="w-full">R$
                                    <?php echo e(number_format($brutorealizado->sum('vlrarrecadacao'), 2, ',', '.')); ?></li>
                            </ul>
                        </div>
                        <div class="py-3 pl-5 bg-gray-50 text-sm rounded-md shadow" name="texto"
                            style="font-size: 0.875rem; color: #333333; line-height: 1.25rem;">
                            <ul class="flex flex-wrap">
                                <li class="font-semibold mr-2 text-gray-600">Dedução Realizada: </li>
                                <li class="w-full">R$
                                    <?php echo e(number_format($categorias->where('codcategoria', '8000000000')->sum('valorreceita'), 2, ',', '.')); ?>

                                </li>
                            </ul>
                        </div>
                        <div class="py-3 pl-5 bg-gray-50 text-sm rounded-md shadow" name="texto"
                            style="font-size: 0.875rem; color: #333333; line-height: 1.25rem;">
                            <ul class="flex flex-wrap">
                                <li class="font-semibold mr-2 text-gray-600">Arrecadação Líquida: </li>
                                <li class="w-full">R$ <?php echo e(number_format($vlrliquido->sum('valorreceita'), 2, ',', '.')); ?>

                                </li>
                            </ul>
                        </div>
                        <div class="py-3 pl-5 bg-gray-50 text-sm rounded-md shadow" name="texto"
                            style="font-size: 0.875rem; color: #333333; line-height: 1.25rem;">
                            <ul class="flex flex-wrap">
                                <?php if($posicao == '=' or $mes == 1): ?>
                                    <li class="font-semibold mr-2 text-gray-600">Exercício: </li>
                                    <li class="w-full"><?php echo app('translator')->get('meses.' . $mes); ?> de <?php echo e($ano); ?></li>
                                <?php else: ?>
                                    <li class="font-semibold mr-2 text-gray-600">Periodo: </li>
                                    <li class="w-full">Janeiro a
                                        <?php echo e(strtolower(__('meses.' . $mes))); ?> de <?php echo e($ano); ?></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <span class="text-xs"> * Receitas intra orçamentárias correntes não inclusas </span>
        </div>
    </div>

    <div class="w-full max-w-xl mx-auto">
        <div class="px-7 bg-white shadow-lg rounded-2xl mb-5">
            <div class="flex">
                <div class="flex-auto hover:w-full group">
                    <form target="_blank"
                        action="<?php echo e(route('export-xls-receita-prevista', [
                            'mes' => $mes,
                            'ano' => $ano,
                            'posicao' => $posicao,
                            'classificacao' => $classificacao,
                        ])); ?>"
                        method="POST" enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>
                        <button type="submit"
                            class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-green-500">
                            <span
                                class="block px-1 py-1 border border-transparent group-hover:border-green-500 rounded-full group-hover:flex-grow">
                                <i class="far fa-file-excel text-2xl pt-1"></i><span
                                    class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Baixar arquivo XLS</span>
                            </span>
                        </button>
                    </form>
                </div>
                <div class="flex-auto hover:w-full group">
                    <form target="_blank"
                        action="<?php echo e(route('export-pdf-receita-prevista', [
                            'mes' => $mes,
                            'ano' => $ano,
                            'posicao' => $posicao,
                            'classificacao' => $classificacao,
                        ])); ?>"
                        method="POST" enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>
                        <button type="submit"
                            class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-red-500">
                            <span
                                class="block px-1 py-1 border border-transparent group-hover:border-red-500 rounded-full group-hover:flex-grow">
                                <i class="far fa-file-pdf text-2xl pt-1"></i><span
                                    class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Baixar arquivo PDF</span>
                            </span>
                        </button>
                    </form>
                </div>
                <div class="flex-auto hover:w-full group">
                    <a href="#" onclick="window.print()"
                        class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-blueGray-500">
                        <span
                            class="block px-1 py-1 border border-transparent group-hover:border-blueGray-500 rounded-full group-hover:flex-grow">
                            <i class="fas fa-print text-2xl pt-1"></i><span
                                class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Imprimir arquivo</span>
                        </span>
                    </a>
                </div>
                <div class="flex-auto hover:w-full group">
                    <a href="<?php echo e(asset('docs/dicionario-de-dados/Dicionario de Dados Abertos-RN - Receita.xlsx')); ?>"
                        class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-indigo-500"
                        target="_blank">
                        <span
                            class="block px-1 py-1 border border-transparent group-hover:border-indigo-500 rounded-full group-hover:flex-grow">
                            <i class="far fa-file-alt text-2xl pt-1"></i><span
                                class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Dicionario de dados</span>
                        </span>
                    </a>
                </div>
                <div class="flex-auto hover:w-full group">
                    <a href="https://creativecommons.org/licenses/by/4.0/?ref=chooser-v1" target="_blank"
                        class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-orange-500">
                        <span
                            class="block px-1 py-1 border border-transparent group-hover:border-orange-500 rounded-full group-hover:flex-grow">
                            <i class="far fa-closed-captioning text-2xl pt-1"></i>
                            <span class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Licença</span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <?php echo $busca; ?>


    <div class="flex flex-col pb-4">
        <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                    <table id="tree" border="1" cellpadding="0" cellspacing="0"
                        class="min-w-full divide-y divide-gray-200">
                        <thead class="bg-lightBlue-100">
                            <tr>
                                <th class="px-4 py-3 text-left w-3/4 text-sm font-semibold text-gray-800 uppercase tracking-wider"
                                    name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Descrição
                                </th>
                                <th class="py-3 text-center w-1/3 font-semibold text-gray-800 uppercase tracking-wider"
                                    name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Receita Prevista (Bruta)
                                </th>
                                <th class="px-5 py-3 text-center w-1/3 font-semibold text-gray-800 uppercase tracking-wider"
                                    name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Receita Arrecadada (Bruta)
                                </th>
                                <th class="px-5 py-3 text-center w-1/3 font-semibold text-gray-800 uppercase tracking-wider"
                                    name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                    PERCENTUAL ARRECADAÇÃO
                                </th>
                            </tr>
                        </thead>
                        <tbody id="table" data-resetable="false" class="bg-white divide-y divide-gray-200 text-sm"
                            name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                            <?php $__currentLoopData = $categorias; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $categoria): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr data-key="<?php echo e($key); ?>" data-fetch="false"
                                    data-codigo='{"codcategoria ": <?php echo e($categoria->codcategoria); ?>}'
                                    class="treegrid-<?php echo e($key); ?>">
                                    <td class="px-4 py-4 w-3/6">
                                        <?php if($categoria->txtdescricaocategoria == 'Deduções da Receita'): ?>
                                            <?php echo e($categoria->txtdescricaocategoria); ?> Corrente
                                        <?php else: ?>
                                            <?php echo e($categoria->txtdescricaocategoria); ?>

                                        <?php endif; ?>
                                    </td>
                                    <td class="pr-12 text-right">
                                        <div class="text-blue-600 font-semibold text-xs leading-loose">
                                            R$ <?php echo e($categoria->valor_previsto); ?> 
                                        </div>
                                    </td>
                                    <td class="py-4 text-center">
                                        <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800"
                                        name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                            R$ <?php echo e(number_format($categoria->valorreceita, 2, ',', '.')); ?>

                                        </span>
                                    </td>
                                    <td class="pr-5 py-4 text-right"> 
                                        <div class="text-blue-600 font-semibold text-xs leading-loose">
                                            <?php echo e($categoria->porcentagem_formatada); ?>%
                                        </div>                                 
                                    </td>
                                </tr>
                                <tr class="treegrid-parent-<?php echo e($key); ?>"></tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                    <div id="pag"><?php echo e($categorias->appends(Request::all())->links()); ?></div>
                </div>
            </div>
        </div>
    </div>
    <div class="alert flex flex-row items-center bg-blue-200 p-1 rounded">
        <div
            class="alert-icon flex items-center bg-blue-100 border-2 border-blue-500 justify-center flex-shrink-0 rounded-full">
            <span class="text-blue-500">
                <svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
                    <path fill-rule="evenodd"
                        d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z"
                        clip-rule="evenodd"></path>
                </svg>
            </span>
        </div>
        <div class="alert-content ml-4">
            <div class="alert-description text-xs text-blue-600" name="texto"
                style="font-size: 0.75rem; line-height: 1rem;">
                <span class="font-semibold">Dados atualizados em:</span> <?php echo e(date('d/m/Y')); ?> | <span
                    class="font-semibold">Fonte:</span>
                SIGEF - SISTEMA INTEGRADO DE PLANEJAMENTO E GESTÃO FISCAL (Poder Executivo)
            </div>
        </div>
    </div>

    <meta id="info" data-source="receita" data-page="receita" data-reloadjs="true" data-mes="<?php echo e($mes); ?>"
        data-ano="<?php echo e($ano); ?>" data-posicao="<?php echo e($posicao); ?>">

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="<?php echo e(asset('js/select2.full.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/jquery.treegrid.min.js')); ?>"></script>

    <script src="<?php echo e(asset('js/search.js')); ?>"></script>

    <script src="<?php echo e(asset('js/dynamicPage.js')); ?>"></script>

    <script>
        function reload_js(src) {
            $('script[src="' + src + '"]').remove();
            $('<script>').attr('src', src).appendTo('#content');
            $("#tree").treegrid({
                initialState: 'collapsed'
            });
        }
    </script>

    <script>
        $("#tree").treegrid({
            initialState: 'collapsed'
        });
    </script>

    <script>
        const formatNumber = new Intl.NumberFormat('pt-BR', {
            style: 'currency',
            currency: 'BRL'
        })

        <?php if($posicao == '=' or $mes == 1): ?>
            const subtitle = `<?php echo e(strtolower(__('meses.' . $mes))); ?> do ano <?php echo e($ano); ?>`
        <?php else: ?>
            const subtitle = `janeiro a <?php echo e(strtolower(__('meses.' . $mes))); ?> do ano <?php echo e($ano); ?>`
        <?php endif; ?>

        //Create the chart
        Highcharts.setOptions({
            lang: {
                drillUpText: `◁ Voltar`
            }
        });

        Highcharts.chart('container', {
            chart: {
                type: 'column',
                spacingBottom: 30
            },

            credits: {
                style: {
                    color: '#333'
                }
            },

            title: {
                text: 'Total da receita arrecadada'
            },

            subtitle: {
                text: subtitle
            },

            accessibility: {
                announceNewData: {
                    enabled: true
                }
            },

            xAxis: {
                type: 'category'
            },

            yAxis: {
                title: {
                    text: 'Receita em R$'
                },
                labels: {
                    enabled: true,
                    formatter: function() {
                        return formatNumber.format(this.value ** 3)
                    }
                }
            },

            legend: {
                enabled: false
            },

            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        formatter: function() {
                            return formatNumber.format(this.point.y ** 3)
                        }
                    }
                }
            },

            tooltip: {
                formatter: function() {
                    return '<span style="font-size:11px">' + this.series.name + '</span><br>' +
                        '<span style="color:' + this.point.color + '">' + this.point.name + '</span>: <b>' +
                        formatNumber.format(this.point.y ** 3) + '</b> de receita<br/>'
                }
            },

            series: [{
                name: "Categoria",
                colorByPoint: true,
                data: [
                    <?php $__currentLoopData = $categoriaschart; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $categoria): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        {
                            name: `<?php echo e($categoria->txtdescricaocategoria); ?>`,
                            <?php if($categoria->valorreceita >= 0): ?>
                                y: Math.pow(<?php echo e($categoria->valorreceita); ?>, 1 / 3),
                            <?php else: ?>
                                y: (Math.pow((<?php echo e($categoria->valorreceita); ?> * (-1)), 1 / 3)) * (-1),
                            <?php endif; ?>
                            drilldown: <?php echo e($categoria->codcategoria); ?>

                        },
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                ]
            }],
            drilldown: {

                activeAxisLabelStyle: {
                    textDecoration: 'none',
                    color: '#666',
                    fontWeight: 'normal'
                },

                activeDataLabelStyle: {
                    textDecoration: 'none',
                    color: '#000'
                },

                series: [
                    <?php $__currentLoopData = $categoriaschart; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $categoria): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        {
                            name: `Subcategoria`,
                            id: <?php echo e($categoria->codcategoria); ?>,
                            data: [
                                <?php $__currentLoopData = $origenschart->where('codcategoria', $categoria->codcategoria); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $elemento): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    [
                                        "<?php echo e($elemento->txtdescricaoorigem); ?>",
                                        <?php if($elemento->valorreceita >= 0): ?>
                                            Math.pow(<?php echo e($elemento->valorreceita); ?>, 1 / 3)
                                        <?php else: ?>
                                            (Math.pow((<?php echo e($elemento->valorreceita); ?> * (-1)), 1 / 3)) * (-
                                                1)
                                        <?php endif; ?>
                                    ],
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            ]
                        },
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                ]
            }
        });
    </script>
    <script src="<?php echo e(asset('js/popover.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/portaltransparencianewrn/resources/views/receitas/receita/index.blade.php ENDPATH**/ ?>