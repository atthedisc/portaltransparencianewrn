<?php $__env->startSection('title'); ?>
- Doadores
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<section class="text-gray-600 body-font">
   <div class="container px-5 mx-auto">
      <div class="flex flex-col text-center w-full">

         <h1 class="pb-4 flex justify-center">
            <a href="<?php echo e(route('campanhas', [
                  'id' => 4
               ])); ?>">
               <img class="" src="images/logo/rn-chega-junto-no-combate-a-fome-logo.png" width="200" height="200"
                  alt="">
            </a>
         </h1>

         <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div class="py-2 align-middle inline-block max-w-md md:max-w-2xl sm:px-6 lg:px-8">
               <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                  <table id="myTable" class="min-w-full divide-y divide-lightBlue-200">
                     <thead class="bg-lightBlue-50">
                        <tr>
                           <th scope="col"
                              class="px-6 py-3 text-center text-xs font-medium text-lightBlue-800 uppercase tracking-wider"
                              name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                              <i class="far fa-heart"></i>
                              DOAÇÕES DA SOCIEDADE - DOADORES
                           </th>
                        </tr>
                     </thead>
                     <tbody class="bg-white divide-y divide-lightBlue-200 text-sm" name="texto"
                        style="font-size: 0.875rem; line-height: 1.25rem;">
                        <?php $__currentLoopData = $tables; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tabela): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                           <td class="px-6 py-4 whitespace-normal">
                              <a href="<?php echo e(asset('docs/rnchegajuntonocombateafome/doadores/'.$tabela->doadoreslink)); ?>"
                                 target="_blank">
                                 <span
                                    class="px-2 inline-flex text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline rounded-full"
                                    name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                    <?php echo e($tabela->doadores); ?>

                                 </span>
                              </a>
                           </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     </tbody>
                  </table>
                  <?php echo e($tables->links()); ?>

               </div>
            </div>
         </div>

      </div>
   </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/covid.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/portaltransparencianewrn/resources/views/rn-chega-junto-no-combate-a-fome/doacoes-da-sociedade/doadores.blade.php ENDPATH**/ ?>