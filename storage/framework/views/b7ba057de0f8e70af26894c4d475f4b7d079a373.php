<?php $__env->startSection('title'); ?>
- Cadastro de Autoridades
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<section class="text-gray-600 body-font">
   <div class="container px-5 mx-auto">
      <div class="flex flex-col text-center w-full">

         <h1 class="text-lg text-gray-900 tracking-widest font-medium title-font pb-2" name="texto"
            style="font-size: 1.125rem; line-height: 2rem;">
            <i class="fas fa-id-card"></i>
            CADASTRO DE AUTORIDADES
         </h1>



         <ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto"
            style="font-size: 0.875rem; line-height: 1.25rem;">
            <li class="inline-flex items-center">
               <a href="<?php echo e(url('/')); ?>">Início</a>
               <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                  <path fill-rule="evenodd"
                     d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                     clip-rule="evenodd"></path>
               </svg>
            </li>
            <li class="inline-flex items-center">
               <a href="#" class="text-teal-400">
                  Cadastro de Autoridades
               </a>
            </li>
         </ul>

         <hr class="border-b-2 border-lightBlue-300 mb-6 mx-4">

         <div class="grid md:grid-flow-col mx-auto">

            <table
               class='mx-10 mb-auto max-w-sm md:max-w-lg rounded-lg bg-white divide-y divide-lightBlue-200 overflow-hidden text-center'>

               <tr class="text-lightBlue-800 text-center bg-lightBlue-50">
                  <th class="font-medium text-sm uppercase tracking-wider px-6 py-4 text-center" name="texto"
                     style="font-size: 0.875rem; line-height: 1.25rem;">
                     CADASTRO DE AUTORIDADES
                  </th>
               </tr>

               <tbody class="divide-y divide-lightBlue-200">
                  <tr>
                     <td class="px-6 py-4 text-center text-sm">
                        <i class="fas fa-up-right-from-square mr-2"></i>
                        <a href="<?php echo e(asset('https://www.rn.gov.br/postagem/governadora/')); ?>"
                           class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                           name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                           Governadora
                        </a>
                     </td>
                  </tr>
                  <tr>
                     <td class="px-6 py-4 text-center text-sm">
                        <i class="fas fa-up-right-from-square mr-2"></i>
                        <a href="<?php echo e(asset('https://www.rn.gov.br/postagem/vice-governador/')); ?>"
                           class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                           name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                           Vice-Governador
                        </a>
                     </td>
                  </tr>
                  <tr>
                     <td class="px-6 py-4 text-center text-sm">
                        <i class="fas fa-up-right-from-square mr-2"></i>
                        <a href="<?php echo e(asset('https://www.rn.gov.br/postagem/administracao-direta/')); ?>"
                           class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                           name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                           Administração Direta
                        </a>
                     </td>
                  </tr>
                  <tr>
                    <td class="px-6 py-4 text-center text-sm">
                       <i class="fas fa-up-right-from-square mr-2"></i>
                       <a href="<?php echo e(asset('https://www.rn.gov.br/postagem/administracao-indireta/')); ?>"
                          class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                          name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                          Administração Indireta
                       </a>
                    </td>
                 </tr>
               </tbody>
            </table>
         </div>

      </div>
   </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/portaltransparencianewrn/resources/views/cadastro-autoridades/index.blade.php ENDPATH**/ ?>