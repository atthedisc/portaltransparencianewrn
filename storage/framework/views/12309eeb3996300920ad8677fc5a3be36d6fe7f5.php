<?php $__env->startSection('title'); ?>
- Doações Recebidas
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<section class="text-gray-600 body-font">
   <div class="container px-5 mx-auto">
      <div class="flex flex-col text-center w-full">

         <h1 class="pb-4 flex justify-center">
            <a href="<?php echo e(route('campanhas', [
                  'id' => 1
               ])); ?>">
               <img class="" src="images/logo/rnchegajunto.png" width="100" height="100" alt="">
            </a>
         </h1>

         <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div class="py-2 flex justify-center">
               <div class="shadow lg:w-2/4 w-full overflow-hidden border-b border-gray-200 sm:rounded-lg">
                  <table id="myTable" class="min-w-full divide-y divide-lightBlue-200">
                     <thead class="bg-lightBlue-50">
                        <tr>
                           <th scope="col"
                              class="px-6 py-3 text-center text-xs font-medium text-lightBlue-800 uppercase tracking-wider"
                              name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                              SOLIDARIEDADE NATALINA - DOAÇÕES RECEBIDAS
                           </th>
                        </tr>
                     </thead>
                     <tbody class="bg-white divide-y divide-lightBlue-200 text-sm" name="texto"
                        style="font-size: 0.875rem; line-height: 1.25rem;">
                        <?php $__currentLoopData = $tables; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tabela): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                           <td class="px-6 py-4 whitespace-normal">
                              <a href="<?php echo e(asset('docs/rnchegajunto/doacoesrecebidas/'.$tabela->doacoesrecebidaslink)); ?>"
                                 target="_blank">
                                 <span
                                    class="px-2 inline-flex text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline rounded-full"
                                    name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                    <?php echo e($tabela->doacoesrecebidas); ?>

                                 </span>
                              </a>
                           </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     </tbody>
                  </table>
                  <?php echo e($tables->links()); ?>

               </div>
            </div>
         </div>

      </div>
   </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts/covid.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/portaltransparencianewrn/resources/views/covid/rn-chega-junto/doacoes-recebidas.blade.php ENDPATH**/ ?>