<?php $__env->startSection('title'); ?>
- Covid
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('css/carousel.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/welcome.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('js/carouselcovid.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <?php echo csrf_field(); ?>
    <div class="w-full px-4 text-gray-800 leading-normal">
        <ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto"
            style="font-size: 0.875rem; line-height: 1.25rem;">
            <li class="inline-flex items-center">
                <a href="<?php echo e(url('/')); ?>">Inicio</a>
                <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                    <path fill-rule="evenodd"
                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                        clip-rule="evenodd"></path>
                </svg>
            </li>
            <li class="inline-flex items-center">
                <a class="text-teal-400" href="<?php echo e(url('/covid')); ?>">Covid</a>
            </li>
        </ul>
        <hr class="border-b-2 border-lightBlue-300 mb-12 mx-4">
        <div class="flex flex-wrap">
            <div class="w-full md:w-1/2 xl:w-1/2 p-3">
                <a id="conteudo" href="<?php echo e(url('/')); ?>">
                    <div
                        class="bg-white p-2 border-b-4 border-lightBlue-500 rounded-2xl overflow-hidden hover:shadow-2xl transition duration-500 transform hover:scale-105 cursor-pointer">
                        <div class="flex flex-row items-center">
                            <div class="flex-shrink pr-4">
                                <div class="bg-white flex items-center"><img src="<?php echo e(asset('images/icons/favicon.ico')); ?>"
                                        width="60" height="60" alt=""></i>
                                </div>
                            </div>
                            <div class="flex-1 text-right md:text-center">
                                <h3 class="font-semibold uppercase text-gray-600" name="texto"
                                    style="font-size: 1.25rem; line-height: 1.75rem;">
                                    PORTAL DA TRANSPARÊNCIA RN
                                </h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="w-full md:w-1/2 xl:w-1/2 p-3">
                <a href="<?php echo e(asset('docs/leialdirblanc/ALDIR BLANC RN - LISTA GERAL DE CONTEMPLADOS.xlsx')); ?>"
                    target="_blank">
                    <div
                        class="bg-white p-2 border-b-4 border-red-500 rounded-2xl overflow-hidden hover:shadow-2xl transition duration-500 transform hover:scale-105 cursor-pointer">
                        <div class="flex flex-row items-center">
                            <div class="flex-shrink pr-4">
                                <div class="bg-white flex h-full items-center"><img
                                        src="<?php echo e(asset('images/icons/aldirblanc.png')); ?>" width="60" height="60"
                                        alt="">
                                </div>
                            </div>
                            <div class="flex-1 text-right md:text-center">
                                <h3 class="font-semibold uppercase text-gray-600" name="texto"
                                    style="font-size: 1.25rem; line-height: 1.75rem;">
                                    Lei Aldir Blanc
                                </h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>


        

        <!--Carousel-->
        
        <!-- /Carousel -->

        <!--/ Console Content-->

        <!--Divider-->
        <hr class="border-b-2 border-blueGray-300 my-8 mx-4">

        <!-- Section -->
        <section class="text-gray-600 mt-16 body-font">
            <div class="container mx-auto">
                <div class="flex flex-col w-full">
                    <h1 class="text-3xl azulclaro font-semibold tracking-widest font-medium title-font mb-8">
                        PORTAL DA TRANSPARÊNCIA
                    </h1>
                    <p class="lg:w-full mx-auto leading-relaxed text-base text-justify" name="texto"
                        style="font-size: 1rem; line-height: 1.5rem;">
                        O Portal da Transparência do Estado do RN possibilita
                        o acompanhamento das informações orçamentárias e financeiras do executivo estadual, permitindo ao
                        cidadão a fiscalização, de forma eficiente e transparente da utilização dos recursos
                        públicos, para assegurar a sua correta aplicação. O Portal também disponibiliza informações sobre os
                        instrumentos de Planejamento, Relatórios Fiscais, Licitações, obras realizadas no Estado e outras
                        informações de relevância para os cidadãos norteriograndenses.
                        <a class="text-blue-400 hover:text-blue-600" href="<?php echo e(route('sobre-o-portal')); ?>" name="texto"
                            style="font-size: 1rem; line-height: 1.5rem;">
                            Clique aqui
                        </a> para mais informações.
                    </p>
                </div>

                <div class="container px-5 py-10 mx-auto">

                    <div class="flex items-center lg:w-full mx-auto border-b pb-10 mb-10 border-gray-200 sm:flex-row flex-col"
                        data-anime="left">
                        <div
                            class="sm:w-20 sm:h-20 h-12 w-12 sm:mr-10 inline-flex items-center justify-center rounded-full bg-lightBlue-100 azulclaro flex-shrink-0">
                            <i class="fas fa-lock fa-2x"></i>
                        </div>
                        <div class="flex-grow sm:text-left text-center mt-6 sm:mt-0">
                            <h2 class="text-gray-900 title-font font-medium mb-2" name="texto"
                                style="font-size: 1.125rem; line-height: 1.75rem;">
                                Confiabilidade
                            </h2>
                            <p class="leading-relaxed text-justify" name="texto"
                                style="font-size: 0.875rem; line-height: 1.25rem;">
                                O Portal é atualizado diariamente com os dados do Sistema
                                Integrado de Planejamento e Gestão Fiscal (SIGEF), de onde são extraídas diretamente as
                                informações da execução financeiro-orçamentária do Estado.
                            </p>
                        </div>
                    </div>

                    <div class="flex items-center lg:w-full mx-auto border-b pb-10 mb-10 border-gray-200 sm:flex-row flex-col"
                        data-anime="right">
                        <div class="flex-grow sm:text-left text-center mt-6 sm:mt-0">
                            <h2 class="text-gray-900 title-font font-medium mb-2 md:text-right sm:text-center"
                                name="texto" style="font-size: 1.125rem; line-height: 1.75rem;">
                                Consultas Personalizadas
                            </h2>
                            <p class="leading-relaxed text-justify" name="texto"
                                style="font-size: 0.875rem; line-height: 1.25rem;">
                                Ao acessar as opções Receita Detalhada e Despesa Detalhada, o
                                cidadão poderá montar a sua própria consulta, a partir de dados e critérios selecionados,
                                obtendo informações que atendam aos seus interesses. Essas consultas trazem
                                recursos gráficos que garantirão uma análise mais rápida e visual.
                            </p>
                        </div>
                        <div
                            class="sm:w-20 sm:order-none order-first sm:h-20 h-12 w-12 sm:ml-10 inline-flex items-center justify-center rounded-full bg-red-100 text-red-500 flex-shrink-0">
                            <i class="fas fa-chart-bar fa-2x"></i>
                        </div>
                    </div>

                    <div class="flex items-center lg:w-full mx-auto border-b pb-10 mb-10 border-gray-200 sm:flex-row flex-col"
                        data-anime="left">
                        <div
                            class="sm:w-20 sm:h-20 h-12 w-12 sm:mr-10 inline-flex items-center justify-center rounded-full bg-yellow-100 text-yellow-500 flex-shrink-0">
                            <i class="fas fa-database fa-2x"></i>
                        </div>
                        <div class="flex-grow sm:text-left text-center mt-6 sm:mt-0">
                            <h2 class="text-gray-900 title-font font-medium mb-2" name="texto"
                                style="font-size: 1.125rem; line-height: 1.75rem;">
                                Dados Abertos
                            </h2>
                            <p class="leading-relaxed text-justify" name="texto"
                                style="font-size: 0.875rem; line-height: 1.25rem;">
                                O Portal da Transparência do RN propicia o acesso a dados no
                                formato aberto, processáveis por máquina e disponibilizados em formato CSV. Dessa forma, o
                                cidadão poderá acessar os dados livremente, fazendo cruzamentos e criando aplicativos
                                que demonstrem novas formas de utilização e visualização desses dados em benefício da
                                sociedade.
                            </p>
                        </div>
                    </div>

                    <div class="flex items-center lg:w-full mx-auto border-b pb-10 mb-10 border-gray-200 sm:flex-row flex-col"
                        data-anime="right">
                        <div class="flex-grow sm:text-left text-center mt-6 sm:mt-0">
                            <h2 class="text-gray-900 title-font font-medium mb-2 md:text-right sm:text-center"
                                name="texto" style="font-size: 1.125rem; line-height: 1.75rem;">
                                Fale Conosco
                            </h2>
                            <p class="leading-relaxed text-justify" name="texto"
                                style="font-size: 0.875rem; line-height: 1.25rem;">
                                Se não encontrou o que procurava, acesse os serviços da Ouvidoria
                                Geral do Estado. Caso deseje ter acesso a dados ou informações públicas adicionais, utilize
                                o
                                Sistema de Informação ao Cidadão – e-SIC RN ou consulte a área de ajuda,
                                que disponibiliza glossário, perguntas frequentes e outras formas de auxílio.
                            </p>
                        </div>
                        <div
                            class="sm:w-20 sm:order-none order-first sm:h-20 h-12 w-12 sm:ml-10 inline-flex items-center justify-center rounded-full bg-green-100 text-green-500 flex-shrink-0">
                            <i class="far fa-comments fa-2x"></i>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- Animate scroll -->
    <script>
        const debounce = function(func, wait, immediate) {
            let timeout;
            return function(...args) {
                const context = this;
                const later = function() {
                    timeout = null;
                    if (!immediate) func.apply(context, args);
                };

                const callNow = immediate && !timeout;
                clearTimeout(timeout);
                timeout = setTimeout(later, wait);
                if (callNow) func.apply(context, args);
            };
        };

        const target = document.querySelectorAll('[data-anime]');
        const animationClass = 'animate';

        function animeScroll() {
            const windowTop = window.pageYOffset + (window.innerHeight * 0.98);
            target.forEach(function(element) {
                if ((windowTop) > element.offsetTop) {
                    element.classList.add(animationClass);
                }
            })
        }

        animeScroll();

        if (target.length) {
            window.addEventListener('scroll', debounce(function() {
                animeScroll();
            }, 2));
        }
    </script>

    <style>
        .azulclaro {
            color: #0185C2;
        }

        .bg-azulclaro {
            background-color: #0185C2;
        }
    </style>
    <!-- /Section -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts/covid.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/portaltransparencianewrn/resources/views/covid/welcome.blade.php ENDPATH**/ ?>