<?php $__env->startSection('title'); ?>
- Campanhas
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<h1 class="text-lg text-center text-gray-900 tracking-widest font-medium title-font pb-2" name="texto"
   style="font-size: 1.125rem; line-height: 2rem;">CAMPANHAS
</h1>

<ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto"
   style="font-size: 0.875rem; line-height: 1.25rem;">

   <li class="inline-flex items-center">
      <a href="<?php echo e(url('/covid')); ?>">Início</a>
      <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
         <path fill-rule="evenodd"
            d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
            clip-rule="evenodd"></path>
      </svg>
   </li>
   <li class="inline-flex items-center">
      <a href="#" class="text-teal-400">
         <?php if($id === '1'): ?>
         RN Chega Junto
         <?php elseif($id === '2'): ?>
         RN + Unido
         <?php elseif($id === '3'): ?>
         RN + Protegido
         <?php elseif($id === '4'): ?>
         RN Chega Junto no Combate à Fome
         <?php endif; ?>
      </a>
   </li>

</ul>

<hr class="border-b-2 border-lightBlue-300 mb-6 mx-4">

<section class="text-gray-600 body-font">
   <div class="container px-5 pt-6 pb-12 mx-auto">
      <div class="flex flex-wrap -m-4">

         <div class="xl:w-1/4 md:w-1/2 p-4 w-full">
            <div id="border4" class="bg-white p-6 h-full rounded-lg border-4 border-transparent">

               <img id="img4" class="h-32 rounded w-full object-contain object-center mb-6 filter grayscale"
                  src="<?php echo e(asset('images/logo/rn-chega-junto-no-combate-a-fome-logo-redi.png ')); ?>" alt="content">

               <h3 id="title4"
                  class="tracking-widest leading-loose text-blueGray-400 text-xs text-center font-semibold title-font uppercase pb-2"
                  name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                  RN Chega Junto no Combate à Fome
               </h3>

               <div class="flex flex-row mb-4 leading-loose">
                  <div id="links4" class="flex flex-col mb-4 ml-2 mt-1 text-blueGray-400">
                     <a href="<?php echo e(asset('docs/rnchegajuntonocombateafome/RN Chega Junto no Combate à Fome.pdf')); ?>"
                        target="_blank">
                        <div class='hover:underline font-base text-sm pb-2' name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           O que é o RN Chega Junto no Combate à fome?
                        </div>
                     </a>
                     <a href="<?php echo e(route('doacoes')); ?>">
                        <div class='hover:underline font-base text-sm pb-2' name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           Doações do Governo
                        </div>
                     </a>
                     <div class='font-base text-sm pb-2'>
                        Doações da Sociedade
                     </div>
                     <a href="<?php echo e(route('doadores')); ?>">
                        <div class='hover:underline font-base text-sm pl-6 pb-2' name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           Doadores
                        </div>
                     </a>
                     <a href="<?php echo e(route('beneficiados')); ?>">
                        <div class='hover:underline font-base text-sm pl-6 pb-2' name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           Beneficiados
                        </div>
                     </a>
                     <a href="<?php echo e(route('registros')); ?>">
                        <div class='hover:underline font-base text-sm pb-2' name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           Registros de Entregas
                        </div>
                     </a>
                     <a href="<?php echo e(asset('docs/rnchegajuntonocombateafome/RN Chega Junto no Combate à Fome - Contatos.pdf')); ?>"
                        target="_blank">
                        <div class='hover:underline font-base text-sm leading-8 pb-2' name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           Canal de Comunicação
                        </div>
                     </a>
                  </div>
               </div>

            </div>
         </div>

         <div class="xl:w-1/4 md:w-1/2 p-4 w-full">
            <div id="border3" class="bg-white p-6 h-full rounded-lg border-4 border-transparent">

               <img id="img3" class="h-32 rounded w-full object-contain object-center mb-6 filter grayscale"
                  src="<?php echo e(asset('images/logo/rnmaisprotegido-edi.png ')); ?>" alt="content">

               <h3 id="title3"
                  class="tracking-widest text-blueGray-400 text-xs text-center font-semibold title-font uppercase pb-2">
                  RN + Protegido
               </h3>

               <div class="flex flex-row pb-28">
                  <div id="links3" class="flex flex-col mb-4 ml-2 mt-1 text-blueGray-400">
                     <a href="<?php echo e(asset('docs/rnmaisprotegido/oqueeornmaisprotegido.pdf')); ?>" target="_blank">
                        <div class='hover:underline font-base text-sm pb-2' name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           O que é o RN + Protegido?
                        </div>
                     </a>
                     <a href="<?php echo e(route('rn-mais-protegido-doacoes-recebidas')); ?>">
                        <div class='hover:underline font-base text-sm pb-2' name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           Doações recebidas e seus beneficiários
                        </div>
                     </a>
                  </div>
               </div>

            </div>
         </div>

         <div class="xl:w-1/4 md:w-1/2 p-4 w-full">
            <div id="border2" class="bg-white p-6 h-full rounded-lg border-4 border-transparent">

               <img id="img2" class="h-32 rounded w-full object-contain object-center mb-6 filter grayscale"
                  src="<?php echo e(asset('images/logo/rnmaisunido_colorido.png ')); ?>" alt="content">

               <h3 id="title2"
                  class="tracking-widest text-blueGray-400 text-xs text-center font-semibold title-font uppercase pb-2"
                  name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                  RN + Unido
               </h3>

               <div class="flex flex-row pb-20">
                  <div id="links2" class="flex flex-col mb-4 ml-2 mt-1 text-blueGray-400">
                     <a href="<?php echo e(asset('docs/rnmaisunido/rnmaisunido.pdf')); ?>" target="_blank">
                        <div class='hover:underline font-base text-sm pb-2' name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           O que é o RN + Unido?
                        </div>
                     </a>
                     <a href="<?php echo e(asset('docs/rnmaisunido/rnmaisunido-oquedoar-tabela.pdf')); ?>" target="_blank">
                        <div class='hover:underline font-base text-sm pb-2 pl-6' name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           O que doar?
                        </div>
                     </a>
                     <a href="<?php echo e(route('doacoesrecebidas')); ?>">
                        <div class='hover:underline font-base text-sm pb-2 pl-6' name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           Doações recebidas
                        </div>
                     </a>
                     <a href="<?php echo e(route('beneficiarios')); ?>">
                        <div class='hover:underline font-base text-sm pb-2 pl-6' name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           Beneficiários
                        </div>
                     </a>
                     <a href="<?php echo e(asset('docs/rnmaisunido/doacoes-pessoas-juridicas.pdf')); ?>" target="_blank">
                        <div class='hover:underline font-base text-sm pb-2 pl-6' name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           Doações pessoas jurídicas
                        </div>
                     </a>
                  </div>
               </div>

            </div>
         </div>

         <div class="xl:w-1/4 md:w-1/2 p-4 w-full">
            <div id="border1" class="bg-white p-6 h-full rounded-lg border-4 border-transparent">

               <img id="img1" class="h-32 rounded w-full object-contain object-center mb-6 filter grayscale"
                  src="<?php echo e(asset('images/logo/rnchegajuntotransp.png ')); ?>" alt="content">

               <h3 id="title1"
                  class="tracking-widest text-blueGray-400 text-xs text-center font-semibold title-font uppercase pb-2"
                  name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                  RN Chega Junto
               </h3>

               <div id="links1" class="flex flex-col mb-4 ml-2 mt-1 text-blueGray-400">
                  <a href="<?php echo e(asset('docs/rnchegajunto/oqueeornchegajunto.pdf')); ?>" target="_blank">
                     <div class='hover:underline font-base text-sm pb-2' name="texto"
                        style="font-size: 0.875rem; line-height: 1.25rem;">
                        O que é o RN Chega Junto?
                     </div>
                  </a>
                  <a href="<?php echo e(route('rnchegajunto-doacoesrealizadas')); ?>">
                     <div class='hover:underline font-base text-sm pb-2' name="texto"
                        style="font-size: 0.875rem; line-height: 1.25rem;">
                        Doações realizadas
                     </div>
                  </a>
                  <a href="<?php echo e(asset('docs/rnchegajunto/solidariedadenatalina.pdf')); ?>" target="_blank">
                     <div class='hover:underline font-base text-sm pb-2' name="texto"
                        style="font-size: 0.875rem; line-height: 1.25rem;">
                        Solidariedade Natalina
                     </div>
                  </a>
                  <a href="<?php echo e(route('rnchegajunto-doacoesrecebidas')); ?>">
                     <div class='hover:underline font-base text-sm pb-2 pl-6' name="texto"
                        style="font-size: 0.875rem; line-height: 1.25rem;">
                        Doações recebidas
                     </div>
                  </a>
                  <a href="<?php echo e(route('rnchegajunto-beneficiarios')); ?>">
                     <div class='hover:underline font-base text-sm pb-2 pl-6' name="texto"
                        style="font-size: 0.875rem; line-height: 1.25rem;">
                        Beneficiários
                     </div>
                  </a>
               </div>

            </div>
         </div>

      </div>
   </div>
</section>

<script>
   const bordercampanha = document.getElementById('border'+ <?php echo e($id); ?>)
    const imgcampanha = document.getElementById('img' + <?php echo e($id); ?>)
    const titlecampanha = document.getElementById('title' + <?php echo e($id); ?>)
    const linkscampanha = document.getElementById('links' + <?php echo e($id); ?>)

    bordercampanha.classList.remove('border-transparent')
    bordercampanha.classList.add('border-blueGray-100')
    bordercampanha.classList.add('shadow-2xl')

    imgcampanha.classList.remove('grayscale')

    titlecampanha.classList.remove('text-blueGray-400')
    titlecampanha.classList.add('text-blue-900')

    linkscampanha.classList.remove('text-blueGray-400')
    linkscampanha.classList.add('text-lightBlue-500')
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/covid.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/portaltransparencianewrn/resources/views/campanhas/index.blade.php ENDPATH**/ ?>