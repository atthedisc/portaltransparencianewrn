<?php $__env->startSection('title'); ?>
- Registros de Entregas
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<link rel="stylesheet" href="<?php echo e(asset('css/registros.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<!-- Section -->
<section class="text-gray-600 body-font">

   <h1 class="pb-4 flex justify-center">
      <a href="<?php echo e(route('campanhas', [
            'id' => 4
         ])); ?>">
         <img class="" src="images/logo/rn-chega-junto-no-combate-a-fome-logo.png" width="200" height="200" alt="">
      </a>
   </h1>

   <div class="flex flex-col text-center">
      <h3 class="pt-4 uppercase font-semibold text-xl" name="texto" style="font-size: 1.25rem; line-height: 1.75rem;">
         Natal
      </h3>
   </div>

   <div class="container mx-auto px-4">
      <section class="py-4 px-4">

         <div class="flex flex-wrap -mx-4 -mb-8">

            <div class="md:w-1/4 px-4 mb-8"><img class="object-contain h-56 w-full cursor-zoom-in modal-open"
                  src="docs/rnchegajuntonocombateafome/registros/ASSOCIAÇÃO BENEFICENTE E CULTURAL DAS ROCAS - NatalRN - 200 cestas básicas.jpeg"
                  onclick="onClick(this)" alt="">
               <h1 class="text-center uppercase text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">ASSOCIAÇÃO BENEFICENTE E CULTURAL DAS
                  ROCAS<br>Natal/RN -
                  200
                  cestas básicas</h1>
            </div>

            <div class="md:w-1/4 px-4 mb-8"><img class="object-contain h-56 w-full cursor-zoom-in modal-open"
                  src="docs/rnchegajuntonocombateafome/registros/ASSOCIAÇÃO COMUNITÁRIA DE ARTESÃOS DE MÃE LUIZA - NatalRN - 100 cestas básicas.jpeg"
                  onclick="onClick(this)" alt="">
               <h1 class="text-center uppercase text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">ASSOCIAÇÃO COMUNITÁRIA DE ARTESÃOS DE MÃE
                  LUIZA<br>Natal/RN
                  -
                  100 cestas básicas</h1>
            </div>

            <div class="md:w-1/4 px-4 mb-8"><img class="object-contain h-56 w-full cursor-zoom-in modal-open"
                  src="docs/rnchegajuntonocombateafome/registros/Associação Cultural e Desportiva do Bairro Vila Paraíso - NatalRN - 73 cestas básicas.jpeg"
                  onclick="onClick(this)">
               <h1 class="text-center uppercase text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">Associação Cultural e Desportiva do Bairro Vila
                  Paraíso<br>Natal/RN - 73 cestas básicas</h1>
            </div>

            <div class="md:w-1/4 px-4 mb-8"><img class="object-contain h-56 w-full cursor-zoom-in modal-open"
                  src="docs/rnchegajuntonocombateafome/registros/ASSOCIAÇÃO CULTURAL - ACARN - NatalRN - 82 cestas básicas.jpeg"
                  onclick="onClick(this)" max-width="1280" max-height="720" alt="">
               <h1 class="text-center uppercase text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">ASSOCIAÇÃO CULTURAL - ACARN<br>Natal/RN - 82 cestas
                  básicas
               </h1>
            </div>

            <div class="md:w-1/4 px-4 mb-8"><img class="object-contain h-56 w-full cursor-zoom-in modal-open"
                  src="docs/rnchegajuntonocombateafome/registros/ASSOCIAÇÃO DE ARTESÃOS CULTURA VIVA - natalRN - 50 cestas básicas.jpeg"
                  onclick="onClick(this)" alt="">
               <h1 class="text-center uppercase text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">ASSOCIAÇÃO DE ARTESÃOS CULTURA VIVA<br>Natal/RN -
                  50 cestas
                  básicas</h1>
            </div>

            <div class="md:w-1/4 px-4 mb-8"><img class="object-contain h-56 w-full cursor-zoom-in modal-open"
                  src="docs/rnchegajuntonocombateafome/registros/ASSOCIAÇÃO DE CARROCEIRO PLANALTO - NatalRN - 82 cestas básicas.jpeg"
                  onclick="onClick(this)" alt="">
               <h1 class="text-center uppercase text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">ASSOCIAÇÃO DE CARROCEIRO PLANALTO<br>Natal/RN - 82
                  cestas
                  básicas</h1>
            </div>

            <div class="md:w-1/4 px-4 mb-8"><img class="object-contain h-56 w-full cursor-zoom-in modal-open"
                  src="docs/rnchegajuntonocombateafome/registros/ASSOCIAÇÃO DE MULHERES DE SANTA CATARINA - NatalRN - 82 cestas básicas.jpeg"
                  onclick="onClick(this)" alt="">
               <h1 class="text-center uppercase text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">ASSOCIAÇÃO DE MULHERES DE SANTA
                  CATARINA<br>Natal/RN - 82
                  cestas básicas</h1>
            </div>

            <div class="md:w-1/4 px-4 mb-8"><img class="object-contain h-56 w-full cursor-zoom-in modal-open"
                  src="docs/rnchegajuntonocombateafome/registros/ASSOCIAÇÃO DE SURDOS DE NATAL - 20 cestas.jpeg"
                  onclick="onClick(this)" alt="">
               <h1 class="text-center uppercase text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">ASSOCIAÇÃO DE SURDOS DE NATAL<br>Natal/RN - 20
                  cestas
                  básicas
               </h1>
            </div>

            <div class="md:w-1/4 px-4 mb-8"><img class="object-contain h-56 w-full cursor-zoom-in modal-open"
                  src="docs/rnchegajuntonocombateafome/registros/ASSOCIAÇÃO DOS DEFICIENTES FISICOS DO RN - ADEFERN - NatalRN - 70 cestas básicas.jpeg"
                  onclick="onClick(this)" alt="">
               <h1 class="text-center uppercase text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">ASSOCIAÇÃO DOS DEFICIENTES FISICOS DO RN -
                  ADEFERN<br>Natal/RN
                  - 70 cestas básicas
               </h1>
            </div>

            <div class="md:w-1/4 px-4 mb-8"><img class="object-contain h-56 w-full cursor-zoom-in modal-open"
                  src="docs/rnchegajuntonocombateafome/registros/ASSOCIAÇÃO PELOTÃO DE OPERAÇÃO DO ESPÍRITO SANTO - NatalRN - 50 cestas básicas.jpeg"
                  onclick="onClick(this)" alt="">
               <h1 class="text-center uppercase text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">ASSOCIAÇÃO PELOTÃO DE OPERAÇÃO DO ESPÍRITO
                  SANTO<br>Natal/RN
                  -
                  50 cestas básicas
               </h1>
            </div>

            <div class="md:w-1/4 px-4 mb-8"><img class="object-contain h-56 w-full cursor-zoom-in modal-open"
                  src="docs/rnchegajuntonocombateafome/registros/CIRCULO OPERÁRIO DE IGAPO.jpg" onclick="onClick(this)"
                  alt="">
               <h1 class="text-center uppercase text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">CIRCULO OPERÁRIO DE IGAPO<br>Natal/RN - 70 cestas
                  básicas
               </h1>
            </div>

            <div class="md:w-1/4 px-4 mb-8"><img class="object-contain h-56 w-full cursor-zoom-in modal-open"
                  src="docs/rnchegajuntonocombateafome/registros/CLUBE DE MAES LOURDES GUILHERME.jpg"
                  onclick="onClick(this)" alt="">
               <h1 class="text-center uppercase text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">CLUBE DE MAES LOURDES GUILHERME<br>Natal/RN - 23
                  cestas
                  básicas
               </h1>
            </div>

            <div class="md:w-1/4 px-4 mb-8"><img class="object-contain h-56 w-full cursor-zoom-in modal-open"
                  src="docs/rnchegajuntonocombateafome/registros/FEDERAÇÃO DA MULHERES DO RN - NatalRN - 194 cestas básicas.jpeg"
                  onclick="onClick(this)" alt="">
               <h1 class="text-center uppercase text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">FEDERAÇÃO DA MULHERES DO RN<br>Natal/RN - 194
                  cestas básicas
               </h1>
            </div>

            <div class="md:w-1/4 px-4 mb-8"><img class="object-contain h-56 w-full cursor-zoom-in modal-open"
                  src="docs/rnchegajuntonocombateafome/registros/GRUPO DE MULHERES EM CONQUISTA - NatalRN - 50 cestas básicas.jpeg"
                  onclick="onClick(this)" alt="">
               <h1 class="text-center uppercase text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">GRUPO DE MULHERES EM CONQUISTA<br>Natal/RN - 50
                  cestas
                  básicas
               </h1>
            </div>

            <div class="md:w-1/4 px-4 mb-8"><img class="object-contain h-56 w-full cursor-zoom-in modal-open"
                  src="docs/rnchegajuntonocombateafome/registros/YALORIXÁ - NatalRN - 25 cestas básicas.jpeg"
                  onclick="onClick(this)" alt="">
               <h1 class="text-center uppercase text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">YALORIXÁ<br>Natal/RN - 25 cestas básicas
               </h1>
            </div>

         </div>

         <div class="flex flex-col text-center">
            <h3 class="pt-8 uppercase font-semibold text-xl" name="texto"
               style="font-size: 1.25rem; line-height: 1.75rem;">
               Macaíba
            </h3>
         </div>

         <div class="flex flex-wrap -mx-4 -mb-8 pt-8">
            <div class="md:w-1/4 px-4 mb-8"><img class="object-contain h-56 w-full cursor-zoom-in modal-open"
                  src="docs/rnchegajuntonocombateafome/registros/ASSOCIAÇÃO MACAIBENSE DE DEFICIÊNTES - 30 cestas básicas.jpeg"
                  onclick="onClick(this)" alt="">
               <h1 class="text-center uppercase text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">ASSOCIAÇÃO MACAIBENSE DE DEFICIENTES<br>30 cestas
                  básicas
               </h1>
            </div>
         </div>

         

         <style>
            .modal {
               transition: opacity 0.25s ease;
               z-index: 200;
            }

            body.modal-active {
               overflow-x: hidden;
               overflow-y: visible !important;
            }
         </style>

         <!--Modal-->
         <div
            class="modal opacity-0 pointer-events-none fixed w-full h-full top-0 left-0 flex items-center justify-center">
            <div class="modal-overlay absolute w-full h-full bg-gray-900 opacity-50"></div>

            <div class="modal-container bg-white w-11/12 md:max-w-2xl mx-auto rounded shadow-lg z-50 overflow-y-auto">

               <div
                  class="modal-close absolute top-0 right-0 cursor-pointer flex flex-col items-center mt-4 mr-4 text-white text-sm z-50"
                  name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                  <svg class="fill-current text-white" xmlns="http://www.w3.org/2000/svg" width="18" height="18"
                     viewBox="0 0 18 18">
                     <path
                        d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z">
                     </path>
                  </svg>
                  <span class="text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">(Esc)</span>
               </div>

               <!-- Add margin if you want to see some of the overlay behind the modal-->
               <div class="modal-content py-4 text-left px-6">

                  <!--botão X de fechar-->
                  <div class="flex justify-end items-center pb-3">
                     <div class="modal-close cursor-pointer z-50">
                        <svg class="fill-current text-black" xmlns="http://www.w3.org/2000/svg" width="18" height="18"
                           viewBox="0 0 18 18">
                           <path
                              d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z">
                           </path>
                        </svg>
                     </div>
                  </div>

                  <!--Body-->
                  <img id="img01" style="max-width:100%">
               </div>
            </div>
         </div>

         <script>
            var openmodal = document.querySelectorAll('.modal-open')
                  for (var i = 0; i < openmodal.length; i++) {
                     openmodal[i].addEventListener('click', function(event){
    	                  event.preventDefault()
    	                  toggleModal()
                     })
                  }

                  const overlay = document.querySelector('.modal-overlay')
                  overlay.addEventListener('click', toggleModal)

                  var closemodal = document.querySelectorAll('.modal-close')
                  for (var i = 0; i < closemodal.length; i++) {
                   closemodal[i].addEventListener('click', toggleModal)
                  }

                  document.onkeydown = function(evt) {
                  evt = evt || window.event
                  var isEscape = false
                  if ("key" in evt) {
    	               isEscape = (evt.key === "Escape" || evt.key === "Esc")
                  } else {
    	               isEscape = (evt.keyCode === 27)
                  }
                  if (isEscape && document.body.classList.contains('modal-active')) {
    	               toggleModal()
                  }
               };

               function toggleModal () {
                  const body = document.querySelector('body')
                  const modal = document.querySelector('.modal')
                  modal.classList.toggle('opacity-0')
                  modal.classList.toggle('pointer-events-none')
                  body.classList.toggle('modal-active')
               }

               function onClick(element) {
                  document.getElementById("img01").src = element.src;
               }
         </script>
      </section>
   </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/portaltransparencianewrn/resources/views/rn-chega-junto-no-combate-a-fome/registros-de-entregas.blade.php ENDPATH**/ ?>