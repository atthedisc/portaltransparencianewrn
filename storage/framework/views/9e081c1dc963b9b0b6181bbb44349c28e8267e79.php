<?php $__env->startSection('title'); ?>
- Cartas de Serviço da SET
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<link rel="stylesheet" href="<?php echo e(asset('css/dropdown.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<section class="text-gray-600 body-font">
    <div class="container px-5 mx-auto">
        <div class="flex flex-col text-center w-full">

            <h1 class="text-lg text-gray-900 tracking-widest font-medium title-font pb-2" name="texto"
                style="font-size: 1.125rem; line-height: 2rem;">
                <i class="fas fa-search-plus"></i>
                CARTAS DE SERVIÇO DA SET
            </h1>

            <div class="flex flex-col text-center w-full mb-3">
                <p class="lg:w-3/4 mx-auto leading-relaxed text-sm text-gray-500 text-center" name="texto"
                    style="font-size: 0.875rem; line-height: 1.25rem;">
                    Clique no órgão desejado para saber mais detalhes
                </p>
            </div>

            <ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto"
                style="font-size: 0.875rem; line-height: 1.25rem;">
                <li class="inline-flex items-center">
                    <a href="<?php echo e(url('/')); ?>">Início</a>
                    <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                        <path fill-rule="evenodd"
                            d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                            clip-rule="evenodd"></path>
                    </svg>
                </li>
                <li class="inline-flex items-center">
                    <a href="#" class="text-teal-400">
                        Cartas de Serviço da SET
                    </a>
                </li>
            </ul>

            <hr class="border-b-2 border-lightBlue-300 mb-6 mx-4">

            <div class="flex flex-wrap justify-center">

                <main class="pl-4 pb-4 md:w-full">
                    <section class="shadow row bg-white rounded-lg">
                        <div class="tabs">

                            <div class="border-b tab">
                                <div class="border-l-2 border-transparent relative">
                                    <header class="p-5 pl-8 pr-8 select-none tab-label">
                                        <span class="text-gray-500 text-sm text-center" name="texto"
                                            style="font-size: 0.875rem; line-height: 1.25rem;">
                                            SECRETARIA DE TRIBUTAÇÃO
                                        </span>
                                    </header>
                                </div>
                            </div>

                            <div class="border-b tab">
                                <div class="border-l-2 border-transparent relative">
                                   <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox"
                                      id="this">
                                   <header
                                      class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label"
                                      for="this">
                                      <span class="text-lightBlue-500 text-sm text-left" name="texto"
                                         style="font-size: 0.875rem; line-height: 1.25rem;">
                                         SECRETARIA DE ESTADO DA TRIBUTAÇÃO - SET
                                      </span>
                                      <div
                                         class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                         <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24"
                                            stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                            <polyline points="6 9 12 15 18 9">
                                            </polyline>
                                         </svg>
                                      </div>
                                   </header>
                                   <div class="tab-content">
                                      <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto"
                                         style="font-size: 0.875rem; line-height: 1.25rem;">
                                         <b>Secretário de Estado:</b> Carlos Eduardo Xavier<br>
                                         <b>Telefones:</b> (84) 3232-2004<br>
                                         <b>Call Center:</b> (84) 3209-7880<br>
                                         <b>Website:</b> http://www.set.rn.gov.br<br>
                                         <b>Horário de funcionamento:</b> 8h às 14h<br>
                                         <b>Endereço:</b> Centro Administrativo do Estado - Lagoa Nova - Natal/RN -
                                         CEP: 59064-901
                                      </div>
                                      <div class="flex justify-left pb-4 pl-8">
                                         <a href="<?php echo e(asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_SET.pdf')); ?>"
                                            target="_blank"><button
                                               class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs"
                                               name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                               Carta de Serviço
                                            </button></a>
                                      </div>
                                   </div>
                                </div>
                             </div>

                             <div class="border-b tab">
                                <div class="border-l-2 border-transparent relative">
                                   <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox"
                                      id="this">
                                   <header
                                      class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label"
                                      for="this">
                                      <span class="text-lightBlue-500 text-sm text-left uppercase" name="texto"
                                         style="font-size: 0.875rem; line-height: 1.25rem;">
                                         1ª URT Primeira Unidade Regional de Tributação
                                      </span>
                                      <div
                                         class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                         <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24"
                                            stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                            <polyline points="6 9 12 15 18 9">
                                            </polyline>
                                         </svg>
                                      </div>
                                   </header>
                                   <div class="tab-content">
                                      <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto"
                                         style="font-size: 0.875rem; line-height: 1.25rem;">
                                         <b>Telefone/WhatsApp:</b> 3232 -2178/3232- 2049<br>
                                         <b>E-mail:</b> protocolo1urt@set.rn.gov.br | plantaofiscal@set.rn.gov.br<br>
                                      </div>
                                      <div class="flex justify-left pb-4 pl-8">
                                         <a href="<?php echo e(asset('docs/orgaosdogoverno/cartasdeservico-set/1 - CS1URT.pdf')); ?>"
                                            target="_blank"><button
                                               class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs"
                                               name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                               Carta de Serviço
                                            </button></a>
                                      </div>
                                   </div>
                                </div>
                             </div>

                             <div class="border-b tab">
                                <div class="border-l-2 border-transparent relative">
                                   <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox"
                                      id="this">
                                   <header
                                      class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label"
                                      for="this">
                                      <span class="text-lightBlue-500 text-sm text-left uppercase" name="texto"
                                         style="font-size: 0.875rem; line-height: 1.25rem;">
                                         SUMAT - Subc. de Mercadorias em Trânsito
                                      </span>
                                      <div
                                         class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                         <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24"
                                            stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                            <polyline points="6 9 12 15 18 9">
                                            </polyline>
                                         </svg>
                                      </div>
                                   </header>
                                   <div class="tab-content">
                                      <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto"
                                         style="font-size: 0.875rem; line-height: 1.25rem;">
                                         <b>Telefone/WhatsApp:</b> 3232-2170/3232- 2172<br>
                                         <b>E-mail:</b> sumat@set.rn.gov.br<br>
                                      </div>
                                      <div class="flex justify-left pb-4 pl-8">
                                         <a href="<?php echo e(asset('docs/orgaosdogoverno/cartasdeservico-set/2 - CSSUMAT.pdf')); ?>"
                                            target="_blank"><button
                                               class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs"
                                               name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                               Carta de Serviço
                                            </button></a>
                                      </div>
                                   </div>
                                </div>
                             </div>

                             <div class="border-b tab">
                                <div class="border-l-2 border-transparent relative">
                                   <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox"
                                      id="this">
                                   <header
                                      class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label"
                                      for="this">
                                      <span class="text-lightBlue-500 text-sm text-left uppercase" name="texto"
                                         style="font-size: 0.875rem; line-height: 1.25rem;">
                                         SUCADI - Subc. de Cadastro e Itinerância Fiscal
                                      </span>
                                      <div
                                         class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                         <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24"
                                            stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                            <polyline points="6 9 12 15 18 9">
                                            </polyline>
                                         </svg>
                                      </div>
                                   </header>
                                   <div class="tab-content">
                                      <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto"
                                         style="font-size: 0.875rem; line-height: 1.25rem;">
                                         <b>Telefone/WhatsApp:</b> 3232-2181/3232-4044<br>
                                         <b>E-mail:</b> sucadi@set.rn.gov.br<br>
                                      </div>
                                      <div class="flex justify-left pb-4 pl-8">
                                         <a href="<?php echo e(asset('docs/orgaosdogoverno/cartasdeservico-set/3 - CSSUCADI.pdf')); ?>"
                                            target="_blank"><button
                                               class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs"
                                               name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                               Carta de Serviço
                                            </button></a>
                                      </div>
                                   </div>
                                </div>
                             </div>

                             <div class="border-b tab">
                                <div class="border-l-2 border-transparent relative">
                                   <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox"
                                      id="this">
                                   <header
                                      class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label"
                                      for="this">
                                      <span class="text-lightBlue-500 text-sm text-left uppercase" name="texto"
                                         style="font-size: 0.875rem; line-height: 1.25rem;">
                                         SUDEFI - Subc. de Controle de Débitos Fiscais
                                      </span>
                                      <div
                                         class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                         <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24"
                                            stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                            <polyline points="6 9 12 15 18 9">
                                            </polyline>
                                         </svg>
                                      </div>
                                   </header>
                                   <div class="tab-content">
                                      <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto"
                                         style="font-size: 0.875rem; line-height: 1.25rem;">
                                         <b>Telefone/WhatsApp:</b> 3232-2190 <br>
                                         <b>E-mail:</b> sudefi@set.rn.gov.br<br>
                                      </div>
                                      <div class="flex justify-left pb-4 pl-8">
                                         <a href="<?php echo e(asset('docs/orgaosdogoverno/cartasdeservico-set/4 - CSSUDEFI.pdf')); ?>"
                                            target="_blank"><button
                                               class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs"
                                               name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                               Carta de Serviço
                                            </button></a>
                                      </div>
                                   </div>
                                </div>
                             </div>

                             <div class="border-b tab">
                                <div class="border-l-2 border-transparent relative">
                                   <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox"
                                      id="this">
                                   <header
                                      class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label"
                                      for="this">
                                      <span class="text-lightBlue-500 text-sm text-left uppercase" name="texto"
                                         style="font-size: 0.875rem; line-height: 1.25rem;">
                                         ITCD – Imposto Transmissão Causa Mortis e Doação(CACE)
                                      </span>
                                      <div
                                         class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                         <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24"
                                            stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                            <polyline points="6 9 12 15 18 9">
                                            </polyline>
                                         </svg>
                                      </div>
                                   </header>
                                   <div class="tab-content">
                                      <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto"
                                         style="font-size: 0.875rem; line-height: 1.25rem;">
                                         <b>Telefone/WhatsApp:</b> 3232-4039 <br>
                                         <b>E-mail:</b> 1urtitcd@set.rn.gov.br<br>
                                      </div>
                                      <div class="flex justify-left pb-4 pl-8">
                                         <a href="<?php echo e(asset('docs/orgaosdogoverno/cartasdeservico-set/5 - CSITCD.pdf')); ?>"
                                            target="_blank"><button
                                               class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs"
                                               name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                               Carta de Serviço
                                            </button></a>
                                      </div>
                                   </div>
                                </div>
                             </div>

                             <div class="border-b tab">
                                <div class="border-l-2 border-transparent relative">
                                   <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox"
                                      id="this">
                                   <header
                                      class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label"
                                      for="this">
                                      <span class="text-lightBlue-500 text-sm text-left uppercase" name="texto"
                                         style="font-size: 0.875rem; line-height: 1.25rem;">
                                         IPVA – Imposto sobre a Propriedade de Veículos Automotores(SUCIVA – CACE) 
                                      </span>
                                      <div
                                         class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                         <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24"
                                            stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                            <polyline points="6 9 12 15 18 9">
                                            </polyline>
                                         </svg>
                                      </div>
                                   </header>
                                   <div class="tab-content">
                                      <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto"
                                         style="font-size: 0.875rem; line-height: 1.25rem;">
                                         <b>Telefone/WhatsApp:</b> 3232-1283 <br>
                                         <b>E-mail:</b> suciva@set.rn.gov.br<br>
                                      </div>
                                      <div class="flex justify-left pb-4 pl-8">
                                         <a href="<?php echo e(asset('docs/orgaosdogoverno/cartasdeservico-set/6 - CSSUCIVA.pdf')); ?>"
                                            target="_blank"><button
                                               class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs"
                                               name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                               Carta de Serviço
                                            </button></a>
                                      </div>
                                   </div>
                                </div>
                             </div>

                             <div class="border-b tab">
                                <div class="border-l-2 border-transparent relative">
                                   <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox"
                                      id="this">
                                   <header
                                      class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label"
                                      for="this">
                                      <span class="text-lightBlue-500 text-sm text-left uppercase" name="texto"
                                         style="font-size: 0.875rem; line-height: 1.25rem;">
                                         PAT – Processo Administrativo Tributário(SUFISE) 
                                      </span>
                                      <div
                                         class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                         <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24"
                                            stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                            <polyline points="6 9 12 15 18 9">
                                            </polyline>
                                         </svg>
                                      </div>
                                   </header>
                                   <div class="tab-content">
                                      <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto"
                                         style="font-size: 0.875rem; line-height: 1.25rem;">
                                         <b>Telefone/WhatsApp:</b> 3232-4070 <br>
                                         <b>E-mail:</b> pat.sufise@set.rn.gov.br<br>
                                      </div>
                                      <div class="flex justify-left pb-4 pl-8">
                                         <a href="<?php echo e(asset('docs/orgaosdogoverno/cartasdeservico-set/7 - CSPAT.pdf')); ?>"
                                            target="_blank"><button
                                               class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs"
                                               name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                               Carta de Serviço
                                            </button></a>
                                      </div>
                                   </div>
                                </div>
                             </div>

                             <div class="border-b tab">
                              <div class="border-l-2 border-transparent relative">
                                 <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox"
                                    id="this">
                                 <header
                                    class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label"
                                    for="this">
                                    <span class="text-lightBlue-500 text-sm text-left" name="texto"
                                       style="font-size: 0.875rem; line-height: 1.25rem;">
                                       URTs
                                    </span>
                                    <div
                                       class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                       <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24"
                                          stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                          <polyline points="6 9 12 15 18 9">
                                          </polyline>
                                       </svg>
                                    </div>
                                 </header>
                                 <div class="tab-content">
                                    <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto"
                                       style="font-size: 0.875rem; line-height: 1.25rem;">
                                       <b>1ª URT – NATAL</b><br>
                                       <b>Endereço:</b> Av.Capitão-Mor Gouveia, 2056 - Cidade da Esperança - Natal – CEP 59070 – 400<br>
                                    </div>
                                    <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto"
                                       style="font-size: 0.875rem; line-height: 1.25rem;">
                                       <b>2ª URT – Sede NOVA CRUZ</b><br>
                                       <b>Endereço:</b> R. Presidente Getúlio Vargas, 20, Centro – Nova Cruz – RN - CEP 59.215-000<br>
                                       <b>Telefones/WhatsApp:</b> (84) 3281- 5925/ (84) 3281-5918<br>
                                       <b>E-mails:</b> protocolo2urt@set.rn.gov.br | plantaofiscal2urt@set.rn.gov.br<br>
                                    </div>
                                    <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto"
                                       style="font-size: 0.875rem; line-height: 1.25rem;">
                                       <b>3ª URT - Sede CURRAIS NOVOS</b><br>
                                       <b>Endereço:</b> Av. Getúlio Vargas, 1008, Currais Novos – RN - CEP 59.380-000<br>
                                       <b>Telefones:</b> (84) 3405- 3619/ (84) 3405- 3618<br>
                                       <b>WhatsApp:</b> (84) 3405-3624<br>
                                       <b>E-mails:</b> protocolo3urt@set.rn.gov.br | plantaofiscal3urt@set.rn.gov.br<br>
                                    </div>
                                    <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto"
                                       style="font-size: 0.875rem; line-height: 1.25rem;">
                                       <b>4ª URT - Sede MACAU</b><br>
                                       <b>Endereço:</b> R. Amaro Cavalcante, 38, Centro, Macau – RN - CEP 59.500 – 000<br>
                                       <b>Telefones:</b> (84) 3521-6466/(84) 3521-6464<br>
                                       <b>E-mails:</b> protocolo4urt@set.rn.gov.br | plantaofiscal4urt@set.rn.gov.br<br>
                                    </div>
                                    <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto"
                                       style="font-size: 0.875rem; line-height: 1.25rem;">
                                       <b>5ª URT - Sede CAICÓ</b><br>
                                       <b>Endereço:</b> Av. Cel. Martiniano, 723, Centro, Caicó – RN - CEP 59.300-000<br>
                                       <b>Telefone:</b> (84) 3421- 6023<br>
                                       <b>WhatsApp:</b> (84) 3421-6012/3421-6010<br>
                                       <b>E-mails:</b> protocolo5urt@set.rn.gov.br | plantaofiscal5urt@set.rn.gov.br<br>
                                    </div>
                                    <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto"
                                       style="font-size: 0.875rem; line-height: 1.25rem;">
                                       <b>6ª URT - Sede MOSSORÓ</b><br>
                                       <b>Endereço:</b> Praça Vigário Antônio Joaquim, s/n, Centro, Mossoró – RN - CEP 59.600 – 520<br>
                                       <b>Telefone:</b> (84) 3315- 3771/(84) 3315-3558<br>
                                       <b>E-mails:</b> protocolo6urt@set.rn.gov.br | plantaofiscal6urt@set.rn.gov.br<br>
                                       <b>WhatsApps:</b> Plantão Fiscal 3315-3770/3315-3777<br>
                                       Protocolo 3315-3558 || Diretoria 3315-3771<br>
                                       Cadastro NUCAD 3315-3774/3315-3775 || Parcelamento NUDEF 3315-3778<br>
                                       Simples Nacional 3315-3779 || Fiscalização NUFIS 3315-3554<br>
                                       Automação Comercial NFCE 3315-3555 || ITCD 3315-3557 || PAT 3315-3548<br>
                                    </div>
                                    <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto"
                                       style="font-size: 0.875rem; line-height: 1.25rem;">
                                       <b>7ª URT - Sede PAU DOS FERROS</b><br>
                                       <b>Endereço:</b> Praça da Matriz, 225, Centro, Pau dos Ferros – RN - CEP 59.900 – 000<br>
                                       <b>Telefone:</b> (84) 3351-9651<br>
                                       <b>WhatsApp:</b> (84) 3351-2017<br>
                                       <b>E-mails:</b> protocolo7urt@set.rn.gov.br | plantaofiscal7urt@set.rn.gov.br<br>
                                    </div>
                                    <div class="flex justify-left pb-4 pl-8">
                                       <a href="<?php echo e(asset('docs/orgaosdogoverno/cartasdeservico-set/9 - CSURTS.pdf')); ?>"
                                          target="_blank"><button
                                             class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs"
                                             name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                             Carta de Serviço
                                          </button></a>
                                    </div>
                                 </div>
                              </div>
                           </div>

                             <div class="border-b tab">
                              <div class="border-l-2 border-transparent relative">
                                 <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox"
                                    id="this">
                                 <header
                                    class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label"
                                    for="this">
                                    <span class="text-lightBlue-500 text-sm text-left uppercase" name="texto"
                                       style="font-size: 0.875rem; line-height: 1.25rem;">
                                       GABINETE DO SECRETÁRIO 
                                    </span>
                                    <div
                                       class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                       <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24"
                                          stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                          <polyline points="6 9 12 15 18 9">
                                          </polyline>
                                       </svg>
                                    </div>
                                 </header>
                                 <div class="tab-content">
                                    <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto"
                                       style="font-size: 0.875rem; line-height: 1.25rem;">
                                       <b>Telefone:</b>  3232-2196 <br>
                                       <b>E-mail:</b> gabineteset@set.rn.gov.br<br>
                                    </div>
                                    <div class="flex justify-left pb-4 pl-8">
                                       <a href="<?php echo e(asset('docs/orgaosdogoverno/cartasdeservico-set/10 - CSGABINETE.pdf')); ?>"
                                          target="_blank"><button
                                             class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs"
                                             name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                             Carta de Serviço
                                          </button></a>
                                    </div>
                                 </div>
                              </div>
                           </div>

                           <div class="border-b tab">
                              <div class="border-l-2 border-transparent relative">
                                 <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox"
                                    id="this">
                                 <header
                                    class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label"
                                    for="this">
                                    <span class="text-lightBlue-500 text-sm text-left uppercase" name="texto"
                                       style="font-size: 0.875rem; line-height: 1.25rem;">
                                       PROTOCOLO GERAL 
                                    </span>
                                    <div
                                       class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                       <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24"
                                          stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                          <polyline points="6 9 12 15 18 9">
                                          </polyline>
                                       </svg>
                                    </div>
                                 </header>
                                 <div class="tab-content">
                                    <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto"
                                       style="font-size: 0.875rem; line-height: 1.25rem;">
                                       <b>E-mail:</b> protocoloset@set.rn.gov.br<br>
                                    </div>
                                    <div class="flex justify-left pb-4 pl-8">
                                       <a href="<?php echo e(asset('docs/orgaosdogoverno/cartasdeservico-set/11 - CSPROTOCOLO.pdf')); ?>"
                                          target="_blank"><button
                                             class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs"
                                             name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                             Carta de Serviço
                                          </button></a>
                                    </div>
                                 </div>
                              </div>
                           </div>

                           <div class="border-b tab">
                              <div class="border-l-2 border-transparent relative">
                                 <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox"
                                    id="this">
                                 <header
                                    class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label"
                                    for="this">
                                    <span class="text-lightBlue-500 text-sm text-left uppercase" name="texto"
                                       style="font-size: 0.875rem; line-height: 1.25rem;">
                                       COFIC – COORDENADORIA DE INTEGRAÇÃO FISCO-CONTRIBUINTE 
                                    </span>
                                    <div
                                       class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                       <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24"
                                          stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                          <polyline points="6 9 12 15 18 9">
                                          </polyline>
                                       </svg>
                                    </div>
                                 </header>
                                 <div class="tab-content">
                                    <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto"
                                       style="font-size: 0.875rem; line-height: 1.25rem;">
                                       <b>WhatsApp:</b> 3232-2033 <br>
                                       <b>E-mail:</b> coficset@set.rn.gov.br<br>
                                    </div>
                                    <div class="flex justify-left pb-4 pl-8">
                                       <a href="<?php echo e(asset('docs/orgaosdogoverno/cartasdeservico-set/12 - CSCOFIC.pdf')); ?>"
                                          target="_blank"><button
                                             class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs"
                                             name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                             Carta de Serviço
                                          </button></a>
                                    </div>
                                 </div>
                              </div>
                           </div>

                           <div class="border-b tab">
                              <div class="border-l-2 border-transparent relative">
                                 <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox"
                                    id="this">
                                 <header
                                    class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label"
                                    for="this">
                                    <span class="text-lightBlue-500 text-sm text-left uppercase" name="texto"
                                       style="font-size: 0.875rem; line-height: 1.25rem;">
                                       COFIS – COORDENADORIA DE FISCALIZAÇÃO 
                                    </span>
                                    <div
                                       class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                       <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24"
                                          stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                          <polyline points="6 9 12 15 18 9">
                                          </polyline>
                                       </svg>
                                    </div>
                                 </header>
                                 <div class="tab-content">
                                    <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto"
                                       style="font-size: 0.875rem; line-height: 1.25rem;">
                                       <b>WhatsApp:</b> 3232-2062 <br>
                                       <b>E-mail:</b> cofisset@set.rn.gov.br<br>
                                    </div>
                                    <div class="flex justify-left pb-4 pl-8">
                                       <a href="<?php echo e(asset('docs/orgaosdogoverno/cartasdeservico-set/13 - CSCOFIS.pdf')); ?>"
                                          target="_blank"><button
                                             class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs"
                                             name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                             Carta de Serviço
                                          </button></a>
                                    </div>
                                 </div>
                              </div>
                           </div>

                           <div class="border-b tab">
                              <div class="border-l-2 border-transparent relative">
                                 <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox"
                                    id="this">
                                 <header
                                    class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label"
                                    for="this">
                                    <span class="text-lightBlue-500 text-sm text-left uppercase" name="texto"
                                       style="font-size: 0.875rem; line-height: 1.25rem;">
                                       COEF – COORDENADORIA DE EDUCAÇÃO FISCAL 
                                    </span>
                                    <div
                                       class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                       <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24"
                                          stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                          <polyline points="6 9 12 15 18 9">
                                          </polyline>
                                       </svg>
                                    </div>
                                 </header>
                                 <div class="tab-content">
                                    <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto"
                                       style="font-size: 0.875rem; line-height: 1.25rem;">
                                       <b>WhatsApp:</b> 3232-7077 <br>
                                       <b>E-mail:</b> atendimento.notapotiguar@set.rn.gov.br<br>
                                    </div>
                                    <div class="flex justify-left pb-4 pl-8">
                                       <a href="<?php echo e(asset('docs/orgaosdogoverno/cartasdeservico-set/14 - CSCOEF.pdf')); ?>"
                                          target="_blank"><button
                                             class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs"
                                             name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                             Carta de Serviço
                                          </button></a>
                                    </div>
                                 </div>
                              </div>
                           </div>

                           <div class="border-b tab">
                              <div class="border-l-2 border-transparent relative">
                                 <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox"
                                    id="this">
                                 <header
                                    class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label"
                                    for="this">
                                    <span class="text-lightBlue-500 text-sm text-left uppercase" name="texto"
                                       style="font-size: 0.875rem; line-height: 1.25rem;">
                                       CACE – COORDENADORIA DE ARRECADAÇÃO 
                                    </span>
                                    <div
                                       class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                       <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24"
                                          stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                          <polyline points="6 9 12 15 18 9">
                                          </polyline>
                                       </svg>
                                    </div>
                                 </header>
                                 <div class="tab-content">
                                    <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto"
                                       style="font-size: 0.875rem; line-height: 1.25rem;">
                                       <b>WhatsApp:</b> 3232-2092 <br>
                                       <b>E-mail:</b> cace@set.rn.gov.br<br>
                                    </div>
                                    <div class="flex justify-left pb-4 pl-8">
                                       <a href="<?php echo e(asset('docs/orgaosdogoverno/cartasdeservico-set/15 - CSCACE.pdf')); ?>"
                                          target="_blank"><button
                                             class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs"
                                             name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                             Carta de Serviço
                                          </button></a>
                                    </div>
                                 </div>
                              </div>
                           </div>

                           <div class="border-b tab">
                              <div class="border-l-2 border-transparent relative">
                                 <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox"
                                    id="this">
                                 <header
                                    class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label"
                                    for="this">
                                    <span class="text-lightBlue-500 text-sm text-left uppercase" name="texto"
                                       style="font-size: 0.875rem; line-height: 1.25rem;">
                                       CEJUSC - CENTRO JUDICIÁRIO DE SOLUÇÃO CONSENSUAL DE CONFLITOS 
                                    </span>
                                    <div
                                       class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                       <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24"
                                          stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                          <polyline points="6 9 12 15 18 9">
                                          </polyline>
                                       </svg>
                                    </div>
                                 </header>
                                 <div class="tab-content">
                                    <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto"
                                       style="font-size: 0.875rem; line-height: 1.25rem;">
                                       <b>E-mail:</b> cejuscfiscal@set.rn.gov.br<br>
                                    </div>
                                    <div class="flex justify-left pb-4 pl-8">
                                       <a href="<?php echo e(asset('docs/orgaosdogoverno/cartasdeservico-set/16 - CSCEJUSC.pdf')); ?>"
                                          target="_blank"><button
                                             class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs"
                                             name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                             Carta de Serviço
                                          </button></a>
                                    </div>
                                 </div>
                              </div>
                           </div>

                           <div class="border-b tab">
                              <div class="border-l-2 border-transparent relative">
                                 <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox"
                                    id="this">
                                 <header
                                    class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label"
                                    for="this">
                                    <span class="text-lightBlue-500 text-sm text-left uppercase" name="texto"
                                       style="font-size: 0.875rem; line-height: 1.25rem;">
                                       CAT – COORDENADORIA DE TRIBUTAÇÃO E ASSESSORIA TÉCNICA 
                                    </span>
                                    <div
                                       class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                       <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24"
                                          stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                          <polyline points="6 9 12 15 18 9">
                                          </polyline>
                                       </svg>
                                    </div>
                                 </header>
                                 <div class="tab-content">
                                    <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto"
                                       style="font-size: 0.875rem; line-height: 1.25rem;">
                                       <b>WhatsApp:</b> 3232 2176 <br>
                                       <b>E-mail:</b> catset@set.rn.gov.br<br>
                                    </div>
                                    <div class="flex justify-left pb-4 pl-8">
                                       <a href="<?php echo e(asset('docs/orgaosdogoverno/cartasdeservico-set/17 - CSCAT.pdf')); ?>"
                                          target="_blank"><button
                                             class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs"
                                             name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                             Carta de Serviço
                                          </button></a>
                                    </div>
                                 </div>
                              </div>
                           </div>

                           <div class="border-b tab">
                              <div class="border-l-2 border-transparent relative">
                                 <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox"
                                    id="this">
                                 <header
                                    class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label"
                                    for="this">
                                    <span class="text-lightBlue-500 text-sm text-left uppercase" name="texto"
                                       style="font-size: 0.875rem; line-height: 1.25rem;">
                                       CODIN – COORDENADORIA DE INFORMÁTICA 
                                    </span>
                                    <div
                                       class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                       <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24"
                                          stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                          <polyline points="6 9 12 15 18 9">
                                          </polyline>
                                       </svg>
                                    </div>
                                 </header>
                                 <div class="tab-content">
                                    <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto"
                                       style="font-size: 0.875rem; line-height: 1.25rem;">
                                       <b>WhatsApp:</b> 3232-2017<br>
                                       <b>E-mails:</b> codinset@set.rn.gov.br e nivel1@set.rn.gov.br<br>
                                    </div>
                                    <div class="flex justify-left pb-4 pl-8">
                                       <a href="<?php echo e(asset('docs/orgaosdogoverno/cartasdeservico-set/18 - CSCODIN.ods')); ?>"
                                          target="_blank"><button
                                             class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs"
                                             name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                             Carta de Serviço
                                          </button></a>
                                    </div>
                                 </div>
                              </div>
                           </div>

                           <div class="border-b tab">
                              <div class="border-l-2 border-transparent relative">
                                 <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox"
                                    id="this">
                                 <header
                                    class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label"
                                    for="this">
                                    <span class="text-lightBlue-500 text-sm text-left uppercase" name="texto"
                                       style="font-size: 0.875rem; line-height: 1.25rem;">
                                       COJUP – COORDENADORIA DE JULGAMENTO DE PROCESSOS 
                                    </span>
                                    <div
                                       class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                       <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24"
                                          stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                          <polyline points="6 9 12 15 18 9">
                                          </polyline>
                                       </svg>
                                    </div>
                                 </header>
                                 <div class="tab-content">
                                    <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto"
                                       style="font-size: 0.875rem; line-height: 1.25rem;">
                                       <b>WhatsApp:</b>  3232-2045<br>
                                       <b>E-mail:</b> cojup@set.rn.gov.br<br>
                                    </div>
                                 </div>
                              </div>
                           </div>

                           <div class="border-b tab">
                              <div class="border-l-2 border-transparent relative">
                                 <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox"
                                    id="this">
                                 <header
                                    class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label"
                                    for="this">
                                    <span class="text-lightBlue-500 text-sm text-left uppercase" name="texto"
                                       style="font-size: 0.875rem; line-height: 1.25rem;">
                                       CRF – CONSELHO DE RECURSO FISCAIS 
                                    </span>
                                    <div
                                       class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                       <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24"
                                          stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                          <polyline points="6 9 12 15 18 9">
                                          </polyline>
                                       </svg>
                                    </div>
                                 </header>
                                 <div class="tab-content">
                                    <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto"
                                       style="font-size: 0.875rem; line-height: 1.25rem;">
                                       <b>WhatsApp:</b>  3232-2037<br>
                                       <b>E-mail:</b> crf@set.rn.gov.br<br>
                                    </div>
                                    <div class="flex justify-left pb-4 pl-8">
                                       <a href="<?php echo e(asset('docs/orgaosdogoverno/cartasdeservico-set/19 - CSCRF.pdf')); ?>"
                                          target="_blank"><button
                                             class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs"
                                             name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                             Carta de Serviço
                                          </button></a>
                                    </div>
                                 </div>
                              </div>
                           </div>

                           <div class="border-b tab">
                              <div class="border-l-2 border-transparent relative">
                                 <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox"
                                    id="this">
                                 <header
                                    class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label"
                                    for="this">
                                    <span class="text-lightBlue-500 text-sm text-left uppercase" name="texto"
                                       style="font-size: 0.875rem; line-height: 1.25rem;">
                                       COGEF – CORREGEDORIA GERAL DO FISCO 
                                    </span>
                                    <div
                                       class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                       <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24"
                                          stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                          <polyline points="6 9 12 15 18 9">
                                          </polyline>
                                       </svg>
                                    </div>
                                 </header>
                                 <div class="tab-content">
                                    <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto"
                                       style="font-size: 0.875rem; line-height: 1.25rem;">
                                       <b>WhatsApp:</b> 3232-2096<br>
                                       <b>E-mail:</b> corregedoria@set.rn.gov.br<br>
                                    </div>
                                 </div>
                              </div>
                           </div>

                           <div class="border-b tab">
                              <div class="border-l-2 border-transparent relative">
                                 <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox"
                                    id="this">
                                 <header
                                    class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label"
                                    for="this">
                                    <span class="text-lightBlue-500 text-sm text-left uppercase" name="texto"
                                       style="font-size: 0.875rem; line-height: 1.25rem;">
                                       SUSCOMEX – SUBCOORDENADORIA DE SUBSTITUIÇÃO TRIBUTÁRIA E COMÉRCIO EXTERIOR 
                                    </span>
                                    <div
                                       class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                       <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24"
                                          stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                          <polyline points="6 9 12 15 18 9">
                                          </polyline>
                                       </svg>
                                    </div>
                                 </header>
                                 <div class="tab-content">
                                    <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto"
                                       style="font-size: 0.875rem; line-height: 1.25rem;">
                                       <b>WhatsApp:</b> 3232-2981<br>
                                       <b>E-mails:</b> suscomex@set.rn.gov.br e comercioexterior@set.rn.gov.br<br>
                                    </div>
                                    <div class="flex justify-left pb-4 pl-8">
                                       <a href="<?php echo e(asset('docs/orgaosdogoverno/cartasdeservico-set/20 - CSSUSCOMEX.pdf')); ?>"
                                          target="_blank"><button
                                             class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs"
                                             name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                             Carta de Serviço
                                          </button></a>
                                    </div>
                                 </div>
                              </div>
                           </div>


                        </div>
                    </section>
                </main>

            </div>

        </div>
    </div>
</section>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/portaltransparencianewrn/resources/views/orgaos-do-governo/cartasdeservico-set.blade.php ENDPATH**/ ?>