<?php $__env->startSection('title'); ?>
- Detalhe do Documento
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <h1 class="text-lg text-gray-900 tracking-widest font-medium title-font uppercase text-center pb-2" name="texto" style="font-size: 1.125rem; line-height: 2rem;">
       <i class="far fa-file-alt"></i> Detalhamento do Documento
    </h1>

    <ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
       <li class="inline-flex items-center">
          <a href="<?php echo e(url('/')); ?>">Inicio</a>
          <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
             <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd"></path>
          </svg>
       </li>
       <li class="inline-flex items-center">
          <a href="<?php echo e(route('despesas')); ?>">Despesas</a>
          <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
             <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd"></path>
          </svg>
       </li>
       <li class="inline-flex items-center">
          <a href="#" class="text-teal-400">Documentos</a>
       </li>
    </ul>

    <hr class="border-b-2 border-lightBlue-300 mb-12 mx-4">

    <div class="pb-4">
        
        <?php if($fase == 'vportal_notaempenho_gastosdiretos'): ?>
        
           <div class="bg-white shadow overflow-hidden sm:rounded-lg">
                <?php $__currentLoopData = $detalhesdocumento; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detalhes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="px-4 py-5 sm:px-6">
                         <h3 class="text-lg leading-6 font-medium text-gray-900" name="texto" style="font-size: 1.125rem; line-height: 1.75rem;">
                            Detalhe do Documento
                         </h3>
                         <p class="mt-1 max-w-2xl text-sm text-gray-500" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                            Detalhamento e informações gerais.
                         </p>
                    </div>
               
                    <div class="border-t border-gray-200">
                         <dl>
                            <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                               <dt class="text-sm font-medium text-gray-500" name="texto"
                                  style="font-size: 0.875rem; line-height: 1.25rem;">
                                  Fase
                               </dt>
                               <dd class="mt-1 text-lg text-gray-500 sm:mt-0 sm:col-span-2 uppercase font-semibold" name="texto"
                                  style="font-size: 1.125rem; line-height: 1.75rem;">
                                  <?php echo e($nomefase); ?>

                               </dd>
                            </div>
                            <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                               <dt class="text-sm font-medium text-gray-500" name="texto"
                                  style="font-size: 0.875rem; line-height: 1.25rem;">
                                  Documento
                               </dt>
                               <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                                  style="font-size: 0.875rem; line-height: 1.25rem;">
                                  <?php echo e($detalhes->numempenho); ?>

                               </dd>
                            </div>
                            <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                               <dt class="text-sm font-medium text-gray-500" name="texto"
                                  style="font-size: 0.875rem; line-height: 1.25rem;">
                                  Tipo de Documento
                               </dt>
                               <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                                  style="font-size: 0.875rem; line-height: 1.25rem;">
                                  <?php echo e($detalhes->especieempenho); ?> 
                               </dd>
                            </div>
                            <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                               <dt class="text-sm font-medium text-gray-500" name="texto"
                                  style="font-size: 0.875rem; line-height: 1.25rem;">
                                  Espécie de Empenho
                               </dt>
                               <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                                  style="font-size: 0.875rem; line-height: 1.25rem;">
                                  <?php echo e($detalhes->tipoempenho); ?> 
                               </dd>
                            </div>
                            <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                               <dt class="text-sm font-medium text-gray-500" name="texto"
                                  style="font-size: 0.875rem; line-height: 1.25rem;">
                                  Data (Emissao)
                               </dt>
                               <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                                  style="font-size: 0.875rem; line-height: 1.25rem;">
                                  <?php echo e(date('d/m/Y', strtotime($detalhes->dtlancamento))); ?> 
                               </dd>
                            </div>
                            <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                               <dt class="text-sm font-medium text-gray-500" name="texto"
                                  style="font-size: 0.875rem; line-height: 1.25rem;">
                                  Órgão
                               </dt>
                               <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                                  style="font-size: 0.875rem; line-height: 1.25rem;">
                                  <?php echo e($detalhes->txtdescricaoorgao); ?>

                               </dd>
                            </div>
                            <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                               <dt class="text-sm font-medium text-gray-500" name="texto"
                                  style="font-size: 0.875rem; line-height: 1.25rem;">
                                  Unidade Gestora
                               </dt>
                               <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                                  style="font-size: 0.875rem; line-height: 1.25rem;">
                                  <?php echo e($detalhes->txtdescricaounidade); ?>

                               </dd>
                            </div>
                            <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                               <dt class="text-sm font-medium text-gray-500" name="texto"
                                  style="font-size: 0.875rem; line-height: 1.25rem;">
                                  Favorecido
                               </dt>
                               <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                                  style="font-size: 0.875rem; line-height: 1.25rem;">
                                  <?php echo e($detalhes->txtnomecredor); ?>

                               </dd>
                            </div>
                            <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                               <dt class="text-sm font-medium text-gray-500" name="texto"
                                  style="font-size: 0.875rem; line-height: 1.25rem;">
                                  CNPJ/CPF/IG
                               </dt>
                               <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                                  style="font-size: 0.875rem; line-height: 1.25rem;">
                                  <?php echo e($detalhes->codcredor); ?>

                               </dd>
                            </div>
                            <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                               <dt class="text-sm font-medium text-gray-500" name="texto"
                                  style="font-size: 0.875rem; line-height: 1.25rem;">
                                  Valor
                               </dt>
                               <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                                  style="font-size: 0.875rem; line-height: 1.25rem;">
                                  R$ <?php echo e(number_format($detalhes->vlrpagamento, 2, ',', '.')); ?>

                               </dd>
                            </div>
                            <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                               <dt class="text-sm font-medium text-gray-500" name="texto"
                                  style="font-size: 0.875rem; line-height: 1.25rem;">
                                  Esfera
                               </dt>
                               <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                                  style="font-size: 0.875rem; line-height: 1.25rem;">
                                  <?php echo e($detalhes->esfera); ?>

                               </dd>
                            </div>
                            <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                               <dt class="text-sm font-medium text-gray-500" name="texto"
                                  style="font-size: 0.875rem; line-height: 1.25rem;">
                                  Fonte de Recurso
                               </dt>
                               <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                                  style="font-size: 0.875rem; line-height: 1.25rem;">
                                  <?php echo e($detalhes->txtdescricaofonterecurso); ?>

                               </dd>
                            </div>
                            <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                               <dt class="text-sm font-medium text-gray-500" name="texto"
                                  style="font-size: 0.875rem; line-height: 1.25rem;">
                                  Função
                               </dt>
                               <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                                  style="font-size: 0.875rem; line-height: 1.25rem;">
                                  <?php echo e($detalhes->txtdescricaofuncao); ?>

                               </dd>
                            </div>
                            <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                               <dt class="text-sm font-medium text-gray-500" name="texto"
                                  style="font-size: 0.875rem; line-height: 1.25rem;">
                                  Subfunção
                               </dt>
                               <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                                  style="font-size: 0.875rem; line-height: 1.25rem;">
                                  <?php echo e($detalhes->txtdescricaosubfuncao); ?>

                               </dd>
                            </div>
                            <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                               <dt class="text-sm font-medium text-gray-500" name="texto"
                                  style="font-size: 0.875rem; line-height: 1.25rem;">
                                  Programa
                               </dt>
                               <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                                  style="font-size: 0.875rem; line-height: 1.25rem;">
                                  <?php echo e($detalhes->txtdescricaoprograma); ?>

                               </dd>
                            </div>
                            <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                               <dt class="text-sm font-medium text-gray-500" name="texto"
                                  style="font-size: 0.875rem; line-height: 1.25rem;">
                                  Ação
                               </dt>
                               <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                                  style="font-size: 0.875rem; line-height: 1.25rem;">
                                  <?php echo e($detalhes->txtdescricaoacao); ?>

                               </dd>
                            </div>

                      <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                         <dt class="text-sm font-medium text-gray-500" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                            Modalidade de Aplicação
                         </dt>
                         <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                            style="font-size: 0.875rem; line-height: 1.25rem;">
                            <?php echo e($detalhes->txtdescricaomodalidadeaplicacao); ?>

                         </dd>
                      </div>
                      <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                         <dt class="text-sm font-medium text-gray-500" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                            Elemento de despesa
                         </dt>
                         <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                            style="font-size: 0.875rem; line-height: 1.25rem;">
                            <?php echo e($detalhes->txtdescricaoelementodespesa); ?>

                         </dd>
                      </div>
                      <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                         <dt class="text-sm font-medium text-gray-500" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                            Modalidade de Licitação
                         </dt>
                         <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                            style="font-size: 0.875rem; line-height: 1.25rem;">
                            <?php echo e($detalhes->txtdescricaomodalidadelicitacao); ?>

                         </dd>
                      </div>
                      <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                         <dt class="text-sm font-medium text-gray-500" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                            Observação do documento
                         </dt>
                         <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                            style="font-size: 0.875rem; line-height: 1.25rem;">
                            <?php echo e($detalhes->txtobservacao); ?>

                         </dd>
                      </div>
                      <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                         <dt class="text-sm font-medium text-gray-500" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                            Processo Nº
                         </dt>
                         <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                            style="font-size: 0.875rem; line-height: 1.25rem;">
                            <?php echo e($detalhes->numprocesso); ?>

                         </dd>
                      </div>
                      </dl>
                   </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
           </div>

        <?php elseif($fase == 'vportal_liquidacaodespesa_gastosdiretos'): ?>
        
            <div class="bg-white shadow overflow-hidden sm:rounded-lg">
               <?php $__currentLoopData = $detalhesdocumento; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detalhes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
               <div class="px-4 py-5 sm:px-6">
                  <h3 class="text-lg leading-6 font-medium text-gray-900" name="texto"
                     style="font-size: 1.125rem; line-height: 1.75rem;">
                     Detalhe do Documento
                  </h3>
                  <p class="mt-1 max-w-2xl text-sm text-gray-500" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                     Detalhamento e informações gerais.
                  </p>
               </div>
               <div class="border-t border-gray-200">
                  <dl>
                     <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                        <dt class="text-sm font-medium text-gray-500" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           Fase
                        </dt>
                        <dd class="mt-1 text-lg text-gray-500 sm:mt-0 sm:col-span-2 uppercase font-semibold" name="texto"
                           style="font-size: 1.125rem; line-height: 1.75rem;">
                           <?php echo e($nomefase); ?>

                        </dd>
                     </div>
                     <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6" name="texto"
                        style="font-size: 0.875rem; line-height: 1.25rem;">
                        <dt class="text-sm font-medium text-gray-500">
                           Documento
                        </dt>
                        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           <?php echo e($detalhes->numnotalancamento); ?>

                        </dd>
                     </div>
                     <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6" name="texto"
                        style="font-size: 0.875rem; line-height: 1.25rem;">
                        <dt class="text-sm font-medium text-gray-500">
                           Tipo de Documento
                        </dt>
                        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           Nota de Lançamento
                        </dd>
                     </div>
                     <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                        <dt class="text-sm font-medium text-gray-500" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           Data
                        </dt>
                        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           <?php echo e(date('d/m/Y', strtotime($detalhes->dtemissao))); ?>

                        </dd>
                     </div>
                     <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                        <dt class="text-sm font-medium text-gray-500" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           Órgão
                        </dt>
                        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           <?php echo e($detalhes->txtdescricaoorgao); ?>

                        </dd>
                     </div>
                     <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                        <dt class="text-sm font-medium text-gray-500" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           Unidade Gestora
                        </dt>
                        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           <?php echo e($detalhes->txtdescricaounidade); ?>

                        </dd>
                     </div>
                     <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                        <dt class="text-sm font-medium text-gray-500" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           Favorecido
                        </dt>
                        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           <?php echo e($detalhes->txtdescricaocredor); ?>

                        </dd>
                     </div>
                     <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                        <dt class="text-sm font-medium text-gray-500" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           CNPJ/CPF/IG
                        </dt>
                        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           <?php echo e($detalhes->codcredor); ?>

                        </dd>
                     </div>
                     <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                        <dt class="text-sm font-medium text-gray-500" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           Valor
                        </dt>
                        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           R$ <?php echo e(number_format($detalhes->vlrpagamento, 2, ',', '.')); ?>

                        </dd>
                     </div>
                     <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                        <dt class="text-sm font-medium text-gray-500" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           Observação do documento
                        </dt>
                        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           <?php echo e($detalhes->txtobservacao); ?>

                        </dd>
                     </div>
                     <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                        <dt class="text-sm font-medium text-gray-500" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           Processo Nº
                        </dt>
                        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">
                           <?php echo e($detalhes->numprocesso); ?>

                        </dd>
                     </div>
                  </dl>
               </div>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>

        <?php else: ?>
        
            <div class="bg-white shadow overflow-hidden sm:rounded-lg">
       <?php if($detalhesdocumento->isEmpty()): ?>
                
           <?php $__currentLoopData = $detalhesrestoapagar; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detalhesrp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="px-4 py-5 sm:px-6">
              <h3 class="text-lg leading-6 font-medium text-gray-900" name="texto"
                 style="font-size: 1.125rem; line-height: 1.75rem;">
                 Detalhe do Documento
              </h3>
              <p class="mt-1 max-w-2xl text-sm text-gray-500" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                 Detalhamento e informações gerais.
              </p>
           </div>
                <div class="border-t border-gray-200">
              <dl>
                 <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       Fase
                    </dt>
                    <dd class="mt-1 text-lg text-gray-500 sm:mt-0 sm:col-span-2 uppercase font-semibold" name="texto"
                       style="font-size: 1.125rem; line-height: 1.75rem;">
                       <?php echo e($nomefase); ?>

                    </dd>
                 </div>
                 <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       Documento
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       <?php echo e($detalhesrp->preparacaopagamento); ?>

                    </dd>
                 </div>
                 <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       Tipo de Documento
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       Preparação Pagamento
                    </dd>
                 </div>
                 <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       Data
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       <?php echo e(date('d/m/Y', strtotime($detalhesrp->dtemissao))); ?>

                    </dd>
                 </div>
                 <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       Órgão
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       <?php echo e($detalhesrp->txtdescricaoorgao); ?>

                    </dd>
                 </div>
                 <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       Unidade Gestora
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       <?php echo e($detalhesrp->txtdescricaounidade); ?>

                    </dd>
                 </div>
                 <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       Favorecido
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       <?php echo e($detalhesrp->txtcredor); ?>

                    </dd>
                 </div>
                 <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       CNPJ/CPF/IG
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       <?php echo e($detalhesrp->codcredor); ?>

                    </dd>
                 </div>
                 <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       Valor (Restos a Pagar)
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       R$ <?php echo e(number_format($detalhesrp->vlrpagamentorp, 2, ',', '.')); ?>

                    </dd>
                 </div>
                 <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       Observação do documento
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       <?php echo e($detalhesrp->txtobservacao); ?>

                    </dd>
                 </div>
                 <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       Processo Nº
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       <?php echo e($detalhesrp->numprocesso); ?>

                    </dd>
                 </div>
              </dl>
           </div>
           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                
       <?php else: ?>
                
           <?php $__currentLoopData = $detalhesdocumento; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detalhes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="px-4 py-5 sm:px-6">
              <h3 class="text-lg leading-6 font-medium text-gray-900" name="texto"
                 style="font-size: 1.125rem; line-height: 1.75rem;">
                 Detalhe do Documento
              </h3>
              <p class="mt-1 max-w-2xl text-sm text-gray-500" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                 Detalhamento e informações gerais.
              </p>
           </div>
                <div class="border-t border-gray-200">
              <dl>
                 <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       Fase
                    </dt>
                    <dd class="mt-1 text-lg text-gray-500 sm:mt-0 sm:col-span-2 uppercase font-semibold" name="texto"
                       style="font-size: 1.125rem; line-height: 1.75rem;">
                       <?php echo e($nomefase); ?>

                    </dd>
                 </div>
                 <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       Documento
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       <?php echo e($detalhes->preparacaopagamento); ?>

                    </dd>
                 </div>
                 <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       Tipo de Documento
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       Preparação Pagamento
                    </dd>
                 </div>
                 <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       Data
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       <?php echo e(date('d/m/Y', strtotime($detalhes->dtemissao))); ?>

                    </dd>
                 </div>
                 <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       Órgão
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       <?php echo e($detalhes->txtdescricaoorgao); ?>

                    </dd>
                 </div>
                 <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       Unidade Gestora
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       <?php echo e($detalhes->txtdescricaounidade); ?>

                    </dd>
                 </div>
                 <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       Favorecido
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       <?php echo e($detalhes->txtdescricaocredor); ?>

                    </dd>
                 </div>
                 <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       CNPJ/CPF/IG
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       <?php echo e($detalhes->codcredor); ?>

                    </dd>
                 </div>
                 <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       Valor (Exercício Corrente)
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       R$ <?php echo e(number_format($detalhes->vlrpagamento, 2, ',', '.')); ?>

                    </dd>
                 </div>
                 <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       Observação do documento
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       <?php echo e($detalhes->txtobservacao); ?>

                    </dd>
                 </div>
                 <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       Processo Nº
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2" name="texto"
                       style="font-size: 0.875rem; line-height: 1.25rem;">
                       <?php echo e($detalhes->numprocesso); ?>

                    </dd>
                 </div>
              </dl>
           </div>
           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                
       <?php endif; ?>
    </div>
        
        <?php endif; ?>
    </div>

    <div class="alert flex flex-row items-center bg-blue-200 p-1 rounded">
       <div
          class="alert-icon flex items-center bg-blue-100 border-2 border-blue-500 justify-center flex-shrink-0 rounded-full">
          <span class="text-blue-500">
             <svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
                <path fill-rule="evenodd"
                   d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z"
                   clip-rule="evenodd"></path>
             </svg>
          </span>
       </div>
       <div class="alert-content ml-4">
          <div class="alert-description text-xs text-blue-600" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
             <span class="font-semibold">Dados atualizados em:</span> <?php echo e(date("d/m/Y")); ?> | <span class="font-semibold">Fonte:</span>
             SIGEF - SISTEMA INTEGRADO DE PLANEJAMENTO E GESTÃO FISCAL
          </div>
       </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/portaltransparencianewrn/resources/views/despesas/gastos-diretos/detalhes-documento.blade.php ENDPATH**/ ?>