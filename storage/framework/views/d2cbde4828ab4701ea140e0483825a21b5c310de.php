<?php $__env->startSection('title'); ?>
- Serviços CONTROL
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<section class="text-gray-600 body-font">
    <div class="container px-5 mx-auto">
        <div class="flex flex-col text-center w-full">

            <h1 class="text-lg text-gray-900 tracking-widest font-medium title-font pb-2" name="texto"
                style="font-size: 1.125rem; line-height: 2rem;">
                <i class="fas fa-cogs"></i>
                Serviços CONTROL
            </h1>

            

            <ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto"
                style="font-size: 0.875rem; line-height: 1.25rem;">
                <li class="inline-flex items-center">
                    <a href="<?php echo e(url('/')); ?>">Início</a>
                    <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                        <path fill-rule="evenodd"
                            d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                            clip-rule="evenodd"></path>
                    </svg>
                </li>
                <li class="inline-flex items-center">
                    <a href="#" class="text-teal-400">
                        Serviços CONTROL
                    </a>
                </li>
            </ul>

            <hr class="border-b-2 border-lightBlue-300 mb-6 mx-4">

            <div class="flex flex-wrap pt-5">

                <div class="w-full md:w-1/3 xl:w-1/3 p-3">
                    <a href="<?php echo e(asset('http://201.76.150.19:8080/conveniorn/conveniorelsite.aspx')); ?>" target="_blank">
                        <div
                            class="bg-white border border-lightBlue-600 rounded-md shadow p-2 hover:bg-lightBlue-300 hover:border-transparent | transition-colors duration-500">
                            <div class="flex flex-row items-center">
                                <div class="">
                                    <div class="rounded p-2 bg-lightBlue-600"><i
                                            class="fas fa-handshake fa-2x fa-fw fa-inverse"></i></div>
                                </div>
                                <div class="flex-1 text-right md:text-center">
                                    <h3 class="font-semibold uppercase text-gray-800" name="texto"
                                        style="font-size: 0.875rem; line-height: 1.25rem;">
                                        Consulta de Convênios
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="w-full md:w-1/3 xl:w-1/3 p-3">
                    <a href="<?php echo e(asset('http://201.76.150.19:8080/conveniorn/certidaosite.aspx')); ?>" target="_blank">
                        <div
                            class="bg-white border border-lightBlue-600 rounded-md shadow p-2 hover:bg-lightBlue-300 hover:border-transparent | transition-colors duration-500">
                            <div class="flex flex-row items-center">
                                <div class="flex-shrink pr-4">
                                    <div class="rounded p-2 bg-lightBlue-600">
                                        <i class="fas fa-file-contract fa-2x fa-fw fa-inverse"></i>
                                    </div>
                                </div>
                                <div class="flex-1 text-right md:text-center">
                                    <h3 class="font-semibold uppercase text-gray-800" name="texto"
                                        style="font-size: 0.875rem; line-height: 1.25rem;">
                                        Certidões - Requerer
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="w-full md:w-1/3 xl:w-1/3 p-3">
                    <a href="<?php echo e(asset('http://201.76.150.19:8080/conveniorn/validacaosite.aspx')); ?>" target="_blank">
                        <div
                            class="bg-white border border-lightBlue-600 rounded-md shadow p-2 hover:bg-lightBlue-300 hover:border-transparent | transition-colors duration-500">
                            <div class="flex flex-row items-center">
                                <div class="flex-shrink pr-4">
                                    <div class="rounded p-2 bg-lightBlue-600"><i
                                            class="fas fa-file-contract fa-2x fa-fw fa-inverse"></i>
                                    </div>
                                </div>
                                <div class="flex-1 text-right md:text-center">
                                    <h3 class="font-semibold uppercase text-gray-800" name="texto"
                                        style="font-size: 0.875rem; line-height: 1.25rem;">
                                        Certidões - Validar
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                

            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/portaltransparencianewrn/resources/views/control/index.blade.php ENDPATH**/ ?>