<?php $__env->startSection('title'); ?>
- Obras e Execução
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<section class="text-gray-600 body-font">
   <div class="container px-5 mx-auto">
      <div class="flex flex-col text-center w-full">

         <h1 class="text-lg text-gray-900 tracking-widest font-medium title-font pb-2" name="texto"
            style="font-size: 1.125rem; line-height: 2rem;">
            <i class="fas fa-file-alt"></i>
            OBRAS E EXECUÇÃO
         </h1>

         <div class="flex flex-col text-center w-full mb-3">
            <p class="lg:w-3/4 mx-auto leading-relaxed text-sm text-center" name="texto"
               style="font-size: 0.875rem; line-height: 1.25rem;">
               
            </p>
         </div>

         <ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto"
            style="font-size: 0.875rem; line-height: 1.25rem;">
            <li class="inline-flex items-center">
               <a href="<?php echo e(url('/')); ?>">Início</a>
               <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                  <path fill-rule="evenodd"
                     d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                     clip-rule="evenodd"></path>
               </svg>
            </li>
            <li class="inline-flex items-center">
               <a href="#" class="text-teal-400">
                Obras e Execução
               </a>
            </li>
         </ul>

         <hr class="border-b-2 border-lightBlue-300 mb-6 mx-4">

         
            <div class='tableauPlaceholder' id='viz1714651076808' style='position: relative'>
                <noscript><a href='#'><img alt='Obras RN ' src='https:&#47;&#47;public.tableau.com&#47;static&#47;images&#47;si&#47;siai-obras&#47;ObrasRN&#47;1_rss.png' style='border: none' /></a></noscript>
                <object class='tableauViz' style='display:none;'>
                   <param name='host_url' value='https%3A%2F%2Fpublic.tableau.com%2F' />
                   <param name='embed_code_version' value='3' />
                   <param name='site_root' value='' />
                   <param name='name' value='siai-obras&#47;ObrasRN' />
                   <param name='tabs' value='no' />
                   <param name='toolbar' value='yes' />
                   <param name='static_image' value='https:&#47;&#47;public.tableau.com&#47;static&#47;images&#47;si&#47;siai-obras&#47;ObrasRN&#47;1.png' />
                   <param name='animate_transition' value='yes' />
                   <param name='display_static_image' value='yes' />
                   <param name='display_spinner' value='yes' />
                   <param name='display_overlay' value='yes' />
                   <param name='display_count' value='yes' />
                   <param name='language' value='pt-BR' />
                   <param name='filter' value='publish=yes' />
                </object>
             </div>
             <script type='text/javascript'> 
                var divElement = document.getElementById('viz1714651076808'); 
                var vizElement = divElement.getElementsByTagName('object')[0]; 
                if ( divElement.offsetWidth > 800 ) { 
                   vizElement.style.width='100%';
                   vizElement.style.height=(divElement.offsetWidth*0.75)+'px';
                } else if ( divElement.offsetWidth > 500 ) { 
                   vizElement.style.width='100%';
                   vizElement.style.height=(divElement.offsetWidth*0.75)+'px';
                } else { 
                   vizElement.style.width='100%';
                   vizElement.style.height='1677px';
                } 
                var scriptElement = document.createElement('script'); 
                scriptElement.src = 'https://public.tableau.com/javascripts/api/viz_v1.js'; 
                vizElement.parentNode.insertBefore(scriptElement, vizElement); 
             </script>
            
         

      </div>
   </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/portaltransparencianewrn/resources/views/obras-e-execucao/index.blade.php ENDPATH**/ ?>