<?php $__env->startSection('title'); ?>
- Contratos
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<link rel="stylesheet" href="<?php echo e(asset('css/modal.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="flex flex-col text-center w-full pb-5">
    <h1 class="text-2xl text-gray-800 tracking-wider uppercase" name="texto" style="font-size: 1.5rem; line-height: 2rem;">
        <i class="fas fa-file-alt"></i>
        Contratos
    </h1>
</div>

<div>

    <form action="<?php echo e(route('contratos')); ?>" method="POST">
        <?php echo csrf_field(); ?>

        <div class="flex flex-wrap -mx-3 mb-2 justify-center">

            <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2 flex justify-center" for="grid-state" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                    Orgão
                </label>
                <div id="select_jurisdicionado" class="relative">
                    <select id="jurisdicionado" name="jurisdicionado"
                        class="block appearance-none w-full border border-blue-400 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-blue-700">
                        <?php $__currentLoopData = $jurisdicionado; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $juris): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($juris->jurisdicionado != NULL): ?>
                        <?php if($juris->jurisdicionado == $jurisdicionado_selected): ?>
                        <option value="<?php echo e($jurisdicionado_selected); ?>" selected> <?php echo e($juris->jurisdicionado); ?> </option>
                        <?php else: ?>
                        <option value="<?php echo e($juris->jurisdicionado); ?>">
                            <?php echo e($juris->jurisdicionado); ?>

                        </option>
                        <?php endif; ?>
                        <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                    <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                        <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                            <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                        </svg>
                    </div>
                </div>
            </div>

            <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
            
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2 flex justify-center" for="grid-state" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                    Ano Vigência
                </label>
                <div id="select_ano" class="relative">
                    <select id="ano" name="ano"
                        class="block appearance-none w-full border border-blue-400 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-blue-700">
                        <?php $__currentLoopData = $anos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ano): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($ano->ano != NULL): ?>
                        <?php if($ano->ano == $ano_selected): ?>
                        <option value="<?php echo e($ano_selected); ?>" selected><?php echo e($ano->ano); ?></option>
                        <?php else: ?>
                        <option value="<?php echo e($ano->ano); ?>"><?php echo e($ano->ano); ?></option>
                        <?php endif; ?>
                        <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                    <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                        <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                            <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                        </svg>
                    </div>
                </div>
            </div>


        </div>
        <div class="flex justify-center md:mt-8">
            <input class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded cursor-pointer"
                type="submit" value="Consultar" />
        </div>
    </form>

</div>

<div id="show" class="flex flex-col pt-8">
    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
            <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                <table class="min-w-full divide-y divide-lightBlue-200">
                    <thead class="bg-lightBlue-50">
                        <tr>
                            <th></th>
                            <th
                                class="px-6 py-3 text-left text-xs font-medium text-lightBlue-700 uppercase tracking-wider" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                Orgão</th>
                            <th
                                class="px-6 py-3 text-left text-xs font-medium text-lightBlue-700 uppercase tracking-wider" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                N. Processo</th>
                            <th
                                class="px-6 py-3 text-left text-xs font-medium text-lightBlue-700 uppercase tracking-wider" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                Ano Processo</th>
                            <th
                                class="px-6 py-3 text-left text-xs font-medium text-lightBlue-700 uppercase tracking-wider" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                N. Contrato</th>
                            <th
                                class="px-6 py-3 text-left text-xs font-medium text-lightBlue-700 uppercase tracking-wider" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                Ano</th>
                            <th
                                class="px-6 py-3 text-left text-xs font-medium text-lightBlue-700 uppercase tracking-wider" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                CNPJ/CPF</th>
                            <th
                                class="px-6 py-3 text-left text-xs font-medium text-lightBlue-700 uppercase tracking-wider" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                Contratado</th>
                            <th
                                class="px-6 py-3 text-left text-xs font-medium text-lightBlue-700 uppercase tracking-wider" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                Objeto</th>
                            <th
                                class="px-6 py-3 text-left text-xs font-medium text-lightBlue-700 uppercase tracking-wider" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                Valor</th>
                        </tr>
                    </thead>
                    <tbody class="bg-white divide-y divide-lightBlue-200 text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        <?php $__currentLoopData = $dados; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $dado): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td>
                                <svg onclick="modalState(<?php echo e($key); ?>)" id="card_open<?php echo e($key); ?>"
                                    xmlns="http://www.w3.org/2000/svg" class="mx-4 h-5 w-5 text-green-600 cursor-pointer"
                                    viewBox="0 0 20 20" fill="currentColor">
                                    <path fill-rule="evenodd"
                                        d="M10 18a8 8 0 100-16 8 8 0 000 16zm1-11a1 1 0 10-2 0v2H7a1 1 0 100 2h2v2a1 1 0 102 0v-2h2a1 1 0 100-2h-2V7z"
                                        clip-rule="evenodd" />
                                </svg>
                                <svg id="card_minus<?php echo e($key); ?>" xmlns="http://www.w3.org/2000/svg"
                                    class="hidden mx-4 h-5 w-5 text-red-600" viewBox="0 0 20 20" fill="currentColor">
                                    <path fill-rule="evenodd"
                                        d="M10 18a8 8 0 100-16 8 8 0 000 16zM7 9a1 1 0 000 2h6a1 1 0 100-2H7z"
                                        clip-rule="evenodd" />
                                </svg>
                            </td>
                            <td class="px-6 py-4 whitespace-normal"><?php echo e($dado->jurisdicionado); ?></td>
                            <td class="px-6 py-4 whitespace-nowrap"><?php echo e($dado->numero_processo_despesa); ?></td>
                            <td class="px-6 py-4 whitespace-nowrap"><?php echo e($dado->ano_processo_despesa); ?></td>
                            <td class="px-6 py-4 whitespace-nowrap text-center"><?php echo e($dado->numero_contrato); ?></td>
                            <td class="px-6 py-4 whitespace-nowrap"><?php echo e($dado->ano); ?></td>
                            <td class="px-6 py-4 whitespace-nowrap"><?php echo e($dado->cpf_cnpj_contratado); ?></td>
                            <td class="px-6 py-4 whitespace-normal"><?php echo e($dado->contratado); ?></td>
                            <td class="px-6 py-4 whitespace-normal"><?php echo e($dado->objeto); ?></td>
                            <td class="px-6 py-4 whitespace-nowrap">R$ <?php echo e(number_format($dado->valor, 2, ',', '.')); ?>

                            </td>
                        </tr>

                        <div id="card_panel<?php echo e($key); ?>"
                            class="bg-black bg-opacity-50 main-modal fixed w-full inset-0 z-50 overflow-hidden flex justify-center items-center hidden">
                            <div
                                class="modal-container bg-white w-4/12 md:max-w-11/12 mx-auto rounded-xl z-50 overflow-y-auto">
                                <div class="modal-content py-4 text-left px-6">
                                    <div class="flex justify-between items-center pb-3">
                                        <p class="text-sm font-bold text-lightBlue-500" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">Mais informações</p>
                                        <div onclick="modalState(<?php echo e($key); ?>)" id="card_close<?php echo e($key); ?>"
                                            class="modal-close cursor-pointer z-50">
                                            <svg class="fill-current text-red-500" xmlns="http://www.w3.org/2000/svg"
                                                width="18" height="18" viewBox="0 0 18 18">
                                                <path
                                                    d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z">
                                                </path>
                                            </svg>
                                        </div>
                                    </div>
                                    <div>
                                        
                                        <p class="text-sm"><b class="mr-2 text-gray-700">Data
                                                Início:</b><?php echo e($dado->data_inicio_vigencia); ?>

                                        </p>
                                        <p class="text-sm"><b class="mr-2 text-gray-700">Data
                                                Fim:</b><?php echo e($dado->data_fim_vigencia); ?>

                                        </p>
                                        <p class="text-sm"><b class="mr-2 text-gray-700">N. Termo
                                                Aditivo:</b><?php echo e($dado->numero_termo_aditivo); ?></p>
                                        <p class="text-sm"><b class="mr-2 text-gray-700">Ano Termo
                                                Aditivo:</b><?php echo e($dado->ano_termo_aditivo); ?>

                                        </p>
                                        <p class="text-sm"><b class="mr-2 text-gray-700">Objetivo do
                                                Aditivo:</b><?php echo e($dado->objetivo_aditivo); ?>

                                        </p>
                                        <p class="text-sm"><b class="mr-2 text-gray-700">Valor
                                                Aditivo:</b><?php echo e($dado->valor_aditivo); ?></p>
                                        <p class="text-sm"><b class="mr-2 text-gray-700">Data Início
                                                Aditivo:</b><?php echo e($dado->data_inicio_vigencia_aditivo); ?></p>
                                        <p class="text-sm"><b class="mr-2 text-gray-700">Data Fim
                                                Aditivo:</b><?php echo e($dado->data_fim_vigencia_aditivo); ?></p>
                                        <p class="text-sm"><b class="mr-2 text-gray-700">Justif.
                                                Aditivo:</b><?php echo e($dado->justificativa_aditivo); ?></p>
                                        <p class="text-sm"><b class="mr-2 text-gray-700">Nº Recibo (Anexo
                                                38):</b><?php echo e($dado->numero_recibo_anexo_38); ?></p>
                                        <p class="text-sm"><b class="mr-2 text-gray-700">Nº Recibo (Anexo
                                                13):</b><?php echo e($dado->numero_recibo_anexo_13); ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>

                <?php if($dados->isEmpty()): ?>
                <p class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" name="texto" style="font-size: 0.75rem; line-height: 1rem;">Não há dados
                    cadastrados para essa busca...</p>
                <?php endif; ?>

                <?php echo e($dados->appends(Request::all())->links()); ?>

            </div>
        </div>
    </div>
</div>

<div class="pt-4">
    <div class="p-2 text-blueGray-800 uppercase tracking-wider text-xs text-right" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
        <i class="fas fa-arrow-up"></i>
        Dados atualizados em: 13/07/2021
    </div>
  </div>

<script>
    function modalState(id) {
      const card_open = document.getElementById('card_open' + id);
      const card_close = document.getElementById('card_close' + id);
      const card_panel = document.getElementById('card_panel' + id);
      const card_minus = document.getElementById('card_minus' + id)

      if (card_panel.classList.contains('hidden')) {
         // Show modal
         card_panel.classList.remove('hidden')
         card_panel.classList.add('block')

         // Delete button Plus
         card_open.classList.add('hidden')
         card_open.classList.remove('block')

         // Show button Minus
         card_minus.classList.add('block')
         card_minus.classList.remove('hidden')


         // Start animation open
         card_panel.classList.add('card_open')
      } else {
         // Delete modal
         card_panel.classList.add('hidden')
         card_panel.classList.remove('block')

         // Show button
         card_open.classList.remove('hidden')
         card_open.classList.add('block')

         // Delete button Minus
         card_minus.classList.add('hidden')
         card_minus.classList.remove('block')

         // Remove animation open
         card_panel.classList.remove('card_open')
      }
   }
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/portaltransparencianewrn/resources/views/contratos/index.blade.php ENDPATH**/ ?>