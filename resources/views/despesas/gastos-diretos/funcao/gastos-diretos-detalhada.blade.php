@extends('layouts.app')

@section('title')
- Despesa Detalhada
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/dropdown.css') }}">
@endsection

@section('content')

<h1 class="text-lg text-gray-900 tracking-widest font-medium title-font uppercase text-center pb-2" name="texto"
   style="font-size: 1.125rem; line-height: 2rem;">
   <i class="far fa-file-alt"></i>
   Documentos
</h1>
<ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto"
   style="font-size: 0.875rem; line-height: 1.25rem;">
   <li class="inline-flex items-center">
      <a href="{{ url('/') }}">Inicio</a>
      <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
         <path fill-rule="evenodd"
            d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
            clip-rule="evenodd"></path>
      </svg>
   </li>
   <li class="inline-flex items-center">
      <a href="{{ route('despesas') }}">Despesas</a>
      <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
         <path fill-rule="evenodd"
            d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
            clip-rule="evenodd"></path>
      </svg>
   </li>
   <li class="inline-flex items-center">
      <a href="javascript:history.back()">Despesa</a>
      <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
         <path fill-rule="evenodd"
            d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
            clip-rule="evenodd"></path>
      </svg>
   </li>
   <li class="inline-flex items-center">
      <a href="#" class="text-teal-400">Documentos</a>
   </li>
</ul>

<hr class="border-b-2 border-lightBlue-300 mb-12 mx-4">
<div class="container">
   <div class="card py-3 px-5 rounded-xl flex flex-col mb-5">
      <div class="title text-xl font-medium mb-3" name="texto" style="font-size: 1.25rem; line-height: 1.75rem;">
         @if ($posicao == '=' or $mes == 1)
         Total de Despesa de @lang('meses.'.$mes) do ano
         {{ $ano }}
         @else
         Total de Despesa de Janeiro a
         @lang('meses.'.$mes) do ano {{ $ano }}
         @endif
      </div>

      <div class="flex px-4 py-4 justify-center bg-white border-b-4 border-lightBlue-300 rounded-md flex-col">
         <div class="px-5 py-4 bg-white border border-gray-200 rounded-md">
            <div class="card rounded-xl flex flex-col">
               <div class="grid grid-row-4 md:grid-row-4 gap-3">
                  <div class="py-3 pl-5 bg-gray-50 text-sm rounded-md shadow" name="texto"
                     style="font-size: 0.875rem; color: #333333; line-height: 1.25rem;">
                     <ul class="flex flex-wrap">
                        @if ($posicao == '=' or $mes == 1)
                        <li class="font-semibold mr-2">Exercício: </li>
                        <li>@lang('meses.'.$mes)</li>
                        @else
                        <li>Janeiro a @lang('meses.'.$mes)</li>
                        @endif
                        <li>
                           <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20"
                              fill="currentColor">
                              <path fill-rule="evenodd"
                                 d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                 clip-rule="evenodd" />
                           </svg>
                        </li>
                        <li>{{ $ano }}</li>
                        <li>
                           <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20"
                              fill="currentColor">
                              <path fill-rule="evenodd"
                                 d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                 clip-rule="evenodd" />
                           </svg>
                        </li>
                        <li>{{ $nomefase }}</li>
                        <li>
                           <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20"
                              fill="currentColor">
                              <path fill-rule="evenodd"
                                 d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                 clip-rule="evenodd" />
                           </svg>
                        </li>
                        <li>R$ {{ number_format($sumtotal, 2, ',', '.') }}</li>
                     </ul>
                  </div>
                  <div class="py-3 pl-5 bg-gray-50 text-sm rounded-md shadow" name="texto"
                     style="font-size: 0.875rem; color: #333333; line-height: 1.25rem;">
                     <ul class="flex flex-wrap">
                        <li class="font-semibold mr-2">Função:</li>
                        <li>{{ $nomefuncao->txtdescricaofuncao }}</li>
                        <li>
                           <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20"
                              fill="currentColor">
                              <path fill-rule="evenodd"
                                 d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                 clip-rule="evenodd" />
                           </svg>
                        </li>
                        <li>R$ {{ number_format($sumfuncao, 2, ',', '.') }}</li>
                     </ul>
                  </div>
                  <div class="py-3 pl-5 bg-gray-50 text-sm rounded-md shadow" name="texto"
                     style="font-size: 0.875rem; color: #333333; line-height: 1.25rem;">
                     <ul class="flex flex-wrap">
                        <li class="font-semibold mr-2">Sub-Função:</li>
                        <li>{{ $nomesubfuncao->txtdescricaosubfuncao }}</li>
                        <li>
                           <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20"
                              fill="currentColor">
                              <path fill-rule="evenodd"
                                 d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                 clip-rule="evenodd" />
                           </svg>
                        </li>
                        <li>R$ {{ number_format($sumsubfuncao, 2, ',', '.') }}</li>
                     </ul>
                  </div>
                  <div class="py-3 pl-5 bg-gray-50 text-sm rounded-md shadow" name="texto"
                     style="font-size: 0.875rem; color: #333333; line-height: 1.25rem;">
                     <ul class="flex flex-wrap">
                        <li class="font-semibold mr-2">Ação:</li>
                        <li>{{ $nomeacao->txtdescricaoacao }}</li>
                        <li>
                           <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20"
                              fill="currentColor">
                              <path fill-rule="evenodd"
                                 d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                 clip-rule="evenodd" />
                           </svg>
                        </li>
                        <li>R$ {{ number_format($sumacao, 2, ',', '.') }}</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

@if ($fase == 'vportal_notapagamento_gastosdiretos')

@foreach ($favorecidos as $favorecido)
<div class="tab relative">
   <input class="absolute w-full h-96 cursor-pointer opacity-0 z-10" type="checkbox" id="chck1">
   <header class="cursor-pointer select-none tab-label" for="chck1">
      <div class="px-10 my-4 py-6 bg-white rounded-lg border-l-2 border border-red-500 shadow-md">
         <div class="flex justify-between items-center">
            <span class="font-semibold text-gray-400 tracking-widest" name="texto"
               style="font-size: 1rem; line-height: 1.5rem;">Documento</span>
            <a name="fase" class="px-2 py-1 text-gray-100 bg-red-600 tracking-widest uppercase" href="#">{{ $nomefase
               }}</a>
         </div>
         <hr class="py-4 w-1/2">
         <div class="mt-2">
            <a class="text-xl text-gray-700 font-semibold hover:text-gray-600" href="#">Favorecido</a>
            <p class="mt-2 text-gray-600 text-sm py-2" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
               <span class="font-semibold">Nome/Razão Social:</span> {{ $favorecido->txtnomecredor }}
            </p>
            <hr class="">
            <p class="mt-2 text-gray-600 text-sm py-2" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
               <span class="font-semibold">CNPJ/CPF/IG:</span>
               @if (strlen($favorecido->codcredor) == 11)
               {{ "###.". substr($favorecido->codcredor, 3, 3) . "." . substr($favorecido->codcredor, 6, 3) . "-##" }}
               @else
               {{ $favorecido->codcredor }}
               @endif
            </p>
            <hr>
            <p class="mt-2 text-gray-600 text-sm py-2" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
               <span class="font-semibold">Exercício Corrente (R$):</span> R$
               {{ number_format($favorecido->vlrpagamento, 2, ',', '.') }}
            </p>
            <hr>
            <p class="mt-2 text-gray-600 text-sm py-2" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
               <span class="font-semibold">Restos a Pagar (R$):</span> R$
               {{ number_format($favorecido->vlrpagamentorp, 2, ',', '.') }}
            </p>
         </div>
         <div class="flex justify-between items-center mt-4 text-sm uppercase" name="texto"
            style="font-size: 0.875rem; line-height: 1.25rem;">
            <a class="flex items-center text-blue-600 hover:underline" href="#">
               <span>Ver Documentos</span>
               <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                  <path fill-rule="evenodd"
                     d="M14.707 12.293a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 111.414-1.414L9 14.586V3a1 1 0 012 0v11.586l2.293-2.293a1 1 0 011.414 0z"
                     clip-rule="evenodd" />
               </svg>
            </a>
         </div>
      </div>
   </header>

   <div class="tab-content">
      <div class="overflow-auto min-h-full max-h-80">
         <table class="min-w-max w-full table-auto">
            <thead>
               <tr class="bg-red-200 text-gray-600 uppercase text-sm leading-normal" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">
                  <th class="py-3 px-6 text-left">Documento</th>
                  <th class="py-3 px-6 text-center">Programa</th>
                  <th class="py-3 px-6 text-center">Exercício Corrente (R$)</th>
                  <th class="py-3 px-6 text-center">Restos a Pagar (R$)</th>
                  <th class="py-3 px-2 text-center"></th>
               </tr>
            </thead>
            <tbody class="text-gray-600 text-sm font-light" name="texto"
               style="font-size: 0.875rem; line-height: 1.25rem;">
               @foreach ($documentos as $documento)
               @if ($favorecido->codcredor == $documento->codcredor)
               <tr class="border-b border-gray-200 hover:bg-gray-100">
                  <td class="py-3 px-6 text-left whitespace-nowrap">
                     <div class="flex items-center">
                        <span class="font-medium">{{ $documento->numdocumento }}</span>
                     </div>
                  </td>
                  <td class="py-3 px-6 text-center">
                     {{ $documento->txtdescricaoprograma }}
                  </td>
                  <td class="py-3 px-6 text-center">
                     <span
                        class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-yellow-100 text-yellow-800"
                        name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                        R$ {{ number_format($documento->vlrpagamento, 2, ',', '.') }}
                     </span>
                  </td>
                  <td class="py-3 px-6 text-center">
                     <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800"
                        name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                        R$ {{ number_format($documento->vlrpagamentorp, 2, ',', '.') }}
                     </span>
                  </td>
                  <td>
                     <a target="_blank" href="{{ route('detalhes-documento',
                        ['request' => Crypt::encrypt([
                           'numdocumento' => $documento->numdocumento,
                           'codcredor' => $documento->codcredor,
                           'txtdescricaoprograma' => $documento->txtdescricaoprograma,
                           'txtdescricaoacao' => $documento->txtdescricaoacao,
                           'codunidadegestora' => $documento->codunidadegestora,
                           'fase' => $fase
                           ])]) }}">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 text-lightBlue-500" viewBox="0 0 20 20"
                           fill="currentColor">
                           <path
                              d="M11 3a1 1 0 100 2h2.586l-6.293 6.293a1 1 0 101.414 1.414L15 6.414V9a1 1 0 102 0V4a1 1 0 00-1-1h-5z" />
                           <path
                              d="M5 5a2 2 0 00-2 2v8a2 2 0 002 2h8a2 2 0 002-2v-3a1 1 0 10-2 0v3H5V7h3a1 1 0 000-2H5z" />
                        </svg>
                     </a>
                  </td>
               </tr>
               @endif
               @endforeach
            </tbody>
         </table>
      </div>
   </div>
</div>
@endforeach

@else

@foreach ($favorecidos as $favorecido)
<div class="tab relative">
   <input class="absolute w-full h-96 cursor-pointer opacity-0 z-10" type="checkbox" id="chck1">
   <header class="cursor-pointer select-none tab-label" for="chck1">
      <div class="px-10 my-4 py-6 bg-white rounded-lg border-l-2 border border-green-500 shadow-md">
         <div class="flex justify-between items-center">
            <span class="font-semibold text-gray-400 tracking-widest" name="texto"
               style="font-size: 1rem; line-height: 1.5rem;">Documento</span>
            <a name="fase" class="px-2 py-1 text-gray-100 bg-green-600 tracking-widest uppercase" href="#">{{ $nomefase
               }}</a>
         </div>
         <hr class="py-4 w-1/2">
         <div class="mt-2">
            <a class="text-xl text-gray-700 font-semibold hover:text-gray-600" href="#" name="texto"
               style="font-size: 1.25rem; line-height: 1.75rem;">Favorecido</a>
            <p class="mt-2 text-gray-600 text-sm py-2" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
               <span class="font-semibold">Nome/Razão Social:</span> {{ $favorecido->txtnomecredor }}
            </p>
            <hr class="">
            <p class="mt-2 text-gray-600 text-sm py-2" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
               <span class="font-semibold">CNPJ/CPF/IG:</span>
               @if (strlen($favorecido->codcredor) == 11)
               {{ "###.". substr($favorecido->codcredor, 3, 3) . "." . substr($favorecido->codcredor, 6, 3) . "-##" }}
               @else
               {{ $favorecido->codcredor }}
               @endif
            </p>
            <hr>
            <p class="mt-2 text-gray-600 text-sm py-2" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
               <span class="font-semibold">Valor (R$):</span> R$
               {{ number_format($favorecido->vlrpagamento, 2, ',', '.') }}
            </p>
         </div>
         <div class="flex justify-between items-center mt-4 text-sm uppercase" name="texto"
            style="font-size: 0.875rem; line-height: 1.25rem;">
            <a class="flex items-center text-blue-600 hover:underline" href="#">
               <span>Ver Documentos</span>
               <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                  <path fill-rule="evenodd"
                     d="M14.707 12.293a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 111.414-1.414L9 14.586V3a1 1 0 012 0v11.586l2.293-2.293a1 1 0 011.414 0z"
                     clip-rule="evenodd" />
               </svg>
            </a>
         </div>
      </div>
   </header>

   <div class="tab-content">
      <div class="overflow-auto min-h-full max-h-80">
         <table class="min-w-max w-full table-auto">
            <thead>
               <tr class="bg-green-200 text-gray-600 uppercase text-sm leading-normal" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">
                  <th class="py-3 px-6 text-left">Documento</th>
                  <th class="py-3 px-6 text-center">Programa</th>
                  <th class="py-3 px-6 text-center">Valor (R$)</th>
                  <th class="py-3 px-2 text-center"></th>
               </tr>
            </thead>
            <tbody class="text-gray-600 text-sm font-light" name="texto"
               style="font-size: 0.875rem; line-height: 1.25rem;">
               @foreach ($documentos as $documento)
               @if ($favorecido->codcredor == $documento->codcredor)
               <tr class="border-b border-gray-200 hover:bg-gray-100">
                  <td class="py-3 px-6 text-left whitespace-nowrap">
                     <div class="flex items-center">
                        <span class="font-medium">{{ $documento->numdocumento }}</span>
                     </div>
                  </td>
                  <td class="py-3 px-6 text-center">
                     {{ $documento->txtdescricaoprograma }}
                  </td>
                  <td class="py-3 px-6 text-center">
                     <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800"
                        name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                        R$ {{ number_format($documento->vlrpagamento, 2, ',', '.') }}
                     </span>
                  </td>
                  <td>
                     <a target="_blank" href="{{ route('detalhes-documento',
                        ['request' => Crypt::encrypt([
                           'numdocumento' => $documento->numdocumento,
                           'codcredor' => $documento->codcredor,
                           'txtdescricaoprograma' => $documento->txtdescricaoprograma,
                           'txtdescricaoacao' => $documento->txtdescricaoacao,
                           'codunidadegestora' => $documento->codunidadegestora,
                           'fase' => $fase
                           ])]) }}">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 text-lightBlue-500" viewBox="0 0 20 20"
                           fill="currentColor">
                           <path
                              d="M11 3a1 1 0 100 2h2.586l-6.293 6.293a1 1 0 101.414 1.414L15 6.414V9a1 1 0 102 0V4a1 1 0 00-1-1h-5z" />
                           <path
                              d="M5 5a2 2 0 00-2 2v8a2 2 0 002 2h8a2 2 0 002-2v-3a1 1 0 10-2 0v3H5V7h3a1 1 0 000-2H5z" />
                        </svg>
                     </a>
                  </td>
               </tr>
               @endif
               @endforeach
            </tbody>
         </table>
      </div>
   </div>
</div>
@endforeach

@endif
<div class="alert flex flex-row items-center bg-blue-200 p-1 rounded">
   <div
      class="alert-icon flex items-center bg-blue-100 border-2 border-blue-500 justify-center flex-shrink-0 rounded-full">
      <span class="text-blue-500">
         <svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
            <path fill-rule="evenodd"
               d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z"
               clip-rule="evenodd"></path>
         </svg>
      </span>
   </div>
   <div class="alert-content ml-4">
      <div class="alert-description text-xs text-blue-600" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
         <span class="font-semibold">Dados atualizados em:</span> {{ date("d/m/Y") }} | <span class="font-semibold">Fonte:</span>
         SIGEF - SISTEMA INTEGRADO DE PLANEJAMENTO E GESTÃO FISCAL
      </div>
   </div>
</div>

@endsection