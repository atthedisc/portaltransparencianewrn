@extends('layouts.app')

@section('title')
- Renúncia de Receita
@endsection

@section('content')
<section class="text-gray-600 body-font">
   <div class="container px-5 mx-auto">
      <div class="flex flex-col text-center w-full">

         <h1 class="text-lg text-gray-900 tracking-widest font-medium title-font pb-2" name="texto"
            style="font-size: 1.125rem; line-height: 2rem;">
            <i class="fas fa-file-alt"></i>
            RENÚNCIA DE RECEITA
         </h1>

         <ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto"
            style="font-size: 0.875rem; line-height: 1.25rem;">
            <li class="inline-flex items-center">
               <a href="{{ url('/') }}">Início</a>
               <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                  <path fill-rule="evenodd"
                     d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                     clip-rule="evenodd"></path>
               </svg>
            </li>
            <li class="inline-flex items-center">
               <a href="#" class="text-teal-400">
                  Renúncia de Receita
               </a>
            </li>
         </ul>

         <hr class="border-b-2 border-lightBlue-300 mb-6 mx-4">

         <div class="grid md:grid-flow-col mx-auto">

            <table
               class='mx-10 mb-auto max-w-sm md:max-w-lg rounded-lg bg-white divide-y divide-lightBlue-200 overflow-hidden text-center'>

               <tr class="text-coolGray-400 text-center">
               <th class="font-normal text-sm tracking-wider px-6 py-4 text-center" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">
                  Clique no link desejado para acessar
               </th>
            </tr>


               <tbody class="divide-y divide-lightBlue-200">
                  <tr>
                     <td class="px-6 py-4 text-left text-sm">
                        <i class="fa fa-link mr-2"></i>
                        <a href="{{ asset('https://www.set.rn.gov.br/contentProducao/aplicacao/set_v2/legislacao/enviados/index.asp') }}"
                           class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                           name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                           Espécies de Desonerações
                        </a>
                     </td>
                  </tr>
                  <tr>
                     <td class="px-6 py-4 text-left text-sm">
                        <i class="fa fa-link mr-2"></i>
                        <a href="{{ asset('http://www.seplan.rn.gov.br/Conteudo.asp?TRAN=PASTAC&TARG=2476&ACT=&PAGE=&PARM=&LBL=') }}"
                           class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                           name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                           Dados Quantitativos - LDO
                        </a>
                     </td>
                  </tr>
                  <tr>
                     <td class="px-6 py-4 text-left text-sm">
                        <i class="fa fa-link mr-2"></i>
                        <a href="{{ asset('https://www.set.rn.gov.br/contentProducao/aplicacao/set_v2/legislacao/enviados/index_incentivos_fiscais-atos_declaratorios.asp') }}"
                           class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                           name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                           Beneficiários
                        </a>
                     </td>
                  </tr>
                  <tr>
                     <td class="px-6 py-4 text-left text-sm">
                        <i class="fa fa-link mr-2"></i>
                        <a href="{{ asset('https://www.set.rn.gov.br/contentProducao/aplicacao/set_v2/legislacao/enviados/index.asp') }}"
                           class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                           name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                           Incentivos à Cultura e ao Esporte
                        </a>
                     </td>
                  </tr>
               </tbody>
            </table>
         </div>

      </div>
   </div>
</section>
@endsection
