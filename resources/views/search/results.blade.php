@extends('layouts.app')

@section('content')
    <div class="flex-1 text-right md:text-left">
        <h1 class="text-lg text-gray-900 tracking-widest font-medium title-font uppercase text-center pb-2" name="texto"
            style="font-size: 1.125rem; line-height: 2rem;">
            <i class="fa-sharp fa-solid fa-magnifying-glass"></i>
            Resultados de Pesquisa:
        </h1>

        <ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto"
            style="font-size: 0.875rem; line-height: 1.25rem;">
            <li class="inline-flex items-center">
                <a href="{{ url('/') }}">Início</a>
                <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                    <path fill-rule="evenodd"
                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                        clip-rule="evenodd"></path>
                </svg>
            </li>
            <li class="inline-flex items-center">
                <a href="#" class="text-teal-400">
                    Pesquisa
                </a>
            </li>
        </ul>
        <hr class="border-b-2 border-lightBlue-300 mb-6 mx-4">
    </div>
    <h1 class="text-lg text-gray-900 tracking-widest font-medium title-font uppercase text-left pb-2" name="texto"
        style="font-size: 1.125rem; line-height: 2rem;">

        Links encontrados dentro do portal:
    </h1>

    @if (!$pages->isEmpty())
        <!-- tinha um [0] mas retirei -->
        @foreach ($pages as $page)
            <div>
                <li class="w-full flex justify-between border-b-2 border-neutral-100 border-opacity-100 py-4 text-lightBlue-700 dark:border-opacity-50 ">
                    <a href="{{ $page->url }}" style="font-weight:bold ">{{ $page->title }}</a>
                    <p class="text-sm">{{ $page->path }}</p>
                </li>

            </div>
        @endforeach
    @else
        <p class="text-center py-20">-Resultado não encontrado-</p>
    @endif
    <br>
    <h1 class="text-lg text-gray-900 tracking-widest font-medium title-font uppercase text-left pb-2" name="texto"
        style="font-size: 1.125rem; line-height: 2rem;">

        Ocorrências no glossário:
    </h1>
    @if (!$glossario->isEmpty())
        <!-- tinha um [0] mas retirei -->
        @foreach ($glossario as $item)
            <div class="w-full md:w-1/1 xl:w-1/1 p-3">
                <div class="p-2 bg-white shadow-md hover:shodow-lg rounded-2xl">
                    <div class="font-medium text-left text-lightBlue-700 uppercase pb-2" name="texto"
                        style="font-size: 1rem; line-height: 1.5rem;">
                        {{ $item->nome }}:
                    </div>
                    <p class="text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        {{ $item->descricao }} <br>
                    </p>
                </div>
            </div>
        @endforeach
    @else
        <p class="text-center py-20">-Resultado não encontrado-</p>
    @endif

@endsection
