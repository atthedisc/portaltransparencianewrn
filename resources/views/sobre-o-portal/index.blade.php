@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('css/pattern.css') }}">
@endsection

@section('title')
- Sobre o Portal
@endsection

@section('content')

<section class="text-gray-600 body-font pb-10">
   <div class="container px-5 mx-auto">
      <div class="flex flex-col text-center w-full">

         <h1 class="text-lg text-gray-900 tracking-widest font-medium title-font pb-2" name="texto"
            style="font-size: 1.125rem; line-height: 2rem;">
            <i class="fas fa-ellipsis-h"></i>
            SOBRE O PORTAL
         </h1>

         <ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto"
            style="font-size: 0.875rem; line-height: 1.25rem;">
            <li class="inline-flex items-center">
               <a href="{{ url('/') }}">Início</a>
               <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                  <path fill-rule="evenodd"
                     d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                     clip-rule="evenodd"></path>
               </svg>
            </li>
            <li class="inline-flex items-center">
               <a href="#" class="text-teal-400">
                  Sobre o Portal
               </a>
            </li>
         </ul>

         <hr class="border-b-2 border-lightBlue-300 mb-6 mx-4">

         <div class="flex-col space-y-4 min-w-screen justify-center items-center">

            <div class="flex flex-col p-8 bg-white shadow-md hover:shodow-lg rounded-2xl">
               <div class="flex items-center justify-between">
                  <div class="flex items-center">
                     <div
                        class="w-16 h-16 rounded-2xl p-3 border border-blue-100 text-blue-400 bg-blue-50 text-center text-3xl"
                        name="texto" style="font-size: 1.875rem; line-height: 2.25rem;">
                        <i class="fas fa-question"></i>
                     </div>
                     <div class="flex flex-col ml-3">
                        <div class="font-medium text-left leading-snug uppercase" name="texto"
                           style="font-size: 1rem; line-height: 1.5rem;">O que é o Portal da Transparência?</div>
                        <p class="text-xs text-left text-gray-600 mt-1" name="texto"
                           style="font-size: 0.75rem; line-height: 1rem;">
                           Descrição para que serve o Portal da Transparência
                        </p>
                     </div>
                  </div>
               </div>
               <p class="leading-relaxed text-sm text-left pt-4" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">
                  O Portal da Transparência do Estado do RN possibilita o
                  acompanhamento das informações orçamentárias e financeiras do executivo estadual, permitindo ao
                  cidadão a fiscalização, de forma eficiente e transparente da utilização dos recursos públicos, para
                  assegurar a
                  sua correta aplicação. O Portal também disponibiliza informações sobre os instrumentos de
                  Planejamento, Relatórios
                  Fiscais, Licitações, obras realizadas no Estado e outras informações de relevância para os cidadãos
                  norteriograndenses.
               </p>
            </div>
            <div class="flex flex-col p-8 bg-coolGray-800 shadow-md hover:shodow-lg rounded-2xl">
               <div class="flex items-center justify-between">
                  <div class="flex items-center">
                     <div
                        class="w-16 h-16 rounded-2xl p-3 border border-coolGray-800 text-blue-400 bg-coolGray-900 text-center text-3xl"
                        name="texto" style="font-size: 1.875rem; line-height: 2.25rem;">
                        <i class="fas fa-hands-helping"></i>
                     </div>
                     <div class="flex flex-col ml-3">
                        <div class="font-medium text-left leading-snug text-gray-200 uppercase" name="texto"
                           style="font-size: 1rem; line-height: 1.5rem;">Sobre confiabilidade</div>
                        <p class="text-xs text-left text-gray-500 mt-1" name="texto"
                           style="font-size: 0.75rem; line-height: 1rem;">
                           Descrição a confiabilidade dos dados apresentados
                        </p>
                     </div>
                  </div>
               </div>
               <p class="leading-relaxed text-sm text-left text-gray-200 pt-4" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">
                  O Portal é atualizado diariamente com os dados do Sistema Integrado de Planejamento e Gestão Fiscal
                  (SIGEF), de onde são extraídas diretamente as informações da execução financeiro-orçamentária do
                  Estado. Dados
                  sobre diárias concedidas aos servidores, são obtidos a partir do “Sistema de Acompanhamento de
                  Diárias”, assim como
                  informações do Portal de Compras do RN (que registra e acompanha as licitações públicas do Executivo
                  Estadual)
                  também estão disponíveis para consulta. Além disso, nosso Portal também disponibiliza informações
                  sobre
                  remunerações dos servidores públicos do Estado.
               </p>
            </div>
            <div class="flex flex-col p-8 bg-white shadow-md hover:shodow-lg rounded-2xl">
               <div class="flex items-center justify-between">
                  <div class="flex items-center">
                     <div
                        class="w-16 h-16 rounded-2xl p-3 border border-blue-100 text-blue-400 bg-blue-50 text-center text-3xl"
                        name="texto" style="font-size: 1.875rem; line-height: 2.25rem;">
                        <i class="fas fa-database"></i>
                     </div>
                     <div class="flex flex-col ml-3">
                        <div class="font-medium text-left leading-snug uppercase" name="texto"
                           style="font-size: 1rem; line-height: 1.5rem;">Dados abertos</div>
                        <p class="text-xs text-left text-gray-600 mt-1" name="texto"
                           style="font-size: 0.75rem; line-height: 1rem;">Descrição sobre os dados abertos
                        </p>
                     </div>
                  </div>
               </div>
               <p class="leading-relaxed text-sm text-left pt-4" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">
                  O Portal da Transparência do RN propicia o acesso a dados no formato aberto, processáveis por máquina
                  e disponibilizados em formato CSV. Dessa forma, o cidadão poderá acessar os dados livremente, fazendo
                  cruzamentos e criando aplicativos que demonstrem novas formas de utilização e visualização desses
                  dados em benefício
                  da sociedade.
               </p>
            </div>
         </div>

      </div>
   </div>
</section>

@endsection
