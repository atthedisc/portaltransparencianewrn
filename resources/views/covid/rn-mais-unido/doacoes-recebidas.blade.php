@extends('layouts/covid.app')

@section('title')
- Doações Recebidas
@endsection

<!--@section('css')
<link rel="stylesheet" href="{{ asset('css/dropdown.css') }}">
@endsection--->

@section('content')
<!-- Section -->
<section class="text-gray-600 body-font">
   <div class="container px-5 mx-auto">

      <div class="flex flex-col text-center w-full">
         <h1>
            <center><img class="" src="images/logo/rnmaisunido_colorido.png" width="150" height="100" alt=""></center>
         </h1>
      </div>

      <main class="w-full p-4 mx-auto mb-12">
         <section class="shadow row bg-white rounded-lg">
            <div class="tabs">
               <div class="border-b tab">
                  <div class="border-l-2 border-transparent relative">
                     <header class="p-5 pl-8 pr-8 select-none tab-label text-center">
                        <span class="text-base" name="texto" style="font-size: 1rem; line-height: 1.5rem;">
                           DOAÇÕES RECEBIDAS
                        </span>
                     </header>
                  </div>
               </div>
               <div class="border-b tab">
                  <div class="border-l-2 border-transparent relative">
                     <input class="w-full absolute z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="chck1">
                     <header
                        class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label"
                        for="chck1">
                        <span class="text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                           1. ALIMENTOS NÃO PERECÍVEIS E MATERIAIS DE LIMPEZA E HIGIENE PESSOAL
                        </span>
                        <div class="rounded-full border border-grey w-7 h-7 flex items-center justify-center test">
                           <!-- icon by feathericons.com -->
                           <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B"
                              stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24"
                              width="24" xmlns="http://www.w3.org/2000/svg">
                              <polyline points="6 9 12 15 18 9">
                              </polyline>
                           </svg>
                        </div>
                     </header>
                     <div class="tab-content overflow-y-auto h-80">
                        <div class="pl-8 pr-8 pb-5 text-grey-darkest">
                           <ul class="pl-4" name="texto" style="font-size: 1rem; line-height: 1.5rem;">
                              @foreach ($tables as $table)
                              @if($table->doacoesrecebidas != null)
                              <li class="text-blue-600 pb-2 text-left hover:underline">
                                 <a href="{{asset('docs/rnmaisunido/doacoes-recebidas/'.$table->doacoesrecebidaslink)}}"
                                    target="_blank">
                                    {{ $table->doacoesrecebidas }}
                                 </a>
                              </li>
                              @endif
                              @endforeach

                        </div>
                     </div>
                  </div>
               </div>
               <div class="border-b tab">
                  <div class="border-l-2 border-transparent relative">
                     <input class="w-full absolute z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="chck2">
                     <header
                        class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label"
                        for="chck2">
                        <span class="text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                           2. EPIs e DEMAIS ITENS HOSPITALARES
                        </span>
                        <div class="rounded-full border border-grey w-7 h-7 flex items-center justify-center test">
                           <!-- icon by feathericons.com -->
                           <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B"
                              stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24"
                              width="24" xmlns="http://www.w3.org/2000/svg">
                              <polyline points="6 9 12 15 18 9">
                              </polyline>
                           </svg>
                        </div>
                     </header>
                     <div class="tab-content overflow-y-auto h-80">
                        <div class="pl-8 pr-8 pb-5 text-grey-darkest ">
                           <ul class="pl-4" name="texto" style="font-size: 1rem; line-height: 1.5rem;">
                              @foreach ($tables as $table)
                              @if($table->doacoesrecebidasepis != null)
                              <li class="text-blue-600 pb-2 text-left hover:underline">
                                 <a href="{{asset('docs/rnmaisunido/doacoes-recebidas/'.$table->doacoesrecebidasepislink)}}"
                                    target="_blank">
                                    {{ $table->doacoesrecebidasepis }}
                                 </a>
                              </li>
                              @endif
                              @endforeach

                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </main>
   </div>
</section>
<!-- /Section -->
@endsection