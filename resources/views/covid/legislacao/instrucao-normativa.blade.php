@extends('layouts/covid.app')

@section('title')
- Instrução Normativa
@endsection

@section('content')
<section class="text-gray-600 body-font">
   <div class="container px-5 mx-auto">
      <div class="flex flex-col text-center w-full">

         <h1 class="text-lg text-gray-900 tracking-widest font-medium title-font pb-2" name="texto"
            style="font-size: 1.125rem; line-height: 2rem;">
            <i class="fas fa-database"></i>
            INSTRUÇÃO NORMATIVA
         </h1>

         <ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto"
            style="font-size: 0.875rem; line-height: 1.25rem;">
            <li class="inline-flex items-center">
               <a href="{{ url('/covid') }}">Início</a>
               <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                  <path fill-rule="evenodd"
                     d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                     clip-rule="evenodd"></path>
               </svg>
            </li>
            <li class="inline-flex items-center">
               <a href="#" class="text-teal-400">
                  Instrução Normativa
               </a>
            </li>
         </ul>

         <hr class="border-b-2 border-lightBlue-300 mb-6 mx-4">

         <table
            class='mx-auto mb-24 max-w-sm md:max-w-full rounded-lg bg-white divide-y divide-lightBlue-200 overflow-hidden text-center'>

            <tr class="text-lightBlue-800 text-center bg-lightBlue-50">
               <th class="font-medium text-sm uppercase tracking-wider px-6 py-4 text-center" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">
                  INSTRUÇÃO NORMATIVA
               </th>
            </tr>

            <tbody class="divide-y divide-lightBlue-200">
               <tr>
                  <td class="px-6 py-4 text-left text-sm">
                     <i class="far fa-file-pdf mr-1"></i>
                     <a href="{{ asset('docs/instrucaonormativa/IN 03 - 20 de abril de 2020 - CONTROL RN.pdf') }}"
                        class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        Instrução Normativa Nº 03, de 20 de abril de 2020 - CONTROL/RN
                     </a>
                  </td>
               </tr>
               <tr>
                  <td class="px-6 py-4 text-left text-sm">
                     <i class="far fa-file-pdf mr-1"></i>
                     <a href="{{ asset('docs/instrucaonormativa/Cartilha - IN 03-2020 0 CONTROL RN.pdf') }}"
                        class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        Cartilha 03-2020
                     </a>
                  </td>
               </tr>
            </tbody>
         </table>

      </div>
   </div>
</section>
@endsection
