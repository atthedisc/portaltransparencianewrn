@extends('layouts/covid.app')

@section('title')
- Compras e Serviços
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/exportfiles.css') }}">
@endsection

@section('js')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
@endsection

@section('content')
<h1 class="text-lg text-center text-gray-900 tracking-widest font-medium title-font pb-2 uppercase" name="texto"
   style="font-size: 1.125rem; line-height: 2rem;">
   <i class="fas fa-medkit"></i>
   Covid 19 - Compras e Serviços
</h1>
<ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto"
   style="font-size: 0.875rem; line-height: 1.25rem;">
   <li class="inline-flex items-center">
      <a href="{{ url('/covid') }}">Inicio</a>
      <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
         <path fill-rule="evenodd"
            d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
            clip-rule="evenodd"></path>
      </svg>
   </li>
   <li class="inline-flex items-center">
      <a href="#" class="text-teal-400">Covid 19 - Compras e Serviços</a>
   </li>
</ul>

<hr class="border-b-2 border-lightBlue-300 mb-10 mx-4">

<figure class="highcharts-figure">
   <div id="container"></div>
   <p class="highcharts-description">
   </p>
</figure>

<div class="flex flex-col pt-8">
   <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
      <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
         <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
            <table class="min-w-full divide-y divide-blueGray-200">
               <thead class="bg-blueGray-50">
                  <tr>
                     <th scope="col"
                        class="px-6 py-3 text-left text-xs font-semibold text-blueGray-600 uppercase tracking-wider"
                        name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                        id
                     </th>
                     <th scope="col"
                        class="px-6 py-3 text-left text-xs font-semibold text-blueGray-600 uppercase tracking-wider"
                        name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                        orgão contratante
                     </th>
                     <th scope="col"
                        class="px-6 py-3 text-left text-xs font-semibold text-blueGray-600 uppercase tracking-wider"
                        name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                        sigla
                     </th>
                     <th scope="col"
                        class="px-6 py-3 text-left text-xs font-semibold text-blueGray-600 uppercase tracking-wider"
                        name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                        valor total de contratos (r$)
                     </th>
                  </tr>
               </thead>
               <tbody class="bg-white divide-y divide-blueGray-200 text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">

                  @foreach ($dados as $dado)
                  <tr>
                     <td class="px-6 py-4 whitespace-nowrap">
                        {{ $dado->id }}
                     </td>
                     <td class="px-6 py-4 whitespace-nowrap">
                        {{ $dado->orgaocontratante }}
                     </td>
                     <td class="px-6 py-4 whitespace-nowrap">
                        <span
                           class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-cyan-100 text-cyan-800">
                           {{ $dado->sigla }}
                        </span>
                     </td>
                     <td class="px-6 py-4 whitespace-nowrap text-center">
                        <span class="px-2 inline-flex leading-5 text-sm rounded-full bg-green-100 text-green-800">
                           R$ {{ number_format($dado->valortotalcontratos, 2, ',', '.') }}
                        </span>
                     </td>
                  </tr>
                  @endforeach
               </tbody>
            </table>
         </div>
      </div>
   </div>
</div>

<div class="flex flex-col text-center w-full">
   <h1 class="text-lg text-center text-gray-900 tracking-widest font-medium title-font pb-2 uppercase py-10"
      name="texto" style="font-size: 1.125rem; line-height: 2rem;">
      <i class="fas fa-file-invoice"></i>
      CONTRATOS
   </h1>
</div>

<div class="w-full max-w-xl mx-auto">
   <div class="px-7 bg-white shadow-lg rounded-2xl mb-5">
      <div class="flex">
         <div class="flex-auto hover:w-full group">
            <form target="_blank" action="{{ route('export-xls-compras-e-servicos') }}" method="POST"
               enctype="multipart/form-data">
               @csrf
               <button type="submit"
                  class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-green-500">
                  <span
                     class="block px-1 py-1 border border-transparent group-hover:border-green-500 rounded-full group-hover:flex-grow">
                     <i class="far fa-file-excel text-2xl pt-1"></i><span
                        class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Baixar arquivo XLS</span>
                  </span>
               </button>
            </form>
         </div>
         <div class="flex-auto hover:w-full group">
            <form target="_blank" action="{{ route('export-pdf-compras-e-servicos') }}" method="POST"
               enctype="multipart/form-data">
               @csrf
               <button type="submit"
                  class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-red-500">
                  <span
                     class="block px-1 py-1 border border-transparent group-hover:border-red-500 rounded-full group-hover:flex-grow">
                     <i class="far fa-file-pdf text-2xl pt-1"></i><span
                        class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Baixar arquivo PDF</span>
                  </span>
               </button>
            </form>
         </div>
         <div class="flex-auto hover:w-full group">
            <a href="#" onclick="window.print()"
               class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-blueGray-500">
               <span
                  class="block px-1 py-1 border border-transparent group-hover:border-blueGray-500 rounded-full group-hover:flex-grow">
                  <i class="fas fa-print text-2xl pt-1"></i><span
                     class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Imprimir arquivo</span>
               </span>
            </a>
         </div>
         <div class="flex-auto hover:w-full group">
            <a href="{{ asset('docs/dicionario-de-dados/Dicionario de Dados Abertos-RN - Compras e Serviços (04.02.2021).xlsx') }}"
               class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-indigo-500"
               target="_blank">
               <span
                  class="block px-1 py-1 border border-transparent group-hover:border-indigo-500 rounded-full group-hover:flex-grow">
                  <i class="far fa-file-alt text-2xl pt-1"></i><span
                     class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Dicionario de dados</span>
               </span>
            </a>
         </div>
         <div class="flex-auto hover:w-full group">
            <a href="https://creativecommons.org/licenses/by/4.0/?ref=chooser-v1" target="_blank"
               class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-orange-500">
               <span
                  class="block px-1 py-1 border border-transparent group-hover:border-orange-500 rounded-full group-hover:flex-grow">
                  <i class="far fa-closed-captioning text-2xl pt-1"></i>
                  <span class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Licença</span>
               </span>
            </a>
         </div>
      </div>
   </div>
</div>

{!! $busca !!}

<div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
   <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
      <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
         <table class="min-w-full divide-y divide-lightBlue-200">
            <thead class="bg-lightBlue-50">
               <tr>
                  <th></th>
                  <th scope="col"
                     class="px-6 py-3 text-left text-xs font-semibold text-lightBlue-500 uppercase tracking-wider"
                     name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                     Orgão
                  </th>
                  <th scope="col"
                     class="px-6 py-3 text-left text-xs font-semibold text-lightBlue-500 uppercase tracking-wider"
                     name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                     N. Contrato
                  </th>
                  <th scope="col"
                     class="px-6 py-3 text-left text-xs font-semibold text-lightBlue-500 uppercase tracking-wider"
                     name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                     N. Processo
                  </th>
                  <th scope="col"
                     class="px-6 py-3 text-left text-xs font-semibold text-lightBlue-500 uppercase tracking-wider"
                     name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                     Valor do Contrato (R$)
                  </th>
                  <th scope="col"
                     class="px-6 py-3 text-left text-xs font-semibold text-lightBlue-500 uppercase tracking-wider"
                     name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                     Valor Pago (R$)
                  </th>
                  <th scope="col"
                     class="px-6 py-3 text-left text-xs font-semibold text-lightBlue-500 uppercase tracking-wider"
                     name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                     Valor anulado (R$)
                  </th>
                  <th scope="col"
                     class="px-6 py-3 text-left text-xs font-semibold text-lightBlue-500 uppercase tracking-wider"
                     name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                     Data Assinatura
                  </th>
               </tr>
            </thead>
            <tbody id="table" data-resetable="false" class="bg-white divide-y divide-lightBlue-200 text-sm">
               @foreach ($tables as $tabela)
               <tr>
                  <td>
                     <svg onclick="toggleModal({{ $tabela->id }})" id="card_plus{{ $tabela->id }}"
                        xmlns="http://www.w3.org/2000/svg" class="mx-4 h-5 w-5 text-green-600 cursor-pointer"
                        viewBox="0 0 20 20" fill="currentColor">
                        <path fill-rule="evenodd"
                           d="M10 18a8 8 0 100-16 8 8 0 000 16zm1-11a1 1 0 10-2 0v2H7a1 1 0 100 2h2v2a1 1 0 102 0v-2h2a1 1 0 100-2h-2V7z"
                           clip-rule="evenodd" />
                     </svg>
                     <svg id="card_minus{{ $tabela->id }}" xmlns="http://www.w3.org/2000/svg"
                        class="hidden mx-4 h-5 w-5 text-red-600" viewBox="0 0 20 20" fill="currentColor">
                        <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM7 9a1 1 0 000 2h6a1 1 0 100-2H7z"
                           clip-rule="evenodd" />
                     </svg>

                  </td>
                  <td class="px-6 py-4 whitespace-nowrap" name="texto" style="font-size: 1rem; line-height: 1.5rem;">
                     {{ $tabela->contratante }}
                  </td>
                  <td class="px-6 py-4 whitespace-normal" name="texto" style="font-size: 1rem; line-height: 1.5rem;">
                     <a href="{{ asset('docs/compras-e-servicos/ncontratos/'.$tabela->ncontratolink) }}" target="_blank"
                        name="texto" style="font-size: 1rem; line-height: 1.5rem;">
                        <span
                           class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-lightBlue-100 text-lightBlue-500"
                           name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                           {{ $tabela->ncontrato }}
                        </span>
                     </a>
                  </td>
                  <td class="px-6 py-4 whitespace-nowrap text-xs" name="texto"
                     style="font-size: 0.75rem; line-height: 1rem;">
                     {{ $tabela->nprocesso }}
                  </td>
                  <td class="px-6 py-4 whitespace-nowrap">
                     <span
                        class="px-2 inline-flex leading-5 text-xs font-semibold rounded-full bg-blueGray-100 text-blueGray-800"
                        name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                        R$ {{ number_format($tabela->valordocontrato, 2, ',', '.') }}
                     </span>
                  </td>
                  <td class="px-6 py-4 whitespace-nowrap">
                     <span
                        class="px-2 inline-flex leading-5 text-xs font-semibold rounded-full bg-emerald-100 text-emerald-800"
                        name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                        R$ {{ number_format($tabela->valorpagors, 2, ',', '.') }}
                     </span>
                  </td>
                  <td class="px-6 py-4 whitespace-nowrap">
                     <span class="px-2 inline-flex leading-5 text-xs font-semibold rounded-full bg-red-100 text-red-800"
                        name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                        R$ {{ number_format($tabela->anulacaors, 2, ',', '.') }}
                     </span>
                  </td>
                  <td class="px-6 py-4 whitespace-nowrap text-xs" name="texto"
                     style="font-size: 0.75rem; line-height: 1rem;">
                     {{ $tabela->dataassinatura }}
                  </td>
               </tr>

               <!--Modal-->
               <div id="modal{{ $tabela->id }}"
                  class="modal opacity-0 pointer-events-none fixed w-full h-full top-0 left-0 flex items-center justify-center">
                  <div class="modal-overlay absolute w-full h-full bg-gray-900 opacity-50"></div>

                  <div
                     class="modal-container bg-white w-11/12 md:max-w-2xl mx-auto rounded shadow-lg z-50 overflow-y-auto">
                     <div
                        class="modal-close absolute top-0 right-0 cursor-pointer flex flex-col items-center mt-4 mr-4 text-white text-sm z-50">
                        <svg onclick="toggleModal({{ $tabela->id }})" class="fill-current text-white"
                           xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                           <path
                              d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z">
                           </path>
                        </svg>
                        <span class="text-sm" name="texto"
                           style="font-size: 0.875rem; line-height: 1.25rem;">(Esc)</span>
                     </div>

                     <!-- Add margin if you want to see some of the overlay behind the modal-->
                     <div class="modal-content py-2 pb-6 text-left px-6">

                        <!--botão X de fechar-->
                        <div class="flex justify-end items-center">
                           <div class="modal-close cursor-pointer z-50">
                              <svg onclick="toggleModal({{ $tabela->id }})" class="fill-current text-red-500"
                                 xmlns="http://www.w3.org/2000/svg" width="22" height="22" font-weight="bold"
                                 viewBox="0 0 18 18">
                                 <path
                                    d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z">
                                 </path>
                              </svg>
                           </div>
                        </div>

                        <!--Body-->
                        <div id={{ $tabela->id }}>
                           <div class="modal-content text-left px-6">
                              <div class="flex justify-between items-center pb-3">
                                 <p class="text-sm font-bold text-lightBlue-500" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">Mais informações</p>
                              </div>
                              <div>
                                 <p class="text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;"><b
                                       class="mr-2 text-gray-700 uppercase">Contratado(a):</b>
                                    {{ $tabela->contratado }}</p>
                                 <p class="text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;"><b
                                       class="mr-2 text-gray-700 uppercase">CPF/CNPJ/RG:</b>
                                    {{ $tabela->cnpjcpf }}</p>
                                 <p class="text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;"><b
                                       class="mr-2 text-gray-700 uppercase">Objeto:</b>{{ $tabela->objeto }}
                                 </p>
                                 <p class="text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;"><b
                                       class="mr-2 text-gray-700 uppercase">Modalidade:</b>
                                    {{ $tabela->modalidadedespesa }}</p>
                                 <p class="text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;"><b
                                       class="mr-2 text-gray-700 uppercase">Fundamento Legal:</b>
                                    {{ $tabela->fundamentolegal }}</p>
                                 <p class="text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;"><b
                                       class="mr-2 text-gray-700 uppercase">Fonte do Recurso:</b>
                              {{ $tabela->fontedorecurso }}</p>
                                 <p class="text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;"><b
                                       class="mr-2 text-gray-700 uppercase">Vigência:</b>
                                    {{ $tabela->vigencia }}</p>
                                 <p class="text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;"><b
                                       class="mr-2 text-gray-700 uppercase">Local de execução:</b>
                                    {{ $tabela->localdeexecucao }}</p>
                                 <p class="text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;"><b
                                       class="mr-2 text-gray-700 uppercase">Entrega do bem/serviço - Situação:</b>
                                    {{ $tabela->situacao }}</p>                                    
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               @endforeach
            </tbody>
         </table>
         <div id="pag">{{ $tables->links() }}</div>
      </div>
   </div>
</div>
{{-- TABELA --}}

<div class="pt-4">
   <div class="p-2 text-blueGray-800 uppercase tracking-wider text-xs text-right" name="texto"
      style="font-size: 0.75rem; line-height: 1rem;">
      <i class="fas fa-arrow-up"></i>
      Dados atualizados em: 03/05/2024
   </div>
</div>

<meta id="info" data-source="compras_servicos" data-page="compras_servicos" data-reloadJs="false">

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="{{ asset('js/select2.full.min.js') }}"></script>

<script src="{{ asset('js/search.js') }}"></script>

<style>
   .modal {
      transition: opacity 0.25s ease;
      z-index: 200;
   }

   body.modal-active {
      overflow-x: hidden;
      overflow-y: visible !important;
   }
</style>

<script>
   function toggleModal (id) {
       const plus = document.querySelector('#card_plus' + id)
       const minus = document.querySelector('#card_minus' + id)

       if (plus.classList.contains('hidden')) {
           // Show button
           plus.classList.remove('hidden')
           plus.classList.add('block')

           // Delete button Minus
           minus.classList.add('hidden')
           minus.classList.remove('block')
       } else {
           // Delete button Plus
           plus.classList.add('hidden')
           plus.classList.remove('block')

           // Show button Minus
           minus.classList.add('block')
           minus.classList.remove('hidden')
       }

       const body = document.querySelector('body')
       const modal = document.querySelector('#modal' + id)
       modal.classList.toggle('opacity-0')
       modal.classList.toggle('pointer-events-none')
       body.classList.toggle('modal-active')

       document.onkeydown = function(evt) {
           evt = evt || window.event
           var isEscape = false

           if ("key" in evt) {
               isEscape = (evt.key === "Escape" || evt.key === "Esc")
           } else {
               isEscape = (evt.keyCode === 27)
           }

           if (isEscape && document.body.classList.contains('modal-active')) {
               toggleModal(id)
           }
       };
   }
</script>

<script>
   const formatNumber = new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' })
   Highcharts.setOptions({
      lang: {
         drillUpText: `◁ Voltar`
      }
   });

   Highcharts.chart('container', {
   chart: {
         type: 'column',
         spacingBottom: 30
      },

   credits : {
      style: {
      color: '#333'
      }
   },
    title: {
        text: 'VALOR TOTAL DE CONTRATOS (R$)'
    },
    subtitle: {
        text: ''
    },
    accessibility: {
        announceNewData: {
            enabled: true
        }
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Valor total de Contratos R$'
        }, labels : {
              enabled: true,
              formatter: function() {
                 return formatNumber.format(this.value ** 3)
              }
           }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
           series: {
               borderWidth: 0,
               dataLabels: {
                   enabled: true,
                   formatter: function() {
                      return formatNumber.format(this.point.y ** 3)
                   }
               }
           }
       },

    tooltip: {
      formatter: function() {
              return '<span style="font-size:11px">' + this.series.name + '</span><br>' +
              '<span style="color:' + this.point.color + '">' + this.point.name + '</span>: <b>' +
              formatNumber.format(this.point.y ** 3) + '</b> (Valor total contrato)<br/>'
            }
    },

     series: [
       {
         name: "Gráfico",
         colorByPoint: true,
         data: [
            @foreach($dados as $dado)
            {
               name: `{{ $dado->sigla }}`,
                       @if($dado->valortotalcontratos >= 0)
                          y: Math.pow({{ $dado->valortotalcontratos }}, 1/3),
                       @else
                          y: (Math.pow(({{ $dado->valortotalcontratos }}*(-1)), 1/3))*(-1),
                       @endif
               drilldown: '{{ $dado->id }}'
           },
           @endforeach
         ]
       }
     ],
     drilldown: {
      activeAxisLabelStyle: {
            textDecoration: 'none',
            color: '#666',
            fontWeight: 'normal'
        },

      activeDataLabelStyle: {
            textDecoration: 'none',
            color: '#000'
       },

       series: [
           @foreach($dados as $dado)
           {
              name: `{{ $dado->sigla }}`,
              id: '{{ $dado->id }}',
              data: [
                 [
                    `{{ $dado->orgaocontratante }}`,
                       @if($dado->valortotalcontratos >= 0)
                           Math.pow({{ $dado->valortotalcontratos }}, 1/3),
                       @else
                           (Math.pow(({{ $dado->valortotalcontratos }}*(-1)), 1/3))*(-1),
                       @endif
                 ],
              ]
           },
           @endforeach
       ]
     }
   });
</script>

@endsection