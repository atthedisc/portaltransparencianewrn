@extends('layouts.app')

@section('title')
- Ordem Cronológica
@endsection

@section('content')
<h1 class="text-lg text-gray-900 tracking-widest font-medium title-font uppercase text-center pb-2" name="texto"
   style="font-size: 1.125rem; line-height: 2rem;">
   <i class="fas fa-coins"></i>
   Ordem Cronológica de Pagamento
</h1>

<ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto"
   style="font-size: 0.875rem; line-height: 1.25rem;">
   <li class="inline-flex items-center">
      <a href="{{ url('/') }}">Inicio</a>
      <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
         <path fill-rule="evenodd"
            d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
            clip-rule="evenodd"></path>
      </svg>
   </li>
   <li class="inline-flex items-center">
      <a href="#" class="text-teal-400">Ordem Cronológica</a>
   </li>
</ul>

<hr class="border-b-2 border-lightBlue-300 mb-12 mx-4">

<div>
   <form action="{{ route('ordem-cronologica') }}" method="POST">
      @csrf
      <div class="flex flex-wrap -mx-3 mb-2">

         <div class="w-full md:w-1/4 px-4 mb-6 md:mb-0">
            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-state"
               name="texto" style="font-size: 0.75rem; line-height: 1rem;">
               Mês
            </label>
            <div id="select_mes" class="relative" name="texto" style="font-size: 1rem; line-height: 1.5rem;">
               <select id="mes" name="mes" onchange="this.form.submit()"
                  class="block appearance-none w-full border border-blue-400 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-blue-700">
                  @foreach ($meses as $mes)
                     @if ($mes == $mes_selected)
                        @if ($mes == null)
                           <option disabled selected>Escolha...</option>
                        @elseif ($mes == 'Todos')
                           <option value="{{ $mes_selected }}" selected> {{ $mes }}</option>
                        @else
                           <option value="{{ $mes_selected }}" selected>
                              @lang('meses.'.$mes)
                           </option>
                        @endif
                     @else
                        @if ($mes == null)
                           <option disabled selected>Escolha...</option>
                        @elseif ($mes == 'Todos')
                           <option value="{{ $mes }}"> {{ $mes }}</option>
                        @else
                           <option value="{{ $mes }}">@lang('meses.'.$mes)</option>
                        @endif
                     @endif
                  @endforeach
               </select>
               <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                  <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                     <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                  </svg>
               </div>
            </div>
         </div>

         <div class="w-full md:w-1/4 px-4 mb-6 md:mb-0">
            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-state"
               name="texto" style="font-size: 0.75rem; line-height: 1rem;">
               Ano
            </label>
            <div id="select_ano" class="relative" name="texto" style="font-size: 1rem; line-height: 1.5rem;">
               <select id="ano" name="ano"
                  class="block appearance-none w-full border border-blue-400 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-blue-700">
                  @foreach ($anos as $ano)
                     @if ($ano == $ano_selected)
                        @if ($ano == null)
                           <option disabled selected>Escolha...</option>
                        @else
                           <option value="{{ $ano_selected }}" selected>{{ $ano }}</option>
                        @endif
                     @else
                        @if ($ano == null)
                           <option disabled selected>Escolha...</option>
                        @else
                           <option value="{{ $ano }}">{{ $ano }}</option>
                        @endif
                     @endif
                  @endforeach
               </select>
               <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                  <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                     <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                  </svg>
               </div>
            </div>
         </div>

         <div class="w-full md:w-1/4 px-4 mb-6 md:mb-0">
            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-state"
               name="texto" style="font-size: 0.75rem; line-height: 1rem;">
               Orgão
            </label>
            <div id="select_orgao" class="relative" name="texto" style="font-size: 1rem; line-height: 1.5rem;">
               <select id="orgao" name="orgao" onchange="this.form.submit()"
                  class="block appearance-none w-full border border-blue-400 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-blue-700">
                  @foreach ($orgaos as $orgao)
                     @if ($orgao->id == $orgao_selected)
                        <option value="{{ $orgao_selected }}" selected> {{ $orgao->nomeorgao}} </option>
                     @else
                        <option value="{{ $orgao->id }}">
                           {{ $orgao->nomeorgao }}
                        </option>
                     @endif
                  @endforeach
               </select>
               <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                  <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                     <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                  </svg>
               </div>
            </div>
         </div>

         <div class="w-full md:w-1/4 px-4 mb-6 md:mb-0">
            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-state"
               name="texto" style="font-size: 0.75rem; line-height: 1rem;">
               Fonte de Recursos
            </label>
            <div id="select_fonte" class="relative" name="texto" style="font-size: 1rem; line-height: 1.5rem;">
               <select id="fonte" name="fonte"
                  class="block appearance-none w-full border border-blue-400 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-blue-700">
                  @foreach ($fontes as $fonte)
                     @if ($fonte->CDFONTE == $fonte_selected)
                        <option value="{{ $fonte_selected }}" selected> {{ $fonte->NMFONTE}} </option>
                     @else
                        <option value="{{ $fonte->CDFONTE }}">{{ $fonte->NMFONTE }}</option>
                     @endif
                  @endforeach
               </select>
               <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                  <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                     <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                  </svg>
               </div>
            </div>
         </div>
      </div>

      <div class="flex justify-center pt-4">
         <input class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded cursor-pointer"
            type="submit" value="Consultar" />
      </div>
   </form>
</div>

@if ($mes_selected && $ano_selected && $fonte_selected && $orgao_selected)
   <div class="w-full max-w-xl mx-auto pt-3">
      <div class="px-7 bg-white shadow-lg rounded-2xl mb-5">
         <div class="flex">
            <div class="flex-auto hover:w-full group">
                  <form target="_blank"
                     action="{{ route('export-xls-ordem-cronologica', [
                        'mes' => $mes_selected, 
                        'ano' => $ano_selected,
                        'orgao' => $orgao_selected,
                        'fonte' => $fonte_selected,
                     ]) }}"
                     method="POST" enctype="multipart/form-data">
                     @csrf
                     <button type="submit"
                        class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-green-500">
                        <span
                              class="block px-1 py-1 border border-transparent group-hover:border-green-500 rounded-full group-hover:flex-grow">
                              <i class="far fa-file-excel text-2xl pt-1"></i><span
                                 class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Baixar arquivo XLS</span>
                        </span>
                     </button>
                  </form>
            </div>
            <div class="flex-auto hover:w-full group">
                  <form target="_blank"
                     action="{{ route('export-pdf-ordem-cronologica', [
                        'mes' => $mes_selected, 
                        'ano' => $ano_selected,
                        'orgao' => $orgao_selected,
                        'fonte' => $fonte_selected,
                     ]) }}"
                     method="POST" enctype="multipart/form-data">
                     @csrf
                     <button type="submit"
                        class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-red-500">
                        <span
                              class="block px-1 py-1 border border-transparent group-hover:border-red-500 rounded-full group-hover:flex-grow">
                              <i class="far fa-file-pdf text-2xl pt-1"></i><span
                                 class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Baixar arquivo PDF</span>
                        </span>
                     </button>
                  </form>
            </div>
            <div class="flex-auto hover:w-full group">
                  <a href="#" onclick="window.print()"
                     class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-blueGray-500">
                     <span
                        class="block px-1 py-1 border border-transparent group-hover:border-blueGray-500 rounded-full group-hover:flex-grow">
                        <i class="fas fa-print text-2xl pt-1"></i><span
                              class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Imprimir arquivo</span>
                     </span>
                  </a>
            </div>
            <div class="flex-auto hover:w-full group">
                  <a href="{{ asset('docs/dicionario-de-dados/Dicionario de Dados Abertos-RN - Receita.xlsx') }}"
                     class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-indigo-500"
                     target="_blank">
                     <span
                        class="block px-1 py-1 border border-transparent group-hover:border-indigo-500 rounded-full group-hover:flex-grow">
                        <i class="far fa-file-alt text-2xl pt-1"></i><span
                              class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Dicionario de dados</span>
                     </span>
                  </a>
            </div>
            <div class="flex-auto hover:w-full group">
                  <a href="https://creativecommons.org/licenses/by/4.0/?ref=chooser-v1" target="_blank"
                     class="flex items-center justify-center text-center mx-auto px-4 py-2 group-hover:w-full text-orange-500">
                     <span
                        class="block px-1 py-1 border border-transparent group-hover:border-orange-500 rounded-full group-hover:flex-grow">
                        <i class="far fa-closed-captioning text-2xl pt-1"></i>
                        <span class="hidden group-hover:inline-block ml-3 align-bottom pb-1">Licença</span>
                     </span>
                  </a>
            </div>
         </div>
      </div>
   </div>
@endif

{!! $busca !!}

<div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
   <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
      <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
         <table id="myTable" class="min-w-full divide-y divide-lightBlue-200">
            <thead class="bg-lightBlue-50">
               <tr>
                  <th></th>
                  <th class="px-6 py-3 text-left text-xs font-medium text-lightBlue-700 uppercase tracking-wider"
                     name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                     Unidade
                     Gestora
                  </th>
                  <th class="px-6 py-3 text-left text-xs font-medium text-lightBlue-700 uppercase tracking-wider"
                     name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                     Numero
                     Processo
                  </th>
                  <th class="px-6 py-3 text-left text-xs font-medium text-lightBlue-700 uppercase tracking-wider"
                     name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                     Credor
                  </th>
                  <th class="px-6 py-3 text-left text-xs font-medium text-lightBlue-700 uppercase tracking-wider"
                     name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                     Data da
                     Autuação
                  </th>
                  <th class="px-6 py-3 text-left text-xs font-medium text-lightBlue-700 uppercase tracking-wider"
                     name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                     Data
                     Atesto
                  </th>
               </tr>
            </thead>
            <tbody id="table" data-resetable="false" class=" bg-white divide-y divide-lightBlue-200 text-sm" name="texto"
               style="font-size: 0.875rem; line-height: 1.25rem;">
               @foreach ($dados as $key => $dado)
               <tr>
                  <td>
                     <svg onclick="toggleModal({{ $key }})" id="card_plus{{ $key }}" xmlns="http://www.w3.org/2000/svg"
                        class="mx-4 h-5 w-5 text-green-600 cursor-pointer" viewBox="0 0 20 20" fill="currentColor">
                        <path fill-rule="evenodd"
                           d="M10 18a8 8 0 100-16 8 8 0 000 16zm1-11a1 1 0 10-2 0v2H7a1 1 0 100 2h2v2a1 1 0 102 0v-2h2a1 1 0 100-2h-2V7z"
                           clip-rule="evenodd" />
                     </svg>
                     <svg id="card_minus{{ $key }}" xmlns="http://www.w3.org/2000/svg"
                        class="hidden mx-4 h-5 w-5 text-red-600" viewBox="0 0 20 20" fill="currentColor">
                        <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM7 9a1 1 0 000 2h6a1 1 0 100-2H7z"
                           clip-rule="evenodd" />
                     </svg>
                  </td>
                  <td class="px-6 py-4 whitespace-normal">{{ $dado->NMUNIDADEGESTORA }}</td>
                  <td class="px-6 py-4 whitespace-nowrap">{{ $dado->NUPROCESSO }}</td>
                  <td class="px-6 py-4 whitespace-normal">{{ $dado->NMCREDOR }}</td>
                  <td class="px-6 py-4 whitespace-nowrap">{{ date('d/m/Y' , strtotime($dado->DTAUTUACAO)) }}</td>
                  <td class="px-6 py-4 whitespace-nowrap">{{ date('d/m/Y' , strtotime($dado->ATESTO)) }}</td>
                  <td>
                     <!--Modal-->
                     <div id="modal{{ $key }}"
                        class="modal opacity-0 pointer-events-none fixed w-full h-full top-0 left-0 flex items-center justify-center">
                        <div class="modal-overlay absolute w-full h-full bg-gray-900 opacity-50"></div>

                        <div
                           class="modal-container bg-white w-11/12 md:max-w-2xl mx-auto rounded shadow-lg z-50 overflow-y-auto">
                           <div
                              class="modal-close absolute top-0 right-0 cursor-pointer flex flex-col items-center mt-4 mr-4 text-white text-sm z-50"
                              name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                              <svg onclick="toggleModal({{ $key }})" class="fill-current text-white"
                                 xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                                 <path
                                    d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z">
                                 </path>
                              </svg>
                              <span class="text-sm" name="texto"
                                 style="font-size: 0.875rem; line-height: 1.25rem;">(Esc)</span>
                           </div>

                           <!-- Add margin if you want to see some of the overlay behind the modal-->
                           <div class="modal-content py-2 pb-6 text-left px-6">

                              <!--botão X de fechar-->
                              <div class="flex justify-end items-center">
                                 <div class="modal-close cursor-pointer z-50">
                                    <svg onclick="toggleModal({{ $key }})" class="fill-current text-red-500"
                                       xmlns="http://www.w3.org/2000/svg" width="22" height="22" font-weight="bold"
                                       viewBox="0 0 18 18">
                                       <path
                                          d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z">
                                       </path>
                                    </svg>
                                 </div>
                              </div>

                              <!--Body-->
                              <div id={{ $key }}>
                                 <div class="modal-content text-left px-6">
                                    <div class="flex justify-between items-center pb-3">
                                       <p class="text-sm font-bold text-lightBlue-500" name="texto"
                                          style="font-size: 0.875rem; line-height: 1.25rem;">Mais informações</p>
                                    </div>
                                    <div>
                                       {{-- {{ setlocale(LC_MONETARY, 'pt_BR') }} --}}
                                       <p class="text-sm" name="texto"
                                          style="font-size: 0.875rem; line-height: 1.25rem;">
                                          <b class="mr-2 text-gray-700">Valor
                                             Liquidação:</b>{{ 'R$'.number_format($dado->VLLIQUIDO, 2, ',', '.') }}
                                       </p>
                                       <p class="text-sm" name="texto"
                                          style="font-size: 0.875rem; line-height: 1.25rem;">
                                          <b class="mr-2 text-gray-700">Data
                                             Vencimento:</b>{{ date('d/m/Y' , strtotime($dado->DTVENCIMENTO)) }}
                                       </p>
                                       <p class="text-sm" name="texto"
                                          style="font-size: 0.875rem; line-height: 1.25rem;">
                                          <b class="mr-2 text-gray-700">Data
                                             Pagamento:</b>{{ date('d/m/Y' , strtotime($dado->DTPAGAMENTO)) }}
                                       </p>
                                       <p class="text-sm" name="texto"
                                          style="font-size: 0.875rem; line-height: 1.25rem;">
                                          <b class="mr-2 text-gray-700">Valor
                                             Pagamento:</b>{{ 'R$'.number_format($dado->VLPREPARACAOPAGAMENTO, 2, ',',
                                          '.')
                                          }}
                                       </p>
                                       <p class="text-sm" name="texto"
                                          style="font-size: 0.875rem; line-height: 1.25rem;">
                                          <b class="mr-2 text-gray-700">Procedimento
                                             Licitatório:</b>{{ $dado->NMMODALIDADE }}
                                       </p>
                                       <p class="text-sm" name="texto"
                                          style="font-size: 0.875rem; line-height: 1.25rem;">
                                          <b class="mr-2 text-gray-700">Num.
                                             Empenho:</b>{{ $dado->NUNOTAEMPENHO }}
                                       </p>
                                       <p class="text-sm" name="texto"
                                          style="font-size: 0.875rem; line-height: 1.25rem;">
                                          <b class="mr-2 text-gray-700">Fonte de
                                             Recurso:</b>{{ $dado->NMFONTE }}
                                       </p>
                                       <p class="text-sm" name="texto"
                                          style="font-size: 0.875rem; line-height: 1.25rem;">
                                          <b class="mr-2 text-gray-700">Ordenador:</b>{{ $dado->ORDENADORDESPESA }}
                                       </p>
                                       <p class="text-sm" name="texto"
                                          style="font-size: 0.875rem; line-height: 1.25rem;">
                                          <b class="mr-2 text-gray-700">Justif. de Quebra Ordem Cronológica:</b>
                                          @if (substr($dado->JUSTIFICATIVAQUEBRAORDEM, 0, 8) == '*Justi:*')
                                             {{ substr($dado->JUSTIFICATIVAQUEBRAORDEM, 8) }}
                                          @elseif(date('Y', strtotime($dado->DTNOTAEMPENHO)) >= 2023)
                                             {{ $dado->JUSTIFICATIVAQUEBRAORDEM }}
                                          @endif
                                       </p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </td>
               </tr>
               @endforeach
            </tbody>
         </table>
         <div id="pag">
            @if ($dados->isEmpty())
            <p class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" name="texto"
               style="font-size: 0.75rem; line-height: 1rem;">Não há dados
               cadastrados para essa busca...</p>
            @endif
            {{ $dados->appends(Request::all())->links() }}
         </div>
      </div>
   </div>
</div>
<style>
   .modal {
      transition: opacity 0.25s ease;
      z-index: 200;
   }

   body.modal-active {
      overflow-x: hidden;
      overflow-y: visible !important;
   }
</style>

<meta id="info" data-source="ordem_cronologica" data-page="ordem_cronologica">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="{{ asset('js/select2.full.min.js') }}"></script>


{{-- Busca --}}
<script src="{{ asset('js/search.js') }}"></script>

{{-- Modal --}}
<script>
   function toggleModal (id) {
       const plus = document.querySelector('#card_plus' + id)
       const minus = document.querySelector('#card_minus' + id)

       if (plus.classList.contains('hidden')) {
           // Show button
           plus.classList.remove('hidden')
           plus.classList.add('block')

           // Delete button Minus
           minus.classList.add('hidden')
           minus.classList.remove('block')
       } else {
           // Delete button Plus
           plus.classList.add('hidden')
           plus.classList.remove('block')

           // Show button Minus
           minus.classList.add('block')
           minus.classList.remove('hidden')
       }

       const body = document.querySelector('body')
       const modal = document.querySelector('#modal' + id)
       modal.classList.toggle('opacity-0')
       modal.classList.toggle('pointer-events-none')
       body.classList.toggle('modal-active')

       document.onkeydown = function(evt) {
           evt = evt || window.event
           var isEscape = false

           if ("key" in evt) {
               isEscape = (evt.key === "Escape" || evt.key === "Esc")
           } else {
               isEscape = (evt.keyCode === 27)
           }

           if (isEscape && document.body.classList.contains('modal-active')) {
               toggleModal(id)
           }
       };
   }
</script>

@endsection