@extends('layouts.app')

@section('css')
    <style>
        .div-com-padding {
            padding-bottom: 15px;
        }

        p {
            padding-bottom: 15px;
        }
    </style>
@endsection

@section('title')
- LGPD
@endsection

@section('content')
    <!-- Section -->
    <section class="text-gray-600 mt-16 body-font">
        <div class="container mx-auto">
            <ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto"
                style="font-size: 0.875rem; line-height: 1.25rem;">
                <li class="inline-flex items-center">
                    <a href="{{ url('/') }}">Inicio</a>
                    <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                        <path fill-rule="evenodd"
                            d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                            clip-rule="evenodd"></path>
                    </svg>
                </li>
                <li class="inline-flex items-center">
                    <a class="text-teal-400" href="{{ route('lgpd')}}">LGPD</a>
                </li>
            </ul>
            <hr class="border-b-2 border-lightBlue-300 mb-12 mx-4">
            <div class="flex flex-col w-full">
                <h1 class="text-3xl azulclaro font-semibold tracking-widest font-medium title-font mb-8">Proteção de Dados
                    Pessoais</h1>

                <h2 class="div-com-padding" style="font-weight: bold;">Aviso de Privacidade</h2>

                <p>
                    A proteção dos seus dados pessoais é muito importante para nós. Para honrar esse compromisso, elaboramos
                    este Aviso de Privacidade que contém informações sobre como e quais dados pessoais tratamos, por quais
                    motivos e com quem os compartilhamos. Além disso, você encontrará nesse Aviso quais são os seus direitos
                    relativos a essas informações e como exercê-los junto ao Portal da Transparência do Rio Grande do Norte.
                </p>

                <p>
                    Ao utilizar o Portal da Transparência do Rio Grande do Norte você declara estar ciente com todo o
                    conteúdo deste Aviso de Privacidade, autorizando o Portal da Transparência, de forma livre, expressa e
                    informada a coletar, usar, armazenar, compartilhar e excluir os dados e informações nos termos e
                    condições estabelecidos neste Aviso. Para acessar o Aviso de Privacidade do Portal da Transparência do
                    Rio Grande do Norte acesse o link <a class="text-blue-400 hover:text-blue-600"
                        href="{{ asset('/docs/lgpd/Aviso_de_privacidade.pdf') }}" target="_blank" rel="noopener noreferrer"
                        name="texto" style="font-size: 1rem; line-height: 1.5rem;">Aviso de
                        Privacidade</a>.
                </p>

                <h2 class="div-com-padding" style="font-weight: bold;">Encarregado de Dados</h2>

                <p>
                    O Encarregado pelo tratamento de dados pessoais é uma pessoa nomeada pela CONTROL/RN.
                </p>

                <p>
                    As atividades do encarregado consistem em:
                </p>

                <p>
                    I - manter continuamente atualizados a análise de risco e o relatório de impacto à proteção de dados
                    pessoais, quando necessário, apontando a adoção de padrões e de boas práticas para os tratamentos de
                    dados pessoais pelo poder público;
                </p>

                <p>
                    II - aceitar reclamações e comunicações dos titulares de dados pessoais, prestar esclarecimentos e
                    adotar providências;
                </p>

                <p>
                    III - receber comunicações da Autoridade Nacional de Proteção de Dados - ANPD e adotar providências;
                </p>

                <p>
                    IV - orientar os servidores, funcionários e os contratados a respeito das práticas a serem tomadas em
                    relação à proteção de dados pessoais;
                </p>

                <p>
                    V - realizar, em colaboração com a Instância de Controle e com o Comitê Gestor de Dados e Informações do
                    Estado, o mapeamento dos processos de tratamento de dados pessoais realizados no âmbito do órgão ou da
                    entidade estadual, inclusive dos compartilhamentos com entidades públicas ou privadas, propondo
                    adequações à luz da LGPD;
                </p>

                <p>
                    VI - seguir as orientações exaradas pela Instância de Controle e pelo Comitê Gestor de Dados e
                    Informações do Estado, bem como apoiá-los por meio de todas as informações necessárias para o
                    cumprimento de suas atribuições;
                </p>

                <p>
                    VII - atender às normas complementares da Autoridade Nacional de Proteção de Dados - ANPD;
                </p>

                <p>
                    VIII - executar outras atribuições normatizadas.
                </p>

                <p>
                    Encarregado Titular: Ivandson Praeiro De Sousa - Matrícula: 226.46-76
                </p>

                <p>
                    Encarregado Suplente: Mauricio Neves Gomes - Matrícula 240.556-3
                </p>

                <p>
                    Confira a Portaria de Nomeação na íntegra
                    <a class="text-blue-400 hover:text-blue-600" href="{{ asset('/docs/lgpd/Portaria_de_nomeação.pdf') }}"
                        target="_blank" rel="noopener noreferrer" name="texto"
                        style="font-size: 1rem; line-height: 1.5rem;">aqui</a>.
                </p>

                <p>
                    Confira a <a class="text-blue-400 hover:text-blue-600"
                        href="{{ asset('/docs/lgpd/Decreto_de_regulamentação.pdf') }}" target="_blank"
                        rel="noopener noreferrer" name="texto" style="font-size: 1rem; line-height: 1.5rem;">Decreto de
                        regulamentação</a> na íntegra
                </p>

                <p>
                    Confira a <a class="text-blue-400 hover:text-blue-600"
                        href="{{ asset('/docs/lgpd/Instrução_normativa.pdf') }}" target="_blank" rel="noopener noreferrer"
                        name="texto" style="font-size: 1rem; line-height: 1.5rem;">Instrução Normativa</a> na íntegra
                </p>

                <h2 class="div-com-padding" style="font-weight: bold;">Canais de Comunicação:</h2>

                <p>
                    Para entrar diretamente em contato conosco, use o canal abaixo:
                </p>

                <p>
                    E-mail: <a class="text-blue-500" href="mailto:lgpd@control.rn.gov.br" target="_blank" rel="noopener noreferrer">lgpd@control.rn.gov.br</a>
                </p>
            </div>
        </div>
    @endsection
