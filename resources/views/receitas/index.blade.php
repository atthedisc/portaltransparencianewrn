@extends('layouts.app')

@section('title')
- Receitas
@endsection

@section('content')
<section class="text-gray-600 body-font">
   <div class="container px-5 mx-auto flex flex-wrap flex-col">
      <ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto"
         style="font-size: 0.875rem; line-height: 1.25rem;">
         <li class="inline-flex items-center">
            <a href="{{ url('/') }}">Inicio</a>
            <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
               <path fill-rule="evenodd"
                  d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                  clip-rule="evenodd"></path>
            </svg>
         </li>
         <li class="inline-flex items-center">
            <a class="text-teal-400" href="#">Receitas</a>
         </li>
      </ul>
      <hr class="border-b-2 border-lightBlue-300 mb-12 mx-4">
      <div id="tabs" class="flex text-center mx-auto flex-wrap mb-10">
         <span class="">
            <a id="default-tab" href="#first"
               class="sm:px-6 py-3 w-1/2 sm:w-auto justify-center sm:justify-start border-b title-font font-medium inline-flex items-center leading-none hover:text-green-600 tracking-wider rounded-t">
               Receitas
            </a>
         </span>
      </div>
      <section id="tab-contents" class="text-gray-600 body-font relative">
         <div id="first" class="lg:w-2/3 container px-5 mx-auto bg-white p-12 rounded-md shadow-lg">
            <div class="flex flex-col text-center w-full mb-12">
               <h1 class="text-xl font-semibold mb-4 text-green-500 uppercase" name="texto"
                  style="font-size: 1.25rem; line-height: 1.75rem;">Receitas</h1>
               <p class="mx-auto leading-relaxed text-base" name="texto" style="font-size: 1rem; line-height: 1.5rem;">A
                  consulta "Receitas" corresponde aos recursos financeiros
                  que o Estado arrecada, em sua maioria sob a forma de impostos, para atender os gastos com serviços,
                  obras, compras e salários dos servidores.
                  Use a pesquisa para acompanhar, em detalhes, as previsões e as receitas realizadas do Governo do RN.
                  Os dados apresentados nesta consulta não contemplam os valores inerentes aos poderes Legislativo e
                  Judiciário, como também ao Ministério Público e Tribunal de Contas da esfera estadual.
               </p>
            </div>
            <div class="mx-auto">
               <form action="{{ route('receita-prevista') }}" method="POST">
                  @csrf
                  <div class="grid grid-cols-1 lg:grid-cols-4 gap-3 items-end">
                     <span>
                        <label for="posicao" class="block text-xs font-semibold text-green-600 uppercase" name="texto"
                           style="font-size: 0.75rem; line-height: 1rem;">Posição a ser Consultada
                        </label>
                        <select id="posicao" name="posicao"
                           class="block w-full p-3 mt-2 text-gray-700 bg-white border border-gray-300 focus:outline-none focus:bg-gray-100 focus:shadow-inner">
                           <option value="Acumulado">Acumulado</option>
                           <option value="No mês">No mês</option>
                        </select>
                     </span>
                     <span>
                        <label for="mes" class="block text-xs font-semibold text-green-600 uppercase" name="texto"
                           style="font-size: 0.75rem; line-height: 1rem;">Mês de Referência
                        </label>
                        <select id="mes" name="mes"
                           class="block w-full p-3 mt-2 text-gray-700 bg-white border border-gray-300 focus:outline-none focus:bg-gray-100 focus:shadow-inner">
                           @foreach ($meses as $mes)
                           <option value="{{ $mes }}">@lang('meses.'.$mes)</option>
                           @endforeach
                        </select>
                     </span>
                     <span>
                        <label for="ano" class="block text-xs font-semibold text-green-600 uppercase" name="texto"
                           style="font-size: 0.75rem; line-height: 1rem;">Ano de Referência
                        </label>
                        <select id="ano" name="ano"
                           class="block w-full p-3 mt-2 text-gray-700 bg-white border border-gray-300 focus:outline-none focus:bg-gray-100 focus:shadow-inner">
                           <option value="2024">2024</option>
                           <option value="2023">2023</option>
			                  <option value="2022">2022</option>
                           <option value="2021">2021</option>
                           <option value="2020">2020</option>
                           <option value="2019">2019</option>
                        </select>
                     </span>
                     <span>
                        <label for="classificacao" class="block text-xs font-semibold text-green-600 uppercase"
                           name="texto" style="font-size: 0.75rem; line-height: 1rem;">Classificação
                        </label>
                        <select id="classificacao" name="classificacao"
                           class="block w-full p-3 mt-2 text-gray-700 bg-white border border-gray-300 focus:outline-none focus:bg-gray-100 focus:shadow-inner">
                           <option value="receita">Por Receita</option>
                           <option value="gestora">Por Unidade Gestora</option>
                           <option value="fonterecurso">Por Fonte</option>
                        </select>
                     </span>
                  </div>
                  <div class="pt-6">
                     <input type="submit"
                        class="cursor-pointer flex mx-auto text-white bg-green-600 border-0 py-2 px-8 focus:outline-none hover:bg-green-700 rounded text-lg"
                        value="Consultar" name="texto" style="font-size: 1.125rem; line-height: 1.75rem;" />
                  </div>
               </form>
            </div>
         </div>
      </section>
   </div>
</section>

<script>
   let tabsContainer = document.querySelector("#tabs");

   let tabTogglers = tabsContainer.querySelectorAll("a");

   tabTogglers.forEach(function(toggler) {
      toggler.addEventListener("click", function(e) {
         e.preventDefault();

         let tabName = this.getAttribute("href");
         let tabContents = document.querySelector("#tab-contents");

         for (let i = 0; i < tabContents.children.length; i++) {
            tabTogglers[i].parentElement.classList.remove("border-green-500", "border-b-4", "text-green-500");  tabContents.children[i].classList.remove("hidden");

            if ("#" + tabContents.children[i].id === tabName) {
               continue;
            }
            tabContents.children[i].classList.add("hidden");
         }
         e.target.parentElement.classList.add("border-green-500", "border-b-4", "text-green-500");
      });
   });

   document.getElementById("default-tab").click();
</script>
<script>
   document.getElementById('mes').value = {{ $mescorrente }};
</script>
@endsection
