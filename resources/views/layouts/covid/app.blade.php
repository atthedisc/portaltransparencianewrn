<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <link rel="icon" href="{{ asset('images/icons/favicon-covid-16x16.png') }}" type="image/ico">
   <title>Portal da Transparência do Rio Grande do Norte @yield('title')</title>

   {{-- TailwindCss --}}
   <link rel="stylesheet" href="{{ asset('css/app.css') }}">

   {{-- css --}}
   <link rel="stylesheet" href="{{ asset('css/main.css') }}">
   <link rel="stylesheet" href="{{ asset('css/nav.css') }}">
   <link rel="stylesheet" href="{{ asset('css/accessbar.css') }}">
   <link id="linkcontraste" rel="stylesheet" href="">

   @if(env("ENVIRONMENT") == 'testing')

   {{-- Global site tag (gtag.js) - Google Analytics --}}
   <script async src="https://www.googletagmanager.com/gtag/js?id={{ env(" G_TAG") }}"></script>
   <script>
      window.dataLayer = window.dataLayer || [];
         function gtag(){dataLayer.push(arguments);}
         gtag('js', new Date());

         gtag('config', '{{ env("G_TAG") }}');
   </script>
   @elseif (env("ENVIRONMENT") == 'production')

   {{-- Global site tag (gtag.js) - Google Analytics --}}
   <script async src="https://www.googletagmanager.com/gtag/js?id={{ env(" G_TAG_PRODUCTION") }}"></script>
   <script>
      window.dataLayer = window.dataLayer || [];
         function gtag(){dataLayer.push(arguments);}
         gtag('js', new Date());

         gtag('config', '{{ env("G_TAG_PRODUCTION") }}');
   </script>

   @endif
   <style>
      .select2-results__options {
         font-size: 0.8em;
      }
   </style>
   <script src="{{ asset('js/jquery-latest.min.js') }}"></script>
   {{-- Font-Awesome --}}
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

   @yield('css')
   @yield('js')
</head>

<noscript>
   <h1 class="text-black text-xl pl-6 pt-6">
      Esta página precisa que o javascript esteja ativado para funcionar.
   </h1>

   <style>
      #pagewrapper {
         display: none;
      }
   </style>
</noscript>

<body id="corpo" class="bg-warmGray-50 font-sans leading-normal tracking-normal">

   <div id="pagewrapper">
      <!-- Cookies -->
      <div id="cookies" class="flex fixed bottom-0 hidden z-50">
         @include('layouts.cookie')
      </div>

      <!-- Accessbar -->
      <div id="accessbar">
         @include('layouts.accessbar')
      </div>

      <!-- Nav -->
      <div id="nav">
         @include('layouts/covid.nav')
      </div>

      <!-- Spacing -->
      <div id='space'></div>

      <!-- Vlibras -->
      <div id="vlibras">
         @include('layouts.vlibras')
      </div>

      <!-- Content -->
      <div id='content' class="container-fluid-covid container w-full mx-auto py-5">
         @yield('content')
      </div>

      <!-- Footer -->
      <div id="footer">
         @include('layouts.footer')
      </div>
   </div>

   <!-- Loading -->
   <div id="load" class="hidden">
      @include('layouts.loading')
   </div>
</body>

<!-- Script -->
<script src="{{ asset('js/contpos.js') }}"></script>
<script src="{{ asset('js/pageload.js') }}"></script>
<script src="{{ asset('js/cookie.js') }}"></script>
<script src="{{ asset('js/accessbar.js') }}"></script>
<script src="https://vlibras.gov.br/app/vlibras-plugin.js"></script>
<script>
   new window.VLibras.Widget('https://vlibras.gov.br/app');
</script>

</html>