<div id="accessbar">
   <div @click.away="open = false" class="relative" x-data="{ open: false }">
      <button aria-label="Acessibilidade" @click="open = !open" href="#" class="float">
         <i class="fas fa-universal-access fa-3x"></i>
      </button>
      <div x-show="open" x-transition:enter="transition ease-out duration-300"
         x-transition:enter-start="transform opacity-0 scale-95"
         x-transition:enter-end="transform opacity-100 scale-100" x-transition:leave="transition ease-in duration-75"
         x-transition:leave-start="transform opacity-100 scale-100"
         x-transition:leave-end="transform opacity-0 scale-95"
         class="absolute z-10 w-full mt-2 origin-bottom rounded-md md:w-60" style="display:none"style="display:none;">
         <div class="float-content py-2 bg-white rounded-md">
            <a @click="open = !open" id="/nav" class="block px-4 py-2 text-sm bg-transparent hover:text-blue-900 hover:bg-blue-100" href="#nav">Cabeçalho
            </a>
            <a @click="open = !open" id="/conteudo" class="block px-4 py-2 text-sm bg-transparent hover:text-blue-900 hover:bg-blue-100" href="#content">Conteúdo
            </a>
            <a @click="open = !open" id="/rodape" class="block px-4 py-2 text-sm bg-transparent hover:text-blue-900 hover:bg-blue-100" href="#footer">Rodapé
            </a>
            <button aria-label="Aumentar Fonte" id="A+" onclick="fonte(this.id);" class="font-semibold cursor-pointer text-center text-gray-600 hover:text-blue-900 hover:bg-blue-100 px-2" style="font-size: 0.875rem;">
               A+
            </button>
            <button aria-label="Diminuir Fonte" id="A-" onclick="fonte(this.id);" class="font-semibold cursor-pointer text-center text-gray-600 hover:text-blue-900 hover:bg-blue-100 px-2" style="font-size: 0.875rem;">
               A-
            </button>
            <button aria-label="Restaurar Fonte" id="A" onclick="fonte(this.id);" class="font-semibold cursor-pointer text-center text-gray-600 hover:text-blue-900 hover:bg-blue-100 px-2" style="font-size: 0.875rem;">
               A
            </button>
            <button  aria-label="Modo alto contraste" id="Contraste" estado="false" onclick="contraste();" class="text-gray-500 hover:text-blue-900 hover:bg-blue-100 px-2 text-center" style="font-size: 0.875rem;">
               <i class="fas fa-adjust"></i>
            </button>
         </div>
      </div>
   </div>
</div>