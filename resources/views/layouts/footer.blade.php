<footer class="text-gray-600 container-fluid xl:px-6 mx-auto body-font bg-gray-300">
    <div class="flex justify-center py-12 mx-auto">
        <div class="flex w-fit justify-between 2xl:justify-center flex-wrap order-first">
            <div class="w-auto px-4 2xl:px-12 py-5">
                <a id="rodape" href="">
                    <h2 class="title-font font-medium text-gray-900 tracking-widest mb-3 uppercase" name="texto"
                        style="font-size:0.875rem;">Ajuda</h2>
                </a>
                <nav class="list-none mb-10">
                    <ol>
                        <li>
                            <a href="{{ asset('http://www.sic.rn.gov.br/') }}" class="hover:text-gray-800"
                                target="_blank" name="texto" style="font-size:1rem;">
                                e-SIC
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('faleconosco') }}" class="text-gray-600 hover:text-gray-800"
                                name="texto" style="font-size:1rem;">
                                Fale conosco
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('glossario') }}" class="text-gray-600 hover:text-gray-800" name="texto"
                                style="font-size:1rem;">
                                Glossário
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('mapadosite') }}" class="text-gray-600 hover:text-gray-800" name="texto"
                                style="font-size:1rem;">
                                Mapa do site
                            </a>
                        </li>
                        <li>
                            <a href="{{ asset('https://www12.senado.leg.br/orcamentofacil') }}"
                                class="hover:text-gray-800" target="_blank" name="texto" style="font-size:1rem;">
                                Orçamento fácil
                            </a>
                        </li>
                        <li>
                            <a href="{{ asset('https://falabr.cgu.gov.br/publico/RN/Manifestacao/RegistrarManifestacao') }}"
                                class="hover:text-gray-800" target="_blank" name="texto" style="font-size:1rem;">
                                Ouvidoria geral
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('perguntas-frequentes') }}" class="text-gray-600 hover:text-gray-800"
                                name="texto" style="font-size:1rem;">
                                Perguntas frequentes
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('lgpd') }}" class="hover:text-gray-800" target="_blank"
                                rel="noopener noreferrer" name="texto" style="font-size:1rem;">
                                Política de Privacidade
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('sobre-o-portal') }}" class="text-gray-600 hover:text-gray-800"
                                name="texto" style="font-size:1rem;">
                                Sobre o Portal
                            </a>
                        </li>
                    </ol>
                </nav>
            </div>
            <div class="w-auto px-4 2xl:px-12 py-5">
                <h2 class="title-font font-medium text-gray-900 tracking-widest mb-3 uppercase" name="texto"
                    style="font-size:0.875rem;">
                    Mais acessados
                </h2>
                <nav class="list-none mb-10">
                    <ol>
                        <li>
                            <a href="{{ asset('http://www.cidadao.rn.gov.br/') }}" class="hover:text-gray-800"
                                target="_blank" name="texto" style="font-size:1rem;">
                                Cidadão
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('ordem-cronologica') }}" class="text-gray-600 hover:text-gray-800"
                                name="texto" style="font-size:1rem;">
                                Cronologia
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('despesas') }}" class="text-gray-600 hover:text-gray-800" name="texto"
                                style="font-size:1rem;">
                                Despesa
                            </a>
                        </li>
                        <li>
                            <a href="{{ asset('http://servicos.searh.rn.gov.br/searh/Licitacao') }}"
                                class="hover:text-gray-800" target="_blank" name="texto" style="font-size:1rem;">
                                Portal de Compras
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('receita') }}" class="text-gray-600 hover:text-gray-800" name="texto"
                                style="font-size:1rem;">
                                Receita prevista
                            </a>
                        </li>
                        <li>
                            <a href="{{ asset('http://www.defesasocial.rn.gov.br/Conteudo.asp?TRAN=ITEM&TARG=54041&ACT=&PAGE=&PARM=&LBL=MAT%C9RIA') }}"
                                class="hover:text-gray-800" target="_blank" name="texto" style="font-size:1rem;">
                                Segurança
                            </a>
                        </li>
                        <li>
                            <a href="{{ asset('http://www.set.rn.gov.br/contentProducao/aplicacao/set_v2/principal/enviados/index.asp') }}"
                                class="hover:text-gray-800" target="_blank" name="texto" style="font-size:1rem;">
                                Tributos
                            </a>
                        </li>
                    </ol>
                </nav>
            </div>
            <div class="w-auto px-4 2xl:px-12 py-5">
                <h2 class="title-font font-medium text-gray-900 tracking-widest mb-3 uppercase" name="texto"
                    style="font-size:0.875rem;">
                    Demais poderes
                </h2>
                <nav class="list-none mb-10">
                    <ol>
                        <li>
                            <a href="{{ asset('http://transparencia.al.rn.leg.br/transparencia/') }}"
                                class="hover:text-gray-800" target="_blank" name="texto" style="font-size:1rem;">
                                Assembleia Legislativa
                            </a>
                        </li>
                        <li>
                            <a href="{{ asset('https://www.defensoria.rn.def.br/pagina/portal-da-transparencia') }}"
                                class="hover:text-gray-800" target="_blank" name="texto" style="font-size:1rem;">
                                Defensoria Pública</a>
                        </li>
                        <li>
                            <a href="{{ asset('http://transparencia.mprn.mp.br/') }}" class="hover:text-gray-800"
                                target="_blank" name="texto" style="font-size:1rem;">Ministério Público</a>
                        </li>
                        <li>
                            <a href="{{ asset('http://www.tce.rn.gov.br/Transparencia/Index#gsc.tab=0') }}"
                                class="hover:text-gray-800" target="_blank" name="texto" style="font-size:1rem;">
                                Tribunal de Contas</a>
                        </li>
                        <li>
                            <a href="{{ asset('http://ww4.tjrn.jus.br/portalTransparencia/despesas.aspx') }}"
                                class="hover:text-gray-800" target="_blank" name="texto" style="font-size:1rem;">
                                Tribunal de Justiça</a>
                        </li>
                    </ol>
                </nav>
            </div>
            <div class="w-auto px-4 2xl:px-12 py-5">
                <h2 class="title-font font-medium text-gray-900 tracking-widest mb-3 uppercase" name="texto"
                    style="font-size:0.875rem;">
                    Empresa Pública/Economia Mista
                </h2>
                <nav class="list-none mb-10">
                    <ol>
                        <li>
                            <a href="{{ asset('http://www.agn.rn.gov.br/') }}" class="hover:text-gray-800"
                                target="_blank" name="texto" style="font-size:1rem;">
                                AGN
                            </a>
                        </li>
                        <li>
                            <a href="{{ asset('https://caern.com.br/#/') }}" class="hover:text-gray-800"
                                target="_blank" name="texto" style="font-size:1rem;">
                                CAERN
                            </a>
                        </li>
                        <li>
                            <a href="{{ asset('http://www.ceasa.rn.gov.br/') }}" class="hover:text-gray-800"
                                target="_blank" name="texto" style="font-size:1rem;">
                                CEASA
                            </a>
                        </li>
                        <li>
                            <a href="{{ asset('http://www.cehab.rn.gov.br/') }}" class="hover:text-gray-800"
                                target="_blank" name="texto" style="font-size:1rem;">
                                CEHAB
                            </a>
                        </li>
                        <li>
                            <a href="{{ asset('http://www.datanorte.rn.gov.br/') }}" class="hover:text-gray-800"
                                target="_blank" name="texto" style="font-size:1rem;">
                                DATANORTE
                            </a>
                        </li>
                        <li>
                            <a href="{{ asset('http://www.emgern.rn.gov.br/') }}" class="hover:text-gray-800"
                                target="_blank" name="texto" style="font-size:1rem;">
                                EMGERN
                            </a>
                        </li>
                        <li>
                            <a href="{{ asset('http://www.emparn.rn.gov.br/') }}" class="hover:text-gray-800"
                                target="_blank" name="texto" style="font-size:1rem;">
                                EMPARN
                            </a>
                        </li>
                        <li>
                            <a href="{{ asset('http://www.emprotur.setur.rn.gov.br/') }}" class="hover:text-gray-800"
                                target="_blank" name="texto" style="font-size:1rem;">
                                EMPROTUR
                            </a>
                        </li>
                        <li>
                            <a href="{{ asset('https://www.potigas.com.br/') }}" class="hover:text-gray-800"
                                target="_blank" name="texto" style="font-size:1rem;">
                                POTIGÁS
                            </a>
                        </li>
                    </ol>
                </nav>
            </div>
            <div class="flex flex-col items-center 2xl:px-12 px-4 py-5 lg:w-1/5 md:w-1/1 w-full">
                <div class="flex justify-center">
                    <a href="{{ asset('https://forms.gle/er4Qc599qz7b68sQ7') }}" target="_blank">
                        <button
                            class="lg:mt-2 xl:mt-0 w-auto text-center inline-flex text-white bg-blue-500 border-0 py-2 px-6 focus:outline-none hover:bg-blue-600 rounded">
                            Dê sua opinião
                        </button>
                    </a>
                </div>
                <p class="flex text-center text-gray-600 mt-2" name="texto" style="font-size:0.875rem;">
                    Por favor deixe sua opinião para ajudar
                    a melhorar o Portal da Transparência
                </p>
            </div>
        </div>
    </div>
    <div class="flex flex-wrap justify-center">
        <div class="md:w-1/1 xl:w-1/1">
            <span class="sm:ml-auto sm:mt-0 mt-4 justify-center">
                <a aria-label="Facebook" href="{{ asset('https://pt-br.facebook.com/GovernodoRN/') }}"
                    class="text-gray-600 hover:text-blue-600" target="_blank" name="texto"
                    style="font-size:1rem;">
                    <i class="fab fa-facebook-f fa-lg"></i>
                </a>
                <a aria-label="Twitter" href="{{ asset('https://twitter.com/governodorn') }}"
                    class="ml-3 text-gray-600 hover:text-blue-400" target="_blank" name="texto"
                    style="font-size:1rem;">
                    <i class="fab fa-twitter fa-lg"></i>
                </a>
                <a aria-label="Instagram" href="{{ asset('https://www.instagram.com/control.rn/') }}"
                    class="ml-3 text-gray-600 hover:text-pink-600" target="_blank" name="texto"
                    style="font-size:1rem;">
                    <i class="fab fa-instagram fa-lg"></i>
                </a>
                <a aria-label="Email" href="{{ asset('mailto:transparenciacontrolrn@gmail.com') }}"
                    class="ml-3 text-gray-600 hover:text-red-500" target="_blank" name="texto"
                    style="font-size:1rem;">
                    <i class="far fa-envelope fa-lg"></i>
                </a>
            </span>
        </div>
        <div class="flex justify-center w-full md:w-1/1 xl:w-1/1">
            <p class="text-center pt-4" name="texto" style="font-size:0.875rem;">
                © 2022 - Portal da Transparência do Rio Grande do
                Norte - Controladoria Geral do Estado
            </p>
        </div>
        <div class="flex justify-center w-full md:w-1/1 xl:w-1/1">
            <p class="text-center" name="texto" style="font-size:0.875rem;">
                Desenvolvimento:
            </p>
        </div>
        <div class="flex justify-center w-full md:w-1/1 xl:w-1/1">
            <p class="pb-8 text-center" name="texto" style="font-size:0.875rem;">
                Controladoria Geral do Estado do Rio Grande do Norte
            </p>
        </div>
    </div>
</footer>
