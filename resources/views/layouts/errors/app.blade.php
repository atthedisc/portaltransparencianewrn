<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="{{ asset('images/icons/favicon.ico') }}" type="image/ico">
    <title>Portal da Transparência do Rio Grande do Norte @yield('title')</title>

    {{-- TailwindCss --}}
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    {{-- Links css --}}
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/nav.css') }}">
    {{-- Font-Awesome --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    @yield('css')
    @yield('js')

</head>

<body class="bg-warmGray-50 font-sans leading-normal tracking-normal">

    <div class="container-fluid container w-full mx-auto lg:pt-14 md:pt-14 pb-10">
       @yield('content')
    </div>

 </body>

</html>
