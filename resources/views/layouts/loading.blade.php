<head>
   {{-- TailwindCss --}}
   <link rel="stylesheet" href="{{ asset('css/app.css') }}">

   {{-- Links css --}}
   <link rel="stylesheet" href="{{ asset('css/loading.css') }}">
</head>

<body>
   <div wire:loading
      class="fixed top-0 left-0 right-0 bottom-0 bg-white opacity-70 w-full h-screen z-50 overflow-hidden flex flex-col items-center justify-center">
   </div>
   <div wire:loading
      class="fixed top-0 left-0 right-0 bottom-0 w-full h-screen z-50 overflow-hidden flex flex-col items-center justify-center">
      <div class="loader ease-linear opacity-100 rounded-full border-4 border-t-4 border-gray-200 h-12 w-12 mb-4"></div>
      <h2 class="text-center text-xl opacity-100 font-semibold">Carregando...</h2>
      <p class="w-1/3 text-center opacity-100">Isso pode levar alguns segundos.</p>
   </div>
</body>