@extends('layouts.app')

@section('title')
- Perguntas Frequentes
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/dropdown.css') }}">
@endsection

@section('js')
<script src="{{ asset('js/perguntas-frequentes.js') }}"></script>
<script src="{{ asset('js/alpine.min.js') }}"></script>
@endsection

@section('content')
<div class="flex justify-center items-start">
   <div class="w-full sm:w-10/12 md:w-full">

      <h1 class="text-lg text-gray-900 tracking-widest font-medium title-font uppercase text-center pb-2" name="texto"
         style="font-size: 1.125rem; line-height: 2rem;">
         <i class="fas fa-question"></i>
         PERGUNTAS FREQUENTES
      </h1>

      <ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto"
         style="font-size: 0.875rem; line-height: 1.25rem;">
         <li class="inline-flex items-center">
            <a href="{{ url('/') }}">Início</a>
            <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
               <path fill-rule="evenodd"
                  d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                  clip-rule="evenodd"></path>
            </svg>
         </li>
         <li class="inline-flex items-center">
            <a href="#" class="text-teal-400">
               Perguntas Frequentes
            </a>
         </li>
      </ul>

      <hr class="border-b-2 border-lightBlue-300 mb-6 mx-4">

      <ul class="flex flex-col">
         <li class="bg-white my-2" x-data="accordion(1)">
            <h2 @click="handleClick()"
               class="flex flex-row justify-between items-center p-3 cursor-pointer uppercase text-sm text-blueGray-700"
               name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
               <span>
                  1- Qual é a diferença entre o Portal anterior e este?
               </span>
               <svg :class="handleRotate()"
                  class="fill-current text-lightBlue-600 h-6 w-6 transform transition-transform duration-500"
                  viewBox="0 0 20 20">
                  <path
                     d="M13.962,8.885l-3.736,3.739c-0.086,0.086-0.201,0.13-0.314,0.13S9.686,12.71,9.6,12.624l-3.562-3.56C5.863,8.892,5.863,8.611,6.036,8.438c0.175-0.173,0.454-0.173,0.626,0l3.25,3.247l3.426-3.424c0.173-0.172,0.451-0.172,0.624,0C14.137,8.434,14.137,8.712,13.962,8.885 M18.406,10c0,4.644-3.763,8.406-8.406,8.406S1.594,14.644,1.594,10S5.356,1.594,10,1.594S18.406,5.356,18.406,10 M17.521,10c0-4.148-3.373-7.521-7.521-7.521c-4.148,0-7.521,3.374-7.521,7.521c0,4.147,3.374,7.521,7.521,7.521C14.148,17.521,17.521,14.147,17.521,10">
                  </path>
               </svg>
            </h2>
            <div x-ref="tab" :style="handleToggle()"
               class="border-l-2 border-lightBlue-500 overflow-hidden max-h-0 duration-500 transition-all">
               <p class="w-11/12 p-3 text-gray-500 text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">
                  O Novo Portal busca atender de forma mais eficiente a crescente demanda e as obrigações de
                  transparência.
                  Entre as principais novidades, estão: formas diversas de apresentação dos dados, mecanismo de busca
                  integrado e intuitivo, melhor usabilidade, mais recursos gráficos, maior e melhor oferta de dados
                  abertos, adequação a plataformas móveis e maior interatividade.
               </p>
            </div>
         </li>
         <li class="bg-white my-2" x-data="accordion(2)">
            <h2 @click="handleClick()"
               class="flex flex-row justify-between items-center p-3 cursor-pointer uppercase text-sm text-blueGray-700"
               name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
               <span>
                  2- O que é o Portal da Transparência?
               </span>
               <svg :class="handleRotate()"
                  class="fill-current text-lightBlue-600 h-6 w-6 transform transition-transform duration-500"
                  viewBox="0 0 20 20">
                  <path
                     d="M13.962,8.885l-3.736,3.739c-0.086,0.086-0.201,0.13-0.314,0.13S9.686,12.71,9.6,12.624l-3.562-3.56C5.863,8.892,5.863,8.611,6.036,8.438c0.175-0.173,0.454-0.173,0.626,0l3.25,3.247l3.426-3.424c0.173-0.172,0.451-0.172,0.624,0C14.137,8.434,14.137,8.712,13.962,8.885 M18.406,10c0,4.644-3.763,8.406-8.406,8.406S1.594,14.644,1.594,10S5.356,1.594,10,1.594S18.406,5.356,18.406,10 M17.521,10c0-4.148-3.373-7.521-7.521-7.521c-4.148,0-7.521,3.374-7.521,7.521c0,4.147,3.374,7.521,7.521,7.521C14.148,17.521,17.521,14.147,17.521,10">
                  </path>
               </svg>
            </h2>
            <div class="border-l-2 border-lightBlue-500 overflow-hidden max-h-0 duration-500 transition-all" x-ref="tab"
               :style="handleToggle()">
               <p class="w-11/12 p-3 text-gray-500 text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">
                  O Portal da Transparência do Rio Grande do Norte é uma ferramenta de participação da sociedade no
                  controle das ações do Governo. O Portal contém informações sobre os gastos acerca das receitas e
                  despesas do Estado, informações gerenciais, dentre outras informações de interesse dos cidadãos.
               </p>
            </div>
         </li>
         <li class="bg-white my-2" x-data="accordion(3)">
            <h2 @click="handleClick()"
               class="flex flex-row justify-between items-center p-3 cursor-pointer uppercase text-sm text-blueGray-700"
               name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
               <span>
                  3- De onde são obtidos os dados divulgados no portal?
               </span>
               <svg :class="handleRotate()"
                  class="fill-current text-lightBlue-600 h-6 w-6 transform transition-transform duration-500"
                  viewBox="0 0 20 20">
                  <path
                     d="M13.962,8.885l-3.736,3.739c-0.086,0.086-0.201,0.13-0.314,0.13S9.686,12.71,9.6,12.624l-3.562-3.56C5.863,8.892,5.863,8.611,6.036,8.438c0.175-0.173,0.454-0.173,0.626,0l3.25,3.247l3.426-3.424c0.173-0.172,0.451-0.172,0.624,0C14.137,8.434,14.137,8.712,13.962,8.885 M18.406,10c0,4.644-3.763,8.406-8.406,8.406S1.594,14.644,1.594,10S5.356,1.594,10,1.594S18.406,5.356,18.406,10 M17.521,10c0-4.148-3.373-7.521-7.521-7.521c-4.148,0-7.521,3.374-7.521,7.521c0,4.147,3.374,7.521,7.521,7.521C14.148,17.521,17.521,14.147,17.521,10">
                  </path>
               </svg>
            </h2>
            <div class="border-l-2 border-lightBlue-500 overflow-hidden max-h-0 duration-500 transition-all" x-ref="tab"
               :style="handleToggle()">
               <p class="w-11/12 p-3 text-gray-500 text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">
                  Os dados são obtidos dos sistemas oficiais do Estado conforme a finalidade. Por exemplo, os dados
                  orçamentários e financeiros relativos às despesas, às receitas, aos demonstrativos contábeis e fiscais
                  são oriundos do Sistema Integrado de Planejamento e Gestão Fiscal (SIGEF), gerenciado pela Secretaria
                  de Estado do Planejamento. Já as informações relacionadas à remuneração e aos dados funcionais dos
                  servidores têm origem nos Sistemas Integrados de Gerenciamento de Recursos Humanos, gerenciados pela
                  Secretaria de Estado da Administração.
               </p>
            </div>
         </li>
         <li class="bg-white my-2" x-data="accordion(4)">
            <h2 @click="handleClick()"
               class="flex flex-row justify-between items-center p-3 cursor-pointer uppercase text-sm text-blueGray-700"
               name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
               <span>
                  4- Qual é a frequência de atualização do Portal da Transparência?
               </span>
               <svg :class="handleRotate()"
                  class="fill-current text-lightBlue-600 h-6 w-6 transform transition-transform duration-500"
                  viewBox="0 0 20 20">
                  <path
                     d="M13.962,8.885l-3.736,3.739c-0.086,0.086-0.201,0.13-0.314,0.13S9.686,12.71,9.6,12.624l-3.562-3.56C5.863,8.892,5.863,8.611,6.036,8.438c0.175-0.173,0.454-0.173,0.626,0l3.25,3.247l3.426-3.424c0.173-0.172,0.451-0.172,0.624,0C14.137,8.434,14.137,8.712,13.962,8.885 M18.406,10c0,4.644-3.763,8.406-8.406,8.406S1.594,14.644,1.594,10S5.356,1.594,10,1.594S18.406,5.356,18.406,10 M17.521,10c0-4.148-3.373-7.521-7.521-7.521c-4.148,0-7.521,3.374-7.521,7.521c0,4.147,3.374,7.521,7.521,7.521C14.148,17.521,17.521,14.147,17.521,10">
                  </path>
               </svg>
            </h2>
            <div class="border-l-2 border-lightBlue-500 overflow-hidden max-h-0 duration-500 transition-all" x-ref="tab"
               :style="handleToggle()">
               <p class="w-11/12 p-3 text-gray-500 text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">
                  As informações sobre as receitas e despesas são atualizadas diariamente. Os relatórios, como, por
                  exemplo, os da Lei de Responsabilidade Fiscal, o Balanço Geral, o Plano Plurianual, a Lei de
                  Diretrizes Orçamentárias e a Lei Orçamentária Anual, são atualizados conforme a sua divulgação, que
                  pode ser mensal, bimestral, quadrimestral ou anual, como determina a legislação.
               </p>
            </div>
         </li>
         <li class="bg-white my-2" x-data="accordion(5)">
            <h2 @click="handleClick()"
               class="flex flex-row justify-between items-center p-3 cursor-pointer uppercase text-sm text-blueGray-700"
               name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
               <span>
                  5- Quem é o responsável pelo Gerenciamento do Portal da Transparência?
               </span>
               <svg :class="handleRotate()"
                  class="fill-current text-lightBlue-600 h-6 w-6 transform transition-transform duration-500"
                  viewBox="0 0 20 20">
                  <path
                     d="M13.962,8.885l-3.736,3.739c-0.086,0.086-0.201,0.13-0.314,0.13S9.686,12.71,9.6,12.624l-3.562-3.56C5.863,8.892,5.863,8.611,6.036,8.438c0.175-0.173,0.454-0.173,0.626,0l3.25,3.247l3.426-3.424c0.173-0.172,0.451-0.172,0.624,0C14.137,8.434,14.137,8.712,13.962,8.885 M18.406,10c0,4.644-3.763,8.406-8.406,8.406S1.594,14.644,1.594,10S5.356,1.594,10,1.594S18.406,5.356,18.406,10 M17.521,10c0-4.148-3.373-7.521-7.521-7.521c-4.148,0-7.521,3.374-7.521,7.521c0,4.147,3.374,7.521,7.521,7.521C14.148,17.521,17.521,14.147,17.521,10">
                  </path>
               </svg>
            </h2>
            <div class="border-l-2 border-lightBlue-500 overflow-hidden max-h-0 duration-500 transition-all" x-ref="tab"
               :style="handleToggle()">
               <p class="w-11/12 p-3 text-gray-500 text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">
                  A Controladoria Geral do Estado do Rio Grande do Norte gerencia os dados e informações apresentadas no
                  Portal.
               </p>
            </div>
         </li>
         <li class="bg-white my-2" x-data="accordion(6)">
            <h2 @click="handleClick()"
               class="flex flex-row justify-between items-center p-3 cursor-pointer uppercase text-sm text-blueGray-700"
               name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
               <span>
                  6- Como faço para encaminhar sugestões, elogios, reclamações ou tirar dúvidas sobre o conteúdo e a
                  navegação do Portal da Transparência?
               </span>
               <svg :class="handleRotate()"
                  class="fill-current text-lightBlue-600 h-6 w-6 transform transition-transform duration-500"
                  viewBox="0 0 20 20">
                  <path
                     d="M13.962,8.885l-3.736,3.739c-0.086,0.086-0.201,0.13-0.314,0.13S9.686,12.71,9.6,12.624l-3.562-3.56C5.863,8.892,5.863,8.611,6.036,8.438c0.175-0.173,0.454-0.173,0.626,0l3.25,3.247l3.426-3.424c0.173-0.172,0.451-0.172,0.624,0C14.137,8.434,14.137,8.712,13.962,8.885 M18.406,10c0,4.644-3.763,8.406-8.406,8.406S1.594,14.644,1.594,10S5.356,1.594,10,1.594S18.406,5.356,18.406,10 M17.521,10c0-4.148-3.373-7.521-7.521-7.521c-4.148,0-7.521,3.374-7.521,7.521c0,4.147,3.374,7.521,7.521,7.521C14.148,17.521,17.521,14.147,17.521,10">
                  </path>
               </svg>
            </h2>
            <div class="border-l-2 border-lightBlue-500 overflow-hidden max-h-0 duration-500 transition-all" x-ref="tab"
               :style="handleToggle()">
               <p class="w-11/12 p-3 text-gray-500 text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">
                  Caso você queira encaminhar sugestões, elogios, reclamações ou tirar dúvidas, entre em contato por
                  meio do email disponibilizado no <a class="text-lightBlue-500 hover:underline"
                     href="{{ route('faleconosco') }}">Fale Conosco</a> no Portal da Transparência.
               </p>
            </div>
         </li>
         <li class="bg-white my-2" x-data="accordion(7)">
            <h2 @click="handleClick()"
               class="flex flex-row justify-between items-center p-3 cursor-pointer uppercase text-sm text-blueGray-700"
               name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
               <span>
                  7- Como tirar dúvidas sobre termos, expressões e siglas que aparecem no Portal da Transparência?
               </span>
               <svg :class="handleRotate()"
                  class="fill-current text-lightBlue-600 h-6 w-6 transform transition-transform duration-500"
                  viewBox="0 0 20 20">
                  <path
                     d="M13.962,8.885l-3.736,3.739c-0.086,0.086-0.201,0.13-0.314,0.13S9.686,12.71,9.6,12.624l-3.562-3.56C5.863,8.892,5.863,8.611,6.036,8.438c0.175-0.173,0.454-0.173,0.626,0l3.25,3.247l3.426-3.424c0.173-0.172,0.451-0.172,0.624,0C14.137,8.434,14.137,8.712,13.962,8.885 M18.406,10c0,4.644-3.763,8.406-8.406,8.406S1.594,14.644,1.594,10S5.356,1.594,10,1.594S18.406,5.356,18.406,10 M17.521,10c0-4.148-3.373-7.521-7.521-7.521c-4.148,0-7.521,3.374-7.521,7.521c0,4.147,3.374,7.521,7.521,7.521C14.148,17.521,17.521,14.147,17.521,10">
                  </path>
               </svg>
            </h2>
            <div class="border-l-2 border-lightBlue-500 overflow-hidden max-h-0 duration-500 transition-all" x-ref="tab"
               :style="handleToggle()">
               <p class="w-11/12 p-3 text-gray-500 text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">
                  Consulte o <a class="text-lightBlue-500 hover:underline" href="{{ route('glossario') }}">Glossário</a>
                  para esclarecimentos acerca de termos técnicos, siglas e demais expressões
                  utilizados nos textos explicativos do Portal da Transparência. Caso você tenha encontrado alguma
                  palavra ou expressão sem definição, ou ainda com definição inconsistente, envie a sua sugestão por
                  meio do email disponibilizado no <a class="text-lightBlue-500 hover:underline"
                     href="{{ route('faleconosco') }}">Fale Conosco</a> no Portal da Transparência.
               </p>
            </div>
         </li>
         <li class="bg-white my-2" x-data="accordion(8)">
            <h2 @click="handleClick()"
               class="flex flex-row justify-between items-center p-3 cursor-pointer uppercase text-sm text-blueGray-700"
               name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
               <span>
                  8- Posso fazer download de consultas?
               </span>
               <svg :class="handleRotate()"
                  class="fill-current text-lightBlue-600 h-6 w-6 transform transition-transform duration-500"
                  viewBox="0 0 20 20">
                  <path
                     d="M13.962,8.885l-3.736,3.739c-0.086,0.086-0.201,0.13-0.314,0.13S9.686,12.71,9.6,12.624l-3.562-3.56C5.863,8.892,5.863,8.611,6.036,8.438c0.175-0.173,0.454-0.173,0.626,0l3.25,3.247l3.426-3.424c0.173-0.172,0.451-0.172,0.624,0C14.137,8.434,14.137,8.712,13.962,8.885 M18.406,10c0,4.644-3.763,8.406-8.406,8.406S1.594,14.644,1.594,10S5.356,1.594,10,1.594S18.406,5.356,18.406,10 M17.521,10c0-4.148-3.373-7.521-7.521-7.521c-4.148,0-7.521,3.374-7.521,7.521c0,4.147,3.374,7.521,7.521,7.521C14.148,17.521,17.521,14.147,17.521,10">
                  </path>
               </svg>
            </h2>
            <div class="border-l-2 border-lightBlue-500 overflow-hidden max-h-0 duration-500 transition-all" x-ref="tab"
               :style="handleToggle()">
               <p class="w-11/12 p-3 text-gray-500 text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">
                  Sim! O Portal da Transparência oferece diversas possibilidades para download dos dados apresentados
                  nas consultas.
                  A maioria das consultas, por padrão, tem botões que apresentam funções de download da consulta, sendo
                  nos formatos .pdf, .xls e .csv, bastando clicar no botão referente ao formato desejado para efetuar o
                  download de arquivo com os dados retornados na consulta efetuada.
               </p>
            </div>
         </li>
         <li class="bg-white my-2" x-data="accordion(9)">
            <h2 @click="handleClick()"
               class="flex flex-row justify-between items-center p-3 cursor-pointer uppercase text-sm text-blueGray-700"
               name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
               <span>
                  9- Todas as campanhas do governo estão disponíveis no Portal da Transparência?
               </span>
               <svg :class="handleRotate()"
                  class="fill-current text-lightBlue-600 h-6 w-6 transform transition-transform duration-500"
                  viewBox="0 0 20 20">
                  <path
                     d="M13.962,8.885l-3.736,3.739c-0.086,0.086-0.201,0.13-0.314,0.13S9.686,12.71,9.6,12.624l-3.562-3.56C5.863,8.892,5.863,8.611,6.036,8.438c0.175-0.173,0.454-0.173,0.626,0l3.25,3.247l3.426-3.424c0.173-0.172,0.451-0.172,0.624,0C14.137,8.434,14.137,8.712,13.962,8.885 M18.406,10c0,4.644-3.763,8.406-8.406,8.406S1.594,14.644,1.594,10S5.356,1.594,10,1.594S18.406,5.356,18.406,10 M17.521,10c0-4.148-3.373-7.521-7.521-7.521c-4.148,0-7.521,3.374-7.521,7.521c0,4.147,3.374,7.521,7.521,7.521C14.148,17.521,17.521,14.147,17.521,10">
                  </path>
               </svg>
            </h2>
            <div class="border-l-2 border-lightBlue-500 overflow-hidden max-h-0 duration-500 transition-all" x-ref="tab"
               :style="handleToggle()">
               <p class="w-11/12 p-3 text-gray-500 text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">
                  O Portal da Transparência apresenta programas orçamentários e programas de governo.
               </p>
            </div>
         </li>
         <li class="bg-white my-2" x-data="accordion(10)">
            <h2 @click="handleClick()"
               class="flex flex-row justify-between items-center p-3 cursor-pointer uppercase text-sm text-blueGray-700"
               name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
               <span>
                  10- Qual a diferença entre valor empenhado, valor liquidado e valor pago?
               </span>
               <svg :class="handleRotate()"
                  class="fill-current text-lightBlue-600 h-6 w-6 transform transition-transform duration-500"
                  viewBox="0 0 20 20">
                  <path
                     d="M13.962,8.885l-3.736,3.739c-0.086,0.086-0.201,0.13-0.314,0.13S9.686,12.71,9.6,12.624l-3.562-3.56C5.863,8.892,5.863,8.611,6.036,8.438c0.175-0.173,0.454-0.173,0.626,0l3.25,3.247l3.426-3.424c0.173-0.172,0.451-0.172,0.624,0C14.137,8.434,14.137,8.712,13.962,8.885 M18.406,10c0,4.644-3.763,8.406-8.406,8.406S1.594,14.644,1.594,10S5.356,1.594,10,1.594S18.406,5.356,18.406,10 M17.521,10c0-4.148-3.373-7.521-7.521-7.521c-4.148,0-7.521,3.374-7.521,7.521c0,4.147,3.374,7.521,7.521,7.521C14.148,17.521,17.521,14.147,17.521,10">
                  </path>
               </svg>
            </h2>
            <div class="border-l-2 border-lightBlue-500 overflow-hidden max-h-0 duration-500 transition-all" x-ref="tab"
               :style="handleToggle()">
               <p class="w-11/12 p-3 text-gray-500 text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">
                  Valor empenhado é aquele que o Estado reservou para efetuar um pagamento planejado. O empenho ocorre,
                  por exemplo, após a assinatura de um contrato para prestação de serviço. Nesse caso, quando o serviço
                  for executado, o valor é liquidado, e quando o prestador do serviço de fato receber o valor, este é
                  considerado um valor pago.
               </p>
            </div>
         </li>
         <li class="bg-white my-2" x-data="accordion(11)">
            <h2 @click="handleClick()"
               class="flex flex-row justify-between items-center p-3 cursor-pointer uppercase text-sm text-blueGray-700"
               name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
               <span>
                  11- Como posso encontrar informações sobre os recursos estaduais repassados ao meu município?
               </span>
               <svg :class="handleRotate()"
                  class="fill-current text-lightBlue-600 h-6 w-6 transform transition-transform duration-500"
                  viewBox="0 0 20 20">
                  <path
                     d="M13.962,8.885l-3.736,3.739c-0.086,0.086-0.201,0.13-0.314,0.13S9.686,12.71,9.6,12.624l-3.562-3.56C5.863,8.892,5.863,8.611,6.036,8.438c0.175-0.173,0.454-0.173,0.626,0l3.25,3.247l3.426-3.424c0.173-0.172,0.451-0.172,0.624,0C14.137,8.434,14.137,8.712,13.962,8.885 M18.406,10c0,4.644-3.763,8.406-8.406,8.406S1.594,14.644,1.594,10S5.356,1.594,10,1.594S18.406,5.356,18.406,10 M17.521,10c0-4.148-3.373-7.521-7.521-7.521c-4.148,0-7.521,3.374-7.521,7.521c0,4.147,3.374,7.521,7.521,7.521C14.148,17.521,17.521,14.147,17.521,10">
                  </path>
               </svg>
            </h2>
            <div class="border-l-2 border-lightBlue-500 overflow-hidden max-h-0 duration-500 transition-all" x-ref="tab"
               :style="handleToggle()">
               <p class="w-11/12 p-3 text-gray-500 text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">
                  As transferências constitucionais dos tributos ICMS, IPI e IPVA podem ser acessadas através do link
                  “<a class="text-lightBlue-500 hover:underline"
                     href="{{ asset('http://www.seplan.rn.gov.br/Conteudo.asp?TRAN=PASTAC&TARG=2474&ACT=&PAGE=0&PARM=&LBL=Transpar%EAncia') }}"
                     target="_blank">Transferências para Municípios</a>”, disponível na página principal do portal.
               </p>
            </div>
         </li>
         <li class="bg-white my-2" x-data="accordion(12)">
            <h2 @click="handleClick()"
               class="flex flex-row justify-between items-center p-3 cursor-pointer uppercase text-sm text-blueGray-700"
               name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
               <span>
                  12- Por que há informações protegidas no Portal?
               </span>
               <svg :class="handleRotate()"
                  class="fill-current text-lightBlue-600 h-6 w-6 transform transition-transform duration-500"
                  viewBox="0 0 20 20">
                  <path
                     d="M13.962,8.885l-3.736,3.739c-0.086,0.086-0.201,0.13-0.314,0.13S9.686,12.71,9.6,12.624l-3.562-3.56C5.863,8.892,5.863,8.611,6.036,8.438c0.175-0.173,0.454-0.173,0.626,0l3.25,3.247l3.426-3.424c0.173-0.172,0.451-0.172,0.624,0C14.137,8.434,14.137,8.712,13.962,8.885 M18.406,10c0,4.644-3.763,8.406-8.406,8.406S1.594,14.644,1.594,10S5.356,1.594,10,1.594S18.406,5.356,18.406,10 M17.521,10c0-4.148-3.373-7.521-7.521-7.521c-4.148,0-7.521,3.374-7.521,7.521c0,4.147,3.374,7.521,7.521,7.521C14.148,17.521,17.521,14.147,17.521,10">
                  </path>
               </svg>
            </h2>
            <div class="border-l-2 border-lightBlue-500 overflow-hidden max-h-0 duration-500 transition-all" x-ref="tab"
               :style="handleToggle()">
               <p class="w-11/12 p-3 text-gray-500 text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">
                  A proteção de informações pode ser necessária em várias situações, como por exemplo servidores em
                  viagem para uma operação sigilosa de combate à corrupção. As regras para proteção de informações no
                  Portal variam de acordo com o assunto e com o órgão responsável pelo dado. O órgão responsável define
                  as regras de restrição de acesso, que devem ser justificadas mediante sigilo ou classificação,
                  conforme disposto nos artigos 22, 23 e 24 da Lei nº 12.527/2011 (Lei de Acesso à Informação).
               </p>
            </div>
         </li>
         <li class="bg-white my-2" x-data="accordion(13)">
            <h2 @click="handleClick()"
               class="flex flex-row justify-between items-center p-3 cursor-pointer uppercase text-sm text-blueGray-700"
               name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
               <span>
                  13- Existem informações que não estão disponíveis e podem ser negadas?
               </span>
               <svg :class="handleRotate()"
                  class="fill-current text-lightBlue-600 h-6 w-6 transform transition-transform duration-500"
                  viewBox="0 0 20 20">
                  <path
                     d="M13.962,8.885l-3.736,3.739c-0.086,0.086-0.201,0.13-0.314,0.13S9.686,12.71,9.6,12.624l-3.562-3.56C5.863,8.892,5.863,8.611,6.036,8.438c0.175-0.173,0.454-0.173,0.626,0l3.25,3.247l3.426-3.424c0.173-0.172,0.451-0.172,0.624,0C14.137,8.434,14.137,8.712,13.962,8.885 M18.406,10c0,4.644-3.763,8.406-8.406,8.406S1.594,14.644,1.594,10S5.356,1.594,10,1.594S18.406,5.356,18.406,10 M17.521,10c0-4.148-3.373-7.521-7.521-7.521c-4.148,0-7.521,3.374-7.521,7.521c0,4.147,3.374,7.521,7.521,7.521C14.148,17.521,17.521,14.147,17.521,10">
                  </path>
               </svg>
            </h2>
            <div class="border-l-2 border-lightBlue-500 overflow-hidden max-h-0 duration-500 transition-all" x-ref="tab"
               :style="handleToggle()">
               <p class="w-11/12 p-3 text-gray-500 text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">
                  A Lei de Acesso à Informação estabelece níveis de sigilo para divulgação de dados. As informações que
                  comprometam a segurança da sociedade e do Estado ou invadam a intimidade de pessoas podem ser negadas.
                  Também têm acesso restrito informações com sigilo imposto por legislações específicas e as contidas em
                  documentos preparatórios, durante o processo de tomada de decisão ou de edição do ato.
               </p>
            </div>
         </li>
         <li class="bg-white my-2" x-data="accordion(14)">
            <h2 @click="handleClick()"
               class="flex flex-row justify-between items-center p-3 cursor-pointer uppercase text-sm text-blueGray-700"
               name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
               <span>
                  14- O que são informações pessoais?
               </span>
               <svg :class="handleRotate()"
                  class="fill-current text-lightBlue-600 h-6 w-6 transform transition-transform duration-500"
                  viewBox="0 0 20 20">
                  <path
                     d="M13.962,8.885l-3.736,3.739c-0.086,0.086-0.201,0.13-0.314,0.13S9.686,12.71,9.6,12.624l-3.562-3.56C5.863,8.892,5.863,8.611,6.036,8.438c0.175-0.173,0.454-0.173,0.626,0l3.25,3.247l3.426-3.424c0.173-0.172,0.451-0.172,0.624,0C14.137,8.434,14.137,8.712,13.962,8.885 M18.406,10c0,4.644-3.763,8.406-8.406,8.406S1.594,14.644,1.594,10S5.356,1.594,10,1.594S18.406,5.356,18.406,10 M17.521,10c0-4.148-3.373-7.521-7.521-7.521c-4.148,0-7.521,3.374-7.521,7.521c0,4.147,3.374,7.521,7.521,7.521C14.148,17.521,17.521,14.147,17.521,10">
                  </path>
               </svg>
            </h2>
            <div class="border-l-2 border-lightBlue-500 overflow-hidden max-h-0 duration-500 transition-all" x-ref="tab"
               :style="handleToggle()">
               <p class="w-11/12 p-3 text-gray-500 text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">
                  Informações pessoais são aquelas relacionadas à pessoa natural identificada ou identificável, cujo
                  tratamento deve ser feito de forma transparente e com respeito à intimidade, vida privada, honra e
                  imagem das pessoas, bem como às liberdades e garantias individuais. As informações pessoais terão seu
                  acesso restrito, independentemente de classificação de sigilo, pelo prazo máximo de 100 (cem) anos a
                  contar da sua data de produção.
               </p>
            </div>
         </li>
         <li class="bg-white my-2" x-data="accordion(15)">
            <h2 @click="handleClick()"
               class="flex flex-row justify-between items-center p-3 cursor-pointer uppercase text-sm text-blueGray-700"
               name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
               <span>
                  15- O que é a Lei de Responsabilidade Fiscal (LRF)?
               </span>
               <svg :class="handleRotate()"
                  class="fill-current text-lightBlue-600 h-6 w-6 transform transition-transform duration-500"
                  viewBox="0 0 20 20">
                  <path
                     d="M13.962,8.885l-3.736,3.739c-0.086,0.086-0.201,0.13-0.314,0.13S9.686,12.71,9.6,12.624l-3.562-3.56C5.863,8.892,5.863,8.611,6.036,8.438c0.175-0.173,0.454-0.173,0.626,0l3.25,3.247l3.426-3.424c0.173-0.172,0.451-0.172,0.624,0C14.137,8.434,14.137,8.712,13.962,8.885 M18.406,10c0,4.644-3.763,8.406-8.406,8.406S1.594,14.644,1.594,10S5.356,1.594,10,1.594S18.406,5.356,18.406,10 M17.521,10c0-4.148-3.373-7.521-7.521-7.521c-4.148,0-7.521,3.374-7.521,7.521c0,4.147,3.374,7.521,7.521,7.521C14.148,17.521,17.521,14.147,17.521,10">
                  </path>
               </svg>
            </h2>
            <div class="border-l-2 border-lightBlue-500 overflow-hidden max-h-0 duration-500 transition-all" x-ref="tab"
               :style="handleToggle()">
               <p class="w-11/12 p-3 text-gray-500 text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">
                  A Lei de Responsabilidade Fiscal é um código de conduta para os administradores públicos de todo o
                  país, que passa a valer para os três Poderes (Executivo, Legislativo e Judiciário), nas três esferas
                  de governo (federal, estadual e municipal). Através dela, todos os governantes passarão a obedecer a
                  normas e limites para administrar as finanças, prestando contas sobre quanto e como gastam os recursos
                  da sociedade.
               </p>
            </div>
         </li>
         <li class="bg-white my-2" x-data="accordion(16)">
            <h2 @click="handleClick()"
               class="flex flex-row justify-between items-center p-3 cursor-pointer uppercase text-sm text-blueGray-700"
               name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
               <span>
                  16- Qual é o objetivo da Lei de Responsabilidade Fiscal (LRF)?
               </span>
               <svg :class="handleRotate()"
                  class="fill-current text-lightBlue-600 h-6 w-6 transform transition-transform duration-500"
                  viewBox="0 0 20 20">
                  <path
                     d="M13.962,8.885l-3.736,3.739c-0.086,0.086-0.201,0.13-0.314,0.13S9.686,12.71,9.6,12.624l-3.562-3.56C5.863,8.892,5.863,8.611,6.036,8.438c0.175-0.173,0.454-0.173,0.626,0l3.25,3.247l3.426-3.424c0.173-0.172,0.451-0.172,0.624,0C14.137,8.434,14.137,8.712,13.962,8.885 M18.406,10c0,4.644-3.763,8.406-8.406,8.406S1.594,14.644,1.594,10S5.356,1.594,10,1.594S18.406,5.356,18.406,10 M17.521,10c0-4.148-3.373-7.521-7.521-7.521c-4.148,0-7.521,3.374-7.521,7.521c0,4.147,3.374,7.521,7.521,7.521C14.148,17.521,17.521,14.147,17.521,10">
                  </path>
               </svg>
            </h2>
            <div class="border-l-2 border-lightBlue-500 overflow-hidden max-h-0 duration-500 transition-all" x-ref="tab"
               :style="handleToggle()">
               <p class="w-11/12 p-3 text-gray-500 text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">
                  Melhorar a administração das contas públicas no Brasil. Com ela, todos os governantes passarão a ter
                  compromisso com orçamento e com metas, que devem ser apresentadas e aprovadas pelo respectivo Poder
                  Legislativo.
               </p>
            </div>
         </li>
         <li class="bg-white my-2" x-data="accordion(17)">
            <h2 @click="handleClick()"
               class="flex flex-row justify-between items-center p-3 cursor-pointer uppercase text-sm text-blueGray-700"
               name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
               <span>
                  17- Quais são os principais pontos da Lei de Responsabilidade Fiscal (LRF)?
               </span>
               <svg :class="handleRotate()"
                  class="fill-current text-lightBlue-600 h-6 w-6 transform transition-transform duration-500"
                  viewBox="0 0 20 20">
                  <path
                     d="M13.962,8.885l-3.736,3.739c-0.086,0.086-0.201,0.13-0.314,0.13S9.686,12.71,9.6,12.624l-3.562-3.56C5.863,8.892,5.863,8.611,6.036,8.438c0.175-0.173,0.454-0.173,0.626,0l3.25,3.247l3.426-3.424c0.173-0.172,0.451-0.172,0.624,0C14.137,8.434,14.137,8.712,13.962,8.885 M18.406,10c0,4.644-3.763,8.406-8.406,8.406S1.594,14.644,1.594,10S5.356,1.594,10,1.594S18.406,5.356,18.406,10 M17.521,10c0-4.148-3.373-7.521-7.521-7.521c-4.148,0-7.521,3.374-7.521,7.521c0,4.147,3.374,7.521,7.521,7.521C14.148,17.521,17.521,14.147,17.521,10">
                  </path>
               </svg>
            </h2>
            <div class="border-l-2 border-lightBlue-500 overflow-hidden max-h-0 duration-500 transition-all" x-ref="tab"
               :style="handleToggle()">
               <p class="w-11/12 p-3 text-gray-500 text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">
                  A Lei fixa limites para despesas com pessoal, para dívida pública e ainda determina que sejam criadas
                  metas para controlar receitas e despesas. Além disso, segundo a LRF, nenhum governante pode criar uma
                  nova despesa continuada (por mais de dois anos), sem indicar sua fonte de receita ou sem reduzir
                  outras despesas já existentes. Isso faz com que o governante consiga sempre pagar despesas, sem
                  comprometer o orçamento ou orçamentos futuros.
                  Pela LRF ainda, são definidos mecanismos adicionais de controle das finanças públicas em anos de
                  eleição.
               </p>
            </div>
         </li>
         <li class="bg-white my-2" x-data="accordion(18)">
            <h2 @click="handleClick()"
               class="flex flex-row justify-between items-center p-3 cursor-pointer uppercase text-sm text-blueGray-700"
               name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
               <span>
                  18- Como faço para denunciar irregularidades, solicitar documentos ou informações relacionadas ao
                  Governo do Rio Grande do Norte e aos convênios federais firmados com o Estado?
               </span>
               <svg :class="handleRotate()"
                  class="fill-current text-lightBlue-600 h-6 w-6 transform transition-transform duration-500"
                  viewBox="0 0 20 20">
                  <path
                     d="M13.962,8.885l-3.736,3.739c-0.086,0.086-0.201,0.13-0.314,0.13S9.686,12.71,9.6,12.624l-3.562-3.56C5.863,8.892,5.863,8.611,6.036,8.438c0.175-0.173,0.454-0.173,0.626,0l3.25,3.247l3.426-3.424c0.173-0.172,0.451-0.172,0.624,0C14.137,8.434,14.137,8.712,13.962,8.885 M18.406,10c0,4.644-3.763,8.406-8.406,8.406S1.594,14.644,1.594,10S5.356,1.594,10,1.594S18.406,5.356,18.406,10 M17.521,10c0-4.148-3.373-7.521-7.521-7.521c-4.148,0-7.521,3.374-7.521,7.521c0,4.147,3.374,7.521,7.521,7.521C14.148,17.521,17.521,14.147,17.521,10">
                  </path>
               </svg>
            </h2>
            <div class="border-l-2 border-lightBlue-500 overflow-hidden max-h-0 duration-500 transition-all" x-ref="tab"
               :style="handleToggle()">
               <p class="w-11/12 p-3 text-gray-500 text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">
                  Caso você queira fazer uma denúncia, uma reclamação, assim como dar sugestões ou elogios ao Governo do
                  Rio Grande do Norte e aos convênios federais firmados com o Estado, entre em contato com a Ouvidoria
                  Geral do Estado do Rio Grande do Norte, através da "<a class="text-lightBlue-500 hover:underline"
                     href="{{ asset('https://sistema.ouvidorias.gov.br/publico/RN/manifestacao/RegistrarManifestacao') }}"
                     target="_blank">Ouvidoria</a>". Para solicitar documentos ou
                  informações utilize o "<a class="text-lightBlue-500 hover:underline"
                     href="{{ asset('http://www.sic.rn.gov.br/') }}" target="_blank">Acesso a Informação</a>". Os links
                  para acessar esses serviços estão disponíveis no
                  portal da transparência.
               </p>
            </div>
         </li>
         <li class="bg-white my-2" x-data="accordion(19)">
            <h2 @click="handleClick()"
               class="flex flex-row justify-between items-center p-3 cursor-pointer uppercase text-sm text-blueGray-700"
               name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
               <span>
                  19- Como denunciar alguma irregularidade na aplicação dos recursos públicos?
               </span>
               <svg :class="handleRotate()"
                  class="fill-current text-lightBlue-600 h-6 w-6 transform transition-transform duration-500"
                  viewBox="0 0 20 20">
                  <path
                     d="M13.962,8.885l-3.736,3.739c-0.086,0.086-0.201,0.13-0.314,0.13S9.686,12.71,9.6,12.624l-3.562-3.56C5.863,8.892,5.863,8.611,6.036,8.438c0.175-0.173,0.454-0.173,0.626,0l3.25,3.247l3.426-3.424c0.173-0.172,0.451-0.172,0.624,0C14.137,8.434,14.137,8.712,13.962,8.885 M18.406,10c0,4.644-3.763,8.406-8.406,8.406S1.594,14.644,1.594,10S5.356,1.594,10,1.594S18.406,5.356,18.406,10 M17.521,10c0-4.148-3.373-7.521-7.521-7.521c-4.148,0-7.521,3.374-7.521,7.521c0,4.147,3.374,7.521,7.521,7.521C14.148,17.521,17.521,14.147,17.521,10">
                  </path>
               </svg>
            </h2>
            <div class="border-l-2 border-lightBlue-500 overflow-hidden max-h-0 duration-500 transition-all" x-ref="tab"
               :style="handleToggle()">
               <p class="w-11/12 w-11/12 p-3 text-gray-500 text-sm" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">
                  Primeiro, certifique-se de que sua denúncia está relacionada a procedimentos e ações de agentes
                  públicos estaduais ou órgãos e entidades do Poder Executivo Estadual. Procure descrever os fatos de
                  maneira clara, simples e objetiva, de forma que a denúncia seja apurada. O ideal é que seja feito um
                  relato o mais completo possível do assunto, com a indicação, por exemplo, de nomes, locais, datas,
                  documentos comprobatórios, bem como tudo o que possa auxiliar na identificação do ilícito.
                  Para registrar sua denúncia, acesse a plataforma <a class="text-lightBlue-500 hover:underline"
                     href="{{ asset('https://sistema.ouvidorias.gov.br/publico/RN/manifestacao/RegistrarManifestacao') }}"
                     target="_blank">Ouvidoria Geral do RN</a>.
               </p>
            </div>
         </li>
      </ul>
   </div>
</div>

<script>
   Spruce.store('accordion', {
      tab: 0,
    });

    const accordion = (idx) => ({
      handleClick() {
        this.$store.accordion.tab = this.$store.accordion.tab === idx ? 0 : idx;
      },
      handleRotate() {
        return this.$store.accordion.tab === idx ? 'rotate-180' : '';
      },
      handleToggle() {
        return this.$store.accordion.tab === idx ? `max-height: ${this.$refs.tab.scrollHeight}px` : '';
      }
    });
</script>
@endsection