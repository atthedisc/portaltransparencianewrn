@extends('layouts.app')

@section('title')
- Terceirizados
@endsection

@section('content')
<section class="text-gray-600 body-font">
   <div class="container px-5 mx-auto">
      <div class="flex flex-col text-center w-full">

         <h1 class="text-lg text-gray-900 tracking-widest font-medium title-font pb-2" name="texto"
            style="font-size: 1.125rem; line-height: 2rem;">
            <i class="fas fa-exclamation-circle"></i>
            TERCEIRIZADOS
         </h1>

         <div class="flex flex-col text-center w-full mb-3">
            <p class="lg:w-3/4 mx-auto leading-relaxed text-sm text-center" name="texto"
               style="font-size: 0.875rem; line-height: 1.25rem;">
               Têm a finalidade de apresentar a lista de funcionários terceirizados
               alocados em cada órgão do governo.
            </p>
         </div>

         <ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto"
            style="font-size: 0.875rem; line-height: 1.25rem;">
            <li class="inline-flex items-center">
               <a href="{{ url('/') }}">Início</a>
               <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                  <path fill-rule="evenodd"
                     d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                     clip-rule="evenodd"></path>
               </svg>
            </li>
            <li class="inline-flex items-center">
               <a href="#" class="text-teal-400">
                 Terceirizados
               </a>
            </li>
         </ul>

         <hr class="border-b-2 border-lightBlue-300 mb-6 mx-4">

         <div class="flex justify-between">
            <table
            class='w-1/2 mx-auto mb-24 max-w-sm md:max-w-lg rounded-lg bg-white divide-y divide-lightBlue-200 overflow-hidden text-center'>

            <tr class="text-lightBlue-800 text-center bg-lightBlue-50">
               <th class="font-medium text-sm uppercase tracking-wider px-6 py-4 text-center" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">
                  TERCEIRIZADOS DOS ÓRGÃOS DA ADMINISTRAÇÃO DIRETA
               </th>
            </tr>

            <tbody class="divide-y divide-lightBlue-200">
             <tr>
                 <td class="px-6 py-4 text-left text-sm">
                    <i class="far fa-file-pdf mr-1"></i>
                    <a href="{{ asset('docs/terceirizados/TERC_ASSECOM.pdf') }}"
                       class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                       name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                       ASSESSORIA DE COMUNICAÇÃO SOCIAL – ASSECOM
                    </a>
                 </td>
              </tr>
              <tr>
                 <td class="px-6 py-4 text-left text-sm">
                    <i class="far fa-file-pdf mr-1"></i>
                    <a href="{{ asset('docs/terceirizados/TERC_GAC.pdf') }}"
                       class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                       name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                       GABINETE CIVIL DO GOVERNO DO RN - GAC
                    </a>
                 </td>
              </tr>
              <tr>
                 <td class="px-6 py-4 text-left text-sm">
                    <i class="far fa-file-pdf mr-1"></i>
                    <a href="{{ asset('docs/terceirizados/TERC_SEDRAF.pdf') }}"
                       class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                       name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                       SECRETARIA DO DESENVOLVIMENTO RURAL E DA AGRICULTURA FAMILIAR - SEDRAF
                    </a>
                 </td>
              </tr>
              <tr>
                 <td class="px-6 py-4 text-left text-sm">
                    <i class="far fa-file-pdf mr-1"></i>
                    <a href="{{ asset('docs/terceirizados/TERC_SEPLAN.pdf') }}"
                       class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                       name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                       SECRETARIA DE ESTADO DO PLANEJAMENTO E DAS FINANÇAS - SEPLAN
                    </a>
                 </td>
              </tr>
              <tr>
                 <td class="px-6 py-4 text-left text-sm">
                    <i class="far fa-file-pdf mr-1"></i>
                    <a href="{{ asset('docs/terceirizados/TERC_GOVCIDADAO.pdf') }}"
                       class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                       name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                       SECRETARIA DE ESTADO DO PLANEJAMENTO E DAS FINANÇAS - GOVERNO CIDADÃO
                    </a>
                 </td>
              </tr>
              <tr>
                 <td class="px-6 py-4 text-left text-sm">
                    <i class="far fa-file-pdf mr-1"></i>
                    <a href="{{ asset('docs/terceirizados/TERC_SESED.pdf') }}"
                       class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                       name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                       SECRETARIA DE ESTADO DA SEGURANÇA PÚBLICA E DA DEFESA SOCIAL - SESED
                    </a>
                 </td>
              </tr>
              <tr>
                 <td class="px-6 py-4 text-left text-sm">
                    <i class="far fa-file-pdf mr-1"></i>
                    <a href="{{ asset('docs/terceirizados/TERC_SESED_SEC.pdf') }}"
                       class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                       name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                       SECRETARIA DE ESTADO DA SEGURANÇA PÚBLICA E DA DEFESA SOCIAL - SESED SEC
                    </a>
                 </td>
              </tr>
              <tr>
                 <td class="px-6 py-4 text-left text-sm">
                    <i class="far fa-file-pdf mr-1"></i>
                    <a href="{{ asset('docs/terceirizados/TERC_SESED_HCCPG.pdf') }}"
                       class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                       name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                       SECRETARIA DE ESTADO DA SEGURANÇA PÚBLICA E DA DEFESA SOCIAL - SESED HOSPITAL CENTRAL CORONEL PEDRO GERMANO
                    </a>
                 </td>
              </tr>
              <tr>
                 <td class="px-6 py-4 text-left text-sm">
                    <i class="far fa-file-pdf mr-1"></i>
                    <a href="{{ asset('docs/terceirizados/TERC_SETUR.pdf') }}"
                       class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                       name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                       SECRETARIA DE ESTADO DO TURISMO - SETUR
                    </a>
                 </td>
              </tr>
              <tr>
                 <td class="px-6 py-4 text-left text-sm">
                    <i class="far fa-file-pdf mr-1"></i>
                    <a href="{{ asset('docs/terceirizados/TERC_SIN.pdf') }}"
                       class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                       name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                       SECRETARIA DE ESTADO DA INFRAESTRUTURA – SIN
                    </a>
                 </td>
              </tr>
           </tbody>
         </table>

            <table
               class='w-1/2 mx-auto mb-24 max-w-sm md:max-w-lg rounded-lg bg-white divide-y divide-lightBlue-200 overflow-hidden text-center'>

               <tr class="text-lightBlue-800 text-center bg-lightBlue-50">
                  <th class="font-medium text-sm uppercase tracking-wider px-6 py-4 text-center" name="texto"
                     style="font-size: 0.875rem; line-height: 1.25rem;">
                     TERCEIRIZADOS DOS ÓRGÃOS DA ADMINISTRAÇÃO INDIRETA
                  </th>
               </tr>

               <tbody class="divide-y divide-lightBlue-200">
                <tr>
                    <td class="px-6 py-4 text-left text-sm">
                       <i class="far fa-file-pdf mr-1"></i>
                       <a href="{{ asset('docs/terceirizados/TERC_ARSEP.pdf') }}"
                          class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                          name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                          AGÊNCIA REGULADORA DE SERVIÇOS PÚBLICOS DO RN – ARSEP
                       </a>
                    </td>
                 </tr>
                 <tr>
                    <td class="px-6 py-4 text-left text-sm">
                       <i class="far fa-file-pdf mr-1"></i>
                       <a href="{{ asset('docs/terceirizados/TERC_CEASA.pdf') }}"
                          class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                          name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                          CENTRAIS DE ABASTECIMENTO DO RIO GRANDE DO NORTE S/A – CEASA
                       </a>
                    </td>
                 </tr>
                 <tr>
                    <td class="px-6 py-4 text-left text-sm">
                       <i class="far fa-file-pdf mr-1"></i>
                       <a href="{{ asset('docs/terceirizados/TERC_CEHAB.pdf') }}"
                          class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                          name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                          COMPANHIA ESTADUAL DE HABITAÇÃO E DESENVOLVIMENTO – CEHAB
                       </a>
                    </td>
                 </tr>
                 <tr>
                    <td class="px-6 py-4 text-left text-sm">
                       <i class="far fa-file-pdf mr-1"></i>
                       <a href="{{ asset('docs/terceirizados/TERC_DETRAN.pdf') }}"
                          class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                          name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                          DEPARTAMENTO ESTADUAL DE TRÂNSITO DO RN – DETRAN
                       </a>
                    </td>
                 </tr>
                 <tr>
                    <td class="px-6 py-4 text-left text-sm">
                       <i class="far fa-file-pdf mr-1"></i>
                       <a href="{{ asset('docs/terceirizados/TERC_EMATER.pdf') }}"
                          class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                          name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                          INSTITUTO DE ASSISTÊNCIA TÉCNICA E EXTENSÃO RURAL DO RIO GRANDE DO NORTE – EMATER
                       </a>
                    </td>
                 </tr>
                 <tr>
                    <td class="px-6 py-4 text-left text-sm">
                       <i class="far fa-file-pdf mr-1"></i>
                       <a href="{{ asset('docs/terceirizados/TERC_FJA.pdf') }}"
                          class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                          name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                          FUNDAÇÃO JOSÉ AUGUSTO - FJA
                       </a>
                    </td>
                 </tr>
                 <tr>
                    <td class="px-6 py-4 text-left text-sm">
                       <i class="far fa-file-pdf mr-1"></i>
                       <a href="{{ asset('docs/terceirizados/TERC_IDEMA.pdf') }}"
                          class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                          name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                          INSTITUTO DE DESENVOLVIMENTO SUSTENTÁVEL E MEIO AMBIENTE DO RN - IDEMA
                       </a>
                    </td>
                 </tr>
                 <tr>
                    <td class="px-6 py-4 text-left text-sm">
                       <i class="far fa-file-pdf mr-1"></i>
                       <a href="{{ asset('docs/terceirizados/TERC_IDIARN.pdf') }}"
                          class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                          name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                          INSTITUTO DE DEFESA E INSPEÇÃO AGROPECUÁRIA - IDIARN
                       </a>
                    </td>
                 </tr>
                 <tr>
                    <td class="px-6 py-4 text-left text-sm">
                       <i class="far fa-file-excel mr-1"></i>
                       <a href="{{ asset('docs/terceirizados/TERC_IGARN.xlsx') }}"
                          class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                          name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                          INSTITUTO DE GESTÃO DAS ÁGUAS DO ESTADO DO RIO GRANDE DO NORTE – IGARN
                       </a>
                    </td>
                 </tr>
                 <tr>
                    <td class="px-6 py-4 text-left text-sm">
                       <i class="far fa-file-pdf mr-1"></i>
                       <a href="{{ asset('docs/terceirizados/TERC_ITEP.pdf') }}"
                          class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                          name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                          INSTITUTO TÉCNICO-CIENTÍFICO DE PERÍCIA - ITEP
                       </a>
                    </td>
                 </tr>
                 <tr>
                    <td class="px-6 py-4 text-left text-sm">
                       <i class="far fa-file-pdf mr-1"></i>
                       <a href="{{ asset('docs/terceirizados/TERC_UERN.pdf') }}"
                          class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                          name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                          UNIVERSIDADE DO ESTADO DO RIO GRANDE DO NORTE – UERN
                       </a>
                    </td>
                 </tr>
              </tbody>
            </table>

            
         </div>

      </div>
   </div>
</section>
@endsection