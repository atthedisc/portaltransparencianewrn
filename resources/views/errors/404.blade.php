@extends('layouts/errors.app')

@section('title')
- Page not found
@endsection

@section('content')

<div class=" flex items-center p-5 lg:p-10 overflow-hidden relative">
   <div class="w-full max-w-6xl rounded bg-white shadow-xl p-10 lg:p-20 mx-auto text-gray-800 relative md:text-left">
      <div class="md:flex items-center -mx-10">

         <div class="w-full md:w-1/2 px-10 mb-10 md:mb-0">
            <div class="relative">
               <img src="{{ asset('/images/errors/404.svg') }}" class="w-full relative z-10">
               <div class="border-4 border-lightBlue-100 absolute top-10 bottom-10 left-10 right-10 z-20"></div>
            </div>
         </div>
         <div class="w-full md:w-1/2 px-10">
            <div class="flex justify-center md:justify-start">
               <a href="{{ url('/') }}">
                  <img src="{{ asset('images/logo/logo-portal-transparencia-45x45.png') }}">
               </a>
            </div>
            <div class="mb-10 pt-10 text-center md:text-left">
               <h1 class="font-bold text-2xl mb-5">Página não encontrada!<br>
                  A página que você está procurando pode ter sido excluída ou nunca ter existido.
               </h1>
               <p class="text-sm">
                  Continue navegando no Portal da Transparência do Rio Grande do Norte,
                  <a href="{{ url('/') }}"
                     class="opacity-50 text-gray-900 hover:opacity-100 inline-block text-sm leading-none border-b border-gray-900">
                     clique aqui
                     <i class="fas fa-arrow-right"></i>
                  </a>
               </p>
            </div>
            <div class="flex justify-center md:justify-start inline-block align-bottom">
               <a href="{{ url('/') }}">
                  <button
                     class="bg-lightBlue-500 hover:bg-lightBlue-700 text-white rounded-full px-10 py-2 font-semibold">
                     VOLTAR
                  </button>
               </a>
            </div>
         </div>

      </div>
   </div>
</div>

@endsection
