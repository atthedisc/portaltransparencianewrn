@extends('layouts.app')

@section('title')
- Saúde
@endsection

@section('content')
<section class="text-gray-600 body-font">
   <div class="container px-5 mx-auto">
      <div class="flex flex-col text-center w-full">

         <h1 class="text-lg text-gray-900 tracking-widest font-medium title-font pb-2" name="texto"
            style="font-size: 1.125rem; line-height: 2rem;">
            <i class="fas fa-file-alt"></i>
            SAÚDE
         </h1>

         <div class="flex flex-col text-center w-full mb-3">
            <p class="lg:w-3/4 mx-auto leading-relaxed text-sm text-center" name="texto"
               style="font-size: 0.875rem; line-height: 1.25rem;">
               
            </p>
         </div>

         <ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto"
            style="font-size: 0.875rem; line-height: 1.25rem;">
            <li class="inline-flex items-center">
               <a href="{{ url('/') }}">Início</a>
               <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                  <path fill-rule="evenodd"
                     d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                     clip-rule="evenodd"></path>
               </svg>
            </li>
            <li class="inline-flex items-center">
               <a href="#" class="text-teal-400">
                  Saúde
               </a>
            </li>
         </ul>

         <hr class="border-b-2 border-lightBlue-300 mb-6 mx-4">

         <div class="grid md:grid-flow-col mx-auto">

            <table
               class='mx-10 mb-auto max-w-sm md:max-w-lg rounded-lg bg-white divide-y divide-lightBlue-200 overflow-hidden text-center'>

               <tr class="text-lightBlue-800 text-center bg-lightBlue-50">
                  <th class="font-medium text-sm uppercase tracking-wider px-6 py-4 text-center" name="texto"
                     style="font-size: 0.875rem; line-height: 1.25rem;">
                     SAÚDE
                  </th>
               </tr>

               <tbody class="divide-y divide-lightBlue-200">
                  <tr>
                     <td class="px-6 py-4 text-center text-sm">
                        <i class="fas fa-external-link-square-alt mr-2"></i>
                        <a href="{{ asset('http://www.saude.rn.gov.br/Conteudo.asp?TRAN=ITEM&TARG=241418&ACT=&PAGE=0&PARM=&LBL=ACERVO+DE+MAT%C9RIAS') }}"
                           class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                           name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                           Instrumento de Gestão do SUS
                        </a>
                     </td>
                  </tr>
                  <tr>
                     <td class="px-6 py-4 text-center text-sm">
                        <i class="fas fa-external-link-square-alt mr-2"></i>
                        <a href="{{ asset('http://www.saude.rn.gov.br/Conteudo.asp?TRAN=ITEM&TARG=4213&ACT=&PAGE=0&PARM=&LBL=Servi%E7os') }}"
                           class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                           name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                           Escala de Plantão
                        </a>
                     </td>
                  </tr>
                  <tr>
                     <td class="px-6 py-4 text-center text-sm">
                        <i class="fas fa-external-link-square-alt mr-2"></i>
                        <a href="{{ asset('http://unicat.rn.gov.br/Conteudo.asp?TRAN=ITEM&TARG=226568&ACT=&PAGE=&PARM=&LBL=MAT%C9RIA') }}"
                           class="text-sm text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank"
                           name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                           Lista de Medicamentos Fornecidos pelo SUS
                        </a>
                     </td>
                  </tr>
               </tbody>
            </table>
         </div>

      </div>
   </div>
</section>
@endsection
