@extends('layouts.app')

@section('title')
- Notas de Esclarecimento
@endsection

@section('content')
<!-- Section -->
<section class="text-gray-600 body-font">
   <ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto"
            style="font-size: 0.875rem; line-height: 1.25rem;">
            <li class="inline-flex items-center">
                <a href="{{ url('/') }}">Inicio</a>
                <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                    <path fill-rule="evenodd"
                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                        clip-rule="evenodd"></path>
                </svg>
            </li>
            <li class="inline-flex items-center">
                <a class="text-teal-400" href="{{ url('/notas-esclarecimento') }}">Notas de Esclarecimento</a>
            </li>
        </ul>
        <hr class="border-b-2 border-lightBlue-300 mb-12 mx-4">
   <div class="container px-5 mx-auto">
      <div class="flex flex-col text-center w-full">

         

         <h1 class="text-lg text-blue-800 tracking-widest title-font mb-1 mt-1" name="texto" style="font-size: 1.125rem; line-height: 2rem;">Nota Técnica: 09/01/2024
         </h1>

         <hr class="border border-gray-120 mb-2 mt-2 mx-4">

         <br>
         <p class="lg:w-3/5 mx-auto leading-relaxed text-base text-justify" name="texto" style="font-size: 1rem; line-height: 1.5rem;">Publicação dos dados referentes à consulta 
            DESPESA:
         </p><br>
         <p class="lg:w-3/5 mx-auto leading-relaxed text-base text-justify" name="texto" style="font-size: 1rem; line-height: 1.5rem;"> 
            Informamos que por motivos de ordem técnica – interrupção parcial na comunicação entre os servidores que hospedam
            o portal da transparência e a fonte dos dados (SIGEF – Sistema Integrado de Planejamento e Gestão Fiscal), que fornece as informações 
            publicadas nas consultas Receita e Despesa - deixou de acontecer a atualização diária dos dados referentes à despesa pública.
            Com efeito, a devida atualização já foi reestabelecida pela unidade técnica da Controladoria-geral do Estado.
         </p><br>
      </div>

      
   </div>
</section>
<!-- /Section -->
@endsection