@extends('layouts.app')

@section('title')
- Órgãos do Governo
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/dropdown.css') }}">
@endsection

@section('content')
<section class="text-gray-600 body-font">
   <div class="container px-5 mx-auto">
      <div class="flex flex-col text-center w-full">

         <h1 class="text-lg text-gray-900 tracking-widest font-medium title-font pb-2" name="texto" style="font-size: 1.125rem; line-height: 2rem;">
            <i class="fas fa-search-plus"></i>
            ÓRGÃOS DO GOVERNO (CARTAS DE SERVIÇOS)
         </h1>

         <div class="flex flex-col text-center w-full mb-3">
            <p class="lg:w-3/4 mx-auto leading-relaxed text-sm text-gray-500 text-center" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
               Clique no órgão desejado para saber mais detalhes
            </p>
         </div>

         <ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
            <li class="inline-flex items-center">
               <a href="{{ url('/') }}">Início</a>
               <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                  <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd"></path>
               </svg>
            </li>
            <li class="inline-flex items-center">
               <a href="#" class="text-teal-400">
                  Órgãos do Governo (Cartas de Serviços)
               </a>
            </li>
         </ul>

         <hr class="border-b-2 border-lightBlue-300 mb-6 mx-4">

         <div class="flex flex-wrap justify-center">

            <main class="pl-4 pb-4 md:w-1/2">
               <section class="shadow row bg-white rounded-lg">
                  <div class="tabs">

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <header class="p-5 pl-8 pr-8 select-none tab-label">
                              <span class="text-gray-500 text-sm text-center" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 ADMINISTRAÇÃO DIRETA
                              </span>
                           </header>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 ASSESSORIA DE COMUNICAÇÃO SOCIAL - ASSECOM
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Assessor de Comunicação Social:</b> Daniel Cabral de Oliveira <br>
                                 <b>Telefones:</b> (84) 3232-5141 / 5204 / 5152 <br>
                                 <b>Fax:</b> (84) 3232-5211 <br />
                                 <b>Website:</b> http://www.assecom.rn.gov.br <br>
                                 <b>Email:</b> governodorn@gmail.com <br>
                                 <b>Horário de funcionamento:</b> 8h às 18h <br>
                                 <b>Endereço:</b> BR 101 KM 0 - Av. Senador Salgado Filho, S/N, Centro
                                 Administrativo do Estado, Lagoa Nova - Natal/RN. <br>
                                 CEP: 59064-901
                              </div>
                              <div class="pb-4">
                                 <a href="{{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_ASSECOM.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>

                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>À Assessoria de Comunicação Social (ASSECOM) compete: I -
                                       orientar e controlar a divulgação dos programas governamentais e
                                       das realizações do Governo, observando o disposto no art. 26, §
                                       1° da Constituição Estadual; II - distribuir informações e
                                       notícias de interesse da administração estadua1; e III -
                                       coordenar as relações dos órgãos da Administração estadual com
                                       os meios de comunicação. Art. 23(LC 163/1999) </p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 CONTROLADORIA GERAL DO ESTADO - CONTROL
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Controladora Geral:</b> Luciana Daltro De Castro Pádua<br>
                                 <b>Telefones:</b> Sem telefones no momento <br>
                                 <b>Website:</b> http://www.control.rn.gov.br <br>
                                 <b>Email:</b> control@rn.gov.br <br>
                                 <b>Horário de funcionamento:</b> 8h às 18h <br>
                                 <!-- (em obediência ao Decreto nº 28.692, de 2 de janeiro de 2019, prorrogado pelo Decreto Nº 28.963, de 1 de julho de 2019) -->
                                 <b>Endereço:</b> BR 101 KM 0 - Av. Senador Salgado Filho, S/N, Centro
                                 Administrativo do Estado, Controladoria Geral do Estado (Bloco SEPLAN),
                                 Lagoa Nova - Natal/RN. <br>
                                 CEP: 59064-901
                              </div>
                              <div class="pb-4">
                                 <a href="{{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_CONTROL.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>À Controladoria Geral do Estado (CONTROL) compete: I -
                                       supervisionar tecnicamente as atividades do sistema integrado de
                                       fiscalização financeira, contabilidade e auditoria; II - expedir
                                       atos normativos concernentes à ação do sistema integrado de
                                       fiscalização financeira, contabilidade e auditoria; III -
                                       determinar, acompanhar e avaliar a execução de auditorias; IV -
                                       proceder ao exame prévio nos processos originários de atos de
                                       gestão orçamentárias, financeira e patrimonial dos órgãos e
                                       entidades da administração pública estadual e nos de aplicação
                                       de recursos públicos estaduais por entidades de direito privado,
                                       emitindo parecer técnico; V - promover a apuração de denúncias
                                       formais relativas a irregularidades ou ilegalidades praticadas
                                       em qualquer órgão ou entidade da administração estadual, dando
                                       ciência imediata ao Governador do Estado, ao interessado e ao
                                       titular do órgão a quem se subordine o autor ou autores do ato
                                       objeto da denúncia, sob pena de responsabilidade solidária; VI -
                                       propor ao Governador do Estado a aplicação das sanções cabíveis,
                                       conforme a legislação vigente, aos gestores inadimplentes,
                                       podendo inclusive sugerir o bloqueio de transferências de
                                       recursos do Tesouro Estadual e de contas bancárias: VII -
                                       elaborar e manter atualizado o plano de contas único para os
                                       órgãos da Administração Direta e aprovar o plano de contas dos
                                       órgãos da administração indireta e fundacional; VIII -
                                       elaboração do Balanço Geral do Estado e da prestação de contas
                                       anual do Governador; e IX - manter com o Tribunal de Contas
                                       elaboração técnica e profissional relativamente à troca de
                                       informações e de dados a nível de execução orçamentária,
                                       objetivando uma maior integração dos controles interno e
                                       externo. Art. 22 (LC 163/1999)</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 GABINETE CIVIL DO GOVERNO DO RN - GAC
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Secretário Chefe do Gabinete Civil:</b> Raimundo Alves Júnior <br>
                                 <b>Telefones:</b> (84) 3232-5179 / 5180 <br>
                                 <b>Fax:</b> (84) 3232-5264 <br>
                                 <b>Website:</b> http://www.gabinetecivil.rn.gov.br/ <br>
                                 <b>Horário de funcionamento:</b> 8h às 18h <br>
                                 <b>Endereço:</b> BR 101 KM 0 - Av. Senador Salgado Filho, S/N, Centro
                                 Administrativo do Estado, Lagoa Nova - Natal/RN. <br>
                                 CEP: 59064-901
                              </div>
                              <div class="pb-4">
                                 <a href=" {{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviço_GAC.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>É responsável pela assistência direta e indireta à governadora no
                                       desempenho de suas atribuições. Atua na coordenação e
                                       articulação com os Órgãos e Entidades da Administração Direta e
                                       Indireta para a elaboração e execução das políticas públicas.
                                       O Gabinete Civil do Governo do Estado do Rio Grande do Norte foi
                                       criado através da Lei Complementar nº190, de 8 de janeiro de
                                       2001 e por meio da Lei Complementar nº 262, de 29 de dezembro de
                                       2003 recebeu nova redação e atribuições, entre elas:
                                       Atuar como órgão de coordenação institucional junto aos órgãos e
                                       entidades da Administração Estadual;
                                       Dar assistência direta e imediata à governadora na sua
                                       representação política e social, bem como coordenar suas
                                       relações, nessa área, com os demais poderes do Estado, o
                                       Ministério Público, o Tribunal de Contas e outras esferas de
                                       governo;
                                       Coordenar, em articulação com as Secretarias de Estado, as ações
                                       relacionadas com a formulação e implementação de políticas para
                                       a juventude;
                                       Acompanhar a tramitação de Projetos de Lei na Assembleia
                                       Legislativa;
                                       Controlar a observância dos prazos para manifestação do Poder
                                       Executivo sobre solicitações da Assembléia Legislativa e o
                                       atendimento de pedidos de informações de Deputados Estaduais;
                                       Articular-se com as lideranças do Governo junto à Assembleia
                                       Legislativa, para equacionamento das questões de interesse
                                       político e legislativo da Administração Estadual;
                                       Assessorar a governadora no trato de matérias e na adoção de
                                       medidas relacionadas a seu expediente particular;
                                       Receber, organizar, avaliar e preparar o expediente da
                                       governadora, fazer publicar seus atos na Imprensa Oficial e
                                       acompanhar a execução das ordens por ele emitidas;
                                       Supervisionar e controlar a publicação dos atos do Poder
                                       Executivo na Imprensa Oficial;
                                       Cuidar da administração geral do Palácio do Governo, do Palácio
                                       dos Despachos e da residência oficial da governadora;
                                       Organizar e dirigir o cerimonial público;
                                       Responsabilizar-se pelo transporte da governadora;
                                       Supervisionar a segurança da governadora e da Governadoria;
                                       Coordenar as relações protocolares do Chefe de Governo com
                                       autoridades militares;
                                       Prestar apoio material, administrativo e técnico às autoridades
                                       estaduais presentes no Distrito Federal a serviço dos órgãos e
                                       entidades que dirijam ou representem;
                                       Administrar o Centro Administrativo;
                                       Coordenar o Centro Integrado de Esporte e Cultura RN Vida;
                                       Também integra a instituição, a Coordenadoria de Proteção e
                                       Defesa Civil (COPDEC).
                                    </p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 GOVERNADORIA
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Governadora:</b> Maria de Fátima Bezerra <br>
                                 <b>Telefones:</b> (84) 3232-5138 / 5139 / 5179 <br>
                                 <b>Website:</b> http://www.rn.gov.br/ <br>
                                 <b>E-mail:</b> governadora@gac.rn.gov.br <br>
                                 <b>Horário de funcionamento:</b> 8h às 18h <br>
                                 <b>Endereço</b> BR 101 KM 0 - Av. Senador Salgado Filho, S/N, Centro
                                 Administrativo do Estado, Lagoa Nova - Natal/RN. <br>
                                 CEP: 59064-901
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 PROCURADORIA GERAL DO ESTADO - PGE
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Procurador-Geral:</b> Antenor Roberto Soares de Medeiros <br>
                                 <b>Telefones:</b> (84) 3232-2799 / 2750 / 2760 / 2774 / 7422 / 7165
                                 <!-- / 2799 --> <br>
                                 <!-- <li><b>Fax:</b>(84) 3232-3718</li> -->
                                 <b>Website:</b> http://www.pge.rn.gov.br <br />
                                 <b>E-mail:</b> gabineteprocuradorgeral@rn.gov.br <br>
                                 <b>Horário de funcionamento:</b> Dívida Ativa e demais setores: 8h às 17h
                                 <br>
                                 <b>Endereço:</b> Av. Afonso Pena, 1155, Tirol - Natal/RN. <br>
                                 CEP: 59020-100
                              </div>
                              <div class="pb-4">
                                 <a href="{{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviço_PGE.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>À Procuradoria Geral do Estado (PGE) compete: I - exercer a
                                       representação judicial e extrajudicial do Estado; II - prestar
                                       assessoramento jurídico ao Poder Executivo, relativamente ao
                                       controle da legalidade dos atos da Administração Estadual; III -
                                       prestar assessoramento jurídico suplementar às entidades da
                                       administração indireta, quando determinado pelo Governador do
                                       Estado; IV - inscrever, controlar e cobrar a dívida ativa do
                                       Estado; V - desenvolver outras atividades definidas em Lei.
                                       Parágrafo Único. O Procurador do Estado pode, mediante expressa
                                       autorização governamental, servir junto aos demais órgãos e
                                       entidades da Administração Direta, indireta e fundacional do
                                       Estado, sem prejuízo de sua remuneração, observada, em caso de
                                       exercício de cargo em comissão, a opção prevista em
                                       Lei.Art.21(LC163/1999) </p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 SECRETARIA DE ESTADO DA AGRICULTURA, DA PECUÁRIA E DA PESCA - SAPE
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Secretário de Estado:</b> Guilherme Moraes Saldanha <br>
                                 <b>Telefones:</b> (84) 3232-1140 / 3100 / 3101 <br>
                                 <!--  / 3100 / 3101 -->
                                 <!-- <li><b>Fax:</b> (84) 3232-3113 </li> -->
                                 <b>Website:</b> http://www.sape.rn.gov.br <br>
                                 <b>E-mail:</b> sape@rn.gov.br <br>
                                 <b>Horário de funcionamento:</b> 8h às 13h <br>
                                 <b>Endereço:</b> BR 101 KM 0 - Av. Senador Salgado Filho, S/N, Centro
                                 Administrativo do Estado, Lagoa Nova - Natal/RN. <br>
                                 CEP: 59064-901
                              </div>
                              <div class="pb-4">
                                 <a href=" {{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_SAPE.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>Coordenar as Políticas Públicas voltadas para o desenvolvimento do
                                       Setor Agropecuário do Rio Grande do Norte, quer diretamente, quer
                                       por meio de suas entidades vinculadas – EMPARN, IDIARN e CEASA,
                                       sempre com o objetivo de promover melhorias crescente nos aspectos
                                       qualitativos e quantitativos da produção rural – alimentos para
                                       abastecimento dos mercados interno e externo; matéria-prima e
                                       insumos para mover as nossas indústrias.</p>
                                 </div>
                              </div>

                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 SECRETARIA DE ESTADO DA ADMINISTRAÇÃO - SEAD
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Secretária de Estado:</b> Pedro Lopes de Araújo Neto <br>
                                 <b>Telefones:</b> (84) 3190-0600 <br>
                                 <!-- <LI><b>Fax:</b> (84) 3232-1065</LI> -->
                                 <b>Website:</b> http://www.sead.rn.gov.br <br>
                                 <b>E-mail:</b> sead@rn.gov.br <br>
                                 <b>Horário de funcionamento:</b> 7h às 17h / Setor de Protocolo: 7h às 18h
                                 <br>
                                 <b>Endereço:</b> BR 101 KM 0 - Av. Senador Salgado Filho, S/N, Centro
                                 Administrativo do Estado, Lagoa Nova - Natal/RN. <br>
                                 CEP: 59064-901</Li>

                              </div>
                              <div class="pb-4">
                                 <a href="{{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_SEAD.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>A Secretaria de Estado da Administração (SEAD) tem por
                                       finalidade, planejar, coordenar, executar e controlar as
                                       atividades de administração geral, de modernização
                                       administrativa, de informatização e a gestão do patrimônio do
                                       Estado, bem como formular e executar a política de recursos
                                       humanos e materiais, de atendimento aos servidores públicos
                                       estaduais, de processamento de dados e de desenvolvimento dos
                                       serviços públicos do Poder Executivo Estadual, conforme o
                                       disposto no art. 37 da Lei Complementar Estadual nº 163, de 5 de
                                       fevereiro de 1999.</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 SECRETARIA DE ESTADO DA ADMINSTRAÇÃO PENITENCIÁRIA - SEAP
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Secretário de Estado:</b> Helton Edi Xavier da Silva <br>
                                 <b>Telefones:</b> (84) 99194-9092 (WhatsApp) <br>
                                 <b>Website:</b> http://www.seap.rn.gov.br<br>
                                 <b>Horário de funcionamento:</b> 8h às 14h, de segunda à sexta-feira <br>
                                 <!-- (em obediência ao Decreto nº 28.692, de 2 de janeiro de 2019, prorrogado pelo Decreto Nº 28.963, de 1 de julho de 2019) -->
                                 <b>Endereço:</b> BR 101 KM 0 - Av. Senador Salgado Filho, S/N, Centro
                                 Administrativo do Estado, Lagoa Nova - Natal/RN. <br>
                                 CEP: 59064-901
                              </div>
                              <div class="pb-4">
                                 <a href="{{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_SEAP.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>Tratar de assuntos relacionados com o funcionamento das
                                       instituições e da ordem jurídica;
                                       Zelar pela proteção dos direitos humanos, colaborando com órgãos
                                       públicos e entidades não governamentais que se dediquem a esse
                                       objetivo ou que tenham por escopo a defesa e desenvolvimento da
                                       cidadania;
                                       Administrar o Sistema Penitenciário do Estado;
                                       Coordenar, no âmbito estadual, as medidas administrativas de
                                       defesa do consumidor, na forma da legislação federal respectiva
                                       e em articulação com os demais órgãos públicos e com as
                                       instituições particulares organizadas para o mesmo fim;
                                       Coordenar a formulação, execução e avaliação das políticas
                                       públicas voltadas para a juventude do Estado do Rio Grande do
                                       Norte;
                                       Coordenar a formulação, execução e avaliação das políticas
                                       públicas voltadas para a promoção da igualdade racial;
                                       Coordenar a formulação, execução e avaliação das políticas
                                       públicas voltadas para a promoção da defesa, das garantias e dos
                                       direitos das mulheres. Art. 30 da (LC nº 163/1999).
                                    </p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 SECRETARIA DE ESTADO DO DESENVOLVIMENTO RURAL ECONÔMICO - SEDEC
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Secretário de Estado:</b> Jaime Calado Pereira dos Santos <br>
                                 <b>Telefones:</b> (84) 3232-1750/ 1705 / (84) 98146-6459 <br>
                                 <!-- <li><b>Fax:</b> (84) 3232-1745</li> -->
                                 <b>Website:</b> http://www.sedec.rn.gov.br<br>
                                 <b>E-mail:</b> secretario.sedec@rn.gov.br <br>
                                 <b>Horário de funcionamento:</b> 8h às 14h / Setor de Marcas e Patentes: 8h
                                 às 12h <br>
                                 <!-- (em obediência ao Decreto nº 28.692, de 2 de janeiro de 2019, prorrogado pelo Decreto Nº 28.963, de 1 de julho de 2019). -->
                                 <b>Endereço:</b> BR 101 KM 0 - Av. Senador Salgado Filho, S/N, Centro
                                 Administrativo do Estado, Lagoa Nova - Natal/RN. <br>
                                 CEP: 59064-901
                              </div>
                              <div class="pb-4">
                                 <a href="{{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_SEDEC.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>I - Promover o desenvolvimento econômico sustentável do Estado,
                                       coordenando e implementando ações setoriais nas áreas da
                                       indústria, recursos minerais, energia, comércio, serviços,
                                       ciência e tecnologia, em articulação com outros órgãos e
                                       entidades competentes;II - Apoiar ações voltadas para o
                                       desenvolvimento econômico equilibrado do Estado, promovendo as
                                       potencialidades regionais por meio da identificação de
                                       oportunidades de negócios, oferta de financiamentos e
                                       capacitação de recursos humanos;III - promover ações em
                                       cooperação com as Secretarias de igual natureza nos municípios e
                                       com Secretarias e entidades da Administração Estadual, sempre
                                       voltadas para o desenvolvimento econômico equilibrado de todas
                                       as regiões do Estado;IV - Elaborar e implementar a política
                                       estadual de desenvolvimento industrial, em articulação com as
                                       entidades atuantes nesse setor;V - Elaborar e implementar a
                                       política estadual de estímulo à expansão da atividade comercial
                                       e do segmento de serviços, articulando-se com as entidades
                                       atuantes nesse setor;VI - Articular e desenvolver as ações
                                       voltadas para estimular as atividades de comércio exterior,
                                       abrindo novos mercados para os produtos e serviços do Estado,
                                       fomentando a implantação de serviços de logística e capacitando
                                       recursos humanos para esse setor;VII - elaborar e implementar a
                                       política estadual dirigida para o aproveitamento econômico do
                                       potencial de recursos minerais, mediante a formulação e execução
                                       de planos e programas, em articulação com as entidades atuantes
                                       nesse setor;VIII - estabelecer as diretrizes e coordenar o
                                       processo de elaboração da política estadual de desenvolvimento
                                       científico e tecnológico, a ser implementada por intermédio das
                                       entidades integrantes do Sistema Estadual de Ciência e
                                       Tecnologia;IX - Elaborar a política estadual de energia,
                                       articulando-se com entidades de outros níveis de governo e
                                       coordenando a implementação das ações no âmbito do Estado do Rio
                                       Grande do Norte;X - Apoiar e fomentar projetos de expansão de
                                       oferta de energia, especialmente a geração de energias
                                       alternativas, no Estado do Rio Grande do Norte;XI -
                                       Supervisionar a execução das atividades de registro comercial e
                                       de metrologia e qualidade.Art.33(LC nº 163/1999) e da Lei
                                       Complementar Estadual n.º 262, de 29 de dezembro de 2003.</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 SECRETARIA DO DESENVOLVIMENTO RURAL E DA AGRICULTURA FAMILIAR - SEDRAF
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Secretário de Estado:</b> Alexandre de Oliveira Lima <br>
                                 <b>Telefones:</b> (84) 3232-7270 / (84) 98142-8764 <br>
                                 <!-- <Li><b>Fax:</b> (84) 3232-7280 / 7270</Li> -->
                                 <b>Website:</b> http://www.sedraf.rn.gov.br/<br>
                                 <b>E-mail:</b> gabinete@sedraf.rn.gov.br / sedraf.rn.2019@gmail.com /
                                 sedraf@rn.gov.br <br>
                                 <b>Horário de funcionamento:</b> 8h às 17h <br>
                                 <!-- (em obediência ao Decreto nº 28.692, de 2 de janeiro de 2019, prorrogado pelo Decreto Nº 28.963, de 1 de julho de 2019). -->
                                 <b>Endereço:</b> BR 101 KM 0 - Av. Senador Salgado Filho, S/N, Centro
                                 Administrativo do Estado, Lagoa Nova - Natal/RN. <br>
                                 CEP: 59064-901
                              </div>
                              <div class="pb-4">
                                 <a href="{{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_SEDRAF.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>Executar, coordenar e monitorar iniciativas, ações, programas e
                                       projetos voltados ao fortalecimento da agricultura familiar e
                                       comunidades tradicionais existentes do Rio Grande do Norte.
                                       A Secretaria de Estado de Assuntos Fundiários e Apoio à Reforma
                                       Agrária (Seara) foi criada pela Lei Complementar nº 207, de 5 de
                                       novembro de 2001, com a competência de: formular, propor e
                                       controlar a execução da política fundiária do Estado; formular,
                                       propor e controlar a execução da política de colonização e
                                       exploração rural, em apoio, principalmente, aos assentados da
                                       reforma agrária e às colônias de exploração rural; e exercer
                                       outras atribuições correlatas.
                                       A secretaria assiste, prioritariamente, o pequeno e médio
                                       produtor rural, promovendo a inclusão social através do acesso à
                                       terra, regularização fundiária e apoio à reforma
                                       agrária.Art.2º(LC649/2019)
                                    </p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 SECRETARIA DE ESTADO DA EDUCAÇÃO, DA CULTURA, DO ESPORTE E DO LAZER - SEEC
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Secretário de Estado:</b> Maria do Socorro da Silva Batista <br>
                                 <b>Telefones:</b> (84) 3232-1301 / 1303 / 1340 / 1370 <br>
                                 <b>Fax:</b> (84) 3232-1310 <br>
                                 <b>Website:</b> http://www.educacao.rn.gov.br <br>
                                 <b>Horário de funcionamento:</b> 8h às 18h <br>
                                 <b>Endereço:</b> BR 101 KM 0 - Av. Senador Salgado Filho, S/N, Centro
                                 Administrativo do Estado, Lagoa Nova - Natal/RN. <br>
                                 CEP: 59064-901
                              </div>
                              <div class="pb-4">
                                 <a href="{{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_SEEC.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>A Secretaria de Estado da Educação, da Cultura, do Esporte e do
                                       Lazer – SEEC/RN, integrante da Administração Pública Estadual
                                       Direta, é uma organização base da Administração Estadual para
                                       planejamento, organização, direção, controle e execução dos
                                       programas e projetos destinados à implementação da política
                                       governamental nos setores de educação e cultura. Garantir à
                                       população um ensino público de qualidade, assegurando-lhe a
                                       universalização do acesso e permanência dos alunos à escola,
                                       visando o pleno exercício da cidadania. Art.27(LC nº 163/1999)
                                    </p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 SECRETARIA DE ESTADO DO MEIO AMBIENTE E DOS RECURSOS HÍDRICOS - SEMARH
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Secretário de Estado:</b> Paulo Lopes Varella Neto <br>
                                 <b>Telefones:</b> (84) 3232-2410 <br>
                                 <!-- / 2400 / 2420 -->
                                 <!-- <li><b>Fax:</b> (84) 3232-2411</li> -->
                                 <b>Website:</b> http://www.semarh.rn.gov.br <br>
                                 <b>E-mail:</b> semarh@rn.gov.br <br>
                                 <b>Horário de funcionamento:</b> 8h às 14h <br>
                                 <!-- (em obediência ao Decreto nº 28.692, de 2 de janeiro de 2019, prorrogado pelo Decreto Nº 28.963, de 1 de julho de 2019). -->
                                 <b>Endereço:</b> Rua Dona Maria Câmara, 1884, Capim Macio - Natal/RN. <br>
                                 CEP: 590082-430
                              </div>
                              <div class="pb-4">
                                 <a href="{{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_SEMARH.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>A Secretaria de Estado dos Recursos Hídricos (SERHID) foi criada
                                       pela Lei Complementar n° 163, de 25 de fevereiro de 1996, e em
                                       31 de janeiro de 2007 foi transformada em Secretaria de Estado
                                       do Meio Ambiente e dos Recursos Hídricos (SEMARH), com a
                                       atribuição de planejar, coordenar e executar as ações públicas
                                       estaduais que contemplem a oferta e a gestão dos recursos
                                       hídricos e do Meio Ambiente no Estado do Rio Grande do Norte.
                                       Conduz a Política Estadual de Recursos Hídricos, compõe o
                                       Sistema Integrado de Gestão dos Recursos Hídricos e exerce a
                                       gestão do Fundo Estadual de Recursos Hídricos.
                                    </p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 SECRETARIA DE ESTADO DAS MULHERES, DA JUVENTUDE, DA IGUALDADE RACIAL E DOS
                                 DIREITOS HUMANOS - SEMJIDH
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Secretária de Estado:</b> Olga Aguiar de Melo <br>
                                 <b>Telefones:</b> (84) 98106-7492 <br>
                                 <!-- <li><b>Fax:</b> Em fase de instalação</li> -->
                                 <b>Website:</b> http://www.semjidh.rn.gov.br/ <br>
                                 <b>E-mail:</b> contato.semjidh@gmail.com <br>
                                 <b>Horário de funcionamento:</b> 8h às 18h <br>
                                 <!-- (em obediência ao Decreto nº 28.692, de 2 de janeiro de 2019, prorrogado pelo Decreto Nº 28.963, de 1 de julho de 2019). -->
                                 <b>Endereço:</b> Av. Miguel Castro, 3130, Lagoa Nova - Natal/RN. <br>
                                 CEP: 59075-740
                              </div>
                              <div class="pb-4">
                                 <a href="{{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviço_da_SEMJIDH.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>A Secretaria de Estado das Mulheres, da Juventude, da Igualdade
                                       Racial e dos Direitos Humanos - SEMJIDH, criada pelo Governo do
                                       Estado através da Lei Complementar nº 649, de 10 de maio de
                                       2019, atua, desde o dia 1º de junho, na formulação e
                                       implementação de políticas públicas destinadas aos segmentos
                                       vulnerabilizados da população, assim como para mulheres, pessoas
                                       com orientações sexuais e identidades de gênero diversas,
                                       negros, indígenas, crianças e adolescentes, juventude e pessoas
                                       com deficiência. Complementa o leque de direitos garantidores de
                                       cidadania, a coordenação da política estadual de direitos
                                       humanos, de proteção e defesa do consumidor, além da política
                                       sobre drogas.
                                    </p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 SECRETARIA DE ESTADO DO PLANEJAMENTO E DAS FINANÇAS - SEPLAN
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Secretário de Estado:</b> José Aldemir Freire <br>
                                 <b>Telefones:</b> (84) 3232-1900 <br>
                                 <b>Website:</b> http://www.seplan.rn.gov.br <br>
                                 <b>E-mail:</b> gabseplan@gmail.com <br>
                                 <b>Horário de funcionamento:</b> 8h às 18h <br>
                                 <b>Endereço:</b> BR 101 KM 0 - Av. Senador Salgado Filho, S/N, Centro
                                 Administrativo do Estado, Lagoa Nova - Natal/RN. <br>
                                 CEP: 59064-901
                              </div>
                              <div class="pb-4">
                                 <a href="{{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_SEPLAN.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>A SEPLAN tem a responsabilidade de planejar, coordenar, executar,
                                       supervisionar, controlar e avaliar os sistemas estaduais de
                                       Planejamento, Orçamento e Finanças.
                                       Compete ainda à Secretaria de Estado do Planejamento e das
                                       Finanças (SEPLAN):
                                       - Elaborar planos de desenvolvimento econômico, social,
                                       científico e tecnológico do Rio Grande do Norte;
                                       - Levantar e divulgar dados e informações sobre o sistema
                                       produtivo e a realidade social do Rio Grande do Norte;
                                       - Orientar a elaboração de propostas orçamentárias e de planos
                                       plurianuais pelas Secretarias de Estado e entidades
                                       descentralizadas;
                                       - Estabelecer os programas de execução orçamentária e acompanhar
                                       sua efetivação;
                                       - Estabelecer a programação financeira dos recursos do Estado;
                                       - Avaliar a programação orçamentária e financeira das entidades
                                       da Administração Indireta dependentes de repasses do Tesouro
                                       Estadual;
                                       - Controlar o movimento da tesouraria, envolvendo ingressos,
                                       pagamentos e disponibilidades;
                                       - Coordenar os entendimentos do Governo do Estado com entidades
                                       federais, internacionais e outras para obtenção de
                                       financiamentos e/ou recursos a fundo perdido, destinados ao
                                       desenvolvimento de programas estaduais; e,
                                       - Coordenar o Sistema de Informações Governamentais, em especial
                                       as prestações de contas das Secretarias para encaminhamento à
                                       Controladoria Geral do Estado, com vistas ao cumprimento do art.
                                       22, VIII, da Lei Complementar nº 163/99.
                                       As atividades compreendidas na área de competência da Secretaria
                                       de Estado do Planejamento e das Finanças (SEPLAN) são exercidas
                                       por:
                                       - Órgãos integrantes de sua estrutura organizacional;
                                       - Entidades da Administração Indireta a ela vinculadas e outras
                                       sujeitas à sua supervisão; e,
                                       - Mecanismos especiais de natureza transitória;
                                    </p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 SECRETARIA DE ESTADO DA SAÚDE PÚBLICA - SESAP
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Secretária de Estado:</b> Lyane Ramalho Cortez <br>
                                 <b>Telefones:</b> (84) 3232-7432 / 7456/ 2548 <br>
                                 <b>Fax:</b> (84) 3232-2614 <br>
                                 <b>Website:</b> http://www.saude.rn.gov.br <br>
                                 <b>Email:</b> gs.sesap@gmail.com <br>
                                 <b>Horário de funcionamento:</b> das 8h às 18h <br>
                                 <!-- (em obediência ao Decreto nº 28.692, de 2 de janeiro de 2019, prorrogado pelo Decreto Nº 28.963, de 1 de julho de 2019) -->
                                 <b>Endereço:</b> Av. Marechal Deodoro da Fonseca, 730, 8º andar, Cidade Alta
                                 - Natal/RN. <br>
                                 CEP: 59025-600
                              </div>
                              <div class="pb-4">
                                 <a href="{{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviço_SESAP.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>Formular, coordenar e implementar, de forma regionalizada e
                                       participativa, a política estadual de atenção à saúde,
                                       assegurando à população ações de promoção, vigilância em saúde e
                                       o acesso integral, humanizado e resolutivo, conforme princípios
                                       e diretrizes do Sistema Único dessaúde – SUS- Lei Complementar
                                       nº 163, de 05/02/1999, alterada posteriormente pelos
                                       dispositivos da Lei Complementar nº 168, de 28/10/1999, e mais
                                       adiante pela Lei Complementar nº 215, de 11/12/2001, publicada
                                       no DOE n° 10.039.
                                    </p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 SECRETARIA DE ESTADO DA SEGURANÇA PÚBLICA E DA DEFESA SOCIAL - SESED
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Secretário de Estado:</b> Francisco Canindé de Araújo Silva <br>
                                 <b>Telefones:</b> (84) 3232-1082 / 1083 / 1084 <br>
                                 <b>Fax:</b> (84) 3232-1081 <br>
                                 <b>Website:</b> http://www.defesasocial.rn.gov.br <br>
                                 <b>E-mail:</b> sesed@rn.gov.br <br>
                                 <b>Horário de funcionamento:</b> 8h às 18h.<br>
                                 <b>Endereço:</b> BR 101 KM 0 - Av. Senador Salgado Filho, S/N, Centro
                                 Administrativo do Estado, Lagoa Nova - Natal/RN. <br>
                                 CEP: 59064-901
                              </div>
                              <div class="pb-4">
                                 <a href="{{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_SESED.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>Promover a segurança pública do Rio Grande do Norte.
                                       Com base nos termos do Art. 29 da Lei Complementar nº 163, de 05
                                       de fevereiro de 1999, e alterações introduzidas pela Lei
                                       Complementar nº 209 de 19 de novembro de 2021, compete:
                                       I- Promover a defesa dos direitos do cidadão e da normalidade
                                       social, através dos órgãos e mecanismos de segurança pública;
                                       II- Programas, supervisionar, orientar e controlar as atividades
                                       de polícia ostensiva, de polícia judiciária e a apuração de
                                       infrações penais, sob a responsabilidade das Polícias Militar e
                                       Civil, promovendo os meios necessários à defesa social,
                                       respeitada a competência da União e assegurada a cooperação com
                                       as autoridades federais e dos demais Estados e Distrito Federal;
                                       III- Exercer atribuições de polícia administrativa e judiciária,
                                       executando ações policiais típicas, preventivas e repressivas,
                                       em todo território do Estado;
                                       IV- Praticar atos de natureza assecurarória, disciplinar,
                                       instrumental e educativa, no exercício das atividades de
                                       polícia;
                                       V- Auxiliar as autoridades do Poder Judiciário e atender às
                                       requisições de força policial para o cumprimento de suas
                                       decisões;
                                       VI- Desenvolver políticas de respeito à pessoa humana e os
                                       direitos dos cidadãos, no exercício das atividades de polícia,
                                       com rigorosa observância das garantias constitucionais e legais;
                                       VII- Propiciar aos interessados, mediante requerimento dirigido
                                       ao titular da Pasta, acesso às informações que lhes digam
                                       respeito;
                                       VIII- Fomentar a participação da sociedade civil e de órgãos
                                       representativos dos seus diversos setores no processo de
                                       aperfeiçoamento da política de defesa social do Estado;
                                       IX- Coordenar as ações conjuntas dos órgãos operativos
                                       integrantes do Sistema de Defesa Social, estabelecendo formas de
                                       observação e controle para evitar superposição de ações,
                                       conflitos operacionais e de competência;
                                       X- Formular e submeter à aprovação governamental a política de
                                       Defesa Social do Estado;
                                       XI- Reprimir, de forma pronta e eficaz, sem prejuízo da
                                       observância das garantias legais, quaisquer abusos praticados
                                       por autoridades investidas de função policial sob sua
                                       circunscrição.

                                    </p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 SECRETARIA DA FAZENDA DO RIO GRANDE DO NORTE- SEFAZ/RN
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Secretário de Estado:</b> Carlos Eduardo Xavier <br>
                                 <b>Telefones:</b> (84) 3232-2169 <br>
                                 <b>Call Center:</b> (84) 3209-7880 <br>
                                 <!-- 2052 / 2169 / 2196 / 2197 / 2199 -->
                                 <!-- (84) 3232-2038 -->
                                 <b>Website:</b> http://www.set.rn.gov.br <br>
                                 <b>Portal de Serviços:</b> https://uvt.set.rn.gov.br/#home <br>
                                 <b>Horário de funcionamento:</b> 8h às 14h <br>
                                 <!-- (em obediência ao Decreto nº 28.692, de 2 de janeiro de 2019, prorrogado pelo Decreto Nº 28.963, de 1 de julho de 2019). -->
                                 <b>Endereço:</b> BR 101 KM 0 - Av. Senador Salgado Filho, S/N, Centro
                                 Administrativo do Estado, Lagoa Nova - Natal/RN. <br>
                                 CEP: 59064-901
                              </div>
                              <div class="pb-4">
                                 <a href="{{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_SEFAZ.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                <!-- <a href="{ { route('cartasdeservico-set') }}"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;"> CARTAS SEPARADAS -->
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>Tem como finalidade arrecadar os tributos de competência estadual
                                       e controlar as atividades de fiscalização das operações
                                       geradoras de tributos, em consonância com a legislação
                                       pertinente.
                                       Conforme o Regulamento da SET, Decreto Nº 22.088, De 16 De
                                       Dezembro De 2010, Art. 2º: À Secretaria de Estado da Tributação
                                       compete:
                                       I - dirigir e executar a política de administração fiscal e
                                       tributária do Estado;
                                       II - realizar estudos e pesquisas para a previsão da receita,
                                       bem como adotar providências executivas para a obtenção de
                                       recursos financeiros de origem tributária e outros;
                                       III - manter cadastro de contribuintes contendo todos os dados
                                       necessários ao exercício das atividades de fiscalização,
                                       previsão de receitas e planejamento tributário do Estado;
                                       IV - orientar os contribuintes sobre a aplicação e a
                                       interpretação da legislação tributária;
                                       V - informar à população os valores de taxas, contribuições,
                                       multas, licenças, alvarás e certidões;
                                       VI - criar mecanismos de articulação permanente com os setores
                                       econômicos do Estado, visando debater a regulamentação e
                                       aplicação da política tributária, o endividamento fiscal das
                                       empresas e a negociação de alternativas para o equacionamento
                                       desses débitos fiscais.

                                    </p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 SECRETARIA DE ESTADO DO TRABALHO, DA HABITAÇÃO E DA ASSISTÊNCIA SOCIAL -
                                 SETHAS
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Secretária de Estado:</b> Iris Maria de Oliveira <br>
                                 <b>Telefones:</b> (84) 3190-0730 <br>
                                 <!-- (84) 3232-1810 / 1816 / 1850 / 1870 -->
                                 <!-- <li><b>Fax:</b> (84) 3232-1830</li> -->
                                 <b>Website:</b> http://www.sethas.rn.gov.br <br>
                                 <b>E-mail:</b> expediente.sethas@gmail.com <br>
                                 <b>Horário de funcionamento:</b> Secretaria: 8h às 17h / Restaurante
                                 Popular: 11h às 14h / Café do Trabalhador: 6h às 8h <br>
                                 <b>Endereço:</b> BR 101 KM 0 - Av. Senador Salgado Filho, S/N, Centro
                                 Administrativo do Estado, Lagoa Nova - Natal/RN. <br>
                                 CEP: 59064-901
                              </div>
                              <div class="pb-4">
                                 <a href="{{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviço_SETHAS.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>Responsável pela gestão das seguintes políticas públicas:
                                       assistência social, trabalho, habitação, políticas estaduais das
                                       pessoas idosa, do artesanato, da economia solidária, de
                                       refugiados apátridas e migrantes e de segurança alimentar.
                                       Art.36(LC nº 163/1999)

                                    </p>
                                 </div>

                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 SECRETARIA DE ESTADO DO TURISMO - SETUR
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Secretária de Estado:</b> Ana Maria da Costa <br>
                                 <b>Telefones:</b> 3232-2486 / (84) 3232-2496 (WhattsApp) <br>
                                 <!-- / 2518 -->
                                 <!-- <li><b>Fax:</b> (84) 3232-2502</li> -->
                                 <b>Website:</b> http://www.setur.rn.gov.br <br>
                                 <b>E-mail:</b> rnsetur@gmail.com <br>
                                 <b>Horário de funcionamento:</b> 8h às 14h <br>
                                 <!-- (em obediência ao Decreto nº 28.692, de 2 de janeiro de 2019, prorrogado pelo Decreto Nº 28.963, de 1 de julho de 2019). -->
                                 <b>Endereço:</b> Via Costeira Sen. Dinarte Medeiros Mariz, S/N, Centro de
                                 Convenções Ponta Negra, Ponta Negra - Natal/RN. <br>
                                 CEP: 59090-002
                              </div>
                              <div class="pb-4">
                                 <a href=" {{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_SETUR.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>A Setur também tem como uma de suas competências promover
                                       políticas que auxiliem no avanço de políticas de infraestrutura
                                       turística, melhorando a qualidade dos serviços públicos
                                       acessados por turistas e pelos potiguares. Com a competência de
                                       formular políticas para o desenvolvimento do turismo no Rio
                                       Grande do Norte, a Secretaria de Estado do Turismo (SETUR) foi
                                       criada em 05 de junho de 1996 e atua em várias frentes,
                                       destacando-se o desenvolvimento de estudos e pesquisas para
                                       avaliar as potencialidades turísticas do estado e a articulação
                                       com os municípios potiguares e demais órgãos da administração
                                       pública para o fomento da atividade turística.Art.32(LC nº
                                       163/1999)

                                    </p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 SECRETARIA DE ESTADO DA INFRA ESTRUTURA - SIN
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Secretário de Estado:</b> Gustavo Fernandes Rosado Coelho <br>
                                 <b>Telefones:</b> (84) 3232-1656 / 1610 / 1611 <br>
                                 <!-- / 1656 / 1610 / 1611 -->
                                 <b>Fax:</b> (84) 3232-1660 <br>
                                 <!-- / 1660 / 1612 / 1656 -->
                                 <b>Website:</b> http://www.sin.rn.gov.br <br>
                                 <b>E-mail:</b> infraestruturarn@gmail.com <br>
                                 <b>Horário de funcionamento:</b> 8h às 14h <br>
                                 <!-- (em obediência ao Decreto nº 28.692, de 2 de janeiro de 2019, prorrogado pelo Decreto Nº 28.963, de 1 de julho de 2019) -->
                                 <b>Endereço:</b> BR 101 KM 0 - Av. Senador Salgado Filho, S/N, Centro
                                 Administrativo do Estado, Lagoa Nova - Natal/RN. <br>
                                 CEP: 59064-901
                              </div>
                              <div class="pb-4">
                                 <a href="{{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_SIN.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>Planejar e executar as ações públicas estaduais nos setores de
                                       viação, transportes, energia, obras e serviços de engenharia e
                                       avaliações de bens imóveis da Administração Estadual Direta.
                                       Secretaria de Estado da Infraestrutura - SIN, órgão de natureza
                                       substantiva integrante da Administração Estadual Direta, nos
                                       termos da Lei Complementar No 163/99 compete:
                                       promover medidas para implantação da política estadual de
                                       viação;
                                       definir políticas públicas relativas a energia elétrica,
                                       rodovias e serviços de trânsito;
                                       controlar, operacional e funcionalmente, a aplicação de
                                       recursos federais nos setores de obras e transportes no Estado;
                                       projetar, licitar, executar, fiscalizar e receber, direta e
                                       indiretamente, obras e serviços de engenharia de interesse da
                                       Administração Direta, excetuando-se as obras da Secretaria dos
                                       Recursos Hídricos e suas vinculadas;
                                       sugerir a desapropriação de imóveis e benfeitorias, realizar
                                       vistorias, avaliações e perícias em edifícios e imóveis públicos
                                       e particulares que se destinem ao uso da Administração Pública
                                       Estadual.º controlar e fiscalizar os custos operacionais e
                                       promover medidas visando a maximização dos investimentos do
                                       Estado nas diferentes modalidades de transportes.
                                       Art.31(LC163/1999)

                                    </p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 GOVERNO CIDADÃO
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Coordenação Geral:</b> Virgínia Ferreira <br>
                                 <b>Telefones:</b> (84) 3232-8722 / 1818 (Recepção Gabinete) <br>
                                 <b>Website:</b> www.governocidadao.rn.gov.br <br>
                                 <b>E-mail:</b> rnsustentavel@rn.gov.br <br>
                                 <b>Horário de funcionamento:</b> 8h às 18h <br>
                                 <!-- (em obediência ao Decreto nº 28.692, de 2 de janeiro de 2019, prorrogado pelo Decreto Nº 28.963, de 1 de julho de 2019) -->
                                 <b>Endereço:</b> BR 101 KM 0 - Av. Senador Salgado Filho, S/N, Centro
                                 Administrativo do Estado, Lagoa Nova - Natal/RN. <br>
                                 CEP: 59064-901
                              </div>
                              <div class="pb-4">
                                 <a href="{{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_GOVCIDADAO.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>A Secretaria de Estado do Planejamento, do Orçamento e Gestão –
                                       SEPLAN é a Coordenadora Geral do Projeto, e através da Unidade
                                       de Gerenciamento do Projeto Integrado de Desenvolvimento
                                       Sustentável (Projeto Governo Cidadão) é um projeto
                                       multissetorial que tem como meta contribuir, com as mudanças no
                                       cenário socioeconômico do Rio Grande do Norte, através da
                                       implementação de um conjunto de ações articuladas destinadas a
                                       reverter o baixo dinamismo do Estado, com foco na redução das
                                       desigualdades regionais.

                                    </p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 VICE-GOVERNADORIA - GVG
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Vice-Governador:</b> Walter Pereira Alves <br>
                                 <b>Telefones:</b> (84) 3232-1184 / 3232-1865 / 7286 <br>
                                 <!-- / 1865 / 7286 -->
                                 <!-- <li><b>Fax:</b> (84) 3232-1122 <br/></li> -->
                                 <b>Website</b> http://www.gvg.rn.gov.br/ <br>
                                 <b>Email:</b> vicegovernador@gvg.rn.gov.br <br>
                                 <b>Horário de funcionamento:</b> 8h às 17h <br>
                                 <b>Endereço:</b> BR 101 KM 0 - Av. Sen. Salgado Filho, 1808 A, Lagoa Nova -
                                 Natal/RN. <br>
                                 CEP: 59056-000
                              </div>
                              <div class="pb-4">
                                 <a href=" {{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_GVG.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>Dar assistência direta e imediata ao Vice-governador.
                                       Ao Gabinete do Vice-Governador compete: I - dar assistência
                                       direta e imediata ao Vice-Governador; II - receber e analisar,
                                       estudar e triar o expediente encaminhado ao Vice-Governador; III
                                       - prover a Vice-Governadoria dos meios necessários ao seu
                                       funcionamento: e IV - executar outras atividades determinadas
                                       pelo Vice-Governador. Art. 24º (LC 163/1999)
                                    </p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                  </div>
               </section>
            </main>

            <main class="pl-4 pb-4 md:w-1/2">
               <section class="shadow row bg-white rounded-lg">
                  <div class="tabs">

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <header class="p-5 pl-8 pr-8 select-none tab-label">
                              <span class="text-gray-500 text-sm text-center" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 ADMINISTRAÇÃO INDIRETA
                              </span>
                           </header>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 AGÊNCIA DE FOMENTO DO RN - AGN </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Diretora Geral:</b> Márcia Faria Maia <br>
                                 <b>Telefones:</b> (84) 3232-1570 / 4204 <br>
                                 <b>Website:</b> http://www.agn.rn.gov.br/ agn@agnrn.com.br <br>
                                 <b>Horário de funcionamento:</b> 8h às 16h <br>
                                 <b>Endereço:</b> Rua Seridó, 466, Petrópolis - Natal/RN. <br>
                                 CEP: 59020-010
                              </div>
                              <div class="pb-4">
                                 <a href="{{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviço_AGN.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>Fomentar o desenvolvimento econômico e sustentável do Rio Grande do Norte, promovendo a criação de
                                       emprego e renda, estimulando a modernização das estruturas econômicas e sociais, aumentando assim,
                                       a competitividade empresarial do Estado.</p>

                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 AGÊNCIA REGULADORA DE SERVIÇOS PÚBLICOS DO RN - ARSEP
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Diretora presidente:</b> Rosângela Maria Fonseca de Oliveira <br>
                                 <b>Telefones:</b> 0800 999 1900 / (84) 99606-2018 (WhatsApp) <br>
                                 <!-- <Li><b>Fax:</b> (84) 3232-1666 </Li> -->
                                 <b>Website:</b> http://www.arsep.rn.gov.br <br>
                                 <b>E-mail:</b> arsep@rn.gov.br <br>
                                 <b>Horário de funcionamento:</b> 8h às 14h <br>
                                 <!-- (em obediência ao Decreto nº 28.692, de 2 de janeiro de 2019, prorrogado pelo Decreto Nº 28.963, de 1 de julho de 2019). -->
                                 <b>Endereço:</b> Rua Demócrito de Souza, 1508, Lagoa Nova - Natal/RN. <br>
                                 CEP: 59062-440
                              </div>
                              <div class="pb-4">
                                 <a href="{{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_ARSEP.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>Autarquia sob regime especial, cuja estrutura organizacional é formada por uma diretoria colegiada (um
                                       Diretor-Presidente e dois Diretores Autárquicos), seis coordenadorias técnico-administrativas e uma
                                       unidade administrativa e financeira. A Diretoria é indicada pelo Chefe do Executivo Estadual e sabatinada
                                       pela Assembleia Legislativa, conforme estabelecido pela Lei Complementar Estadual nº 584/2016.<br>
                                       Regulando, controlando, fiscalizando os serviços públicos de Energia, Saneamento e Gás.<br>
                                       Promover e zelar pela eficiência econômica e técnica dos serviços públicos; proteger o
                                       usuário contra abusos; fornecer subsídios aos processos de reajustes, revisão e definição
                                       de tarifas para os serviços; regular e fiscalizar a atuação dos prestadores de serviços
                                       públicos no Estado do Rio Grande do Norte.</p>

                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 COMPANHIA DE ÁGUAS E ESGOTOS DO RN - CAERN
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Diretor Presidente:</b> Roberto Sergio Ribeiro Linhares <br>
                                 <b>Telefones:</b> (84) 3232-4100/ 4107 / (84) 98118-8400 (WhatsApp) <br>
                                 <b>Ouvidoria:</b> 0800 842 0100 <br>
                                 <!-- (84) 3232-4106 -->
                                 <b>Website:</b> http://www.caern.com.br <br>
                                 <b>Horário de funcionamento:</b> Escritórios: 7h30 às 17h30 <br>
                                 <b>Endereço:</b> Av. Senador Salgado Filho, 1555, Tirol - Natal/RN. <br>
                                 CEP: 59015-000
                              </div>
                              <div class="pb-4">
                                 <a href=" {{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_CAERN.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>Atender toda a população do Rio Grande do Norte com água potável, coleta e tratamento de
                                       esgotos.<br>
                                       I - Planejar, projetar, executar, ampliar, manter e explorar industrialmente o sistema de água potável
                                       e esgotos sanitários;<br>II - fixar e arrecadar tarifas, provenientes de serviços prestados, promovendo
                                       realinhamento periódico, de modo que atendam à cobertura das amortizações dos investimentos,
                                       dos custos de operação e manutenção e acúmulo de reservas para expansão dos sistemas. Art. 52º
                                       (LC 163/1999)</p>

                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 CENTRAIS DE ABASTECIMENTO DO RIO GRANDE DO NORTE S/A - CEASA
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Diretor Presidente:</b> Flávio Morais <br>
                                 <b>Telefones:</b> Em fase de instalação<br>
                                 <!-- <Li><b>Fax:</b> (84) 3232-5284 </Li> -->
                                 <b>Website:</b> http://www.ceasa.rn.gov.br <br>
                                 <b>E-mail:</b> Não encontrado <br>
                                 <b>Horário de funcionamento:</b> Administração: 7h às 13h (segunda a
                                 sexta) <br>
                                 Mercado: das 03h às 13h (segunda a sábado) <br>
                                 <b>Endereço:</b> Av. Cap. Mor Gouveia, 3005, Lagoa Nova - Natal/RN. <br>
                                 CEP: 59060-400
                              </div>
                              <div class="pb-4">
                                 <a href="{{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviço_CEASA.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>As Centrais de Abastecimento do Estado do Rio Grande do Norte S/A – CEASA-RN foi inaugurada
                                       em 17 de outubro de 1976, com o objetivo de oferecer uma estrutura para que agricultores,
                                       comerciantes, cooperativas e empresas do agronegócio realizem operações comerciais no atacado
                                       e varejo de produtos hortifrutigranjeiros e outros produtos alimentícios. Visando implementar uma
                                       política de produção e abastecimento de hortifrutigranjeiros no RN, a Ceasa-RN se constitui como
                                       uma empresa de economia mista e está vinculada à Secretaria de Estado da Agricultura, da
                                       Pecuária e da Pesca (Sape-RN).</p>

                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 COMPANHIA DE PROCESSAMENTO DE DADOS DO RN - DATANORTE
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Diretor Geral:</b> Jonny Araújo da Costa <br>
                                 <b>Telefones:</b> (84) 3232-9709 <br>
                                 <!-- / 45 -->
                                 <!-- <Li><b>Fax:</b> (84) 3232-9723</Li> -->
                                 <b>Website:</b> http://www.datanorte.rn.gov.br/ <br>
                                 <b>E-mail:</b> datanorte@rn.gov.br <br>
                                 <b>Horário de funcionamento:</b> 8h às 14h<br>
                                 <!-- (em obediência ao Decreto nº 28.692, de 2 de janeiro de 2019, prorrogado pelo Decreto Nº 28.963, de 1 de julho de 2019).  -->
                                 <b>Endereço:</b> Praça Augusto Severo, 264/66, Ribeira - Natal/RN. <br>
                                 CEP: 59012-380
                              </div>
                              <div class="pb-4">
                                 <a href="{{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviço_Datanorte.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 COMPANHIA ESTADUAL DE HABITAÇÃO E DESENVOLVIMENTO - CEHAB
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Diretor Presidente:</b> Pablo Thiago Lins de Oliveira Cruz<br>
                                 <b>Telefones:</b> (84) 3232-1899 <br>
                                 <!-- 0660/1157 -->
                                 <!-- <Li><b>Fax:</b> (84) 3232-1803</Li> -->
                                 <b>Website:</b> http://www.cehab.rn.gov.br <br>
                                 <b>E-mail:</b> cg.cehab@gmail.com <br>
                                 <b>Horário de funcionamento:</b> 8h às 14h <br>
                                 <!-- (em obediência ao Decreto nº 28.692, de 2 de janeiro de 2019, prorrogado pelo Decreto Nº 28.963, de 1 de julho de 2019). </Li> -->
                                 <b>Endereço:</b> BR 101 KM 0 - Av. Senador Salgado Filho, S/N, Centro
                                 Administrativo do Estado, Lagoa Nova - Natal- RN. <br>
                                 CEP: 59064-901

                              </div>
                              <div class="pb-4">
                                 <a href="{{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_CEHAB.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;" s>
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>A Companhia Estadual de Habitação e Desenvolvimento Urbano (CEHAB), foi institucionalizada e
                                       autorizada pela Lei Complementar Estadual n° 338, de 24 de janeiro de 2007, é uma Sociedade de
                                       Economia Mista, de capital fechado, integrante da Administração Indireta do Estado do Rio Grande
                                       do Norte. Desenvolver políticas habitacionais de interesse social orientadas pelos princípios da
                                       sustentabilidade ambiental urbana e rural, efetuar pesquisas tecnológicas concernentes à habitação
                                       de baixo custo, operacionalizar políticas de desenvolvimento urbano visando minimizar o déficit
                                       habitacional da população menos favorecida do estado do Rio Grande do Norte.</p>

                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 COMPANHIA POTIGUAR DE GÁS DO RN - POTIGÁS
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Diretora Geral:</b> Marina Melo Alves Siqueira <br>
                                 <b>Telefones:</b> (84) 3204-8500 <br>
                                 <!-- <li><b>Fax:</b> (84) 3204-8505</li> -->
                                 <b>Website:</b> http://www.potigas.com.br/ <br>
                                 <b>E-mail:</b> faleconosco@potigas.com.br <br>
                                 <b>Horário de funcionamento:</b> 8h às 12h e 13h às 17h <br>
                                 <b>Endereço:</b> Av. Prudente de Morais, 675, Tirol - Natal/RN. <br>
                                 CEP: 59020-505
                              </div>
                              <div class="pb-4">
                                 <a href=" {{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_POTIGAS.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 CORPO DE BOMBEIROS MILITAR DO RN - CBM
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Comandante Geral:</b> Coronel BM Luis Monteiro da Silva Junior <br>
                                 <b>Telefones:</b> (84) 3232-6871 <br>
                                 <!-- 1568 / 1566  -->
                                 <b>Emergencia:</b> 193 <br>
                                 <!-- (84) 3232-6871 -->
                                 <b>Website:</b> http://www.cbm.rn.gov.br <br>
                                 <b>Horário de funcionamento:</b> 7h às 16h <br>
                                 <b>Endereço:</b> Av.Prudente de Morais, 2410, Barro Vermelho, Natal - RN.
                                 <br>
                                 CEP: 59030-350
                              </div>
                              <div class="pb-4">
                                 <a href=" {{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_CBM.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>O atual Corpo de Bombeiros Militar do Estado do Rio Grande do Norte surgiu na segunda década do
                                       século XX, através da lei nº 424, de 29 de novembro de 1917, sancionada pelo governador Joaquim
                                       Ferreira Chaves, que criou uma Seção de Bombeiros anexa ao Esquadrão de Cavalaria, tendo como
                                       missão proteger a vida, o patrimônio e o meio ambiente, além de fomentar de maneira contínua e
                                       permanente as ações de proteção e defesa civil.</p>

                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 DEFENSORIA PÚBLICA GERAL DO ESTADO - DPGE
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Defensor Publico Geral:</b> Marcus Vinicius Soares Alves <br>
                                 <b>Telefones:</b> (84) 3606-0376 <br>
                                 <!-- <Li><b>Fax:</b> (84) 3232-7451</Li> -->
                                 <b>Website:</b> http://www.defensoria.rn.gov.br <br>
                                 <b>Horário de funcionamento:</b> 8h às 14h <br>
                                 <!-- (em obediência ao Decreto nº 28.692, de 2 de janeiro de 2019, prorrogado pelo Decreto Nº 28.963, de 1 de julho de 2019). -->
                                 <b>Endereço:</b> Rua Sérgio Severo, 2037, Lagoa Nova - Natal/RN. <br>
                                 CEP: 59063-380
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 DEPARTAMENTO DE ESTRADAS DE RODAGEM - DER
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Diretora Geral:</b> Natécia Shiley Nunes  <br>
                                 <b>Telefones:</b> (84) 3232-2350 / 2310 <br>
                                 <!-- / 2365 / 2345 -->
                                 <!-- <Li><b>Fax:</b> (84) 3232-2370 </Li> -->
                                 <b>Website:</b> http://www.der.rn.gov.br <br>
                                 <b>E-mail:</b> derdg@rn.gov.br <br>
                                 <b>Horário de funcionamento:</b> 8h às 14h <br>
                                 <!-- (em obediência ao Decreto nº 28.692, de 2 de janeiro de 2019, prorrogado pelo Decreto Nº 28.963, de 1 de julho de 2019)  -->
                                 <b>Endereço:</b> Av. Senador Salgado Filho, 1808, Lagoa Nova - Natal/RN.
                                 <br>
                                 CEP: 59075-000
                              </div>
                              <div class="pb-4">
                                 <a href=" {{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_DER.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>Planejar, executar e controlar os serviços de conservação reparação, restauração,
                                       melhoramento, adequação da capacidade e ampliação de malha viária estadual.</p>

                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 DEPARTAMENTO ESTADUAL DE IMPRENSA - DEI
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Diretora Geral:</b> Flávia Celeste Martini Assaf <br>
                                 <b>Telefones:</b> (84) 3232-6780 <br>
                                 <b>Website:</b> http://www.dei.rn.gov.br <br>
                                 <b>E-mail:</b> diretoria.dei@rn.gov.br <br>
                                 <b>Horário de funcionamento:</b> Administração: 8h às 14h <br>
                                 <!-- (em obediência ao Decreto nº 28.692, de 2 de janeiro de 2019, prorrogado pelo Decreto Nº 28.963, de 1 de julho de 2019) Publicação: das 8h às 17h. -->
                                 <b>Endereço:</b> Av. Câmara Cascudo, 355, Prédio da República, Ribeira -
                                 Natal/RN.
                                 CEP 59025-280
                              </div>
                              <div class="pb-4">
                                 <a href=" {{ asset('http://diariooficial.rn.gov.br/dei/dorn3/Carta_de_Servicos_ao_Cidadao.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>É o sistema gráfico e editorial do Poder Executivo. Departamento Estadual de Imprensa é um órgão
                                       da administração vinculado à Assessoria de Comunicação Social, conforme a Lei Complementar nº
                                       163, de 05 de fevereiro de 1999. O D.E.I, conforme a sua regulamentação, tem por objetivos a
                                       edição do Diário Oficial e outras publicações de interesse do Estado, como livros, revistas e
                                       periódicos, bem como a execução de serviços gráficos oficiais ou de interesse de terceiros.<br>
                                       Compete-lhe:<br>
                                       a) editar, imprimir, distribuir e comercializar o Diário Oficial;<br>
                                       b) explorar, industrial e comercialmente, a edição de livros, revistas e publicações de fins;<br>
                                       educativos, culturais, sócio-econômicos, científicos e de caráter informativo;<br>
                                       c) apoiar as atividades da Assessoria de Comunicação Social.</p>

                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 DEPARTAMENTO ESTADUAL DE TRÂNSITO DO RN - DETRAN
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Diretor Geral:</b> Jonielson Pereira de Oliveira <br>
                                 <b>Telefones:</b> (84) 99660-6816 <br>
                                 <!-- 1207 / 1208 -->
                                 <!-- <Li><b>Fax:</b> (84) 3232-1206</Li> -->
                                 <b>Website:</b> http://www.detran.rn.gov.br/ <br>
                                 <b>E-mail:</b> www.detran.rn.gov.br/ <br>
                                 <b>Horário de funcionamento:</b> 8h às 14h (sede) <br>
                                 <!-- (em obediência ao Decreto nº 28.692, de 2 de janeiro de 2019, prorrogado pelo Decreto Nº 28.963, de 1 de julho de 2019). -->
                                 <b>Endereço:</b> Av. Perimetral Leste, 113, Cidade da Esperança - Natal/RN.
                                 <br>
                                 CEP: 59071-445
                              </div>
                              <div class="pb-4">
                                 <a href="{{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_DETRAN.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>Objetivo principal a disciplina e a fiscalização dos serviços de trânsito e tráfego no âmbito do
                                       Estado, nos termos do Código de Trânsito Brasileiro (CTB) e demais Leis aplicáveis, cumprindo as
                                       determinações do CONTRAN (Conselho Nacional de Trânsito) e do CETRAN (Conselho Estadual de
                                       Trânsito). O DETRAN/RN faz parte do Sistema Nacional de Transito é um órgão executivo de
                                       Trânsito do Estado. Tem como base a legislação do Conselho Nacional de Trânsito (Contran).
                                       Atualmente é vinculado à Secretaria de Infraestrutura do Rio Grande do Norte (Lei Complementar nº
                                       163/99).</p>

                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 EMPRESA DE PESQUISA AGROPECUÁRIA DO RN - EMPARN
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Diretor Presidente:</b> Rodrigo Oliveira Maranhão <br>
                                 <b>Telefones:</b> (84) 3232-5864 <br>

                                 <b>Website:</b> http://www.emparn.rn.gov.br/ <br>
                                 <b>E-mail:</b> emparn@rn.gov.br <br>
                                 <b>Horário de funcionamento:</b> 7h30 às 17h <br>
                                 <b>Endereço:</b> Av. Eliza Branco Pereira dos Santos (COOPHAB), S/N, Cx.
                                 Postal 188
                                 Parque das Nações - Parnamirim/RN. <br>
                                 CEP: 59158-160
                              </div>
                              <div class="pb-4">
                                 <a href="{{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviço_EMPARN.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>Promover, planejar, estimular, supervisionar, coordenar e executar as atividades de pesquisa e
                                       experimentação no Estado, visando a criar e desenvolver conhecimentos e tecnologia a serem
                                       empregadas no desenvolvimento do setor agropecuário estadual; II - colaborar na formulação, orientação
                                       e coordenação da política agropecuária do Estado, bem como programar e desenvolver pesquisas,
                                       diretamente ou em cooperação com instituições próprias, referentes a assuntos florestais, pesca,
                                       meteorologia e outras modalidades compreendidas na área de atuação da Secretaria de Agricultura e
                                       Abastecimento. III - exercer a coordenação técnica dos programa e projetos de pesquisa agropecuária
                                       cuja execução envolva a atuação técnico-administrativa ou a cooperação financeira de órgãos e/ou
                                       entidades da administração estadual, direta e indireta; IV - prestar serviços de sua especialidade a
                                       qualquer entidade pública ou privada, mediante prévio ajuste; V - exercer outras atividades correlatas.
                                       uma Empresa Pública vinculada à Secretaria de Agricultura, da Pecuária e da Pesca - SAPE, dotada de
                                       personalidade jurídica de direito privado, com patrimônio próprio e autonomia administrativa e financeira,
                                       regendo-se pela Lei Estadual nº 4.855, de 11 de setembro de 1979, pela Lei Federal nº 6.404, de 15 de
                                       dezembro de 1976, por seus estatutos, aprovado pelo Decreto Estadual nº 7.866, de 7 de abril de 1980, e
                                       demais normas de direito aplicáveis, subsidiariamente, pelos princípios consignados no decreto federal nº
                                       200, de 25 de fevereiro de 1967. Art.50(LC163/1999)</p>

                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 EMPRESA GESTORA DE ATIVOS DO RN - EMGERN
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Diretor Presidente:</b> Kaio César Carneiro <br>
                                 <b>Telefones:</b> (84) 3211-5240 <br>
                                 <b>Website:</b> http://www.emgern.rn.gov.br <br>
                                 <b>E-mail:</b> ouvidoria.emgern@gmail.com <br>
                                 <b>Horário de funcionamento:</b> 7h às 13h<br>
                                 <!-- (em obediência ao Decreto nº 28.692, de 2 de janeiro de 2019, prorrogado pelo Decreto Nº 28.963, de 1 de julho de 2019). -->
                                 <b>Endereço:</b> Rua Frei Miguelinho, 109, 1º andar, Ribeira - Natal/RN.
                                 <br>
                                 CEP: 59012-180
                              </div>
                              <div class="pb-4">
                                 <a href=" {{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_EMGERN.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>A Empresa Gestora de Ativos do Rio Grande do Norte S.A. (EMGERN), cuja criação foi
                                       autorizada pela Lei Complementar Estadual n.° 288, de 1° de fevereiro de 2005, é uma
                                       Empresa Pública, organizada sob a forma de sociedade por ações de capital fechado,
                                       integrante da Administração Indireta do Estado do Rio Grande do Norte, vinculada à Secretaria
                                       de Estado do Planejamento e das Finanças (SEPLAN). A EMGERN tem sede e foro no Município
                                       de Natal, Capital do Estado do Rio Grande do Norte, e atuação em todo o território nacional,
                                       sendo indeterminado o prazo de sua duração.<br>
                                       Principal atividade a administração de empresas da massa falida do Estado, como BANDERN
                                       Banco do Estado do Rio Grande do RN, BANDERN Crédito Imobiliário - BCI e BDRN Banco do
                                       Desenvolvimento do Rio Grande do Norte</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 EMPRESA POTIGUAR DE PROMOÇÃO TURÍSTICA - EMPROTUR
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Diretor Presidente:</b> Bruno Giovanni dos Reis <br>
                                 <b>Telefones:</b> (84) 3232-2494 <br>
                                 <!-- /2513  -->
                                 <b>Website:</b> http://www.emprotur.setur.rn.gov.br/ <br>
                                 <b>E-mail:</b> gabemprotur@gmail.com <br>
                                 <!-- http://natalbrasil.tur.br/setur/emprotur -->
                                 <b>Horário de funcionamento:</b> 8h às 14h<br>
                                 <!-- (em obediência ao Decreto nº 28.692, de 2 de janeiro de 2019, prorrogado pelo Decreto Nº 28.963, de 1 de julho de 2019). -->
                                 <b>Endereço:</b> Av. Senador Dinarte Medeiros Mariz, Via Costeira, Centro de
                                 Convenções de Natal,
                                 S/N, Ponta Negra - Natal/RN. <br>
                                 CEP: 59090-002
                              </div>
                              <div class="pb-4">
                                 <a href="{{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_EMPROTUR.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 FUNDAÇÃO DE APOIO À PESQUISA DO ESTADO DO RIO GRANDE DO NORTE - FAPERN
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Diretor Geral:</b> Gilton Sampaio de Souza <br>
                                 <b>Telefones:</b> (84) 3232-1731 <br>
                                 <!-- <Li><b>Fax:</b> (84) 3232-1731</Li> -->
                                 <b>Website:</b> http://www.fapern.rn.gov.br <br>
                                 <b>E-mail:</b> gabinetepresidenciafapern@gmail.com <br>
                                 <b>Horário de funcionamento:</b> 8h às 14h <br>
                                 <!-- (em obediência ao Decreto nº 28.692, de 2 de janeiro de 2019, prorrogado pelo Decreto Nº 28.963, de 1 de julho de 2019). -->
                                 <b>Endereço:</b> BR 101 KM 0 - Av. Senador Salgado Filho, S/N, Centro
                                 Administrativo do Estado, Lagoa Nova - Natal/RN. <br>
                                 CEP: 59064-901
                              </div>
                              <div class="pb-4">
                                 <a href=" {{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_FAPERN.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>Fomentar pesquisas científicas, tecnológicas e de inovação, de acordo com as políticas de
                                       desenvolvimento sócio econômico do Rio Grande do Norte. Criada pela Lei Complementar Nº 257, de
                                       14/11/2003 e tem seu funcionamento regido pelo Decreto Nº 17.456, de 19/04/2004 com os seguintes
                                       objetivos: Apoiar e fomentar os programas ou os projetos de pesquisa realizados em instituições públicas
                                       ou privadas; a criação, a complementação e a modernização da infraestrutura necessária ao
                                       desenvolvimento científico e tecnológico; a concessão de bolsas de estudos e de pesquisa no País e no
                                       exterior; a formação ou a atualização de acervos bibliográficos e bancos de dados e de transmissão de
                                       informações, vinculados ao desenvolvimento do conhecimento.<br>
                                       - Fiscalizar e supervisionar a aplicação dos auxílios que fornece;<br>
                                       - Acompanhar e avaliar os programas de bolsas de estudo ou de pesquisa que conceder;<br>
                                       - Captar recursos mediante a celebração de contratos ou convênios com entidades nacionais ou
                                       internacionais;<br>
                                       - Assessorar o Governo do Estado e o Conselho Estadual de Ciência e Tecnologia (CONECIT) na
                                       formulação da política estadual de Ciência e Tecnologia;<br>
                                       - Manter articulação permanente com órgãos e instituições públicas e privadas, nacionais ou
                                       internacionais, visando à formação de redes de cooperação para o desenvolvimento científico,
                                       tecnológico ou de inovação;<br>
                                       - Estabelecer parcerias com o setor privado da economia, visando ao engajamento deste setor no
                                       desenvolvimento da pesquisa científica, tecnológica e de inovação no Estado;<br>
                                       - Estimular e apoiar a criação e o desenvolvimento de empresas de base tecnológica;<br>
                                       - Apoiar cursos de doutorado, de mestrado ou outros programas de pós-graduação de interesse
                                       estratégico para o Estado;<br>
                                       - Executar as políticas científica e tecnológica, no âmbito do Estado, respeitando as prioridades
                                       estabelecidas nos Planos Anuais de Ciência e Tecnologia, elaborados pelo CONECIT (art. 7º, da Lei
                                       Complementar Estadual n.º 136, de 12 de setembro de 1995);<br>
                                       - Gerenciar os recursos do Fundo Estadual de Desenvolvimento Científico e Tecnológico (FUNDET),
                                       conforme legislação em vigor;<br>
                                       - Desenvolver outras atividades compatíveis com o seu objetivo.</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>


                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 FUNDAÇÃO DE ATENDIMENTO SOCIOEDUCATIVO - FUNDASE/RN
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Diretor Geral:</b> Herculano Ricardo Campos <br>
                                 <b>Telefones:</b> (84) 3232-7001 / 4029 <br>

                                 <b>Website:</b> http://www.fundase.rn.gov.br <br>
                                 <b>E-mail:</b> www.cidadao.rn.gov.br <br>
                                 <b>Horário de funcionamento:</b> 8h às 14h <br>
                                 <!-- (em obediência ao Decreto nº 28.692, de 2 de janeiro de 2019, prorrogado pelo Decreto Nº 28.963, de 1 de julho de 2019).  -->
                                 <b>Endereço:</b> BR 101 KM 0 - Av. Senador Salgado Filho, S/N, Centro
                                 Administrativo do Estado, Lagoa Nova - Natal/RN. <br>
                                 CEP: 59064-901
                              </div>
                              <div class="pb-4">
                                 <a href="{{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_FUNDASE.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>No Estado do Rio Grande do Norte, os programas socioeducativos são executados
                                       pelo Poder Executivo, Fundase/RN e por alguns municípios, onde a
                                       fundação municipalizou o atendimento em meio aberto. Assim, a Fundase executa
                                       as medidas restritivas e privativas de liberdade (Semiliberdade, Internação
                                       Provisória e Internação), cabendo aos municípios a execução das medidas
                                       socioeducativas, em meio aberto, de Prestação de Serviços à Comunidade-PSC e
                                       de Liberdade Assistida-LA.</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 FUNDAÇÃO JOSÉ AUGUSTO - FJA
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Diretor Geral:</b> José Gilson Matias Barros <br>
                                 <b>Telefones:</b> (84) 3232-5304 / 5323 <br>

                                 <b>Website:</b> http://www.cultura.rn.gov.br <br>
                                 <b>E-mail:</b> cultura.gabinete@rn.gov.br <br>
                                 <b>Horário de funcionamento:</b> 8h às 14h <br>
                                 <!-- (em obediência ao Decreto nº 28.692, de 2 de janeiro de 2019, prorrogado pelo Decreto Nº 28.963, de 1 de julho de 2019).  -->
                                 <b>Endereço:</b> Rua Jundiaí, 641, Tirol - Natal/RN. <br>
                                 CEP: 59020-120
                              </div>
                              <div class="pb-4">
                                 <a href=" {{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_FJA.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>Responsável por desenvolver, incentivar, apoiar, difundir, estimular e documentar
                                       as atividades culturais.<br>
                                       À Fundação José Augusto (FJA) compete: I - estimular, desenvolver, difundir e
                                       documentar as atividades culturais do Estado, bem como as manifestações de
                                       cultura popular; II - desenvolver um plano editorial visando à promoção do autor
                                       potiguar e nordestino; III - desenvolver pesquisa sócio-econômicocultural visando
                                       ao conhecimento da realidade estadual; IV - promover ações voltadas para a
                                       preservação do patrimônio arqueológico, histórico e anistico do Estado
                                       (restauração, conservação e manutenção de bens imóveis e imóveis); V -
                                       coordenar e apoiar tecnicamente as atividades do Sistema Estadual de Bibliotecas
                                       dos museus ligados à instituição; VI - promover a documentação e manutenção de
                                       bens móveis e imóveis, culturais e históricos: VII - planejar, coordenar e
                                       supervisionar as atividades do Teatro Alberto Maranhão; VIII - planejar, coordenar
                                       e supervisionar as atividades do Instituto de Música Waldemar de Almeida; IX -
                                       planejar, coordenar e supervisionar as atividades da Orquestra Sinfônica do Rio
                                       Grande do Norte. - Art. 47.(LC1963/1999)</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 INSTITUTO DE ASSISTÊNCIA TÉCNICA E EXTENSÃO RURAL DO RIO GRANDE DO NORTE -
                                 EMATER
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Diretor Geral:</b> Cesar José de Oliveira <br>
                                 <b>Telefones:</b> (84) 3232-2240 / (84) 98137-2051 / (84) 3190-0620 / (84)
                                 3190-0621 / (84) 98132-8755 <br>
                                 <!-- <Li><b>Fax:</b> (84) 3232-2256</Li -->
                                 <b>Website:</b> http://www.emater.rn.gov.br/ <br>
                                 <b>E-mail:</b> emater@rn.gov.br <br>
                                 <b>Horário de funcionamento:</b> 07h às 17h <br>
                                 <b>Endereço:</b> BR 101 KM 0 - Av. Senador Salgado Filho, S/N, Centro
                                 Administrativo do Estado, Lagoa Nova - Natal/RN. <br>
                                 CEP: 59064-901
                              </div>
                              <div class="pb-4">
                                 <a href=" {{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviço_EMATER.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>À Empresa de Assistência Técnica e Extensão Rural do Rio Grande do Norte (EMATER-
                                       RN) compete; I - planejar, coordenar, executar, acompanhar e avaliar as atividades
                                       de assistência técnica e extensão rural, no território do Estado, em articulação com
                                       os órgãos e, bem assim, com as demais entidades públicas ou privadas do setor
                                       agrícola ou voltadas para o desenvolvimento rural; II - propor medidas para a
                                       melhoria das condições de vida das famílias rurais e executar, para esse fim ações
                                       educativas e tecnológicas relacionada com a agricultura, a pecuária, a nutrição, a
                                       saúde e a agroindústria; III - elaborar projetos de crédito rural, com a finalidade de
                                       desenvolvimento das atividades agropecuárias e agroindustriais, e orientar e
                                       acompanhar sua implantação; IV - executar ações tendentes à preservação e
                                       recuperação do meio ambiente, através de programas de florestamento e
                                       reflorestamento, manejo adequado dos recursos naturais, do solo, da água e das
                                       plantas e uso correto de agrotóxicos; V - estudar e propor diretrizes para as políticas
                                       agrícolas; VI - colaborar em atividades de pesquisa e experimentação agropecuárias,
                                       em articulação com entidades especializadas; VII - ministrar cursos de treinamento
                                       em assistência técnica e extensão rural;</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 INSTITUTO DE DEFESA E INSPEÇÃO AGROPECUÁRIA - IDIARN
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Diretor Geral:</b> Mário Victor Freire Manso <br>
                                 <b>Telefones:</b> (84) 3232-1112 / 1116 <br>

                                 <b>Website:</b> http://www.idiarn.rn.gov.br <br>
                                 <b>E-mail:</b> idiarn@rn.gov.br <br>
                                 <b>Horário de funcionamento:</b> 8h às 13h e 15h às 17h <br>
                                 <!-- (em obediência ao Decreto nº 28.692, de 2 de janeiro de 2019, prorrogado pelo Decreto Nº 28.963, de 1 de julho de 2019).  -->
                                 <b>Endereço:</b> BR 101 KM 0 - Av. Senador Salgado Filho, S/N, Centro
                                 Administrativo do Estado, Secretaria de Estado da Agricultura, da Pecuária e
                                 da Pesca
                                 (SAPE), Lagoa Nova - Natal/RN. <br>
                                 CEP: 59064-901</Li>
                              </div>
                              <div class="pb-4">
                                 <a href="{{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_IDIARN.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>Criado por meio de lei complementar Nº 324, de 29 de março de 2006, como uma autarquia
                                       com personalidade jurídica de direito público interno, com autonomia administrativa e financeira,
                                       patrimônio próprio, sede em Natal e com atuação em todo o território estadual, vinculado a
                                       Secretaria de Estado da Agricultura, da Pecuária e da Pesca do Rio Grande do Norte (SAPE), a
                                       qual estabelece competências ao órgão para promover e executar a Defesa Animal e Vegetal, o
                                       controle e a Inspeção de Produtos de Origem Agropecuária.</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 INSTITUTO DE DESENVOLVIMENTO SUSTENTÁVEL E MEIO AMBIENTE DO RN - IDEMA
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Diretor Geral:</b> Leonlene de Sousa Aguiar <br>
                                 <b>Telefones:</b> (84) 3113-6100 <br>
                                 <b>Website:</b> http://www.idema.rn.gov.br/ <br>
                                 <b>E-mail:</b> idema@rn.gov.br <br>
                                 <b>Horário de funcionamento:</b> 8h às 17h <br>
                                 <b>Endereço:</b> Av. Alexandrino de Alencar, 1701, Tirol - Natal/RN. <br>
                                 CEP: 59015-350
                              </div>
                              <div class="pb-4">
                                 <a href="{{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_IDEMA.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>Tem como competência coordenar, executar e supervisionar a política estadual do
                                       Meio Ambiente.<br>
                                       O Instituto de Desenvolvimento Sustentável e Meio Ambiente do Rio Grande do Norte -
                                       IDEMA é uma autarquia fruto da união de atribuições entre a Fundação Instituto de
                                       Desenvolvimento do RN (IDEC), criada pela Lei n.º 4.286, de 6 de dezembro de 1973,
                                       (alterada pela Lei n.º 4.414, de 04 de novembro de 1974), e a Coordenadoria de Meio
                                       Ambiente (CMA), criada por meio do Decreto n.º 8.718, de 16 de setembro de 1983.<br>  
                                       Ao Instituto de Desenvolvimento Econômico e Meio Ambiente do Rio Grande do
                                       Norte - IDEMARN compete: I - produzir e difundir informações técnicas e
                                       estatísticas pertinentes ao conhecimento da realidade estatal; II - realizar os
                                       estudos e pesquisas necessários à atividade do planejamento público estadual, ou
                                       mediante remuneração, a preço de mercado, de interesse de terceiros; III -
                                       formular, coordenar, executar e supervisionar a política estadual de preservação,
                                       conservação, aproveitamento, uso racional e recuperação dos recursos
                                       ambientais; IV - fiscalizar o cumprimento das normas de proteção, controle,
                                       utilização e recuperação dos recursos ambientais, aplicando as penalidades
                                       disciplinares e/ou compensatórias às infrações apuradas; e V - exercer outras
                                       atividades correlatas. Art. 38.(lc163/1999)</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 INSTITUTO DE FORMAÇÃO DE PROFESSORES PRESIDENTE KENNEDY - IFESP
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Diretora Geral:</b> Márcia Maria Alves de Assis <br>
                                 <b>Telefones:</b> (84) 3232-6232 / 6231 <br>
                                 <b>Website:</b> http://www.ifesp.edu.br <br>
                                 <b>E-mail:</b> ifesp@ifesp.edu.br <br>
                                 <b>Horário de funcionamento:</b> 8h às 12h (Manhã) / 13h30 às 17h30 (Tarde)
                                 / 18 às 22h (Noite)<br>
                                 <b>Endereço:</b> Av. Jaguarari, 2100, Lagoa Nova - Natal/RN. <br>
                                 CEP: 59062-500
                              </div>
                              <div class="pb-4">
                                 <a href=" {{ asset('http://www.ifesp.edu.br/ik/index.php/principal/carta-de-servico-ao-cidadao-ifesp') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>Promover a formação de qualidade para profissionais que atuam em processos
                                       educacionais, através do ensino, pesquisa e extensão.<br>
                                       O Instituto Kennedy, como é popularmente conhecido, conta com 43 professores
                                       em seu quadro que atuam nos cursos de Graduação, Extensão e Pós-graduação e
                                       nas atividades de pesquisa. Na Graduação são ofertados três cursos: Pedagogia -
                                       Licenciatura, Licenciatura em Matemática e Licenciatura em Letras – Habilitação
                                       Língua Portuguesa. Na Pós-graduação Latos Sensu são oferecidos cursos de
                                       especialização em seis áreas: Gestão de Processos Educacionais, Educação de
                                       Jovens de Adultos, Língua Portuguesa, Educação Ambiental, Educação Infantil e
                                       Educação Matemática. Associados a estes cursos existem os seguintes grupos de
                                       pesquisa: Corporeidade e Educação; Estudos em Educação Matemática; e
                                       Linguagem, Ensino e Aprendizagem.</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 INSTITUTO DE GESTÃO DAS ÁGUAS DO ESTADO DO RIO GRANDE DO NORTE - IGARN
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Diretor Geral:</b> Paulo Sidney Gomes Silva <br>
                                 <b>Telefones:</b> (84) 98137-2094 <br>
                                 <!-- <Li><b>Fax:</b> (84) 3209-9255 </Li> -->
                                 <b>Website:</b> http://www.igarn.rn.gov.br/ <br>
                                 <b>E-mail:</b> igarn@rn.gov.br <br>
                                 <b>Horário de funcionamento:</b> 8h às 14h <br>
                                 <b>Endereço:</b> Rua Raposo Câmara, 3588, Candelária - Natal/RN. <br>
                                 CEP: 59065-150
                              </div>
                              <div class="pb-4">
                                 <a href=" {{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviço_IGARN.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>Responsável pela gestão técnica e operacional dos recursos hídricos em todo o
                                       território norte-rio-grandense.<br>
                                       Criado pela Lei nº 8.086, de 15 de abril de 2002, e desde de janeiro de 2013 a Lei
                                       Complementar 483 revogou essa lei, é uma autarquia vinculada à Secretaria de Estado do
                                       Meio Ambiente e dos Recursos Hídricos (Semarh), dotada de personalidade jurídica de
                                       direito público interno e autonomia administrativa e financeira, com patrimônio próprio.</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 INSTITUTO DE PESOS E MEDIDAS DO RN - IPEM
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Diretor Geral:</b> João Henrique Maia de Farias <br>
                                 <b>Telefones:</b>  (84) 3222-9079 / (84) 98147-9433 (WhatsApp) <br>
                                 <b>Website:</b> http://www.ipem.rn.gov.br/ <br>
                                 <b>E-mail:</b> direcao@ipem.rn.gov.br <br>
                                 <b>Horário de funcionamento:</b> 7h às 13h <br>
                                 <!-- (em obediência ao Decreto nº 28.692, de 2 de janeiro de 2019, prorrogado pelo Decreto Nº 28.963, de 1 de julho de 2019). -->
                                 <b>Endereço:</b> Av. Olinto Meira, 1036, Barro Vermelho - Natal/RN. <br>
                                 CEP: 59030-180
                              </div>
                              <div class="pb-4">
                                 <a href="{{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviço_IPEM.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>O Instituto de Pesos e Medidas do Rio Grande do Norte - IPEM/RN foi criado pela Lei n°
                                       6.203, de 06 de dezembro de 1991. Por meio de Lei Estadual, regulamentada através
                                       do decreto n° 11.417, de 18 de agosto de 1992, o Instituto se transformou em uma
                                       autarquia estadual do Rio Grande do Norte, vinculada administrativamente à
                                       Secretaria de Desenvolvimento Econômico, da Ciência, da Tecnologia e da Inovação
                                       (SEDEC/RN), mediante convênio com o INMETRO - Instituto Nacional de Metrologia,
                                       Qualidade e Tecnologia. O órgão é responsável por executar serviços considerados
                                       essenciais na proteção ao cidadão em suas relações de consumo, garantindo a
                                       segurança e a saúde do consumidor no exercício da verificação e fiscalização em três
                                       áreas: Avaliação da Conformidade, Instrumentos e Pré-medidos.</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 INSTITUTO DE PREVIDÊNCIA DO RN - IPERN
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Presidente:</b> Nereu Batista Linhares <br>
                                 <b>Telefones:</b> Em fase de instalação <br>
                                 <!-- <Li><b>Fax:</b> (84) 3232-2903 </Li> -->
                                 <b>Website:</b> http://www.ipe.rn.gov.br <br>
                                 <b>E-mail:</b> ipernouvidoria@rn.gov.br <br>
                                 <b>Horário de funcionamento:</b> 7h30 às 13h30 <br>
                                 <b>Endereço:</b> Rua Jundiaí, 410, Tirol - Natal/RN. <br>
                                 CEP: 59020-120
                              </div>
                              <div class="pb-4">
                                 <a href=" {{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_IPERN.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>O IPERN, autarquia estadual, com sede e foro na Capital do Estado do Rio Grande do Norte, foi
                                       criado pela Lei nº 2.728, de 1º de maio de 1962. Com a reforma da previdência e através da Lei
                                       Complementar Nº 308, o instituto passou por uma reestruturação do Regime Próprio de
                                       Previdência Social do Estado do Rio Grande do Norte (RPPS/RN), conforme trata o artigo 40 da
                                       Constituição Federal.<br>
                                       São beneficiários do IPERN, na qualidade de segurados obrigatórios, todos os servidores
                                       efetivos, civis e militares dos três Poderes e das autarquias estaduais, que exerçam atividade
                                       remunerada.</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 INSTITUTO TÉCNICO-CIENTÍFICO DE PERÍCIA - ITEP
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Diretor Geral:</b> Marcos José Brandão Guimarães <br>
                                 <b>Telefones:</b> (84) 3232-6905 / 6917 / 6916 <br>
                                 <b>Website:</b> http://www.itep.rn.gov.br/ <br>
                                 <b>E-mail:</b> itepchgab@hotmail.com <br>
                                 <b>Horário de funcionamento:</b> 8h às 18h <br>
                                 <b>Endereço:</b> Av. Duque de Caxias, 97, Ribeira - Natal/RN. <br>
                                 CEP: 59010-200
                              </div>
                              <div class="pb-4">
                                 <a href=" {{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviço_ITEP.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>Prestar um serviço de qualidade para a sociedade do Rio Grande do Norte, nas
                                       áreas da medicina legal, criminalística e identificação.<br>
                                       As ações do Itep devem está em sintonia com os objetivos traçados pela Secretaria
                                       Estadual de Segurança Pública e Defesa Social, sempre tratando a Segurança Pública
                                       como prioridade, prezando pela austeridade, transparência e o comprometimento com os
                                       resultados.</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 JUNTA COMERCIAL DO ESTADO DO RN - JUCERN
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Presidente:</b> Carlos Augusto de Paiva Maia <br>
                                 <b>Telefones:</b> (84) 3232-3056 <br>
                                 <b>Website:</b> http://www.jucern.rn.gov.br <br>
                                 <b>E-mail:</b> presidência@jucern.rn.gov.br <br>
                                 <b>Horário de funcionamento:</b> 8h às 14h <br>
                                 <b>Endereço:</b> Av. Duque de Caxias, 214, Ribeira - Natal/RN. <br>
                                 CEP: 59012-200
                              </div>
                              <div class="pb-4">
                                 <a href="{{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_JUCERN.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>É o órgão responsável pelo registro, fé pública e publicidade dos documentos
                                       arquivados pelos empresários, sociedade empresarias e sociedades cooperativas
                                       no Estado.<br>
                                       A Junta Comercial do Estado do Rio Grande do Norte é uma instituição secular, nascida em 13
                                       de setembro de 1899, com o advento da Lei Estadual nº 132. Com a Lei Estadual nº 3.715, de
                                       10 de dezembro de 1968, ela é transformada em autarquia, isto é, passou a fazer parte da
                                       administração indireta, cuja vinculação é à Secretaria de Desenvolvimento Econômico, antiga
                                       Secretaria de Indústria e Comércio, de acordo com a legislação estadual.</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 POLÍCIA CIVIL DO ESTADO DO RIO GRANDE DO NORTE - PC/RN
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Delegada Geral:</b> Ana Cláudia Saraiva Gomes <br>
                                 <b>Telefones:</b> (84) 3232-4074 / 7670 / 7667 <br>
                                 <b>Website:</b> http://www.policiacivil.rn.gov.br <br>
                                 <b>Horário de funcionamento:</b> 7h às 18h <br>
                                 <b>Endereço:</b> Av. Interventor Mário Negócio Câmara, 2550, Cidade
                                 da Esperança - Natal/RN. <br>
                                 CEP: 59064-600
                              </div>
                              <div class="pb-4">
                                 <a href=" {{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_PCRN.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>Funções de polícia judiciária e a apuração de infrações penais.<br>
                                       De acordo com a Constituição Federal, nos termos do artigo 144, § 4º: &quot;As polícias
                                       civis, dirigidas por delegados de polícia de carreira, incumbem, ressalvada a
                                       competência da União, as funções de polícia judiciária e a apuração de infrações
                                       penais, exceto as militares."</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 POLÍCIA MILITAR DO RN - PM/RN
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Comandante Geral:</b> Coronel PM Alarico José Pessoa Azevêdo Júnior <br>
                                 <b>Telefones:</b> (84) 3232-6330 / 6333 <br>
                                 <b>Website:</b> http://www.pm.rn.gov.br <br>
                                 <b>Horário de funcionamento:</b> 7h às 18h <br>
                                 <b>Endereço:</b> Av. Rodrigues Alves, S/N, Tirol - Natal/RN. <br>
                                 CEP: 59020-200
                              </div>
                              <div class="pb-4">
                                 <a href="{{ asset('docs/orgaosdogoverno/cartasdeservico/Carta_de_Serviços_PMRN.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>A Organização Básica da Polícia Militar do Estado do Rio Grande do Norte foi instituída pela Lei
                                       Complementar nº 090, de 04 de janeiro de 1991.<br>
                                       A referida Lei Complementar prevê que a Polícia Militar do Estado do Rio Grande do Norte é
                                       estruturada em órgãos de apoio e órgãos de execução, onde os órgãos de direção realizam
                                       o comando e a administração da Polícia Militar, sob autoridade do Comandante Geral, e
                                       incumbem-se do seu planejamento e organização, visando às necessidades em pessoal e
                                       material e o emprego da Corporação para o cumprimento de suas missões, e acionam, por
                                       meio de diretrizes e ordens, os órgãos de apoio e de execução, controlando e fiscalizando a
                                       atuação desses órgãos.</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 PROCON ESTADUAL
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Coordenador Geral do Procon Estadual:</b> Thiago Gomes da Silva <br>
                                 <b>Telefones:</b> (84) 98146-6136 <br>
                                 <b>Website:</b> http://www.procon.rn.gov.br/ <br>
                                 <b>Horário de funcionamento:</b> 8h às 14h <br>
                                 <!-- (em obediência ao Decreto nº 28.692, de 2 de janeiro de 2019, prorrogado pelo Decreto Nº 28.963, de 1 de julho de 2019). -->
                                 <b>Endereço:</b> Av. Sen. Salgado Filho, 1808, Prédio do DER/RN, Lagoa Nova
                                 - Natal/RN <br>
                                 CEP: 59075-000
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="border-b tab">
                        <div class="border-l-2 border-transparent relative">
                           <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                           <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                              <span class="text-lightBlue-500 text-sm text-left" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 UNIVERSIDADE DO ESTADO DO RIO GRANDE DO NORTE - UERN
                              </span>
                              <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                 <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                 </svg>
                              </div>
                           </header>
                           <div class="tab-content">
                              <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                                 <b>Reitora:</b> Profa. Dra. Cicília Raquel Maia Leite <br>
                                 <b>Telefones:</b> (84) 3315-2145 <br>
                                 <!-- <Li><b>Fax:</b> (84) 3315-2108</Li> -->
                                 <b>Website:</b> http://www.uern.br/ <br>
                                 <b>E-mail:</b> chgab@uern.br <br>
                                 <b>Horário de funcionamento:</b> 8h às 17h <br>
                                 <b>Endereço:</b> Rua Almino Afonso, 478, Centro - Mossoró/RN. <br>
                                 CEP: 59610-210
                              </div>
                              <div class="pb-4">
                                 <a href=" {{ asset('http://portal.uern.br/wp-content/uploads/2019/10/UERN_Carta-de-servi%C3%A7os.pdf') }}" target="_blank"><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full text-xs" name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                       Carta de Serviço
                                    </button></a>
                              </div>
                              <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox" id="this">
                              <header class="flex gap-2 items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label" for="this">
                                 <span class="text-gray-500 text-sm text-left" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                                    <b>Competências</b>
                                 </span>
                                 <div class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                    <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24" stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                       <polyline points="6 9 12 15 18 9">
                                       </polyline>
                                    </svg>
                                 </div>
                              </header>
                              <div class="tab-content">
                                 <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm text-justify" name="texto">
                                    <br>
                                    <p>À Fundação Universidade Estadual do Rio Grande do Norte (FURRN), instituição de caráter
                                       educacional, compete: I - atuar na área de ensino, pesquisa e extensão, visando contribuir para
                                       a solução de problemas regionais de natureza econômica, social e cultural; e II - exercer outras
                                       atividades correlatas. Art.48(LC163/1999).</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                  </div>
               </section>
            </main>

         </div>

      </div>
   </div>
</section>
@endsection