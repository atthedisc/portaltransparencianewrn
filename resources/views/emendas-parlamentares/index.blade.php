@extends('layouts.app')

@section('title')
- Emendas Parlamentares
@endsection

@section('content')
<section class="text-gray-600 body-font">
   <div class="container px-5 mx-auto">
      <div class="flex flex-col text-center w-full">

         <h1 class="text-lg text-gray-900 tracking-widest font-medium title-font pb-2" name="texto" style="font-size: 1.125rem; line-height: 2rem;">
            <i class="fas fa-file-alt"></i>
            EMENDAS PARLAMENTARES
         </h1>

         <div class="flex flex-col text-center w-full mb-3">
            <p class="lg:w-3/4 mx-auto leading-relaxed text-sm text-center" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
               Tem como objetivo fornecer dados obtidos através dos relatórios exigidos pela Lei de
               Responsabilidade Fiscal de maneira simples, gráfica e objetiva.
            </p>
         </div>

         <ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
            <li class="inline-flex items-center">
               <a href="{{ url('/') }}">Início</a>
               <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                  <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd"></path>
               </svg>
            </li>
            <li class="inline-flex items-center">
               <a href="#" class="text-teal-400">
                  Emendas Parlamentares
               </a>
            </li>
         </ul>

         <hr class="border-b-2 border-lightBlue-300 mb-6 mx-4">

         <div class="grid md:grid-flow-col mx-auto">

            <table class='mx-10 mb-auto max-w-sm md:max-w-lg rounded-lg bg-white divide-y divide-lightBlue-200 overflow-hidden text-center'>

               <tr class="text-lightBlue-800 text-center bg-white-50">
                  <th class="font-medium text-sm uppercase tracking-wider px-6 py-4 text-center" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                     <style>
                        summary {
                           position: relative;
                           text-decoration: none;
                        }

                        summary::before {
                           content: '';
                           position: absolute;
                           width: 100%;
                           height: 2px;
                           border-radius: 2px;
                           background-color: #39a5e9;
                           bottom: 0;
                           left: 0;
                           transform-origin: right;
                           transform: scaleX(0);
                           transition: transform .3s ease-in-out;

                        }

                        summary:hover::before {
                           transform-origin: left;
                           transform: scaleX(1);
                        }
                     </style>
                     <details style="cursor: url(hand.cur), pointer;">
                        <summary>EMENDAS PARLAMENTARES 2021</summary>
                        <br>
                        <table>
                           <tr>
                              <td>
                              <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2021/Execução Orçamentária - Janeiro 2021.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 01/2021
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2021/Execução Orçamentária - Fevereiro 2021.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 02/2021
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2021/Execução Orçamentária - Março 2021.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 03/2021
                                 </a>
                                 <br>
                                 <br>
                                 
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2021/Execução Orçamentária - Abril 2021.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 04/2021
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2021/Execução Orçamentária - Maio 2021.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 05/2021
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2021/Execução Orçamentária - Junho 2021.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 06/2021
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2021/Execução Orçamentária - Julho 2021.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 07/2021
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2021/Execução Orçamentária - Agosto 2021.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 08/2021
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2021/Execução Orçamentária - Setembro 2021.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 09/2021
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2021/Execução Orçamentária - Outubro 2021.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 10/2021
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2021/Execução Orçamentária - Novembro 2021.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 11/2021
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2021/Execução Orçamentária - Dezembro 2021.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 12/2021
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                        </table>
                     </details>
                     <br>
                     <details style="cursor: url(hand.cur), pointer;">
                        <summary>EMENDAS PARLAMENTARES 2022</summary>
                        <br>
                        <table>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2022/Execução Orçamentária - Janeiro 2022.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 01/2022
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2022/Execução Orçamentária - Fevereiro 2022.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 02/2022
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2022/Execução Orçamentária - Março 2022.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 03/2022
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2022/Execução Orçamentária - Abril 2022.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 04/2022
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2022/Execução Orçamentária - Maio 2022.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 05/2022
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2022/Execução Orçamentária - Junho 2022.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 06/2022
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2022/Execução Orçamentária - Julho 2022.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 07/2022
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2022/Execução Orçamentária - Agosto 2022.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 08/2022
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2022/Execução Orçamentária - Setembro 2022.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 09/2022
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2022/Execução Orçamentária - Outubro 2022.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 10/2022
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2022/Execução Orçamentária - Novembro 2022.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 11/2022
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2022/Execução Orçamentária - Dezembro 2022.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 12/2022
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                        </table>
                     </details>
                     <br>
                     <details style="cursor: url(hand.cur), pointer;">
                        <summary>EMENDAS PARLAMENTARES 2023</summary>
                        <br>
                        <table>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2023/Execução Orçamentária - Janeiro 2023.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 01/2023
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2023/Execução Orçamentária - Fevereiro 2023.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 02/2023
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2023/Execução Orçamentária - Março 2023.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 03/2023
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2023/Execução Orçamentária - Abril 2023.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 04/2023
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2023/Execução Orçamentária - Maio 2023.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 05/2023
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2023/Execução Orçamentária - Junho 2023.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 06/2023
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2023/Execução Orçamentária - Julho 2023.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 07/2023
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2023/Execução Orçamentária - Agosto 2023.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 08/2023
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2023/Execução Orçamentária - Setembro 2023.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 09/2023
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2023/Execução Orçamentária - Outubro 2023.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 10/2023
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2023/Execução Orçamentária - Novembro 2023.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 11/2023
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2023/Execução Orçamentária - Dezembro 2023.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 12/2023
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                        </table>
                     </details>
                     <br>
                     <details style="cursor: url(hand.cur), pointer;">
                        <summary>EMENDAS PARLAMENTARES 2024</summary>
                        <br>
                        <table>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2024/Execução Orçamentária - Janeiro 2024.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 01/2024
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2024/Execução Orçamentária - Fevereiro 2024.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 02/2024
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <i class="fa-regular fa-file-pdf fa-xl"></i>
                                 <a href="{{ asset('docs/emendasparlamentares/2024/Execução Orçamentária - Março 2024.pdf') }}" class="text-xs text-lightBlue-500 hover:text-lightBlue-700 hover:underline" target="_blank" name="texto" style="font-size: 0.75rem; line-height: 1.25rem;">
                                    Execução Orçamentária - 03/2024
                                 </a>
                                 <br>
                                 <br>
                              </td>
                           </tr>
                        </table>
                     </details>
                  </th>
               </tr>
            </table>
         </div>
      </div>
   </div>
</section>
@endsection