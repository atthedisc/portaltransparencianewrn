@extends('layouts.app')

@section('title')
- Ouvidoria
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/accordion.css') }}">
@endsection

@section('content')
<div class="flex flex-col text-center w-full">
    <h1 class="text-lg text-gray-900 tracking-widest font-medium title-font pb-2" name="texto"
        style="font-size: 1.125rem; line-height: 2rem;">
        <i class="fas fa-search-plus"></i>
        OUVIDORIA
    </h1>

    <ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto"
    style="font-size: 0.875rem; line-height: 1.25rem;">
    <li class="inline-flex items-center">
        <a href="{{ url('/') }}">Início</a>
        <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
            <path fill-rule="evenodd"
                d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                clip-rule="evenodd"></path>
        </svg>
    </li>
    <li class="inline-flex items-center">
        <a href="#" class="text-teal-400">
            Ouvidoria
        </a>
    </li>
    </ul>

    <hr class="border-b-2 border-lightBlue-300 mb-6 mx-4">

    <div class="flex flex-wrap justify-center">

        <main class="pl-4 pb-4 md:w-1/2">
            <section class="shadow row bg-white rounded-lg">
                <div class="tabs">

                    <div class="border-b">
                        <div class="border-l-2 border-transparent relative">
                            <header class="p-5 pl-8 pr-8 select-none label">
                                <span class="text-gray-500 text-sm text-center" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    CANAIS DE ATENDIMENTO DA OUVIDORIA
                                </span>
                            </header>
                        </div>
                    </div>

                    <div class="border-b">
                        <div class="border-l-2 border-transparent relative">
                            <header
                                class="flex justify-between items-center p-5 pl-8 pr-8 select-none tab-label"
                                for="this">
                                <span class="text-lightBlue-500 text-sm text-left" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    PRESENCIALMENTE
                                </span>
                            </header>
                            <div class="content">
                                <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">      
                                    <b>Horário de funcionamento:</b> 8h às 16h (Segunda a Sexta-feira)<br>
                                    <b>Endereço:</b> BR 101 KM 0 - Av. Senador Salgado Filho, S/N, Centro Administrativo do Estado, Lagoa Nova - Natal/RN. <br> 
                                    CEP: 59064-901<br>
                                    <b>Telefone:</b> 84 98128-1103<br>
                                    <b>Email:</b> <a href="mailto:ouvidoria@control.rn.gov.br">ouvidoria@control.rn.gov.br</a><br>
                                </div>
                                <div class="pb-4">
                                    <div class="pl-8 pr-8 pb-5 text-gray-500 text-center text-sm" name="texto"
                                        style="font-size: 0.875rem; line-height: 1.25rem;">
                                        Em caso de dúvidas, contate-nos:
                                    </div>
                                    <a href="{{ asset('https://wa.me/5584981281103')}}"
                                        target="_blank"><button
                                        class="bg-transparent hover:bg-green-500 text-green-700 font-semibold hover:text-white py-2 px-4 border border-green-500 hover:border-transparent rounded-full text-xs"
                                        name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                        Whatsapp
                                        </button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="border-b">
                        <div class="border-l-2 border-transparent relative">
                            <header
                                class="flex justify-between items-center p-5 pl-8 pr-8 select-none label"
                                for="this">
                                <span class="text-lightBlue-500 text-sm text-left" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    ONLINE
                                </span>                          
                            </header>
                            <div class="content">
                                <div class="pl-8 pr-8 pb-5 text-gray-500 text-center text-sm" name="texto"
                                    style="font-size: 0.875rem; line-height: 1.25rem;">
                                    Plataforma Integrada de Ouvidoria e Acesso à Informação do Governo:<br>
                                </div>
                                <div class="pb-4">
                                    <a href="{{ asset('https://falabr.cgu.gov.br/publico/Manifestacao/SelecionarTipoManifestacao.aspx?ReturnUrl=%2f') }}"
                                        target="_blank"><button
                                        class="bg-transparent hover:bg-yellow-500 text-yellow-700 font-semibold hover:text-white py-2 px-4 border border-yellow-500 hover:border-transparent rounded-full text-xs"
                                        name="texto" style="font-size: 0.75rem; line-height: 1rem;">
                                        Fala.BR
                                        </button></a>
                                </div>
                                <div class="border-b tab">
                                    <div class="border-l-2 border-transparent relative">
                                        <input class="w-full absolute left-0 z-10 cursor-pointer opacity-0 h-20" type="checkbox"
                                            id="this">
                                        <header
                                            class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none tab-label"
                                            for="this">
                                            <span class="text-gray-500 text-sm text-left" name="texto"
                                                style="font-size: 0.815rem; line-height: 1.25rem;">
                                                <b>Caso haja dificuldades no acesso do portal, clique aqui.</b>
                                            </span>  
                                            <div
                                            class="rounded-full border border-gray w-7 h-7 flex items-center justify-center test">
                                            <svg aria-hidden="true" class="" data-reactid="266" fill="none" height="24"
                                                stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                                <polyline points="6 9 12 15 18 9">
                                                </polyline>
                                            </svg>
                                        </div>                                 
                                        </header>
                                        <div class="tab-content">
                                            <div class="pl-8 pr-8 pb-5 text-gray-500 text-left text-sm" name="texto"
                                                style="font-size: 0.825rem; line-height: 1.25rem;">      
                                                Há situações onde o navegador pode apresentar o erro abaixo:<br>
                                                <img src="{{ asset('images/prints/fail01.png') }}" alt="" class="shadow my-2">
                                                Isso se deve pelo fato do certificado de segurança não estar pré-registrado no navegador.<br><br>
                                                Para resolver esse problema, basta clicar no botão "Avançado" e em seguida clicar no "Ir para sistema.ouvidoria.gov.br (não seguro)".<br>
                                                <img src="{{ asset('images/prints/fail02.png') }}" alt="" class="shadow my-2">
                                                Embora apareça esse alerta, o site do governo é perfeitamente seguro.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                   
                </div>
            </section>
        </main>
    </div>
</div>
@endsection 
