@extends('layouts.app')

@section('title')
- Conselhos Estaduais
@endsection

@section('content')
<section class="text-gray-600 body-font">
    <div class="container px-5 mx-auto">
        <div class="flex flex-col text-center w-full">

            <h1 class="text-lg text-gray-900 tracking-widest font-medium title-font pb-2" name="texto"
                style="font-size: 1.125rem; line-height: 2rem;">
                <i class="fas fa-users-cog"></i>
                Conselhos Estaduais do Rio Grande do Norte
            </h1>

            <div class="flex flex-col text-center w-full mb-3">
                <p class="lg:w-3/4 mx-auto leading-relaxed text-sm text-gray-500 text-center" name="texto"
                    style="font-size: 0.875rem; line-height: 1.25rem;">
                    Órgãos e Entidades
                </p>
            </div>

            <ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto"
                style="font-size: 0.875rem; line-height: 1.25rem;">
                <li class="inline-flex items-center">
                    <a href="{{ url('/') }}">Início</a>
                    <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                        <path fill-rule="evenodd"
                            d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                            clip-rule="evenodd"></path>
                    </svg>
                </li>
                <li class="inline-flex items-center">
                    <a href="#" class="text-teal-400">
                        Conselhos Estaduais
                    </a>
                </li>
            </ul>

            <hr class="border-b-2 border-lightBlue-300 mb-6 mx-4">

            <table class="divide-y divide-lightBlue-200 w-full">

                <thead>
                    <tr class="text-lightBlue-800 text-center bg-lightBlue-50">
                        <th class="p-3 font-medium text-sm uppercase tracking-wider border border-lightBlue-200 hidden lg:table-cell"
                            name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                            Órgãos/Entidades
                        </th>
                        <th class="p-3 font-medium text-sm uppercase tracking-wider border border-lightBlue-200 hidden lg:table-cell"
                            name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                            Nome do Conselho
                        </th>
                        <th class="p-3 font-medium text-sm uppercase tracking-wider border border-lightBlue-200 hidden lg:table-cell"
                            name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                            Link
                        </th>
                    </tr>
                </thead>

                <tbody>
                    <tr class="bg-white text-sm lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Órgãos/Entidades</span>
                            <span class="font-semibold">AGN</span> – Agência de Fomento
                        </td>
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Nome do
                                Conselho</span>
                            Conselho Administrativo e Fiscal
                        </td>
                        <td
                            class="w-full lg:w-1/6 p-3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Link</span>
                            <a href="{{ asset('http://www.agn.rn.gov.br/Conteudo.asp?TRAN=ITEM&TARG=89560&ACT=&PAGE=0&PARM=&LBL=AGN%2FRN') }}"
                                target="_blank" class="text-blue-400 hover:text-blue-600 underline">Visitar</a>
                        </td>
                    </tr>

                    <tr class="bg-white text-sm lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Órgãos/Entidades</span>
                            <span class="font-semibold">CAERN</span> – Companhia de Água e Esgotos do Rio Grande do
                            Norte
                        </td>
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Nome do
                                Conselho</span>
                            Conselho Administrativo e Fiscal
                        </td>
                        <td
                            class="w-full lg:w-1/6 p-3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Link</span>
                            <a href="{{ asset('http://portal.caern.com.br/Conteudo.asp?TRAN=ITEM&TARG=659&ACT=&PAGE=0&PARM=&LBL=A+Caern') }}"
                                target="_blank" class="text-blue-400 hover:text-blue-600 underline">Visitar</a>
                        </td>
                    </tr>

                    <tr class="bg-white text-sm lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Órgãos/Entidades</span>
                            <span class="font-semibold">CEASA</span> – Centrais de Abastecimento o RN
                        </td>
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Nome do
                                Conselho</span>
                            Conselho Administrativo e Fiscal
                        </td>
                        <td
                            class="w-full lg:w-1/6 p-3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Link</span>
                            <a href="{{ asset('https://www.ceasa.rn.gov.br/p/conselho-de-administracao') }}"
                                target="_blank" class="text-blue-400 hover:text-blue-600 underline">Visitar</a>
                        </td>
                    </tr>

                    <tr class="bg-white text-sm lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Órgãos/Entidades</span>
                            <span class="font-semibold">CEHAB</span> – Companhia de processamento de Dados do Rio Grande
                            do Norte
                        </td>
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Nome do
                                Conselho</span>
                            Conselho Administrativo e Fiscal
                        </td>
                        <td
                            class="w-full lg:w-1/6 p-3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Link</span>
                            <a href="{{ asset('http://www.cehab.rn.gov.br/Conteudo.asp?TRAN=ITEM&TARG=34813&ACT=&PAGE=0&PARM=&LBL=A+CEHAB') }}"
                                target="_blank" class="text-blue-400 hover:text-blue-600 underline">Visitar</a>
                        </td>
                    </tr>

                    <tr class="bg-white text-sm lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Órgãos/Entidades</span>
                            <span class="font-semibold">DATANORTE</span> – Companhia de Processamento de Dados do RN
                        </td>
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Nome do
                                Conselho</span>
                            Conselho Administrativo e Fiscal
                        </td>
                        <td
                            class="w-full lg:w-1/6 p-3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Link</span>
                            <a href="{{ asset('http://www.datanorte.rn.gov.br/Conteudo.asp?TRAN=ITEM&TARG=253688&ACT=&PAGE=0&PARM=&LBL=Conselho') }}"
                                target="_blank" class="text-blue-400 hover:text-blue-600 underline">Visitar</a>
                        </td>
                    </tr>

                    <tr class="bg-white text-sm lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Órgãos/Entidades</span>
                            <span class="font-semibold">EMATER RN</span> – Instituto de Assistência Técnica e Extensão
                            Rural
                        </td>
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Nome do
                                Conselho</span>
                            Conselho Administrativo
                        </td>
                        <td
                            class="w-full lg:w-1/6 p-3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Link</span>
                            <a href="{{ asset('http://www.emater.rn.gov.br/Conteudo.asp?TRAN=ITEM&TARG=234009&ACT=&PAGE=0&PARM=&LBL=Relat%F3rios+Ouvidoria') }}"
                                target="_blank" class="text-blue-400 hover:text-blue-600 underline">Visitar</a>
                        </td>
                    </tr>

                    <tr class="bg-white text-sm lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Órgãos/Entidades</span>
                            <span class="font-semibold">EMGERN</span> – Empresa Gestora de Ativos do RN
                        </td>
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Nome do
                                Conselho</span>
                            Conselho Administrativo e Fiscal
                        </td>
                        <td
                            class="w-full lg:w-1/6 p-3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Link</span>
                            <a href="{{ asset('http://www.adcon.rn.gov.br/ACERVO/EMGERN/DOC/DOC000000000274806.PDF') }}"
                                target="_blank" class="text-blue-400 hover:text-blue-600 underline">Visitar</a>
                        </td>
                    </tr>

                    <tr class="bg-white text-sm lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Órgãos/Entidades</span>
                            <span class="font-semibold">EMPROTUR</span> – Empresa Potiguar de Promoção Turística
                        </td>
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Nome do
                                Conselho</span>
                            Conselho Administrativo e Fiscal
                        </td>
                        <td
                            class="w-full lg:w-1/6 p-3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Link</span>
                            <a href="{{ asset('https://www.emprotur.setur.rn.gov.br/organograma') }}"
                                target="_blank" class="text-blue-400 hover:text-blue-600 underline">Visitar</a>
                        </td>
                    </tr>

                    <tr class="bg-white text-sm lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Órgãos/Entidades</span>
                            <span class="font-semibold">FAPERN</span> – Fundação de Apoio à Pesquisa do RN
                        </td>
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Nome do
                                Conselho</span>
                            Conselho Fiscal e Conselho Estadual de Ciência e Tecnologia
                        </td>
                        <td
                            class="w-full lg:w-1/6 p-3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Link</span>
                            <a href="{{ asset('http://www.fapern.rn.gov.br/Conteudo.asp?TRAN=ITEM&TARG=8872&ACT=&PAGE=0&PARM=&LBL=Institui%E7%E3o') }}"
                                target="_blank" class="text-blue-400 hover:text-blue-600 underline">Visitar</a>
                        </td>
                    </tr>

                    <tr class="bg-white text-sm lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Órgãos/Entidades</span>
                            <span class="font-semibold">FJA</span> – Fundação José Augusto
                        </td>
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Nome do
                                Conselho</span>
                            Conselho Estadual de Cultura da Lei de Cultura Câmara Cascudo
                        </td>
                        <td
                            class="w-full lg:w-1/6 p-3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Link</span>
                            <a href="{{ asset('http://www.cultura.rn.gov.br/Conteudo.asp?TRAN=ITEM&TARG=179796&ACT=&PAGE=&PARM=&LBL=Materia') }}"
                                target="_blank" class="text-blue-400 hover:text-blue-600 underline">Visitar</a>
                        </td>
                    </tr>

                    <tr class="bg-white text-sm lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Órgãos/Entidades</span>
                            <span class="font-semibold">FUNDASE</span> – Fundação de Atendimento Socioeducativo
                        </td>
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Nome do
                                Conselho</span>
                            Conselho Estadual da Criança e do Adolescente
                        </td>
                        <td
                            class="w-full lg:w-1/6 p-3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Link</span>
                            <a href="{{ asset('http://www.fundase.rn.gov.br/Conteudo.asp?TRAN=ITEM&TARG=200442&ACT=&PAGE=&PARM=&LBL=Materia') }}"
                                target="_blank" class="text-blue-400 hover:text-blue-600 underline">Visitar</a>
                        </td>
                    </tr>

                    <tr class="bg-white text-sm lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Órgãos/Entidades</span>
                            <span class="font-semibold">IDEMA</span> – Instituto de Desenvolvimento Sustentável e Meio
                            Ambiente
                        </td>
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Nome do
                                Conselho</span>
                            Conselho Estadual do Meio Ambiente (CONEMA)
                        </td>
                        <td
                            class="w-full lg:w-1/6 p-3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Link</span>
                            <a href="{{ asset('http://www.idema.rn.gov.br/Conteudo.asp?TRAN=ITEM&TARG=175069&ACT=&PAGE=0&PARM=&LBL=conema') }}"
                                target="_blank" class="text-blue-400 hover:text-blue-600 underline">Visitar</a>
                        </td>
                    </tr>

                    <tr class="bg-white text-sm lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Órgãos/Entidades</span>
                            <span class="font-semibold">IFESP</span> – Instituto de Formação de Professores Presidente
                            Kennedy
                        </td>
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Nome do
                                Conselho</span>
                            Conselho Administrativo e o Conselho Científico/Pedagógico
                        </td>
                        <td
                            class="w-full lg:w-1/6 p-3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Link</span>
                            <a href="{{ asset('https://ifesp.edu.br/ik/index.php/all-departments/19-categoria-principal/artigos/233-administracao-superior') }}"
                                target="_blank" class="text-blue-400 hover:text-blue-600 underline">Visitar</a>
                        </td>
                    </tr>

                    <tr class="bg-white text-sm lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Órgãos/Entidades</span>
                            <span class="font-semibold">IPERN</span> – Instituto de Previdência do Estado do RN
                        </td>
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Nome do
                                Conselho</span>
                            Conselho Fiscal e Conselho Estadual de Previdência Social
                        </td>
                        <td
                            class="w-full lg:w-1/6 p-3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Link</span>
                            <a href="{{ asset('http://adcon.rn.gov.br/ACERVO/IPERN/DOC/DOC000000000131573.PDF') }}"
                                target="_blank" class="text-blue-400 hover:text-blue-600 underline">Visitar</a>
                        </td>
                    </tr>

                    <tr class="bg-white text-sm lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Órgãos/Entidades</span>
                            <span class="font-semibold">JUCERN</span> – Junta Comercial do RN
                        </td>
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Nome do
                                Conselho</span>
                            Conselho de Contabilidade CRC /RN (representação) e Conselho Regional de Adm do RN - CRA
                        </td>
                        <td
                            class="w-full lg:w-1/6 p-3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Link</span>
                            <a href="{{ asset('http://www.jucern.rn.gov.br/Conteudo.asp?TRAN=ITEM&TARG=8431&ACT=&PAGE=0&PARM=&LBL=Institui%E7%E3o') }}"
                                target="_blank" class="text-blue-400 hover:text-blue-600 underline">Visitar</a>
                        </td>
                    </tr>

                    <tr class="bg-white text-sm lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Órgãos/Entidades</span>
                            <span class="font-semibold">PGE</span> – Procuradoria-Geral do Estado
                        </td>
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Nome do
                                Conselho</span>
                            Conselho Superior
                        </td>
                        <td
                            class="w-full lg:w-1/6 p-3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Link</span>
                            <a href="{{ asset('http://www.pge.rn.gov.br/Conteudo.asp?TRAN=ITEM&TARG=208874&ACT=&PAGE=0&PARM=&LBL=') }}"
                                target="_blank" class="text-blue-400 hover:text-blue-600 underline">Visitar</a>
                        </td>
                    </tr>

                    <tr class="bg-white text-sm lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Órgãos/Entidades</span>
                            <span class="font-semibold">POTIGÁS</span> – Companhia Potiguar de Gás
                        </td>
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Nome do
                                Conselho</span>
                            Conselho Administrativo e Fiscal
                        </td>
                        <td
                            class="w-full lg:w-1/6 p-3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Link</span>
                            <a href="{{ asset('https://www.potigas.com.br/institucional') }}" target="_blank"
                                class="text-blue-400 hover:text-blue-600 underline">Visitar</a>
                        </td>
                    </tr>

                    <tr class="bg-white text-sm lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Órgãos/Entidades</span>
                            <span class="font-semibold">SAPE</span> – Secretaria de Estado da Pecuária e da Pesca
                        </td>
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Nome do
                                Conselho</span>
                            Conselho Estadual de Desenvolvimento Rural Sustentável (CEDRUS)
                        </td>
                        <td
                            class="w-full lg:w-1/6 p-3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Link</span>
                            <a href="{{ asset('http://adcon.rn.gov.br/ACERVO/gac/DOC/DOC000000000059070.PDF') }}"
                                target="_blank" class="text-blue-400 hover:text-blue-600 underline">Visitar</a>
                        </td>
                    </tr>

                    <tr class="bg-white text-sm lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Órgãos/Entidades</span>
                            <span class="font-semibold">SEAD</span> – Secretaria de Estado da Administração
                        </td>
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Nome do
                                Conselho</span>
                            Conselho de Política de Administração e Remuneração de Pessoal (COARP)
                            Conselho de Gerenciamento do Patrimônio do Estado (CGP)
                            Conselho Diretor do Fundo de Desenvolvimento do Sistema de Pessoal do Estado (FUNDESPE)
                        </td>
                        <td
                            class="w-full lg:w-1/6 p-3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Link</span>
                            <a href="{{ asset('http://www.searh.rn.gov.br/Conteudo.asp?TRAN=ITEM&TARG=2791&ACT=&PAGE=0&PARM=&LBL=A+Secretaria') }}"
                                target="_blank" class="text-blue-400 hover:text-blue-600 underline">Visitar</a>
                        </td>
                    </tr>

                    <tr class="bg-white text-sm lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Órgãos/Entidades</span>
                            <span class="font-semibold">SEAP</span> – Secretaria de Estado da Administração
                            Penitenciária do RN
                        </td>
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Nome do
                                Conselho</span>
                            Conselho Penitenciário do RN
                        </td>
                        <td
                            class="w-full lg:w-1/6 p-3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Link</span>
                            <a href="{{ asset('http://www.seap.rn.gov.br/Conteudo.asp?TRAN=ITEM&TARG=95935&ACT=&PAGE=0&PARM=&LBL=Conselho+Penitenci%E1rio') }}"
                                target="_blank" class="text-blue-400 hover:text-blue-600 underline">Visitar</a>
                        </td>
                    </tr>

                    <tr class="bg-white text-sm lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Órgãos/Entidades</span>
                            <span class="font-semibold">SEDEC</span> – Secretaria de Desenvolvimento Econômico do Estado
                            do RN
                        </td>
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Nome do
                                Conselho</span>
                            Conselho Fiscal da FAPERN – CONECIT
                        </td>
                        <td
                            class="w-full lg:w-1/6 p-3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Link</span>
                            <a href="{{ asset('http://www.fapern.rn.gov.br/Conteudo.asp?TRAN=ITEM&TARG=8872&ACT=&PAGE=0&PARM=&LBL=Institui%E7%E3o') }}"
                                target="_blank" class="text-blue-400 hover:text-blue-600 underline">Visitar</a>
                        </td>
                    </tr>

                    <tr class="bg-white text-sm lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Órgãos/Entidades</span>
                            <span class="font-semibold">SEEC</span> – Secretaria de Estado da Educação, da Cultura, do
                            esporte e do lazer do RN
                        </td>
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Nome do
                                Conselho</span>
                            Conselho Estadual de Educação e Conselho Estadual de Cultura
                        </td>
                        <td
                            class="w-full lg:w-1/6 p-3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Link</span>
                            <a href="{{ asset('http://www.educacao.rn.gov.br/Conteudo.asp?TRAN=ITEM&TARG=175466&ACT=&PAGE=0&PARM=&LBL=Conselho+Estadual+de+Cultura') }}"
                                target="_blank" class="text-blue-400 hover:text-blue-600 underline">Visitar</a>
                        </td>
                    </tr>

                    <tr class="bg-white text-sm lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Órgãos/Entidades</span>
                            <span class="font-semibold">SEMARH</span> – Secretaria de Estado do Meio Ambiente e dos
                            Recursos Hídricos
                        </td>
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Nome do
                                Conselho</span>
                            Conselho Estadual dos Recursos Hídricos
                        </td>
                        <td
                            class="w-full lg:w-1/6 p-3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Link</span>
                            <a href="{{ asset('http://www.semarh.rn.gov.br/conteudo.asp?tran=item&targ=20214&act=&page=0&parm=&lbl=conselho') }}"
                                target="_blank" class="text-blue-400 hover:text-blue-600 underline">Visitar</a>
                        </td>
                    </tr>

                    <tr class="bg-white text-sm lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Órgãos/Entidades</span>
                            <span class="font-semibold">SEMJIDH</span> – Secretaria de Estado das Mulheres, da
                            Juventude, da Igualdade Racial e dos Direitos Humanos do RN
                        </td>
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Nome do
                                Conselho</span>
                            Conselho Estadual dos Direitos Humanos e Cidadania (COEDHUCI);
                            Conselho Estadual de Direito do Consumidor (CEDC);
                            Conselho Estadual de Políticas de Promoção da Igualdade Racial (CONSEPPIR)
                            Conselho Estadual dos Direitos da Criança e do Adolescente (CONSEC);
                            Conselho Estadual de Juventude (CEJUV);
                            Conselho Estadual dos Direitos das Mulheres (CEDIM);
                            Conselho Estadual dos Direitos da Pessoa com Deficiência (COEDE);
                            Conselho Estadual de Políticas Públicas sobre Drogas (CONED);
                            Conselho Estadual Deliberativo do Programa Estadual de Apoio e Proteção a Testemunhas,
                            Vítimas e Familiares de Vítimas da Violência (PROVITA);
                            Conselho Gestor Estadual do Programa de Proteção a Crianças e Adolescentes Ameaçados de
                            Morte (PPCAAM);

                        </td>
                        <td
                            class="w-full lg:w-1/6 p-3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Link</span>
                            <a href="{{ asset('http://www.semjidh.rn.gov.br/Conteudo.asp?TRAN=ITEM&TARG=220052&ACT=&PAGE=0&PARM=&LBL=conselhos') }}"
                                target="_blank" class="text-blue-400 hover:text-blue-600 underline">Visitar</a>
                        </td>
                    </tr>

                    <tr class="bg-white text-sm lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Órgãos/Entidades</span>
                            <span class="font-semibold">SEPLAN</span> – Secretaria de Estado do Planejamento e das
                            Finanças
                        </td>
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Nome do
                                Conselho</span>
                            Conselho de Desenvolvimento do Estado
                        </td>
                        <td
                            class="w-full lg:w-1/6 p-3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Link</span>
                            <a href="{{ asset('http://www.semarh.rn.gov.br/conteudo.asp?tran=item&targ=20214&act=&page=0&parm=&lbl=conselho') }}"
                                target="_blank" class="text-blue-400 hover:text-blue-600 underline">Visitar</a>
                        </td>
                    </tr>

                    <tr class="bg-white text-sm lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Órgãos/Entidades</span>
                            <span class="font-semibold">SESAP</span> – Secretaria de Estado de Saúde Pública do RN
                        </td>
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Nome do
                                Conselho</span>
                            Conselho Estadual da Saúde
                        </td>
                        <td
                            class="w-full lg:w-1/6 p-3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Link</span>
                            <a href="{{ asset('http://www.saude.rn.gov.br/Conteudo.asp?TRAN=ITEM&TARG=133547&ACT=&PAGE=&PARM=&LBL=MAT%C9RIA') }}"
                                target="_blank" class="text-blue-400 hover:text-blue-600 underline">Visitar</a>
                        </td>
                    </tr>

                    <tr class="bg-white text-sm lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Órgãos/Entidades</span>
                            <span class="font-semibold">SET</span> – Secretaria de Estado da Tributação do RN
                        </td>
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Nome do
                                Conselho</span>
                            Conselho de Recursos Fiscais
                        </td>
                        <td
                            class="w-full lg:w-1/6 p-3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Link</span>
                            <a href="{{ asset('http://www.set.rn.gov.br/content/aplicacao/set_v2/conselho_recursos_fiscais/enviados/listagem_filtro.asp?assunto=55&assuntoEsp=506&mes=Janeiro&ano=2012&oque=votos') }}"
                                target="_blank" class="text-blue-400 hover:text-blue-600 underline">Visitar</a>
                        </td>
                    </tr>

                    <tr class="bg-white text-sm lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Órgãos/Entidades</span>
                            <span class="font-semibold">SETHAS</span> – Secretaria de Estado do Trabalho, da Habitação e
                            da Ação Social
                        </td>
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Nome do
                                Conselho</span>
                            Conselho Estadual de Assistência Social do RN
                        </td>
                        <td
                            class="w-full lg:w-1/6 p-3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Link</span>
                            <a href="{{ asset('http://www.sethas.rn.gov.br/Conteudo.asp?TRAN=ITEM&TARG=180613&ACT=&PAGE=0&PARM=&LBL=Ceas-RN') }}"
                                target="_blank" class="text-blue-400 hover:text-blue-600 underline">Visitar</a>
                        </td>
                    </tr>

                    <tr class="bg-white text-sm lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0"
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Órgãos/Entidades</span>
                            <span class="font-semibold">UERN</span> – Universidade do Estado do RN
                        </td>
                        <td
                            class="w-full md:py-2 pt-4 pb-4 lg:w-1/3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Nome do
                                Conselho</span>
                            Conselho Diretor / Conselho Curador / Conselho Universitário e Conselho de Ensino, Pesquisa
                            e Extensão.
                        </td>
                        <td
                            class="w-full lg:w-1/6 p-3 text-gray-800 text-center border border-lightBlue-200 text-center block lg:table-cell relative lg:static">
                            <span class="lg:hidden absolute -top-4 -left-4 bg-blue-200 px-2 py-1 text-xs" name="texto"
                                style="font-size: 0.75rem; line-height: 1rem;">Link</span>
                            <a href="{{ asset('https://portal.uern.br/conselhos/') }}" target="_blank"
                                class="text-blue-400 hover:text-blue-600 underline">Visitar</a>
                        </td>
                    </tr>

                </tbody>
            </table>

        </div>
    </div>
</section>
@endsection