@extends('layouts.app')

@section('title')
- Plano Estratégico
@endsection

@section('content')
<section class="text-gray-600 body-font">
   <div class="container px-5 mx-auto">
      <div class="flex flex-col text-center w-full">

         <h1 class="text-lg text-gray-900 tracking-widest font-medium title-font pb-2" name="texto"
            style="font-size: 1.125rem; line-height: 2rem;">
            <i class="fas fa-exclamation-circle"></i>
            PLANO ESTRATÉGICO
         </h1>

         
         <ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto"
            style="font-size: 0.875rem; line-height: 1.25rem;">
            <li class="inline-flex items-center">
               <a href="{{ url('/') }}">Início</a>
               <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                  <path fill-rule="evenodd"
                     d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                     clip-rule="evenodd"></path>
               </svg>
            </li>
            <li class="inline-flex items-center">
               <a href="#" class="text-teal-400">
                  Plano Estratégico
               </a>
            </li>
         </ul>

         <hr class="border-b-2 border-lightBlue-300 mb-6 mx-4">

         
         <table
            class='mx-auto mb-8 max-w-sm md:max-w-lg rounded-lg bg-white divide-y divide-lightBlue-200 overflow-hidden text-center'>

            <tr class="text-lightBlue-800 text-center bg-lightBlue-50">
               <th class="font-medium text-sm uppercase tracking-wider px-6 py-3 text-center" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">
                  VISÃO DE FUTURO
               </th>
            </tr>

            <tbody class="divide-y divide-lightBlue-200">
               <tr>
                  <td class="px-6 py-4 text-left text-sm">
                     <p class="text-sm text-lightBlue-500" 
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        RN, um lugar melhor para viver com desenvolvimento e justiça social.
                  </p>
                  </td>
               </tr>
            </tbody>
         </table>

         <table
            class='mx-auto mb-8 max-w-sm md:max-w-lg rounded-lg bg-white divide-y divide-lightBlue-200 overflow-hidden text-center'>

            <tr class="text-lightBlue-800 text-center bg-lightBlue-50">
               <th class="font-medium text-sm uppercase tracking-wider px-6 py-3 text-center" name="texto"
                  style="font-size: 0.875rem; line-height: 1.25rem;">
                  PREMISSAS
               </th>
            </tr>

            <tbody class="divide-y divide-lightBlue-200">
               <tr>
                  <td class="px-6 py-4 text-left text-sm">
                     <p class="text-sm text-lightBlue-500" 
                        name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                        Sociedade enquanto parte integrante do ciclo de planejamento governamental.
                  </p>
                  </td>
               </tr>
               <tr>
                <td class="px-6 py-4 text-left text-sm">
                   <p class="text-sm text-lightBlue-500" 
                      name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                      Território como espaços de pactuações sociais e da promoção do desenvolvimento.
                </p>
                </td>
             </tr>
             <tr>
                <td class="px-6 py-4 text-left text-sm">
                   <p class="text-sm text-lightBlue-500" 
                      name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                      Ética, transparência e controle social nas ações públicas.
                </p>
                </td>
             </tr>
             <tr>
                <td class="px-6 py-4 text-left text-sm">
                   <p class="text-sm text-lightBlue-500" 
                      name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                      Eficiência, eficácia e efetividade na prestação dos serviços públicos.
                </p>
                </td>
             </tr>
             <tr>
                <td class="px-6 py-4 text-left text-sm">
                   <p class="text-sm text-lightBlue-500" 
                      name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                      Gestão integrada com foco em resultados.
                </p>
                </td>
             </tr>
            </tbody>
         </table>

        <table class='mx-auto mb-8 max-w-sm md:max-w-lg rounded-lg bg-white divide-y divide-lightBlue-200 overflow-hidden text-center'>
            <tr class="text-lightBlue-800 text-center bg-lightBlue-50">
                <th class="font-medium text-sm uppercase tracking-wider px-6 py-3" colspan="2" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">
                    FOCOS PRIORITÁRIOS
                </th>
            </tr>
            <tr>
                <td class="px-6 py-4 text-left text-sm md:px-3 md:py-2 w-1/2 border-r">
                    <p class="text-sm text-lightBlue-500" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">População e Setores mais vulneráveis.</p>
                </td>
                <td class="px-6 py-4 text-left text-sm md:px-3 md:py-2 w-1/2">
                    <p class="text-sm text-lightBlue-500" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">Desenvolvimento Econômico Social, com interiorização.</p>
                </td>
            </tr>
        </table>
        

        <table class='mx-auto mb-8 max-w-sm md:max-w-lg rounded-lg bg-white divide-y divide-lightBlue-200 overflow-hidden text-center'>
            <tr>
                <th class="font-medium text-lg uppercase tracking-wider px-6 py-2" colspan="4" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">EIXOS</th>
            </tr>
            <tr class="text-lightBlue-800 text-center bg-lightBlue-50">
                <th class="font-medium text-sm uppercase tracking-wider px-6 py-3" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">EIXO 1</th>
                <th class="font-medium text-sm uppercase tracking-wider px-6 py-3" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">EIXO 2</th>
                <th class="font-medium text-sm uppercase tracking-wider px-6 py-3" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">EIXO 3</th>
                <th class="font-medium text-sm uppercase tracking-wider px-6 py-3" name="texto" style="font-size: 0.875rem; line-height: 1.25rem;">EIXO 4</th>
            </tr>
            <tr>
                <td class="px-4 py-4 text-center text-sm md:px-3 md:py-2">
                    <p class="text-sm text-lightBlue-500" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                        Desenvolvimento Governamental e Gestão Pública Transparente e Eficiente
                    </p>
                </td>
                <td class="px-4 py-4 text-center text-sm md:px-3 md:py-2">
                    <p class="text-sm text-lightBlue-500" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                        Desenvolvimento Sustentável e Integração Regional e Metropolitana
                    </p>
                </td>
                <td class="px-4 py-4 text-center text-sm md:px-3 md:py-2">
                    <p class="text-sm text-lightBlue-500" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                        Desenvolvimento Humano e Social e Segurança Pública
                    </p>
                </td>
                <td class="px-4 py-4 text-center text-sm md:px-3 md:py-2">
                    <p class="text-sm text-lightBlue-500" name="texto" style="font-size: 0.815rem; line-height: 1.25rem;">
                        Desenvolvimento Econômico Sustentável
                    </p>
                </td>
            </tr>
        </table>
        
         <div class="flex justify-center mt-8">
            <a href="https://portaldemetas.rn.gov.br/" target="_blank" class="flex items-center mr-4">
                <img src="images\logo\dark-logo-pm-horizontal.png" alt="Logo Portal de Metas" class="h-10">
            </a>
            <a href="https://observa.rn.gov.br/index.html" target="_blank" class="flex items-center mr-4">
                <img src="images\logo\logotipo-observarn.png" alt="Logo ObservaRN" class="h-8">
            </a>
            <a href="https://www.seplan.rn.gov.br/documentos/Plano%20Plurianual/" target="_blank" class="flex items-center mr-4">
                <img src="images\logo\1646e77abb99f3.png" alt="Logo PPA" class="h-20">
            </a>
            
        </div>
        
      </div>
   </div>
</section>
@endsection
