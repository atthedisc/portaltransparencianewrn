@extends('layouts.app')

@section('title')
- Contabilidade Fácil
@endsection

@section('content')
<!-- Section -->
<section class="text-gray-600 body-font">
   <ul class="flex text-gray-500 text-sm justify-end mx-4 my-2" name="texto"
            style="font-size: 0.875rem; line-height: 1.25rem;">
            <li class="inline-flex items-center">
                <a href="{{ url('/') }}">Inicio</a>
                <svg class="h-5 w-auto text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                    <path fill-rule="evenodd"
                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                        clip-rule="evenodd"></path>
                </svg>
            </li>
            <li class="inline-flex items-center">
                <a class="text-teal-400" href="{{ url('/oque-e-contabilidade-facil') }}">O que é contabilidade fácil</a>
            </li>
        </ul>
        <hr class="border-b-2 border-lightBlue-300 mb-12 mx-4">
   <div class="container px-5 mx-auto">
      <div class="flex flex-col text-center w-full">

         {{-- <center><img src="images/logo/contabilidadefacil.png" width="200" height="200" class="mb-4"></center> --}}

         <h1 class="text-2xl text-blue-800 tracking-widest title-font mb-1 mt-1" name="texto" style="font-size: 1.5rem; line-height: 2rem;">O QUE É CONTABILIDADE
            FÁCIL?
         </h1>

         <hr class="border border-gray-300 mb-2 mt-2 mx-4">

         <center><img class="pb-6 pt-2" src="images/logo/contabilidadefacil.png" width="200" height="200"></center>

         <p class="lg:w-4/5 mx-auto leading-relaxed text-base text-justify" name="texto" style="font-size: 1rem; line-height: 1.5rem;">Contabilidade Fácil foi criada para ajudar
            os usuários da informação
            contábil a compreender a situação orçamentária, financeira, patrimonial e fiscal do estado de maneira
            objetiva, simples, gráfica
            e lúdica, assim como trazer respostas às dúvidas mais recorrentes sobre o dia a dia da contabilidade e
            orçamento público.

            <h1 class="text-xl text-blue-800 tracking-widest title-font mt-8 mb-2 uppercase" name="texto" style="font-size: 1.25rem; line-height: 1.75rem;">Dividida em quatro grandes eixos
            </h1>
         </p>
      </div>

      <div class="container px-5 py-5 mx-auto">

         <div class="flex items-center lg:w-4/5 mx-auto border-b pb-10 mb-10 border-gray-200 sm:flex-row flex-col">
            <div
               class="sm:w-32 sm:h-32 h-20 w-20 sm:mr-10 inline-flex items-center justify-center rounded-full bg-blue-100 text-blue-500 flex-shrink-0">
               <i class="fas fa-search-dollar fa-3x"></i>
            </div>
            <div class="flex-grow sm:text-left text-center mt-6 sm:mt-0">
               <h2 class="text-gray-900 text-lg title-font font-medium mb-2 uppercase" name="texto" style="font-size: 1.125rem; line-height: 1.75rem;">RN Contábil</h2>
               <p class="leading-relaxed text-base text-justify" name="texto" style="font-size: 1rem; line-height: 1.5rem;">RN Contábil, de periodicidade bimestral,
                  tem como objetivo fornecer dados obtidos através dos relatórios exigidos pela Lei de Responsabilidade
                  Fiscal
                  de maneira simples, gráfica e objetiva.</p>
            </div>
         </div>

         <div class="flex items-center lg:w-4/5 mx-auto border-b pb-10 mb-10 border-gray-200 sm:flex-row flex-col">
            <div class="flex-grow sm:text-left text-center mt-6 sm:mt-0">
               <h2 class="text-gray-900 text-lg title-font font-medium mb-2 uppercase lg:text-right" name="texto" style="font-size: 1.125rem; line-height: 1.75rem;">RN em Foco</h2>
               <p class="leading-relaxed text-base text-justify" name="texto" style="font-size: 1rem; line-height: 1.5rem;">RN em Foco, elaborado quadrimestralmente e também a
                  cada semestre,
                  traz como função apresentar os resultados da execução orçamentária de maneira completa, com análises
                  gráficas
                  e comentários que permitam entender os resultados alcançados.</p>
            </div>
            <div
               class="sm:w-32 sm:order-none order-first sm:h-32 h-20 w-20 sm:ml-10 inline-flex items-center justify-center rounded-full bg-red-100 text-red-500 flex-shrink-0">
               <i class="fas fa-bullseye fa-3x"></i>
            </div>
         </div>

         <div class="flex items-center lg:w-4/5 mx-auto border-b pb-10 mb-10 border-gray-200 sm:flex-row flex-col">
            <div
               class="sm:w-32 sm:h-32 h-20 w-20 sm:mr-10 inline-flex items-center justify-center rounded-full bg-yellow-100 text-yellow-500 flex-shrink-0">
               <i class="fas fa-plus fa-3x"></i>
            </div>
            <div class="flex-grow sm:text-left text-center mt-6 sm:mt-0">
               <h2 class="text-gray-900 text-lg title-font font-medium mb-2 uppercase" name="texto" style="font-size: 1.125rem; line-height: 1.75rem;">RN + Fácil</h2>
               <p class="leading-relaxed text-base text-justify" name="texto" style="font-size: 1rem; line-height: 1.5rem;">RN + Fácil, elaborado anualmente,
                  tem o propósito de apresentar as informações referentes ao Balanço Geral do Estado de forma lúdica,
                  objetiva e atrativa para a população em geral.
               </p>
            </div>
         </div>

         <div class="flex items-center lg:w-4/5 mx-auto sm:flex-row flex-col">
            <div class="flex-grow sm:text-left text-center mt-6 sm:mt-0">
               <h2 class="text-gray-900 text-lg title-font font-medium mb-2 uppercase lg:text-right" name="texto" style="font-size: 1.125rem; line-height: 1.75rem;">Dicas de Execução Orçamentária</h2>
               <p class="leading-relaxed text-base text-justify" name="texto" style="font-size: 1rem; line-height: 1.5rem;">Dicas de Execução Orçamentária têm a finalidade de
                  tirar dúvidas das pessoas
                  que trabalham e acompanham a execução orçamentária do governo de forma simples e didática.</p>
            </div>
            <div
               class="sm:w-32 sm:order-none order-first sm:h-32 h-20 w-20 sm:ml-10 inline-flex items-center justify-center rounded-full bg-green-100 text-green-500 flex-shrink-0">
               <i class="fas fa-lightbulb fa-3x"></i>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- /Section -->
@endsection